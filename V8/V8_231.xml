<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard J. Gornall</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">132</other_info_on_meta>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">TELESONIX</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Tellur.</publication_title>
      <place_in_publication>2: 69. 1837  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus TELESONIX</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, teleos, complete, and onyx, claw, alluding to petals</other_info_on_name>
    <other_info_on_name type="fna_id">132401</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, rhizomatous, not stoloniferous;</text>
      <biological_entity id="o14151" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome branched, scaly.</text>
      <biological_entity id="o14152" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems erect, leafy, 3–23 cm, densely stipitate-glandular and eglandular-pubescent.</text>
      <biological_entity id="o14153" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="23" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eglandular-pubescent" value_original="eglandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in basal rosettes and cauline;</text>
      <biological_entity id="o14154" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14155" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <relation from="o14154" id="r1209" name="in" negation="false" src="d0_s3" to="o14155" />
    </statement>
    <statement id="d0_s4">
      <text>cauline reduced distally;</text>
      <biological_entity constraint="cauline" id="o14156" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules expanded from petiole base;</text>
      <biological_entity id="o14157" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character constraint="from petiole base" constraintid="o14158" is_modifier="false" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o14158" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petiole stipitate-glandular;</text>
      <biological_entity id="o14159" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade usually reniform, sometimes orbiculate-cordate, sometimes lobed, base cordate, ultimate margins 1–2 times crenate, apex rounded, surfaces stipitate-glandular, adaxial surface ± smooth;</text>
      <biological_entity id="o14160" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="orbiculate-cordate" value_original="orbiculate-cordate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o14161" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o14162" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character constraint="apex" constraintid="o14163" is_modifier="false" name="size_or_quantity" src="d0_s7" value="1-2 times crenate apex rounded" />
        <character constraint="apex" constraintid="o14164" is_modifier="false" name="size_or_quantity" src="d0_s7" value="1-2 times crenate apex rounded" />
      </biological_entity>
      <biological_entity id="o14163" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o14164" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o14165" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation palmate.</text>
      <biological_entity constraint="adaxial" id="o14166" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences cylindric panicles or simple cymes, terminal from terminal bud in rosette, branches 2–3-flowered, sometimes flowers solitary, bracteate.</text>
      <biological_entity id="o14167" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character constraint="from terminal bud" constraintid="o14170" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14168" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character constraint="from terminal bud" constraintid="o14170" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14169" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character constraint="from terminal bud" constraintid="o14170" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o14170" name="bud" name_original="bud" src="d0_s9" type="structure" />
      <biological_entity id="o14171" name="rosette" name_original="rosette" src="d0_s9" type="structure" />
      <biological_entity id="o14172" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="2-3-flowered" value_original="2-3-flowered" />
      </biological_entity>
      <biological_entity id="o14173" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <relation from="o14170" id="r1210" name="in" negation="false" src="d0_s9" to="o14171" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers ± radially symmetric;</text>
      <biological_entity id="o14174" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less radially" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium adnate to 1/4–1/2 of ovary, free from ovary 1–3.5 mm, purplish, sometimes green;</text>
      <biological_entity id="o14175" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character char_type="range_value" constraint="of ovary" constraintid="o14176" from="0" name="quantity" src="d0_s11" to="1/4-1/2" />
        <character constraint="from ovary" constraintid="o14177" is_modifier="false" name="fusion" notes="" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o14176" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o14177" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 5, purplish green;</text>
      <biological_entity id="o14178" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish green" value_original="purplish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 5, crimson-purple to violet-purple;</text>
      <biological_entity id="o14179" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="crimson-purple" name="coloration_or_density" src="d0_s13" to="violet-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectary tissue green, encircling base of styles at junction of ovary and free-hypanthium;</text>
      <biological_entity constraint="style" id="o14180" name="tissue" name_original="tissue" src="d0_s14" type="structure" constraint_original="style nectary; style">
        <character is_modifier="false" name="coloration" src="d0_s14" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o14181" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o14182" name="style" name_original="styles" src="d0_s14" type="structure" />
      <biological_entity id="o14183" name="junction" name_original="junction" src="d0_s14" type="structure" />
      <biological_entity id="o14184" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
      <biological_entity id="o14185" name="free-hypanthium" name_original="free-hypanthium" src="d0_s14" type="structure" />
      <relation from="o14180" id="r1211" name="encircling" negation="false" src="d0_s14" to="o14181" />
      <relation from="o14180" id="r1212" name="part_of" negation="false" src="d0_s14" to="o14182" />
      <relation from="o14180" id="r1213" name="at" negation="false" src="d0_s14" to="o14183" />
      <relation from="o14183" id="r1214" name="part_of" negation="false" src="d0_s14" to="o14184" />
      <relation from="o14183" id="r1215" name="part_of" negation="false" src="d0_s14" to="o14185" />
    </statement>
    <statement id="d0_s15">
      <text>stamens 10;</text>
      <biological_entity id="o14186" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments linear to subulate;</text>
      <biological_entity id="o14187" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s16" to="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary 1/2 inferior, 2-locular, carpels connate in proximal 1/2;</text>
      <biological_entity id="o14188" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s17" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s17" value="2-locular" value_original="2-locular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14190" name="1/2" name_original="1/2" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>placentation axile;</text>
      <biological_entity id="o14189" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in proximal 1/2" constraintid="o14190" is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character is_modifier="false" name="placentation" src="d0_s18" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles 2;</text>
      <biological_entity id="o14191" name="style" name_original="styles" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas 2.</text>
      <biological_entity id="o14192" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 2-beaked.</text>
      <biological_entity id="o14193" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="2-beaked" value_original="2-beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds brown, oblong, smooth.</text>
    </statement>
    <statement id="d0_s23">
      <text>x = 7.</text>
      <biological_entity id="o14194" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s22" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o14195" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>wc North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="wc North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Boykinia Nuttall sect. Telesonix (Rafinesque) Gornall &amp; Bohm</discussion>
  <discussion>Species 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals crimson-purple, length at least 1.75 times sepals; styles connate ca. 3/4 their lengths.</description>
      <determination>1 Telesonix jamesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals violet-purple, length ca. 1.3 times sepals; styles connate ca. 1/2 their lengths.</description>
      <determination>2 Telesonix heucheriformis</determination>
    </key_statement>
  </key>
</bio:treatment>