<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Douglas in W. J. Hooker" date="1832" rank="species">bracteosum</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker,  Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 233. 1832  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species bracteosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065808</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–4 m.</text>
      <biological_entity id="o13538" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, with dense, yellowish, shiny, sessile, crystalline, round glands, sparsely pubescent throughout;</text>
      <biological_entity id="o13539" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13540" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" name="reflectance" src="d0_s1" value="shiny" value_original="shiny" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s1" value="crystalline" value_original="crystalline" />
        <character is_modifier="true" name="shape" src="d0_s1" value="round" value_original="round" />
      </biological_entity>
      <relation from="o13539" id="r1143" modifier="throughout" name="with" negation="false" src="d0_s1" to="o13540" />
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes absent;</text>
      <biological_entity id="o13541" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o13542" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o13541" id="r1144" name="at" negation="false" src="d0_s2" to="o13542" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o13543" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o13544" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o13543" id="r1145" name="on" negation="false" src="d0_s3" to="o13544" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 2–10 cm, sparsely pubescent;</text>
      <biological_entity id="o13545" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13546" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate, deeply 5–7-lobed, cleft 1/2+ to midrib, (1.5–) 2–10 (–22) cm, base cordate, surfaces with yellow, dull, sessile glands and sparse hairs abaxially, with yellow, shiny, sessile glands and glabrous adaxially, lobes with main segments ovatelanceolate, margins 1–2 times sharply serrate, apex acute, shallowly lobed.</text>
      <biological_entity id="o13547" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o13548" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="5-7-lobed" value_original="5-7-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o13549" from="1/2" name="quantity" src="d0_s5" upper_restricted="false" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="22" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13549" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o13550" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o13551" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o13552" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13553" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13554" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13555" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity constraint="main" id="o13556" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o13557" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character constraint="apex" constraintid="o13558" is_modifier="false" name="size_or_quantity" src="d0_s5" value="1-2 times sharply serrate apex acute" />
        <character constraint="apex" constraintid="o13559" is_modifier="false" name="size_or_quantity" src="d0_s5" value="1-2 times sharply serrate apex acute" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13558" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o13559" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o13551" id="r1146" name="with" negation="false" src="d0_s5" to="o13552" />
      <relation from="o13553" id="r1147" name="with" negation="false" src="d0_s5" to="o13554" />
      <relation from="o13555" id="r1148" name="with" negation="false" src="d0_s5" to="o13556" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ascending to erect, 20–50-flowered racemes, (10–) 15–30 cm, axis sparsely pubescent, flowers evenly spaced.</text>
      <biological_entity id="o13560" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
      </biological_entity>
      <biological_entity id="o13561" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="20-50-flowered" value_original="20-50-flowered" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13562" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13563" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels jointed, 5–12 mm, pubescent and glandular;</text>
      <biological_entity id="o13564" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts conspicuous, proximal ones ovate, lobed, distal ones narrowly oblong, unlobed, (3–) 4–5 mm, sparsely glandular and pubescent.</text>
      <biological_entity id="o13565" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium green, saucer-shaped, widely flared, 0.5–1.5 mm, ± pubescent and sparsely glandular abaxially, glabrous adaxially;</text>
      <biological_entity id="o13566" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13567" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="saucer--shaped" value_original="saucer--shaped" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s9" value="flared" value_original="flared" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals nearly to somewhat overlapping, spreading, brownish purple to greenish or sometimes nearly white, ovatelanceolate to oblong-lanceolate, 3–5 mm;</text>
      <biological_entity id="o13568" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13569" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly to somewhat" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="brownish purple" name="coloration" src="d0_s10" to="greenish" />
        <character is_modifier="false" modifier="sometimes nearly" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s10" to="oblong-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals widely separated, erect, white, cuneate-flabelliform, not conspicuously revolute or inrolled, 1 mm;</text>
      <biological_entity id="o13570" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13571" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s11" value="separated" value_original="separated" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cuneate-flabelliform" value_original="cuneate-flabelliform" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc wine red, thick, lobed, covering and submerging ovary;</text>
      <biological_entity id="o13572" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o13573" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="wine red" value_original="wine red" />
        <character is_modifier="false" name="width" src="d0_s12" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="position_relational" src="d0_s12" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o13574" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <relation from="o13573" id="r1149" name="submerging" negation="false" src="d0_s12" to="o13574" />
    </statement>
    <statement id="d0_s13">
      <text>stamens slightly longer than petals;</text>
      <biological_entity id="o13575" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o13576" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="than petals" constraintid="o13577" is_modifier="false" name="length_or_size" src="d0_s13" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o13577" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear to slightly broader at base, 1 mm, glabrous;</text>
      <biological_entity id="o13578" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o13579" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character constraint="at base" constraintid="o13580" is_modifier="false" modifier="slightly" name="width" src="d0_s14" value="broader" value_original="broader" />
        <character name="some_measurement" notes="" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13580" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers white, transversely oblong-cordate, 0.5 mm, broader than long, apex shallowly notched;</text>
      <biological_entity id="o13581" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o13582" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s15" value="oblong-cordate" value_original="oblong-cordate" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="width" src="d0_s15" value="broader than long" value_original="broader than long" />
      </biological_entity>
      <biological_entity id="o13583" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s15" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary somewhat hairy and densely sessile-glandular;</text>
      <biological_entity id="o13584" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o13585" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s16" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate 1/4–1/2 their lengths, 1 mm, glabrous or hairy.</text>
      <biological_entity id="o13586" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o13587" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="lengths" src="d0_s17" to="1/2" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries bland, black, subglobose, 8–10 mm, glandular.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 16.</text>
      <biological_entity id="o13588" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="taste" src="d0_s18" value="bland" value_original="bland" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s18" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13589" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, moist woods, floodplains, shorelines, thickets, avalanche tracks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="shorelines" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="avalanche tracks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Stink currant</other_name>
  <discussion>Ribes bracteosum occurs along the Pacific Coast from southeastern Alaska to northern California. Its thin leaves have a sweetish, disagreeable odor and the conspicuous bracts bear acicular, mostly persistent processes near the base along the slightly winged, stipular margins.</discussion>
  
</bio:treatment>