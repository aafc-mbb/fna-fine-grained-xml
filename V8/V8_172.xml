<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">heuchera</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="species">micrantha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">micrantha</taxon_name>
    <taxon_hierarchy>family saxifragaceae;genus heuchera;species micrantha;variety micrantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065941</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole glabrous or long-stipitate-glandular;</text>
      <biological_entity id="o27807" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o27808" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade orbiculate to pentagonal, shallowly lobed.</text>
      <biological_entity id="o27809" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o27810" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s1" to="pentagonal" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences glabrous or sparsely short to medium stipitate-glandular.</text>
      <biological_entity id="o27811" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="size" src="d0_s2" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="size" src="d0_s2" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: hypanthium obconic, 1.4–4.5 × 1–3.7 mm, short to long-stipitate-glandular;</text>
      <biological_entity id="o27812" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o27813" name="hypanthium" name_original="hypanthium" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3.7" to_unit="mm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 0.6–1.2 mm, apex rounded to acute;</text>
      <biological_entity id="o27814" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o27815" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27816" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 0.2–0.7 mm wide;</text>
      <biological_entity id="o27817" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o27818" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>styles 1.6–3.1 mm. 2n = 28.</text>
      <biological_entity id="o27819" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o27820" name="style" name_original="styles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s6" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27821" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Evergreen coniferous forests on basalt, schistose shale, metavolcanic, ultra basic, or serpentine-derived soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="evergreen" />
        <character name="habitat" value="forests" modifier="coniferous" constraint="on basalt , schistose shale , metavolcanic , ultra basic , or serpentine-derived soils" />
        <character name="habitat" value="basalt" modifier="on" />
        <character name="habitat" value="schistose shale" />
        <character name="habitat" value="metavolcanic" />
        <character name="habitat" value="basic" modifier="ultra" />
        <character name="habitat" value="serpentine-derived soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <discussion>Variety micrantha occurs in the Coast Range, Cascade Range, Blue and Klamath mountains, northern Sierra Nevada, and the Columbia River gorge, and on wooded banks of the Columbia and its tributaries.</discussion>
  
</bio:treatment>