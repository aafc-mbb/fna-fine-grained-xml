<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">57</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">micranthes</taxon_name>
    <taxon_name authority="(D. Don) Small in N. L. Britton et al." date="1905" rank="species">spicata</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al.,  N. Amer. Fl.</publication_title>
      <place_in_publication>22: 146. 1905,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus micranthes;species spicata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065879</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="species">spicata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>13: 354. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species spicata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or tufted, short, thick-rhizomatous.</text>
      <biological_entity id="o25189" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="thick-rhizomatous" value_original="thick-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o25190" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole rounded, 4–15 cm;</text>
      <biological_entity id="o25191" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade round, ovate, or reniform, 3–15 cm, thin, base cordate, margins serrate to dentate, ciliate, surfaces hairy to glabrate.</text>
      <biological_entity id="o25192" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o25193" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o25194" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrate to dentate" value_original="serrate to dentate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o25195" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s3" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 15+-flowered, congested, spikelike thyrses, 15–50 (–70) cm, tangled, pink to purple-tipped stipitate-glandular.</text>
      <biological_entity id="o25196" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="15+-flowered" value_original="15+-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o25197" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="70" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s4" to="50" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="tangled" value_original="tangled" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals reflexed, oblong to ovate;</text>
      <biological_entity id="o25198" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25199" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals cream to yellowish, not spotted, oblong, not clawed, 3–4.5 mm, longer than sepals;</text>
      <biological_entity id="o25200" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25201" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s6" to="yellowish" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s6" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o25202" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o25202" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>filaments narrowly club-shaped;</text>
      <biological_entity id="o25203" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o25204" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistils connate at least 1/2 their lengths;</text>
      <biological_entity id="o25205" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25206" name="pistil" name_original="pistils" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="at least" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary superior, (to 1/3 adnate to hypanthium).</text>
      <biological_entity id="o25207" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25208" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules green, purplish tinged, ± folliclelike, (basally connate).</text>
      <biological_entity id="o25209" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish tinged" value_original="purplish tinged" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="folliclelike" value_original="folliclelike" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet tundra, heaths, moist, rocky areas, streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet tundra" />
        <character name="habitat" value="heaths" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="rocky areas" />
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  
</bio:treatment>