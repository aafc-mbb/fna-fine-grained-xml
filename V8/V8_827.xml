<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="treatment_page">432</other_info_on_meta>
    <other_info_on_meta type="illustration_page">428</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">arctostaphylos</taxon_name>
    <taxon_name authority="(J. E. Adams) J. E. Adams" date="1940" rank="species">pajaroensis</taxon_name>
    <place_of_publication>
      <publication_title>J. Elisha Mitchell Sci. Soc.</publication_title>
      <place_in_publication>56: 41. 1940  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus arctostaphylos;species pajaroensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092373</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctostaphylos</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">andersonii</taxon_name>
    <taxon_name authority="J. E. Adams" date="unknown" rank="variety">pajaroensis</taxon_name>
    <place_of_publication>
      <publication_title>in H. McMinn, Man. Calif. Shrubs,</publication_title>
      <place_in_publication>418, fig. 492. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arctostaphylos;species andersonii;variety pajaroensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect or mound-forming, 1–4 m;</text>
      <biological_entity id="o2869" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mound-forming" value_original="mound-forming" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>burl absent;</text>
      <biological_entity id="o2870" name="burl" name_original="burl" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark on older stems persistent, gray, shredded;</text>
      <biological_entity id="o2871" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o2872" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o2871" id="r234" name="on" negation="false" src="d0_s2" to="o2872" />
    </statement>
    <statement id="d0_s3">
      <text>twigs short-hairy, sometimes with long hairs.</text>
      <biological_entity id="o2873" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity id="o2874" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
      </biological_entity>
      <relation from="o2873" id="r235" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o2874" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves bifacial in stomatal distribution;</text>
      <biological_entity id="o2875" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="in stomatal" constraintid="o2876" is_modifier="false" name="architecture" src="d0_s4" value="bifacial" value_original="bifacial" />
      </biological_entity>
      <biological_entity id="o2876" name="stomatal" name_original="stomatal" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole to 2 mm;</text>
      <biological_entity id="o2877" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade light green abaxially, dark blue-green, glaucous adaxially, dull, ovate to triangular-ovate, 2–4 × 1–2 cm, base distinctly lobed, auriculate-clasping, margins (tinged with red), entire, ± cupped, surfaces smooth, glabrous, midvein hairy abaxially.</text>
      <biological_entity id="o2878" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark blue-green" value_original="dark blue-green" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="triangular-ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2879" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o2880" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2881" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2882" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences panicles, 2–5-branched;</text>
      <biological_entity constraint="inflorescences" id="o2883" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-5-branched" value_original="2-5-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>immature inflorescence pendent, branches spreading, axis 1–1.5 cm, 1+ mm diam., short-hairy, sometimes with long hairs;</text>
      <biological_entity id="o2884" name="inflorescence" name_original="inflorescence" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="immature" value_original="immature" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o2885" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o2886" name="axis" name_original="axis" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s8" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity id="o2887" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
      </biological_entity>
      <relation from="o2886" id="r236" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o2887" />
    </statement>
    <statement id="d0_s9">
      <text>bracts not appressed, leaflike, linear-lanceolate, 5–10 mm, apex acuminate, surfaces glabrous.</text>
      <biological_entity id="o2888" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2889" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o2890" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 5–8 mm, finely hairy or glabrous.</text>
      <biological_entity id="o2891" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: corolla white to pink, urceolate;</text>
      <biological_entity id="o2892" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2893" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary densely white-hairy.</text>
      <biological_entity id="o2894" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2895" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits depressed-globose, 6–8 mm diam., sparsely hairy or glabrous.</text>
      <biological_entity id="o2896" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Stones distinct.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 26.</text>
      <biological_entity id="o2897" name="stone" name_original="stones" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2898" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early winter–early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Maritime chaparral on deep to shallow, sandy soils or sandstone outcroppings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" modifier="maritime" constraint="on deep to shallow , sandy soils or sandstone outcroppings" />
        <character name="habitat" value="shallow" modifier="on" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="deep to shallow" />
        <character name="habitat" value="sandstone outcroppings" />
        <character name="habitat" value="maritime" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Pajaro manzanita</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Arctostaphylos pajaroensis is found in the vicinity of the Pajaro Hills in Monterey County.</discussion>
  
</bio:treatment>