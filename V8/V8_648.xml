<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="illustration_page">323</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">theaceae</taxon_name>
    <taxon_name authority="W. Bartram ex Marshall" date="1785" rank="genus">franklinia</taxon_name>
    <taxon_name authority="Marshall" date="1785" rank="species">alatamaha</taxon_name>
    <place_of_publication>
      <publication_title>Arbust. Amer.,</publication_title>
      <place_in_publication>49. 1785  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family theaceae;genus franklinia;species alatamaha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220005345</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gordonia</taxon_name>
    <taxon_name authority="(Marshall) Sargent" date="unknown" rank="species">alatamaha</taxon_name>
    <taxon_hierarchy>genus Gordonia;species alatamaha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees: crown rounded;</text>
      <biological_entity id="o20465" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o20466" name="crown" name_original="crown" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>twigs reddish, smooth.</text>
      <biological_entity id="o20467" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o20468" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 12–18 (–25) × 5–7 (–9.5) cm, abaxial surface hairy, hairs simple, bifurcate, and stellate, midrib depressed distally.</text>
      <biological_entity id="o20469" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s2" to="18" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20470" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o20471" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o20472" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="depressed" value_original="depressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 1–2 mm diam. at narrowest point.</text>
      <biological_entity id="o20473" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at narrowest point" constraintid="o20474" from="1" from_unit="mm" name="diameter" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="narrowest" id="o20474" name="point" name_original="point" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals suborbiculate, 7–9 × 8–9 mm, base constricted;</text>
      <biological_entity id="o20475" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o20476" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20477" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="constricted" value_original="constricted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals to 6 × 4 cm, apex rounded;</text>
      <biological_entity id="o20478" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o20479" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20480" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments: free portion 8–14 mm.</text>
      <biological_entity id="o20481" name="filament" name_original="filaments" src="d0_s6" type="structure" />
      <biological_entity id="o20482" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules 1.4–1.5 × 1.5–1.7 cm.</text>
      <biological_entity id="o20483" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s7" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 4–7 × 3.5–4.5 mm. 2n = 36.</text>
      <biological_entity id="o20484" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20485" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun-)Aug–Sep(-Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low, wet soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soils" modifier="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Franklin tree</other_name>
  <discussion>Franklinia alatamaha is believed to be extinct in the wild. It was known from only one location near historic Fort Barrington, along the Altamaha River in Georgia, where it was last collected in 1803. The species was known from relatively few individuals and it has been suggested that its demise was due to over-collection to meet horticultural demands in Europe during the late 1700s. It is an ornamental of limited cultivation; it is found in arboreta and botanic gardens as far north and east as Massachusetts, west to Missouri, and south to Florida, and in warm-temperate and temperate regions in Europe.</discussion>
  
</bio:treatment>