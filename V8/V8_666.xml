<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="treatment_page">337</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">diapensiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">DIAPENSIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 141. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 69. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family diapensiaceae;genus DIAPENSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek dia-, composed, and pente, five, alluding to sepal, petal, and stamen numbers</other_info_on_name>
    <other_info_on_name type="fna_id">109798</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, forming tussocks, cushions, domes, or mats, caulescent, taprooted.</text>
      <biological_entity id="o12298" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o12299" name="tussock" name_original="tussocks" src="d0_s0" type="structure" />
      <biological_entity id="o12300" name="cushion" name_original="cushions" src="d0_s0" type="structure" />
      <biological_entity id="o12301" name="dome" name_original="domes" src="d0_s0" type="structure" />
      <biological_entity id="o12302" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o12298" id="r1038" name="forming tussocks , cushions , domes , or mats" negation="false" src="d0_s0" to="o12299" />
      <relation from="o12298" id="r1039" name="forming tussocks , cushions , domes , or mats" negation="false" src="d0_s0" to="o12300" />
      <relation from="o12298" id="r1040" name="forming tussocks , cushions , domes , or mats" negation="false" src="d0_s0" to="o12301" />
      <relation from="o12298" id="r1041" name="forming tussocks , cushions , domes , or mats" negation="false" src="d0_s0" to="o12302" />
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly prostrate to decumbent, branched;</text>
      <biological_entity id="o12303" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="mostly prostrate" name="growth_form_or_orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches procumbent or decumbent to erect.</text>
      <biological_entity id="o12304" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character name="growth_form" src="d0_s2" value="decumbent to erect" value_original="decumbent to erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, mostly opposite or compactly whorled, densely imbricate or crowded, 3–15 mm;</text>
      <biological_entity id="o12305" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="compactly" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="compactly" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present or absent;</text>
      <biological_entity id="o12306" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong-lanceolate to narrowly spatulate, obovate, or spatulate-elliptic, slightly falcate, margins entire, apex acute or obtuse, surfaces glabrous, appearing 1-veined, sometimes with 1–2, indistinct, lateral-veins.</text>
      <biological_entity id="o12307" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s5" to="narrowly spatulate obovate or spatulate-elliptic" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s5" to="narrowly spatulate obovate or spatulate-elliptic" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s5" to="narrowly spatulate obovate or spatulate-elliptic" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity id="o12308" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12309" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o12310" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12311" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="1" is_modifier="true" modifier="sometimes" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="true" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <relation from="o12310" id="r1042" name="appearing" negation="false" src="d0_s5" to="o12311" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary flowers, pedicellate to subsessile, immediately subtended by 2–3 bracts.</text>
      <biological_entity id="o12312" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="pedicellate" name="architecture" notes="" src="d0_s6" to="subsessile" />
      </biological_entity>
      <biological_entity id="o12313" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o12314" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <relation from="o12312" id="r1043" modifier="immediately" name="subtended by" negation="false" src="d0_s6" to="o12314" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ebracteate, elongating after anthesis.</text>
      <biological_entity id="o12315" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character constraint="after anthesis" is_modifier="false" name="length" src="d0_s7" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals distinct or connate proximally;</text>
      <biological_entity id="o12316" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12317" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals connate in proximal 1/2, corolla cupulate to campanulate, 7–10 mm, lobes white to cream or pinkish tinged to rose, margins entire;</text>
      <biological_entity id="o12318" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12319" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="in proximal 1/2" constraintid="o12320" is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12320" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
      <biological_entity id="o12321" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="cupulate" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12322" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="cream or pinkish tinged" />
      </biological_entity>
      <biological_entity id="o12323" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 2-locular, without basal spurs, longitudinally dehiscent;</text>
      <biological_entity id="o12324" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12325" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" notes="" src="d0_s10" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12326" name="spur" name_original="spurs" src="d0_s10" type="structure" />
      <relation from="o12325" id="r1044" name="without" negation="false" src="d0_s10" to="o12326" />
    </statement>
    <statement id="d0_s11">
      <text>filaments adnate to corolla-tube;</text>
      <biological_entity id="o12327" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12328" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character constraint="to corolla-tube" constraintid="o12329" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o12329" name="corolla-tube" name_original="corolla-tube" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent or vestigial.</text>
      <biological_entity id="o12330" name="flower" name_original="flowers" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>x = 6.</text>
      <biological_entity id="o12331" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <biological_entity constraint="x" id="o12332" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Pincushion plant</other_name>
  <discussion>Species 5 (2 in the flora).</discussion>
  <discussion>The three temperate species of Diapensia are endemic to the Sino-Himalayan Mountains. They differ as a group from the two boreal species by their production of vestigial staminodes (see F. Ludlow 1976).</discussion>
  <references>
    <reference>Evans, W. E. 1927. A revision of the genus Diapensia with special reference to the Sino-Himalayan species. Notes Roy. Bot. Gard. Edinburgh 15: 209–236.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants forming tussocks; branches not adventitiously rooted; leaf blades oblong-oblanceolate to narrowly spatulate, 7-15 × 1.3-2.3 mm, margins with narrow hyaline flange proximally.</description>
      <determination>1 Diapensia lapponica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants usually forming mats or low mounds or cushions; branches adventitiously rooted; leaf blades obovate to spatulate-elliptic, 3-8(-12) × 1.5-2.2(-3) mm, margins sometimes with narrow hyaline flange.</description>
      <determination>2 Diapensia obovata</determination>
    </key_statement>
  </key>
</bio:treatment>