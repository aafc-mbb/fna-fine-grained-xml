<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">499</other_info_on_meta>
    <other_info_on_meta type="illustration_page">500</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="D. Don ex G. Don" date="1834" rank="genus">agarista</taxon_name>
    <taxon_name authority="(Lamarck) Judd" date="1979" rank="species">populifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>60: 495. 1979  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus agarista;species populifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065686</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andromeda</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">populifolia</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>1: 159. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Andromeda;species populifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucothoë</taxon_name>
    <taxon_name authority="(Aiton) G. Don" date="unknown" rank="species">acuminata</taxon_name>
    <taxon_hierarchy>genus Leucothoë;species acuminata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucothoë</taxon_name>
    <taxon_name authority="(Lamarck) Dipple" date="unknown" rank="species">populifolia</taxon_name>
    <taxon_hierarchy>genus Leucothoë;species populifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 7 m.</text>
      <biological_entity id="o5724" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems terete, with chambered pith.</text>
      <biological_entity id="o5725" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o5726" name="pith" name_original="pith" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="chambered" value_original="chambered" />
      </biological_entity>
      <relation from="o5725" id="r481" name="with" negation="false" src="d0_s1" to="o5726" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 2.6–9 (–11.2) × 0.9–4 (–5) cm, base narrowly cuneate to rounded, apex acuminate, surfaces sometimes stipitate-glandular-hairy, hairy on midvein adaxially.</text>
      <biological_entity id="o5727" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="11.2" to_unit="cm" />
        <character char_type="range_value" from="2.6" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5728" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly cuneate" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o5729" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o5730" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character constraint="on midvein" constraintid="o5731" is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5731" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: calyx lobes 0.9–2 × 0.6–1.5 mm;</text>
      <biological_entity id="o5732" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="calyx" id="o5733" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla 6–9.5 × 3–5 mm, glabrous;</text>
      <biological_entity id="o5734" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5735" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments 4–5.5 mm;</text>
      <biological_entity id="o5736" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5737" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>style impressed into ovary apex.</text>
      <biological_entity id="o5738" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5739" name="style" name_original="style" src="d0_s6" type="structure">
        <character constraint="into ovary apex" constraintid="o5740" is_modifier="false" name="prominence" src="d0_s6" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o5740" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules 3–4 × 4.5–6.5 mm, placentae subapical.</text>
      <biological_entity id="o5741" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5742" name="placenta" name_original="placentae" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="subapical" value_original="subapical" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds brown, 1.4–2 mm. 2n = 24.</text>
      <biological_entity id="o5743" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5744" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid swamps to moist broadleaved forests, especially along streams, in ravines, or near springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid" />
        <character name="habitat" value="swamps to moist broadleaved forests" />
        <character name="habitat" value="streams" modifier="especially along" />
        <character name="habitat" value="ravines" modifier="in" />
        <character name="habitat" value="near springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Pipe-plant</other_name>
  <other_name type="common_name">pipe-stem wood</other_name>
  <discussion>Agarista populifolia is occasionally used as an ornamental; it is the northernmost species of the genus, and is closely related to A. sleumeri Judd of Mexico. Agarista populifolia is rather variable in glandular indumentum and in the size and margins of the leaves. The plants are apparently very rare in Georgia, and the two known collections from that state lack exact localities; W. Bartram (1791) reported it growing along Saint Mary’s River.</discussion>
  
</bio:treatment>