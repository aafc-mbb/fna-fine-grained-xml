<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="Rafinesque" date="1840" rank="genus">ORTHILIA</taxon_name>
    <place_of_publication>
      <publication_title>Autik. Bot.,</publication_title>
      <place_in_publication>103. 1840  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus orthilia;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek orthos, straight, and ilium, side or flank, possibly alluding to secund inflorescence</other_info_on_name>
    <other_info_on_name type="fna_id">123245</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, chlorophyllous, autotrophic.</text>
      <biological_entity id="o20736" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="chlorophyllous" value_original="chlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="autotrophic" value_original="autotrophic" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="chlorophyllous" value_original="chlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="autotrophic" value_original="autotrophic" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent or erect, glabrous or papillose, especially distally.</text>
      <biological_entity id="o20738" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s1" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, alternate or pseudoverticillate in (1–) 2–4 whorls;</text>
      <biological_entity id="o20739" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character constraint="in whorls" constraintid="o20740" is_modifier="false" name="arrangement" src="d0_s2" value="pseudoverticillate" value_original="pseudoverticillate" />
      </biological_entity>
      <biological_entity id="o20740" name="whorl" name_original="whorls" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s2" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o20741" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade not maculate, ovate to broadly ovate or elliptic to orbiculate, subcoriaceous, margins entire or undulate to serrulate, rarely serrate, plane or, rarely, obscurely revolute, surfaces glabrous.</text>
      <biological_entity id="o20742" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="maculate" value_original="maculate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly ovate or elliptic" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o20743" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="to " constraintid="o20744" is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o20744" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="true" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="true" modifier="obscurely" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o20745" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="true" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="true" modifier="obscurely" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o20744" id="r1712" name="to" negation="false" src="d0_s4" to="o20745" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, often lax in bud or flower, usually erect in fruit, (secund);</text>
      <biological_entity constraint="inflorescences" id="o20746" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character constraint="in flower" constraintid="o20748" is_modifier="false" modifier="often" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character constraint="in fruit" constraintid="o20749" is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o20747" name="bud" name_original="bud" src="d0_s5" type="structure" />
      <biological_entity id="o20748" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <biological_entity id="o20749" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>peduncular bracts usually present;</text>
      <biological_entity constraint="peduncular" id="o20750" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inflorescence bracts free from pedicels.</text>
      <biological_entity constraint="inflorescence" id="o20751" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character constraint="from pedicels" constraintid="o20752" is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o20752" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels recurved to reflexed in fruit;</text>
      <biological_entity id="o20753" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o20754" from="recurved" name="orientation" src="d0_s8" to="reflexed" />
      </biological_entity>
      <biological_entity id="o20754" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o20755" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers radially symmetric, spreading or nodding;</text>
      <biological_entity id="o20756" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, connate proximally, often obscurely so, calyx lobes broadly triangular to ovate;</text>
      <biological_entity id="o20757" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o20758" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly triangular" modifier="often obscurely; obscurely" name="shape" src="d0_s11" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, distinct, greenish white, yellowish white, or white, with 2 inconspicuous basal tubercles, corolla suburceolate;</text>
      <biological_entity id="o20759" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20760" name="tubercle" name_original="tubercles" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s12" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o20761" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="suburceolate" value_original="suburceolate" />
      </biological_entity>
      <relation from="o20759" id="r1713" name="with" negation="false" src="d0_s12" to="o20760" />
    </statement>
    <statement id="d0_s13">
      <text>intrastaminal nectary disc absent;</text>
      <biological_entity constraint="nectary" id="o20762" name="disc" name_original="disc" src="d0_s13" type="structure" constraint_original="intrastaminal nectary">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 10, included or barely exserted;</text>
      <biological_entity id="o20763" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments broad proximally, gradually narrowed medially, slender distally, glabrous;</text>
      <biological_entity id="o20764" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="width" src="d0_s15" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="gradually; medially" name="shape" src="d0_s15" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s15" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers oblong, without awns, without tubules, dehiscent by 2 round to elliptic pores;</text>
      <biological_entity id="o20765" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character constraint="by pores" constraintid="o20768" is_modifier="false" name="dehiscence" notes="" src="d0_s16" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o20766" name="awn" name_original="awns" src="d0_s16" type="structure" />
      <biological_entity id="o20767" name="tubule" name_original="tubules" src="d0_s16" type="structure" />
      <biological_entity id="o20768" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character char_type="range_value" from="round" is_modifier="true" name="shape" src="d0_s16" to="elliptic" />
      </biological_entity>
      <relation from="o20765" id="r1714" name="without" negation="false" src="d0_s16" to="o20766" />
      <relation from="o20765" id="r1715" name="without" negation="false" src="d0_s16" to="o20767" />
    </statement>
    <statement id="d0_s17">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o20769" name="pistil" name_original="pistil" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary imperfectly 5-locular;</text>
    </statement>
    <statement id="d0_s19">
      <text>placentation intruded-parietal;</text>
      <biological_entity id="o20770" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s18" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="placentation" src="d0_s19" value="intruded-parietal" value_original="intruded-parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>style (exserted), straight or nearly so, expanded distally or not;</text>
      <biological_entity id="o20771" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
        <character name="course" src="d0_s20" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="distally; distally; not" name="size" src="d0_s20" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma 5-ridged, without subtending ring of hairs.</text>
      <biological_entity id="o20772" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="5-ridged" value_original="5-ridged" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o20773" name="ring" name_original="ring" src="d0_s21" type="structure" />
      <biological_entity id="o20774" name="hair" name_original="hairs" src="d0_s21" type="structure" />
      <relation from="o20772" id="r1716" name="without" negation="false" src="d0_s21" to="o20773" />
      <relation from="o20773" id="r1717" name="part_of" negation="false" src="d0_s21" to="o20774" />
    </statement>
    <statement id="d0_s22">
      <text>Fruits capsular, pendulous, dehiscence loculicidal, cobwebby tissue exposed by splitting valves at dehiscence.</text>
      <biological_entity id="o20775" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s22" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="dehiscence" src="d0_s22" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
      <biological_entity id="o20776" name="tissue" name_original="tissue" src="d0_s22" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s22" value="cobwebby" value_original="cobwebby" />
        <character constraint="by valves" constraintid="o20777" is_modifier="false" name="prominence" src="d0_s22" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o20777" name="valve" name_original="valves" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture_or_dehiscence" src="d0_s22" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds ca. 1000, fusiform, winged.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 19.</text>
      <biological_entity id="o20778" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="1000" value_original="1000" />
        <character is_modifier="false" name="shape" src="d0_s23" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o20779" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Guatemala), Eurasia; circumboreal.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="circumboreal" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Side-bells or one-sided wintergreen</other_name>
  <other_name type="common_name">pyrole unilatérale</other_name>
  <discussion>Species 1: North America, Mexico, Central America (Guatemala), Eurasia; circumboreal.</discussion>
  <discussion>The basal tubercles on the petals of Orthilia are unique among Monotropoideae. On fresh or rehydrated flowers they are about 0.2–0.3 mm in diameter and 0.1–0.2 mm tall. The tubercles appear as obscure thickenings on dried specimens. H. F. Copeland (1947) found no evidence of glandular activity associated with these tubercles and speculated that they may influence the opening of the flowers, as do the lodicules of grasses.</discussion>
  <discussion>Species 1</discussion>
  
</bio:treatment>