<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="(Britton &amp; Rose) Moran" date="1942" rank="subgenus">Stylophyllum</taxon_name>
    <taxon_name authority="(Rose) Moran" date="1943" rank="species">virens</taxon_name>
    <taxon_name authority="(Rose) Moran" date="1995" rank="subspecies">insularis</taxon_name>
    <place_of_publication>
      <publication_title>Haseltonia</publication_title>
      <place_in_publication>3: 2. 1995  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus stylophyllum;species virens;subspecies insularis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092052</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stylophyllum</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">insulare</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and J. N. Rose, New N. Amer. Crassul.,</publication_title>
      <place_in_publication>34. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stylophyllum;species insulare;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="(Rose) P. H. Thomson" date="unknown" rank="species">insularis</taxon_name>
    <taxon_hierarchy>genus Dudleya;species insularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices to 10 dm × 2–6 (–8) cm.</text>
      <biological_entity id="o4220" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s0" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rosette leaf-blades whitish or sometimes green, triangular-lanceolate, 6–25 × 1–3 cm, surfaces usually farinose.</text>
      <biological_entity constraint="rosette" id="o4221" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s1" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4222" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: floral shoots 2–7 dm × 5–15 mm;</text>
      <biological_entity id="o4223" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="floral" id="o4224" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s2" to="7" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cyme often ± cylindric;</text>
      <biological_entity id="o4225" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o4226" name="cyme" name_original="cyme" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often more or less" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches 1–3 times bifurcate;</text>
      <biological_entity id="o4227" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o4228" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="1-3 times bifurcate" value_original="1-3 times bifurcate " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cincinni mostly 3–8-flowered, mostly 1–5 cm.</text>
      <biological_entity id="o4229" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o4230" name="cincinnus" name_original="cincinni" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s5" value="3-8-flowered" value_original="3-8-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 14–23 mm diam. 2n = 34.</text>
      <biological_entity id="o4231" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="diameter" src="d0_s6" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4232" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and rocky slopes near sea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="sea" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Island green dudleya</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies insularis grows only on Santa Catalina and San Nicolas islands and on the mainland on sea bluffs at the south base of San Pedro Hill—a former island now connected by the alluvial Los Angeles Plain but related to Santa Catalina and San Clemente islands directly to the south (W. S. T. Smith 1900).</discussion>
  
</bio:treatment>