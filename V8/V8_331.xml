<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="mention_page">166</other_info_on_meta>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rhodiola</taxon_name>
    <taxon_name authority="Rafinesque" date="1832" rank="species">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Atlantic J.</publication_title>
      <place_in_publication>1: 146. 1832  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus rhodiola;species integrifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092043</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhodiola</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rosea</taxon_name>
    <taxon_name authority="(Rafinesque) H. Hara" date="unknown" rank="subspecies">integrifolia</taxon_name>
    <taxon_hierarchy>genus Rhodiola;species rosea;subspecies integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhodiola</taxon_name>
    <taxon_name authority="(Rafinesque) A. Nelson" date="unknown" rank="species">rosea</taxon_name>
    <taxon_name authority="(Rafinesque) Jepson" date="unknown" rank="variety">integrifolia</taxon_name>
    <taxon_hierarchy>genus Rhodiola;species rosea;variety integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="(Linnaeus) Scopoli" date="unknown" rank="species">integrifolium</taxon_name>
    <taxon_hierarchy>genus Sedum;species integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="(Rafinesque) Á. Löve &amp; D. Löve" date="unknown" rank="species">rosea</taxon_name>
    <taxon_name authority="(Rafinesque) A. Berger" date="unknown" rank="variety">integrifolium</taxon_name>
    <taxon_hierarchy>genus Sedum;species rosea;variety integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tolmachevia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">integrifolia</taxon_name>
    <taxon_hierarchy>genus Tolmachevia;species integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dioecious or polygamodioecious.</text>
      <biological_entity id="o23422" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rootstock erect or spreading, to 1–5 cm diam.</text>
      <biological_entity id="o23423" name="rootstock" name_original="rootstock" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Floral stems deciduous, 3–50 × 0.1–0.7 cm.</text>
      <biological_entity constraint="floral" id="o23424" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s2" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades usually bright green, sometimes glaucous, elliptic to ovate or oblanceolate to linear, 0.5–5.5 × 0.2–1.5 (–2) cm, margins entire or toothed, apex acute to obtuse.</text>
      <biological_entity id="o23425" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="bright green" value_original="bright green" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate or oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23426" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o23427" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences corymbose cymes, dense, to 250-flowered, to 8 cm diam.</text>
      <biological_entity id="o23428" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="density" notes="" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="250-flowered" value_original="250-flowered" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23429" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels ca. 2 mm.</text>
      <biological_entity id="o23430" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers mostly unisexual, 4–5-merous;</text>
      <biological_entity id="o23431" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-5-merous" value_original="4-5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals lanceolate to ovate, 1.5–3 mm;</text>
      <biological_entity id="o23432" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals mostly dark red, sometimes yellowish at base or yellow with apical 1/3 red, elliptic-oblong, 1.5–5 mm, shorter than stamens, in staminate flowers spreading, hooded, 1.3–1.7 mm wide, in pistillate erect.</text>
      <biological_entity id="o23433" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark red" value_original="dark red" />
        <character constraint="at " constraintid="o23435" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character constraint="than stamens" constraintid="o23436" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="hooded" value_original="hooded" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23434" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o23435" name="red" name_original="red" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="apical" value_original="apical" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o23436" name="stamen" name_original="stamens" src="d0_s8" type="structure" />
      <biological_entity id="o23437" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o23433" id="r1949" name="in" negation="false" src="d0_s8" to="o23437" />
    </statement>
    <statement id="d0_s9">
      <text>Follicles 4–9 mm, beaks spreading.</text>
      <biological_entity id="o23438" name="follicle" name_original="follicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23439" name="beak" name_original="beaks" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds winged, pyriform or oblanceoloid, 1.1–2.8 mm. 2n = 36.</text>
      <biological_entity id="o23440" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23441" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Calif., Colo., Idaho, Minn., Mont., N.Mex., N.Y., Nev., Oreg., Utah, Wash., Wyo.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Western roseroot</other_name>
  <other_name type="common_name">king’s crown</other_name>
  <discussion>Subspecies 3+ (3 in the flora).</discussion>
  <discussion>The plants treated here as Rhodiola integrifolia and R. rosea are part of a difficult polymorphic complex of arctic to cool-temperate North America and Eurasia and of high mountains southward. Some authors have included them all in R. rosea [or Sedum rosea (Linnaeus) Scopoli], often with subspecies or varieties; N. L. Britton and J. N. Rose (1905) earlier divided them into two to several species.</discussion>
  <discussion>For this complex C. H. Uhl (1952) cited six published chromosome counts from Greenland through Eurasia to Japan, all n = 11 or 2n = 22; he found the same numbers in seven collections from northeastern North America (all these Rhodiola rosea proper). From Eurasia, according to R. L. Taylor and G. A. Mulligan (1968), races with 2n = 16 and 33 also are known. On the other hand, for endemics in Minnesota and New York and for five plants from New Mexico and California, Uhl found n = 18 or 2n = 36, and Taylor and Mulligan likewise found 2n = 36 in plants of Moresby Island, British Columbia. With the support of five more counts, but with none for the large area of Oregon and Wyoming to the Bering Sea, R. T. Clausen (1975) separated the 36-chromosome plants as Sedum integrifolium. More counts of 2n = 36 have since appeared, including one from Sutwick Island, off the Alaska Peninsula (Á. Löve 1979).</discussion>
  <discussion>In middle North America, Rhodiola integrifolia and R. rosea are geographically distinct. The local endemic subsp. leedyi of the former grows in Minnesota, midway between the western subspecies of R. integrifolia and the eastern R. rosea, and grows in New York state within 100 km of R. rosea. Otherwise, the ranges of the two species are over 2000 km apart in the south and nearly 3000 km in the north. Rhodiola integrifolia also is the prevailing plant in eastern Asia, where it has been named Sedum atropurpureum N. S. Turczaninow (E. Hultén 1941–1950, vol. 5), and R. rosea seems to extend (although not verified by chromosome counts) from eastern Asia to far-western Alaska, on the coast of the Bering Sea.</discussion>
  <discussion>Although saying that Sedum integrifolium differs from S. rosea in many ways besides the chromosome number, R. T. Clausen (1975) found few absolute distinctions. His best key characters were those used here, petal width of staminate flowers, largely supported by flower color. Although questions remain unanswered, it seems best for now to follow Clausen in keeping the two species for North America.</discussion>
  <discussion>Over its broad range, Rhodiola integrifolia is quite variable (e.g., see E. Hultén 1941–1950, vol. 5). R. T. Clausen (1975) noted that in some populations pistillate plants outnumber staminate; in others staminate may be six times as many as pistillate. He distinguished two outlying endemics as subspp. leedyi and neomexicana, also kept as subspecies here. He also proposed subsp. procer[a]  for tall robust plants of Colorado, New Mexico, and (less typical) California, all within the range of subsp. integrifolia and all with the same chromosome number. Some of his plants look remarkably different from the usual dwarf forms of subsp. integrifolia that grow at the same high elevations. He did not include in subsp. procera (and apparently did not see alive) the tall plants often found inland in Alaska and northwestern Canada, which would be Sedum frigidum Rydberg according to Hultén. Thus the racial situation is much more complex than the naming of only two peripheral subspecies might suggest.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals mostly dark red throughout; stems 3-15(-50) cm; leaf blades ovate to elliptic or oblanceolate, 0.5-3(-5) × 0.2-1.5(-2) cm; seeds 1.4-2 mm; Alaska to Rocky Mountains.</description>
      <determination>2a Rhodiola integrifolia subsp. integrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals (red or) dark red or mostly yellow at base, yellow or red at apex; stems 15-45 cm; leaf blades linear-oblanceolate to narrowly oblanceolate, 2-5.5 × 0.3-1.5 cm; seeds 1.1-2.8 mm; Minnesota, s New Mexico, New York</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades green, linear-oblanceolate, 0.3-0.7 cm wide; petals yellow with red tips and keel; seeds 1.1-1.9 mm; s New Mexico.</description>
      <determination>2b Rhodiola integrifolia subsp. neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades blue-green, narrowly oblanceolate, 0.5-1.5 cm wide; petals dark red throughout or usually greenish yellow at base; seeds 2-2.8 mm; Minnesota, New York.</description>
      <determination>2c Rhodiola integrifolia subsp. leedyi</determination>
    </key_statement>
  </key>
</bio:treatment>