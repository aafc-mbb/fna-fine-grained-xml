<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="mention_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="treatment_page">161</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="H. Ohba" date="1977" rank="genus">HYLOTELEPHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag. (Tokyo)</publication_title>
      <place_in_publication>90: 46, figs. 1–3. 1977  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus HYLOTELEPHIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hyle, wood, and genus Telephium</other_info_on_name>
    <other_info_on_name type="fna_id">200077</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial (dying back in winter, base persisting as annual addition to rootstock), not viviparous, 2–9 dm, glabrous, (from sympodial rootstock with thickened or tuberous roots).</text>
      <biological_entity id="o3168" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems [mostly] erect or ascending, little-branched proximal to cyme, succulent.</text>
      <biological_entity id="o3169" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="little-branched" value_original="little-branched" />
        <character constraint="to cyme" constraintid="o3170" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="texture" notes="" src="d0_s1" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o3170" name="cyme" name_original="cyme" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, cauline, alternate, opposite [or whorled], sessile or short-petiolate, not connate basally;</text>
      <biological_entity id="o3171" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o3172" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole to 5 mm;</text>
      <biological_entity id="o3173" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade [mostly] broad, ovate to obovate, oblanceolate, elliptic, or elliptic-oblong, or spatulate, laminar, 2–11 cm, herbaceous, base not spurred, margins entire or dentate or serrate;</text>
      <biological_entity id="o3174" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate oblanceolate elliptic or elliptic-oblong or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate oblanceolate elliptic or elliptic-oblong or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate oblanceolate elliptic or elliptic-oblong or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate oblanceolate elliptic or elliptic-oblong or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate oblanceolate elliptic or elliptic-oblong or spatulate" />
        <character is_modifier="false" name="position" src="d0_s4" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3175" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o3176" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>veins not conspicuous.</text>
      <biological_entity id="o3177" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal corymbose cymes, often paniculate.</text>
      <biological_entity id="o3178" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" notes="" src="d0_s6" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3179" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o3180" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers erect, 5-merous;</text>
      <biological_entity id="o3181" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals connate basally, all alike;</text>
      <biological_entity id="o3182" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="alike" value_original="alike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals ascending or spreading, distinct, white or greenish to pinkish or purple [red];</text>
      <biological_entity id="o3183" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="pinkish or purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o3184" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o3186" is_modifier="false" modifier="not" name="dehiscence" src="d0_s11" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o3185" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o3186" is_modifier="false" modifier="not" name="dehiscence" src="d0_s11" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o3186" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>nectaries quadrate to oblong [spatulate];</text>
      <biological_entity id="o3187" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s12" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2 times as many as sepals or absent;</text>
      <biological_entity id="o3188" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="sepal" constraintid="o3189" is_modifier="false" name="size_or_quantity" src="d0_s13" value="2 times as many as sepals" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3189" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments adnate to corolla base;</text>
      <biological_entity id="o3190" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="to corolla base" constraintid="o3191" is_modifier="false" name="fusion" src="d0_s14" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o3191" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pistils erect, distinct;</text>
      <biological_entity id="o3192" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary base attenuate, sometimes sharply narrowed, tapering to slender styles;</text>
      <biological_entity constraint="ovary" id="o3193" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="sometimes sharply" name="shape" src="d0_s16" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="slender" id="o3194" name="style" name_original="styles" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles shorter than ovary.</text>
      <biological_entity id="o3195" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="than ovary" constraintid="o3196" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3196" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits erect.</text>
      <biological_entity id="o3197" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ellipsoid, with narrow, parallel grooves.</text>
      <biological_entity id="o3199" name="groove" name_original="grooves" src="d0_s19" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s19" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="arrangement" src="d0_s19" value="parallel" value_original="parallel" />
      </biological_entity>
      <relation from="o3198" id="r258" name="with" negation="false" src="d0_s19" to="o3199" />
    </statement>
    <statement id="d0_s20">
      <text>x = 12.</text>
      <biological_entity id="o3198" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o3200" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia; temperate areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="temperate areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Orpine</other_name>
  <other_name type="common_name">stonecrop</other_name>
  <discussion>Sedum Linnaeus subg. Telephium (Gray) R. T. Clausen</discussion>
  <discussion>Species ca. 30 (3 in the flora).</discussion>
  <discussion>Hylotelephium has usually been treated as a section or subgenus of the polymorphic and, as now understood, polyphyletic genus Sedum. From Sedum as redefined by H. ’t Hart (1995) these species differ in their distinct and basally attenuate pistils; in general, they differ further in having tall, leafy annual floral stems from an underground tuberous base, very broad and thin leaves, corymbose cymes, and stems dying back in winter to a tuberous base (H. Ohba 1977).</discussion>
  <discussion>The vascular structure of flowers of this group were found by M. W. Quimby (1939) to be more primitive than in Sedum, having six distinct whorls of vascular traces from stele to appendages. In the distinctive Hylotelephium anacampseros Linnaeus of southern Europe, H. ’t Hart (1985) found only four whorls of traces and found evidence that this pattern derived from that of Hylotelephium and was independent of patterns in Sedum.</discussion>
  <references>
    <reference>Baldwin, J. T. 1937. The cyto-taxonomy of the Telephium section of Sedum. Amer. J. Bot. 24: 126–132.</reference>
    <reference>Ohba, H. 1977. The taxonomic status of Sedum telephium and its allied species (Crassulaceae). Bot. Mag. (Tokyo) 90: 41–56.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 2 times as long as sepals, white or pink tinged to pinkish; nectaries wider than long; flowers fertile.</description>
      <determination>1 Hylotelephium telephioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 2.5-3 times as long as sepals, white with green midribs or greenish, purple, or purplish red; nectaries longer than wide; flowers often sterile</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals purple or purplish red; cymes dense; leaves becoming markedly smaller distally; stamens usually 10; pistils 5.</description>
      <determination>2 Hylotelephium telephium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals white with green midribs or greenish; cymes lax; leaves becoming scarcely smaller distally; stamens absent or 1-10; pistils absent or 1-5</description>
      <determination>3 Hylotelephium erythrostictum</determination>
    </key_statement>
  </key>
</bio:treatment>