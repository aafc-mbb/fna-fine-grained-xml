<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="treatment_page">41</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">rotundifolium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 110. 1803  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species rotundifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065866</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(Michaux) Coville &amp; Britton" date="unknown" rank="species">rotundifolia</taxon_name>
    <taxon_hierarchy>genus Grossularia;species rotundifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.7–1.5 m.</text>
      <biological_entity id="o19589" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.7" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to recurving, (rooting at tips), glabrous;</text>
      <biological_entity id="o19590" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="recurving" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes absent or sometimes 1–2, 3–11 mm;</text>
      <biological_entity id="o19591" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19592" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s2" value="sometimes" value_original="sometimes" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <relation from="o19591" id="r1622" name="at" negation="false" src="d0_s2" to="o19592" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent (rarely present).</text>
      <biological_entity id="o19593" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o19594" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o19593" id="r1623" name="on" negation="false" src="d0_s3" to="o19594" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 0.8–2 cm, glabrous or short-pilose;</text>
      <biological_entity id="o19595" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o19596" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obovate to rotund, 3-lobed, cleft 1/2 to midrib, 1.5–5 cm, base widely cuneate to truncate, surfaces glabrous or puberulent, lobes oblong, margins with rounded teeth, apex acute.</text>
      <biological_entity id="o19597" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o19598" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="rotund" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character constraint="to midrib" constraintid="o19599" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19599" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o19600" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="widely cuneate" name="shape" src="d0_s5" to="truncate" />
      </biological_entity>
      <biological_entity id="o19601" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o19602" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o19603" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o19604" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o19605" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o19603" id="r1624" name="with" negation="false" src="d0_s5" to="o19604" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, solitary flowers or 2–3-flowered racemes, 3–5 cm, axis glabrous or sparsely stipitate-glandular.</text>
      <biological_entity id="o19606" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o19607" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19608" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3-flowered" value_original="2-3-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19609" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 4–7 mm, glabrous;</text>
      <biological_entity id="o19610" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts ovate, 1.5–2 mm, glabrous or with few short glands.</text>
      <biological_entity id="o19611" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with few short glands" value_original="with few short glands" />
      </biological_entity>
      <biological_entity id="o19612" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <relation from="o19611" id="r1625" name="with" negation="false" src="d0_s8" to="o19612" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium green, tubular-campanulate to narrowly tubular, 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o19613" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19614" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="tubular-campanulate" name="shape" src="d0_s9" to="narrowly tubular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, reflexed, green suffused with red, oblanceolate, 3.5–5 mm;</text>
      <biological_entity id="o19615" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19616" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green suffused with red" value_original="green suffused with red" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals not connivent, erect, cream with green or reddish tint, spatulate-obovate, not conspicuously revolute or inrolled, 2–2.5 mm;</text>
      <biological_entity id="o19617" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19618" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream with green or cream with reddish tint" />
        <character name="coloration" src="d0_s11" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate-obovate" value_original="spatulate-obovate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o19619" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o19620" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2.5–4 times longer than petals;</text>
      <biological_entity id="o19621" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19622" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="petal" constraintid="o19623" is_modifier="false" name="length_or_size" src="d0_s13" value="2.5-4 times longer than petals" />
      </biological_entity>
      <biological_entity id="o19623" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 6–8 mm, pilose;</text>
      <biological_entity id="o19624" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19625" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers cream, oblong-oval, 1 mm, apex rounded;</text>
      <biological_entity id="o19626" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19627" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" value_original="cream" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-oval" value_original="oblong-oval" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o19628" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary glabrous;</text>
      <biological_entity id="o19629" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o19630" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate to middle, 6–8 mm, villous in proximal 1/2.</text>
      <biological_entity id="o19631" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o19632" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s17" value="middle" value_original="middle" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
        <character constraint="in proximal 1/2" constraintid="o19633" is_modifier="false" name="pubescence" src="d0_s17" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19633" name="1/2" name_original="1/2" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, pale-purple, globose, 7–12 mm, glabrous.</text>
      <biological_entity id="o19634" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s18" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods, rocky slopes, boulderfields, heath and grassy balds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="boulderfields" />
        <character name="habitat" value="heath" />
        <character name="habitat" value="grassy balds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., D.C., Ga., Md., Mass., N.J., N.Y., N.C., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <other_name type="common_name">Wild gooseberry</other_name>
  <discussion>Ribes rotundifolium is known mainly from the Appalachian Mountains.</discussion>
  
</bio:treatment>