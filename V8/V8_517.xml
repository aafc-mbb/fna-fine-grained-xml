<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard P. Wunderlin</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="treatment_page">252</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="D. Don" date="unknown" rank="family">theophrastaceae</taxon_name>
    <taxon_name authority="Bertero ex Colla" date="1824" rank="genus">BONELLIA</taxon_name>
    <place_of_publication>
      <publication_title>Hortus Ripul.,</publication_title>
      <place_in_publication>21. 1824  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family theophrastaceae;genus BONELLIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Franco Andrea Bonelli, 1784–1830, Italian zoologist</other_info_on_name>
    <other_info_on_name type="fna_id">104241</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, resin canals absent.</text>
      <biological_entity id="o7754" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity constraint="resin" id="o7756" name="canal" name_original="canals" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, much-branched;</text>
      <biological_entity id="o7757" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>young branches puberulous, hairs relatively short, uniseriate [glabrous].</text>
      <biological_entity id="o7758" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulous" value_original="puberulous" />
      </biological_entity>
      <biological_entity id="o7759" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate or pseudoverticillate;</text>
      <biological_entity id="o7760" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="pseudoverticillate" value_original="pseudoverticillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to oblanceolate, base obtuse [attenuate], margins entire [serrate], apex obtuse to acute, aristate [mucronate or mucro absent], surfaces glabrous [pubescent], punctate.</text>
      <biological_entity id="o7761" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o7762" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o7763" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7764" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity id="o7765" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal racemes, 5–20-flowered, pedunculate.</text>
      <biological_entity id="o7766" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="5-20-flowered" value_original="5-20-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o7767" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels present, bracteate.</text>
      <biological_entity id="o7768" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals distinct;</text>
      <biological_entity id="o7769" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7770" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla yellow or orange [white or whitish], salverform or short-campanulate, lobes as long as or longer than tube, apex obtuse;</text>
      <biological_entity id="o7771" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7772" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s8" value="salverform" value_original="salverform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-campanulate" value_original="short-campanulate" />
      </biological_entity>
      <biological_entity id="o7773" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o7775" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7774" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7776" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o7773" id="r621" name="as long as" negation="false" src="d0_s8" to="o7775" />
    </statement>
    <statement id="d0_s9">
      <text>stamens borne at base of corolla-tube;</text>
      <biological_entity id="o7777" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7778" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o7779" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o7780" name="corolla-tube" name_original="corolla-tube" src="d0_s9" type="structure" />
      <relation from="o7778" id="r622" name="borne at" negation="false" src="d0_s9" to="o7779" />
      <relation from="o7778" id="r623" name="borne at" negation="false" src="d0_s9" to="o7780" />
    </statement>
    <statement id="d0_s10">
      <text>filaments distinct;</text>
      <biological_entity id="o7781" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7782" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers at first aggregated around stigma and style, later spreading;</text>
      <biological_entity id="o7783" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7784" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character constraint="around style" constraintid="o7786" is_modifier="false" name="arrangement" src="d0_s11" value="aggregated" value_original="aggregated" />
        <character is_modifier="false" modifier="later" name="orientation" notes="" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o7785" name="stigma" name_original="stigma" src="d0_s11" type="structure" />
      <biological_entity id="o7786" name="style" name_original="style" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>staminodes borne at apex of corolla-tube, petaloid;</text>
      <biological_entity id="o7787" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7788" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o7789" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o7790" name="corolla-tube" name_original="corolla-tube" src="d0_s12" type="structure" />
      <relation from="o7788" id="r624" name="borne at" negation="false" src="d0_s12" to="o7789" />
      <relation from="o7788" id="r625" name="borne at" negation="false" src="d0_s12" to="o7790" />
    </statement>
    <statement id="d0_s13">
      <text>stigma capitate, lobed.</text>
      <biological_entity id="o7791" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7792" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries yellow [orange], ovoid or globose, apex apiculate.</text>
      <biological_entity id="o7793" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o7794" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–8, dark-brown, oblong or elliptic, flattened, slightly alveolate, partially covered by placental tissue.</text>
      <biological_entity id="o7795" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s15" value="alveolate" value_original="alveolate" />
      </biological_entity>
      <biological_entity id="o7796" name="placental" name_original="placental" src="d0_s15" type="structure" />
      <biological_entity id="o7797" name="tissue" name_original="tissue" src="d0_s15" type="structure" />
      <relation from="o7795" id="r626" modifier="partially" name="covered by" negation="false" src="d0_s15" to="o7796" />
      <relation from="o7795" id="r627" modifier="partially" name="covered by" negation="false" src="d0_s15" to="o7797" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Mexico, West Indies (Greater Antilles), Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies (Greater Antilles)" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 22 (1 in the flora).</discussion>
  <references>
    <reference>Ståhl, B. and M. Källersjö. 2004. Reinstatement of Bonellia (Theophrastaceae). Novon 14: 115–118.</reference>
  </references>
  
</bio:treatment>