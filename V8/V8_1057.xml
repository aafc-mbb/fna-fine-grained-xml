<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">vaccinium</taxon_name>
    <taxon_name authority="(Small) Sleumer" date="1941" rank="section">Herpothamnus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>71: 414. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus vaccinium;section herpothamnus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">317365</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Small" date="1933" rank="genus">Herpothamnus</taxon_name>
    <taxon_hierarchy>genus Herpothamnus</taxon_hierarchy>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.</publication_title>
      <place_in_publication>1017, 1507, fig. p. 1017. 1933</place_in_publication>
    </place_of_publication>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, trailing, (0.5-) 0.7-1.8 (-3) dm, rhizomatous.</text>
      <biological_entity id="o2272" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="0.7" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s0" to="1.8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent.</text>
      <biological_entity id="o2273" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences axillary, racemes, 2-4-flowered, borne on growth of preceding year.</text>
      <biological_entity id="o2274" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2275" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
      <biological_entity id="o2276" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s2" value="preceding" value_original="preceding" />
      </biological_entity>
      <relation from="o2275" id="r192" name="borne on" negation="false" src="d0_s2" to="o2276" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels articulated with calyx.</text>
      <biological_entity id="o2277" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character constraint="with calyx" constraintid="o2278" is_modifier="false" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o2278" name="calyx" name_original="calyx" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 5;</text>
      <biological_entity id="o2279" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2280" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 5, connate for nearly their entire lengths, corolla urceolate;</text>
      <biological_entity id="o2281" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2282" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character constraint="for nearly their entire lengths" is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o2283" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 10, included, (ca. 2.5 mm);</text>
      <biological_entity id="o2284" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2285" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s6" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers without awns;</text>
      <biological_entity id="o2286" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2287" name="anther" name_original="anthers" src="d0_s7" type="structure" />
      <biological_entity id="o2288" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <relation from="o2287" id="r193" name="without" negation="false" src="d0_s7" to="o2288" />
    </statement>
    <statement id="d0_s8">
      <text>tubules ca. 1 mm, with terminal pores.</text>
      <biological_entity id="o2289" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2290" name="tubule" name_original="tubules" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2291" name="pore" name_original="pores" src="d0_s8" type="structure" />
      <relation from="o2290" id="r194" name="with" negation="false" src="d0_s8" to="o2291" />
    </statement>
    <statement id="d0_s9">
      <text>Berries 5-locular.</text>
      <biological_entity id="o2292" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="5-locular" value_original="5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 5-10.</text>
      <biological_entity id="o2293" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
    </statement>
  </description>
  <number>45h.</number>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1</discussion>
  <references>
    <reference>Kirkman, W. B. and J. R. Ballington. 1990. Creeping blueberries (Ericaceae: Vaccinium sect. Herpothamnus): A new look at V. crassifolium including V. sempervirens. Syst. Bot. 15: 679–699.</reference>
    <reference>Uttal, L. J. 1986. The taxonomic position of Vaccinium sempervirens (Ericaceae). Rhodora 88: 515–516.</reference>
  </references>
  
</bio:treatment>