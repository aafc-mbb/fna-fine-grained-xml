<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
    <other_info_on_meta type="treatment_page">182</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="(Lemaire) Britton &amp; Rose" date="1903" rank="species">cymosa</taxon_name>
    <taxon_name authority="K. M. Nakai &amp; Verity" date="1988" rank="subspecies">crebrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>34: 344, fig. 3. 1988,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species cymosa;subspecies crebrifolia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092066</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices mostly simple, 1–2 cm diam.</text>
      <biological_entity id="o27634" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes usually 6–15-leaved;</text>
      <biological_entity id="o27635" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o27636" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="6-15-leaved" value_original="6-15-leaved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade olive green, elliptic to spatulate, 4–10 (–15) × 2–5 cm, apex acute to acuminate, surfaces not farinose, rarely glaucous.</text>
      <biological_entity id="o27637" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o27638" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="olive green" value_original="olive green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="spatulate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27639" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <biological_entity id="o27640" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="farinose" value_original="farinose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: floral shoots 20–50-leaved, 10–30 (–50) cm;</text>
      <biological_entity id="o27641" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="floral" id="o27642" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="20-50-leaved" value_original="20-50-leaved" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cincinni 3+, 2–15-flowered, 2–10 cm.</text>
      <biological_entity id="o27643" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o27644" name="cincinnus" name_original="cincinni" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-15-flowered" value_original="2-15-flowered" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Petals mustard yellow, 9–10 × 3–3.5 mm. 2n = 34.</text>
      <biological_entity id="o27645" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="mustard yellow" value_original="mustard yellow" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27646" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Nearly vertical granite slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vertical granite slopes" modifier="nearly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies crebrifolia is known only from Fish Canyon in the San Gabriel Mountains; it is considered fairly threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi).</discussion>
  <discussion>Subspecies crebrifolia is remarkable for the large number of cauline leaves and the wide rosette leaves. It is said to be quite distinct from subsp. pumila, which is more widespread at generally higher elevations in the same mountains; it differs further in its taller floral shoots and its later flowering.</discussion>
  
</bio:treatment>