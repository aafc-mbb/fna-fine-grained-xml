<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">album</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 432. 1753,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species album</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250092127</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, laxly cespitose, minutely puberulent, papillose.</text>
      <biological_entity id="o14313" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="laxly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="relief" src="d0_s0" value="papillose" value_original="papillose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping and short-ascending, much-branched, (densely glandular-pubescent basally), not bearing rosettes.</text>
      <biological_entity id="o14314" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="short-ascending" value_original="short-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
      </biological_entity>
      <biological_entity id="o14315" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o14314" id="r1225" name="bearing" negation="true" src="d0_s1" to="o14315" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, patent or appressed, sessile;</text>
      <biological_entity id="o14316" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green, often reddish, not glaucous, linear to ovate, subterete but adaxial surface somewhat flattened, 4–20 (–25) × 1–20 mm, base scarcely spurred, not scarious, apex obtuse or rounded, (surfaces glabrous or sparsely hairy).</text>
      <biological_entity id="o14317" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="ovate" />
        <character constraint="but adaxial surface" constraintid="o14318" is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14318" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o14319" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="scarcely" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o14320" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots erect, simple or branched, 5–18 (–30) cm, (glabrous or sparsely hairy);</text>
      <biological_entity id="o14321" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades linear to ovate, base scarcely spurred;</text>
      <biological_entity id="o14322" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="ovate" />
      </biological_entity>
      <biological_entity id="o14323" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="scarcely" name="architecture_or_shape" src="d0_s5" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o14324" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate cymes, 15–50+-flowered, 3–5-branched;</text>
      <biological_entity id="o14325" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="15-50+-flowered" value_original="15-50+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-5-branched" value_original="3-5-branched" />
      </biological_entity>
      <biological_entity id="o14326" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches reflexed, forked;</text>
      <biological_entity id="o14327" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts similar to leaves, smaller.</text>
      <biological_entity id="o14328" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o14329" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o14328" id="r1226" name="to" negation="false" src="d0_s9" to="o14329" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 3–5 mm.</text>
      <biological_entity id="o14330" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o14331" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals erect, connate basally, green, ovate to triangular, equal, 0.5–1.5 × 0.2–0.5 mm, apex acute, (glabrous or sparsely and minutely puberulent);</text>
      <biological_entity id="o14332" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="triangular" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s12" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14333" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spreading, distinct, white or rarely pink, lanceolate, not carinate, 2–4.5 mm, apex subacute;</text>
      <biological_entity id="o14334" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14335" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments white;</text>
      <biological_entity id="o14336" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers red;</text>
      <biological_entity id="o14337" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar scales white or yellow, spatulate.</text>
      <biological_entity constraint="nectar" id="o14338" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spatulate" value_original="spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels erect in fruit, distinct, whitish.</text>
      <biological_entity id="o14340" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 34, 51, 68, 85, 102, 136.</text>
      <biological_entity id="o14339" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in fruit" constraintid="o14340" is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14341" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="34" value_original="34" />
        <character name="quantity" src="d0_s18" value="51" value_original="51" />
        <character name="quantity" src="d0_s18" value="68" value_original="68" />
        <character name="quantity" src="d0_s18" value="85" value_original="85" />
        <character name="quantity" src="d0_s18" value="102" value_original="102" />
        <character name="quantity" src="d0_s18" value="136" value_original="136" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous rock ledges, gravelly flat areas, ruderal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous rock ledges" />
        <character name="habitat" value="gravelly flat areas" />
        <character name="habitat" value="ruderal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., Ont., Que.; Calif., Ind., Maine, Mich., N.J., N.Y., Ohio, Oreg., Pa., Utah, Wash., W.Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion>Sedum album was first reported as naturalized in the United States in 1934.</discussion>
  
</bio:treatment>