<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1839" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>346. 1839  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065834</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Coville &amp; Britton" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Grossularia;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.6–1.4 m.</text>
      <biological_entity id="o7670" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_unit="m" name="some_measurement" src="d0_s0" to="1.4" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous;</text>
      <biological_entity id="o7671" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 3, 5–15 mm;</text>
      <biological_entity id="o7672" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7673" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o7672" id="r612" name="at" negation="false" src="d0_s2" to="o7673" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o7674" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o7675" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o7674" id="r613" name="on" negation="false" src="d0_s3" to="o7675" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole ca. 0.5 cm, tomentose;</text>
      <biological_entity id="o7676" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7677" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="cm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade roundish, shallowly 3 (–5) -lobed, cleft nearly 1/4 to midrib, 1–3 cm, base truncate, surfaces glabrous, glabrate, or hairy, not glandular, lobes broadly oblong, sides parallel, margins with 2–4 blunt teeth, apex rounded.</text>
      <biological_entity id="o7678" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7679" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="roundish" value_original="roundish" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="3(-5)-lobed" value_original="3(-5)-lobed" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character constraint="to midrib" constraintid="o7680" name="quantity" src="d0_s5" value="1/4" value_original="1/4" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7680" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o7681" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o7682" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o7683" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o7684" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o7685" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o7686" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="true" name="shape" src="d0_s5" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o7687" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o7685" id="r614" name="with" negation="false" src="d0_s5" to="o7686" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ascending, solitary flowers or 2–3-flowered racemes, 1.5–4 cm, axis glabrous or with few stipitate-glands, flowers evenly spaced.</text>
      <biological_entity id="o7688" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o7689" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7690" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3-flowered" value_original="2-3-flowered" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7691" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="with few stipitate-glands" value_original="with few stipitate-glands" />
      </biological_entity>
      <biological_entity id="o7692" name="stipitate-gland" name_original="stipitate-glands" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o7693" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
      <relation from="o7691" id="r615" name="with" negation="false" src="d0_s6" to="o7692" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 6–8 mm, pubescent, sparsely stipitate-glandular;</text>
      <biological_entity id="o7694" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts broadly ovate, 3–5 mm, glabrous or sparsely hairy.</text>
      <biological_entity id="o7695" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium greenish, broadly tubular, 2 mm, glabrous or puberulent;</text>
      <biological_entity id="o7696" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7697" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, reflexed, dark red or green tinged with dark red, deltate-ovate, 6–8 mm;</text>
      <biological_entity id="o7698" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7699" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green tinged with dark red" />
        <character is_modifier="false" name="shape" src="d0_s10" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals connivent, erect, white, oblong, inrolled, 2–4 mm;</text>
      <biological_entity id="o7700" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7701" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o7702" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o7703" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 0.9–4 times as long as petals, sometimes slightly shorter or longer than petals;</text>
      <biological_entity id="o7704" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7705" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="petal" constraintid="o7706" is_modifier="false" name="length" src="d0_s13" value="0.9-4 times as long as petals" />
        <character is_modifier="false" modifier="sometimes slightly" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character constraint="than petals" constraintid="o7707" is_modifier="false" name="length_or_size" src="d0_s13" value="sometimes slightly shorter or longer" />
      </biological_entity>
      <biological_entity id="o7706" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <biological_entity id="o7707" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 6–8 mm, glabrous;</text>
      <biological_entity id="o7708" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7709" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers greenish yellow, lanceolate, somewhat sagittate, 1.8–2 mm, apex apiculate;</text>
      <biological_entity id="o7710" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o7711" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s15" value="sagittate" value_original="sagittate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7712" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary with longer, glandless bristles among gland-tipped hairs;</text>
      <biological_entity id="o7713" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o7714" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
      <biological_entity id="o7715" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="glandless" value_original="glandless" />
      </biological_entity>
      <biological_entity id="o7716" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o7714" id="r616" name="with" negation="false" src="d0_s16" to="o7715" />
      <relation from="o7715" id="r617" name="among" negation="false" src="d0_s16" to="o7716" />
    </statement>
    <statement id="d0_s17">
      <text>styles connate nearly to middle, 8–10 mm, glabrous.</text>
      <biological_entity id="o7717" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o7718" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="to middle" constraintid="o7719" is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7719" name="middle" name_original="middle" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, red, globose, 9–10 mm, bristles stiff, glandular and eglandular.</text>
      <biological_entity id="o7720" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7721" name="bristle" name_original="bristles" src="d0_s18" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s18" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s18" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Hillside gooseberry</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Ribes amarum, R. binominatum, R. californicum, R. lobbii, R. menziesii, R. roezlii, and R. speciosum formed a clade sister to R. velutinum in the analyses of L. M. Schultheis and M. J. Donoghue (2004). These western taxa have stamens with relatively long filaments and anthers that are short-apiculate with indehiscent tips, and most have deep red or pink, reflexed sepals and erect, mostly white petals that form a tube. Based on these morphologic characters, R. marshallii, R. thacherianum, and R. victoris Janczewski also belong here. Taxa have been recognized at specific, subspecific, or varietal level within this striking group; the entire complex requires more study.</discussion>
  <discussion>Ribes californicum occurs in the Coast Ranges from Mendocino County to the Santa Ana and San Gabriel Mountains.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades glabrous or glabrate; stamens 2-4 times as long as petals.</description>
      <determination>32a Ribes californicum var. californicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades hairy; stamens 0.9-1 times as long as petals.</description>
      <determination>32b Ribes californicum var. hesperium</determination>
    </key_statement>
  </key>
</bio:treatment>