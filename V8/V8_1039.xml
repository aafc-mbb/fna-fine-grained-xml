<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="treatment_page">533</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1819" rank="genus">gaylussacia</taxon_name>
    <taxon_name authority="Small" date="1927" rank="species">mosieri</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>27: 36. 1927  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus gaylussacia;species mosieri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065724</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasiococcus</taxon_name>
    <taxon_name authority="(Small) Small" date="unknown" rank="species">mosieri</taxon_name>
    <taxon_hierarchy>genus Lasiococcus;species mosieri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–10 (–15) dm, forming small to extensive colonies;</text>
      <biological_entity id="o7292" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7293" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s0" to="extensive" />
      </biological_entity>
      <relation from="o7292" id="r571" name="forming" negation="false" src="d0_s0" to="o7293" />
    </statement>
    <statement id="d0_s1">
      <text>branches somewhat spreading;</text>
      <biological_entity id="o7294" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs of current season pale green, often finely glandular-hairy (hairs reddish).</text>
      <biological_entity id="o7295" name="twig" name_original="twigs" src="d0_s2" type="structure" constraint="pale-green; pale-green">
        <character is_modifier="false" modifier="often finely" name="pubescence" src="d0_s2" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity id="o7296" name="pale-green" name_original="pale green" src="d0_s2" type="structure" constraint="current" />
      <relation from="o7295" id="r572" name="part_of" negation="false" src="d0_s2" to="o7296" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–2 mm;</text>
      <biological_entity id="o7297" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7298" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pale green abaxially, green adaxially, ovate to oblong, 2.5–5 × 0.7–2.5 cm, subcoriaceous, base cuneate, margins entire, apex rounded to obtuse, surfaces stipitate-glandular-hairy and eglandular-hairy (hairs relatively shorter), abaxial surface also sessile-glandular.</text>
      <biological_entity id="o7299" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7300" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o7301" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7302" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7303" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o7304" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7305" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences spreading, 4–8-flowered, bracteate, 3–6 cm, often glandular-hairy (hairs silvery-silky, reddish, 1–1.5 mm);</text>
      <biological_entity id="o7306" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-8-flowered" value_original="4-8-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts persistent, leaflike, 5–12 mm at maturity, longer than pedicels, hairy and stipitate-glandular-hairy.</text>
      <biological_entity id="o7307" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" constraint="at pedicels" constraintid="o7309" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
      </biological_entity>
      <biological_entity id="o7309" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7308" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 5–7 mm, stipitate-glandular-hairy;</text>
      <biological_entity id="o7310" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1–2, 3–6 mm.</text>
      <biological_entity id="o7311" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, 2–3 mm, stipitate-glandular-hairy (hairs 1–1.5 mm);</text>
      <biological_entity id="o7312" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7313" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4–5, corolla white or pinkish, campanulate, 6.5–8.5 mm (averaging 7.4 mm), lobes triangular, 1.2–1.7 mm;</text>
      <biological_entity id="o7314" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7315" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o7316" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7317" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 3–4 mm, glabrous;</text>
      <biological_entity id="o7318" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7319" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers included, 2.5–4.3 mm, thecae divergent distally;</text>
      <biological_entity id="o7320" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7321" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7322" name="theca" name_original="thecae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary stipitate-glandular-hairy (hairs 1–1.5 mm).</text>
      <biological_entity id="o7323" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7324" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Drupes juicy, sweet, glossy black, 6–9 mm diam., stipitate-glandular-hairy (hairs to 1 mm).</text>
      <biological_entity id="o7325" name="drupe" name_original="drupes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="taste" src="d0_s14" value="sweet" value_original="sweet" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2–2.3 mm.</text>
      <biological_entity id="o7326" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pitcher plant bogs, hillside seepage bogs, wet pine savannas and flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pitcher plant bogs" />
        <character name="habitat" value="hillside seepage bogs" />
        <character name="habitat" value="wet pine savannas" />
        <character name="habitat" value="flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Hirsute huckleberry</other_name>
  <discussion>Gaylussacia mosieri is an eastern Gulf coastal plain endemic found in Sarracenia-dominated seepage bogs; it is related to G. bigeloviana, G. dumosa, and G. orocola and is easily distinguished by the glandular hairs on the hypanthium, bracts, pedicels, and twigs: 1–1.5 mm versus less than 0.5 mm for the other three species.</discussion>
  
</bio:treatment>