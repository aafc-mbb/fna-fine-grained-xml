<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="treatment_page">502</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">lyonia</taxon_name>
    <taxon_name authority="(Lamarck) K. Koch" date="1872" rank="species">lucida</taxon_name>
    <place_of_publication>
      <publication_title>Dendrologie</publication_title>
      <place_in_publication>2(1): 118. 1872,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus lyonia;species lucida;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065690</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andromeda</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">lucida</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>1: 157. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Andromeda;species lucida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Desmothamnus</taxon_name>
    <taxon_name authority="(Lamarck) Small" date="unknown" rank="species">lucidus</taxon_name>
    <taxon_hierarchy>genus Desmothamnus;species lucidus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 2.5 (–5) m.</text>
      <biological_entity id="o27474" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to arching, usually sharply 3-angled.</text>
      <biological_entity id="o27475" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="arching" />
        <character is_modifier="false" modifier="usually sharply" name="shape" src="d0_s1" value="3-angled" value_original="3-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent;</text>
      <biological_entity id="o27476" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly to widely elliptic, obovate, or slightly ovate, 1–8.5 (–10.5) × 0.5–4.5 (–5.5) cm, rigidly coriaceous, base attenuate or cuneate to rounded, margins entire, usually revolute, apex acuminate to acute, rarely rounded, surfaces scattered, multicellular, glandular short-headed-hairy, not lepidote, otherwise glabrous or hairy on midvein adaxially, intramarginal vein present.</text>
      <biological_entity id="o27477" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="rigidly" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o27478" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o27479" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o27480" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o27481" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-headed-hairy" value_original="short-headed-hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="lepidote" value_original="lepidote" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="on midvein" constraintid="o27482" is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o27482" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity constraint="intramarginal" id="o27483" name="vein" name_original="vein" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences fascicles, developing from buds along distal portion of stems of previous year;</text>
      <biological_entity id="o27484" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character constraint="from buds" constraintid="o27485" is_modifier="false" name="development" src="d0_s4" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o27485" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o27486" name="portion" name_original="portion" src="d0_s4" type="structure" />
      <biological_entity id="o27487" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity constraint="previous" id="o27488" name="year" name_original="year" src="d0_s4" type="structure" />
      <relation from="o27485" id="r2305" name="along" negation="false" src="d0_s4" to="o27486" />
      <relation from="o27486" id="r2306" name="part_of" negation="false" src="d0_s4" to="o27487" />
      <relation from="o27486" id="r2307" name="part_of" negation="false" src="d0_s4" to="o27488" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 1 per flower, linear-lanceolate, to 4 mm.</text>
      <biological_entity id="o27489" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character constraint="per flower" constraintid="o27490" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27490" name="flower" name_original="flower" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels glandular-hairy, not lepidote.</text>
      <biological_entity id="o27491" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="lepidote" value_original="lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes 2–7.5 × 1–2 mm, glandular-hairy, not lepidote;</text>
      <biological_entity id="o27492" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o27493" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="lepidote" value_original="lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla usually pink, sometimes white or red, cylindric (base swollen), 5–9 × 2–5 mm;</text>
      <biological_entity id="o27494" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o27495" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 3–5 mm, roughened, with 2 well-developed spurs.</text>
      <biological_entity id="o27496" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27497" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s9" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o27498" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="development" src="d0_s9" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o27497" id="r2308" name="with" negation="false" src="d0_s9" to="o27498" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules ovoid to ovoid-globose, 3–5 × 3–5 mm, apex slightly constricted, glabrous or short-headed-hairy;</text>
      <biological_entity id="o27499" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="ovoid-globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27500" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s10" value="constricted" value_original="constricted" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="short-headed-hairy" value_original="short-headed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sutures usually remaining attached to adjacent valve;</text>
      <biological_entity id="o27501" name="suture" name_original="sutures" src="d0_s11" type="structure" />
      <biological_entity id="o27502" name="valve" name_original="valve" src="d0_s11" type="structure">
        <character is_modifier="true" name="fixation" src="d0_s11" value="attached" value_original="attached" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o27501" id="r2309" name="remaining" negation="false" src="d0_s11" to="o27502" />
    </statement>
    <statement id="d0_s12">
      <text>placentae central to nearly basal.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 24.</text>
      <biological_entity id="o27503" name="placenta" name_original="placentae" src="d0_s12" type="structure">
        <character char_type="range_value" from="central" name="position" src="d0_s12" to="nearly basal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27504" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, acid, pine flatwoods and savannas, streamhead pocosins and baygalls, acid blackwater swamps, shrub bogs and peat-based pocosins, pond margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="acid" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pocosins" modifier="streamhead" />
        <character name="habitat" value="baygalls" />
        <character name="habitat" value="acid blackwater swamps" />
        <character name="habitat" value="shrub bogs" />
        <character name="habitat" value="peat-based pocosins" />
        <character name="habitat" value="pond margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Va.; West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Fetterbush</other_name>
  <other_name type="common_name">hurrah-bush</other_name>
  <discussion>Lyonia lucida is occasionally used as an ornamental; it is most closely related to L. mariana. The Cuban populations tend to have slightly longer and more densely hairy calyx lobes than do those of the southeastern United States.</discussion>
  
</bio:treatment>