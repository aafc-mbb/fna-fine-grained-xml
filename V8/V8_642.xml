<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John J. Pipoly III,Jon M. Ricketson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">302</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">myrsinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MYRSINE</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 196. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 90. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrsinaceae;genus MYRSINE</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek name for a kind of myrtle</other_info_on_name>
    <other_info_on_name type="fna_id">121522</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, monoecious or dioecious [polygamous], not succulent, [densely pubescent] glabrous.</text>
      <biological_entity id="o4417" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent, alternate, monomorphic;</text>
      <biological_entity id="o4419" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present;</text>
      <biological_entity id="o4420" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic, oblong, obovate, or oblanceolate, base attenuate to rounded, margins entire, revolute, apex acute to rounded, often emarginate, surfaces glabrous.</text>
      <biological_entity id="o4421" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o4422" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o4423" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o4424" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="rounded" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s3" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o4425" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences lateral (axillary) fascicles [umbels], sessile or subsessile, shorter than petiole;</text>
      <biological_entity id="o4426" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character constraint="than petiole" constraintid="o4427" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4427" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>peduncle accrescent, forming short-shoot;</text>
      <biological_entity id="o4428" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <biological_entity id="o4429" name="short-shoot" name_original="short-shoot" src="d0_s5" type="structure" />
      <relation from="o4428" id="r366" name="forming" negation="false" src="d0_s5" to="o4429" />
    </statement>
    <statement id="d0_s6">
      <text>bracts persistent.</text>
      <biological_entity id="o4430" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present [absent].</text>
      <biological_entity id="o4431" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers [bisexual] unisexual;</text>
      <biological_entity id="o4432" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4–6, green, calyx lobes triangular to ovate, ± equaling tube;</text>
      <biological_entity id="o4433" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o4434" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
      <biological_entity id="o4435" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals [4–] 5 [–6], corolla green to white or pink, cupuliform to campanulate, lobes longer than tube, apex acute;</text>
      <biological_entity id="o4436" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4437" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="white or pink" />
        <character char_type="range_value" from="cupuliform" name="shape" src="d0_s10" to="campanulate" />
      </biological_entity>
      <biological_entity id="o4438" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character constraint="than tube" constraintid="o4439" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4439" name="tube" name_original="tube" src="d0_s10" type="structure" />
      <biological_entity id="o4440" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 4–6;</text>
      <biological_entity id="o4441" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct or connate basally (forming tube);</text>
      <biological_entity id="o4442" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminodes present;</text>
      <biological_entity id="o4443" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas conic, 3–5-lobed.</text>
      <biological_entity id="o4444" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits drupaceous, green to black, globose [subglobose, ellipsoid, ovoid, obovoid, or subovoid];</text>
      <biological_entity id="o4445" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="drupaceous" value_original="drupaceous" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s15" to="black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exocarp somewhat fleshy;</text>
      <biological_entity id="o4446" name="exocarp" name_original="exocarp" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endocarp crusty or leathery.</text>
      <biological_entity id="o4447" name="endocarp" name_original="endocarp" src="d0_s17" type="structure">
        <character is_modifier="false" name="texture" src="d0_s17" value="crusty" value_original="crusty" />
        <character is_modifier="false" name="texture" src="d0_s17" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1, white to black, globose or depressed, usually covered with membranous remnants of placenta.</text>
      <biological_entity id="o4448" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s18" to="black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="depressed" value_original="depressed" />
      </biological_entity>
      <biological_entity id="o4449" name="remnant" name_original="remnants" src="d0_s18" type="structure">
        <character is_modifier="true" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o4450" name="placenta" name_original="placenta" src="d0_s18" type="structure">
        <character is_modifier="true" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o4448" id="r367" modifier="usually" name="covered with" negation="false" src="d0_s18" to="o4449" />
      <relation from="o4448" id="r368" modifier="usually" name="covered with" negation="false" src="d0_s18" to="o4450" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; pantropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="pantropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Colicwood</other_name>
  <discussion>Rapanea Aublet</discussion>
  <discussion>Species ca. 300 (1 in the flora).</discussion>
  <discussion>J. J. Pipoly (1991, 1992, 1992b, 1996, 2007) and J. M. Ricketson and Pipoly (1997, 1999) have discussed the circumscription of Myrsine to include Rapanea, among others. Field work has shown that the dioecious plants often have bisexual flowers.</discussion>
  
</bio:treatment>