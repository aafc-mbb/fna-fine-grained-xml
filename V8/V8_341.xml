<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="illustration_page">165</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sempervivum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">tectorum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 464. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sempervivum;species tectorum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220012387</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: rosettes usually open, 4–10 cm diam., producing offsets attached to parental plants by stout stolons;</text>
      <biological_entity id="o2222" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o2223" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s0" to="10" to_unit="cm" />
        <character constraint="to plants" constraintid="o2225" is_modifier="false" name="fixation" src="d0_s0" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o2224" name="offset" name_original="offsets" src="d0_s0" type="structure" />
      <biological_entity id="o2225" name="plant" name_original="plants" src="d0_s0" type="structure" />
      <biological_entity id="o2226" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o2223" id="r190" name="producing" negation="false" src="d0_s0" to="o2224" />
      <relation from="o2225" id="r191" name="by" negation="false" src="d0_s0" to="o2226" />
    </statement>
    <statement id="d0_s1">
      <text>blade 1–1.5 cm wide, apex stoutly mucronate, surfaces usually glabrous, sometimes sparingly hairy (some cultivars hairy).</text>
      <biological_entity id="o2227" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2228" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2229" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="stoutly" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o2230" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparingly" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences erect, flat-topped to thyrsiform cymes, 10–40 (–100+) -flowered, bracteate, (10–) 20–30 (–50) cm, usually 5–15 cm diam.;</text>
      <biological_entity id="o2231" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="10-40(-100+)-flowered" value_original="10-40(-100+)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" modifier="usually" name="diameter" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2232" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="thyrsiform" value_original="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts 1 per flower, clasping at base, ovatelanceolate, base narrow.</text>
      <biological_entity id="o2233" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character constraint="per flower" constraintid="o2234" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character constraint="at base" constraintid="o2235" is_modifier="false" name="architecture_or_fixation" notes="" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o2234" name="flower" name_original="flower" src="d0_s3" type="structure" />
      <biological_entity id="o2235" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o2236" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals strongly reflexed at anthesis, 9–12 mm, margins entire, abaxially pubescent;</text>
      <biological_entity id="o2237" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2238" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="strongly" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2239" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens red to purple.</text>
      <biological_entity id="o2240" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = (36, 40), 72.</text>
      <biological_entity id="o2241" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s5" to="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2242" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="[36" value_original="[36" />
        <character name="quantity" src="d0_s6" value="40]" value_original="40]" />
        <character name="quantity" src="d0_s6" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering usually mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="usually" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky microsites such as rocky outcrops, stone walls, rock piles, adjacent to rocks, along cliff bases</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky microsites" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="stone walls" />
        <character name="habitat" value="rock piles" />
        <character name="habitat" value="rocks" constraint="along cliff" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="adjacent to rocks" />
        <character name="habitat" value="along cliff bases" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Conn., Maine, Mass., N.J., N.Y., Utah, Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Common houseleek</other_name>
  <discussion>Sempervivum tectorum has naturalized populations in the flora area but is rarely, if ever, considered an aggressive competitor with or threat to native species. Populations occur in areas that provide a measure of physical protection because they are intolerant of treading effects.</discussion>
  <discussion>Sempervivum tectorum is highly variable. H. ’t Hart et al. (2003) recognized two varieties [var. arvernense (Lecoq &amp; Lamotte) Zonneveld and var. tectorum] and presented an extensive synonomy for var. tectorum. Hundreds of cultivars have been propagated, sold, and traded for nearly 200 years. The variety of forms that could be found naturalized is large; any given naturalized population may differ in certain characteristics from the description above.</discussion>
  
</bio:treatment>