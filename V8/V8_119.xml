<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="treatment_page">63</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">micranthes</taxon_name>
    <taxon_name authority="(Greene) Small in N. L. Britton et al." date="1905" rank="species">marshallii</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al.,  N. Amer. Fl.</publication_title>
      <place_in_publication>22: 145. 1905  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus micranthes;species marshallii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065900</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">marshallii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 159. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species marshallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in groups or sometimes mat-forming, rhizomatous.</text>
      <biological_entity id="o13100" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13101" name="group" name_original="groups" src="d0_s0" type="structure" />
      <relation from="o13100" id="r1111" name="in" negation="false" src="d0_s0" to="o13101" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o13102" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole flattened, 3–15 cm;</text>
      <biological_entity id="o13103" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate, 1–5 cm, ± fleshy, base attenuate, margins shallowly crenate-serrate (teeth ca. 2 mm), eciliate, surfaces sparsely tangled, reddish brown-hairy abaxially, glabrate adaxially.</text>
      <biological_entity id="o13104" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o13105" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o13106" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s3" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o13107" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="arrangement" src="d0_s3" value="tangled" value_original="tangled" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s3" value="reddish brown-hairy" value_original="reddish brown-hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 15–50+-flowered, open, lax thyrses, 20–40 cm, purple-tipped stipitate-glandular.</text>
      <biological_entity id="o13108" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="15-50+-flowered" value_original="15-50+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o13109" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s4" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals reflexed, lanceolate to oblong;</text>
      <biological_entity id="o13110" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o13111" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, with 2 basal yellow spots (often faded when dried), ovate, clawed, 1.5–4.5 mm, longer than sepals;</text>
      <biological_entity id="o13112" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13113" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character constraint="than sepals" constraintid="o13114" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13114" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>filaments strongly club-shaped, sometimes petaloid (short-clawed, equaling petals);</text>
      <biological_entity id="o13115" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13116" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s7" value="club--shaped" value_original="club--shaped" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistils distinct almost to base;</text>
      <biological_entity id="o13117" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13118" name="pistil" name_original="pistils" src="d0_s8" type="structure">
        <character constraint="to base" constraintid="o13119" is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o13119" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary superior, (to 1/3 adnate to hypanthium).</text>
      <biological_entity id="o13120" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13121" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules greenish to reddish purple, folliclelike.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 20.</text>
      <biological_entity id="o13122" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="reddish purple" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="folliclelike" value_original="folliclelike" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13123" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deeply shaded watercourses, stream banks, seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="watercourses" modifier="deeply shaded" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="shaded" modifier="deeply" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Marshall’s saxifrage</other_name>
  
</bio:treatment>