<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">arctostaphylos</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">nummularia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 366. 1868  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus arctostaphylos;species nummularia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220001036</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, prostrate, erect, or mound-forming, 0.1–5 m;</text>
      <biological_entity id="o11616" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mound-forming" value_original="mound-forming" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>burl absent;</text>
      <biological_entity id="o11617" name="burl" name_original="burl" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark on older stems persistent, gray or red-gray, shredded or rough;</text>
      <biological_entity id="o11618" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="red-gray" value_original="red-gray" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o11619" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o11618" id="r976" name="on" negation="false" src="d0_s2" to="o11619" />
    </statement>
    <statement id="d0_s3">
      <text>twigs densely short-haired with longer gland-tipped hairs.</text>
      <biological_entity id="o11620" name="twig" name_original="twigs" src="d0_s3" type="structure" />
      <biological_entity constraint="longer" id="o11621" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o11620" id="r977" name="with" negation="false" src="d0_s3" to="o11621" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves bifacial in stomatal distribution;</text>
      <biological_entity id="o11622" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="in stomatal" constraintid="o11623" is_modifier="false" name="architecture" src="d0_s4" value="bifacial" value_original="bifacial" />
      </biological_entity>
      <biological_entity id="o11623" name="stomatal" name_original="stomatal" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole to 3 mm;</text>
      <biological_entity id="o11624" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade bifacial, light green abaxially, dark green adaxially, shiny, orbiculate to orbiculate-ovate or oblongelliptic, 1.5–2.2 × 0.3–1.8 cm, base cuneate to obtuse or truncate to ± lobed, margins entire, cupped, surfaces smooth, glabrous, midvein hairy.</text>
      <biological_entity id="o11625" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="bifacial" value_original="bifacial" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s6" to="orbiculate-ovate or oblongelliptic" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="1.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11626" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="obtuse or truncate" />
      </biological_entity>
      <biological_entity id="o11627" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11628" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11629" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences panicles, 4–12-branched;</text>
      <biological_entity constraint="inflorescences" id="o11630" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="4-12-branched" value_original="4-12-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>immature inflorescence pendent, branches spreading, axis 0.5–1 cm, to 1 mm diam., densely short-haired with longer gland-tipped hairs;</text>
      <biological_entity id="o11631" name="inflorescence" name_original="inflorescence" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="immature" value_original="immature" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o11632" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o11633" name="axis" name_original="axis" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o11634" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o11633" id="r978" modifier="densely" name="with" negation="false" src="d0_s8" to="o11634" />
    </statement>
    <statement id="d0_s9">
      <text>bracts appressed, scalelike, deltate, 0.5–2 mm, apex acute, surfaces glabrous.</text>
      <biological_entity id="o11635" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11636" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11637" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 3–5 mm, glabrous.</text>
      <biological_entity id="o11638" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 4-merous;</text>
      <biological_entity id="o11639" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla white, urceolate;</text>
      <biological_entity id="o11640" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary white-hairy.</text>
      <biological_entity id="o11641" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits subcylindric, 3–4 mm diam., glabrous.</text>
      <biological_entity id="o11642" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Stones distinct (breaking apart in mature fruit).</text>
      <biological_entity id="o11643" name="stone" name_original="stones" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Glossyleaf manzanita</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 0.5-5 m; bark on older stems gray, shredded.</description>
      <determination>10a Arctostaphylos nummularia subsp. nummularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 0.1-0.5 m; bark on older stems gray or reddish, rough or shredded.</description>
      <determination>10b Arctostaphylos nummularia subsp. mendocinoensis</determination>
    </key_statement>
  </key>
</bio:treatment>