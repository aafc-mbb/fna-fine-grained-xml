<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">228</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1828" rank="genus">ECHEVERIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle,  Prodr.</publication_title>
      <place_in_publication>3: 401. 1828  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus ECHEVERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Atanasio Echevería y Godoy, fl. 1787–1803, Mexican botanical artist</other_info_on_name>
    <other_info_on_name type="fna_id">111196</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [shrubs], perennial, not viviparous, [0.2–] 2.5 [–6] dm, glabrous [pubescent].</text>
      <biological_entity id="o7095" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="some_measurement" src="d0_s0" unit="dm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, mostly branching, succulent;</text>
      <biological_entity id="o7096" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>floral stems annual, from leaf-axil, overtopping rosette, with scattered smaller leaves.</text>
      <biological_entity constraint="floral" id="o7097" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o7098" name="leaf-axil" name_original="leaf-axil" src="d0_s2" type="structure" />
      <biological_entity id="o7099" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="smaller" id="o7100" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o7097" id="r558" name="from" negation="false" src="d0_s2" to="o7098" />
      <relation from="o7097" id="r559" name="overtopping" negation="false" src="d0_s2" to="o7099" />
      <relation from="o7097" id="r560" name="with" negation="false" src="d0_s2" to="o7100" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves falling after withering, crowded in rosette, alternate, sessile, not connate basally;</text>
      <biological_entity id="o7101" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="after withering" is_modifier="false" name="life_cycle" src="d0_s3" value="falling" value_original="falling" />
        <character constraint="in rosette" constraintid="o7102" is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o7102" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to broadly rhombic [ovate-deltate to elliptic or oblanceolate], laminar, 3–11 [–40] cm, fleshy, base not spurred, margins entire;</text>
      <biological_entity id="o7103" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="broadly rhombic" />
        <character is_modifier="false" name="position" src="d0_s4" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o7104" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>with 1 vein entering margins.</text>
      <biological_entity id="o7105" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7106" name="vein" name_original="vein" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7107" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o7105" id="r561" name="with" negation="false" src="d0_s5" to="o7106" />
      <relation from="o7106" id="r562" name="entering" negation="false" src="d0_s5" to="o7107" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences lateral cymes [racemes or spikes].</text>
      <biological_entity id="o7108" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o7109" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o7110" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (not fetid), erect, ascending, or pendent, 5-merous;</text>
      <biological_entity id="o7111" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ascending, distinct, unalike in size;</text>
      <biological_entity id="o7112" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s9" value="unalike" value_original="unalike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals erect, connate basally, forming sharply 5-gonal–pyramidal tube, deep pink, yellow adaxially, (triquetrous [thinner]);</text>
      <biological_entity id="o7113" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="depth" src="d0_s10" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o7114" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="sharply" name="shape" src="d0_s10" value="5-gonal-pyramidal" value_original="5-gonal-pyramidal" />
      </biological_entity>
      <relation from="o7113" id="r563" name="forming" negation="false" src="d0_s10" to="o7114" />
    </statement>
    <statement id="d0_s11">
      <text>calyx and corolla not circumscissle;</text>
      <biological_entity id="o7115" name="calyx" name_original="calyx" src="d0_s11" type="structure" />
      <biological_entity id="o7116" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>nectaries truncate, wider than high;</text>
      <biological_entity id="o7117" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="width" src="d0_s12" value="wider than high" value_original="wider than high" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10;</text>
      <biological_entity id="o7118" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments adnate to corolla base;</text>
      <biological_entity id="o7119" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="to corolla base" constraintid="o7120" is_modifier="false" name="fusion" src="d0_s14" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o7120" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pistils erect and appressed, nearly distinct;</text>
      <biological_entity id="o7121" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary base not narrowed, tapering to styles;</text>
      <biological_entity constraint="ovary" id="o7122" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="narrowed" value_original="narrowed" />
        <character constraint="to styles" constraintid="o7123" is_modifier="false" name="shape" src="d0_s16" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o7123" name="style" name_original="styles" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 2+ times shorter than ovary.</text>
      <biological_entity id="o7124" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="ovary" constraintid="o7125" is_modifier="false" name="size_or_quantity" src="d0_s17" value="2+ times shorter than ovary" />
      </biological_entity>
      <biological_entity id="o7125" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits erect to spreading.</text>
      <biological_entity id="o7126" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s18" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ovoid, reticulate.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = ca. 34.</text>
      <biological_entity id="o7127" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s19" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o7128" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., Mexico, Central America, South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Species ca. 120 (1 in the flora).</discussion>
  <discussion>Echeveria is a remarkable group in which diploid species have anywhere from 12 to 34 chromosome pairs and polyploid species have from 28 to about 260 (C. H. Uhl 1992, 1995). Uhl concluded that the ancestral number was probably about x = 34, with a descending series to n = 12, and that plants with higher numbers are mostly autoploid. He found Echeveria to be part of a giant comparium that also includes Graptopetalum, Lenophyllum, Pachyphytum, Villadia, and others, as well as many Mexican species of Sedum, but not Dudleya.</discussion>
  <references>
    <reference>Moran, R. V. 1999. The name of Echeveria. Cact. Succ. J. (Los Angeles) 71: 301–306.</reference>
    <reference>Moran, R. V. and C. H. Uhl. 1964. The inflorescence of Echeveria. Cact. Succ. J. (Los Angeles) 36: 167–180.</reference>
    <reference>Uhl, C. H. 1992. Polyploidy, dysploidy, and chromosome pairing in Echeveria (Crassulaceae) and its hybrids. Amer. J. Bot. 79: 556–566.</reference>
    <reference>Uhl, C. H. 1995. Chromosomes and hybrids of Echeveria III. Series Secundae (Baker) Berger (Crassulaceae). Haseltonia 3: 34–48.</reference>
    <reference>Walther, E. 1972. Echeveria. San Francisco.</reference>
  </references>
  
</bio:treatment>