<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">519</other_info_on_meta>
    <other_info_on_meta type="illustration_page">511</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">vaccinium</taxon_name>
    <taxon_name authority="(Hill) W. D. J. Koch" date="1837" rank="section">Oxycoccus</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">macrocarpon</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 13, plate 7. 1789,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus vaccinium;section oxycoccus;species macrocarpon;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065705</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxycoccus</taxon_name>
    <taxon_name authority="(Aiton) Persoon" date="unknown" rank="species">macrocarpus</taxon_name>
    <taxon_hierarchy>genus Oxycoccus;species macrocarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often ascending, shoots 0.4–1.5 dm.</text>
      <biological_entity id="o2717" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2718" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades glaucous abaxially, green adaxially, usually narrowly elliptic to elliptic, rarely oblong, 5–18 × 2–55 mm, margins entire, slightly revolute.</text>
      <biological_entity id="o2719" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually narrowly" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2720" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences in axils of leaflike bracts at base of current-year’s shoots.</text>
      <biological_entity id="o2721" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o2722" name="axil" name_original="axils" src="d0_s2" type="structure" />
      <biological_entity id="o2723" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o2724" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="current-year" id="o2725" name="’" name_original="’" src="d0_s2" type="structure" />
      <biological_entity id="o2726" name="shoot" name_original="shoots" src="d0_s2" type="structure" />
      <relation from="o2721" id="r215" name="in" negation="false" src="d0_s2" to="o2722" />
      <relation from="o2722" id="r216" name="part_of" negation="false" src="d0_s2" to="o2723" />
      <relation from="o2722" id="r217" name="at" negation="false" src="d0_s2" to="o2724" />
      <relation from="o2724" id="r218" name="part_of" negation="false" src="d0_s2" to="o2725" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels nodding, slender, 2–3 cm, bracteolate;</text>
      <biological_entity id="o2727" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 2, greenish white, scalelike, 1–2 mm wide.</text>
      <biological_entity id="o2728" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="shape" src="d0_s4" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes relatively small;</text>
      <biological_entity id="o2729" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o2730" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla strongly reflexed at anthesis, white to pink;</text>
      <biological_entity id="o2731" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2732" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="strongly" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments hairy;</text>
      <biological_entity id="o2733" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2734" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anther tubules 1–2 mm.</text>
      <biological_entity id="o2735" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="anther" id="o2736" name="tubule" name_original="tubules" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Berries red to pink, 9–14 mm diam., smooth.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o2737" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="pink" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2738" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs, swamps, mires, wet shores and headlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="mires" />
        <character name="habitat" value="wet shores" />
        <character name="habitat" value="headlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Del., D.C., Ill., Ind., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis.; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Cranberry</other_name>
  <other_name type="common_name">canneberge à gros fruits</other_name>
  <discussion>Vaccinium macrocarpon is introduced and escaping elsewhere (British Columbia, Oregon, Washington) with respect to its normal range in eastern North America.</discussion>
  <references>
    <reference>Bruederle, L. P., M. S. Hugan, and J. M. Dignan. 1996. Genetic variation in natural populations of the large cranberry, Vaccinium macrocarpon Ait. (Ericaceae). Bull. Torrey Bot. Club 123: 41–47.</reference>
    <reference>Ogle, D. W. 1984. Phytogeography of Vaccinium macrocarpon Aiton in the southern United States. Virginia J. Sci. 35: 31–47.</reference>
    <reference>Upton, R., ed. 2002. Cranberry Fruit: Vaccinium macrocarpon Aiton. Standards of Analysis, Quality Control, and Therapeutics. Santa Cruz.</reference>
  </references>
  
</bio:treatment>