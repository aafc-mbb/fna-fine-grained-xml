<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">493</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erica</taxon_name>
    <taxon_name authority="Rudolphi" date="1800" rank="species">lusitanica</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>2: 286. 1800  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus erica;species lusitanica;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250065680</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± erect, 15–30 cm;</text>
      <biological_entity id="o10736" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>twigs of current season green, with short, stiff hairs ca. 0.3 mm, older twigs gray and brown striped, glabrescent.</text>
      <biological_entity id="o10737" name="twig" name_original="twigs" src="d0_s1" type="structure" constraint="season; season">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="current" id="o10738" name="season" name_original="season" src="d0_s1" type="structure" />
      <biological_entity id="o10739" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character name="some_measurement" src="d0_s1" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o10740" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray and brown striped" value_original="gray and brown striped" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <relation from="o10737" id="r904" name="part_of" negation="false" src="d0_s1" to="o10738" />
      <relation from="o10737" id="r905" name="with" negation="false" src="d0_s1" to="o10739" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in whorls of (3–) 4;</text>
      <biological_entity id="o10741" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10742" name="whorl" name_original="whorls" src="d0_s2" type="structure" />
      <relation from="o10741" id="r906" modifier="of (3-)4" name="in" negation="false" src="d0_s2" to="o10742" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.2–0.3 mm;</text>
      <biological_entity id="o10743" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear-lanceolate, flat to compressed-triangular in cross-section, 2.5–4 (–7) × 0.2 mm, margins revolute, sparsely prickled, surfaces glabrous.</text>
      <biological_entity id="o10744" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o10745" from="flat" name="shape" src="d0_s4" to="compressed-triangular" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" notes="" src="d0_s4" to="4" to_unit="mm" />
        <character name="width" notes="" src="d0_s4" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o10745" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity id="o10746" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o10747" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal panicles, ellipsoid, 10–25 cm.</text>
      <biological_entity id="o10748" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o10749" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–1.5 mm, shortly hairy.</text>
      <biological_entity id="o10750" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes (connate ca. 1/3 their lengths), ovate, 1 × 0.7 mm, margins entire, apex subacute, glabrous;</text>
      <biological_entity id="o10751" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="ovate" value_original="ovate" />
        <character name="length" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character name="width" src="d0_s7" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o10752" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o10753" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10754" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subacute" value_original="subacute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla white to pinkish white, broadly campanulate, 4–5 mm, lobes ovate-deltate, 0.5–1 mm, apex broadly rounded;</text>
      <biological_entity id="o10755" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10756" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pinkish white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10757" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate-deltate" value_original="ovate-deltate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10758" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 10;</text>
      <biological_entity id="o10759" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10760" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 2 mm;</text>
      <biological_entity id="o10761" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10762" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers awned, ca. 0.7 mm, awns 2, basal, 2-ciliate, 0.3 mm;</text>
      <biological_entity id="o10763" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10764" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
      <biological_entity id="o10765" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s11" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="2-ciliate" value_original="2-ciliate" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary glabrous;</text>
      <biological_entity id="o10766" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10767" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 2–2.5 mm;</text>
      <biological_entity id="o10768" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10769" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma exserted, obconic.</text>
      <biological_entity id="o10770" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o10771" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obconic" value_original="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 1–2 mm, glabrous.</text>
      <biological_entity id="o10772" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds ellipsoid, 0.6 × 0.5 mm, finely pitted.</text>
      <biological_entity id="o10773" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s16" unit="mm" value="0.6" value_original="0.6" />
        <character name="width" src="d0_s16" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy coastal sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sites" modifier="sandy" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; sw Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spanish heath</other_name>
  <discussion>Erica lusitanica is found naturalized in coastal California (Humboldt County south to San Diego County), and in southwestern Oregon.</discussion>
  
</bio:treatment>