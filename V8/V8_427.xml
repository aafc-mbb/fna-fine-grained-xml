<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="Torrey &amp; E. James ex Eaton" date="1829" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. ed.</publication_title>
      <place_in_publication>5, 388. 1829  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092123</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, not tufted, glabrous.</text>
      <biological_entity id="o17292" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or decumbent, simple or branched, not bearing rosettes.</text>
      <biological_entity id="o17293" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o17294" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o17293" id="r1458" name="bearing" negation="true" src="d0_s1" to="o17294" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, widely divergent, sessile;</text>
      <biological_entity id="o17295" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade pale or bluish green, not glaucous, narrowly lanceolate-elliptic or oblong, subterete to, rarely, globular, 3–6 × 1.5–2 mm, base short-spurred, not scarious, apex obtuse.</text>
      <biological_entity id="o17296" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="globular" value_original="globular" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17297" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="short-spurred" value_original="short-spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o17298" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots erect, simple or branched, 2.5–11 cm;</text>
      <biological_entity id="o17299" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades lanceolate-elliptic or oblong, base short-spurred;</text>
      <biological_entity id="o17300" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o17301" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="short-spurred" value_original="short-spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o17302" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymes, 20–60 (–100) -flowered, 1–3-branched;</text>
      <biological_entity constraint="inflorescences" id="o17303" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="20-60(-100)-flowered" value_original="20-60(-100)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-branched" value_original="1-3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches ± erect to spreading, sometimes slightly recurved, usually not forked, sometimes 1–2-forked;</text>
      <biological_entity id="o17304" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="less erect" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" modifier="sometimes slightly" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s8" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="1-2-forked" value_original="1-2-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts similar to leaves.</text>
      <biological_entity id="o17305" name="bract" name_original="bracts" src="d0_s9" type="structure" />
      <biological_entity id="o17306" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o17305" id="r1459" name="to" negation="false" src="d0_s9" to="o17306" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or to 1 mm.</text>
      <biological_entity id="o17307" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0-1 mm" value_original="0-1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o17308" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals erect, distinct, yellow-green, lanceolate or lanceolate-oblong, unequal, 0.6–3 × 0.4–1.5 mm, (base short-spurred), apex acute;</text>
      <biological_entity id="o17309" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17310" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spreading, distinct, yellow, elliptic-oblong, slightly cucullate, 2–4 mm, apex mucronate;</text>
      <biological_entity id="o17311" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="cucullate" value_original="cucullate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17312" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments yellow;</text>
      <biological_entity id="o17313" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow;</text>
      <biological_entity id="o17314" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar scales yellow or translucent, stipitate-reniform, subquadrate, or obovate.</text>
      <biological_entity constraint="nectar" id="o17315" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="stipitate-reniform" value_original="stipitate-reniform" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels widely spreading in fruit, connate basally, straw-yellow.</text>
      <biological_entity id="o17317" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 20.</text>
      <biological_entity id="o17316" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in fruit" constraintid="o17317" is_modifier="false" modifier="widely" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s17" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="straw-yellow" value_original="straw-yellow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17318" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas, shallow soil, commonly over granite or sandstone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" />
        <character name="habitat" value="shallow soil" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="sandstone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., La., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Yellow stonecrop</other_name>
  <discussion>Until recently, Sedum nuttallii was thought to have been published first in 1833 (not 1829), a year after S. nuttallianum Rafinesque; consequently the latter name has been applied incorrectly to this species.</discussion>
  
</bio:treatment>