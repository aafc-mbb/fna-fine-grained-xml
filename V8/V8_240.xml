<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
    <other_info_on_meta type="illustration_page">118</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="genus">bolandra</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">oregana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 292. 1879,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus bolandra;species oregana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065991</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole 3–15 cm;</text>
      <biological_entity id="o1783" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o1784" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade shallowly lobed, 2–7 cm, ultimate margins serrate.</text>
      <biological_entity id="o1785" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1786" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o1787" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 6–18-flowered, 16–40 cm, stipitate-glandular;</text>
      <biological_entity id="o1788" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="6-18-flowered" value_original="6-18-flowered" />
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts subtending pedicels conspicuous, auriculate.</text>
      <biological_entity id="o1789" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o1790" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <relation from="o1789" id="r157" name="subtending" negation="false" src="d0_s3" to="o1790" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium campanulate-urceolate, 5–10 mm, glabrous;</text>
      <biological_entity id="o1791" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1792" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate-urceolate" value_original="campanulate-urceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 6–10 mm, apex long-acuminate;</text>
      <biological_entity id="o1793" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1794" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1795" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals usually reddish purple to dark purple, narrowly lanceolate, unlobed, 7–12 (–14) mm, apex long-tapered;</text>
      <biological_entity id="o1796" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1797" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually reddish purple" name="coloration_or_density" src="d0_s6" to="dark purple" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1798" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="long-tapered" value_original="long-tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens included, 2 mm;</text>
      <biological_entity id="o1799" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1800" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="included" value_original="included" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovaries connate proximally 1/4 their lengths;</text>
      <biological_entity id="o1801" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1802" name="ovary" name_original="ovaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s8" value="1/4" value_original="1/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles barely exserted, 2 mm;</text>
      <biological_entity id="o1803" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1804" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="barely" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas distinct.</text>
      <biological_entity id="o1805" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1806" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules brown, ovoid, 8–9 mm.</text>
      <biological_entity id="o1807" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 100–125, 0.7–9 mm. 2n = 14.</text>
      <biological_entity id="o1808" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" unit=",0.7-9 mm" />
        <character char_type="range_value" from="100" name="some_measurement" src="d0_s12" to="125" unit=",0.7-9 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1809" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, shaded, rocky crevices and cliffs, usually near water courses</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky crevices" modifier="wet shaded" />
        <character name="habitat" value="cliffs" modifier="and" />
        <character name="habitat" value="water courses" modifier="usually near" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Bolandra oregana is found in northern Oregon and southern Washington in the vicinity of the Columbia River gorge and in the Snake River region of eastern Oregon and Idaho.</discussion>
  
</bio:treatment>