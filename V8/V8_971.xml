<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter S. Judd</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">497</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="D. Don" date="1834" rank="genus">PIERIS</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>17: 159. 1834  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus pieris;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin name for one of the Muses</other_info_on_name>
    <other_info_on_name type="fna_id">125410</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [trees] or vines.</text>
      <biological_entity id="o817" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o819" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs multicellular stipitate-glandular-hairy and, sometimes, conspicuously strigose (hairs elongate, stiff, long-celled, eglandular), otherwise sparsely to densely unicellular-hairy.</text>
      <biological_entity id="o820" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="sometimes; conspicuously" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="otherwise sparsely; sparsely to densely" name="pubescence" src="d0_s2" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent or deciduous, alternate, sometimes pseudoverticillate [in whorls of 3];</text>
      <biological_entity id="o821" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="pseudoverticillate" value_original="pseudoverticillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, elliptic, or slightly obovate, coriaceous, margins obscurely to clearly toothed or serrulate [entire], plane or revolute, surfaces multicellular, short-stalked stipitate-glandular-hairy (along with, in P. floribunda, stout, elongate hairs associated with marginal teeth, and unicellular-hairy on midvein adaxially);</text>
      <biological_entity id="o822" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o823" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="clearly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation brochidodromous to reticulodromous (veins of varied thickness and conspicuousness).</text>
      <biological_entity id="o824" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character char_type="range_value" from="brochidodromous" name="architecture" src="d0_s5" to="reticulodromous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary or terminal panicles or racemes, 15–35-flowered, (borne the year preceding flowering).</text>
      <biological_entity id="o825" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="15-35-flowered" value_original="15-35-flowered" />
      </biological_entity>
      <biological_entity id="o826" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o827" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: bracteoles 2, proximal or distal.</text>
      <biological_entity id="o828" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o829" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s7" value="distal" value_original="distal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 5, slightly connate, ovate [oblong-lanceolate];</text>
      <biological_entity id="o830" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o831" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" modifier="slightly" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, connate for nearly their entire lengths, white, corolla urceolate to cylindric-urceolate, lobes short;</text>
      <biological_entity id="o832" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o833" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character constraint="for nearly their entire lengths" is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o834" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="urceolate" name="shape" src="d0_s9" to="cylindric-urceolate" />
      </biological_entity>
      <biological_entity id="o835" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10, included;</text>
      <biological_entity id="o836" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o837" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments straight or geniculate, glabrous or hairy, with 2 stout, minutely papillose spurs at anther-filament junction;</text>
      <biological_entity id="o838" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o839" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o840" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="true" modifier="minutely" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="anther-filament" id="o841" name="junction" name_original="junction" src="d0_s11" type="structure" />
      <relation from="o839" id="r88" name="with" negation="false" src="d0_s11" to="o840" />
      <relation from="o840" id="r89" name="at" negation="false" src="d0_s11" to="o841" />
    </statement>
    <statement id="d0_s12">
      <text>anthers without awns, dehiscent by elliptic pores;</text>
      <biological_entity id="o842" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o843" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character constraint="by pores" constraintid="o845" is_modifier="false" name="dehiscence" notes="" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o844" name="awn" name_original="awns" src="d0_s12" type="structure" />
      <biological_entity id="o845" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <relation from="o843" id="r90" name="without" negation="false" src="d0_s12" to="o844" />
    </statement>
    <statement id="d0_s13">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o846" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o847" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 5-locular;</text>
      <biological_entity id="o848" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o849" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="5-locular" value_original="5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate.</text>
      <biological_entity id="o850" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o851" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular, subglobose or globose to ovoid, (with unthickened sutures), dry.</text>
      <biological_entity id="o852" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="ovoid" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s16" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 40–100, ellipsoidal to obovoid, ± rectangular or angular-ovoid, or narrowly conic, flattened or not, (sometimes slightly winged);</text>
      <biological_entity id="o853" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s17" to="100" />
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s17" to="obovoid more or less rectangular or angular-ovoid or narrowly conic" />
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s17" to="obovoid more or less rectangular or angular-ovoid or narrowly conic" />
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s17" to="obovoid more or less rectangular or angular-ovoid or narrowly conic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character name="shape" src="d0_s17" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>testa cells elongate or isodiametric.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 12.</text>
      <biological_entity constraint="testa" id="o854" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity constraint="x" id="o855" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, West Indies (Cuba), e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36.</number>
  <discussion>Ampelothamnus Small; Arcterica Coville; Portuna Nuttall</discussion>
  <discussion>Species 7 (2 in the flora).</discussion>
  <references>
    <reference>Judd, W. S. 1982. A taxonomic revision of Pieris (Ericaceae). J. Arnold Arbor. 63: 103–144.</reference>
    <reference>Setoguchi, H., W. Watanabe, Y. Maeda, and C. I. Peng. 2008. Molecular phylogeny of the genus Pieris (Ericaceae) with special reference to phylogenetic relationships of insular plants on the Ryukyu Islands. Pl. Syst. Evol. 270: 217–230.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences axillary racemes; capsules with placentae central to nearly basal; styles strongly sunken into ovary apex; seeds with isodiametric testa cells; filaments geniculate, glabrous; twigs multicellular stipitate-glandular-hairy.</description>
      <determination>1 Pieris phillyreifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences terminal panicles; capsules with placentae subapical; styles slightly sunken into ovary apex; seeds with elongated testa cells; filaments straight, hairy; twigs multicellular stipitate-glandular-hairy and conspicuously strigose (hairs stout, elongate).</description>
      <determination>2 Pieris floribunda</determination>
    </key_statement>
  </key>
</bio:treatment>