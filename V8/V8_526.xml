<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="D. Don" date="unknown" rank="family">theophrastaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">samolus</taxon_name>
    <taxon_name authority="Greene" date="1909" rank="species">vagans</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>7: 196. 1909  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family theophrastaceae;genus samolus;species vagans</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092187</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants light green, stoloniferous or mat-forming, 0.2–3 dm.</text>
      <biological_entity id="o12524" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually prostrate or arching.</text>
      <biological_entity id="o12525" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves often crowded near plant base, petiolate or nearly sessile;</text>
      <biological_entity id="o12526" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near plant base" constraintid="o12527" is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="plant" id="o12527" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate to broadly spatulate, 0.5–6 cm, base decurrent, broadly cuneate (or somewhat rounded), apex obtuse.</text>
      <biological_entity id="o12528" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s3" to="broadly spatulate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12529" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12530" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and axillary in distal leaves, racemose, sessile or short-pedunculate;</text>
      <biological_entity id="o12531" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character constraint="in distal leaves" constraintid="o12532" is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-pedunculate" value_original="short-pedunculate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12532" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>peduncle to 1.5 cm, shorter than to equaling stem.</text>
      <biological_entity id="o12533" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
        <character constraint="than to equaling stem" constraintid="o12534" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12534" name="stem" name_original="stem" src="d0_s5" type="structure">
        <character is_modifier="true" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels usually spreading, bracteate, 1.4–10 mm, glabrous;</text>
      <biological_entity id="o12535" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bract proximal to midlength.</text>
      <biological_entity id="o12536" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximal" name="position" src="d0_s7" to="midlength" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 1–2 mm, lobes triangular-ovate, equaling or longer than tube, apex acute, not glandular;</text>
      <biological_entity id="o12537" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12538" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12539" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-ovate" value_original="triangular-ovate" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than tube" constraintid="o12540" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12540" name="tube" name_original="tube" src="d0_s8" type="structure" />
      <biological_entity id="o12541" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla white, 1–3 mm, lobes oblong, longer than tube, base glabrous, apex rounded or slightly emarginate;</text>
      <biological_entity id="o12542" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12543" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12544" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character constraint="than tube" constraintid="o12545" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12545" name="tube" name_original="tube" src="d0_s9" type="structure" />
      <biological_entity id="o12546" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12547" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 5.</text>
      <biological_entity id="o12548" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12549" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 2–3 mm.</text>
      <biological_entity id="o12550" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet sandy places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet sandy places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Chiricahua Mountain brookweed</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>