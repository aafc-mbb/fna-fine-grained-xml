<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="(Britton &amp; Rose) Moran" date="1942" rank="subgenus">Stylophyllum</taxon_name>
    <taxon_name authority="(S. Watson) Moran" date="1943" rank="species">attenuata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">attenuata</taxon_name>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus stylophyllum;species attenuata;subspecies attenuata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092057</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">attenuata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">orcuttii</taxon_name>
    <taxon_hierarchy>genus Dudleya;species attenuata;subspecies orcuttii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stylophyllum</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">orcuttii</taxon_name>
    <taxon_hierarchy>genus Stylophyllum;species orcuttii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stylophyllum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parishii</taxon_name>
    <taxon_hierarchy>genus Stylophyllum;species parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices 0.5–2 dm × 0.3–1 [–1.5] cm, clumps 1–4 dm diam.</text>
      <biological_entity id="o3339" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="area" src="d0_s0" to="2" to_unit="dm" unit="0.5-2 dm×" />
        <character char_type="range_value" from="1" from_inclusive="false" name="average_area" src="d0_s0" to="1.5" unit="0.5-2 dm×" />
        <character char_type="range_value" from="0.3" name="area" src="d0_s0" to="1" unit="0.5-2 dm×" />
      </biological_entity>
      <biological_entity id="o3340" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="diameter" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves without resinous odor;</text>
      <biological_entity id="o3341" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rosette (lax), 5–20-leaved, 2–5 (–10) cm diam.;</text>
      <biological_entity id="o3342" name="rosette" name_original="rosette" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-20-leaved" value_original="5-20-leaved" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade often purplish, linear to linear-oblanceolate and somewhat clavate, terete or flattened adaxially towards base, 2–10 × 0.2–0.5 cm, 2–5 mm thick, to 2 times wider than thick, base 5–15 mm wide, surfaces farinose, not viscid, not oily.</text>
      <biological_entity id="o3343" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-oblanceolate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character constraint="towards base" constraintid="o3344" is_modifier="false" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character name="thickness" notes="" src="d0_s3" unit="cm" value="2-10×0.2-0.5" value_original="2-10×0.2-0.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" notes="" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="width_or_width" src="d0_s3" value="0-2 times wider than thick" />
      </biological_entity>
      <biological_entity id="o3344" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o3345" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3346" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s3" value="viscid" value_original="viscid" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s3" value="oily" value_original="oily" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: cyme 1–3-branched, obpyramidal, (2–15 (–20) × 1–10 dm);</text>
      <biological_entity id="o3347" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o3348" name="cyme" name_original="cyme" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-branched" value_original="1-3-branched" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="[2" from_unit="dm" name="length" src="d0_s4" to="15" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s4" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches simple;</text>
      <biological_entity id="o3349" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o3350" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cincinni 3–15-flowered, not circinate, 2–11 cm;</text>
      <biological_entity id="o3351" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o3352" name="cincinnus" name_original="cincinni" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-15-flowered" value_original="3-15-flowered" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s6" value="circinate" value_original="circinate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral shoots 0.5–2.5 dm × 1–3 mm;</text>
      <biological_entity id="o3353" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o3354" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s7" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaves 5–10, ascending, linear-lanceolate, (turgid), 5–3.5 × 0.2–0.5 cm.</text>
      <biological_entity id="o3355" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o3356" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s8" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.5–3 mm.</text>
      <biological_entity id="o3357" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: petals widespreading or somewhat ascending from near middle, connate 0.5–1 mm, white often flushed with rose, especially on keel, triangular-ovate, 6–10 × 1.5–3 mm, apex acute, corolla ca. 10 mm diam.;</text>
      <biological_entity id="o3358" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3359" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="widespreading" value_original="widespreading" />
        <character constraint="from near middle , connate 0.5-1 mm" is_modifier="false" modifier="somewhat" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="white often flushed with rose" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3360" name="keel" name_original="keel" src="d0_s10" type="structure" />
      <biological_entity id="o3361" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3362" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character name="diameter" src="d0_s10" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <relation from="o3359" id="r268" modifier="especially" name="on" negation="false" src="d0_s10" to="o3360" />
    </statement>
    <statement id="d0_s11">
      <text>pistils erect, 4–7 mm;</text>
      <biological_entity id="o3363" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3364" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 1–2.5 mm.</text>
      <biological_entity id="o3365" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3366" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Follicles ascending, with adaxial margins ca. 45–60º above horizontal.</text>
      <biological_entity constraint="adaxial" id="o3368" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <relation from="o3367" id="r269" modifier="above horizontal" name="with" negation="false" src="d0_s13" to="o3368" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34 [68, Mexico].</text>
      <biological_entity id="o3367" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3369" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bluffs, flats, and rocky slopes near sea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="rocky slopes" constraint="near sea" />
        <character name="habitat" value="sea" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <other_name type="common_name">Orcutt’s dudleya</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies attenuata is abundant southward in Mexico but barely enters southwestern San Diego County on the coast, where it is now rare. It is considered seriously threatened in California. Dudleya attenuata subsp. orcuttii (Rose) Moran was thought to be distinct in having white or rose-tinged petals; subsequent field work showed that some populations include plants with yellow petals (R. V. Moran 2001). Stylophyllum parishii was supposedly from Pala, 90 kilometers to the north of the southwestern San Diego County population of subsp. attenuata, and was based on cultivated plants that apparently were mislabeled (Moran 1943).</discussion>
  <discussion>In northwest Baja California subsp. attenuata hybridizes with Dudleya brittonii, D. edulis, D. formosa, and D. variegata; all the hybrids apparently are rare.</discussion>
  
</bio:treatment>