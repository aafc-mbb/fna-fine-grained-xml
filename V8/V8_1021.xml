<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="treatment_page">524</other_info_on_meta>
    <other_info_on_meta type="illustration_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">vaccinium</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Myrtillus</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1817" rank="species">ovalifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>36: Vaccinium no. 2. 1817,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus vaccinium;section myrtillus;species ovalifolium;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250065711</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vaccinium</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">alaskaense</taxon_name>
    <taxon_hierarchy>genus Vaccinium;species alaskaense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants crown-forming, clumped, suckering when disturbed, rarely forming extensive colonies, 3–40 dm, not rhizomatous;</text>
      <biological_entity id="o3941" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="clumped" value_original="clumped" />
        <character is_modifier="false" modifier="when disturbed" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="3" from_unit="dm" modifier="rarely" name="some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3942" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="extensive" value_original="extensive" />
      </biological_entity>
      <relation from="o3941" id="r326" modifier="rarely" name="forming" negation="false" src="d0_s0" to="o3942" />
    </statement>
    <statement id="d0_s1">
      <text>twigs yellow-green or golden brown, glaucous, usually terete, sometimes somewhat angled, glabrous, sometimes hairy in lines.</text>
      <biological_entity id="o3943" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="golden brown" value_original="golden brown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="sometimes somewhat" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="in lines" constraintid="o3944" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3944" name="line" name_original="lines" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades pale green or glaucous abaxially, slightly darker pale green adaxially, ovate to elliptic, rarely obovate, 25–39 × 16–20 mm, margins entire to obscurely serrate, abaxial surface glabrous, eglandular (sometimes hairy or glandular along midvein), adaxial surface usually glabrous (sometimes hairy and/or glandular).</text>
      <biological_entity id="o3945" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="slightly" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="darker pale" value_original="darker pale" />
        <character is_modifier="false" modifier="slightly; adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s2" to="39" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3946" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s2" to="obscurely serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3947" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3948" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: calyx pale green or glaucous, lobes vestigial or absent, glabrous;</text>
      <biological_entity id="o3949" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o3950" name="calyx" name_original="calyx" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o3951" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla pink, bronze-pink, or greenish white, globose, sometimes urceolate, 5–7 × 4–5 mm, thin, glaucous;</text>
      <biological_entity id="o3952" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o3953" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="bronze-pink" value_original="bronze-pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="bronze-pink" value_original="bronze-pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="shape" src="d0_s4" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments glabrous or pilose basally.</text>
      <biological_entity id="o3954" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3955" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Berries blue, dull purplish black, or black, sometimes glaucous, 8–10 mm diam.</text>
      <biological_entity id="o3956" name="berry" name_original="berries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="black" value_original="black" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds ca. 1 mm. 2n = 24, 48.</text>
      <biological_entity id="o3957" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3958" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="24" value_original="24" />
        <character name="quantity" src="d0_s7" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring-mid summer(-late summer).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist or mesic coniferous woods, transitional habitats adjacent to these coniferous stands, cut-over coniferous woods, verges of road cuts, margins of coniferous woods, peaty slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="mesic coniferous woods" />
        <character name="habitat" value="transitional habitats" />
        <character name="habitat" value="these coniferous stands" />
        <character name="habitat" value="cut-over coniferous" />
        <character name="habitat" value="adjacent to these coniferous stands" />
        <character name="habitat" value="cut-over coniferous woods" />
        <character name="habitat" value="verges" constraint="of road cuts" />
        <character name="habitat" value="road cuts" />
        <character name="habitat" value="margins" constraint="of coniferous woods , peaty slopes" />
        <character name="habitat" value="coniferous woods" />
        <character name="habitat" value="peaty slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Nfld. and Labr. (Nfld.), N.S., Ont., Que., Yukon; Alaska, Idaho, Mich., Oreg., S.Dak., Wash.; e Asia (c Japan, Kamchatka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="e Asia (c Japan)" establishment_means="native" />
        <character name="distribution" value="e Asia (Kamchatka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Early blueberry</other_name>
  <other_name type="common_name">oval-leaf huckleberry</other_name>
  <other_name type="common_name">airelle à feuilles ovées</other_name>
  
</bio:treatment>