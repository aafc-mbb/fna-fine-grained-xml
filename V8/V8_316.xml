<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">157</other_info_on_meta>
    <other_info_on_meta type="illustration_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">kalanchoë</taxon_name>
    <taxon_name authority="(Medikus) Kuntze" date="1891" rank="species">integra</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>1: 229. 1891,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus kalanchoë;species integra</taxon_hierarchy>
    <other_info_on_name type="fna_id">241000241</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotyledon</taxon_name>
    <taxon_name authority="Medikus" date="unknown" rank="species">integra</taxon_name>
    <place_of_publication>
      <publication_title>Hist. &amp; Commentat. Acad. Elect. Sci. Theod.-Palat.</publication_title>
      <place_in_publication>3: 200, plate 9. 1775</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotyledon;species integra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalanchoë</taxon_name>
    <taxon_name authority="Welwitsch ex Oliver" date="unknown" rank="species">coccinea</taxon_name>
    <taxon_hierarchy>genus Kalanchoë;species coccinea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or glandular-pubescent distally.</text>
      <biological_entity id="o24082" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–20 dm.</text>
      <biological_entity id="o24083" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades obovate, 5–20 cm, margins shallowly and irregularly crenate, apex obtuse.</text>
      <biological_entity id="o24084" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="distance" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24085" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o24086" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cymes often lax, compound, glandular-pubescent.</text>
      <biological_entity id="o24087" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 0.5–2 cm.</text>
      <biological_entity id="o24088" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers fragrant;</text>
      <biological_entity id="o24089" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="odor" src="d0_s5" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals ascending, triangular-lanceolate, 4–10 mm;</text>
      <biological_entity id="o24090" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla yellow or red-orange, flask-shaped (inflated on abaxial side), tube 15–16 mm, lobes spreading, 8–10 mm.</text>
      <biological_entity id="o24091" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flask--shaped" value_original="flask--shaped" />
      </biological_entity>
      <biological_entity id="o24092" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24093" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hummocks, waste places, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hummocks" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; South America (Brazil); Asia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Kalanchoë integra is sometimes included in K. laciniata. In any case it is a difficult complex needing more study.</discussion>
  
</bio:treatment>