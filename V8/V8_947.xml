<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">kalmia</taxon_name>
    <taxon_name authority="(Hooker) A. Heller" date="1898" rank="species">microphylla</taxon_name>
    <taxon_name authority="(Small) Ebinger" date="1974" rank="variety">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>76: 340. 1974  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus kalmia;species microphylla;variety occidentalis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065672</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalmia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>29: 53. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Kalmia;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalmia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">microphylla</taxon_name>
    <taxon_name authority="(Small) Roy L. Taylor &amp; MacBryde" date="unknown" rank="subspecies">occidentalis</taxon_name>
    <taxon_hierarchy>genus Kalmia;species microphylla;subspecies occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs not compact, 0.2–0.6 (–0.8) m;</text>
      <biological_entity id="o11954" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.8" to_unit="m" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>twig nodes mostly 1+ cm apart.</text>
      <biological_entity constraint="twig" id="o11955" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate, 2–4 cm, 2.5–4 times as long as wide.</text>
      <biological_entity id="o11956" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="2.5-4" value_original="2.5-4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: calyx light pink, 7–10 mm diam.;</text>
      <biological_entity id="o11957" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o11958" name="calyx" name_original="calyx" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light pink" value_original="light pink" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla rose-purple, 15–20 mm diam.</text>
      <biological_entity id="o11959" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o11960" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="rose-purple" value_original="rose-purple" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open bogs, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open bogs" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <other_name type="common_name">Western swamp laurel</other_name>
  <discussion>Variety occidentalis and Kalmia polifolia are strikingly similar. Both have the same general habit and size and are very similar in most morphological characteristics. These taxa are easily separated by the revolute leaf margins and small stalked glands along the leaf midrib in K. polifolia, which are lacking in var. occidentalis (J. E. Ebinger 1974). Hybrids between them are sterile (R. A. Jaynes 1988).</discussion>
  
</bio:treatment>