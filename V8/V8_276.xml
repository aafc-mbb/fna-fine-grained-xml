<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Sternberg" date="1822" rank="species">eschscholtzii</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Saxifrag., suppl.</publication_title>
      <place_in_publication>1: 9. 1822  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species eschscholtzii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092012</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cushion-forming, rounded, sometimes mat-forming (then stems trailing), (stems slightly woody at base), not stoloniferous, rhizomatous, with caudex.</text>
      <biological_entity id="o1239" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cushion-forming" value_original="cushion-forming" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1240" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <relation from="o1239" id="r120" name="with" negation="false" src="d0_s0" to="o1240" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal (sometimes cauline on trailing stems), (closely imbricate);</text>
      <biological_entity id="o1241" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole absent;</text>
      <biological_entity id="o1242" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade round to broadly obovate, unlobed, 1–4 mm, ± fleshy, margins entire, stiffly bristly-ciliate, apex obtuse or rounded, surfaces glabrous.</text>
      <biological_entity id="o1243" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s3" to="broadly obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o1244" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="stiffly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="bristly-ciliate" value_original="bristly-ciliate" />
      </biological_entity>
      <biological_entity id="o1245" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o1246" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences solitary flowers, to 1 cm, glabrous;</text>
      <biological_entity id="o1247" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1248" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts sessile.</text>
      <biological_entity id="o1249" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers unisexual (plants dioecious);</text>
      <biological_entity id="o1250" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals spreading in flower, reflexed in fruit, (purple), triangular to ovate, margins ciliate, surfaces hairy;</text>
      <biological_entity id="o1251" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in flower" constraintid="o1252" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="in fruit" constraintid="o1253" is_modifier="false" name="orientation" notes="" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="triangular" name="shape" notes="" src="d0_s7" to="ovate" />
      </biological_entity>
      <biological_entity id="o1252" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <biological_entity id="o1253" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o1254" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1255" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, drying cream to brown, not spotted, linear to oblanceolate, 1–2.5 mm, ± equaling sepals;</text>
      <biological_entity id="o1256" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="drying" value_original="drying" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s8" to="brown" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1257" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary superior.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 12.</text>
      <biological_entity id="o1258" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1259" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and gravelly spits, rocky outcrops, gravel shores, tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="gravelly spits" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="gravel shores" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Asia (Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Cushion saxifrage</other_name>
  
</bio:treatment>