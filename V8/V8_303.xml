<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">154</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crassula</taxon_name>
    <taxon_name authority="(Rose) M. Bywater &amp; Wickens" date="1984" rank="species">longipes</taxon_name>
    <place_of_publication>
      <publication_title>Kew Bull.</publication_title>
      <place_in_publication>39: 712. 1984,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus crassula;species longipes</taxon_hierarchy>
    <other_info_on_name type="fna_id">250013857</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tillaeastrum</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">longipes</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>13: 301. 1911 (as Tilleastrum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tillaeastrum;species longipes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants aquatic or terrestrial, annual.</text>
      <biological_entity id="o11862" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading (in water) or erect, yellow in age, branched, to 25 cm if aquatic, to 5 cm if terrestrial.</text>
      <biological_entity id="o11863" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in age" constraintid="o11864" is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" constraint="if aquatic" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s1" value="terrestrial" value_original="terrestrial" />
      </biological_entity>
      <biological_entity id="o11864" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate to oblanceolate, 2–5 mm, apex acute to obtuse.</text>
      <biological_entity id="o11865" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11866" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences lax;</text>
      <biological_entity id="o11867" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>flowers 1 per node.</text>
      <biological_entity id="o11868" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o11869" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11869" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–8 mm.</text>
      <biological_entity id="o11870" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 4-merous;</text>
      <biological_entity id="o11871" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals triangular-ovate, 0.4–0.7 mm, apex obtuse;</text>
      <biological_entity id="o11872" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11873" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals lanceolate, 1.1–1.7 mm.</text>
      <biological_entity id="o11874" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Follicles ascending, 12–14-seeded, obliquely lanceoloid;</text>
      <biological_entity id="o11875" name="follicle" name_original="follicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="12-14-seeded" value_original="12-14-seeded" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s9" value="lanceoloid" value_original="lanceoloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>old follicles spreading, flat.</text>
      <biological_entity id="o11876" name="follicle" name_original="follicles" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="old" value_original="old" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds oblong with rounded ends, 0.2–0.4 × 0.1–0.2 mm, not papillate, dull, rugulose.</text>
      <biological_entity id="o11877" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character constraint="with ends" constraintid="o11878" is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" notes="" src="d0_s11" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" notes="" src="d0_s11" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="dull" value_original="dull" />
        <character is_modifier="false" name="relief" src="d0_s11" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o11878" name="end" name_original="ends" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Aquatic or in wet sand or mud</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sand" modifier="aquatic or in" />
        <character name="habitat" value="mud" />
        <character name="habitat" value="aquatic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., La., Miss., Tex.; Mexico; s South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>M. Bywater and G. E. Wickens (1984) showed Crassula longipes in southern Florida, but according to D. B. Ward (pers. comm.), the record is probably based on a W. M. Carpenter collection from East Feliciana Parish, Louisiana.</discussion>
  
</bio:treatment>