<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">arctostaphylos</taxon_name>
    <taxon_name authority="Merriam" date="1918" rank="species">mewukka</taxon_name>
    <taxon_name authority="(W. Knight) P. V. Wells" date="1992" rank="subspecies">truei</taxon_name>
    <place_of_publication>
      <publication_title>Four Seasons</publication_title>
      <place_in_publication>9(2): 62. 1992  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus arctostaphylos;species mewukka;subspecies truei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092339</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctostaphylos</taxon_name>
    <taxon_name authority="W. Knight" date="unknown" rank="species">truei</taxon_name>
    <place_of_publication>
      <publication_title>Four Seasons</publication_title>
      <place_in_publication>3(1): 19, fig. p. 20. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arctostaphylos;species truei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants: burl absent;</text>
      <biological_entity id="o4265" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4266" name="burl" name_original="burl" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>twigs glabrous or glandular-hairy.</text>
      <biological_entity id="o4267" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4268" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 10–15 mm;</text>
      <biological_entity id="o4269" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4270" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade white-glaucous, orbiculate to orbiculate-ovate, 4–7 cm wide, apex obtuse.</text>
      <biological_entity id="o4271" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4272" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="white-glaucous" value_original="white-glaucous" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s3" to="orbiculate-ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>2n = 52.</text>
      <biological_entity id="o4273" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4274" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral openings in mixed conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral openings" constraint="in mixed conifer forests" />
        <character name="habitat" value="mixed conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15b.</number>
  <other_name type="common_name">True’s manzanita</other_name>
  <discussion>Subspecies truei is found in the vicinity of Paradise in the northern Sierra Nevada, Butte County.</discussion>
  
</bio:treatment>