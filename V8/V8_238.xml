<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Elizabeth Fortson Wells,Patrick E. Elvander†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="genus">BOLANDRA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 341. 1868  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus BOLANDRA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Henry Nicholas Bolander, 1831–1897, physician and collector for California State Geological Survey</other_info_on_name>
    <other_info_on_name type="fna_id">104179</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, not rhizomatous or stoloniferous;</text>
      <biological_entity id="o18155" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex erect, slender, bearing bulbils at base.</text>
      <biological_entity id="o18156" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o18157" name="bulbil" name_original="bulbils" src="d0_s1" type="structure" />
      <biological_entity id="o18158" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o18156" id="r1522" name="bearing" negation="false" src="d0_s1" to="o18157" />
      <relation from="o18156" id="r1523" name="at" negation="false" src="d0_s1" to="o18158" />
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems erect to ascending, leafy, 10–40 cm, stipitate-glandular.</text>
      <biological_entity id="o18159" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in basal rosette and cauline;</text>
      <biological_entity id="o18160" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18161" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o18160" id="r1524" name="in" negation="false" src="d0_s3" to="o18161" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves conspicuous, reduced and bractlike distally;</text>
      <biological_entity constraint="cauline" id="o18162" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules present;</text>
      <biological_entity id="o18163" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole glabrous;</text>
      <biological_entity id="o18164" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade reniform to orbiculate, 5–13-lobed, base cordate, ultimate margins irregularly serrate to crenate-dentate, apex obtuse to acute, surfaces glabrous;</text>
      <biological_entity id="o18165" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s7" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="5-13-lobed" value_original="5-13-lobed" />
      </biological_entity>
      <biological_entity id="o18166" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18167" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s7" value="serrate to crenate-dentate" value_original="serrate to crenate-dentate" />
      </biological_entity>
      <biological_entity id="o18168" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation palmate.</text>
      <biological_entity id="o18169" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences compound, dichasial cymes, terminal from terminal bud in basal rosette, 5–18-flowered, bracteate.</text>
      <biological_entity id="o18170" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o18171" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="dichasial" value_original="dichasial" />
        <character constraint="from terminal bud" constraintid="o18172" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="5-18-flowered" value_original="5-18-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o18172" name="bud" name_original="bud" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o18173" name="rosette" name_original="rosette" src="d0_s9" type="structure" />
      <relation from="o18172" id="r1525" name="in" negation="false" src="d0_s9" to="o18173" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: hypanthium free from ovary, greenish to purple;</text>
      <biological_entity id="o18174" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18175" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character constraint="from ovary" constraintid="o18176" is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character char_type="range_value" from="greenish" name="coloration" notes="" src="d0_s10" to="purple" />
      </biological_entity>
      <biological_entity id="o18176" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, greenish to purple, (narrowly triangular to triangular-ovate);</text>
      <biological_entity id="o18177" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18178" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s11" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, greenish with purple margins or reddish purple to dark purple;</text>
      <biological_entity id="o18179" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18180" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character constraint="with margins" constraintid="o18181" is_modifier="false" name="coloration" src="d0_s12" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="reddish purple" name="coloration_or_density" src="d0_s12" to="dark purple" />
      </biological_entity>
      <biological_entity id="o18181" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary tissue not seen;</text>
      <biological_entity id="o18182" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectary" id="o18183" name="tissue" name_original="tissue" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 5;</text>
      <biological_entity id="o18184" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o18185" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments filiform;</text>
      <biological_entity id="o18186" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o18187" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary nearly superior, 2-locular, ovaries connate 1/4–1/2 their lengths;</text>
      <biological_entity id="o18188" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o18189" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s16" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="2-locular" value_original="2-locular" />
      </biological_entity>
      <biological_entity id="o18190" name="ovary" name_original="ovaries" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="lengths" src="d0_s16" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>placentation axile;</text>
      <biological_entity id="o18191" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s17" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 2;</text>
      <biological_entity id="o18192" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o18193" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 2.</text>
      <biological_entity id="o18194" name="flower" name_original="flowers" src="d0_s19" type="structure" />
      <biological_entity id="o18195" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Capsules 2-beaked.</text>
      <biological_entity id="o18196" name="capsule" name_original="capsules" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="2-beaked" value_original="2-beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds dark-brown, prismatic (or angular) and fusiform, minutely tuberculate.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 7.</text>
      <biological_entity id="o18197" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="shape" src="d0_s21" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s21" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="x" id="o18198" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Both the monograph by R. J. Gornall and B. A. Bohm (1985) and the molecular systematic work of D. E. Soltis et al. (1993) support a close relationship among Bolandra, Boykinia, and Suksdorfia. Other genera that appear to be closely related to these are Jepsonia, Sullivantia, and Telesonix (Soltis et al.).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades deeply lobed, ultimate margins crenate-dentate; ovaries connate proximally 1/2 their lengths; petals usually greenish with purple margins, 4-5(-7) mm.</description>
      <determination>1 Bolandra californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades shallowly lobed, ultimate margins serrate; ovaries connate proximally 1/4 their lengths; petals usually reddish purple to dark purple, 7-12(-14) mm.</description>
      <determination>2 Bolandra oregana</determination>
    </key_statement>
  </key>
</bio:treatment>