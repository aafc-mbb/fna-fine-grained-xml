<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">344</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle &amp; Sprengel" date="unknown" rank="family">styracaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">styrax</taxon_name>
    <taxon_name authority="Engelmann ex Torrey" date="1853" rank="species">platanifolius</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>6(4): 4. 1853 (as platinifolium),</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family styracaceae;genus styrax;species platanifolius</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092271</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 6 m, not suckering from roots.</text>
      <biological_entity id="o856" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character constraint="from roots" constraintid="o857" is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o857" name="root" name_original="roots" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 6–20 mm;</text>
      <biological_entity id="o858" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o859" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade with 5–6 secondary-veins, depressed-orbiculate or broadly ovate, 4.5–9 (–12) × 4.2–9 (–11.5) cm, margins entire, coarsely toothed, or 3-lobed, longest arms of abaxial hairs to 1 mm.</text>
      <biological_entity id="o860" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o861" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="depressed-orbiculate" value_original="depressed-orbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="4.2" from_unit="cm" name="width" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o862" name="secondary-vein" name_original="secondary-veins" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o863" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="longest" id="o864" name="arm" name_original="arms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o865" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <relation from="o861" id="r91" name="with" negation="false" src="d0_s2" to="o862" />
      <relation from="o864" id="r92" name="part_of" negation="false" src="d0_s2" to="o865" />
    </statement>
    <statement id="d0_s3">
      <text>False-terminal inflorescences 2–7-flowered or solitary flower, 2–5 cm;</text>
      <biological_entity constraint="false-terminal" id="o866" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-7-flowered" value_original="2-7-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o867" name="flower" name_original="flower" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>axillary flowers absent.</text>
      <biological_entity constraint="axillary" id="o868" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 4–9 mm, 1.3–2.3 times as long as calyx.</text>
      <biological_entity id="o869" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
        <character constraint="calyx" constraintid="o870" is_modifier="false" name="length" src="d0_s5" value="1.3-2.3 times as long as calyx" />
      </biological_entity>
      <biological_entity id="o870" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 3–5 (–6) × 4.5–5.5 mm;</text>
      <biological_entity id="o871" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o872" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s6" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 12–21 mm, tube 3–4 mm, lobes 5–6, imbricate in bud, slightly reflexed, elliptic, 11–18 × 3–7 mm;</text>
      <biological_entity id="o873" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o874" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o875" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o876" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="6" />
        <character constraint="in bud" constraintid="o877" is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" notes="" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o877" name="bud" name_original="bud" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments connate 1–6 mm beyond adnation to corolla.</text>
      <biological_entity id="o878" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o879" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" constraint="beyond adnation" constraintid="o880" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o880" name="adnation" name_original="adnation" src="d0_s8" type="structure" />
      <biological_entity id="o881" name="corolla" name_original="corolla" src="d0_s8" type="structure" />
      <relation from="o880" id="r93" name="to" negation="false" src="d0_s8" to="o881" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules globose, 7–10 × 7–11 mm (broader when 2–3-seeded), grayish white stellate-pubescent, dehiscent nearly or completely to proximal end, broadly exposing seed (s);</text>
      <biological_entity id="o882" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="grayish white" value_original="grayish white" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character constraint="to proximal end" constraintid="o883" is_modifier="false" modifier="nearly" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o883" name="end" name_original="end" src="d0_s9" type="structure" />
      <biological_entity id="o884" name="seed" name_original="seed" src="d0_s9" type="structure" />
      <relation from="o882" id="r94" modifier="broadly" name="exposing" negation="false" src="d0_s9" to="o884" />
    </statement>
    <statement id="d0_s10">
      <text>fruit wall 0.3–0.5 mm thick.</text>
      <biological_entity constraint="fruit" id="o885" name="wall" name_original="wall" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="thickness" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; n Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Subspecies 5 (4 in the flora).</discussion>
  <discussion>The treatment presented here is based on the morphological and molecular analyses of P. W. Fritsch (1996, 1997).</discussion>
  <discussion>The only subspecies of Styrax platanifolius to occur outside the flora area is subsp. mollis P. W. Fritsch, from Nuevo León and Tamaulipas, Mexico. It grows in wooded canyons along the eastern escarpment of the Sierra Madre Oriental. All five subspecies are narrow endemics; of the four subspecies in the flora, three (subspp. platanifolius, stellatus, and texanus) occur only in the Edwards Plateau region of Texas; the fourth (subsp. youngiae) is known only from the Davis Mountains of western Texas and from northern Coahuila, Mexico.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades with abaxial surface nearly glabrous or visible through pubescence</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades with abaxial surface completely covered and obscured by pubescence</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades glabrous except for scattered, orange-brown or dark-brown, stalked, stellate hairs abaxially on some; pedicels and calyces glabrous.</description>
      <determination>4a Styrax platanifolius subsp. platanifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades with scattered, white, stellate pubescence in addition to scattered, orange-brown or dark-brown, stalked, stellate hairs abaxially on some; pedicels and calyces with white, stellate hairs.</description>
      <determination>4b Styrax platanifolius subsp. stellatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Young twigs faintly glaucous; leaf blades glabrous adaxially; styles pubescent from proximal end to ca. 15-35% of total length; abaxial surface of leaf blades, pedicels, and calyces white stellate-tomentose.</description>
      <determination>4c Styrax platanifolius subsp. texanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Young twigs stellate-pubescent; leaf blades stellate-pubescent adaxially; styles pubescent from proximal end to ca. 60-80% of total length; abaxial surface of leaf blades, pedicels, and calyces white stellate-lanate.</description>
      <determination>4d Styrax platanifolius subsp. youngiae</determination>
    </key_statement>
  </key>
</bio:treatment>