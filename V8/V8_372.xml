<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">185</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="species">abramsii</taxon_name>
    <taxon_name authority="(Bartel &amp; Shevock) Moran" date="2007" rank="subspecies">costatifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 1015. 2007 (as costafolia)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species abramsii;subspecies costatifolia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092077</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="(Lemaire) Britton &amp; Rose" date="unknown" rank="species">cymosa</taxon_name>
    <taxon_name authority="Bartel &amp; Shevock" date="unknown" rank="subspecies">costatifolia</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>12: 701, fig. 1. 1990 (as costafolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dudleya;species cymosa;subspecies costatifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices branching, 1.5–2 cm diam.</text>
      <biological_entity id="o19008" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes 5–40;</text>
      <biological_entity id="o19009" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19010" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear, subterete, 4–8 × 0.2–0.8 cm.</text>
      <biological_entity id="o19011" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19012" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: floral shoots 5–15 (–20) × 0.1–0.4 cm;</text>
      <biological_entity id="o19013" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="floral" id="o19014" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximalmost leaf-blades 4–20 mm;</text>
      <biological_entity id="o19015" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="proximalmost" id="o19016" name="blade-leaf" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 2–4, 1–3 times bifurcate.</text>
      <biological_entity id="o19017" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o19018" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="shape" src="d0_s5" value="1-3 times bifurcate" value_original="1-3 times bifurcate " />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 3–9 (–17) mm.</text>
      <biological_entity id="o19019" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 4–6 × 4–6 mm;</text>
      <biological_entity id="o19020" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19021" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals connate 1–3 mm, bright-yellow, not lineolate, 5–13 (–17) × 2–4 mm, tips spreading.</text>
      <biological_entity id="o19022" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19023" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s8" value="lineolate" value_original="lineolate" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 34.</text>
      <biological_entity id="o19024" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19025" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Xeric limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="xeric limestone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9g.</number>
  <other_name type="common_name">Pierpont Springs liveforever</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies costatifolia is known from one southwest-facing outcrop of pre-Cretaceous limestone just west of Pierpoint Springs, Tulare County, in the southern Sierra Nevada, in an area that has been subdivided; it is considered to be fairly threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi).</discussion>
  <discussion>Subspecies costatifolia forms clumps to 10 cm in diameter. The bright yellow corolla and more-branching cyme and sometimes longer pedicels set it apart from the other subspecies of Dudleya abramsii and approach D. cymosa, where the original authors placed it. The cespitose small rosettes of narrow leaves have much more the aspect of D. abramsii, especially recalling the lowland subsp. bettinae, and the higher insertion of the antisepalous stamens is a mark of D. abramsii (and the related D. parva in contrast to D. cymosa and other species). The branching cyme is somewhat approached in subsp. calcicola, which grows just to the north.</discussion>
  
</bio:treatment>