<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">myrsinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lysimachia</taxon_name>
    <taxon_name authority="Torrey in H. B. Croom" date="1837" rank="species">loomisii</taxon_name>
    <place_of_publication>
      <publication_title>in H. B. Croom, Catal. Pl. New Bern</publication_title>
      <place_in_publication>1837, 46. 1837 (as Lisymachia) ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrsinaceae;genus lysimachia;species loomisii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092255</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">stricta</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Lysimachia;species stricta;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="(Linnaeus) Britton, Sterns &amp; Poggenberg" date="unknown" rank="species">terrestris</taxon_name>
    <taxon_name authority="(Chapman) Handel-Mazzetti" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Lysimachia;species terrestris;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, much-branched, 3–7 dm, glabrous or sparsely stipitate-glandular, especially distally;</text>
      <biological_entity id="o22575" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes slender;</text>
      <biological_entity id="o22576" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o22577" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually opposite (to subopposite or whorled);</text>
      <biological_entity id="o22578" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o22579" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to narrowly elliptic, 2–6 × 0.1–0.8 cm, base truncate or cuneate, slightly decurrent, margins entire, revolute, eciliolate, apex acute to obtuse, surfaces punctate, glabrous;</text>
      <biological_entity id="o22581" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o22582" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o22583" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o22584" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <relation from="o22580" id="r1886" name="linear to narrowly" negation="false" src="d0_s5" to="o22581" />
      <relation from="o22580" id="r1887" name="linear to narrowly" negation="false" src="d0_s5" to="o22582" />
      <relation from="o22580" id="r1888" name="linear to narrowly" negation="false" src="d0_s5" to="o22583" />
      <relation from="o22580" id="r1889" name="linear to narrowly" negation="false" src="d0_s5" to="o22584" />
    </statement>
    <statement id="d0_s6">
      <text>venation single-veined.</text>
      <biological_entity id="o22580" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="single-veined" value_original="single-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, racemes, 3–10 cm.</text>
      <biological_entity id="o22585" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o22586" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.7–1.1 cm, glabrous to sparsely stipitate-glandular.</text>
      <biological_entity id="o22587" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s8" to="sparsely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, calyx streaked with black resin canals, 3–6 mm, slightly stipitate-glandular at least marginally, lobes lanceolate, margins thin;</text>
      <biological_entity id="o22588" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22589" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o22590" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character constraint="with resin canals" constraintid="o22591" is_modifier="false" name="coloration" src="d0_s9" value="streaked" value_original="streaked" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; at-least marginally; marginally" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="resin" id="o22591" name="canal" name_original="canals" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o22592" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o22593" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, corolla yellow with reddish base, streaked with black or reddish-black resin canals, rotate, 4–7 mm, lobes with margins entire, apex rounded, stipitate-glandular adaxially;</text>
      <biological_entity id="o22594" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22595" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o22596" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="with base" constraintid="o22597" is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character constraint="with resin canals" constraintid="o22598" is_modifier="false" name="coloration" notes="" src="d0_s10" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22597" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="resin" id="o22598" name="canal" name_original="canals" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="reddish-black" value_original="reddish-black" />
      </biological_entity>
      <biological_entity id="o22599" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o22600" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22601" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o22599" id="r1890" name="with" negation="false" src="d0_s10" to="o22600" />
    </statement>
    <statement id="d0_s11">
      <text>filaments connate 0.4–1 mm, shorter than corolla;</text>
      <biological_entity id="o22602" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22603" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character constraint="than corolla" constraintid="o22604" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22604" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent.</text>
      <biological_entity id="o22605" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o22606" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 3–3.5 mm, not punctate, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 42.</text>
      <biological_entity id="o22607" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s13" value="punctate" value_original="punctate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22608" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine savannas, moist roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="moist roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Loomis’s loosestrife</other_name>
  <discussion>A rare hybrid (known only from one population in Washington County, North Carolina) of Lysimachia loomisii with L. quadrifolia has been called L. ×radfordii H. E. Ahles.</discussion>
  
</bio:treatment>