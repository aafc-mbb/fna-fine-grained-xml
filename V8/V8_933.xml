<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="treatment_page">478</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Salisbury" date="1806" rank="genus">phyllodoce</taxon_name>
    <taxon_name authority="(Smith) D. Don" date="1834" rank="species">empetriformis</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>17: 160. 1834  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus phyllodoce;species empetriformis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065661</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Menziesia</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">empetriformis</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>10: 380. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Menziesia;species empetriformis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate to erect, diffusely branched, 0.5–5 dm;</text>
      <biological_entity id="o23879" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="diffusely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>young branches sparsely glandular.</text>
      <biological_entity id="o23880" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending, densely imbricate;</text>
      <biological_entity id="o23881" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear, 4–15 × 1–1.5 (–2) mm, margins entire or glandular-serrulate, surfaces usually glabrous.</text>
      <biological_entity id="o23882" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23883" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="glandular-serrulate" value_original="glandular-serrulate" />
      </biological_entity>
      <biological_entity id="o23884" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences corymbiform, 1–14-flowered.</text>
      <biological_entity id="o23885" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-14-flowered" value_original="1-14-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 5–25 mm, glandular;</text>
      <biological_entity id="o23886" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles 2.</text>
      <biological_entity id="o23887" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers usually erect;</text>
      <biological_entity id="o23888" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals broadly ovate, 2–3 mm, margins ciliate, abaxial surface glabrous;</text>
      <biological_entity id="o23889" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23890" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23891" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pink, campanulate, not constricted at mouth, 5–8 mm, not glandular, lobes spreading, 1.5–2.5 mm;</text>
      <biological_entity id="o23892" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="at mouth" constraintid="o23893" is_modifier="false" modifier="not" name="size" src="d0_s9" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o23893" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <biological_entity id="o23894" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10, included;</text>
      <biological_entity id="o23895" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1.5–3 mm, glabrous;</text>
      <biological_entity id="o23896" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1.5–2.5 mm;</text>
      <biological_entity id="o23897" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary broadly ovoid, 1.5–2 mm, glandular;</text>
      <biological_entity id="o23898" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style exserted, 5–7 mm.</text>
      <biological_entity id="o23899" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 5-valved, globose, 3–4 mm, glandular.</text>
      <biological_entity id="o23900" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-valved" value_original="5-valved" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s15" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist subalpine and alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist subalpine" />
        <character name="habitat" value="alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Ariz., Calif., Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Pink mountain heather</other_name>
  <discussion>Hybrids between Phyllodoce empetriformis and P. glanduliflora are encountered occasionally where the two species occur together. The hybrids, P. ×intermedia (Hooker) Rydberg, consisting largely of first-generation crosses (F1 progeny), have a decidedly intermediate floral morphology, combining glandular, mostly nonciliate sepals more than 3 mm long and pinkish, cylindric to ovoid corollas.</discussion>
  
</bio:treatment>