<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 15:24:17</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">334</other_info_on_meta>
    <other_info_on_meta type="illustration_page">327</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">diapensiaceae</taxon_name>
    <taxon_name authority="Sims" date="1804" rank="genus">galax</taxon_name>
    <taxon_name authority="(Poiret) Brummitt" date="1972" rank="species">urceolata</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>21: 309. 1972,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family diapensiaceae;genus galax;species urceolata</taxon_hierarchy>
    <other_info_on_name type="fna_id">220005401</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrola</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">urceolata</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>5: 743. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pyrola;species urceolata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes forming dense groundcover.</text>
      <biological_entity id="o68" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o69" name="groundcover" name_original="groundcover" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o68" id="r9" name="forming" negation="false" src="d0_s0" to="o69" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 5–25 cm;</text>
      <biological_entity id="o70" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o71" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 30–100 mm, base cordate.</text>
      <biological_entity id="o72" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o73" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s2" to="100" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o74" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scapes 20–40 (–80) cm.</text>
      <biological_entity id="o75" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="80" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyces 1.5–2.5 mm.</text>
      <biological_entity id="o76" name="calyx" name_original="calyces" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsules 2.5–3 mm. 2n = 6, 12.</text>
      <biological_entity id="o77" name="capsule" name_original="capsules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o78" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="6" value_original="6" />
        <character name="quantity" src="d0_s5" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountains, rocky woodlands, bald margins, rock outcrops and crevices, stream banks, hemlock to oak-hickory forests, piedmont, coastal plain, usually bluffs or moist slopes along streams, sometimes beech forests, associated with Kalmia latifolia or Rhododendron maximum</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountains" />
        <character name="habitat" value="rocky woodlands" />
        <character name="habitat" value="bald margins" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="hemlock to oak-hickory forests" />
        <character name="habitat" value="piedmont" />
        <character name="habitat" value="coastal plain" />
        <character name="habitat" value="bluffs" modifier="usually" constraint="along streams" />
        <character name="habitat" value="moist slopes" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="beech forests" modifier="sometimes" />
        <character name="habitat" value="kalmia latifolia" modifier="associated with" />
        <character name="habitat" value="rhododendron maximum" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(20-)500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1800" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Md., Mass., N.Y., N.C., Ohio, S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Galax recorded from Massachusetts and New York apparently are naturalized or persistent garden escapes.</discussion>
  <discussion>Diploid populations of Galax urceolata occur over most of its range—throughout the montane regions and onto the piedmont of South Carolina and the southern piedmont and coastal plain of North Carolina. Tetraploids occur exclusive of diploids on the coastal plain of Virginia and the northern piedmont and coastal plain of North Carolina. Along the Blue Ridge escarpment (North Carolina southward), the two cytotypes occur in complex patterns of sympatry, and triploids are abundant, even in some sites where plants apparently are otherwise only diploid or only tetraploid (G. L. Nesom 1983c; T. L. Burton and B. C. Husband 1999).</discussion>
  <discussion>Compared to diploids, tetraploids tend to produce longer rhizomes, thicker scapes, larger leaves, and slightly larger flowers. For example, in the area of Highlands, North Carolina, leaves of tetraploids range to 15 cm wide, those of diploids to 10.5 cm; corolla size is 4.5–8 × 1.3–2.5 mm for tetraploids, 3.5–6 × 0.8–2.3 mm for diploids. It is sometimes possible to distinguish the cytotypes in the field; there apparently is little other morphological or phenological difference. The cytotypes sometimes appear to be ecologically sorted, with tetraploids occurring in more mesic sites, particularly in the mountains where diploids occur sympatrically. No differences in habitat are apparent on the piedmont and coastal plain. A hypothesis of autoploid origin of the tetraploids is supported by a study showing no significant variation in flavonoids (D. E. Soltis et al. 1983).</discussion>
  
</bio:treatment>