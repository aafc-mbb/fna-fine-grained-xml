<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="illustration_page">111</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mitella</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">diphylla</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 406. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus mitella;species diphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220008661</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not stoloniferous.</text>
      <biological_entity id="o16906" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems 10–45 (–51) cm.</text>
      <biological_entity id="o16907" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="51" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1.8–18 cm, medium and long-stipitate-glandular, longer hairs retrorse, white or tan;</text>
      <biological_entity id="o16908" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16909" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s2" to="18" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s2" value="medium" value_original="medium" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="longer" id="o16910" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to broadly ovate, ± as long as or longer than wide, 1.4–8.5 × 1.4–9.6 cm, margins shallowly to prominently 3-lobed or 5-lobed, crenate or dentate, irregularly to regularly ciliate, apex of terminal lobe acute, rarely obtuse, surfaces subglabrous or sparsely short and long-stipitate-glandular;</text>
      <biological_entity id="o16911" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16912" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="more" value_original="more" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="less as-long-as" value_original="less as-long-as" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="longer than wide" value_original="longer than wide" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s3" to="9.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16913" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character name="shape" src="d0_s3" value="dentate to irregularly regularly ciliate" />
        <character char_type="range_value" from="dentate" name="architecture" src="d0_s3" to="irregularly regularly ciliate" />
      </biological_entity>
      <biological_entity id="o16914" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16915" name="lobe" name_original="lobe" src="d0_s3" type="structure" />
      <biological_entity id="o16916" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <relation from="o16914" id="r1443" name="part_of" negation="false" src="d0_s3" to="o16915" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2, mid cauline or distal, opposite or subopposite, subsessile to short-petiolate, blade (1.1–) 1.6–8 × 0.7–6.5 cm.</text>
      <biological_entity id="o16917" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o16918" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="subopposite" value_original="subopposite" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s4" to="short-petiolate" />
      </biological_entity>
      <biological_entity id="o16919" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.1" from_unit="cm" name="atypical_length" src="d0_s4" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–5, remotely or closely 5–22 (–27) -flowered, 1 flower per node, not secund, 10–45 (–51) cm, sparsely to densely spreading or retrorsely long-stipitate-glandular proximally, short-stipitate-glandular distally.</text>
      <biological_entity id="o16920" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" modifier="remotely; closely" name="architecture" src="d0_s5" value="5-22(-27)-flowered" value_original="5-22(-27)-flowered" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16921" name="flower" name_original="flower" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="51" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="45" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="retrorsely; retrorsely; proximally" name="pubescence" src="d0_s5" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o16922" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o16921" id="r1444" name="per" negation="false" src="d0_s5" to="o16922" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–3 mm, short-stipitate-glandular.</text>
      <biological_entity id="o16923" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium broadly campanulate, 1–1.6 × 2–3.4 mm;</text>
      <biological_entity id="o16924" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16925" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals spreading, greenish white or yellowish green, triangular, 1–1.3 × 0.8–1.1 mm;</text>
      <biological_entity id="o16926" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16927" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, 9–11 (–15) -lobed, 2–4 mm, lobes linear, lateral lobes spreading or ascending;</text>
      <biological_entity id="o16928" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16929" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s9" value="9-11(-15)-lobed" value_original="9-11(-15)-lobed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16930" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o16931" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10, opposite and alternate with sepals;</text>
      <biological_entity id="o16932" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16933" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character constraint="with sepals" constraintid="o16934" is_modifier="false" name="arrangement" src="d0_s10" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o16934" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>filaments white, 0.2–0.3 mm;</text>
      <biological_entity id="o16935" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16936" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.1–0.3 × 0.1–0.2 mm;</text>
      <biological_entity id="o16937" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16938" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s12" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary nearly superior;</text>
      <biological_entity id="o16939" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16940" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s13" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles divergent, flattened, 0.1–0.2 mm;</text>
      <biological_entity id="o16941" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o16942" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas unlobed.</text>
      <biological_entity id="o16943" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o16944" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark reddish-brown or blackish, 1.2–1.6 mm, nearly smooth.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o16945" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16946" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods, hardwoods on ravine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
        <character name="habitat" value="hardwoods" />
        <character name="habitat" value="ravine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Ky., Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Two-leaf mitrewort</other_name>
  <other_name type="common_name">mitrelle à deux feuilles</other_name>
  <discussion>A decoction from whole plants of Mitella diphylla was used by the Iroquois as an emetic, eye medicine, and good-luck charm; seeds were used by the Menominee as sacred items in medicine dances (D. E. Moerman 1998).</discussion>
  <discussion>Mitella intermedia T. A. Bruhin ex Small &amp; Rydberg is a presumed interspecific hybrid between M. diphylla and M. nuda. It has been reported from New York and Wisconsin.</discussion>
  
</bio:treatment>