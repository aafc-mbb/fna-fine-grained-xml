<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
    <other_info_on_meta type="illustration_page">393</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="Small in N. L. Britton et al." date="1914" rank="genus">pityopus</taxon_name>
    <taxon_name authority="(Eastwood) H. F. Copeland" date="1935" rank="species">californicus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 155. 1935 (as californica)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus pityopus;species californicus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092307</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Monotropa</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 75, plate 7. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Monotropa;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences 1–4 cm;</text>
      <biological_entity id="o23901" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bracts cream, 1–2 × 0.5–2 cm.</text>
      <biological_entity id="o23902" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="cream" value_original="cream" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: lateral sepals 1–1.8 × 0.5 cm, others 0.8–1.2 × 0.3–0.5 cm;</text>
      <biological_entity id="o23903" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o23904" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="1.8" to_unit="cm" />
        <character name="width" src="d0_s2" unit="cm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o23905" name="other" name_original="others" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s2" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals oblong, 1–1.5 × 0.4–0.7 cm, base slightly saccate, apex rounded;</text>
      <biological_entity id="o23906" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o23907" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23908" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s3" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o23909" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens 0.6–1.1 cm;</text>
      <biological_entity id="o23910" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o23911" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s4" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers horizontal at anthesis, 1–1.5 mm diam.;</text>
      <biological_entity id="o23912" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o23913" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="diam" src="d0_s5" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 3–5 × 2.5–5 mm, glabrous;</text>
      <biological_entity id="o23914" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o23915" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 3–5 × 2–3 mm;</text>
      <biological_entity id="o23916" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o23917" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigma 5+ mm diam., apex depressed.</text>
      <biological_entity id="o23918" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23919" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23920" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="depressed" value_original="depressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Berries 5–8 (–10) diam.</text>
      <biological_entity id="o23921" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="diameter" src="d0_s9" to="10" />
        <character char_type="range_value" from="5" name="diameter" src="d0_s9" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds ca. 0.2–0.4 mm diam.</text>
      <biological_entity id="o23922" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, coniferous or mixed-deciduous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="mixed-deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Pine foot</other_name>
  
</bio:treatment>