<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch ex Borkhausen" date="unknown" rank="family">primulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dodecatheon</taxon_name>
    <taxon_name authority="L. F. Henderson" date="1930" rank="species">poeticum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>32: 27. 1930  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family primulaceae;genus dodecatheon;species poeticum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092224</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Primula</taxon_name>
    <taxon_name authority="(L. F. Henderson) Mast &amp; Reveal" date="unknown" rank="species">poetica</taxon_name>
    <taxon_hierarchy>genus Primula;species poetica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–45 cm;</text>
      <biological_entity id="o18962" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scape glandular-pubescent.</text>
      <biological_entity id="o18963" name="scape" name_original="scape" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Caudices not obvious at anthesis;</text>
      <biological_entity id="o18964" name="caudex" name_original="caudices" src="d0_s2" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="not" name="prominence" src="d0_s2" value="obvious" value_original="obvious" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>roots white;</text>
      <biological_entity id="o18965" name="root" name_original="roots" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bulblets usually present.</text>
      <biological_entity id="o18966" name="bulblet" name_original="bulblets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves (3–) 5–16 (–20) × 0.5–2.5 (–3) cm;</text>
      <biological_entity id="o18967" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole usually winged;</text>
      <biological_entity id="o18968" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate to spatulate, base usually decurrent onto stem, gradually tapering to petiole, margins usually entire, sometimes denticulate to slightly toothed, surfaces glandular-pubescent.</text>
      <biological_entity id="o18969" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="spatulate" />
      </biological_entity>
      <biological_entity id="o18970" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="onto stem" constraintid="o18971" is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
        <character constraint="to petiole" constraintid="o18972" is_modifier="false" modifier="gradually" name="shape" notes="" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18971" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <biological_entity id="o18972" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o18973" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character char_type="range_value" from="denticulate" modifier="sometimes" name="shape" src="d0_s7" to="slightly toothed" />
      </biological_entity>
      <biological_entity id="o18974" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 2–10 (–17) -flowered;</text>
      <biological_entity id="o18975" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-10(-17)-flowered" value_original="2-10(-17)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts narrowly to broadly lanceolate, 2–10 mm, glandular-pubescent.</text>
      <biological_entity id="o18976" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1–3.5 cm, glandular-pubescent.</text>
      <biological_entity id="o18977" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: calyx greenish, often with pinkish purple to purple speckles, 5–9 mm, glabrous or slightly glandular at least along margins, tube 2–4 (–5) mm, lobes 5, 3–5 mm;</text>
      <biological_entity id="o18978" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18979" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="with pinkish purple to purple speckles" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o18980" is_modifier="false" modifier="slightly" name="pubescence" src="d0_s11" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18980" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <biological_entity id="o18981" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18982" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla-tube maroon and yellow with reddish, thin, wavy ring, lobes 5, magenta to lavender, (8–) 10–15 (–18) mm;</text>
      <biological_entity id="o18983" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18984" name="corolla-tube" name_original="corolla-tube" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="maroon and yellow with reddish" />
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o18985" name="ring" name_original="ring" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o18986" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s12" to="lavender" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments connate, tube maroon, 1.5–3 × 2–3 mm;</text>
      <biological_entity id="o18987" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18988" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o18989" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 4–7 mm;</text>
      <biological_entity id="o18990" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o18991" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollen-sacs maroon to black, connective deep purple to black, transversely rugose (sometimes seemingly smooth [when immature] or transversely wrinkled [when dried]);</text>
      <biological_entity id="o18992" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o18993" name="pollen-sac" name_original="pollen-sacs" src="d0_s15" type="structure">
        <character char_type="range_value" from="maroon" name="coloration" src="d0_s15" to="black" />
      </biological_entity>
      <biological_entity id="o18994" name="connective" name_original="connective" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="deep" value_original="deep" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s15" to="black" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s15" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma not enlarged compared to style.</text>
      <biological_entity id="o18995" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o18996" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s16" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o18997" name="style" name_original="style" src="d0_s16" type="structure" />
      <relation from="o18996" id="r1579" name="compared to" negation="false" src="d0_s16" to="o18997" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules tan, often faintly reddish apically, valvate, short-ovoid, 6–9 × 4–7 mm, glandular-pubescent;</text>
      <biological_entity id="o18998" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="often faintly; apically" name="coloration" src="d0_s17" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s17" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="short-ovoid" value_original="short-ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s17" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s17" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>walls usually thick and firm.</text>
      <biological_entity id="o18999" name="wall" name_original="walls" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="width" src="d0_s18" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s18" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds without membrane along edges.</text>
      <biological_entity id="o19001" name="membrane" name_original="membrane" src="d0_s19" type="structure" />
      <biological_entity id="o19002" name="edge" name_original="edges" src="d0_s19" type="structure" />
      <relation from="o19000" id="r1580" name="without" negation="false" src="d0_s19" to="o19001" />
      <relation from="o19001" id="r1581" name="along" negation="false" src="d0_s19" to="o19002" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 44, 88.</text>
      <biological_entity id="o19000" name="seed" name_original="seeds" src="d0_s19" type="structure" />
      <biological_entity constraint="2n" id="o19003" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="44" value_original="44" />
        <character name="quantity" src="d0_s20" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist flats, slopes, and cliff faces in grassland communities and in oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="grassland communities" modifier="faces in" />
        <character name="habitat" value="oak" modifier="and in" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Poet’s shootingstar</other_name>
  <discussion>Dodecatheon poeticum grows mainly in the Columbia River gorge and on the eastern edge of the Cascade Range in Washington, and in Oregon. Nearby one can find D. conjugens var. conjugens and D. pulchellum var. cusickii, features of which (the rugose connective of the former, the glandular condition of the latter) are combined in D. poeticum. The distinct filaments of var. conjugens readily distinguish that taxon from D. poeticum; distinction between D. poeticum and D. pulchellum var. cusickii is difficult. The former has maroon pollen sacs; var. cusickii has yellow ones. Plants with all of the features of D. poeticum rarely have the smooth connective typical of D. pulchellum. H. J. Thompson (1953) suggested that D. poeticum (a tetraploid) might be the product of an allopolyploid involving var. cusickii and D. hendersonii (both diploids).</discussion>
  <discussion>The leaves of Dodecatheon poeticum are occasionally slightly toothed and relatively broad (e.g., K. L. Chambers 2080, OSC) and resemble the leaves of D. dentatum, a species that flowers in the Gorge typically after D. poeticum. Rootstocks with bulblets are rarely seen on herbarium specimens.</discussion>
  
</bio:treatment>