<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <place_of_publication>
      <publication_title>Niedenzu, Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>11: 135. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily Arbutoideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20813</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Meisner" date="unknown" rank="tribe">Arbuteae</taxon_name>
    <taxon_hierarchy>tribe Arbuteae</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, multicellular hairs present or, sometimes, absent (Comarostaphylis);</text>
      <biological_entity id="o965" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o967" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s0" value="," value_original="," />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark smooth, not furrowed, often flaky, sometimes shredding.</text>
      <biological_entity id="o968" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" modifier="often" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or prostrate.</text>
      <biological_entity id="o969" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent (deciduous in Arctous), usually alternate, sometimes opposite or whorled (Ornithostaphylos), rarely opposite (Xylococcus);</text>
      <biological_entity id="o970" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole usually present, sometimes absent (Arctostaphylos);</text>
      <biological_entity id="o971" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade plane, abaxial groove absent.</text>
      <biological_entity id="o972" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o973" name="groove" name_original="groove" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, panicles or racemes;</text>
      <biological_entity id="o974" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o975" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o976" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>perulae absent;</text>
      <biological_entity id="o977" name="perula" name_original="perulae" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts much shorter than sepals;</text>
      <biological_entity id="o978" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character constraint="than sepals" constraintid="o979" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o979" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 2 or absent (Arctostaphylos, Arctous).</text>
      <biological_entity id="o980" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers pendulous;</text>
      <biological_entity id="o981" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (4-) 5;</text>
      <biological_entity id="o982" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (4-) 5, connate, corolla deciduous, usually urceolate, sometimes cylindric, conic, or globose, lobes much shorter than tube;</text>
      <biological_entity id="o983" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o984" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o985" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character constraint="than tube" constraintid="o986" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o986" name="tube" name_original="tube" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>intrastaminal nectary disc present;</text>
      <biological_entity constraint="nectary" id="o987" name="disc" name_original="disc" src="d0_s13" type="structure" constraint_original="intrastaminal nectary">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens (8-) 10;</text>
      <biological_entity id="o988" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s14" to="10" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dehiscent by slits or pores;</text>
      <biological_entity id="o989" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character constraint="by pores" constraintid="o991" is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o990" name="slit" name_original="slits" src="d0_s15" type="structure" />
      <biological_entity id="o991" name="pore" name_original="pores" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>ovary 2-10-locular;</text>
    </statement>
    <statement id="d0_s17">
      <text>placentation axile;</text>
      <biological_entity id="o992" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="2-10-locular" value_original="2-10-locular" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style straight.</text>
      <biological_entity id="o993" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits drupaceous (baccate in Arbutus), (pulp mealy or juicy), indehiscent;</text>
      <biological_entity id="o994" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>pyrenes 1-5, often connate into stonelike endocarp.</text>
      <biological_entity id="o995" name="pyrene" name_original="pyrenes" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s20" to="5" />
        <character constraint="into endocarp" constraintid="o996" is_modifier="false" modifier="often" name="fusion" src="d0_s20" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o996" name="endocarp" name_original="endocarp" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="stonelike" value_original="stonelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds 1-10, usually distinct, sometimes connate, globose (sometimes 3-sided), not winged.</text>
      <biological_entity id="o997" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s21" to="10" />
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes" name="fusion" src="d0_s21" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s21" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s21" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 6, species 91 or 92 (6 genera, 70 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, Europe, n Africa, n Atlantic Islands (Canary Islands); most species endemic to western North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="n Atlantic Islands (Canary Islands)" establishment_means="native" />
        <character name="distribution" value="most species endemic to western North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19b.</number>
  <references>
    <reference>Hileman, L. C., M. C. Vasey, and V. T. Parker. 2001. Phylogeny and biogeography of the Arbutoideae (Ericaceae): Implications for the Madrean-Tethyan hypothesis. Syst. Bot. 26: 131–143.</reference>
  </references>
  
</bio:treatment>