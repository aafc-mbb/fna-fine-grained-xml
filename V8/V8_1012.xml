<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">vaccinium</taxon_name>
    <taxon_name authority="W. D. J. Koch" date="1837" rank="section">Vitis-idaea</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. Germ. Helv.,</publication_title>
      <place_in_publication>474. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus vaccinium;section vitis-idaea;</taxon_hierarchy>
    <other_info_on_name type="fna_id">317366</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, procumbent to ascending, 0.1–3.5 dm, rhizomatous, (floral buds prominent, pseudo terminal, rotund).</text>
      <biological_entity id="o27382" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent.</text>
      <biological_entity id="o27383" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences terminal, racemes, 2–3-flowered, sometimes flowers solitary, borne on previous year’s growth.</text>
      <biological_entity id="o27384" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o27385" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-3-flowered" value_original="2-3-flowered" />
      </biological_entity>
      <biological_entity id="o27386" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="previous" id="o27387" name="year" name_original="year" src="d0_s2" type="structure" />
      <biological_entity constraint="previous" id="o27388" name="growth" name_original="growth" src="d0_s2" type="structure" />
      <relation from="o27386" id="r2292" name="borne on" negation="false" src="d0_s2" to="o27387" />
      <relation from="o27386" id="r2293" name="borne on" negation="false" src="d0_s2" to="o27388" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels articulated with calyx-tube.</text>
      <biological_entity id="o27389" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character constraint="with calyx-tube" constraintid="o27390" is_modifier="false" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o27390" name="calyx-tube" name_original="calyx-tube" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers (short-pedicellate);</text>
      <biological_entity id="o27391" name="flower" name_original="flowers" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>sepals 4;</text>
      <biological_entity id="o27392" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 4 (–5), connate ca. 3/4 their lengths, corolla ± campanulate, (lobes not reflexed);</text>
      <biological_entity id="o27393" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s6" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o27394" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 8, included;</text>
      <biological_entity id="o27395" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
        <character is_modifier="false" name="position" src="d0_s7" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers without awns, tubules 0.8–1.4 mm, with terminal pores.</text>
      <biological_entity id="o27396" name="anther" name_original="anthers" src="d0_s8" type="structure" />
      <biological_entity id="o27397" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o27398" name="tubule" name_original="tubules" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o27399" name="pore" name_original="pores" src="d0_s8" type="structure" />
      <relation from="o27396" id="r2294" name="without" negation="false" src="d0_s8" to="o27397" />
      <relation from="o27398" id="r2295" name="with" negation="false" src="d0_s8" to="o27399" />
    </statement>
    <statement id="d0_s9">
      <text>Berries 4-locular.</text>
      <biological_entity id="o27400" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="4-locular" value_original="4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds ca. 10–20.</text>
      <biological_entity id="o27401" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia; circumboreal.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="circumboreal" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45d.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Hall, I. V. and J. M. Shay. 1981. Biological flora of Canada: 3. Vaccinium vitis-idaea L. var. minus Lodd. Supplementary account. Canad. Field-Naturalist 45: 435–464.</reference>
  </references>
  
</bio:treatment>