<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="(Lemaire) Britton &amp; Rose" date="1903" rank="species">cymosa</taxon_name>
    <taxon_name authority="Moran" date="1957" rank="subspecies">marcescens</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>14: 106, fig. 1. 1957,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species cymosa;subspecies marcescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092069</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="(Moran) P. H. Thomson" date="unknown" rank="species">marcescens</taxon_name>
    <taxon_hierarchy>genus Dudleya;species marcescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices often branching, 0.2–0.7 cm diam.</text>
      <biological_entity id="o8564" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="diameter" src="d0_s0" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves withering in summer;</text>
      <biological_entity id="o8565" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="in summer" is_modifier="false" name="life_cycle" src="d0_s1" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rosettes 8–15-leaved;</text>
      <biological_entity id="o8566" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="8-15-leaved" value_original="8-15-leaved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green, oblanceolate, 1.5–3 (–5) × 0.5–1.5 cm, apex acute to subobtuse, surfaces not farinose, glaucous.</text>
      <biological_entity id="o8567" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8568" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="subobtuse" />
      </biological_entity>
      <biological_entity id="o8569" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: floral shoots 5–15-leaved, 4–10 cm;</text>
      <biological_entity id="o8570" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="floral" id="o8571" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-15-leaved" value_original="5-15-leaved" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cincinni 1–2, (1–) 3–5-flowered, 1–4 cm.</text>
      <biological_entity id="o8572" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o8573" name="cincinnus" name_original="cincinni" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)3-5-flowered" value_original="(1-)3-5-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Petals bright-yellow, often marked with red, 10–14 × 2.5–3.5 mm. 2n = 34.</text>
      <biological_entity id="o8574" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="10" from_unit="mm" modifier="often" name="length" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" modifier="often" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8575" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>With moss and lichens in very thin soil on sheer north-facing cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moss" modifier="with" constraint="in very thin soil on sheer north-facing cliffs" />
        <character name="habitat" value="lichens" constraint="in very thin soil on sheer north-facing cliffs" />
        <character name="habitat" value="thin soil" modifier="very" constraint="on sheer north-facing cliffs" />
        <character name="habitat" value="sheer north-facing cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8h.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies marcescens is known from the Santa Monica Mountains, Ventura and Los Angeles counties; it is considered fairly threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi). This is the most distinct and distinctive of the subspecies of Dudleya cymosa, very different from subsp. cymosa but more or less connected by subsp. ovatifolia, also in the Santa Monica Mountains. It differs in its slender caudex, small leaves, and few flowers, and is one of only two members of subg. Dudleya whose leaves wither in early summer and are not replaced until after the first rains, often in late fall. The other is D. parva, growing some 13 kilometers to the north. In habit, they resemble subg. Hasseanthus, which differs in its slow-growing underground stems with crowded leaves and its open flowers. As noted by M. Dodero (1996), these two also are paedomorphic forms.</discussion>
  
</bio:treatment>