<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">oxyacanthoides</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) P. K. Holmgren in A. Cronquist et al." date="1997" rank="variety">hendersonii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Cronquist et al., Intermount. Fl.</publication_title>
      <place_in_publication>3(A): 16. 1997,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species oxyacanthoides;variety hendersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065861</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="species">hendersonii</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>3: 73. 1961,</place_in_publication>
      <other_info_on_pub>based on Grossularia neglecta A. Berger, Techn. Bull. New York Agric. Exp. Sta., Geneva 109: 106. 1924, not R. neglectum Rose 1905</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Ribes;species hendersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">oxyacanthoides</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) Q. P. Sinnott" date="unknown" rank="subspecies">hendersonii</taxon_name>
    <taxon_hierarchy>genus Ribes;species oxyacanthoides;subspecies hendersonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: spines at nodes (1–) 2–3 (–7);</text>
      <biological_entity id="o16576" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o16577" name="spine" name_original="spines" src="d0_s0" type="structure" />
      <biological_entity id="o16578" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s0" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s0" to="7" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="3" />
      </biological_entity>
      <relation from="o16577" id="r1412" name="at" negation="false" src="d0_s0" to="o16578" />
    </statement>
    <statement id="d0_s1">
      <text>internodal prickles absent or sparse.</text>
      <biological_entity id="o16579" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="internodal" id="o16580" name="prickle" name_original="prickles" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 0.7–1.1 cm, base deeply cordate, surfaces finely pubescent and glandular-puberulent, more glandular abaxially, lobes cuneate-rounded, margins with 3–7 unequal crenate dentations.</text>
      <biological_entity id="o16581" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="distance" src="d0_s2" to="1.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16582" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o16583" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o16584" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate-rounded" value_original="cuneate-rounded" />
      </biological_entity>
      <biological_entity id="o16585" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity id="o16586" name="dentation" name_original="dentations" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="7" />
        <character is_modifier="true" name="size" src="d0_s2" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <relation from="o16585" id="r1413" name="with" negation="false" src="d0_s2" to="o16586" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 1–3 mm.</text>
      <biological_entity id="o16587" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 1–3 (–4) mm, glabrous.</text>
      <biological_entity id="o16588" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: hypanthium greenish white to white or pinkish, broadly campanulate to rotate, 1.4–3 mm, glabrous;</text>
      <biological_entity id="o16589" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16590" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s5" to="white or pinkish" />
        <character char_type="range_value" from="broadly campanulate" name="shape" src="d0_s5" to="rotate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals pale greenish white or with slight reddish tinge, ovate-oblong, 1–3.3 mm, apex rounded, glabrous or sparsely hairy;</text>
      <biological_entity id="o16591" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o16592" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale greenish" value_original="pale greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="with slight" value_original="with slight" />
        <character constraint="with slight" is_modifier="false" name="coloration" src="d0_s6" value="reddish tinge" value_original="reddish tinge" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-oblong" value_original="ovate-oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16593" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, broadly obovate-rhombic to flabelliform-reniform, 1–1.6 mm, base shortly, broadly clawed, apex truncate, slightly erose;</text>
      <biological_entity id="o16594" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16595" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="broadly obovate-rhombic" name="shape" src="d0_s7" to="flabelliform-reniform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16596" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o16597" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 0.5–1 mm;</text>
      <biological_entity id="o16598" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16599" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 2.5–3.3 mm.</text>
      <biological_entity id="o16600" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16601" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs and talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49b.</number>
  
</bio:treatment>