<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="species">abramsii</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and J. N. Rose,  New N. Amer. Crassul.,</publication_title>
      <place_in_publication>14. 1903 (as abramsi)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species abramsii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092070</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices simple or apically branching and cespitose, 0.5–8 × 1–3 cm, axillary branches absent.</text>
      <biological_entity id="o13914" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o13915" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes 1–100+, in clumps or not, 10–20 (–30) -leaved, 2–8 (–15) cm diam.;</text>
      <biological_entity id="o13916" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13917" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="100" upper_restricted="false" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="10-20(-30)-leaved" value_original="10-20(-30)-leaved" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13918" name="clump" name_original="clumps" src="d0_s1" type="structure" />
      <relation from="o13917" id="r1193" name="in" negation="false" src="d0_s1" to="o13918" />
    </statement>
    <statement id="d0_s2">
      <text>blade green, mostly linear or elliptic or oblong to oblong-lanceolate or tapering from base, 1–6 (–11) × 0.2–1.2 (–2) cm, 2–4 mm thick, base 0.5–1.5 (–2) cm wide, apex acute to subacuminate, often apiculate, surfaces not farinose, glaucous (at least when young).</text>
      <biological_entity id="o13919" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13920" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="mostly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character name="arrangement_or_course_or_shape" src="d0_s2" value="elliptic or oblong to oblong-lanceolate or tapering" />
        <character char_type="range_value" from="1-6" from_inclusive="false" from_unit="cm" name="thickness" notes="" src="d0_s2" to="11" to_unit="cm" />
        <character name="thickness" notes="" src="d0_s2" unit="cm" value="1-6" value_original="1-6" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" notes="" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13921" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o13922" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13923" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="subacuminate" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o13924" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="farinose" value_original="farinose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <relation from="o13920" id="r1194" name="from" negation="false" src="d0_s2" to="o13921" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: cyme 1–4-branched, narrowly obpyramidal;</text>
      <biological_entity id="o13925" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o13926" name="cyme" name_original="cyme" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-4-branched" value_original="1-4-branched" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="obpyramidal" value_original="obpyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches not twisted (flowers on topside), simple or 1–3 times bifurcate;</text>
      <biological_entity id="o13927" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o13928" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s4" value="1-3 times bifurcate" value_original="1-3 times bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cincinni 2–3, 2–10 (–18) -flowered, scarcely circinate, 3–15 cm;</text>
      <biological_entity id="o13929" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o13930" name="cincinnus" name_original="cincinni" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-10(-18)-flowered" value_original="2-10(-18)-flowered" />
        <character is_modifier="false" modifier="scarcely" name="shape_or_vernation" src="d0_s5" value="circinate" value_original="circinate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral shoots 2–25 × 0.1–0.4 (–0.6) cm;</text>
      <biological_entity id="o13931" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o13932" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaves 5–20, ascending, triangular-lanceolate, 4–40 × 2–11 mm, apex acute.</text>
      <biological_entity id="o13933" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o13934" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13935" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect, not bent in fruit, 1–5 (–17) mm.</text>
      <biological_entity id="o13936" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o13937" is_modifier="false" modifier="not" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13937" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx 3–7 × 2–7 mm;</text>
      <biological_entity id="o13938" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13939" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals connate 1–3 (–4.5) mm, mostly pale-yellow, straw yellow, or almost white, rarely bright-yellow, commonly red-lineolate especially on keel, 5–16 (–18) × 1.5–4.5 mm, (margins often somewhat erose), apex acute, with tips mostly erect;</text>
      <biological_entity id="o13940" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13941" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="straw yellow" value_original="straw yellow" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character constraint="on keel" constraintid="o13942" is_modifier="false" modifier="commonly" name="coloration_or_pubescence_or_relief" src="d0_s10" value="red-lineolate" value_original="red-lineolate" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s10" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13942" name="keel" name_original="keel" src="d0_s10" type="structure" />
      <biological_entity id="o13943" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13944" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o13943" id="r1195" name="with" negation="false" src="d0_s10" to="o13944" />
    </statement>
    <statement id="d0_s11">
      <text>pistils connivent, erect.</text>
      <biological_entity id="o13945" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13946" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Unripe follicles erect.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 34.</text>
      <biological_entity id="o13947" name="follicle" name_original="follicles" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="unripe" value_original="unripe" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13948" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Abrams’s dudleya or liveforever</other_name>
  <discussion>Subspecies 7 (7 in the flora).</discussion>
  <discussion>Dudleya abramsii, a widespread diploid, is largely montane, growing mostly on granite or limestone, but its northwestern subspp. bettinae, murina, and setchellii are local at lower elevations, on serpentine. It forms small clumps, 2–11 cm in diameter. From the widespread diploid D. cymosa, generally a larger plant, it is nearly distinct in its mostly cespitose, narrow-leaved rosettes and simple cyme branches, and distinctively, the antisepalous stamens are adnate higher on the corolla tube than the epipetalous and are often 1–2 mm longer. The petals commonly much exceed the stamens and pistils, and the pistils are attenuate. Both species are variable, and subsp. costatifolia is somewhat intermediate.</discussion>
  <discussion>Five of the six subspecies are very similar, with only small differences, and only subsp. costatifolia is more distinctive.</discussion>
  <references>
    <reference>Moran, R. V. 1952. Dudleya abramsii Rose. Desert Pl. Life 24: 16–17.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals bright yellow; inflorescence branches 1-3 times bifurcate; leaf blades linear, subterete.</description>
      <determination>9g Dudleya abramsii subsp. costatifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals straw yellow or pale yellow, almost white, often red-lineolate; inflorescence branches mostly simple, or some 1-2 times bifurcate; leaf blades elliptic, oblong, oblanceolate, oblong-lanceolate, or tapering from base, terete or subterete to laminar</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rosette leaf blades oblanceolate to elliptic, 1.5-4 cm; rosettes solitary</description>
      <determination>9e Dudleya abramsii subsp. affinis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rosette leaf blades oblong, oblong-lanceolate, or tapering from base, 1-10(-11) cm; rosettes (1-)3-100+</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescence branches often simple, sometimes 1-2 times bifurcate; petals red- lineolate or not.</description>
      <determination>9f Dudleya abramsii subsp. calcicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescence branches simple; petals often red-lineolate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Floral shoots 2-15 × 0.1-0.3 cm; proximalmost inflorescence leaf blades 4-15(-40) mm; petal tips often strongly outcurved; mostly on granite, 300- 1900 m.</description>
      <determination>9a Dudleya abramsii subsp. abramsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Floral shoots 5-25 × 0.2-0.6 cm; proximalmost inflorescence leaf blades 10-30 mm; petal tips erect; serpentine, 0-600 m</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Rosettes 100+, leaf blades 0.3-0.7 cm wide.</description>
      <determination>9c Dudleya abramsii subsp. bettinae</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Rosettes mostly 1-5, leaf blades 0.5-2 cm wide</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals straw yellow, marked with purplish red; rosette leaf blades 0.5-2 cm wide.</description>
      <determination>9b Dudleya abramsii subsp. murina</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals pale yellow, almost white, not red-marked; rosette leaf blades 0.5-1 cm wide.</description>
      <determination>9d Dudleya abramsii subsp. setchellii</determination>
    </key_statement>
  </key>
</bio:treatment>