<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
    <other_info_on_meta type="illustration_page">73</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Voss" date="1899" rank="genus">darmera</taxon_name>
    <taxon_name authority="(Torrey ex Bentham) Voss" date="1899" rank="species">peltata</taxon_name>
    <place_of_publication>
      <publication_title>Gärtn. Zentralbl.</publication_title>
      <place_in_publication>1: 645. 1899,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus darmera;species peltata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220003860</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Torrey ex Bentham" date="unknown" rank="species">peltata</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>311. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species peltata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole 20–150 cm;</text>
      <biological_entity id="o19360" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o19361" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade (5–) 10–60 (–90) cm wide.</text>
      <biological_entity id="o19362" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19363" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="width" src="d0_s1" to="90" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences scapose, 30–150 cm, sparsely stipitate-glandular.</text>
      <biological_entity id="o19364" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: hypanthium saucerlike, 0.5–1 mm;</text>
      <biological_entity id="o19365" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o19366" name="hypanthium" name_original="hypanthium" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="saucerlike" value_original="saucerlike" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reflexed, rounded to oblong, 2.5–5 mm, apex rounded;</text>
      <biological_entity id="o19367" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19368" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19369" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals spreading, broadly elliptic to obovate, unlobed, 5–9 × 4–4.5 mm;</text>
      <biological_entity id="o19370" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19371" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s5" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 4 mm;</text>
      <biological_entity id="o19372" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19373" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>styles divergent, 1 mm.</text>
      <biological_entity id="o19374" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19375" name="style" name_original="styles" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 2, reddish, elliptic, 7–11 mm.</text>
      <biological_entity id="o19376" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 120–150, 1–1.5 mm. 2n = 34.</text>
      <biological_entity id="o19377" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" unit=",1-1.5 mm" />
        <character char_type="range_value" from="120" name="some_measurement" src="d0_s9" to="150" unit=",1-1.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19378" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Among wet rocks in and near streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet rocks" modifier="among" constraint="in and near streams" />
        <character name="habitat" value="near streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Darmera peltata is found in the Sierra Nevada of California from Tulare County to Siskiyou County and in the Klamath Mountains and Coast Range from Humboldt County in California to Benton County, Oregon. It is sometimes cultivated as an ornamental. Pregnant Karok women took an infusion of the roots of D. peltata to prevent the fetus from getting too large; Karok and Miwok Indians ate the plant (D. E. Moerman 1998).</discussion>
  
</bio:treatment>