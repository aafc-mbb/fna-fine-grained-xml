<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PYROLA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 396. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 188. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus pyrola;</taxon_hierarchy>
    <other_info_on_name type="etymology">olus, diminutive, alluding to resemblance of leaves</other_info_on_name>
    <other_info_on_name type="fna_id">127777</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, chlorophyllous, autotrophic (achlorophyllous and heterotrophic in forms of P. chlorantha and P. picta).</text>
      <biological_entity id="o16627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="chlorophyllous" value_original="chlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="autotrophic" value_original="autotrophic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o16628" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves essentially basal or, sometimes, highly reduced or absent (P. chlorantha, P. picta), alternate;</text>
      <biological_entity id="o16629" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16630" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="essentially" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="sometimes; highly" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o16631" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade maculate or not, elliptic, ovate-elliptic, oblongelliptic, oblanceolate, oblong-obovate, ovate, obovate, spatulate, subreniform, reniform, or round, subcoriaceous to coriaceous, margins entire, denticulate, crenulate, crenate, or crenate-serrulate, plane or revolute, surfaces glabrous.</text>
      <biological_entity id="o16632" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="maculate" value_original="maculate" />
        <character name="coloration" src="d0_s4" value="not" value_original="not" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subreniform" value_original="subreniform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character char_type="range_value" from="subcoriaceous" name="texture" src="d0_s4" to="coriaceous" />
      </biological_entity>
      <biological_entity id="o16633" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-serrulate" value_original="crenate-serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-serrulate" value_original="crenate-serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-serrulate" value_original="crenate-serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o16634" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, usually erect in flower and fruit, (symmetric);</text>
      <biological_entity constraint="inflorescences" id="o16635" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character constraint="in fruit" constraintid="o16637" is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16636" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <biological_entity id="o16637" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>peduncular bracts present or absent;</text>
      <biological_entity constraint="peduncular" id="o16638" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inflorescence bracts free from pedicels.</text>
      <biological_entity constraint="inflorescence" id="o16639" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character constraint="from pedicels" constraintid="o16640" is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o16640" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels pendent in fruit;</text>
      <biological_entity id="o16641" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o16642" is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o16642" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o16643" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers radially symmetric (bilaterally symmetric in P. minor), spreading or nodding;</text>
      <biological_entity id="o16644" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, connate proximally, often obscurely so, calyx lobes lanceolate, ovate, triangular, deltate, oblong, or obovate;</text>
      <biological_entity id="o16645" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o16646" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often obscurely; obscurely" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, distinct, white, greenish white, yellowish white, pink, or purplish red, without basal tubercles, corolla crateriform to broadly campanulate;</text>
      <biological_entity id="o16647" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish red" value_original="purplish red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish red" value_original="purplish red" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16648" name="tubercle" name_original="tubercles" src="d0_s12" type="structure" />
      <biological_entity id="o16649" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character char_type="range_value" from="crateriform" name="shape" src="d0_s12" to="broadly campanulate" />
      </biological_entity>
      <relation from="o16647" id="r1415" name="without" negation="false" src="d0_s12" to="o16648" />
    </statement>
    <statement id="d0_s13">
      <text>intrastaminal nectary disc absent;</text>
      <biological_entity constraint="nectary" id="o16650" name="disc" name_original="disc" src="d0_s13" type="structure" constraint_original="intrastaminal nectary">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 10, exserted;</text>
      <biological_entity id="o16651" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments broad proximally, gradually narrowed medially, slender distally, glabrous;</text>
      <biological_entity id="o16652" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="width" src="d0_s15" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="gradually; medially" name="shape" src="d0_s15" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s15" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers oblong, without awns, with or without tubules, dehiscent by 2 round to elliptic or obovate pores;</text>
      <biological_entity id="o16653" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character constraint="by pores" constraintid="o16655" is_modifier="false" name="dehiscence" notes="" src="d0_s16" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o16654" name="tubule" name_original="tubules" src="d0_s16" type="structure" />
      <biological_entity id="o16655" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character char_type="range_value" from="round" is_modifier="true" name="shape" src="d0_s16" to="elliptic or obovate" />
      </biological_entity>
      <relation from="o16653" id="r1416" name="without awns , with or without" negation="false" src="d0_s16" to="o16654" />
    </statement>
    <statement id="d0_s17">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o16656" name="pistil" name_original="pistil" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary imperfectly 5-locular;</text>
    </statement>
    <statement id="d0_s19">
      <text>placentation intruded-parietal;</text>
      <biological_entity id="o16657" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s18" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="placentation" src="d0_s19" value="intruded-parietal" value_original="intruded-parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>style (exserted or included), bent downward or straight (P. minor), expanded distally;</text>
      <biological_entity id="o16658" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="bent" value_original="bent" />
        <character is_modifier="false" name="orientation" src="d0_s20" value="downward" value_original="downward" />
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s20" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma 5-lobed, without subtending ring of hairs.</text>
      <biological_entity id="o16659" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o16660" name="ring" name_original="ring" src="d0_s21" type="structure" />
      <biological_entity id="o16661" name="hair" name_original="hairs" src="d0_s21" type="structure" />
      <relation from="o16659" id="r1417" name="without" negation="false" src="d0_s21" to="o16660" />
      <relation from="o16660" id="r1418" name="part_of" negation="false" src="d0_s21" to="o16661" />
    </statement>
    <statement id="d0_s22">
      <text>Fruits capsular, pendulous, dehiscence loculicidal, cobwebby tissue exposed by splitting valves at dehiscence.</text>
      <biological_entity id="o16662" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s22" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="dehiscence" src="d0_s22" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
      <biological_entity id="o16663" name="tissue" name_original="tissue" src="d0_s22" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s22" value="cobwebby" value_original="cobwebby" />
        <character constraint="by valves" constraintid="o16664" is_modifier="false" name="prominence" src="d0_s22" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o16664" name="valve" name_original="valves" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture_or_dehiscence" src="d0_s22" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds ca. 1000, fusiform, winged.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 23.</text>
      <biological_entity id="o16665" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="1000" value_original="1000" />
        <character is_modifier="false" name="shape" src="d0_s23" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o16666" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="23" value_original="23" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Guatemala), Europe, Asia (including Sumatra).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (including Sumatra)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Wintergreen</other_name>
  <other_name type="common_name">Latin pyrus</other_name>
  <other_name type="common_name">pear</other_name>
  <other_name type="common_name">and</other_name>
  <discussion>Species ca. 30 (7 in the flora).</discussion>
  <discussion>The apparent absence of strong genetic discontinuities within many species complexes, as well as morphologic and cytologic uniformity, have challenged attempts to delimit species in Pyrola. Chromosome counts for all species are diploid (2n = 46) except for the boreal European species P. media, which is a tetraploid (2n = 92), and some triploid counts (2n = 69) for P. grandiflora. Natural hybrids have been reported widely. Some species complexes have been examined in detail; a modern, comprehensive monograph of the genus is needed. Of particular interest in the flora area are relationships among members of sect. Pyrola, which includes, among other species, North American P. americana, amphi-Pacific P. asarifolia, arctic and circumpolar P. grandiflora, and Eurasian P. rotundifolia Linnaeus. J. V. Freudenstein (1999b) found limited cladistic structure in Pyrola. Morphologic and molecular data support a clade comprising P. chlorantha and P. picta (including P. aphylla). Molecular data suggest that this clade is sister to one comprising P. elliptica and P. minor.</discussion>
  <discussion>Pyrola americana, P. asarifolia, P. chlorantha, P. elliptica, and P. picta have a variety of drug, food, and ceremonial uses among a dozen tribes of Native Americans (D. E. Moerman 1998).</discussion>
  <references>
    <reference>Camp, W. H. 1940. Aphyllous forms of Pyrola. Bull. Torrey Bot. Club 67: 453–465.</reference>
    <reference>Haber, E. 1970. Some aberrant Pyrola collections from eastern North America. Rhodora 72: 480–485.</reference>
    <reference>Haber, E. 1984. A comparative study of Pyrola minor × P. asarifolia (Ericaceae) and its parental species in North America. Canad. J. Bot. 62: 1054–1061.</reference>
    <reference>Haber, E. 1993. Hybridization of Pyrola chlorantha (Ericaceae) in North America. Canad. J. Bot. 66: 1993–2000.</reference>
    <reference>Křísa, B. 1966. Contribution to the taxonomy of the genus Pyrola L. in North America. Bot. Jahrb. Syst. 85: 612–637.</reference>
    <reference>Křísa, B. 1971. Beitrag zur Taxonomie und Chorologie der Gattung Pyrola L. Bot. Jahrb. Syst. 90: 476–508.</reference>
    <reference>Takahashi, Hiroshi. 1986. Pollen morphology of Pyrola and its taxonomic significance. Bot. Mag. (Tokyo) 99: 137–154.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles (0.5-)0.8-1.5(-1.8) mm, included, straight; anthers 0.8-1.4 mm, tubules absent; flowers radially symmetric.</description>
      <determination>6 Pyrola minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 4-10 mm, exserted, bent downward; anthers (1.6-)2.2-5.5 mm, tubules present; flowers bilaterally symmetric</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence bracts as long as or longer than subtended pedicels (sometimes shorter than subtended pedicels in P. asarifolia subsp. asarifolia); calyx lobes longer than wide</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence bracts usually shorter than subtended pedicels, rarely longer than subtended pedicels; calyx lobes ± as long as wide</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Filament bases 0.2-0.3 mm wide; anther apiculations absent or less than 0.1 mm, thecae creamy yellow to golden yellow, tubules yellow to yellowish brown.</description>
      <determination>5 Pyrola grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Filament bases 0.5-1.1 mm wide; anther apiculations 0.1-0.5(-0.7) mm, thecae creamy white, greenish white, tan, pink, reddish, dark purple, or yellowish, tubules yellowish brown, orange, pink, reddish, or dark purple</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Calyx lobes ovate, ovate-oblong, or obovate, apices obtuse to acute; petals white, often suffused with pink.</description>
      <determination>1 Pyrola americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Calyx lobes triangular, apices acute to acuminate; petals white proximally and pinkish distally, or pink to purplish red throughout</description>
      <determination>2 Pyrola asarifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Anther tubules abruptly narrowed from thecae, lateral walls not touching or connivent distally, 0.7-1.1 mm; calyx lobe apices acute to obtuse.</description>
      <determination>3 Pyrola chlorantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Anther tubules gradually narrowed (at least when viewed laterally) from thecae, lateral walls touching for most of their lengths or connivent distally, 0.3-0.8 mm; calyx lobe apices acute to acuminate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades not maculate or, rarely, maculate, broadly elliptic to oblong or oblong-obovate, margins crenulate or obscurely denticulate; petals white to greenish white; apices of calyx lobes acute to short-acuminate.</description>
      <determination>4 Pyrola elliptica</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades usually maculate, sometimes not maculate, ovate or ovate-elliptic to oblanceolate or spatulate, margins entire or denticulate to coarsely denticulate, or plants leafless; petals greenish white, white, pink, or reddish; apices of calyx lobes acute.</description>
      <determination>7 Pyrola picta</determination>
    </key_statement>
  </key>
</bio:treatment>