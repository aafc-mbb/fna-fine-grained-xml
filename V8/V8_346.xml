<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">174</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="(Britton &amp; Rose) Moran" date="1942" rank="subgenus">Stylophyllum</taxon_name>
    <taxon_name authority="(Rose) Moran" date="1943" rank="species">virens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">virens</taxon_name>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus stylophyllum;species virens;subspecies virens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092051</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="(Rose) P. H. Thomson" date="unknown" rank="species">albida</taxon_name>
    <taxon_hierarchy>genus Dudleya;species albida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stylophyllum</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">albidum</taxon_name>
    <taxon_hierarchy>genus Stylophyllum;species albidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices to 2+ dm × 1–3 cm.</text>
      <biological_entity id="o27999" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rosette leaf-blades usually green, triangular-lanceolate to linear-lanceolate, 5–15 × 1–1.5 cm, surfaces sometimes farinose.</text>
      <biological_entity constraint="rosette" id="o28000" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s1" to="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28001" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: floral shoots 2–4.5 dm × 4–7 mm;</text>
      <biological_entity id="o28002" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="floral" id="o28003" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s2" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cyme mostly obpyramidal;</text>
      <biological_entity id="o28004" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o28005" name="cyme" name_original="cyme" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s3" value="obpyramidal" value_original="obpyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches 1–3 times bifurcate;</text>
      <biological_entity id="o28006" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o28007" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="1-3 times bifurcate" value_original="1-3 times bifurcate " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cincinni 5–12-flowered, 3–5 cm.</text>
      <biological_entity id="o28008" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o28009" name="cincinnus" name_original="cincinni" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-12-flowered" value_original="5-12-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas ca. 16–20 mm diam. 2n = 34.</text>
      <biological_entity id="o28010" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28011" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs, flat terraces, and rocky banks near sea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="flat terraces" />
        <character name="habitat" value="rocky banks" />
        <character name="habitat" value="sea" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies virens is endemic to San Clemente Island.</discussion>
  
</bio:treatment>