<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="treatment_page">80</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1840" rank="genus">lithophragma</taxon_name>
    <taxon_name authority="Eastwood" date="1905" rank="species">trifoliatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>32: 200. 1905 (as trifoliata),</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus lithophragma;species trifoliatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065928</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lithophragma</taxon_name>
    <taxon_name authority="(Hooker) Nuttall ex Torrey &amp; A. Gray" date="unknown" rank="species">parviflorum</taxon_name>
    <taxon_name authority="(Eastwood) Jepson" date="unknown" rank="variety">trifoliatum</taxon_name>
    <taxon_hierarchy>genus Lithophragma;species parviflorum;variety trifoliatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender.</text>
      <biological_entity id="o2091" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems simple, 20–50 cm.</text>
      <biological_entity id="o2092" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in basal rosette and cauline, basal digitately 3-lobed, segments again lobed, cauline (2–3), 3-foliolate or deeply lobed, reduced, similar to basal (except lobes longer);</text>
      <biological_entity id="o2093" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="digitately" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2094" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity id="o2095" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="again" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <relation from="o2093" id="r183" name="in" negation="false" src="d0_s2" to="o2094" />
    </statement>
    <statement id="d0_s3">
      <text>stipules large, decurrent on petiole base;</text>
      <biological_entity id="o2096" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="large" value_original="large" />
        <character constraint="on petiole base" constraintid="o2097" is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o2097" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole to 11 cm;</text>
      <biological_entity id="o2098" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade dark green or reddish green, orbiculate, (base hastate), surfaces densely hairy.</text>
      <biological_entity id="o2099" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
      <biological_entity id="o2100" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences usually 1, (lax), nodding, 4–8-flowered racemes, simple.</text>
      <biological_entity id="o2101" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o2102" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="4-8-flowered" value_original="4-8-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not exceeding length of hypanthium.</text>
      <biological_entity id="o2103" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers deciduous if unfertilized, fragrant, horizontal;</text>
      <biological_entity id="o2104" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="if unfertilized" is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="odor" src="d0_s8" value="fragrant" value_original="fragrant" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium elongate-obconic, elongating in fruit, abruptly expanding, open at throat, (9–11 mm, length 3–4 times diam.);</text>
      <biological_entity id="o2105" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate-obconic" value_original="elongate-obconic" />
        <character constraint="in fruit" constraintid="o2106" is_modifier="false" name="length" src="d0_s9" value="elongating" value_original="elongating" />
        <character is_modifier="false" modifier="abruptly" name="size" notes="" src="d0_s9" value="expanding" value_original="expanding" />
        <character constraint="at throat" constraintid="o2107" is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o2106" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o2107" name="throat" name_original="throat" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals erect, triangular;</text>
      <biological_entity id="o2108" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals widely spreading, pink, obovate-rhombic, narrowly clawed, 3-lobed, 4–14 mm, ultimate margins entire;</text>
      <biological_entity id="o2109" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate-rhombic" value_original="obovate-rhombic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2110" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 1/2+ to ± entirely inferior;</text>
      <biological_entity id="o2111" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s12" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less entirely" name="position" src="d0_s12" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles included in fruit;</text>
      <biological_entity id="o2112" name="style" name_original="styles" src="d0_s13" type="structure" />
      <biological_entity id="o2113" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
      <relation from="o2112" id="r184" name="included in" negation="false" src="d0_s13" to="o2113" />
    </statement>
    <statement id="d0_s14">
      <text>stigma papillae in narrow subapical band.</text>
      <biological_entity constraint="stigma" id="o2114" name="papilla" name_original="papillae" src="d0_s14" type="structure" />
      <biological_entity constraint="subapical" id="o2115" name="band" name_original="band" src="d0_s14" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o2114" id="r185" name="in" negation="false" src="d0_s14" to="o2115" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.6–0.7 mm, smooth or wrinkled.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o2116" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s15" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2117" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed runnels or near small streams, oak-coniferous woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="runnels" modifier="exposed" />
        <character name="habitat" value="near small streams" />
        <character name="habitat" value="oak-coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Lithophragma trifoliatum is known from the western slope of the Cascade Range and Sierra Nevada in California. It is closely related to L. parviflorum and was considered a variety by P. E. Elvander (1993). The shape of the hypanthium, the fragrant flowers, the relatively large pink petals, and the relatively large seeds are distinctive. Lithophragma trifoliatum rarely produces seed; in cultivation, self-pollination was unsuccessful (R. L. Taylor 1965). It may represent a sterile derivative of L. parviflorum that now persists by vegetative reproduction.</discussion>
  
</bio:treatment>