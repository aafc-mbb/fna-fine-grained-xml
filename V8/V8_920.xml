<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">471</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
    <other_info_on_meta type="treatment_page">470</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rhododendron</taxon_name>
    <taxon_name authority="(Pursh) Torrey" date="1824" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Middle United States,</publication_title>
      <place_in_publication>425. 1824  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus rhododendron;species arborescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065654</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Azalea</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 152. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Azalea;species arborescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 6 m, usually not rhizomatous.</text>
      <biological_entity id="o11678" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: bark smooth to vertically furrowed, shredding;</text>
      <biological_entity id="o11680" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o11681" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s1" to="vertically furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs glabrous or, very rarely, sparsely, widely scattered, unicellular and multicellular eglandular-hairy.</text>
      <biological_entity id="o11682" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o11683" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s2" value="," value_original="," />
        <character is_modifier="false" modifier="sparsely; widely" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unicellular" value_original="unicellular" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o11684" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole glabrous or multicellular eglandular-hairy;</text>
      <biological_entity id="o11685" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to obovate, 3–8 (–10.5) × 1.3–3.5 cm, thin, chartaceous, margins entire, plane, ciliate, eglandular-hairy, apex acute to obtuse, often mucronate, abaxial surface glabrous, (sometimes glaucous), adaxial surface glabrous, (lustrous).</text>
      <biological_entity id="o11686" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s5" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o11687" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o11688" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11689" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11690" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bud-scales usually glabrous abaxially, margins unicellular-ciliate.</text>
      <biological_entity constraint="floral" id="o11691" name="bud-scale" name_original="bud-scales" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually; abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11692" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="unicellular-ciliate" value_original="unicellular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–7-flowered;</text>
      <biological_entity id="o11693" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-7-flowered" value_original="3-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts similar to bud-scales.</text>
      <biological_entity id="o11694" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o11695" name="bud-scale" name_original="bud-scales" src="d0_s8" type="structure" />
      <relation from="o11694" id="r979" name="to" negation="false" src="d0_s8" to="o11695" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 6–21 mm, glabrous or stipitate-glandular-hairy, sometimes also unicellular-hairy.</text>
      <biological_entity id="o11696" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="21" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers opening after leaves, erect to horizontal, very fragrant;</text>
      <biological_entity id="o11697" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="horizontal" />
        <character is_modifier="false" modifier="very" name="odor" src="d0_s10" value="fragrant" value_original="fragrant" />
      </biological_entity>
      <biological_entity id="o11698" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <relation from="o11697" id="r980" name="opening after" negation="false" src="d0_s10" to="o11698" />
    </statement>
    <statement id="d0_s11">
      <text>calyx lobes 0.8–6 (–9) mm, surfaces and margins scattered, stipitate-glandular and/or, sometimes, eglandular-hairy;</text>
      <biological_entity constraint="calyx" id="o11699" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11700" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o11701" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla white or, sometimes, light pink (contrasting with dark-pink to red style and filaments), without blotch on upper lobe, funnelform, 30–55 mm, stipitate-glandular-hairy (hairs continuing in lines up lobes), otherwise glabrous or sparsely unicellular-hairy on outer surface, petals connate, lobes 10–24 mm, tube ± gradually expanded into lobes, 20–37 mm (equaling or much longer than lobes);</text>
      <biological_entity id="o11702" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character name="coloration" src="d0_s12" value="," value_original="," />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light pink" value_original="light pink" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="55" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="on outer surface" constraintid="o11705" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o11703" name="blotch" name_original="blotch" src="d0_s12" type="structure" />
      <biological_entity constraint="upper" id="o11704" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o11705" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o11706" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o11707" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11708" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character constraint="into lobes" constraintid="o11709" is_modifier="false" modifier="more or less gradually" name="size" src="d0_s12" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="37" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11709" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <relation from="o11702" id="r981" name="without" negation="false" src="d0_s12" to="o11703" />
      <relation from="o11703" id="r982" name="on" negation="false" src="d0_s12" to="o11704" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5, much exserted, ± unequal, 44–82 mm.</text>
      <biological_entity id="o11710" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="much" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="44" from_unit="mm" name="some_measurement" src="d0_s13" to="82" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules borne on erect pedicels, 8–17 × 4.5–8 mm, ± densely multicellular stipitate-glandular-hairy, sometimes also sparsely unicellular-hairy.</text>
      <biological_entity id="o11711" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s14" to="17" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" notes="" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="more or less densely" name="architecture" src="d0_s14" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s14" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o11712" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o11711" id="r983" name="borne on" negation="false" src="d0_s14" to="o11712" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds without distinct tails;</text>
      <biological_entity id="o11713" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity id="o11714" name="tail" name_original="tails" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o11713" id="r984" name="without" negation="false" src="d0_s15" to="o11714" />
    </statement>
    <statement id="d0_s16">
      <text>testa not dorsiventrally flattened, usually ± loose.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 26.</text>
      <biological_entity id="o11715" name="testa" name_original="testa" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not dorsiventrally" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="usually more or less" name="architecture_or_fragility" src="d0_s16" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11716" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, rocky streamsides, heath balds, swampy woods or bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="rocky streamsides" />
        <character name="habitat" value="balds" modifier="heath" />
        <character name="habitat" value="swampy woods" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Md., N.C., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Sweet or smooth azalea</other_name>
  <discussion>Rhododendron arborescens is most closely related to R. viscosum, as evidenced by their glabrous floral bud scales and flowers that appear after the leaves have expanded (K. A. Kron 1993). It can be distinguished by its glabrous branchlets, red style and filaments (which contrast with the white corollas), and distinctive seeds that lack loose, expanded testae. These two species occasionally hybridize; hybrids with R. cumberlandense also are known.</discussion>
  
</bio:treatment>