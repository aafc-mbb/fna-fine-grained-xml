<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="treatment_page">479</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Rehder" date="1932" rank="genus">kalmiopsis</taxon_name>
    <taxon_name authority="Meinke &amp; Kaye" date="2007" rank="species">fragrans</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 10, figs. 1, 2A,C, 3A, 4A,B. 2007  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus kalmiopsis;species fragrans;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065664</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect, larger plants frequently trailing, loose, to 12 (–30) dm.</text>
      <biological_entity id="o886" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity constraint="larger" id="o887" name="plant" name_original="plants" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="frequently" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="12" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs reddish to purplish, becoming gray to dark gray, puberulent and sparsely sessile-glandular, becoming glabrate.</text>
      <biological_entity id="o888" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s1" to="purplish" />
        <character char_type="range_value" from="gray" modifier="becoming" name="coloration" src="d0_s1" to="dark gray" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s1" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sweetly aromatic;</text>
      <biological_entity id="o889" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sweetly" name="odor" src="d0_s2" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–4 mm, sparsely puberulent, glandular;</text>
      <biological_entity id="o890" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade rich, deep green abaxially, pale green adaxially, elliptic to elliptic-oblong, (5–) 8–30 (–45) × 4–10 mm, base ± cuneate, margins entire, plane, apex obtuse, apiculate, surfaces glabrous or sparsely sessile dotted-glandular abaxially, moderately covered with sessile crystalline-punctate glands adaxially.</text>
      <biological_entity id="o891" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="rich" value_original="rich" />
        <character is_modifier="false" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="elliptic-oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o892" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o893" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o894" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o895" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="dotted-glandular" value_original="dotted-glandular" />
      </biological_entity>
      <biological_entity id="o896" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="coloration_or_relief" src="d0_s4" value="crystalline-punctate" value_original="crystalline-punctate" />
      </biological_entity>
      <relation from="o895" id="r95" modifier="moderately; adaxially" name="covered with" negation="false" src="d0_s4" to="o896" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect, (2–) 4–8 (–12) -flowered;</text>
      <biological_entity id="o897" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(2-)4-8(-12)-flowered" value_original="(2-)4-8(-12)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike.</text>
      <biological_entity id="o898" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–2.5 (–3.3) cm, hairy, glandular.</text>
      <biological_entity id="o899" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes reddish-pink to purple, ovate, 3–5 mm, margins ± glandular-ciliate;</text>
      <biological_entity id="o900" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o901" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="reddish-pink" name="coloration" src="d0_s8" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o902" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pale reddish purple to deep pink, 16–28 (–33) mm diam., petal ridges connected within corolla lobes, connate ca. 1/3 their lengths, abaxial surface ± puberulent and glandular toward apex (throat mostly glabrous);</text>
      <biological_entity id="o903" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o904" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale reddish" value_original="pale reddish" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s9" to="deep pink" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s9" to="33" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s9" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="petal" id="o905" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character constraint="within corolla lobes" constraintid="o906" is_modifier="false" name="fusion" src="d0_s9" value="connected" value_original="connected" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s9" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o906" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o907" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character constraint="toward apex" constraintid="o908" is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o908" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>filaments 7–16 mm, with yellowish cilia densely tufted basally;</text>
      <biological_entity id="o909" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o910" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o911" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="densely; basally" name="arrangement_or_pubescence" src="d0_s10" value="tufted" value_original="tufted" />
      </biological_entity>
      <relation from="o910" id="r96" name="with" negation="false" src="d0_s10" to="o911" />
    </statement>
    <statement id="d0_s11">
      <text>anthers purple, oblong, 0.7–1.8 mm;</text>
      <biological_entity id="o912" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o913" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style dimorphic, 11–15 mm (long form), 5–8 mm (short form);</text>
      <biological_entity id="o914" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o915" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary puberulent, dotted-glandular.</text>
      <biological_entity id="o916" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o917" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s13" value="dotted-glandular" value_original="dotted-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 0.3–0.7 mm.</text>
      <biological_entity id="o918" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tuffaceous outcrops, within shaded, mesic, coniferous forests, open ridges, bare rock or shallow soil at bases of cliffs or boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tuffaceous outcrops" constraint="within shaded , mesic , coniferous forests" />
        <character name="habitat" value="shaded" />
        <character name="habitat" value="mesic" />
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="ridges" modifier="open" />
        <character name="habitat" value="bare rock" />
        <character name="habitat" value="shallow soil" />
        <character name="habitat" value="bases" modifier="at" constraint="of cliffs or boulders" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="boulders" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Umpqua kalmiopsis</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In cultivation, Kalmiopsis fragrans has sometimes been sold or labeled as the “LePiniec” form of Kalmiopsis. In the wild, it has a narrower geographic range (restricted to Douglas County in the southern Cascade Mountains of Oregon) and is significantly rarer than K. leachiana.</discussion>
  
</bio:treatment>