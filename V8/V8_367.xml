<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="species">abramsii</taxon_name>
    <taxon_name authority="(Eastwood) Moran" date="1957" rank="subspecies">murina</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>14: 106. 1957  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species abramsii;subspecies murina;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092072</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">murina</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 20: 147. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dudleya;species murina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices simple or 1–3-branched, 1–3 cm diam.</text>
      <biological_entity id="o24094" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="1-3-branched" value_original="1-3-branched" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes 1–5;</text>
      <biological_entity id="o24095" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o24096" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblong-lanceolate or tapering from base, subterete, 5–10 × 0.5–2 cm.</text>
      <biological_entity id="o24097" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24098" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="from base" constraintid="o24099" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24099" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: floral shoots 5–25 × 0.2–0.6 cm;</text>
      <biological_entity id="o24100" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="floral" id="o24101" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximalmost leaf-blades 10–30 mm;</text>
      <biological_entity id="o24102" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="proximalmost" id="o24103" name="blade-leaf" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 2–3, simple.</text>
      <biological_entity id="o24104" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o24105" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–5 mm.</text>
      <biological_entity id="o24106" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 4–6 × 4–5 mm;</text>
      <biological_entity id="o24107" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24108" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals connate 1.5–3 mm, straw yellow, marked with purplish red, 9–14 × 3–3.5 mm, tips erect.</text>
      <biological_entity id="o24109" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24110" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="straw yellow" value_original="straw yellow" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 34.</text>
      <biological_entity id="o24111" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24112" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <other_name type="common_name">San Luis Obispo dudleya</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies murina is locally common on serpentine soil near San Luis Obispo.</discussion>
  
</bio:treatment>