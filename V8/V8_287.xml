<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Small in N. L. Britton et al." date="1905" rank="species">radiata</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al.,  N. Amer. Fl.</publication_title>
      <place_in_publication>22: 128. 1905,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species radiata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092019</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Stephan ex Sternberg" date="unknown" rank="species">exilis</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Saxifrag., suppl.</publication_title>
      <place_in_publication>1: 8, plate 3, fig. 1. 1821,</place_in_publication>
      <other_info_on_pub>not Pollini 1816</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species exilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in tufts, not stoloniferous, weakly rhizomatous (with bulbils at leaf-bases).</text>
      <biological_entity id="o18654" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18655" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o18654" id="r1564" name="in" negation="false" src="d0_s0" to="o18655" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline (basal ephemeral);</text>
      <biological_entity id="o18656" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole flattened, 5–15 mm;</text>
      <biological_entity id="o18657" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade reniform, 5–7-lobed usually more than halfway to midvein (cauline less-lobed distally), 5–20 mm, thin to slightly fleshy, margins entire, eciliate, apex obtuse to acute, surfaces glabrous or glabrate to ± long-hairy.</text>
      <biological_entity id="o18658" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o18659" name="midvein" name_original="midvein" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity id="o18660" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o18661" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o18662" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s3" to="more or less long-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–5-flowered cymes, 5–18 cm, purple-tipped stipitate-glandular;</text>
      <biological_entity id="o18663" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="18" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o18664" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts petiolate.</text>
      <biological_entity id="o18665" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals erect, (sometimes purplish), ovate to lanceolate, margins glandular-ciliate or eciliate, surfaces glabrous;</text>
      <biological_entity id="o18666" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18667" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o18668" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-ciliate" value_original="glandular-ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o18669" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, sometimes with purple veins, not spotted, obovate to narrowly oblanceolate, 7–15 mm, longer than sepals;</text>
      <biological_entity id="o18670" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18671" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="coloration" notes="" src="d0_s7" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="narrowly oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character constraint="than sepals" constraintid="o18673" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o18672" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o18673" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <relation from="o18671" id="r1565" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o18672" />
    </statement>
    <statement id="d0_s8">
      <text>ovary 1/4–1/3 inferior.</text>
      <biological_entity id="o18674" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 26, 48, 52.</text>
      <biological_entity id="o18675" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s8" to="1/3" />
        <character is_modifier="false" name="position" src="d0_s8" value="inferior" value_original="inferior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18676" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="26" value_original="26" />
        <character name="quantity" src="d0_s9" value="48" value_original="48" />
        <character name="quantity" src="d0_s9" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Snow-bed meadows, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="snow-bed meadows" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Asia (Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  
</bio:treatment>