<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">obtusatum</taxon_name>
    <taxon_name authority="(R. T. Clausen) H. Ohba" date="2007" rank="variety">boreale</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 889. 2007,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species obtusatum;variety boreale</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092137</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">obtusatum</taxon_name>
    <taxon_name authority="R. T. Clausen" date="unknown" rank="subspecies">boreale</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>69: 32. 1942</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sedum;species obtusatum;subspecies boreale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rosettes dense, internodes not visible.</text>
      <biological_entity id="o14509" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o14510" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s0" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 4–9 (–13) mm.</text>
      <biological_entity id="o14511" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s1" to="13" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s1" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering shoots 9–12 cm.</text>
      <biological_entity id="o14512" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s2" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 3.5–4.5 mm, apex obtuse or rarely acute;</text>
      <biological_entity id="o14513" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o14514" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14515" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals white, greenish or creamy white, or pale orange suffused with pink, 6–9 × 2.4–3.8 mm;</text>
      <biological_entity id="o14516" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o14517" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="suffused with pink" value_original="suffused with pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s4" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers 1.2–1.5 mm.</text>
      <biological_entity id="o14518" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o14519" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock slides in crevices, ledges and open areas of siliceous argillite, quartz porphyry, andesite, or granite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="crevices" modifier="slides in" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="open areas" constraint="of siliceous argillite , quartz porphyry , andesite , or granite" />
        <character name="habitat" value="siliceous argillite" />
        <character name="habitat" value="quartz porphyry" />
        <character name="habitat" value="andesite" />
        <character name="habitat" value="granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34b.</number>
  <discussion>Variety boreale occurs in the northern Sierra Nevada and southern Cascade Mountains.</discussion>
  
</bio:treatment>