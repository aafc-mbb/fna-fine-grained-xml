<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard A. Lis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="(de Candolle) Opiz" date="1852" rank="genus">JOVIBARBA</taxon_name>
    <place_of_publication>
      <publication_title>Seznam,</publication_title>
      <place_in_publication>54. 1852  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus JOVIBARBA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin Jovis, Jupiter, and barba, beard, alluding to fringed petals</other_info_on_name>
    <other_info_on_name type="fna_id">116846</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, monocarpic, not viviparous, 0.8–2.5 dm, glabrous.</text>
      <biological_entity id="o16033" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (compressed), not branched, succulent.</text>
      <biological_entity id="o16034" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, forming basal rosette, alternate, sessile, not connate basally;</text>
      <biological_entity id="o16035" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16036" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <relation from="o16035" id="r1364" name="forming" negation="false" src="d0_s2" to="o16036" />
    </statement>
    <statement id="d0_s3">
      <text>blade oblong to obovate, laminar, 2–4 (–6) cm, succulent, base not spurred, margins entire, sometimes ciliate;</text>
      <biological_entity id="o16037" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="obovate" />
        <character is_modifier="false" name="position" src="d0_s3" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o16038" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o16039" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins not conspicuous.</text>
      <biological_entity id="o16040" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal cymes.</text>
      <biological_entity id="o16041" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o16042" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels absent or to 1 mm.</text>
      <biological_entity id="o16043" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s6" value="0-1 mm" value_original="0-1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect, 6–7-merous;</text>
      <biological_entity id="o16044" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="6-7-merous" value_original="6-7-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals connate basally, all alike;</text>
      <biological_entity id="o16045" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s8" value="alike" value_original="alike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals erect, distinct, greenish white or greenish yellow to yellow, margins fimbrillate;</text>
      <biological_entity id="o16046" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="greenish yellow" name="coloration" src="d0_s9" to="yellow" />
      </biological_entity>
      <biological_entity id="o16047" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="fimbrillate" value_original="fimbrillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o16048" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o16050" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o16049" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o16050" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o16050" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectary disc quadrate;</text>
      <biological_entity constraint="nectary" id="o16051" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 2 times as many as sepals;</text>
      <biological_entity id="o16052" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="sepal" constraintid="o16053" is_modifier="false" name="size_or_quantity" src="d0_s12" value="2 times as many as sepals" />
      </biological_entity>
      <biological_entity id="o16053" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments not adnate to petals;</text>
      <biological_entity id="o16054" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="to petals" constraintid="o16055" is_modifier="false" modifier="not" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o16055" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pistils erect, connate;</text>
      <biological_entity id="o16056" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary base rounded;</text>
      <biological_entity constraint="ovary" id="o16057" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 4–5 times shorter than ovary.</text>
      <biological_entity id="o16058" name="style" name_original="styles" src="d0_s16" type="structure">
        <character constraint="ovary" constraintid="o16059" is_modifier="false" name="size_or_quantity" src="d0_s16" value="4-5 times shorter than ovary" />
      </biological_entity>
      <biological_entity id="o16059" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsular, erect, composed of basally connate pistils.</text>
      <biological_entity id="o16060" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16061" name="pistil" name_original="pistils" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s17" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o16060" id="r1365" name="composed of" negation="false" src="d0_s17" to="o16061" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds (brown), ellipsoid, ribbed.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 19.</text>
      <biological_entity id="o16062" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="x" id="o16063" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Wis., Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Hen-and-chickens or</other_name>
  <other_name type="common_name">chicks</other_name>
  <discussion>Sempervivum Linnaeus sect. Jovibarba de Candolle in A. P. de Candolle and A. L. P. P. de Candolle, Prodr. 3: 413. 1828</discussion>
  <discussion>Species 3 (1 in the flora).</discussion>
  <discussion>Segregation of Jovibarba from Sempervivum has been problematic and uncertain for over a century. The uncertainty has centered upon whether Jovibarba should be treated as a section of Sempervivum or as a genus. Recent work has shown that the two are closely related (M. E. Mort et al. 2001). J. Parnell and C. Favarger (1992) maintained Jovibarba at the generic level; H. ’t Hart et al. (2003) treated it at sectional rank within Sempervivum. R. C. H. J. van Ham and ’t Hart (1998) did not include any accessions of Jovibarba in their study of the Crassulaceae using chloroplast DNA.</discussion>
  <discussion>Taxa assigned to Jovibarba generally have been segregated from Sempervivum based on: (1) the erect petals in Jovibarba versus reflexed petals in Sempervivum; (2) 6(–7)-merous flowers in Jovibarba versus 8–16-merous flowers in Sempervivum; (3) fimbrillate petals in Jovibarba versus entire petals in Sempervivum. J. Parnell (1991) examined pollen morphology and ultrastructure and found significant differences between Jovibarba and Sempervivum and concluded that these palynological differences supported retention of Jovibarba at the generic level. H. ’t Hart et al. (2003) discussed their decision to submerge Jovibarba into Sempervivum and discounted the floral morphology differences as being insignificant relative to similarities. They did not discuss, or cite, the palynological differences presented by Parnell, which are congruent with the character-state differences in floral morphology, and support segregation of Jovibarba from Sempervivum.</discussion>
  
</bio:treatment>