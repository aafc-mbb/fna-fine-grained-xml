<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary D. Wallace</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">391</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1858" rank="genus">ALLOTROPA</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.],  Pacif. Railr. Rep.</publication_title>
      <place_in_publication>6(3): 81. 1858  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus allotropa;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek allos, other or different, and tropos, turn or direction, alluding to inflorescence</other_info_on_name>
    <other_info_on_name type="fna_id">101143</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, achlorophyllous, heterotrophic.</text>
      <biological_entity id="o18452" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="achlorophyllous" value_original="achlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="heterotrophic" value_original="heterotrophic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems absent.</text>
      <biological_entity id="o18453" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves absent.</text>
      <biological_entity id="o18454" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes, erect at emergence from soil, axis fleshy and fibrous, persistent after seed dispersal, white with red to maroon vertical stripes, 0.5–1 cm diam. proximal to proximalmost flower.</text>
      <biological_entity constraint="inflorescences" id="o18455" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character constraint="at emergence" constraintid="o18456" is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o18456" name="emergence" name_original="emergence" src="d0_s3" type="structure" />
      <biological_entity id="o18457" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o18458" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character constraint="after seed" constraintid="o18459" is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="with stripes" constraintid="o18460" is_modifier="false" name="coloration" notes="" src="d0_s3" value="white" value_original="white" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18459" name="seed" name_original="seed" src="d0_s3" type="structure" />
      <biological_entity id="o18460" name="stripe" name_original="stripes" src="d0_s3" type="structure">
        <character char_type="range_value" from="red" is_modifier="true" name="coloration" src="d0_s3" to="maroon" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="diam" id="o18461" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o18462" name="flower" name_original="flower" src="d0_s3" type="structure" />
      <relation from="o18456" id="r1543" name="from" negation="false" src="d0_s3" to="o18457" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels erect, somewhat elongated in fruit;</text>
      <biological_entity id="o18463" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o18464" is_modifier="false" modifier="somewhat" name="length" src="d0_s4" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o18464" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracteoles absent.</text>
      <biological_entity id="o18465" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers radially symmetric, erect to spreading;</text>
      <biological_entity id="o18466" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals absent, rarely 2–5, distinct, linear to filiform;</text>
      <biological_entity id="o18467" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="2" modifier="rarely" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5, distinct, white, without basal tubercles, (surfaces glabrous), corolla crateriform;</text>
      <biological_entity id="o18468" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18469" name="tubercle" name_original="tubercles" src="d0_s8" type="structure" />
      <biological_entity id="o18470" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="crateriform" value_original="crateriform" />
      </biological_entity>
      <relation from="o18468" id="r1544" name="without" negation="false" src="d0_s8" to="o18469" />
    </statement>
    <statement id="d0_s9">
      <text>intrastaminal nectary disc present;</text>
      <biological_entity constraint="nectary" id="o18471" name="disc" name_original="disc" src="d0_s9" type="structure" constraint_original="intrastaminal nectary">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10, exserted;</text>
      <biological_entity id="o18472" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments terete, glabrous;</text>
      <biological_entity id="o18473" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers ovoid, without awns, without tubules, dehiscent by 2 oval slits;</text>
      <biological_entity id="o18474" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character constraint="by slits" constraintid="o18477" is_modifier="false" name="dehiscence" notes="" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o18475" name="awn" name_original="awns" src="d0_s12" type="structure" />
      <biological_entity id="o18476" name="tubule" name_original="tubules" src="d0_s12" type="structure" />
      <biological_entity id="o18477" name="slit" name_original="slits" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s12" value="oval" value_original="oval" />
      </biological_entity>
      <relation from="o18474" id="r1545" name="without" negation="false" src="d0_s12" to="o18475" />
      <relation from="o18474" id="r1546" name="without" negation="false" src="d0_s12" to="o18476" />
    </statement>
    <statement id="d0_s13">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o18478" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 5-locular;</text>
    </statement>
    <statement id="d0_s15">
      <text>placentation axile;</text>
      <biological_entity id="o18479" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style straight, stout;</text>
      <biological_entity id="o18480" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s16" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma peltate, without subtending ring of hairs.</text>
      <biological_entity id="o18481" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="peltate" value_original="peltate" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o18482" name="ring" name_original="ring" src="d0_s17" type="structure" />
      <biological_entity id="o18483" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <relation from="o18481" id="r1547" name="without" negation="false" src="d0_s17" to="o18482" />
      <relation from="o18482" id="r1548" name="part_of" negation="false" src="d0_s17" to="o18483" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsular, erect, dehiscence basipetally loculicidal, no cobwebby tissue exposed by splitting valves at dehiscence.</text>
      <biological_entity id="o18484" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basipetally" name="dehiscence" src="d0_s18" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
      <biological_entity id="o18485" name="tissue" name_original="tissue" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="no" value_original="no" />
        <character is_modifier="true" name="pubescence" src="d0_s18" value="cobwebby" value_original="cobwebby" />
        <character constraint="by valves" constraintid="o18486" is_modifier="false" name="prominence" src="d0_s18" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o18486" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture_or_dehiscence" src="d0_s18" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 100+, fusiform, mostly narrowly winged.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 13.</text>
      <biological_entity id="o18487" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s19" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s19" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="mostly narrowly" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o18488" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Species 1: w North America.</discussion>
  <discussion>Species 1</discussion>
  <references>
    <reference>Copeland, H. F. 1938. The structure of Allotropa. Madroño 4: 137–153.</reference>
  </references>
  
</bio:treatment>