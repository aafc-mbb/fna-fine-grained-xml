<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">391</other_info_on_meta>
    <other_info_on_meta type="illustration_page">390</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="Torrey" date="1851" rank="genus">sarcodes</taxon_name>
    <taxon_name authority="Torrey" date="1851" rank="species">sanguinea</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Assoc. Advancem. Sci.</publication_title>
      <place_in_publication>4: 193. 1851  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus sarcodes;species sanguinea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220011980</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences 1.5–5 dm, glandular-hairy;</text>
      <biological_entity id="o26366" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bracts red, 1–8 × 5–15 mm.</text>
      <biological_entity id="o26367" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s1" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 8–15 × 4–6 mm;</text>
      <biological_entity id="o26368" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o26369" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla 12–18 mm, base saccate, lobes reflexed, rounded to blunt;</text>
      <biological_entity id="o26370" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o26371" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26372" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o26373" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens 8–12 mm;</text>
      <biological_entity id="o26374" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o26375" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers inverted at anthesis, 3–4 mm;</text>
      <biological_entity id="o26376" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26377" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s5" value="inverted" value_original="inverted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 5–7 × 6–9 mm, glabrous;</text>
      <biological_entity id="o26378" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26379" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 5–8 × 1–2 mm;</text>
      <biological_entity id="o26380" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26381" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigma reddish, 2–3 mm diam.</text>
      <biological_entity id="o26382" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26383" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules persistent to disintegrating, 10–25 mm diam., walls brittle, segments not separating.</text>
      <biological_entity id="o26384" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="disintegrating" value_original="disintegrating" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26385" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity id="o26386" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s9" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.5–1 mm diam. 2n = 64.</text>
      <biological_entity id="o26387" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26388" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous or mixed-deciduous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="mixed-deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Snow plant</other_name>
  
</bio:treatment>