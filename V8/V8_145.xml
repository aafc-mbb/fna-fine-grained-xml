<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chrysosplenium</taxon_name>
    <taxon_name authority="Franchet &amp; Savatier" date="1878" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl. Jap.</publication_title>
      <place_in_publication>2: 356. 1878  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus chrysosplenium;species wrightii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065925</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysosplenium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alternifolium</taxon_name>
    <taxon_name authority="(Franchet &amp; Savatier) Sutó" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>genus Chrysosplenium;species alternifolium;variety wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stolons white, tan, or yellowish, 1–2.5 (–3) mm diam., usually densely villous, hairs reddish-brown.</text>
      <biological_entity id="o26389" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26390" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems erect, branching in distal 1/10–1/4 (–1/3), (1.5–) 2–11 (–16) cm, sparsely to densely villous especially proximally, hairs reddish-brown.</text>
      <biological_entity id="o26391" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in distal 1/10-1/4(-1/3) , (1.5-)2-11(-16) cm" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="sparsely to densely; especially proximally; proximally" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26392" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, fleshy.</text>
      <biological_entity id="o26393" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stolon leaves: petiole (7–) 25–55 (–68) mm, sparsely to densely villous, hairs reddish-brown;</text>
      <biological_entity constraint="stolon" id="o26394" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o26395" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="68" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s3" to="55" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26396" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade depressed-ovate to reniform, 4–19 × 7–24 mm, base cordate or truncate, margins 5–9-crenate, usually purple-spotted, usually ciliate, hairs white, surfaces glabrous or sparsely villous, especially near petiole, hairs usually white, brown, or reddish-brown, sometimes purple.</text>
      <biological_entity constraint="stolon" id="o26397" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26398" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="depressed-ovate" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26399" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o26400" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="5-9-crenate" value_original="5-9-crenate" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26401" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26402" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26403" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o26404" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o26402" id="r2210" modifier="especially" name="near" negation="false" src="d0_s4" to="o26403" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves absent or 1 (–2);</text>
      <biological_entity constraint="cauline" id="o26405" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 3–16 (–24) mm, glabrous or sparsely villous, hairs reddish-brown;</text>
      <biological_entity id="o26406" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="24" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26407" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade ovate to broadly ovate, reniform, or flabellate, 3.5–12 × 4.5–19 mm, base cuneate, margins 3–7-crenate, usually glabrous, sometimes villous, hairs white, surfaces glabrous.</text>
      <biological_entity id="o26408" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly ovate reniform or flabellate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly ovate reniform or flabellate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly ovate reniform or flabellate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26409" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o26410" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="3-7-crenate" value_original="3-7-crenate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26411" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26412" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, 3–30-flowered, compact cymes;</text>
      <biological_entity id="o26413" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-30-flowered" value_original="3-30-flowered" />
      </biological_entity>
      <biological_entity id="o26414" name="cyme" name_original="cymes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts green or greenish yellow, usually purple-spotted, foliaceous, usually ovate to flabellate or, rarely, elliptic, 3–13 × 2–15 mm, margins subentire or 3–7-crenate.</text>
      <biological_entity id="o26415" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s9" to="flabellate" />
        <character name="shape" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26416" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="3-7-crenate" value_original="3-7-crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or 0.1–1.5 mm.</text>
      <biological_entity id="o26417" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0.1-1.5 mm" value_original="0.1-1.5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: hypanthium green or greenish yellow, sometimes purple-spotted distally, turbinate, 1.2–2.2 × 1.8–3.5 mm, glabrous;</text>
      <biological_entity id="o26418" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26419" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="sometimes; distally" name="coloration" src="d0_s11" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals spreading, greenish yellow, purple, or reddish orange, usually purple-spotted throughout or distally, rarely not spotted, broadly ovate to depressed-ovate or nearly round, 0.9–2.3 × 1.2–3.1 mm, apex obtuse to rounded;</text>
      <biological_entity id="o26420" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26421" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" modifier="usually; throughout; throughout; distally" name="coloration" src="d0_s12" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="rarely not" name="coloration" src="d0_s12" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s12" to="depressed-ovate or nearly round" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s12" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26422" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary disc prominent, yellow or purple, 8-lobed;</text>
      <biological_entity id="o26423" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectary" id="o26424" name="disc" name_original="disc" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="8-lobed" value_original="8-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 8, 0.7–0.9 mm;</text>
      <biological_entity id="o26425" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o26426" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow or purple, 0.2–0.3 × 0.2–0.3 mm;</text>
      <biological_entity id="o26427" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o26428" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s15" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 0.5–0.9 mm.</text>
      <biological_entity id="o26429" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o26430" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds (2–) 5–12, reddish-brown, ovoid to ellipsoid, (0.8–) 1–1.3 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 24.</text>
      <biological_entity id="o26431" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s17" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s17" to="12" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="ellipsoid" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26432" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly tundra and stream banks, boulder fields, scree slopes, solifluction terraces, seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly tundra" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="boulder fields" />
        <character name="habitat" value="scree slopes" />
        <character name="habitat" value="solifluction terraces" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska; e Asia (Russia: Kamtchatka, e Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russia: Kamtchatka)" establishment_means="native" />
        <character name="distribution" value="e Asia (e Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Wright’s golden-saxifrage</other_name>
  <discussion>H. Hara (1957) recognized two varieties of Chrysosplenium wrightii. He considered var. wrightii to be restricted to Herald Island, the type locality, and var. beringianum (Rose) H. Hara to be the element in southern Alaska, St. Matthew Island and the Pribiloff Islands, and across the Aleutian Islands to Kamtchatka. He distinguished the varieties by the indumentum of the flowering stems and thickness of the leaf blades. These characters vary too widely among populations to warrant recognition of infraspecific taxa.</discussion>
  
</bio:treatment>