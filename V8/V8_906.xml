<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="treatment_page">463</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rhododendron</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">catawbiense</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 258. 1803,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus rhododendron;species catawbiense;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065641</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 3.5 (–6) m, sometimes rhizomatous.</text>
      <biological_entity id="o24179" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3.5" to_unit="m" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3.5" to_unit="m" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: bark smooth to vertically furrowed, shredding;</text>
      <biological_entity id="o24181" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24182" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s1" to="vertically furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs multicellular eglandular-hairy (hairs branched basally, crisp/matted), glabrate in age.</text>
      <biological_entity id="o24183" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o24184" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character constraint="in age" constraintid="o24185" is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o24185" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent;</text>
      <biological_entity id="o24186" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole multicellular-hairy (hairs ± branched), often glabrescent;</text>
      <biological_entity id="o24187" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="multicellular-hairy" value_original="multicellular-hairy" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic to obovate or slightly ovate, (5–) 6–12 (–17) × (2.5–) 3.5–6 (–7.7) cm (length/width ratio 1.3–3.5), thick, coriaceous, margins entire, revolute to plane, glabrous or sparsely hairy along margins (hairs branched, ephemeral), apex rounded/mucronate to obtuse or acute, surfaces sparsely eglandular-hairy (hairs basally branched, crisped, quickly deciduous), abaxial surface minutely, obscurely papillose.</text>
      <biological_entity id="o24188" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="obovate or slightly ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="17" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_width" src="d0_s5" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="7.7" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o24189" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="revolute" name="shape" src="d0_s5" to="plane" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o24190" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24190" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o24191" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded/mucronate" name="shape" src="d0_s5" to="obtuse or acute" />
      </biological_entity>
      <biological_entity id="o24192" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24193" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bud-scales multicellular stipitate-glandular-hairy, eglandular-hairy (hairs basally branched, crisped), and unicellular-hairy (hairs short-to-elongate), margins eglandular-hairy (hairs branched).</text>
      <biological_entity constraint="floral" id="o24194" name="bud-scale" name_original="bud-scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o24195" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 12–20-flowered;</text>
      <biological_entity id="o24196" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="12-20-flowered" value_original="12-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts similar to bud-scales.</text>
      <biological_entity id="o24197" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o24198" name="bud-scale" name_original="bud-scales" src="d0_s8" type="structure" />
      <relation from="o24197" id="r2006" name="to" negation="false" src="d0_s8" to="o24198" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 10–50 mm, sparsely to moderately multicellular eglandular-hairy (hairs ferruginous, branched, crisped), glabrate in age.</text>
      <biological_entity id="o24199" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s9" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character constraint="in age" constraintid="o24200" is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o24200" name="age" name_original="age" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers opening after development of leaves (of flowering shoots), erect to horizontal, fragrant;</text>
      <biological_entity id="o24201" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="development" notes="" src="d0_s10" to="horizontal" />
        <character is_modifier="false" name="odor" src="d0_s10" value="fragrant" value_original="fragrant" />
      </biological_entity>
      <biological_entity id="o24202" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <relation from="o24201" id="r2007" name="opening after" negation="false" src="d0_s10" to="o24202" />
    </statement>
    <statement id="d0_s11">
      <text>calyx lobes 0.5–1.7 mm, glabrous or eglandular-hairy;</text>
      <biological_entity constraint="calyx" id="o24203" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla deep pink to purple, rarely white, usually with yellowish-green spots on upper lobe, campanulate, 27–50 mm, glabrous on outer surface, petals connate, lobes 15–30 mm, tube gradually expanding into lobes, 12–28 mm;</text>
      <biological_entity id="o24204" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="deep" value_original="deep" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s12" to="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character constraint="on upper lobe" constraintid="o24205" is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="yellowish-green spots" value_original="yellowish-green spots" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="27" from_unit="mm" name="some_measurement" src="d0_s12" to="50" to_unit="mm" />
        <character constraint="on outer surface" constraintid="o24206" is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24205" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o24206" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o24207" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o24208" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24209" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character constraint="into lobes" constraintid="o24210" is_modifier="false" modifier="gradually" name="size" src="d0_s12" value="expanding" value_original="expanding" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24210" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 10, included, ± unequal, 19–39 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>(ovary multicellular eglandular-hairy).</text>
      <biological_entity id="o24211" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s13" to="39" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules borne on erect pedicels, 10–23 × 3.5–7 mm, eglandular-hairy (hairs ferruginous, branched).</text>
      <biological_entity id="o24212" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="" src="d0_s15" to="23" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" notes="" src="d0_s15" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o24213" name="pedicel" name_original="pedicels" src="d0_s15" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s15" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o24212" id="r2008" name="borne on" negation="false" src="d0_s15" to="o24213" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds without distinct tails, flattened portion of testa well developed at each end;</text>
      <biological_entity id="o24214" name="seed" name_original="seeds" src="d0_s16" type="structure" />
      <biological_entity id="o24215" name="tail" name_original="tails" src="d0_s16" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o24216" name="portion" name_original="portion" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o24217" name="testa" name_original="testa" src="d0_s16" type="structure" />
      <biological_entity id="o24218" name="end" name_original="end" src="d0_s16" type="structure" />
      <relation from="o24214" id="r2009" name="without" negation="false" src="d0_s16" to="o24215" />
      <relation from="o24216" id="r2010" name="part_of" negation="false" src="d0_s16" to="o24217" />
      <relation from="o24216" id="r2011" modifier="well" name="developed at each" negation="false" src="d0_s16" to="o24218" />
    </statement>
    <statement id="d0_s17">
      <text>testa expanded, dorsiventrally flattened, loose.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 26.</text>
      <biological_entity id="o24219" name="testa" name_original="testa" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s17" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24220" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, ridges, and balds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="balds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., N.C., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Mountain rosebay</other_name>
  <other_name type="common_name">purple laurel</other_name>
  <discussion>Large-flowered, large-leaved plants of Rhododendron catawbiense from eastern North Carolina have been named forma insularis Coker. Rare hybrids with R. maximum have been reported by A. E. Radford et al. (1968) (R. ×wellesleyanum Waterer ex Rehder); we have seen no specimens.</discussion>
  
</bio:treatment>