<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="species">abramsii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">abramsii</taxon_name>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species abramsii;subspecies abramsii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092071</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices 2–5-branched, 1–1.5 cm diam.</text>
      <biological_entity id="o13825" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="2-5-branched" value_original="2-5-branched" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes 3–50+;</text>
      <biological_entity id="o13826" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13827" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblong, terete, 2–6 (–11) × 0.6–1.2 (–1.8) cm.</text>
      <biological_entity id="o13828" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13829" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s2" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: floral shoots 2–15 × 0.1–0.3 cm;</text>
      <biological_entity id="o13830" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="floral" id="o13831" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximalmost leaf-blades 4–15 (–40) mm;</text>
      <biological_entity id="o13832" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="proximalmost" id="o13833" name="blade-leaf" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 2–3, simple.</text>
      <biological_entity id="o13834" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o13835" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–5 mm.</text>
      <biological_entity id="o13836" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 3–5 × 3–4 mm;</text>
      <biological_entity id="o13837" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13838" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals connate 2–4.5 mm, pale-yellow, red-lineolate, 8–12.5 × 2–2.5 (–3) mm, tips often strongly outcurved.</text>
      <biological_entity id="o13839" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13840" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="red-lineolate" value_original="red-lineolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="12.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 34.</text>
      <biological_entity id="o13841" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often strongly" name="shape" src="d0_s8" value="outcurved" value_original="outcurved" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13842" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices in mountains, especially on granite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" modifier="rock" />
        <character name="habitat" value="mountains" modifier="in" />
        <character name="habitat" value="granite" modifier="especially on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja Calfornia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja Calfornia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9a.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies abramsii occurs in the Peninsular Ranges of southern California and northern Baja California. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>