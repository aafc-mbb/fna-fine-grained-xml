<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">54</other_info_on_meta>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
    <other_info_on_meta type="illustration_page">60</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">micranthes</taxon_name>
    <taxon_name authority="(Michaux) Small" date="1903" rank="species">virginiensis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>501. 1903  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus micranthes;species virginiensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065890</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">virginiensis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 269. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species virginiensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or in clumps, with bulbils on caudices.</text>
      <biological_entity id="o15014" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="in clumps" value_original="in clumps" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15015" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <biological_entity id="o15016" name="bulbil" name_original="bulbils" src="d0_s0" type="structure" />
      <biological_entity id="o15017" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o15014" id="r1285" name="in" negation="false" src="d0_s0" to="o15015" />
      <relation from="o15014" id="r1286" name="with" negation="false" src="d0_s0" to="o15016" />
      <relation from="o15016" id="r1287" name="on" negation="false" src="d0_s0" to="o15017" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o15018" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole flattened, 1–9 cm;</text>
      <biological_entity id="o15019" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to elliptic, 2–8 cm, ± fleshy, base attenuate, margins irregularly crenate to serrate, ciliate, surfaces sparsely to ± densely stipitate-glandular and tangled, reddish brown-hairy, adaxially glabrescent.</text>
      <biological_entity id="o15020" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o15021" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o15022" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate to serrate" value_original="crenate to serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15023" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to more or less irregularly" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sparsely to more or less irregularly; densely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="tangled" value_original="tangled" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish brown-hairy" value_original="reddish brown-hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 30+-flowered, (flowers sometimes secund), very open, lax, ± flat-topped thyrses, 6–50 cm, proximally hairy, distally densely purple-tipped stipitate-glandular.</text>
      <biological_entity id="o15024" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="30+-flowered" value_original="30+-flowered" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o15025" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally densely" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals erect to ascending (even in fruit), ovate to triangular;</text>
      <biological_entity id="o15026" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15027" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="ascending" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, not spotted, broadly oblong to elliptic, not or rarely slightly clawed, 3–6 mm, 2+ times as long as sepals;</text>
      <biological_entity id="o15028" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15029" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s6" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s6" to="elliptic" />
        <character is_modifier="false" modifier="not; rarely slightly" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o15030" is_modifier="false" name="length" src="d0_s6" value="2+ times as long as sepals" />
      </biological_entity>
      <biological_entity id="o15030" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>filaments linear, flattened;</text>
      <biological_entity id="o15031" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15032" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistils distinct almost to base;</text>
      <biological_entity id="o15033" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15034" name="pistil" name_original="pistils" src="d0_s8" type="structure">
        <character constraint="to base" constraintid="o15035" is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o15035" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary ± superior, (to 1/3 adnate hypanthium).</text>
      <biological_entity id="o15036" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15037" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules green to purplish, folliclelike.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 20 (+ 0–6 supernumeraries), 38.</text>
      <biological_entity id="o15038" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="purplish" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="folliclelike" value_original="folliclelike" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15039" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
        <character name="quantity" src="d0_s11" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides, cliffs and shaded rock outcrops, stream banks, wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="shaded rock outcrops" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="slopes" modifier="wooded" />
        <character name="habitat" value="wooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Ont., Que.; Ala., Ark., Conn., D.C., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Virginia or early saxifrage</other_name>
  <other_name type="common_name">saxifrage de Virginie</other_name>
  <discussion>Also reported for Micranthes virginiensis is 2n = 28; D. E. Soltis (1983) documented zero to six supernumerary chromosomes in this species and speculated that this report may have included eight supernumeraries.</discussion>
  
</bio:treatment>