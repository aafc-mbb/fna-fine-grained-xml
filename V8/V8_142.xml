<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chrysosplenium</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton" date="1901" rank="species">iowense</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton,  Man. Fl. N. States,</publication_title>
      <place_in_publication>483. 1901 (as iowensis)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus chrysosplenium;species iowense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065922</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysosplenium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alternifolium</taxon_name>
    <taxon_name authority="(Rydberg) B. Boivin" date="unknown" rank="variety">iowense</taxon_name>
    <taxon_hierarchy>genus Chrysosplenium;species alternifolium;variety iowense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stolons white, 0.3–1.2 mm diam., sparsely villous, hairs white or reddish-brown.</text>
      <biological_entity id="o21123" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s0" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21124" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems erect, branching in distal 1/6–1/3, 3–15 cm, glabrous.</text>
      <biological_entity id="o21125" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in distal 1/6-1/3" constraintid="o21126" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21126" name="1/6-1/3" name_original="1/6-1/3" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, membranous.</text>
      <biological_entity id="o21127" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stolon leaves: petiole 7–43 mm, sparsely villous, hairs white or reddish-brown;</text>
      <biological_entity constraint="stolon" id="o21128" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21129" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="43" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21130" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade depressed-ovate to reniform, 6–13 × 7–18 mm, base cordate, margins 5–9-crenate, not purple-spotted, glabrous or sparsely ciliate, hairs white, surfaces glabrous or sparsely villous, especially near petiole, hairs usually white, sometimes purplish.</text>
      <biological_entity constraint="stolon" id="o21131" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21132" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="depressed-ovate" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21133" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o21134" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="5-9-crenate" value_original="5-9-crenate" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21135" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21136" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21137" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o21138" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="purplish" value_original="purplish" />
      </biological_entity>
      <relation from="o21136" id="r1743" modifier="especially" name="near" negation="false" src="d0_s4" to="o21137" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 1–3;</text>
      <biological_entity constraint="cauline" id="o21139" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 3–23 (–26) mm, glabrous or villous proximally, hairs purplish;</text>
      <biological_entity id="o21140" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="26" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="23" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21141" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade depressed-ovate to reniform or flabellate, 6–17 × 9–23 mm, base cordate or, rarely, cuneate, margins 5–9-crenate, glabrous, surfaces glabrous abaxially and adaxially or, rarely, villous near petiole, hairs purplish.</text>
      <biological_entity id="o21142" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="depressed-ovate" name="shape" src="d0_s7" to="reniform or flabellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s7" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21143" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
        <character name="shape" src="d0_s7" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o21144" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="5-9-crenate" value_original="5-9-crenate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21145" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="near petiole" constraintid="o21146" is_modifier="false" modifier="adaxially; rarely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21146" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o21147" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, 3–12-flowered, compact cymes;</text>
      <biological_entity id="o21148" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-12-flowered" value_original="3-12-flowered" />
      </biological_entity>
      <biological_entity id="o21149" name="cyme" name_original="cymes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts greenish yellow, not purple-spotted, foliaceous, ovate to depressed-ovate or flabellate, 2–10 × 2–11 mm, margins 3–7-crenate.</text>
      <biological_entity id="o21150" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="depressed-ovate or flabellate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21151" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="3-7-crenate" value_original="3-7-crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or 0.1–1.2 mm.</text>
      <biological_entity id="o21152" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0.1-1.2 mm" value_original="0.1-1.2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: hypanthium yellow or greenish yellow, not purple-spotted, campanulate, 1.2–2.3 × 1.5–3 mm, glabrous;</text>
      <biological_entity id="o21153" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21154" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s11" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals spreading, yellow or greenish yellow, not purple-spotted, ovate to broadly ovate, 0.9–1.5 × 1.1–2 mm, apex obtuse to rounded;</text>
      <biological_entity id="o21155" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21156" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s12" value="purple-spotted" value_original="purple-spotted" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="broadly ovate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s12" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21157" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary disc inconspicuous, yellow, unlobed;</text>
      <biological_entity id="o21158" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectary" id="o21159" name="disc" name_original="disc" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 2–8, 0.6–0.7 mm;</text>
      <biological_entity id="o21160" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o21161" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="8" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow, 0.1–0.2 × 0.1–0.2 mm;</text>
      <biological_entity id="o21162" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o21163" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s15" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s15" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 0.3–0.4 mm.</text>
      <biological_entity id="o21164" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o21165" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds (8–) 20–30 (–56), reddish-brown, ovoid to ellipsoid, (0.5–) 0.6–0.9 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = ca. 120.</text>
      <biological_entity id="o21166" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s17" to="20" to_inclusive="false" />
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="56" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s17" to="30" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21167" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, bogs, wet meadows, stream banks, moist seeps, algific talus slopes, 500-1500 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="moist seeps" />
        <character name="habitat" value="algific talus slopes" />
        <character name="habitat" value="500-1500 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Sask.; Iowa, Minn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Iowa golden-saxifrage</other_name>
  <discussion>Iowa populations of Chrysosplenium iowense are widely disjunct from Canadian populations. The former are thought to represent Pleistocene relicts, isolated in the Driftless Area of northeastern Iowa and southeastern Minnesota. R. M. Weber (1979) studied the reproductive biology and ecology of the species in northeastern Iowa.</discussion>
  <discussion>H. Hara (1957) treated Chrysosplenium iowense as synonymous with C. alternifolium Linnaeus var. sibiricum H. Hara. J. G. Packer (1963) recognized the close similarity between C. iowense and C. alternifolium var. sibiricum but maintained the two as distinct species in the absence of a modern, detailed analysis of the C. alternifolium complex. That approach is adopted here.</discussion>
  <references>
    <reference>Weber. R. M. 1979. Species Biology of Chrysosplenium iowense. Master’s thesis. University of Northern Iowa.</reference>
  </references>
  
</bio:treatment>