<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">sapotaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chrysophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">oliviforme</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">oliviforme</taxon_name>
    <taxon_hierarchy>family sapotaceae;genus chrysophyllum;species oliviforme;subspecies oliviforme</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092179</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Twigs russet, older twigs grayish.</text>
      <biological_entity id="o13662" name="twig" name_original="twigs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="russet" value_original="russet" />
      </biological_entity>
      <biological_entity id="o13663" name="twig" name_original="twigs" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 5–10 mm;</text>
      <biological_entity id="o13664" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13665" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade broadly elliptic to ovate, (13–) 30–120 × (6–) 15–40 (–67) mm, marginal vein distinct, surfaces abaxially coppery-hairy, becoming grayish with age, adaxially glossy green.</text>
      <biological_entity id="o13666" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13667" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" src="d0_s2" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" src="d0_s2" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="67" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o13668" name="vein" name_original="vein" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o13669" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s2" value="coppery-hairy" value_original="coppery-hairy" />
        <character constraint="with age" constraintid="o13670" is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" notes="" src="d0_s2" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o13670" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2–10-flowered or flowers solitary.</text>
      <biological_entity id="o13671" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-10-flowered" value_original="2-10-flowered" />
      </biological_entity>
      <biological_entity id="o13672" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 3–8 mm, sericeous-tomentose.</text>
      <biological_entity id="o13673" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous-tomentose" value_original="sericeous-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals broadly ovate, 1.5–2 mm, margins ciliate, apex ciliate;</text>
      <biological_entity id="o13674" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o13675" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13676" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13677" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 0.5–1 mm.</text>
      <biological_entity id="o13678" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13679" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Berries 14–30 mm.</text>
      <biological_entity id="o13680" name="berry" name_original="berries" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds elongate, 13–15 mm;</text>
      <biological_entity id="o13681" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hilum ca. 1/3 seed length.</text>
      <biological_entity id="o13682" name="hilum" name_original="hilum" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 52.</text>
      <biological_entity id="o13683" name="seed" name_original="seed" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o13684" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly late summer (sporadic throughout year).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly;  (sporadic throughout year)" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, scrub and wet thickets, frequently on coquina, coral, sandy and shelly soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="coquina" />
        <character name="habitat" value="coral" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="shelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-5 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="5" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Bahamas, Cuba, Hispaniola, Jamaica, Puerto Rico).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
        <character name="distribution" value="West Indies (Puerto Rico)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Chrysophyllum oliviforme is a handsome tree that is frequently cultivated.</discussion>
  
</bio:treatment>