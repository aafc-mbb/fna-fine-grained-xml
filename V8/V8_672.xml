<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
    <other_info_on_meta type="illustration_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle &amp; Sprengel" date="unknown" rank="family">styracaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">styrax</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">grandifolius</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 75. 1789 (as grandifolium)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family styracaceae;genus styrax;species grandifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092270</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 6 m, often suckering extensively from roots.</text>
      <biological_entity id="o21538" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character constraint="from roots" constraintid="o21540" is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character constraint="from roots" constraintid="o21540" is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o21540" name="root" name_original="roots" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 4–12 mm;</text>
      <biological_entity id="o21541" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o21542" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade with 5–8 secondary-veins, obovate to broadly elliptic or broadly rhombic, 7–20 × 4–14.3 cm, largest blades on sterile shoots 5–20 cm wide, margins of at least some leaves on sterile shoots (and often fertile shoots) denticulate to serrate, rarely also lobed, longest arms of abaxial hairs 0.2–0.6 mm.</text>
      <biological_entity id="o21543" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21544" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s2" to="broadly elliptic or broadly rhombic" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s2" to="14.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21545" name="secondary-vein" name_original="secondary-veins" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity constraint="largest" id="o21546" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <biological_entity id="o21547" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21548" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o21549" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21550" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s2" to="serrate" />
      </biological_entity>
      <biological_entity constraint="longest" id="o21551" name="arm" name_original="arms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21552" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <relation from="o21544" id="r1797" name="with" negation="false" src="d0_s2" to="o21545" />
      <relation from="o21546" id="r1798" name="on" negation="false" src="d0_s2" to="o21547" />
      <relation from="o21548" id="r1799" name="part_of" negation="false" src="d0_s2" to="o21549" />
      <relation from="o21548" id="r1800" name="on" negation="false" src="d0_s2" to="o21550" />
      <relation from="o21551" id="r1801" name="part_of" negation="false" src="d0_s2" to="o21552" />
    </statement>
    <statement id="d0_s3">
      <text>False-terminal inflorescences 2–19-flowered or solitary flower, 3–11.5 cm;</text>
      <biological_entity constraint="false-terminal" id="o21553" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-19-flowered" value_original="2-19-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="11.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21554" name="flower" name_original="flower" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>axillary flowers present on at least some shoots (subtending leaves often reduced).</text>
      <biological_entity constraint="axillary" id="o21555" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o21556" name="shoot" name_original="shoots" src="d0_s4" type="structure" />
      <relation from="o21555" id="r1802" name="present on" negation="false" src="d0_s4" to="o21556" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 4–9 mm, usually shorter than calyx.</text>
      <biological_entity id="o21557" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
        <character constraint="than calyx" constraintid="o21558" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o21558" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 4–6 × 3–6 mm;</text>
      <biological_entity id="o21559" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o21560" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 10–21 mm, tube 3–5 mm, lobes 5 (–6), imbricate in bud, slightly reflexed, elliptic, 8–16 × 3–7 mm;</text>
      <biological_entity id="o21561" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o21562" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21563" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21564" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character constraint="in bud" constraintid="o21565" is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" notes="" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21565" name="bud" name_original="bud" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments distinct beyond adnation to corolla.</text>
      <biological_entity id="o21566" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o21567" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character constraint="beyond adnation" constraintid="o21568" is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o21568" name="adnation" name_original="adnation" src="d0_s8" type="structure" />
      <biological_entity id="o21569" name="corolla" name_original="corolla" src="d0_s8" type="structure" />
      <relation from="o21568" id="r1803" name="to" negation="false" src="d0_s8" to="o21569" />
    </statement>
    <statement id="d0_s9">
      <text>Nutlike fruits globose to subglobose, 8–12 × 6–8 mm (broader when 2–3-seeded), gray to yellowish gray stellate-pubescent, indehiscent or at most with 1–3 narrow longitudinal fissures, barely exposing seed (s);</text>
      <biological_entity id="o21570" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="nutlike" value_original="nutlike" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="subglobose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s9" to="yellowish gray" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character name="dehiscence" src="d0_s9" value="0-with" value_original="0-with" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o21571" name="fissure" name_original="fissures" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o21572" name="seed" name_original="seed" src="d0_s9" type="structure" />
      <relation from="o21570" id="r1804" modifier="barely" name="exposing" negation="false" src="d0_s9" to="o21572" />
    </statement>
    <statement id="d0_s10">
      <text>fruit wall 0.4–0.5 mm thick.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 32.</text>
      <biological_entity constraint="fruit" id="o21573" name="wall" name_original="wall" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21574" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Upland woods, ravines, rocky banks, bluffs, outcrops, usually sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="upland woods" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="rocky banks" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="sandy soils" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Ky., La., Miss., N.C., Ohio, S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Bigleaf snowbell</other_name>
  <discussion>Small-leaved sterile specimens of Styrax grandifolius can usually be distinguished from S. americanus by the hairs on the abaxial leaf blade surface, and the shape of the leaves (particularly those at the distal end of new sterile shoots), which are usually obovate to broadly rhombic versus usually elliptic in S. americanus.</discussion>
  
</bio:treatment>