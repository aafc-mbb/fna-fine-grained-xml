<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="genus">VILLADIA</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and J. N. Rose,  New N. Amer. Crassul.,</publication_title>
      <place_in_publication>3. 1903  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus VILLADIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Manuel M. Villada, 1841–1924, Mexican scientist</other_info_on_name>
    <other_info_on_name type="fna_id">134583</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [subshrubs], perennial, (dying to near base [or not], new shoots forming dense rosettes often by anthesis), not viviparous, 0.5–2.5 dm, glabrous [pubescent].</text>
      <biological_entity id="o4651" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent-ascending [erect], branching [or not], herbaceous [± woody].</text>
      <biological_entity id="o4652" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent-ascending" value_original="decumbent-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, basal and cauline, alternate, ± alike, gradually smaller, sessile, not connate basally;</text>
      <biological_entity id="o4653" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s2" value="alike" value_original="alike" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s2" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear, subterete, 1–2.5 cm, fleshy, base spurred, margins entire;</text>
      <biological_entity id="o4654" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4655" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o4656" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins not conspicuous.</text>
      <biological_entity id="o4657" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal spikes or narrow thyrses.</text>
      <biological_entity id="o4658" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o4659" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o4660" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: flowers subsessile.</text>
      <biological_entity id="o4661" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o4662" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect, 5-merous;</text>
      <biological_entity id="o4663" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals distinct, all alike;</text>
      <biological_entity id="o4664" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s8" value="alike" value_original="alike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals spreading from middle [erect], connate basally, nearly distinct, rose;</text>
      <biological_entity id="o4665" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="from middle" constraintid="o4666" is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose" value_original="rose" />
      </biological_entity>
      <biological_entity id="o4666" name="middle" name_original="middle" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o4667" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o4669" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o4668" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o4669" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o4669" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectaries cuneate;</text>
      <biological_entity id="o4670" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 2 times as many as sepals;</text>
      <biological_entity id="o4671" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="sepal" constraintid="o4672" is_modifier="false" name="size_or_quantity" src="d0_s12" value="2 times as many as sepals" />
      </biological_entity>
      <biological_entity id="o4672" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments adnate to corolla base;</text>
      <biological_entity id="o4673" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="to corolla base" constraintid="o4674" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o4674" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pistils erect, nearly distinct;</text>
      <biological_entity id="o4675" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary base truncate;</text>
      <biological_entity constraint="ovary" id="o4676" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2+ times shorter than ovary.</text>
      <biological_entity id="o4677" name="style" name_original="styles" src="d0_s16" type="structure">
        <character constraint="ovary" constraintid="o4678" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2+ times shorter than ovary" />
      </biological_entity>
      <biological_entity id="o4678" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits erect.</text>
      <biological_entity id="o4679" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds obovoid, with papillose ribs.</text>
      <biological_entity id="o4681" name="rib" name_original="ribs" src="d0_s18" type="structure">
        <character is_modifier="true" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o4680" id="r391" name="with" negation="false" src="d0_s18" to="o4681" />
    </statement>
    <statement id="d0_s19">
      <text>x = 9–17, 20–22, 33+.</text>
      <biological_entity id="o4680" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o4682" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s19" to="17" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s19" to="22" />
        <character char_type="range_value" from="33" name="quantity" src="d0_s19" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., Mexico, Central America (Guatemala), South America (Peru).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Species ca. 25 (1 in the flora).</discussion>
  <discussion>The status of Villadia is debatable. The former sect. Altamiranoa (Rose) R. T. Clausen, which clearly intergrades with Mexican Sedum and differs chiefly in its connate petals, seems better placed in Sedum (C. H. Uhl and R. V. Moran 1999). Villadia proper is weakly distinguished from the huge and multifarious genus Sedum, now treated by H. ’t Hart (1995) as paraphyletic, but it seems a taxonomic convenience to remove such peripheral groups as this (Moran 1990). Uhl (1963, 1992) has found that Altamiranoa and Villadia belong to the same huge comparium as Echeveria, Graptopetalum, Lenophyllum, and others. Villadia is like Echeveria in having a remarkably long, descending series of evidently diploid chromosome numbers, namely 21, 20, and 17 to 9, as well as a few higher, polyploid numbers (Uhl and Moran). R. C. H. J. van Ham and ’t Hart (1998) found that Villadia is closely related to least advanced Eurasian Sedum and the Mexican genera Echeveria and Pachyphytum.</discussion>
  <discussion>Villadia misera (Lindley) R. T. Clausen is reported from Arizona under the synonym V. parviflora (Hemsley) Rose; R. T. Clausen (1940), without comment, cited a collection by W. B. Cannon and F. E. Lloyd (NY [also US]), supposedly from the San Francisco Mountains. There is no other record for Arizona, and the nearest undoubted locality is in Zacatecas, some 1500 km to the southeast. These are cultivated specimens, and the label locality evidently is wrong.</discussion>
  <references>
    <reference>Clausen, R. T. 1940. Studies in the Crassulaceae: Villadia, Altamiranoa, and Thompsonella. Bull. Torrey Bot. Club 67: 195–198.</reference>
    <reference>Uhl, C. H. and R. V. Moran. 1999. Chromosomes of Villadia and Altamiranoa (Crassulaceae). Amer. J. Bot. 86: 387–397.</reference>
  </references>
  
</bio:treatment>