<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rhododendron</taxon_name>
    <taxon_name authority="(Michaux) Shinners" date="1962" rank="species">periclymenoides</taxon_name>
    <place_of_publication>
      <publication_title>Castanea</publication_title>
      <place_in_publication>27: 95. 1962  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus rhododendron;species periclymenoides;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065651</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Azalea</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">periclymenoides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 151. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Azalea;species periclymenoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 3 (–5) m, usually not rhizomatous.</text>
      <biological_entity id="o24468" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: bark smooth to vertically furrowed, shredding;</text>
      <biological_entity id="o24469" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24470" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s1" to="vertically furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs scattered, multicellular eglandular-hairy (hairs unbranched) or such hairs ± absent, otherwise glabrous or sparsely unicellular-hairy.</text>
      <biological_entity id="o24471" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o24472" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o24473" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="more or less" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o24474" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole multicellular eglandular-hairy and unicellular-hairy;</text>
      <biological_entity id="o24475" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to obovate, 3–9 (–12.5) × 1.4–3.8 (–5) cm, thin, membranous, margins entire, plane, ciliate, eglandular-hairy, often strikingly so, apex acute to obtuse, often mucronate, abaxial surface often glabrous or, sometimes, sparsely unicellular-hairy, sometimes sparsely eglandular-hairy, adaxial surface sometimes scattered eglandular-hairy, otherwise glabrous or, sometimes, sparsely unicellular-hairy.</text>
      <biological_entity id="o24476" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s5" to="3.8" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24477" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o24478" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" modifier="often strikingly; strikingly" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24479" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s5" value="," value_original="," />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="unicellular-hairy" value_original="unicellular-hairy" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24480" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s5" value="," value_original="," />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bud-scales usually glabrous abaxially, margins unicellular-ciliate.</text>
      <biological_entity constraint="floral" id="o24481" name="bud-scale" name_original="bud-scales" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually; abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24482" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="unicellular-ciliate" value_original="unicellular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 6–15-flowered;</text>
      <biological_entity id="o24483" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="6-15-flowered" value_original="6-15-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts similar to bud-scales.</text>
      <biological_entity id="o24484" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o24485" name="bud-scale" name_original="bud-scales" src="d0_s8" type="structure" />
      <relation from="o24484" id="r2032" name="to" negation="false" src="d0_s8" to="o24485" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 4–19 mm, eglandular-hairy, otherwise glabrous or sparsely unicellular-hairy.</text>
      <biological_entity id="o24486" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers opening before or with leaves, erect to horizontal, slightly fragrant;</text>
      <biological_entity id="o24487" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24488" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="true" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character is_modifier="true" modifier="slightly" name="odor" src="d0_s10" value="fragrant" value_original="fragrant" />
      </biological_entity>
      <relation from="o24487" id="r2033" name="opening before or with" negation="false" src="d0_s10" to="o24488" />
    </statement>
    <statement id="d0_s11">
      <text>calyx lobes 0.5–2.5 (–4) mm, surfaces and margins scattered, eglandular and/or, rarely, stipitate-glandular-hairy, otherwise glabrous or moderately unicellular-hairy;</text>
      <biological_entity constraint="calyx" id="o24489" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24490" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s11" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o24491" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s11" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla deep pink to white with pink tube, without blotch on upper lobe, funnelform, 23–45 mm, scattered, multicellular eglandular-hairy and/or, less commonly, multicellular stipitate-glandular-hairy (hairs not forming distinct lines), otherwise sparsely to moderately unicellular-hairy on outer surface, petals connate, lobes 9–25 mm, tube usually gradually expanded into lobes, 12–27 mm (equaling or longer than lobes);</text>
      <biological_entity id="o24492" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="deep" value_original="deep" />
        <character char_type="range_value" constraint="with tube" constraintid="o24493" from="pink" name="coloration" src="d0_s12" to="white" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="45" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="less commonly; commonly" name="architecture" src="d0_s12" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular-hairy" value_original="stipitate-glandular-hairy" />
        <character constraint="on outer surface" constraintid="o24496" is_modifier="false" modifier="otherwise sparsely; sparsely to moderately" name="pubescence" src="d0_s12" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o24493" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o24494" name="blotch" name_original="blotch" src="d0_s12" type="structure" />
      <biological_entity constraint="upper" id="o24495" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o24496" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o24497" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o24498" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24499" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character constraint="into lobes" constraintid="o24500" is_modifier="false" modifier="usually gradually" name="size" src="d0_s12" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24500" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <relation from="o24492" id="r2034" name="without" negation="false" src="d0_s12" to="o24494" />
      <relation from="o24494" id="r2035" name="on" negation="false" src="d0_s12" to="o24495" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5, much exserted, ± unequal, 32–68 mm.</text>
      <biological_entity id="o24501" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="much" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="32" from_unit="mm" name="some_measurement" src="d0_s13" to="68" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules borne on erect pedicels, 10–30 × 3–6.5 mm, eglandular-hairy, otherwise glabrous or sparsely unicellular-hairy.</text>
      <biological_entity id="o24502" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="" src="d0_s14" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s14" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="unicellular-hairy" value_original="unicellular-hairy" />
      </biological_entity>
      <biological_entity id="o24503" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o24502" id="r2036" name="borne on" negation="false" src="d0_s14" to="o24503" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds without distinct tails;</text>
      <biological_entity id="o24504" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity id="o24505" name="tail" name_original="tails" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o24504" id="r2037" name="without" negation="false" src="d0_s15" to="o24505" />
    </statement>
    <statement id="d0_s16">
      <text>testa rather close, but ± loose.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 26.</text>
      <biological_entity id="o24506" name="testa" name_original="testa" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="rather" name="arrangement" src="d0_s16" value="close" value_original="close" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fragility" src="d0_s16" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24507" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed, dry to moist woods, often along streams, thickets, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist woods" modifier="mixed" />
        <character name="habitat" value="dry to moist woods" modifier="along" />
        <character name="habitat" value="streams" modifier="often along" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Del., Ga., Ky., Md., Mass., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Pinxterbloom azalea</other_name>
  <other_name type="common_name">pinxter-flower</other_name>
  <other_name type="common_name">election-pink</other_name>
  <discussion>Rhododendron periclymenoides is most similar to R. canescens, from which it can be distinguished by its less hairy leaves and bud scales, more gradually expanded corolla tube, and usually eglandular corolla indumentum; plants with stipitate-glandular hairs on the corolla occur sporadically throughout the range of this species (K. A. Kron 1993). Occasional hybridization with R. atlanticum, R. canescens, and R. prinophyllum probably occurs. The name R. nudiflorum Torrey has been incorrectly applied to this species; that name was superfluous when published and applies to R. luteum.</discussion>
  
</bio:treatment>