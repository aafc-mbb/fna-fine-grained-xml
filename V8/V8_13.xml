<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="treatment_page">15</other_info_on_meta>
    <other_info_on_meta type="illustration_page">16</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">aureum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 164. 1813  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species aureum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250063214</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–3 m.</text>
      <biological_entity id="o21003" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous or finely puberulent or villous, glabrescent;</text>
      <biological_entity id="o21004" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes absent;</text>
      <biological_entity id="o21005" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o21006" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o21005" id="r1732" name="at" negation="false" src="d0_s2" to="o21006" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o21007" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o21008" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o21007" id="r1733" name="on" negation="false" src="d0_s3" to="o21008" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole (0.4–) 1–3 (–4.8) cm, margins of young leaves often with slender extensions like multicelled hairs, surfaces finely pubescent, glabrescent;</text>
      <biological_entity id="o21009" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21010" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21011" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o21012" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <biological_entity constraint="slender" id="o21013" name="extension" name_original="extensions" src="d0_s4" type="structure" />
      <biological_entity id="o21014" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="multicelled" value_original="multicelled" />
      </biological_entity>
      <biological_entity id="o21015" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <relation from="o21011" id="r1734" name="part_of" negation="false" src="d0_s4" to="o21012" />
      <relation from="o21011" id="r1735" name="with" negation="false" src="d0_s4" to="o21013" />
      <relation from="o21013" id="r1736" name="like" negation="false" src="d0_s4" to="o21014" />
    </statement>
    <statement id="d0_s5">
      <text>blade broadly deltate-ovate to obovate, 3 (–5) -lobed, cleft less than to slightly more than 1/2 to midrib (sometimes proximal leaves again shallowly lobed), (1–) 1.6–3.6 (–5.7) cm, base broadly cuneate to somewhat cordate, surfaces sometimes with colorless or yellowish, nearly sessile glands, finely pubescent or glabrous, glabrescent, lobes oblong-rounded, margins entire or with 2–5 rounded teeth, apex acute to obtuse.</text>
      <biological_entity id="o21016" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21017" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly deltate-ovate" name="shape" src="d0_s5" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3(-5)-lobed" value_original="3(-5)-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="to midrib" constraintid="o21018" name="quantity" src="d0_s5" value="/2" value_original="/2" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="5.7" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="3.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21018" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o21019" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate to somewhat" value_original="cuneate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o21020" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o21021" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="colorless" value_original="colorless" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" modifier="nearly" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21022" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-rounded" value_original="oblong-rounded" />
      </biological_entity>
      <biological_entity id="o21023" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="with 2-5 rounded teeth" value_original="with 2-5 rounded teeth" />
      </biological_entity>
      <biological_entity id="o21024" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o21025" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <relation from="o21020" id="r1737" name="with" negation="false" src="d0_s5" to="o21021" />
      <relation from="o21023" id="r1738" name="with" negation="false" src="d0_s5" to="o21024" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ascending to reflexed, 5–18-flowered racemes, 3–7 cm, axis glabrous, finely pubescent, or densely villous, flowers evenly spaced.</text>
      <biological_entity id="o21026" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="reflexed" />
      </biological_entity>
      <biological_entity id="o21027" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="5-18-flowered" value_original="5-18-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21028" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o21029" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels jointed, 2–8 mm, glabrous or densely villous;</text>
      <biological_entity id="o21030" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts broadly deltate to obovate (similar to leaves), 4–9 mm, glabrous or densely villous.</text>
      <biological_entity id="o21031" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly deltate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium yellow to yellowish green, narrowly tubular, 6–20 mm, glabrous;</text>
      <biological_entity id="o21032" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21033" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="yellowish green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, spreading, usually not reflexed, golden yellow, oblongelliptic, 3–8 mm;</text>
      <biological_entity id="o21034" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21035" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually not" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals connivent, erect, yellow to orange or deep red, oblong-obovate, not conspicuously revolute or inrolled, 2–3 (–4) mm;</text>
      <biological_entity id="o21036" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21037" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange or deep red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not conspicuous;</text>
      <biological_entity id="o21038" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o21039" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens nearly as long as petals;</text>
      <biological_entity id="o21040" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21041" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o21042" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <relation from="o21041" id="r1739" name="as long as" negation="false" src="d0_s13" to="o21042" />
    </statement>
    <statement id="d0_s14">
      <text>filaments slightly expanded at base, 0.9–1.5 (–2.2) mm, glabrous;</text>
      <biological_entity id="o21043" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o21044" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="at base" constraintid="o21045" is_modifier="false" modifier="slightly" name="size" src="d0_s14" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21045" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers white, oblong, 1.1–2 mm, apex minutely apiculate;</text>
      <biological_entity id="o21046" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o21047" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21048" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary glabrous;</text>
      <biological_entity id="o21049" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o21050" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate almost to stigmas, (8.5–) 9.8–12.5 mm, glabrous.</text>
      <biological_entity id="o21051" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o21052" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="to stigmas" constraintid="o21053" is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s17" to="9.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9.8" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="12.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21053" name="stigma" name_original="stigmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, usually red, orange, brown, or black, rarely yellow, globose, 5.2–10 mm, glabrous.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 16.</text>
      <biological_entity id="o21054" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s18" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21055" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Ont., Que., Sask.; Ariz., Ark., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Md., Mich., Minn., Mo., Mont., N.Dak., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., S.Dak., Tenn., Tex., Utah, Vt., W.Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Golden currant</other_name>
  <other_name type="common_name">gadellier doré</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Ribes aureum was introduced into cultivation in Europe early in the nineteenth century (F. V. Coville 1903). It is a major host of pinyon blister rust in Arizona, Colorado, and Utah, and of pinyon leaf rust in New Mexico (E. P. Van Arsdel and B. W. Geils 2004). Ribes aureum is a variable complex and the varieties may seem to intergrade. In California, var. aureum occurs in sagebrush scrub or coniferous forests at higher elevations (800–2600 m) than var. gracillimum; the sepals of var. aureum are longer than those of var. gracillimum (5–8 mm versus 3–4 mm), and its hypanthium is noticeably shorter relative to the sepals. Leaves of var. aureum are more highly lobed and are sparsely glandular in the Pacific Northwest and less lobed and more densely glandular in the southwest (H. D. Hammond, pers. comm.). In most of its range, var. villosum is so conspicuously villous as to be unmistakable; in the west some plants with strikingly long hypanthia are scarcely villous.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Hypanthium lengths 1.5-2 times sepals.</description>
      <determination>4a Ribes aureum var. aureum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Hypanthium lengths 2-3 times sepals</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals yellow, turning deep red; hypanthia 6-12 mm.</description>
      <determination>4b Ribes aureum var. gracillimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals yellow, turning orange; hypanthia 9-20 mm.</description>
      <determination>4c Ribes aureum var. villosum</determination>
    </key_statement>
  </key>
</bio:treatment>