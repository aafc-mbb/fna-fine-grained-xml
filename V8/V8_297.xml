<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Bongard" date="1832" rank="species">mertensiana</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Acad. Imp. Sci. St.-Pétersbourg, Sér.</publication_title>
      <place_in_publication>6, Sci. Math. 2: 141. 1832  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species mertensiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092029</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mertensiana</taxon_name>
    <taxon_name authority="(Small) Engler &amp; Irmscher" date="unknown" rank="variety">eastwoodiae</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species mertensiana;variety eastwoodiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or in clumps, not stoloniferous, with caudex or short-rhizomatous.</text>
      <biological_entity id="o3613" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="in clumps" value_original="in clumps" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3614" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <biological_entity id="o3615" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <relation from="o3613" id="r300" name="in" negation="false" src="d0_s0" to="o3614" />
      <relation from="o3613" id="r301" name="with" negation="false" src="d0_s0" to="o3615" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, (basal persistent, cauline ± inconspicuous);</text>
      <biological_entity id="o3616" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole rounded, 2–20 mm;</text>
      <biological_entity id="o3617" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade round to reniform, irregularly shallowly lobed, 20–80 (–100) mm, thin, margins serrate, stipitate glandular-ciliate, apex obtuse to rounded, surfaces sparsely hairy.</text>
      <biological_entity id="o3618" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s3" to="reniform" />
        <character is_modifier="false" modifier="irregularly shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="80" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o3619" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
      <biological_entity id="o3620" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o3621" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 30+-flowered, open, much-branched thyrses, usually some or all flowers replaced by bulbils (sometimes bulbils absent), 15–40 cm, dark purple-tipped stipitate-glandular;</text>
      <biological_entity id="o3622" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="30+-flowered" value_original="30+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o3623" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="much-branched" value_original="much-branched" />
      </biological_entity>
      <biological_entity id="o3624" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s4" to="40" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark purple-tipped" value_original="dark purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o3625" name="bulbil" name_original="bulbils" src="d0_s4" type="structure" />
      <relation from="o3624" id="r302" modifier="usually" name="replaced by" negation="false" src="d0_s4" to="o3625" />
    </statement>
    <statement id="d0_s5">
      <text>bracts (± inconspicuous), petiolate or sessile.</text>
      <biological_entity id="o3626" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals reflexed (at least in fruit), ovate to elliptic, margins eciliate, surfaces sparsely stipitate-glandular or glabrous;</text>
      <biological_entity id="o3627" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3628" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="elliptic" />
      </biological_entity>
      <biological_entity id="o3629" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o3630" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, not spotted, narrowly ovate to elliptic, (3–) 4–6 mm, longer than sepals;</text>
      <biological_entity id="o3631" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3632" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="than sepals" constraintid="o3633" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3633" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments strongly club-shaped;</text>
      <biological_entity id="o3634" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3635" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s8" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary superior.</text>
      <biological_entity id="o3636" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 36, ca. 48, 50.</text>
      <biological_entity id="o3637" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3638" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
        <character name="quantity" src="d0_s10" value="50" value_original="50" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet stream banks, mossy cliffs and slopes, waterfall spray zones</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet stream" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="mossy cliffs" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="waterfall spray zones" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Merten’s or wood or woodland saxifrage</other_name>
  <discussion>Plants of Saxifraga mertensiana bear bulbils in the axils of basal leaves.</discussion>
  
</bio:treatment>