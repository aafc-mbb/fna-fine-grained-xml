<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="treatment_page">219</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="(Britton) A. Berger in H. G. A. Engler et al." date="1930" rank="species">laxum</taxon_name>
    <taxon_name authority="(Denton) H. Ohba" date="2007" rank="variety">flavidum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 889. 2007,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species laxum;variety flavidum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092143</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laxum</taxon_name>
    <taxon_name authority="Denton" date="unknown" rank="subspecies">flavidum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 233, fig. 1. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sedum;species laxum;subspecies flavidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades slightly glaucous, obovate, 15–25 × 4.5–10 mm, apex obtuse.</text>
      <biological_entity id="o15985" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s0" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s0" to="25" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15986" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering shoots 10–24 cm;</text>
      <biological_entity id="o15987" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="24" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaf-blades slightly glaucous, obovate, base often truncate, not clasping stem.</text>
      <biological_entity id="o15988" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o15989" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o15990" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="not" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 2.5–4 mm, apex obtuse;</text>
      <biological_entity id="o15991" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o15992" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15993" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals pale-yellow or white with pink midveins, 7.5–10 mm;</text>
      <biological_entity id="o15994" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o15995" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-yellow" value_original="pale-yellow" />
        <character constraint="with midveins" constraintid="o15996" is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15996" name="midvein" name_original="midveins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers yellow to ± redbrown.</text>
      <biological_entity id="o15997" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 60.</text>
      <biological_entity id="o15998" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s5" to="more or less redbrown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15999" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring-mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine or basalt outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine or basalt outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35c.</number>
  <discussion>Variety flavidum is restricted to the Trinity Mountains.</discussion>
  
</bio:treatment>