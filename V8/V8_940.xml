<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">kalmia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 391. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus kalmia;species angustifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065665</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaedaphne</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_hierarchy>genus Chamaedaphne;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs erect, 0.3–1.5 m.</text>
      <biological_entity id="o1542" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs terete, viscid, glabrous or puberulent.</text>
      <biological_entity id="o1543" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually in whorls of 3, rarely alternate or opposite;</text>
      <biological_entity id="o1544" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o1545" name="whorl" name_original="whorls" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o1544" id="r138" name="in" negation="false" src="d0_s2" to="o1545" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 6–16 mm, usually puberulent;</text>
      <biological_entity id="o1546" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong to elliptic-lanceolate, 1.5–8 × 0.5–2.5 cm, margins usually plane, apex obtuse to acute, usually apiculate, abaxial surface glabrous or puberulent, sometimes stipitate-glandular, adaxial lightly puberulent (hairs white, to 0.1 mm), sometimes glabrescent, midrib puberulent.</text>
      <biological_entity id="o1547" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1548" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o1549" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1550" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1551" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="lightly" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o1552" name="midrib" name_original="midrib" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary near distal end, corymbiform racemes, 4–12-flowered.</text>
      <biological_entity id="o1553" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="near distal end" constraintid="o1554" is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1554" name="end" name_original="end" src="d0_s5" type="structure" />
      <biological_entity id="o1555" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-12-flowered" value_original="4-12-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 5–20 mm.</text>
      <biological_entity id="o1556" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals usually green, sometimes reddish apically or throughout, ovate, 2–2.8 mm, apex usually acuminate, surfaces puberulent;</text>
      <biological_entity id="o1557" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1558" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes; apically" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1559" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o1560" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals connate nearly their entire lengths, usually reddish purple to pink, rarely white or bluish pink, usually deeper colored near anther pockets and with ring of red to purple spots just proximal to pockets, 7.5–9.5 × 6–13 mm, abaxial surface puberulent, adaxial glabrous, puberulent toward base;</text>
      <biological_entity id="o1561" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1562" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="lengths" src="d0_s8" value="entire" value_original="entire" />
        <character char_type="range_value" from="usually reddish purple" name="coloration" src="d0_s8" to="pink rarely white or bluish pink" />
        <character char_type="range_value" from="usually reddish purple" name="coloration" src="d0_s8" to="pink rarely white or bluish pink" />
        <character constraint="near anther pockets and with ring, surface, surface" constraintid="o1563, o1565" is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="deeper colored" value_original="deeper colored" />
        <character constraint="toward base" constraintid="o1569" is_modifier="false" name="pubescence" notes="" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1563" name="ring" name_original="ring" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red to purple spots" value_original="red to purple spots" />
        <character is_modifier="false" modifier="just" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="length" src="d0_s8" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1565" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red to purple spots" value_original="red to purple spots" />
        <character is_modifier="false" modifier="just" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="length" src="d0_s8" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1566" name="pocket" name_original="pockets" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o1567" name="ring" name_original="ring" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o1568" name="ring" name_original="ring" src="d0_s8" type="structure" />
      <biological_entity id="o1569" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o1563" id="r139" name="to" negation="false" src="d0_s8" to="o1566" />
      <relation from="o1565" id="r140" name="to" negation="false" src="d0_s8" to="o1566" />
    </statement>
    <statement id="d0_s9">
      <text>filaments 2.5–3.5 mm;</text>
      <biological_entity id="o1570" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1571" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 3.5–4.5 mm.</text>
      <biological_entity id="o1572" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1573" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 5-locular, 2–3.5 × 3–5 mm, puberulent, stipitate-glandular.</text>
      <biological_entity id="o1574" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="5-locular" value_original="5-locular" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds winged, obovoid, 0.6–1 mm. 2n = 24.</text>
      <biological_entity id="o1575" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1576" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que.; Conn., Del., Ga., Maine, Mass., Md., Mich., N.C., N.H., N.J., N.Y., Pa., R.I., S.C., Tenn., Va., Vt., W.Va.; introduced in n Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="in n Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Sheep laurel</other_name>
  <other_name type="common_name">lambkill</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The two varieties of Kalmia angustifolia are largely separate geographically and show different flavonoid profiles (S. Liu 1993). Southeastern Virginia has long been known as a region of overlap of these two entities; field and herbarium work (B. A. Sorrie and A. S. Weakley, unpubl.) has demonstrated that the two taxa retain their distinctiveness and that hybrids are rare. Sorrie and Weakley concluded that the two should be treated as separate species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces densely stipitate-glandular; leaf blade surfaces glabrous or scattered, stipitate-glandular trichomes.</description>
      <determination>2a Kalmia angustifolia var. angustifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces usually not stipitate-glandular; leaf blade surfaces densely puberulent abaxially, usually not stipitate-glandular.</description>
      <determination>2b Kalmia angustifolia var. carolina</determination>
    </key_statement>
  </key>
</bio:treatment>