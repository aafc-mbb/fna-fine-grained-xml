<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wayne J. Elisens</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">sapotaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MIMUSOPS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 349. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 165. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sapotaceae;genus MIMUSOPS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek mimo, ape, and ops, face, alluding to appearance of flower</other_info_on_name>
    <other_info_on_name type="fna_id">120757</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [shrubs].</text>
      <biological_entity id="o4003" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems not armed, glabrate or hairy.</text>
      <biological_entity id="o4004" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, alternate;</text>
      <biological_entity id="o4005" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules caducous;</text>
      <biological_entity id="o4006" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o4007" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade: base rounded, cuneate, or attenuate, apex rounded, retuse, acute, or acuminate to cuspidate, surfaces glabrous or glabrate [hairy] abaxially, usually glabrous adaxially.</text>
      <biological_entity id="o4008" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o4009" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o4010" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="retuse" value_original="retuse" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="cuspidate" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="cuspidate" />
      </biological_entity>
      <biological_entity id="o4011" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="usually; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences fascicles or solitary flowers.</text>
      <biological_entity id="o4012" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o4013" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 8 in 2 whorls of 4, outer valvate, larger, inner imbricate, [glabrate] densely hairy abaxially;</text>
      <biological_entity id="o4014" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4015" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in whorls" constraintid="o4016" name="quantity" src="d0_s7" value="8" value_original="8" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o4016" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s7" value="valvate" value_original="valvate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4017" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4018" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o4016" id="r332" name="part_of" negation="false" src="d0_s7" to="o4017" />
    </statement>
    <statement id="d0_s8">
      <text>petals (7–) 8, white to yellow or pink, glabrous or hairy, lobes longer than corolla-tube, each divided into 1 median and 2 lateral segments, median segment equaling or longer than laterals;</text>
      <biological_entity id="o4019" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4020" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="yellow or pink" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4021" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character constraint="than corolla-tube" constraintid="o4022" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
        <character constraint="into median" constraintid="o4023" is_modifier="false" name="shape" src="d0_s8" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o4022" name="corolla-tube" name_original="corolla-tube" src="d0_s8" type="structure" />
      <biological_entity id="o4023" name="median" name_original="median" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4024" name="segment" name_original="segments" src="d0_s8" type="structure" />
      <biological_entity constraint="median" id="o4025" name="segment" name_original="segment" src="d0_s8" type="structure">
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than laterals" constraintid="o4026" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4026" name="lateral" name_original="laterals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens (7–) 8, distinct distal to corolla-tube;</text>
      <biological_entity id="o4027" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4028" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s9" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="8" value_original="8" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character constraint="to corolla-tube" constraintid="o4029" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o4029" name="corolla-tube" name_original="corolla-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminodes (7–) 8, alternating with stamens, inflexed, petaloid, lanceolate, hairy;</text>
      <biological_entity id="o4030" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4031" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s10" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character constraint="with stamens" constraintid="o4032" is_modifier="false" name="arrangement" src="d0_s10" value="alternating" value_original="alternating" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="inflexed" value_original="inflexed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4032" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistil (7–) 8-carpellate;</text>
      <biological_entity id="o4033" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4034" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="(7-)8-carpellate" value_original="(7-)8-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary (7–) 8-locular, hairy;</text>
      <biological_entity id="o4035" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4036" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="(7-)8-locular" value_original="(7-)8-locular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>placentation basal or basiventral.</text>
      <biological_entity id="o4037" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s13" value="basal" value_original="basal" />
        <character name="placentation" src="d0_s13" value="basiventral" value_original="basiventral" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries yellow to orange, subglobose, [glabrous] glabrate.</text>
      <biological_entity id="o4038" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="orange" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1 [–6], [buff] brown to black, laterally compressed;</text>
      <biological_entity id="o4039" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="6" />
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s15" to="black" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hilum circular [elliptic];</text>
      <biological_entity id="o4040" name="hilum" name_original="hilum" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="circular" value_original="circular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>embryo vertical;</text>
      <biological_entity id="o4041" name="embryo" name_original="embryo" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="vertical" value_original="vertical" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm present.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 12.</text>
      <biological_entity id="o4042" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o4043" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Asia, Africa, Indian Ocean Islands, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species ca. 30 (1 in the flora).</discussion>
  <discussion>Mimusops is not native in the New World. Three species are cultivated commonly in the Neotropics: M. balata C. F. Gaertner, M. coriacea (A. de Candolle) Miquel and M. elengi. Mimusops elengi is naturalized and described here, whereas M. balata and M. coriacea apparently are becoming naturalized in southern Florida. American and West Indian species formerly placed in Mimusops are now included in Manilkara (T. D. Pennington 1990, 1991). Mimusops is characterized by elliptic or circular seed scars and eight-merous flowers.</discussion>
  
</bio:treatment>