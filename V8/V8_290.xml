<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="species">hyperborea</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>16. 1823  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species hyperborea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092022</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Sternberg" date="unknown" rank="species">flexuosa</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species flexuosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="(Sternberg) Engler &amp; Irmscher" date="unknown" rank="variety">flexuosa</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species rivularis;variety flexuosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="(R. Brown) Dorn" date="unknown" rank="subspecies">hyperborea</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species rivularis;subspecies hyperborea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="(R. Brown) Hooker" date="unknown" rank="variety">hyperborea</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species rivularis;variety hyperborea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="Lange" date="unknown" rank="variety">purpurascens</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species rivularis;variety purpurascens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or in compact, or sometimes loose, tufts, (usually wholly purple, sometimes green), not stoloniferous, not rhizomatous, with caudex.</text>
      <biological_entity id="o8118" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="in compact" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="in compact" value_original="in compact" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8119" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity id="o8120" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <relation from="o8119" id="r654" name="with" negation="false" src="d0_s0" to="o8120" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, (cauline 1–3, dissimilar from basal, reduced);</text>
      <biological_entity id="o8121" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole ± flattened, (2–) 5–35 mm;</text>
      <biological_entity id="o8122" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade reniform to orbiculate, (2–) 3–5 (–7) -lobed (lobes rounded), (2–) 3–6 (–10) mm, thin or slightly fleshy, margins entire, eciliate or sparsely stipitate-glandular, (inconspicuous nonsecreting hydathodes sometimes present), apex acute, surfaces glabrous or sparsely hairy.</text>
      <biological_entity id="o8123" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s3" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="(2-)3-5(-7)-lobed" value_original="(2-)3-5(-7)-lobed" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o8124" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o8125" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8126" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–5-flowered cymes, sometimes solitary flowers, 1–5 cm, tangled, purple-tipped stipitate-glandular;</text>
      <biological_entity id="o8127" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o8128" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
      <biological_entity id="o8129" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="tangled" value_original="tangled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts petiolate.</text>
      <biological_entity id="o8130" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: (hypanthium U-shaped in longisection, sparsely to densely long-stipitate-glandular);</text>
      <biological_entity id="o8131" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, (sometimes purple), ovate to lance-oblong, (1.5–2.1 mm wide), margins eciliate, surfaces ± stipitate-glandular;</text>
      <biological_entity id="o8132" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o8133" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lance-oblong" />
      </biological_entity>
      <biological_entity id="o8134" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o8135" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals purple or white, often midvein ± purplish, faded when dried, not spotted, oblong, (1.5–) 2–3.4 (–5) mm, equaling or to 1.5 times sepals;</text>
      <biological_entity id="o8136" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8137" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8138" name="midvein" name_original="midvein" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="when dried; not" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1/2 inferior.</text>
      <biological_entity id="o8139" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 26.</text>
      <biological_entity id="o8140" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s9" value="inferior" value_original="inferior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8141" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet tundra, open gravel and silt, stream and lake margins, snow beds, shady ravines and cliffs, seepage under rocks, silty and gravelly seashores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet tundra" />
        <character name="habitat" value="open gravel" />
        <character name="habitat" value="silt" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="snow beds" />
        <character name="habitat" value="shady ravines" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocks" modifier="seepage under" />
        <character name="habitat" value="silty" />
        <character name="habitat" value="gravelly seashores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3000+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mont., Oreg., Wash., Wyo.; n Asia; Atlantic Islands (Spitsbergen).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Spitsbergen)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Pygmy saxifrage</other_name>
  <other_name type="common_name">saxifrage hyperboréale</other_name>
  <discussion>Reports of Saxifraga hyperborea from Mount Washington, New Hampshire (e.g., Á. Löve and D. Löve 1964) require confirmation; all specimens examined from this location appear to be S. rivularis. C. L. Hitchcock (1961) treated all western material as S. debilis, including that of the Pacific Northwest that is included here. For Colorado, W. A. Weber (1990) appears to have applied the name S. rivularis to what we call S. hyperborea, and S. hyperborea subsp. debilis to what we call S. debilis. P. K. Holmgren and N. H. Holmgren (1997) included under their broad concept of S. rivularis both S. hyperborea and S. debilis, noting that the plants had gone usually under the latter name. Both species are present in the Rockies and the Intermountain Region.</discussion>
  <discussion>In northeastern North America, Saxifraga hyperborea and S. rivularis are often sympatric, although the latter is absent from the extreme north. The major distinguishing feature between diploid S. hyperborea and tetraploid S. rivularis is the presence of rhizomes in the former and their absence in the latter (see key), a character that is sometimes difficult to interpret on herbarium sheets. Plants of S. hyperborea are usually smaller (2–5 cm) and reddish (sometimes green), with usually three- to five-lobed leaves and petals about one and one-half times longer than sepals, while plants of S. rivularis are taller (5–10 cm) and usually green (although sometimes reddish, particularly in the western var. arctolitoralis), with usually five-lobed leaves and petals two to three times longer than sepals.</discussion>
  
</bio:treatment>