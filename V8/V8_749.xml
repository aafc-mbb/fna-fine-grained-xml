<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary D. Wallace</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">396</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Monotropoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="genus">PLEURICOSPORA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 369. 1868  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily monotropoideae;genus pleuricospora;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pleurikos, of the side, and spora, sown seed, alluding to parietal placentation</other_info_on_name>
    <other_info_on_name type="fna_id">125986</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, achlorophyllous, heterotrophic.</text>
      <biological_entity id="o28088" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="achlorophyllous" value_original="achlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="heterotrophic" value_original="heterotrophic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems absent.</text>
      <biological_entity id="o28089" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves absent.</text>
      <biological_entity id="o28090" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes, erect at emergence from soil, axis fleshy but not fibrous, persistent after seed dispersal, cream to yellowish, 1.5–3 cm diam. proximal to proximalmost flower.</text>
      <biological_entity constraint="inflorescences" id="o28091" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character constraint="at emergence" constraintid="o28092" is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o28092" name="emergence" name_original="emergence" src="d0_s3" type="structure" />
      <biological_entity id="o28093" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o28094" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character constraint="after seed" constraintid="o28095" is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="cream" name="coloration" notes="" src="d0_s3" to="yellowish" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28095" name="seed" name_original="seed" src="d0_s3" type="structure" />
      <biological_entity constraint="diam" id="o28096" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o28097" name="flower" name_original="flower" src="d0_s3" type="structure" />
      <relation from="o28092" id="r2359" name="from" negation="false" src="d0_s3" to="o28093" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels erect, somewhat elongate in fruit;</text>
      <biological_entity id="o28098" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o28099" is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o28099" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracteoles absent.</text>
      <biological_entity id="o28100" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers radially symmetric, erect;</text>
      <biological_entity id="o28101" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 4, distinct, lanceolate to ovate;</text>
      <biological_entity id="o28102" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 4 (–5), distinct, cream, without basal tubercles, (surfaces glabrous), corolla tubular-cylindric;</text>
      <biological_entity id="o28103" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28104" name="tubercle" name_original="tubercles" src="d0_s8" type="structure" />
      <biological_entity id="o28105" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular-cylindric" value_original="tubular-cylindric" />
      </biological_entity>
      <relation from="o28103" id="r2360" name="without" negation="false" src="d0_s8" to="o28104" />
    </statement>
    <statement id="d0_s9">
      <text>intrastaminal nectary disc present;</text>
      <biological_entity constraint="nectary" id="o28106" name="disc" name_original="disc" src="d0_s9" type="structure" constraint_original="intrastaminal nectary">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 8, included;</text>
      <biological_entity id="o28107" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments ± uniformly slender, glabrous;</text>
      <biological_entity id="o28108" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less uniformly" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers elongate, without awns, without tubules, dehiscent by 2 longitudinal slits;</text>
      <biological_entity id="o28109" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character constraint="by slits" constraintid="o28112" is_modifier="false" name="dehiscence" notes="" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o28110" name="awn" name_original="awns" src="d0_s12" type="structure" />
      <biological_entity id="o28111" name="tubule" name_original="tubules" src="d0_s12" type="structure" />
      <biological_entity id="o28112" name="slit" name_original="slits" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o28109" id="r2361" name="without" negation="false" src="d0_s12" to="o28110" />
      <relation from="o28109" id="r2362" name="without" negation="false" src="d0_s12" to="o28111" />
    </statement>
    <statement id="d0_s13">
      <text>pistil 4 (–6) -carpellate;</text>
      <biological_entity id="o28113" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="4(-6)-carpellate" value_original="4(-6)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 1-locular;</text>
    </statement>
    <statement id="d0_s15">
      <text>placentation intruded-parietal;</text>
      <biological_entity id="o28114" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="intruded-parietal" value_original="intruded-parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style straight, slender;</text>
      <biological_entity id="o28115" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma crownlike, (2 mm diam.), without subtending ring of hairs.</text>
      <biological_entity id="o28116" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="crownlike" value_original="crownlike" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o28117" name="ring" name_original="ring" src="d0_s17" type="structure" />
      <biological_entity id="o28118" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <relation from="o28116" id="r2363" name="without" negation="false" src="d0_s17" to="o28117" />
      <relation from="o28117" id="r2364" name="part_of" negation="false" src="d0_s17" to="o28118" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits baccate, erect, fleshy, indehiscent.</text>
      <biological_entity id="o28119" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="baccate" value_original="baccate" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" name="texture" src="d0_s18" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 25–100, ovoid, not winged.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 13.</text>
      <biological_entity id="o28120" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s19" to="100" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o28121" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Species 1: w North America.</discussion>
  <discussion>Species 1</discussion>
  <references>
    <reference>Copeland, H. F. 1937. The reproductive structures of Pleuricospora. Madroño 4: 1–16.</reference>
  </references>
  
</bio:treatment>