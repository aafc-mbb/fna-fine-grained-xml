<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
    <other_info_on_meta type="illustration_page">137</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Willdenow in C. M. von Sternberg" date="1810" rank="species">flagellaris</taxon_name>
    <taxon_name authority="(Trautvetter) A. E. Porsild" date="1954" rank="subspecies">platysepala</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>51: 295. 1954,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species flagellaris;subspecies platysepala</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250092010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="(Trautvetter) Tolmatchev" date="unknown" rank="species">flagellaris</taxon_name>
    <taxon_name authority="Trautvetter" date="unknown" rank="variety">platysepala</taxon_name>
    <place_of_publication>
      <publication_title>in A. T. von Middendorff, Reise Siber.</publication_title>
      <place_in_publication>1(2): 42. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species flagellaris;variety platysepala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">platysepala</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species platysepala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences 1–4 (–6) cm;</text>
      <biological_entity id="o14691" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bract blade longer than internode.</text>
      <biological_entity constraint="bract" id="o14692" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character constraint="than internode" constraintid="o14693" is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14693" name="internode" name_original="internode" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers: hypanthia black-tipped stipitate-glandular;</text>
      <biological_entity id="o14694" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o14695" name="hypanthium" name_original="hypanthia" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="black-tipped" value_original="black-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals reddish purple, ovate, black-tipped stipitate-glandular;</text>
      <biological_entity id="o14696" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o14697" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="black-tipped" value_original="black-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals broadly obovate, length 2+ times sepals;</text>
      <biological_entity id="o14698" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o14699" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character constraint="sepal" constraintid="o14700" is_modifier="false" name="length" src="d0_s4" value="2+ times sepals" value_original="2+ times sepals" />
      </biological_entity>
      <biological_entity id="o14700" name="sepal" name_original="sepals" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ovary ca. 1/2 inferior.</text>
      <biological_entity id="o14701" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 32.</text>
      <biological_entity id="o14702" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s5" value="inferior" value_original="inferior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14703" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly arctic tundra, rocky mountain slopes, damp, open ground, often coastal</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly arctic tundra" />
        <character name="habitat" value="rocky mountain slopes" />
        <character name="habitat" value="open ground" modifier="damp" />
        <character name="habitat" value="damp" modifier="often" />
        <character name="habitat" value="coastal" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut; Alaska; Atlantic Islands (Svalbard).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Svalbard)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6c.</number>
  
</bio:treatment>