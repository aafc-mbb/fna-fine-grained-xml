<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">122</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="genus">sullivantia</taxon_name>
    <taxon_name authority="(J. M. Coulter &amp; Fisher) J. M. Coulter" date="1892" rank="species">hapemanii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>17: 421. 1892 (as hapemani),</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus sullivantia;species hapemanii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065993</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="J. M. Coulter &amp; Fisher" date="unknown" rank="species">hapemanii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>17: 348. 1892 (as hapemani)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Heuchera;species hapemanii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sullivantia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">oregana</taxon_name>
    <taxon_name authority="(J. M. Coulter &amp; Fisher) Rosendahl" date="unknown" rank="variety">hapemanii</taxon_name>
    <taxon_hierarchy>genus Sullivantia;species oregana;variety hapemanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not stoloniferous.</text>
      <biological_entity id="o20357" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems erect, 5–60 cm.</text>
      <biological_entity id="o20358" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade 1–11 cm wide, 1–2 times denticulate to subcrenate, 5–13-lobed for 1/4–1/2 length, lobes cuneate to subovate, 1 or 2 times denticulate to subcrenulate, margins entire or erose with long bristles to rarely sublaciniate.</text>
      <biological_entity id="o20359" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20360" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="11" to_unit="cm" />
        <character is_modifier="false" name="length" src="d0_s2" value="1-2 times denticulate to subcrenate 5-13-lobed for 1/4-1/2 length" />
        <character is_modifier="false" name="length" src="d0_s2" value="1-2 times denticulate to subcrenate 5-13-lobed for 1/4-1/2 length" />
      </biological_entity>
      <biological_entity id="o20361" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="subovate" />
        <character name="quantity" src="d0_s2" unit="or 2times" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subcrenulate" value_original="subcrenulate" />
      </biological_entity>
      <biological_entity id="o20362" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="with bristles" constraintid="o20363" is_modifier="false" name="architecture" src="d0_s2" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o20363" name="bristle" name_original="bristles" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s2" value="sublaciniate" value_original="sublaciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences erect, primary and higher-order branches perpendicular to central axis.</text>
      <biological_entity id="o20364" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="primary" value_original="primary" />
      </biological_entity>
      <biological_entity id="o20365" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity constraint="central" id="o20366" name="axis" name_original="axis" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium narrowly turbinate-campanulate, portion distal to adnation to ovary marked by conspicuous flaring, 1.8–3.3 × 1.2–2.5 mm, stipitate-glandular proximally;</text>
      <biological_entity id="o20367" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o20368" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="turbinate-campanulate" value_original="turbinate-campanulate" />
      </biological_entity>
      <biological_entity id="o20369" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character constraint="to adnation" constraintid="o20370" is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" notes="" src="d0_s4" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" notes="" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20370" name="adnation" name_original="adnation" src="d0_s4" type="structure" />
      <biological_entity id="o20371" name="ovary" name_original="ovary" src="d0_s4" type="structure" />
      <relation from="o20370" id="r1691" name="to" negation="false" src="d0_s4" to="o20371" />
    </statement>
    <statement id="d0_s5">
      <text>sepals triangular to triangular-ovate, 0.6–1.7 × 0.6–1.2 mm, not as broad as long at apex, apex acute;</text>
      <biological_entity id="o20372" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o20373" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="triangular-ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s5" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="1.2" to_unit="mm" />
        <character constraint="at apex" constraintid="o20374" is_modifier="false" modifier="not" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o20374" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o20375" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals unlobed, 2.5–3.1 mm, ovate and abruptly clawed to ovatelanceolate and gradually tapering to claw, 1–1.8 mm wide;</text>
      <biological_entity id="o20376" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20377" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="abruptly clawed" name="shape" src="d0_s6" to="ovatelanceolate" />
        <character constraint="to claw" constraintid="o20378" is_modifier="false" modifier="gradually" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s6" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20378" name="claw" name_original="claw" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ovary 4/5+ inferior.</text>
      <biological_entity id="o20379" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20380" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character char_type="range_value" from="4/5" name="quantity" src="d0_s7" upper_restricted="false" />
        <character is_modifier="false" name="position" src="d0_s7" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules cylindric to cylindric-ovate, 3.2–8 × 1.5–4 mm, beaks ± spreading.</text>
      <biological_entity id="o20381" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="cylindric-ovate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20382" name="beak" name_original="beaks" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds dark redbrown, 0.9–1.7 × 0.3–0.6 mm, main body (excluding winged margins) 0.7–1 mm. 2n = 14.</text>
      <biological_entity id="o20383" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark redbrown" value_original="dark redbrown" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="main" id="o20384" name="body" name_original="body" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20385" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries ± as long as wide; capsules to 2.5 times longer than wide (usually ca. 2 times longer than wide); Idaho, Montana, Wyoming.</description>
      <determination>3a Sullivantia hapemanii var. hapemanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries ca. 2 times longer than wide; capsules 2.5+ times longer than wide; Colorado.</description>
      <determination>3b Sullivantia hapemanii var. purpusii</determination>
    </key_statement>
  </key>
</bio:treatment>