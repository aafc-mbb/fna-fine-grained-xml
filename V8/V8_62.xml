<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="treatment_page">34</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Eastwood" date="1902" rank="species">sericeum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>3, 2: 246, plate 24, fig. 9a–f. 1902  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species sericeum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065848</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(Eastwood) Coville &amp; Britton" date="unknown" rank="species">sericea</taxon_name>
    <taxon_hierarchy>genus Grossularia;species sericea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2 m.</text>
      <biological_entity id="o9025" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, densely hairy, bristly, bristles often gland-tipped;</text>
      <biological_entity id="o9026" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o9027" name="bristle" name_original="bristles" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 3, 6–12 mm;</text>
      <biological_entity id="o9028" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9029" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o9028" id="r730" name="at" negation="false" src="d0_s2" to="o9029" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes dense, often gland-tipped.</text>
      <biological_entity id="o9030" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" notes="" src="d0_s3" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o9031" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o9030" id="r731" name="on" negation="false" src="d0_s3" to="o9031" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 0.5–1.5 cm, pubescent, stipitate-glandular;</text>
      <biological_entity id="o9032" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9033" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade round-ovate, 3–5-lobed, cleft nearly 1/3 to midrib, 1–3.5 cm, base shallowly cordate, surfaces villous, stipitate-glandular, lobes cuneate-rounded, margins with 2–5 rounded teeth, apex rounded.</text>
      <biological_entity id="o9034" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9035" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="round-ovate" value_original="round-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character constraint="to midrib" constraintid="o9036" name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9036" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o9037" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o9038" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9039" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate-rounded" value_original="cuneate-rounded" />
      </biological_entity>
      <biological_entity id="o9040" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o9041" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9042" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o9040" id="r732" name="with" negation="false" src="d0_s5" to="o9041" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, solitary flowers or 2–3-flowered racemes, 3.5–4.5 cm, axis stipitate-glandular and pilose.</text>
      <biological_entity id="o9043" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o9044" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9045" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3-flowered" value_original="2-3-flowered" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9046" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 5–10 mm, pilose, stipitate-glandular;</text>
      <biological_entity id="o9047" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate, 1–2 mm, pilose, stipitate-glandular.</text>
      <biological_entity id="o9048" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium green, conic to campanulate, 3–4 mm, sericeous;</text>
      <biological_entity id="o9049" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9050" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, strongly reflexed, greenish red, long-triangular, 6–8 mm;</text>
      <biological_entity id="o9051" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9052" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish red" value_original="greenish red" />
        <character is_modifier="false" name="shape" src="d0_s10" value="long-triangular" value_original="long-triangular" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals connivent, erect, white, oblong-deltate-spatulate, strongly concave abaxially, 4–6 mm;</text>
      <biological_entity id="o9053" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9054" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-deltate-spatulate" value_original="oblong-deltate-spatulate" />
        <character is_modifier="false" modifier="strongly; abaxially" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o9055" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o9056" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2 times as long as petals;</text>
      <biological_entity id="o9057" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9058" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="petal" constraintid="o9059" is_modifier="false" name="length" src="d0_s13" value="2 times as long as petals" />
      </biological_entity>
      <biological_entity id="o9059" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 1–1.3 mm, glabrous;</text>
      <biological_entity id="o9060" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o9061" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers cream, oblong, 1.5–1.8 mm, apex blunt or rounded;</text>
      <biological_entity id="o9062" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o9063" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" value_original="cream" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9064" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary pubescent, copiously glandular-bristly;</text>
      <biological_entity id="o9065" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o9066" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="copiously" name="pubescence" src="d0_s16" value="glandular-bristly" value_original="glandular-bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate 3/4 their lengths, 15 mm, glabrous.</text>
      <biological_entity id="o9067" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o9068" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s17" value="3/4" value_original="3/4" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="15" value_original="15" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries palatability not known, purple, globose, 15–25 mm, glandular-bristly.</text>
      <biological_entity id="o9069" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="coloration_or_density" src="d0_s18" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s18" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glandular-bristly" value_original="glandular-bristly" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <other_name type="common_name">Santa Lucia gooseberry</other_name>
  <discussion>Ribes sericeum is known primarily from the Santa Lucia Mountains; a disjunct population occurs north of Figueroa Mountain in Santa Barbara County.</discussion>
  
</bio:treatment>