<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Elizabeth Fortson Wells,Patrick E. Elvander†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1879" rank="genus">SUKSDORFIA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>15: 41. 1879, name conserved  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus SUKSDORFIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Wilhelm Nikolaus Suksdorf, 1850–1932, German botanist and collector in the Pacific Northwest</other_info_on_name>
    <other_info_on_name type="fna_id">131895</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, not rhizomatous, not stoloniferous;</text>
      <biological_entity id="o15359" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base of slender caudex bearing bulbils.</text>
      <biological_entity id="o15360" name="base" name_original="base" src="d0_s1" type="structure" constraint="caudex" constraint_original="caudex; caudex" />
      <biological_entity constraint="slender" id="o15361" name="caudex" name_original="caudex" src="d0_s1" type="structure" />
      <biological_entity id="o15362" name="bulbil" name_original="bulbils" src="d0_s1" type="structure" />
      <relation from="o15360" id="r1307" name="part_of" negation="false" src="d0_s1" to="o15361" />
      <relation from="o15360" id="r1308" name="bearing" negation="false" src="d0_s1" to="o15362" />
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems erect, leafy, 8–40 cm, stipitate-glandular.</text>
      <biological_entity id="o15363" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in basal rosette and cauline;</text>
      <biological_entity id="o15364" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15365" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o15364" id="r1309" name="in" negation="false" src="d0_s3" to="o15365" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves reduced distally;</text>
      <biological_entity constraint="cauline" id="o15366" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules of basal leaves inconspicuous, stipules of cauline leaves conspicuous;</text>
      <biological_entity id="o15367" name="stipule" name_original="stipules" src="d0_s5" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15368" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15369" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o15370" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o15367" id="r1310" name="part_of" negation="false" src="d0_s5" to="o15368" />
      <relation from="o15369" id="r1311" name="part_of" negation="false" src="d0_s5" to="o15370" />
    </statement>
    <statement id="d0_s6">
      <text>petiole sparsely to moderately stipitate-glandular;</text>
      <biological_entity id="o15371" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade reniform to orbiculate, usually 3–9-lobed, base cordate to truncate, ultimate margins irregularly crenate, ciliate, apex obtuse to rounded, surfaces glabrous or sparsely stipitate-glandular;</text>
      <biological_entity id="o15372" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s7" to="orbiculate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="3-9-lobed" value_original="3-9-lobed" />
      </biological_entity>
      <biological_entity id="o15373" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s7" to="truncate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15374" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15375" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation palmate.</text>
      <biological_entity id="o15376" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences flat-topped panicles of simple or compound cymes, terminal from terminal bud in rosette, 2–35-flowered, bracteate or ebracteate.</text>
      <biological_entity id="o15377" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o15378" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="flat-topped" value_original="flat-topped" />
        <character constraint="from terminal bud" constraintid="o15380" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="2-35-flowered" value_original="2-35-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o15379" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o15380" name="bud" name_original="bud" src="d0_s9" type="structure" />
      <biological_entity id="o15381" name="rosette" name_original="rosette" src="d0_s9" type="structure" />
      <relation from="o15378" id="r1312" name="part_of" negation="false" src="d0_s9" to="o15379" />
      <relation from="o15380" id="r1313" name="in" negation="false" src="d0_s9" to="o15381" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: hypanthium adnate to ovary in proximal 7/8, free from ovary 0.3–1 mm, greenish (with purple band distally in S. ranunculifolia);</text>
      <biological_entity id="o15382" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15383" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character constraint="to ovary" constraintid="o15384" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
        <character constraint="from ovary" constraintid="o15386" is_modifier="false" name="fusion" notes="" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o15384" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o15385" name="7/8" name_original="7/8" src="d0_s10" type="structure" />
      <biological_entity id="o15386" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o15384" id="r1314" name="in" negation="false" src="d0_s10" to="o15385" />
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, greenish (with purple margins in S. violacea);</text>
      <biological_entity id="o15387" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15388" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, white or pink, purple, or violet;</text>
      <biological_entity id="o15389" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15390" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="violet" value_original="violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary tissue partially covering ovary (in S. ranunculifolia) or absent (in S. violacea);</text>
      <biological_entity id="o15391" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectary" id="o15392" name="tissue" name_original="tissue" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15393" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
      <relation from="o15392" id="r1315" modifier="partially" name="covering" negation="false" src="d0_s13" to="o15393" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 5;</text>
      <biological_entity id="o15394" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15395" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments filiform;</text>
      <biological_entity id="o15396" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o15397" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary 1/2–3/4 inferior, 2-locular, carpels connate proximally;</text>
      <biological_entity id="o15398" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o15399" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s16" to="3/4" />
        <character is_modifier="false" name="position" src="d0_s16" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="2-locular" value_original="2-locular" />
      </biological_entity>
      <biological_entity id="o15400" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>placentation axile;</text>
      <biological_entity id="o15401" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s17" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles absent or 2;</text>
      <biological_entity id="o15402" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o15403" name="style" name_original="styles" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 2.</text>
      <biological_entity id="o15404" name="flower" name_original="flowers" src="d0_s19" type="structure" />
      <biological_entity id="o15405" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Capsules 2 (–3) -beaked.</text>
      <biological_entity id="o15406" name="capsule" name_original="capsules" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="2(-3)-beaked" value_original="2(-3)-beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds (50–75), dark-brown, ellipsoid and prismatic (or angular), warty.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 7.</text>
      <biological_entity id="o15407" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s21" to="75" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s21" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s21" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity constraint="x" id="o15408" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, South America (Argentina, Bolivia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Hemieva Rafinesque, name rejected</discussion>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Suksdorfia has been split into three monospecific genera by different authors. The work of R. J. Gornall and B. A. Bohm (1980, 1984, 1985) emphasized the similarities of the species in supporting a single genus concept. More recent, molecular data (D. E. Soltis et al. 1993; L. A. Johnson and Soltis 1994) suggest that S. violacea is more closely related to species of Bolandra, that S. ranunculifolia is more closely related to species of Boykinia, and that the two North American species indeed should be placed into monospecific genera. The South American species is S. alchemilloides (Grisebach) Engler of northern Argentina and Bolivia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades shallowly lobed; stipules of all cauline leaves conspicuous; petals erect, usually pink to violet, rarely white (sometimes drying bluish), elliptic to obovate, 6-9 mm.</description>
      <determination>1 Suksdorfia violacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades deeply lobed; stipules of proximal cauline leaves barely expanded bases of petiole, stipules of distal cauline leaves conspicuous; petals spreading, white (sometimes pink tipped), broadly elliptic to obovate, 3-5 mm.</description>
      <determination>2 Suksdorfia ranunculifolia</determination>
    </key_statement>
  </key>
</bio:treatment>