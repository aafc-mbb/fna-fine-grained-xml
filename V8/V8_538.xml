<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sylvia Kelso</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch ex Borkhausen" date="unknown" rank="family">primulaceae</taxon_name>
    <taxon_name authority="Lindley" date="1827" rank="genus">DOUGLASIA</taxon_name>
    <place_of_publication>
      <publication_title>Quart. J. Sci. Lit. Arts [</publication_title>
      <place_in_publication>24]: 385. 1827, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family primulaceae;genus DOUGLASIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For David Douglas, 1798–1834, Scottish botanist and collector in northwestern North America</other_info_on_name>
    <other_info_on_name type="fna_id">110869</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial (biennial in D. alaskana), usually cushion or mat-forming, sometimes succulent (often suffrutescent).</text>
      <biological_entity id="o16667" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="cushion" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes absent;</text>
      <biological_entity id="o16669" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots slightly fibrous or a taproot.</text>
      <biological_entity id="o16670" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o16671" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems prostrate to ascending, simple or dichotomously branched.</text>
      <biological_entity id="o16672" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="dichotomously" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves in multiple basal rosettes (single rosette in D. alaskana), simple;</text>
      <biological_entity id="o16673" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16674" name="rosette" name_original="rosettes" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="multiple" value_original="multiple" />
      </biological_entity>
      <relation from="o16673" id="r1419" name="in" negation="false" src="d0_s4" to="o16674" />
    </statement>
    <statement id="d0_s5">
      <text>petiole absent or obscure, broadly winged;</text>
      <biological_entity id="o16675" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear to broadly lanceolate, spatulate, or cuneate, base attenuate, margins entire or slightly dentate, apex acute to obtuse, sometimes 3-toothed, surfaces glabrous or hairy, hairs simple, branched, or stellate.</text>
      <biological_entity id="o16676" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="broadly lanceolate spatulate or cuneate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="broadly lanceolate spatulate or cuneate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="broadly lanceolate spatulate or cuneate" />
      </biological_entity>
      <biological_entity id="o16677" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o16678" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16679" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
      <biological_entity id="o16680" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16681" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Scapes usually several per rosette, elongating until fruiting.</text>
      <biological_entity id="o16682" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character constraint="per rosette" constraintid="o16683" is_modifier="false" modifier="usually" name="quantity" src="d0_s7" value="several" value_original="several" />
        <character constraint="until fruiting" constraintid="o16684" is_modifier="false" name="length" notes="" src="d0_s7" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o16683" name="rosette" name_original="rosette" src="d0_s7" type="structure" />
      <biological_entity id="o16684" name="fruiting" name_original="fruiting" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences umbels, 2–10-flowered, involucrate, or solitary flowers;</text>
      <biological_entity constraint="inflorescences" id="o16685" name="umbel" name_original="umbels" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-10-flowered" value_original="2-10-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="involucrate" value_original="involucrate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="involucrate" value_original="involucrate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o16686" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracts absent or 1–10.</text>
      <biological_entity id="o16687" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or ascending, erect in fruit.</text>
      <biological_entity id="o16688" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character constraint="in fruit" constraintid="o16689" is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16689" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers homostylous;</text>
      <biological_entity id="o16690" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="homostylous" value_original="homostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 5, green, keeled at least on tube, calyx broadly campanulate, 5-angled, glabrous or stellate-hairy, lobes not reflexed, length ± equaling tube;</text>
      <biological_entity id="o16691" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character constraint="on tube" constraintid="o16692" is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o16692" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <biological_entity id="o16693" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="5-angled" value_original="5-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o16694" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o16695" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 5, pink, rose, or purple, sometimes turning white or violet in age, corolla salverform, constricted at throat, lobes not reflexed, shorter than tube, apex entire or erose;</text>
      <biological_entity id="o16696" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o16697" name="age" name_original="age" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o16698" name="corolla" name_original="corolla" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s13" value="salverform" value_original="salverform" />
        <character constraint="at throat" constraintid="o16699" is_modifier="false" name="size" src="d0_s13" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o16699" name="throat" name_original="throat" src="d0_s13" type="structure" />
      <biological_entity id="o16700" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character constraint="than tube" constraintid="o16701" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o16701" name="tube" name_original="tube" src="d0_s13" type="structure" />
      <biological_entity id="o16702" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o16696" id="r1420" modifier="sometimes" name="turning" negation="false" src="d0_s13" to="o16697" />
    </statement>
    <statement id="d0_s14">
      <text>stamens included;</text>
      <biological_entity id="o16703" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments indistinct;</text>
      <biological_entity id="o16704" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers not connivent.</text>
      <biological_entity id="o16705" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s16" value="connivent" value_original="connivent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules ovoid to globose, valvate, dehiscent to base.</text>
      <biological_entity id="o16706" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="globose" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s17" value="valvate" value_original="valvate" />
        <character constraint="to base" constraintid="o16707" is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o16707" name="base" name_original="base" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1–4, brown, 4-angled, oblong, reticulate.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 18, 19.</text>
      <biological_entity id="o16708" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s18" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s18" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s18" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o16709" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
        <character name="quantity" src="d0_s19" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America, e Asia (Russian Far East); arctic and alpine regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="arctic and alpine regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Species 9 (9 in the flora).</discussion>
  <discussion>Whether Douglasia should be considered a separate genus, part of a very broadly construed Androsace, or, perhaps, part of a new segregate genus comprising Androsace sect. Aretia, has been a longstanding question and remains unresolved. Molecular analyses (G. M. Schneeweiss et al. 2004) show a distinct clade that includes the North American and arctic Douglasia species; it is part of a larger primarily European group of Androsace species in sect. Aretia. Although morphologically very similar to those Androsace species, the Douglasia clade differs in chromosome number (chiefly 2n = 38) and shows a large range disjunction between the Alps and the Bering Strait. As currently construed, Androsace in the broad sense covers a broad range of morphologies, especially in the little-studied Asiatic sect. Pseudoprimula, which, morphologically and karyologically, is closer to Primula than to other Androsace; other sections such as the Chamaejasme group show lesser but still significant discontinuities. Additional comprehensive genetic analyses of the entire Androsace complex are needed before it will be possible to assess the appropriateness of segregate or aggregate nomenclature. This treatment follows the current, conventional view in North America of a segregate generic status for Douglasia.</discussion>
  <discussion>Throughout its range, Douglasia shows a pattern of closely related species with narrow distributions. The species differ primarily in the type and placement of hairs on the vegetative parts; these characters are consistent and reliable markers. Molecular markers (G. M. Schneeweiss et al. 2004) support close relationships but also indicate separate branches that support species-level designations as well as separate clades within Douglasia, where arctic species form one group and those of the continental northwest, from Washington to Montana, another.</discussion>
  <references>
    <reference>Constance, L. 1938. A revision of the genus Douglasia Lindl. Amer. Midl. Naturalist 19: 249–259.</reference>
    <reference>Kelso, S. 1992. Conspectus of the genus Douglasia (Primulaceae) with comments on Douglasia alaskana, an Alaska–Yukon alpine endemic. Canad. J. Bot. 70: 593–596.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves in single basal rosette; plants not cushion- or mat-forming</description>
      <determination>1 Douglasia alaskana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves in multiple rosettes or plants cushion- or mat-forming</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems ± densely covered with reddish or reddish brown, marcescent leaves; arctic species of Alaska and Canada</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems with green or gray to light maroon leaves (not densely covered with reddish or reddish brown, marcescent leaves); Cordilleran species of northwestern North America and southern Canada</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades usually glabrous adaxially, margin hairs simple</description>
      <determination>2 Douglasia arctica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades usually hairy adaxially, sometimes only at apex, margin hairs simple, forked, or branched</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves prominently recurved, blade with simple hairs.</description>
      <determination>9 Douglasia ochotensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves ascending or erect, blade with mostly forked, branched or stellate hairs</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Calyx and bracts densely hairy, hairs branched and/or stellate.</description>
      <determination>3 Douglasia beringensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Calyx and bracts glabrous.</description>
      <determination>4 Douglasia gormanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences 1-2-flowered.</description>
      <determination>7 Douglasia montana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences (2-)3-10-flowered</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades densely hairy, hairs branched and stellate.</description>
      <determination>8 Douglasia nivalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades usually glabrous, sometimes ciliate on margins</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades 1-2 mm wide; involucral bracts lanceolate to ovate- lanceolate.</description>
      <determination>5 Douglasia idahoensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades 2-6 mm wide; involucral bracts lanceolate to ovate.</description>
      <determination>6 Douglasia laevigata</determination>
    </key_statement>
  </key>
</bio:treatment>