<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="treatment_page">230</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Rydberg ex Britton" date="unknown" rank="family">PENTHORACEAE</taxon_name>
    <taxon_hierarchy>family PENTHORACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">20226</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennial herbs.</text>
      <biological_entity id="o7607" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branched.</text>
      <biological_entity id="o7608" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, alternate, simple;</text>
      <biological_entity id="o7609" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o7610" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present or absent;</text>
      <biological_entity id="o7611" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins sharply serrate or doubly serrate.</text>
      <biological_entity constraint="blade" id="o7612" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, scorpioid racemes.</text>
      <biological_entity id="o7613" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o7614" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="scorpioid" value_original="scorpioid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o7615" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth and androecium hypogynous or perigynous;</text>
      <biological_entity id="o7616" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o7617" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium present;</text>
      <biological_entity id="o7618" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5 (–8), connate proximally;</text>
      <biological_entity id="o7619" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="8" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals absent or 1–8, distinct;</text>
      <biological_entity id="o7620" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc absent;</text>
      <biological_entity constraint="nectary" id="o7621" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10, distinct, adnate to rim of hypanthium;</text>
      <biological_entity id="o7622" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character constraint="to rim" constraintid="o7623" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o7623" name="rim" name_original="rim" src="d0_s13" type="structure" />
      <biological_entity id="o7624" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure" />
      <relation from="o7623" id="r603" name="part_of" negation="false" src="d0_s13" to="o7624" />
    </statement>
    <statement id="d0_s14">
      <text>anthers dehiscent by longitudinal slits;</text>
      <biological_entity id="o7625" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="by slits" constraintid="o7626" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o7626" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 1, (4–) 5 (–8) -carpellate, connate basally and laterally, partially adnate to hypanthium;</text>
      <biological_entity id="o7627" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="(4-)5(-8)-carpellate" value_original="(4-)5(-8)-carpellate" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="laterally; partially" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o7628" name="hypanthium" name_original="hypanthium" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>ovary superior or partially inferior, (4–) 5 (–8) -locular proximally;</text>
    </statement>
    <statement id="d0_s17">
      <text>placentation marginal;</text>
      <biological_entity id="o7629" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="superior" value_original="superior" />
        <character is_modifier="false" modifier="partially" name="position" src="d0_s16" value="inferior" value_original="inferior" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="(4-)5(-8)-locular" value_original="(4-)5(-8)-locular" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="marginal" value_original="marginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules anatropous, bitegmic, crassinucellate;</text>
      <biological_entity id="o7630" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="crassinucellate" value_original="crassinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles 1 per carpel, distinct;</text>
      <biological_entity id="o7631" name="style" name_original="styles" src="d0_s19" type="structure">
        <character constraint="per carpel" constraintid="o7632" name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s19" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o7632" name="carpel" name_original="carpel" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>stigmas 1 per carpel, terminal, capitate.</text>
      <biological_entity id="o7633" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character constraint="per carpel" constraintid="o7634" name="quantity" src="d0_s20" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s20" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o7634" name="carpel" name_original="carpel" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsular, dehiscence circumscissile proximal to free portion of styles.</text>
      <biological_entity id="o7635" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s21" value="circumscissile" value_original="circumscissile" />
        <character is_modifier="false" name="position" src="d0_s21" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o7636" name="portion" name_original="portion" src="d0_s21" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s21" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o7637" name="style" name_original="styles" src="d0_s21" type="structure" />
      <relation from="o7636" id="r604" name="part_of" negation="false" src="d0_s21" to="o7637" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds 300–400, tan, yellowish-brown, or pinkish, ellipsoid to subfusiform, funicular end more tapered;</text>
      <biological_entity id="o7638" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="300" name="quantity" src="d0_s22" to="400" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s22" to="subfusiform" />
      </biological_entity>
      <biological_entity id="o7639" name="end" name_original="end" src="d0_s22" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s22" value="funicular" value_original="funicular" />
        <character is_modifier="false" name="shape" src="d0_s22" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>embryo straight;</text>
      <biological_entity id="o7640" name="embryo" name_original="embryo" src="d0_s23" type="structure">
        <character is_modifier="false" name="course" src="d0_s23" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>endosperm scant, cellular.</text>
      <biological_entity id="o7641" name="endosperm" name_original="endosperm" src="d0_s24" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s24" value="scant" value_original="scant" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="cellular" value_original="cellular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe (Russia), Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe (Russia)" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Ditch</other_name>
  <other_name type="common_name">stonecrop Family</other_name>
  <discussion>Genus 1, species 2 (1 in the flora).</discussion>
  <discussion>The position of Penthorum within Rosales has been disputed extensively. A. Cronquist (1981) considered it to be transitional between Crassulaceae and Saxifragaceae. He included it in Saxifragaceae, stating that Penthorum was not distinct enough from Crassulaceae and Saxifragaceae to warrant being treated as a distinct family. Placement of the genus by others has depended on the morphological, anatomical, and embryological traits emphasized. Molecular studies suggest that the genus is sister to Haloragaceae (D. R. Morgan and D. E. Soltis 1993; D. E. Soltis and P. S. Soltis 1997). Recent authors often have placed it in the monogeneric Penthoraceae.</discussion>
  <references>
    <reference>Baldwin, J. T. and B. M. Speese. 1951. Penthorum: Its chromosomes. Rhodora 53: 89–91.</reference>
    <reference>Haskins, M. L. and W. J. Hayden. 1987. Anatomy and affinities of Penthorum. Amer. J. Bot. 74: 164–177.</reference>
    <reference>Spongberg, S. A. 1972. The genera of Saxifragaceae in the southeastern United States. J. Arnold Arbor. 53: 409–498.</reference>
  </references>
  
</bio:treatment>