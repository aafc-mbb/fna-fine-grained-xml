<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="treatment_page">475</other_info_on_meta>
    <other_info_on_meta type="illustration_page">472</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">epigaea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">repens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 395. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus epigaea;species repens;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220004812</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epigaea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">repens</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">glabrifolia</taxon_name>
    <taxon_hierarchy>genus Epigaea;species repens;variety glabrifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epigaea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">repens</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="variety">rubicunda</taxon_name>
    <taxon_hierarchy>genus Epigaea;species repens;variety rubicunda;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate or creeping;</text>
      <biological_entity id="o1872" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches spreading.</text>
      <biological_entity id="o1873" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems to 30 (–50) cm.</text>
      <biological_entity id="o1874" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Twigs often hirsute-hispid, especially when young.</text>
      <biological_entity id="o1875" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="hirsute-hispid" value_original="hirsute-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves alternate (distal leaves sometimes closely spaced, appearing subopposite or opposite);</text>
      <biological_entity id="o1876" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–5 cm, with unicellular hairs of varying lengths;</text>
      <biological_entity id="o1877" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1878" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <relation from="o1877" id="r163" modifier="of varying lengths" name="with" negation="false" src="d0_s5" to="o1878" />
    </statement>
    <statement id="d0_s6">
      <text>blade ovate-elliptic, (2–) 3–8 (–10) × 1.5–5.5 cm, base rounded or cordate, margins ciliate with stiff hairs, plane, apex obtuse to acute and mucronate, abaxial surface with distinctive reticulate venation, adaxial and abaxial surfaces with long, soft or stiff hairs, becoming glabrate.</text>
      <biological_entity id="o1879" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s6" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1880" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o1881" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="with hairs" constraintid="o1882" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o1882" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1883" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1884" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o1885" name="vein" name_original="venation" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="distinctive" value_original="distinctive" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="and abaxial adaxial" id="o1886" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="becoming" name="pubescence" notes="" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o1887" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
        <character is_modifier="true" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o1884" id="r164" name="with" negation="false" src="d0_s6" to="o1885" />
      <relation from="o1886" id="r165" name="with" negation="false" src="d0_s6" to="o1887" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary and terminal clusters, 2–5 cm;</text>
      <biological_entity id="o1888" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles persistent, 2, 4–7 mm (often extending to calyx lobes), hirsute.</text>
      <biological_entity id="o1889" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or unisexual due to undeveloped or sterile stamens or ovary (staminate flowers larger than carpellate), with spicy fragrance;</text>
      <biological_entity id="o1890" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character char_type="range_value" from="due" name="prominence" src="d0_s9" to="undeveloped" />
      </biological_entity>
      <biological_entity id="o1891" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o1892" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals distinct nearly to base, 5–6 mm, hirsute;</text>
      <biological_entity id="o1893" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character constraint="to base" constraintid="o1894" is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o1894" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>corolla-tube 6–10 (–15) mm, lobes 5, spreading, rose or pink to white, 6–10 mm;</text>
      <biological_entity id="o1895" name="corolla-tube" name_original="corolla-tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1896" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments adnate to base of corolla-tube, hairy basally;</text>
      <biological_entity id="o1897" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character constraint="to base" constraintid="o1898" is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="basally" name="pubescence" notes="" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o1898" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o1899" name="corolla-tube" name_original="corolla-tube" src="d0_s12" type="structure" />
      <relation from="o1898" id="r166" name="part_of" negation="false" src="d0_s12" to="o1899" />
    </statement>
    <statement id="d0_s13">
      <text>ovary glandular-hairy;</text>
      <biological_entity id="o1900" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style slender, with collar or ring of tissue partly adnate to stigma.</text>
      <biological_entity id="o1901" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o1902" name="collar" name_original="collar" src="d0_s14" type="structure" />
      <biological_entity id="o1903" name="ring" name_original="ring" src="d0_s14" type="structure">
        <character constraint="to stigma" constraintid="o1905" is_modifier="false" modifier="partly" name="fusion" src="d0_s14" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o1904" name="tissue" name_original="tissue" src="d0_s14" type="structure" />
      <biological_entity id="o1905" name="stigma" name_original="stigma" src="d0_s14" type="structure" />
      <relation from="o1901" id="r167" name="with" negation="false" src="d0_s14" to="o1902" />
      <relation from="o1901" id="r168" name="with" negation="false" src="d0_s14" to="o1903" />
      <relation from="o1903" id="r169" name="part_of" negation="false" src="d0_s14" to="o1904" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules depressed-globose, 5-lobed, 5–8 mm diam., fleshy, outer surface glandular-hirsute, white and pulpy within due to persistent, fleshy placentae.</text>
      <biological_entity id="o1906" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-globose" value_original="depressed-globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="5-lobed" value_original="5-lobed" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1907" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glandular-hirsute" value_original="glandular-hirsute" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character constraint="within placentae" constraintid="o1908" is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="pulpy" value_original="pulpy" />
      </biological_entity>
      <biological_entity id="o1908" name="placenta" name_original="placentae" src="d0_s15" type="structure">
        <character is_modifier="true" name="duration" src="d0_s15" value="due-to-persistent" value_original="due-to-persistent" />
        <character is_modifier="true" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, 0.5 mm. 2n = 24.</text>
      <biological_entity id="o1909" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1910" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to xeric pine or deciduous forests, clearings, in sandy, rocky, or peaty soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to xeric pine" />
        <character name="habitat" value="moist to deciduous forests" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="sandy" modifier="in" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="peaty soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I.; Ala., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Mayflower</other_name>
  <other_name type="common_name">ground laurel</other_name>
  <discussion>Although common in much of its range, Epigaea repens is rare in Florida, occurring only in the western part of the state. Although documented from Illinois, it has not been seen there since the late 1800s. It is a well known member of the early spring flora, usually beginning to flower well before deciduous trees leaf out. Commonly known as mayflower, it was designated the state flower of Massachusetts in 1918 and received legal protection there in 1925. The flowers are sweetly fragrant.</discussion>
  
</bio:treatment>