<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="illustration_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">sapotaceae</taxon_name>
    <taxon_name authority="Aublet" date="1775" rank="genus">pouteria</taxon_name>
    <taxon_name authority="(Kunth) Baehni" date="1942" rank="species">campechiana</taxon_name>
    <place_of_publication>
      <publication_title>Candollea</publication_title>
      <place_in_publication>9: 398. 1942,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sapotaceae;genus pouteria;species campechiana</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250043374</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lucuma</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">campechiana</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>3(fol.): 188; 3(qto.): 240. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lucuma;species campechiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lucuma</taxon_name>
    <taxon_name authority="A. de Candolle" date="unknown" rank="species">nervosa</taxon_name>
    <taxon_hierarchy>genus Lucuma;species nervosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 8 m.</text>
      <biological_entity id="o15513" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="8" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 10–25 (–45) mm, finely hairy;</text>
      <biological_entity id="o15514" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15515" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic to oblanceolate or obovate, 80–250 (–330) × 30–80 (–150) mm, margins revolute.</text>
      <biological_entity id="o15516" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15517" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblanceolate or obovate" />
        <character char_type="range_value" from="250" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="330" to_unit="mm" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s2" to="250" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="150" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s2" to="80" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15518" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 6–12 mm, densely hairy.</text>
      <biological_entity id="o15519" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals ovate to suborbiculate, 4.5–11 mm;</text>
      <biological_entity id="o15520" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o15521" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 8–12 mm, tube 5–6 (–8) mm;</text>
      <biological_entity id="o15522" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15523" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15524" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminodes petaloid, 2–4 mm.</text>
      <biological_entity id="o15525" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15526" name="staminode" name_original="staminodes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Berries 25–70 mm, apex short-rostrate, surface smooth.</text>
      <biological_entity id="o15527" name="berry" name_original="berries" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15528" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
      <biological_entity id="o15529" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1–6, 20–40 mm.</text>
      <biological_entity id="o15530" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hardwood hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hardwood hammocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-5 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="5" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Mexico; West Indies (Cuba); Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Canistel</other_name>
  <other_name type="common_name">zapotillo</other_name>
  <other_name type="common_name">zapote blanco</other_name>
  <discussion>Pouteria campechiana, native from Mexico to Panama, is frequently cultivated and escaped or persistent in the West Indies and Florida.</discussion>
  
</bio:treatment>