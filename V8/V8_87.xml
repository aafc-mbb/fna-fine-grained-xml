<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">49</other_info_on_meta>
    <other_info_on_meta type="illustration_page">33</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="A. M. Johnson" date="1927" rank="genus">cascadia</taxon_name>
    <taxon_name authority="(Small) A. M. Johnson" date="1927" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Corrig.</publication_title>
      <place_in_publication>1927  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus cascadia;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065869</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 368. 1896,</place_in_publication>
      <other_info_on_pub>based on S. elegans Nuttall in J. Torrey and A. Gray, Fl. N. Amer. 1: 573. 1840, not Sternberg 1831</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems branched proximal to middle, slender.</text>
      <biological_entity id="o1172" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="position" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s0" to="middle" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves largest along middle of stems;</text>
      <biological_entity id="o1173" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="along middle leaves" constraintid="o1174" is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity constraint="middle" id="o1174" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1175" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <relation from="o1174" id="r114" name="part_of" negation="false" src="d0_s1" to="o1175" />
    </statement>
    <statement id="d0_s2">
      <text>petiole distinct, 1–6 mm;</text>
      <biological_entity id="o1176" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 3–20 mm.</text>
      <biological_entity id="o1177" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels filiform.</text>
      <biological_entity id="o1178" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals erect, triangular, apex acute;</text>
      <biological_entity id="o1179" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1180" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o1181" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals elliptic to obovate, 3–6 mm, much longer than sepals.</text>
      <biological_entity id="o1182" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1183" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character constraint="than sepals" constraintid="o1184" is_modifier="false" name="length_or_size" src="d0_s6" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o1184" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules 2–5 mm. 2n = 16.</text>
      <biological_entity id="o1185" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1186" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, shaded cliffs and ledges, sometimes coastal or near waterfalls, mossy mats, wet rocks, moist crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded cliffs" modifier="wet" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="coastal" modifier="sometimes" />
        <character name="habitat" value="near waterfalls" />
        <character name="habitat" value="mossy mats" />
        <character name="habitat" value="wet rocks" />
        <character name="habitat" value="moist crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Nuttall’s saxifrage</other_name>
  <discussion>Cascadia nuttallii is found from the coastal mountains to the western slopes of the Cascade Range, from extreme northwestern California to southwestern Washington. The accepted species name was validated in a correction slip attached to reprints of Johnson’s article describing Cascadia.</discussion>
  
</bio:treatment>