<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
    <other_info_on_meta type="illustration_page">141</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="(Small) Fedde" date="1906" rank="species">vespertina</taxon_name>
    <place_of_publication>
      <publication_title>Just’s Bot. Jahresber.</publication_title>
      <place_in_publication>33(1): 613. 1906,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species vespertina</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092016</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptasea</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">vespertina</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 153. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leptasea;species vespertina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">bronchialis</taxon_name>
    <taxon_name authority="(Small) Piper" date="unknown" rank="subspecies">vespertina</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species bronchialis;subspecies vespertina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bronchialis</taxon_name>
    <taxon_name authority="(Small) Rosendahl" date="unknown" rank="variety">vespertina</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species bronchialis;variety vespertina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming loose mats, (stems trailing), not stoloniferous, rhizomatous.</text>
      <biological_entity id="o3258" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3259" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o3258" id="r264" name="forming" negation="false" src="d0_s0" to="o3259" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline (crowded proximally);</text>
      <biological_entity id="o3260" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole absent;</text>
      <biological_entity id="o3261" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade spatulate, unlobed or minutely 3-toothed or lobed apically, 4–11 mm, leathery, margins entire, (not cartilaginous), stiffly ciliate, apex rounded or obtuse, mucronate, surfaces glabrous.</text>
      <biological_entity id="o3262" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s3" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="11" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o3263" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="stiffly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o3264" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o3265" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–6-flowered cymes, 2.5–12 cm, pink to purple-tipped stipitate-glandular;</text>
      <biological_entity id="o3266" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o3267" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="3-6-flowered" value_original="3-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts sessile.</text>
      <biological_entity id="o3268" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals erect, ovate to oblong, margins sparsely ciliate, surfaces glabrous;</text>
      <biological_entity id="o3269" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3270" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="oblong" />
      </biological_entity>
      <biological_entity id="o3271" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o3272" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white to cream, yellow-spotted proximally, red or orange-spotted distally, these often faded in dried specimens, elliptic, 4–6 mm, much longer than sepals;</text>
      <biological_entity id="o3273" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3274" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="cream yellow-spotted proximally" />
        <character char_type="range_value" from="white" modifier="distally" name="coloration" src="d0_s7" to="cream yellow-spotted proximally" />
        <character is_modifier="false" modifier="often" name="arrangement_or_shape" notes="" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="than sepals" constraintid="o3276" is_modifier="false" name="length_or_size" src="d0_s7" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o3275" name="specimen" name_original="specimens" src="d0_s7" type="structure">
        <character is_modifier="true" name="condition" src="d0_s7" value="dried" value_original="dried" />
      </biological_entity>
      <biological_entity id="o3276" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <relation from="o3274" id="r265" name="faded in" negation="false" src="d0_s7" to="o3275" />
    </statement>
    <statement id="d0_s8">
      <text>ovary superior.</text>
      <biological_entity id="o3277" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 26.</text>
      <biological_entity id="o3278" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="superior" value_original="superior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3279" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, ledges, crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  
</bio:treatment>