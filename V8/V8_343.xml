<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
    <other_info_on_meta type="illustration_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="(de Candolle) Opiz" date="1852" rank="genus">jovibarba</taxon_name>
    <taxon_name authority="(Schott) Á. Löve &amp; D. Löve" date="1961" rank="species">heuffelii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>114: 39. 1961,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus jovibarba;species heuffelii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092049</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sempervivum</taxon_name>
    <taxon_name authority="Schott" date="unknown" rank="species">heuffelii</taxon_name>
    <place_of_publication>
      <publication_title>Oesterr. Bot. Wochenbl.</publication_title>
      <place_in_publication>2: 18. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sempervivum;species heuffelii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: rosettes usually closed, globose, 0.5–5 cm diam., producing offsets attached directly to primary-roots;</text>
      <biological_entity id="o5055" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o5056" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="condition" src="d0_s0" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s0" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="diameter" src="d0_s0" to="5" to_unit="cm" />
        <character constraint="to primary-roots" constraintid="o5058" is_modifier="false" name="fixation" src="d0_s0" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o5057" name="offset" name_original="offsets" src="d0_s0" type="structure" />
      <biological_entity id="o5058" name="primary-root" name_original="primary-roots" src="d0_s0" type="structure" />
      <relation from="o5056" id="r439" name="producing" negation="false" src="d0_s0" to="o5057" />
    </statement>
    <statement id="d0_s1">
      <text>blade 1–1.5 cm wide, apex mucronate, surfaces usually glabrous, margins sometimes ciliate with reflexed hairs, often glaucous.</text>
      <biological_entity id="o5059" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5060" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5061" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o5062" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5063" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character constraint="with hairs" constraintid="o5064" is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="often" name="pubescence" notes="" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o5064" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences erect, usually densely ca. 12–40-flowered, bracteate, 7–20 cm;</text>
      <biological_entity id="o5065" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually densely" name="architecture" src="d0_s2" value="12-40-flowered" value_original="12-40-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>dichasium compact, 3–5 cm diam.;</text>
      <biological_entity id="o5066" name="dichasium" name_original="dichasium" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="compact" value_original="compact" />
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rachis 5–15 (–20) cm, glabrous;</text>
      <biological_entity id="o5067" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 1 per flower, clasping at base, lanceolate, 1–3 cm, base broad.</text>
      <biological_entity id="o5068" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character constraint="per flower" constraintid="o5069" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="at base" constraintid="o5070" is_modifier="false" name="architecture_or_fixation" notes="" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5069" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <biological_entity id="o5070" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o5071" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: petal margins fimbrillate, abaxially pubescent.</text>
      <biological_entity id="o5072" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 38.</text>
      <biological_entity constraint="petal" id="o5073" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="fimbrillate" value_original="fimbrillate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5074" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Jovibarba heuffelii is likely to have naturalized elsewhere in the flora area. Plants may have been misidentified as Sempervivum tectorum. It is intolerant of treading and therefore is likely to be found only in areas that provide a measure of physical protection.</discussion>
  
</bio:treatment>