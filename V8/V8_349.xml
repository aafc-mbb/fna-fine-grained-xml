<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="(Britton &amp; Rose) Moran" date="1942" rank="subgenus">Stylophyllum</taxon_name>
    <taxon_name authority="(S. Watson) Moran" date="1943" rank="species">viscida</taxon_name>
    <place_of_publication>
      <publication_title>Desert Pl. Life</publication_title>
      <place_in_publication>14: 191. 1943  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus stylophyllum;species viscida;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092054</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotyledon</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">viscida</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 372. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotyledon;species viscida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stylophyllum</taxon_name>
    <taxon_name authority="(S. Watson) Britton &amp; Rose" date="unknown" rank="species">viscidum</taxon_name>
    <taxon_hierarchy>genus Stylophyllum;species viscidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices to 5 dm × 1–4 cm, clumps to 5+ dm diam.</text>
      <biological_entity id="o8479" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8480" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="diameter" src="d0_s0" to="5" to_unit="dm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves with resinous odor;</text>
      <biological_entity id="o8481" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rosette 15–50-leaved, 10–20 cm diam.;</text>
      <biological_entity id="o8482" name="rosette" name_original="rosette" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="15-50-leaved" value_original="15-50-leaved" />
        <character char_type="range_value" from="10" from_unit="cm" name="diameter" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light to dark or yellowish green, linear-deltate, tapering from base, convex or obtusely angled abaxially, laminar and flat or convex adaxially, often terete near apex, 6–15 × 0.5–1.5 cm, 2–5 mm thick, 2–6 times wider than thick, base 1–2 cm wide, surfaces not farinose, viscid, appearing oily.</text>
      <biological_entity id="o8483" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s3" to="dark or yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-deltate" value_original="linear-deltate" />
        <character constraint="from base" constraintid="o8484" is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="obtusely; abaxially" name="shape" src="d0_s3" value="angled" value_original="angled" />
        <character is_modifier="false" name="position" src="d0_s3" value="laminar" value_original="laminar" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s3" value="convex" value_original="convex" />
        <character constraint="near apex" constraintid="o8485" is_modifier="false" modifier="often" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character name="thickness" notes="" src="d0_s3" unit="cm" value="6-15×0.5-1.5" value_original="6-15×0.5-1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" notes="" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="width_or_width" src="d0_s3" value="2-6 times wider than thick" />
      </biological_entity>
      <biological_entity id="o8484" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o8485" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o8486" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8487" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
        <character is_modifier="false" name="coating" src="d0_s3" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="coating" src="d0_s3" value="oily" value_original="oily" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: cyme 3–15-branched, flat to cylindric, (to 3 × 1.5 dm);</text>
      <biological_entity id="o8488" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o8489" name="cyme" name_original="cyme" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-15-branched" value_original="3-15-branched" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches (close-set), 1–2 times bifurcate;</text>
      <biological_entity id="o8490" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="1-2 times bifurcate" value_original="1-2 times bifurcate " />
      </biological_entity>
      <biological_entity id="o8491" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cincinni 3–11-flowered, scarcely circinate, 2–7 cm;</text>
      <biological_entity id="o8492" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o8493" name="cincinnus" name_original="cincinni" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-11-flowered" value_original="3-11-flowered" />
        <character is_modifier="false" modifier="scarcely" name="shape_or_vernation" src="d0_s6" value="circinate" value_original="circinate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral shoots 2–7 dm × 2–10 mm;</text>
      <biological_entity id="o8494" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o8495" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s7" to="7" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaves 20–40, ascending, triangular-lanceolate, 1–6 × 0.3–0.5 cm.</text>
      <biological_entity id="o8496" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o8497" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s8" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 1–4 mm.</text>
      <biological_entity id="o8498" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: petals widespreading from middle or slightly reflexed, connate 1–2 mm, white to pink, elliptic, 6–10 × 2.5–3.5 mm, apex acute, corolla 12–22 mm diam.;</text>
      <biological_entity id="o8499" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8500" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="from " constraintid="o8502" is_modifier="false" name="orientation" src="d0_s10" value="widespreading" value_original="widespreading" />
      </biological_entity>
      <biological_entity id="o8501" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o8502" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" constraint="from " constraintid="o8504" from="1" from_unit="mm" name="location" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8503" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o8504" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" constraint="from " constraintid="o8506" from="white" name="coloration" src="d0_s10" to="pink" />
      </biological_entity>
      <biological_entity id="o8505" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o8506" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" constraint="from " constraintid="o8508" from="2.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8507" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o8508" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8509" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistils suberect or ascending, (pink, slender), 6–10 mm;</text>
      <biological_entity id="o8510" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8511" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 2.4–4 mm.</text>
      <biological_entity id="o8512" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8513" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Follicles ascending, with adaxial margins ca. 45–60º above horizontal.</text>
      <biological_entity constraint="adaxial" id="o8515" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <relation from="o8514" id="r683" modifier="above horizontal" name="with" negation="false" src="d0_s13" to="o8515" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o8514" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8516" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Among rocks and on north-facing cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocks" modifier="among" />
        <character name="habitat" value="north-facing cliffs" modifier="and on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Sticky dudleya</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Dudleya viscida is known from only five or six areas on the mainland, in Orange and San Diego counties, some affected by development and road construction; it is considered fairly threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi). It is abundant where it grows; in San Juan Canyon, Orange County, for example, it can be seen from the highway, off and on, for some six kilometers, from 190 to 350 meters, with a total of thousands of plants. Reproduction is vigorous, and old roadcuts are heavily colonized. It is a local plant but hardly a rare one.</discussion>
  <discussion>Dudleya viscida differs conspicuously from all other species of the genus in its strongly viscid and fragrant herbage. The sticky exudate seems to come from the epidermal cells generally, in which the cytoplasm looks much denser than in subepidermal cells and in epidermal cells of nonviscid Crassulaceae. R. L. Rodriguez C. (pers. comm.) said that this exudate appeared to be a soft resin or oleoresin with a terpene solvent. Among other species, only D. anomala of Baja California is even slightly viscid and odorous; it is a polyploid that very likely is derived in part from D. viscida.</discussion>
  <discussion>According to P. H. Thomson (1993), Dudleya viscida hybridizes in nature with D. edulis and with D. pulverulenta. It is an attractive plant that does well in cultivation.</discussion>
  
</bio:treatment>