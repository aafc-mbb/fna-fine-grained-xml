<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">412</other_info_on_meta>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">arctostaphylos</taxon_name>
    <taxon_name authority="Eastwood" date="1934" rank="species">bakeri</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>1: 115. 1934  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus arctostaphylos;species bakeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092400</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctostaphylos</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stanfordiana</taxon_name>
    <taxon_name authority="(Eastwood) J. E. Adams" date="unknown" rank="subspecies">bakeri</taxon_name>
    <taxon_hierarchy>genus Arctostaphylos;species stanfordiana;subspecies bakeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect, 1–3 m;</text>
      <biological_entity id="o17174" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>burl absent;</text>
      <biological_entity id="o17175" name="burl" name_original="burl" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs densely glandular-hairy, puberulent, or finely tomentose with sessile glands.</text>
      <biological_entity id="o17176" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character constraint="with glands" constraintid="o17177" is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o17177" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 3–8 mm;</text>
      <biological_entity id="o17178" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17179" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dark green, not glaucous, dull, oblong-ovate to orbiculate-ovate, 2.5–3 × 1.5–2 cm, base usually cuneate, sometimes truncate to rounded, margins entire, plane, surfaces papillate, glandular-hispidulous.</text>
      <biological_entity id="o17180" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17181" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s4" to="orbiculate-ovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17182" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="truncate" modifier="sometimes" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o17183" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o17184" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-hispidulous" value_original="glandular-hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences panicles, 2–5-branched;</text>
      <biological_entity constraint="inflorescences" id="o17185" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-5-branched" value_original="2-5-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>immature inflorescence pendent, branches spreading, axis 1–2 cm, 1+ mm diam., finely glandular-hairy, puberulent, or finely tomentose;</text>
      <biological_entity id="o17186" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="immature" value_original="immature" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o17187" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o17188" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s6" upper_restricted="false" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts not fully appressed, scalelike, deltate to awl-like, 2–4 mm, apex acute, surfaces finely glandular-hairy, puberulent, or finely tomentose.</text>
      <biological_entity id="o17189" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not fully" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s7" to="awl-like" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17190" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o17191" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 3–5 mm, glabrous.</text>
      <biological_entity id="o17192" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: corolla white, conic to urceolate;</text>
      <biological_entity id="o17193" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17194" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary glabrous.</text>
      <biological_entity id="o17195" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17196" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits depressed-globose, 8–10 mm diam., glabrous.</text>
      <biological_entity id="o17197" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stones distinct.</text>
      <biological_entity id="o17198" name="stone" name_original="stones" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <other_name type="common_name">Baker’s manzanita</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Twigs, petioles, rachises, and bracts glandular-hairy; immature inflorescence axes 1-1.5 cm.</description>
      <determination>59a Arctostaphylos bakeri subsp. bakeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Twigs, petioles, rachises, and bracts eglandular-hairy, with sessile glands beneath; immature inflorescence axes 1.5-2 cm.</description>
      <determination>59b Arctostaphylos bakeri subsp. sublaevis</determination>
    </key_statement>
  </key>
</bio:treatment>