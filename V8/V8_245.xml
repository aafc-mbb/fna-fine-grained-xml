<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">123</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="genus">sullivantia</taxon_name>
    <taxon_name authority="(J. M. Coulter &amp; Fisher) J. M. Coulter" date="1892" rank="species">hapemanii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">hapemanii</taxon_name>
    <taxon_hierarchy>family saxifragaceae;genus sullivantia;species hapemanii;variety hapemanii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065994</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sullivantia</taxon_name>
    <taxon_name authority="A. Nelson ex Small" date="unknown" rank="species">halmicola</taxon_name>
    <taxon_hierarchy>genus Sullivantia;species halmicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowering-stems 5–60 cm.</text>
      <biological_entity id="o10970" name="flowering-stem" name_original="flowering-stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1–11 cm wide, 5–13-lobed for 1/4–1/2 length, lobes cuneate-oblong to subovate, 1–2 times dentate to subcrenate.</text>
      <biological_entity id="o10971" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="11" to_unit="cm" />
        <character is_modifier="false" name="length" src="d0_s1" value="5-13-lobed" value_original="5-13-lobed" />
      </biological_entity>
      <biological_entity id="o10972" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="cuneate-oblong" name="shape" src="d0_s1" to="subovate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-2 times dentate to subcrenate " />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals triangular-ovate, 0.6–1.3 × 0.6–1 mm;</text>
      <biological_entity id="o10973" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o10974" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s2" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals ovate and abruptly clawed to ovatelanceolate and more gradually narrowed into claw;</text>
      <biological_entity id="o10975" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o10976" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="abruptly clawed" name="shape" src="d0_s3" to="ovatelanceolate" />
        <character constraint="into claw" constraintid="o10977" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o10977" name="claw" name_original="claw" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ovary ± as long as wide.</text>
      <biological_entity id="o10978" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o10979" name="ovary" name_original="ovary" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsules cylindric-ovate, 3.2–8 × 1.5–4 mm, to 2.5 times longer than wide.</text>
      <biological_entity id="o10980" name="capsule" name_original="capsules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric-ovate" value_original="cylindric-ovate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="0-2.5" value_original="0-2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seeds 0.9–1.6 mm. 2n = 14.</text>
      <biological_entity id="o10981" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10982" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist cliffs, stream banks, springs, near waterfalls, typically on limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist cliffs" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="near waterfalls" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <discussion>Variety hapemanii occurs in the Bighorn Mountains of north-central Wyoming and Bighorn Canyon of south-central Montana, with outliers in the Owl Creek Mountains and northern Laramie Range, Wyoming. It is also disjunct in the middle fork of the Salmon River in central Idaho.</discussion>
  
</bio:treatment>