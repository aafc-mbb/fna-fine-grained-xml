<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Engelmann ex A. Gray" date="1864" rank="species">debilis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>15: 62. 1864,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species debilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092021</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cernua</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Engler" date="unknown" rank="variety">debilis</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species cernua;variety debilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="species">hyperborea</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Á. Löve, D. Löve &amp; B. M. Kapoor" date="unknown" rank="subspecies">debilis</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species hyperborea;subspecies debilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Dorn" date="unknown" rank="variety">debilis</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species rivularis;variety debilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually densely tufted, sometimes loosely so, not stoloniferous, not rhizomatous.</text>
      <biological_entity id="o7163" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="sometimes loosely; loosely; not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, (3–5, proximal similar to basal);</text>
      <biological_entity id="o7164" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole ± flattened, 5–70 mm;</text>
      <biological_entity id="o7165" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade round or reniform, (3–) 5–7-lobed (lobes obtuse), (3–) 4.5–6.7 (–10.3) mm, slightly fleshy, margins entire, eciliate, surfaces glabrous.</text>
      <biological_entity id="o7166" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="(3-)5-7-lobed" value_original="(3-)5-7-lobed" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10.3" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s3" to="6.7" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o7167" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o7168" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–3 (–5) -flowered, capitate cymes, sometimes solitary flowers, (flowers subsessile), (3–) 6.7–9 (–19.4) cm, tangled, nonglandular-hairy;</text>
      <biological_entity id="o7169" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-3(-5)-flowered" value_original="2-3(-5)-flowered" />
      </biological_entity>
      <biological_entity id="o7170" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o7171" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="19.4" to_unit="cm" />
        <character char_type="range_value" from="6.7" from_unit="cm" name="some_measurement" src="d0_s4" to="9" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="tangled" value_original="tangled" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="nonglandular-hairy" value_original="nonglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts petiolate.</text>
      <biological_entity id="o7172" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers (hypanthium V-shaped in longisection, glabrous or sparsely short-stipitate-glandular);</text>
      <biological_entity id="o7173" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, oblong to ovate, (0.7–1 mm wide), margins eciliate, surfaces abaxially glabrous;</text>
      <biological_entity id="o7174" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
      <biological_entity id="o7175" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o7176" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white to pale-purple, not spotted, oblong, (1.7–) 3–4.4 (–6.2) mm, ± equaling sepals;</text>
      <biological_entity id="o7177" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pale-purple" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7178" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1/2 inferior.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 26.</text>
      <biological_entity id="o7179" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s9" value="inferior" value_original="inferior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7180" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, snow beds, open gravel and silt, seepage areas, stream and lake margins, shady taluses, ravines or cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="snow beds" />
        <character name="habitat" value="open gravel" />
        <character name="habitat" value="silt" />
        <character name="habitat" value="seepage areas" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="shady taluses" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Mont., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Saxifraga debilis is known only from the central and southern Rocky Mountains, where it is often called S. rivularis (a species not present in the area). Its V-shaped (in longisection), glabrous or sparsely short stipitate-glandular hypanthia, and larger, more-lobed leaves (similar to S. bracteata in this) distinguish it from S. hyperborea, which is sometimes sympatric (M. H. Jørgensen et al. 2006).</discussion>
  
</bio:treatment>