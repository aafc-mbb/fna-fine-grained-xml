<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sarraceniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sarracenia</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">rubra</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">rubra</taxon_name>
    <taxon_hierarchy>family sarraceniaceae;genus sarracenia;species rubra;subspecies rubra</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092292</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Pitchers (6–) 12–30 (–50) cm, gradually tapering from base to orifice, glabrous or short-hairy;</text>
      <biological_entity id="o15068" name="pitcher" name_original="pitchers" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character constraint="from base" constraintid="o15069" is_modifier="false" modifier="gradually" name="shape" src="d0_s0" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity id="o15069" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o15070" name="orifice" name_original="orifice" src="d0_s0" type="structure" />
      <relation from="o15069" id="r1288" name="to" negation="false" src="d0_s0" to="o15070" />
    </statement>
    <statement id="d0_s1">
      <text>orifice 0.5–2.5 cm wide;</text>
      <biological_entity id="o15071" name="orifice" name_original="orifice" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hood narrowly ovate, ± flat, not much undulated, 0.7–4.5 × 0.7–3.9 cm, length-to-width ratio 1–4.3.</text>
      <biological_entity id="o15072" name="hood" name_original="hood" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s2" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="3.9" to_unit="cm" />
        <character char_type="range_value" from="length" modifier="not much; much" name="ratio" src="d0_s2" to="width" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="4.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scapes 12–66 cm, usually 2–3 times taller than tallest pitchers.</text>
      <biological_entity id="o15073" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s3" to="66" to_unit="cm" />
        <character constraint="pitcher" constraintid="o15074" is_modifier="false" name="height_or_size" src="d0_s3" value="2-3 times taller than tallest pitchers" />
      </biological_entity>
      <biological_entity id="o15074" name="pitcher" name_original="pitchers" src="d0_s3" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet pine savannas and flatwoods, pineland seepage slopes, bogs, boggy streamheads, boggy pond margins, springy hillsides near fall line</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet pine savannas" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="pineland seepage slopes" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="boggy streamheads" />
        <character name="habitat" value="boggy pond margins" />
        <character name="habitat" value="springy hillsides" constraint="near fall line" />
        <character name="habitat" value="fall line" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11a.</number>
  <discussion>Subspecies rubra occurs on the coastal plain from southeastern North Carolina (Carteret and Wayne counties) to western Georgia (Muskogee County). In southeastern North Carolina, some plants can be relatively small and form dense colonies.</discussion>
  
</bio:treatment>