<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mitella</taxon_name>
    <taxon_name authority="Graham" date="1829" rank="species">trifida</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>7: 185. 1829  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus mitella;species trifida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065982</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mitella</taxon_name>
    <taxon_name authority="(Piper) Rydberg" date="unknown" rank="species">trifida</taxon_name>
    <taxon_name authority="(Rydberg) Rosendahl" date="unknown" rank="variety">violacea</taxon_name>
    <taxon_hierarchy>genus Mitella;species trifida;variety violacea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ozomelis</taxon_name>
    <taxon_name authority="(Piper) Rydberg" date="unknown" rank="species">anomola</taxon_name>
    <taxon_hierarchy>genus Ozomelis;species anomola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ozomelis</taxon_name>
    <taxon_name authority="(Graham) Rydberg" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_hierarchy>genus Ozomelis;species micrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ozomelis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trifida</taxon_name>
    <taxon_hierarchy>genus Ozomelis;species trifida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not stoloniferous.</text>
      <biological_entity id="o9627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems 12–45 cm.</text>
      <biological_entity id="o9628" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 2.5–10 cm, subglabrous or short-stipitate-glandular and long-stipitate-glandular, longer hairs retrorse, white, tan, or brown;</text>
      <biological_entity id="o9629" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9630" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="longer" id="o9631" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade cordate, cordate-ovate, or reniform, usually shorter than or sometimes ± as long as wide, 1.2–6 × 2–8 cm, margins shallowly 5-lobed or 7-lobed, crenate, uniformly ciliate, apex of terminal lobe rounded to obtuse, surfaces short-stipitate-glandular and sparsely long-stipitate-glandular;</text>
      <biological_entity id="o9632" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9633" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate-ovate" value_original="cordate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate-ovate" value_original="cordate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character constraint="as-long-as margins, apex, surfaces" constraintid="o9634, o9635, o9636" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9634" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="uniformly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9635" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="uniformly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9636" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="uniformly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o9637" name="lobe" name_original="lobe" src="d0_s3" type="structure" />
      <relation from="o9634" id="r786" name="part_of" negation="false" src="d0_s3" to="o9637" />
      <relation from="o9635" id="r787" name="part_of" negation="false" src="d0_s3" to="o9637" />
      <relation from="o9636" id="r788" name="part_of" negation="false" src="d0_s3" to="o9637" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves absent.</text>
      <biological_entity id="o9638" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o9639" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3, closely 4–20-flowered, 1 flower per node, weakly secund or not secund, 12–45 cm, short-stipitate-glandular and, sometimes, sparsely long-stipitate-glandular proximally and distally.</text>
      <biological_entity id="o9640" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="closely" name="architecture" src="d0_s5" value="4-20-flowered" value_original="4-20-flowered" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9641" name="flower" name_original="flower" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s5" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes; sparsely; proximally; distally" name="pubescence" src="d0_s5" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9642" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o9641" id="r789" name="per" negation="false" src="d0_s5" to="o9642" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.3–1.5 mm, short-stipitate-glandular.</text>
      <biological_entity id="o9643" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium campanulate, 1–1.5 × 1.5–2.5 (–3) mm;</text>
      <biological_entity id="o9644" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9645" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals erect or spreading, whitish or purple-tinged, oblong to triangular-ovate, 0.8–1.5 × 1–1.3 mm;</text>
      <biological_entity id="o9646" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9647" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="triangular-ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s8" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, sometimes pink or purple-tinged, 3-lobed or, sometimes, unlobed, 1–3.5 mm, lobes lanceolate, lateral lobes ascending;</text>
      <biological_entity id="o9648" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9649" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
        <character name="shape" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9650" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9651" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 5, opposite sepals;</text>
      <biological_entity id="o9652" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9653" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o9654" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments white, 0.1–0.2 mm;</text>
      <biological_entity id="o9655" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9656" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.1–0.2 × 0.1–0.2 mm;</text>
      <biological_entity id="o9657" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9658" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s12" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary ca. 1/2 inferior;</text>
      <biological_entity id="o9659" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9660" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s13" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles divergent, flattened, 0.1–0.2 mm;</text>
      <biological_entity id="o9661" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o9662" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas unlobed.</text>
      <biological_entity id="o9663" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o9664" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark reddish-brown, 0.7–1 mm, pitted.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o9665" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9666" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep forest, moist wooded and open slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep forest" />
        <character name="habitat" value="moist wooded" />
        <character name="habitat" value="open slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Pacific mitrewort</other_name>
  <discussion>Mitella trifida varies in flower size, petal-blade lobing, and pubescence. Plants with relatively small flowers and petal blades entire or shallowly trifid and often purplish have been named var. violacea. Plants matching this description occur in British Columbia, Montana, and Washington and appear to represent a minor morphological variant that does not warrant recognition.</discussion>
  <discussion>The Gosiute Indians of Utah made an infusion from roots of Mitella trifida to treat colic (D. E. Moerman 1998).</discussion>
  
</bio:treatment>