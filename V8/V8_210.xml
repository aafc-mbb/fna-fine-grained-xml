<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Elizabeth Fortson Wells,Patrick E. Elvander†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="treatment_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Rydberg" date="1905" rank="genus">ELMERA</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al.,  N. Amer. Fl.</publication_title>
      <place_in_publication>22: 97. 1905  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus ELMERA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Adolph Daniel Edward Elmer, 1870–1942, collector and botanist in western North America</other_info_on_name>
    <other_info_on_name type="fna_id">111481</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, not rhizomatous, not stoloniferous;</text>
      <biological_entity id="o1577" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex slender, scaly.</text>
      <biological_entity id="o1578" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems erect, leafy, 10–35 cm, stipitate-glandular.</text>
      <biological_entity id="o1579" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in basal rosette and cauline, cauline leaves 2–3, smaller distally;</text>
      <biological_entity id="o1580" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o1581" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o1582" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o1583" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o1580" id="r141" name="in" negation="false" src="d0_s3" to="o1581" />
    </statement>
    <statement id="d0_s4">
      <text>stipules present;</text>
      <biological_entity id="o1584" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole sparsely to densely stipitate-glandular;</text>
      <biological_entity id="o1585" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade orbiculate to broadly reniform, 5–9-lobed, sinuses relatively shallow, base cordate, ultimate margins crenate, apex obtuse, surfaces stipitate-glandular;</text>
      <biological_entity id="o1586" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s6" to="broadly reniform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="5-9-lobed" value_original="5-9-lobed" />
      </biological_entity>
      <biological_entity id="o1587" name="sinuse" name_original="sinuses" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="relatively" name="depth" src="d0_s6" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o1588" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o1589" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o1590" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation palmate.</text>
      <biological_entity id="o1591" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences spikelike racemes, from axillary buds in rosette, 10–28-flowered, bracteate.</text>
      <biological_entity id="o1592" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="10-28-flowered" value_original="10-28-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o1593" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1594" name="bud" name_original="buds" src="d0_s8" type="structure" />
      <biological_entity id="o1595" name="rosette" name_original="rosette" src="d0_s8" type="structure" />
      <relation from="o1592" id="r142" name="from" negation="false" src="d0_s8" to="o1594" />
      <relation from="o1594" id="r143" name="in" negation="false" src="d0_s8" to="o1595" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium adnate to proximal 1/4 of ovary, free from ovary 2.5–4 mm, green, white, or cream;</text>
      <biological_entity id="o1596" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1597" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character constraint="of ovary" constraintid="o1598" name="quantity" src="d0_s9" value="1/4" value_original="1/4" />
        <character constraint="from ovary" constraintid="o1599" is_modifier="false" name="fusion" notes="" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream" value_original="cream" />
      </biological_entity>
      <biological_entity id="o1598" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o1599" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, greenish yellow;</text>
      <biological_entity id="o1600" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1601" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, white or cream;</text>
      <biological_entity id="o1602" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1603" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary tissue not seen;</text>
      <biological_entity id="o1604" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o1605" name="tissue" name_original="tissue" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5, (opposite sepals);</text>
      <biological_entity id="o1606" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1607" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments subulate;</text>
      <biological_entity id="o1608" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1609" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1/4 inferior, 1-locular, ovaries completely connate;</text>
      <biological_entity id="o1610" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o1611" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <biological_entity id="o1612" name="ovary" name_original="ovaries" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="completely" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>placentation parietal;</text>
      <biological_entity id="o1613" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s16" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 2;</text>
      <biological_entity id="o1614" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o1615" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 2.</text>
      <biological_entity id="o1616" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o1617" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Capsules 2-beaked.</text>
      <biological_entity id="o1618" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="2-beaked" value_original="2-beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds dark-brown, ellipsoid, minutely papillate.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 7.</text>
      <biological_entity id="o1619" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s20" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o1620" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Yellow coralbells</other_name>
  <discussion>Species 1: nw North America.</discussion>
  <discussion>Species 1</discussion>
  
</bio:treatment>