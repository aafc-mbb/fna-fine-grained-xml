<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">543</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subsection">Poa nervosa Complex</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">tracyi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection poa nervosa complex;species tracyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>27</number>
  <other_name type="common_name">Tracy's bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely tufted, shortly rhizomatous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly extravaginal.</text>
      <biological_entity id="o6006" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms (25) 32-125 cm, erect or the bases decumbent, not branching above the base, terete or weakly compressed;</text>
      <biological_entity id="o6007" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="32" from_unit="cm" name="some_measurement" src="d0_s3" to="125" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6008" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character constraint="above base" constraintid="o6009" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o6009" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>nodes terete or slightly compressed, 1-2 (3) exserted.</text>
      <biological_entity id="o6010" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character name="atypical_quantity" src="d0_s4" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for (2/5) 1/2 – 9/10 their length, compressed, distinctly keeled, keels winged, wing to 0.5 mm wide, smooth or sparsely to infrequently densely scabrous, glabrous or infrequently retrorsely pubescent, bases of basal sheaths glabrous, distal sheath lengths 0.7-1.6 times blade lengths;</text>
      <biological_entity id="o6011" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6012" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o6013" name="wing" name_original="wing" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s5" to="sparsely infrequently densely scabrous glabrous or infrequently retrorsely pubescent" />
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s5" to="sparsely infrequently densely scabrous glabrous or infrequently retrorsely pubescent" />
      </biological_entity>
      <biological_entity id="o6014" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6015" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o6016" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o6017" is_modifier="false" name="length" src="d0_s5" value="0.7-1.6 times blade lengths" value_original="0.7-1.6 times blade lengths" />
      </biological_entity>
      <biological_entity id="o6017" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o6014" id="r964" name="part_of" negation="false" src="d0_s5" to="o6015" />
    </statement>
    <statement id="d0_s6">
      <text>collars with vestiture similar to the sheaths;</text>
      <biological_entity id="o6018" name="collar" name_original="collars" src="d0_s6" type="structure" />
      <biological_entity id="o6019" name="vestiture" name_original="vestiture" src="d0_s6" type="structure" />
      <biological_entity id="o6020" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <relation from="o6018" id="r965" name="with" negation="false" src="d0_s6" to="o6019" />
      <relation from="o6019" id="r966" name="to" negation="false" src="d0_s6" to="o6020" />
    </statement>
    <statement id="d0_s7">
      <text>ligules 2-4.5 mm, smooth or scabrous, glabrous or softly puberulent, obtuse to acute;</text>
      <biological_entity id="o6021" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>innovation blades similar to the cauline blades;</text>
      <biological_entity constraint="innovation" id="o6022" name="blade" name_original="blades" src="d0_s8" type="structure" />
      <biological_entity constraint="cauline" id="o6023" name="blade" name_original="blades" src="d0_s8" type="structure" />
      <relation from="o6022" id="r967" name="to" negation="false" src="d0_s8" to="o6023" />
    </statement>
    <statement id="d0_s9">
      <text>cauline blades (1.5) 2-5.5 mm wide, flat, lax, smooth or sparsely scabrous mainly over the veins, apices broadly prow-shaped, flag leaf-blades 6-20 cm.</text>
      <biological_entity constraint="cauline" id="o6024" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character name="width" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="over veins" constraintid="o6025" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6025" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o6026" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o6027" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="distance" src="d0_s9" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles (8) 13-29 cm, erect, usually narrowly pyramidal, open, sparse, with 30-100 spikelets, proximal internodes usually 4+ cm, with (1) 2-4 (5) branches per node;</text>
      <biological_entity id="o6028" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s10" to="29" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s10" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o6029" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s10" to="100" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6030" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6031" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o6032" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o6028" id="r968" name="with" negation="false" src="d0_s10" to="o6029" />
      <relation from="o6030" id="r969" name="with" negation="false" src="d0_s10" to="o6031" />
      <relation from="o6031" id="r970" name="per" negation="false" src="d0_s10" to="o6032" />
    </statement>
    <statement id="d0_s11">
      <text>branches 2.5-18 cm, spreading to eventually reflexed, fairly flexuous, terete to weakly angled, sparsely to moderately scabrous, with 3-34 spikelets.</text>
      <biological_entity id="o6033" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s11" to="18" to_unit="cm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s11" to="eventually reflexed" />
        <character is_modifier="false" modifier="fairly" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="weakly angled" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6034" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="34" />
      </biological_entity>
      <relation from="o6033" id="r971" name="with" negation="false" src="d0_s11" to="o6034" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3-8 mm, lengths 3.5 times widths, laterally compressed, not sexually dimorphic;</text>
      <biological_entity id="o6035" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="3.5" value_original="3.5" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-8;</text>
      <biological_entity id="o6036" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes 1+ mm, smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o6037" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes narrowly lanceolate, distinctly keeled;</text>
      <biological_entity id="o6038" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 1.6-3.5 mm, 1 (3) -veined, 1/2-1/3 as long as the adjacent lemmas;</text>
      <biological_entity constraint="lower" id="o6039" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="1(3)-veined" value_original="1(3)-veined" />
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o6040" from="1/2" name="quantity" src="d0_s16" to="1/3" />
      </biological_entity>
      <biological_entity id="o6040" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s16" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes 2.2-4.9 mm;</text>
      <biological_entity constraint="upper" id="o6041" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s17" to="4.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calluses webbed, hairs over 1/2 the lemma length;</text>
      <biological_entity id="o6042" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o6043" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o6044" name="lemma" name_original="lemma" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o6043" id="r972" name="over" negation="false" src="d0_s18" to="o6044" />
    </statement>
    <statement id="d0_s19">
      <text>lemmas 2.6-5 mm, lanceolate, distinctly keeled, keels and marginal veins long-villous, extending 1/2 - 2/3 the keel length, 1/3-1/2 the marginal vein length, lateral-veins sometimes short-villous, the lateral-veins obscure to moderately prominent, intercostal regions usually sparsely softly puberulent, margins glabrous, apices acute;</text>
      <biological_entity id="o6045" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6046" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6047" name="vein" name_original="veins" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s19" to="1/2" />
      </biological_entity>
      <biological_entity id="o6048" name="keel" name_original="keel" src="d0_s19" type="structure">
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s19" to="2/3" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6049" name="vein" name_original="vein" src="d0_s19" type="structure">
        <character is_modifier="false" name="length" src="d0_s19" value="length" value_original="length" />
      </biological_entity>
      <biological_entity id="o6050" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s19" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o6051" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s19" to="moderately prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o6052" name="region" name_original="regions" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually sparsely softly" name="pubescence" src="d0_s19" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o6053" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6054" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o6046" id="r973" name="extending" negation="false" src="d0_s19" to="o6048" />
      <relation from="o6047" id="r974" name="extending" negation="false" src="d0_s19" to="o6048" />
    </statement>
    <statement id="d0_s20">
      <text>palea keels scabrous, rarely softly puberulent at midlength;</text>
      <biological_entity constraint="palea" id="o6055" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character constraint="at midlength" constraintid="o6056" is_modifier="false" modifier="rarely softly" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o6056" name="midlength" name_original="midlength" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>anthers vestigial (0.1-0.2 mm) or (1.3) 2-3 mm. 2n = 28, 28+1.</text>
      <biological_entity id="o6057" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s21" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s21" value="(1.3)2-3 mm" value_original="(1.3)2-3 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6058" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
        <character char_type="range_value" from="28" name="quantity" src="d0_s21" upper_restricted="false" />
        <character name="quantity" src="d0_s21" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa tracyi grows primarily in coniferous forest openings, sometimes with gambel oak, and in subalpine mesic meadows. It is restricted to the front ranges of the southern Rocky Mountains; it is not common. It differs from P. occidentalis (p. 536) in having longer and/or rudimentary anthers, shorter ligules relative to the leaf blade width, and a loose, shortly rhizomatous habit. Retrorsely pubescent sheaths are common in the more southern plants. It is sequentially gynomonoecious.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>