<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">714</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="(Trin.) Tzvelev" date="unknown" rank="species">sesquiflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species sesquiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">tasuensis</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species purpurascens;subspecies tasuensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">arctica</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species purpurascens;subspecies arctica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species arctica</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">One-and-a-half-flowered reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rarely with sterile culms;</text>
      <biological_entity id="o1942" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o1941" id="r300" name="with" negation="false" src="d0_s0" to="o1942" />
    </statement>
    <statement id="d0_s1">
      <text>strongly cespitose, usually without rhizomes, sometimes with rhizomes 1-2 cm long, 1-2 mm thick.</text>
      <biological_entity id="o1941" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1943" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o1944" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (15) 30-46 (50) cm, unbranched, usually smooth, rarely slightly scabrous beneath the panicles;</text>
      <biological_entity id="o1945" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="46" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="beneath panicles" constraintid="o1946" is_modifier="false" modifier="rarely slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1946" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-2 (3).</text>
      <biological_entity id="o1947" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars smooth;</text>
      <biological_entity id="o1948" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1949" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (0.5) 2-5 (6) mm, usually truncate, sometimes obtuse, usually entire, sometimes lacerate;</text>
      <biological_entity id="o1950" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (3) 8-25 (31) cm long, (2) 3-7 mm wide, flat, abaxial surfaces usually scabrous, rarely smooth, adaxial surfaces smooth or slightly scabrous, glabrous or sparsely hairy.</text>
      <biological_entity id="o1951" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1952" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1953" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-11 (12) cm long, 0.8-2.5 (2.8) cm wide, erect, contracted to somewhat open, usually purple-tinged, sometimes brown or green;</text>
      <biological_entity id="o1954" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="11" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 1.5-3 (4) cm, scabrous, prickles sometimes almost hairlike, usually spikelet-bearing to the base, lowest branches sometimes not so.</text>
      <biological_entity id="o1955" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1956" name="prickle" name_original="prickles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes almost" name="shape" src="d0_s8" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <biological_entity id="o1957" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="lowest" id="o1958" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <relation from="o1956" id="r301" modifier="usually" name="to" negation="false" src="d0_s8" to="o1957" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (5) 5.5-8.5 (9.5) mm;</text>
      <biological_entity id="o1959" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s9" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla prolongations (1) 1.5 (2.2) mm, hairs 1-2.2 mm.</text>
      <biological_entity id="o1960" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="" value_original="" />
      </biological_entity>
      <biological_entity id="o1961" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes keeled, keels usually scabrous for their whole length, sometimes the surfaces also scabrous, lateral-veins prominent, apices long-acuminate, usually twisted distally;</text>
      <biological_entity id="o1962" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o1963" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1964" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1965" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o1966" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs (0.8) 1.2-1.8 (3) mm, 0.1-0.4 times as long as the lemmas, abundant;</text>
      <biological_entity constraint="callus" id="o1967" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="0.8" value_original="0.8" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character constraint="lemma" constraintid="o1968" is_modifier="false" name="length" src="d0_s12" value="0.1-0.4 times as long as the lemmas" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o1968" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas (3.5) 4-4.5 (6) mm, (0.5) 1-2.5 (4.5) mm shorter than the glumes;</text>
      <biological_entity id="o1969" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o1970" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o1970" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>awns (5.4) 7-11 (13) mm, attached to the lower 1/10 – 2/5 of the lemmas, exserted more than 2 mm, stout, easily distinguished from the callus hairs, bent;</text>
      <biological_entity id="o1971" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="5.4" value_original="5.4" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s14" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o1972" from="1/10" name="quantity" src="d0_s14" to="2/5" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" upper_restricted="false" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character constraint="from callus hairs" constraintid="o1973" is_modifier="false" modifier="easily" name="prominence" src="d0_s14" value="distinguished" value_original="distinguished" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o1972" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="callus" id="o1973" name="hair" name_original="hairs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers (1.2) 2.2-3 (3.4) mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o1974" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1975" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis sesquiflora grows at 0-1000 m in open heath, meadows, and forest openings, on or at the base of open rocky cliffs and knolls, as well as in moist talus. It grows in strictly maritime habitats along the west coast of North America, from the Aleutian Islands in Alaska to the Queen Charlotte Islands and south to Vancouver Island (Brooks Peninsula) in British Columbia. There is also a single collection from the coast of mainland British Columbia. In northeast Asia, it ranges into the Kamchatka Peninsula and Kuril Archipelago.</discussion>
  <discussion>Some specimens from the northwestern United States are incorrectly identified, partly because an earlier name for Trisetum spicatum (L.) K. Richt. was Trisetum sesquiflorum Trin.</discussion>
  <discussion>Calamagrostis sesquiflora has sometimes included C. tacomensis (see next) [as C. vaseyi Beal]. Several specimens that were previously identified as C. sesquiflora are actually C. tacomensis. Calamagrostis sesquiflora differs in preferring moister habitats, having wider leaves, callus hairs that are shorter relative to the lemmas, shorter panicle branches, and glumes that are often twisted at the apices.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;Wash.;Oreg.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>