<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Neil Snow;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">776</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L.H. Dewey" date="unknown" rank="genus">LIMNODEA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus limnodea</taxon_hierarchy>
  </taxon_identification>
  <number>14.63</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o28091" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 60 cm, often prostrate and branching at the base, glabrous;</text>
      <biological_entity id="o28092" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="at base" constraintid="o28093" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28093" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes solid.</text>
      <biological_entity id="o28094" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths open or closed, rounded on the back, frequently hispid;</text>
      <biological_entity id="o28095" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="condition" src="d0_s4" value="closed" value_original="closed" />
        <character constraint="on back" constraintid="o28096" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="frequently" name="pubescence" notes="" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o28096" name="back" name_original="back" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>auricles absent;</text>
      <biological_entity id="o28097" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous, usually lacerate and minutely ciliate;</text>
      <biological_entity id="o28098" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat, mostly ascending, abaxial and/or adaxial surfaces glabrous or hispid.</text>
      <biological_entity id="o28099" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28100" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o28101" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences panicles, loosely contracted;</text>
      <biological_entity constraint="inflorescences" id="o28102" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches spikelet-bearing to the base or nearly so, some branches longer than 1 cm;</text>
      <biological_entity id="o28104" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o28105" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o28106" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o28107" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28103" id="r4414" name="to" negation="false" src="d0_s9" to="o28104" />
      <relation from="o28103" id="r4415" name="to" negation="false" src="d0_s9" to="o28105" />
      <relation from="o28105" id="r4416" name="to" negation="false" src="d0_s9" to="o28106" />
      <relation from="o28105" id="r4417" name="to" negation="false" src="d0_s9" to="o28107" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation below the glumes.</text>
      <biological_entity id="o28103" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o28108" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o28103" id="r4418" name="below" negation="false" src="d0_s10" to="o28108" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets pedicellate, subterete, with 1 floret;</text>
      <biological_entity id="o28109" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
      </biological_entity>
      <biological_entity id="o28110" name="floret" name_original="floret" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28109" id="r4419" name="with" negation="false" src="d0_s11" to="o28110" />
    </statement>
    <statement id="d0_s12">
      <text>rachillas prolonged beyond the base of the floret as a slender bristle, glabrous.</text>
      <biological_entity id="o28111" name="rachilla" name_original="rachillas" src="d0_s12" type="structure">
        <character constraint="beyond base" constraintid="o28112" is_modifier="false" name="length" src="d0_s12" value="prolonged" value_original="prolonged" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28112" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o28113" name="floret" name_original="floret" src="d0_s12" type="structure" />
      <biological_entity constraint="slender" id="o28114" name="bristle" name_original="bristle" src="d0_s12" type="structure" />
      <relation from="o28112" id="r4420" name="part_of" negation="false" src="d0_s12" to="o28113" />
      <relation from="o28112" id="r4421" name="as" negation="false" src="d0_s12" to="o28114" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes equal, rigid, coriaceous, hispid or scabrous throughout, rounded over the midvein, veinless or obscurely 3-5-veined, acute, unawned;</text>
      <biological_entity id="o28115" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="false" name="texture" src="d0_s13" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="texture" src="d0_s13" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character constraint="over midvein" constraintid="o28116" is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="veinless" value_original="veinless" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28116" name="midvein" name_original="midvein" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>calluses blunt, glabrous;</text>
      <biological_entity id="o28117" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas equaling the glumes, chartaceous, smooth, veinless or inconspicuously 3-veined, minutely bifid or acute, awned, awns subterminal, exceeding the florets, geniculate near midlength, basal segment twisted;</text>
      <biological_entity id="o28118" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" notes="" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="veinless" value_original="veinless" />
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s15" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o28119" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o28120" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subterminal" value_original="subterminal" />
        <character constraint="near basal segment" constraintid="o28122" is_modifier="false" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o28121" name="floret" name_original="florets" src="d0_s15" type="structure" />
      <biological_entity constraint="basal" id="o28122" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character is_modifier="true" name="position" src="d0_s15" value="midlength" value_original="midlength" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="twisted" value_original="twisted" />
      </biological_entity>
      <relation from="o28120" id="r4422" name="exceeding the" negation="false" src="d0_s15" to="o28121" />
    </statement>
    <statement id="d0_s16">
      <text>paleas shorter than the lemmas, veinless or the base 2-veined, hyaline;</text>
      <biological_entity id="o28123" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character constraint="than the lemmas" constraintid="o28124" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="veinless" value_original="veinless" />
      </biological_entity>
      <biological_entity id="o28124" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o28125" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lodicules 2, glabrous, toothed or not;</text>
      <biological_entity id="o28126" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s17" value="toothed" value_original="toothed" />
        <character name="shape" src="d0_s17" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3;</text>
      <biological_entity id="o28127" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovaries glabrous.</text>
      <biological_entity id="o28128" name="ovary" name_original="ovaries" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses about 2.5 mm, shorter than the lemmas, concealed at maturity, linear, x = 7.</text>
      <biological_entity id="o28129" name="caryopse" name_original="caryopses" src="d0_s20" type="structure">
        <character name="some_measurement" src="d0_s20" unit="mm" value="2.5" value_original="2.5" />
        <character constraint="than the lemmas" constraintid="o28130" is_modifier="false" name="height_or_length_or_size" src="d0_s20" value="shorter" value_original="shorter" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s20" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s20" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o28130" name="lemma" name_original="lemmas" src="d0_s20" type="structure" />
      <biological_entity constraint="x" id="o28131" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Limnodea is a monotypic genus of the southern United States and adjacent Mexico.</discussion>
  <references>
    <reference>Brandenburg, D.M. and J.W. Thieret. 2000. Cinna and Limnodea (Poaceae): Not congeneric. Sida 19:195-200.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;Tex.;La.;Ala.;Ark.;Miss.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>