<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">580</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Nannf. ex Tzvelev" date="unknown" rank="section">Abbreviatae</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">lettermanii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section abbreviatae;species lettermanii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mor.tevansi</taxon_name>
    <taxon_hierarchy>genus poa;species mor.tevansi</taxon_hierarchy>
  </taxon_identification>
  <number>59</number>
  <other_name type="common_name">Letterman's bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not glaucous;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely tufted, not stoloniferous, not rhizomatous.</text>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching all or mainly intravaginal.</text>
      <biological_entity id="o20662" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s3" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 1-12 cm, slender.</text>
      <biological_entity id="o20663" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/6 – 1/4 their length, terete;</text>
      <biological_entity id="o20664" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 1-3 mm, milky white to hyaline, smooth;</text>
      <biological_entity id="o20665" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="milky white" name="coloration" src="d0_s6" to="hyaline" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 0.5-2 mm wide, flat or folded, or slightly inrolled, thin, without papillae (at 100x), apices narrowly prow-shaped.</text>
      <biological_entity id="o20666" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o20667" name="papilla" name_original="papillae" src="d0_s7" type="structure" />
      <biological_entity id="o20668" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <relation from="o20666" id="r3268" name="without" negation="false" src="d0_s7" to="o20667" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 1-3 cm, erect, contracted, usually exserted from the sheaths;</text>
      <biological_entity id="o20669" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o20670" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o20669" id="r3269" modifier="usually" name="exserted from the" negation="false" src="d0_s8" to="o20670" />
    </statement>
    <statement id="d0_s9">
      <text>branches to 1.5 cm, erect to steeply ascending, slender, sulcate or angled, smooth or the angles sparsely scabrous;</text>
      <biological_entity id="o20671" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="steeply ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20672" name="angle" name_original="angles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels shorter than the spikelets.</text>
      <biological_entity id="o20673" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than the spikelets" constraintid="o20674" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20674" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 3-4 mm, laterally compressed, green or anthocyanic;</text>
      <biological_entity id="o20675" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="anthocyanic" value_original="anthocyanic" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets 2-3;</text>
      <biological_entity id="o20676" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachilla internodes shorter than 1 mm, smooth.</text>
      <biological_entity constraint="rachilla" id="o20677" name="internode" name_original="internodes" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Glumes usually equaling or exceeding the lowest lemmas, sometimes also equaling or exceeding the upper florets, lanceolate to broadly lanceolate, distinctly keeled, keels smooth;</text>
      <biological_entity id="o20678" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s14" value="exceeding the lowest lemmas" value_original="exceeding the lowest lemmas" />
        <character is_modifier="false" modifier="sometimes" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s14" value="exceeding the upper florets" value_original="exceeding the upper florets" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s14" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o20679" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o20680" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses glabrous;</text>
      <biological_entity id="o20681" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 2.5-3 mm, lanceolate, distinctly keeled, thin, usually glabrous, keels and marginal veins rarely sparsely puberulent proximally, apices acute;</text>
      <biological_entity id="o20682" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20683" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="rarely sparsely; proximally" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o20684" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="rarely sparsely; proximally" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20685" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>palea keels scabrous;</text>
      <biological_entity constraint="palea" id="o20686" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 0.2-0.8 mm. 2n = 14.</text>
      <biological_entity id="o20687" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20688" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa lettermanii grows on rocky slopes of the highest peaks and ridges in the alpine zone, from northern British Columbia to western Alberta and south to California and Colorado, usually in the shelter of rocks or on mesic to wet, frost-scarred slopes. It is one of only three known diploid Poa species native to the Western Hemisphere. Its glabrous calluses and lemmas usually distinguish it from P. abbreviata (p. 582); it also differs in having flat or folded leaf blades, and shorter spikelets with glumes that are longer than the adjacent florets. Poa montevansii E.H. Kelso is tentatively included here, although its slightly longer lemmas that slightly exceed the glumes suggest that it may represent rare, glabrous forms of P. abbreviata.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Wash.;Utah;Alta.;B.C.;Oreg.;Mont.;Wyo.;Idaho;Calif.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>