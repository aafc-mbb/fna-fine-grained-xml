<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul P.H. But;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">103</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">PLEUROPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus pleuropogon</taxon_hierarchy>
  </taxon_identification>
  <number>9.04</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or rhizomatous.</text>
      <biological_entity id="o18039" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-160 cm, erect or geniculate at the base, glabrous;</text>
      <biological_entity id="o18041" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal branching extravaginal.</text>
      <biological_entity id="o18040" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="160" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o18041" is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s3" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed almost to the top;</text>
      <biological_entity id="o18042" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="to top" constraintid="o18043" is_modifier="false" name="condition" src="d0_s4" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity id="o18043" name="top" name_original="top" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous;</text>
      <biological_entity id="o18044" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat to folded, adaxial surfaces with prominent midribs.</text>
      <biological_entity id="o18045" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="folded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18046" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o18047" name="midrib" name_original="midribs" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o18046" id="r2867" name="with" negation="false" src="d0_s6" to="o18047" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, racemes, rarely panicles.</text>
      <biological_entity id="o18048" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o18049" name="raceme" name_original="racemes" src="d0_s7" type="structure" />
      <biological_entity id="o18050" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets laterally compressed, with 5-20 (30) florets, upper florets reduced;</text>
      <biological_entity id="o18051" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o18052" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="30" value_original="30" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
      <relation from="o18051" id="r2868" name="with" negation="false" src="d0_s8" to="o18052" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes and beneath the florets.</text>
      <biological_entity constraint="upper" id="o18053" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o18054" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o18053" id="r2869" name="above the glumes and beneath" negation="false" src="d0_s9" to="o18054" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes unequal to subequal, shorter than the adjacent lemmas, membranous to subhyaline, margins scarious;</text>
      <biological_entity id="o18055" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="unequal" name="size" src="d0_s10" to="subequal" />
        <character constraint="than the adjacent lemmas" constraintid="o18056" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s10" value="subhyaline" value_original="subhyaline" />
      </biological_entity>
      <biological_entity id="o18056" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o18057" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 1-veined;</text>
      <biological_entity constraint="lower" id="o18058" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 1-3-veined;</text>
      <biological_entity constraint="upper" id="o18059" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachilla internodes in some species swollen and glandular basally, the glandular portion turning whitish when dry;</text>
      <biological_entity constraint="rachilla" id="o18060" name="internode" name_original="internodes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_function_or_pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18061" name="species" name_original="species" src="d0_s13" type="taxon_name">
        <character is_modifier="false" name="shape" src="d0_s13" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o18062" name="portion" name_original="portion" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o18060" id="r2870" name="in" negation="false" src="d0_s13" to="o18061" />
    </statement>
    <statement id="d0_s14">
      <text>calluses rounded, glabrous;</text>
      <biological_entity id="o18063" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas thick, herbaceous to membranous, 7 (9) -veined, veins parallel, margins scarious, apices scarious, entire or emarginate, midvein sometimes extended into an awn, awns straight;</text>
      <biological_entity id="o18064" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thick" value_original="thick" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s15" to="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7(9)-veined" value_original="7(9)-veined" />
      </biological_entity>
      <biological_entity id="o18065" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o18066" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o18067" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o18068" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character constraint="into awn" constraintid="o18069" is_modifier="false" modifier="sometimes" name="size" src="d0_s15" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o18069" name="awn" name_original="awn" src="d0_s15" type="structure" />
      <biological_entity id="o18070" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas subequal to the lemmas, 2-veined, keeled over each vein, keels winged, with 1 or 2 awns or a flat triangular appendage;</text>
      <biological_entity id="o18071" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character constraint="to lemmas" constraintid="o18072" is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s16" value="2-veined" value_original="2-veined" />
        <character constraint="over vein" constraintid="o18073" is_modifier="false" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o18072" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o18073" name="vein" name_original="vein" src="d0_s16" type="structure" />
      <biological_entity id="o18074" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o18075" name="appendage" name_original="appendage" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character is_modifier="true" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o18074" id="r2871" name="with" negation="false" src="d0_s16" to="o18075" />
    </statement>
    <statement id="d0_s17">
      <text>lodicules 2, completely fused;</text>
      <biological_entity id="o18076" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" modifier="completely" name="fusion" src="d0_s17" value="fused" value_original="fused" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, opening by pores;</text>
      <biological_entity id="o18077" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o18078" name="pore" name_original="pores" src="d0_s18" type="structure" />
      <relation from="o18077" id="r2872" name="opening by" negation="false" src="d0_s18" to="o18078" />
    </statement>
    <statement id="d0_s19">
      <text>ovaries glabrous, x = 8, 9, 10.</text>
      <biological_entity id="o18079" name="ovary" name_original="ovaries" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o18080" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
        <character name="quantity" src="d0_s19" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pleuropogon is a genus of five hydrophilous species, one circumboreal in the arctic, the other four restricted to the Pacific coast of North America, extending from southern British Columbia to central California. The Pacific coast species are sometimes treated as a separate genus, Lophochlaena Nees, but are here regarded as constituting a subgenus of Pleuropogon.</discussion>
  <discussion>The flat, triangular paleal appendages differ from bristly or flattened awns in being wider at the base, and smooth rather than scabrous.</discussion>
  <references>
    <reference>But, P.P.H. 1977. Systematics of Pleuropogon R. Br. (Poaceae). Ph.D. dissertation, University of California, Berkeley, California, U.S.A. 229 pp.</reference>
    <reference>But, P.P.H., J. Kagan, V. Crosby, and J.S. Shelly. 1985. Rediscovery and reproductive biology of Pleuropogon oregonus (Poaceae). Madrono 32:189-190.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.;Greenland;Nfld. and Labr. (Nfld.);Nunavut;Ont.;Que.;Alaska;Calif.;Wash.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Paleal keels each with 2 awns, the lower awn 1-3 mm long, the upper awn 0.3-1 mm long: lemmas 3.5-5 mm long; plants of the arctic (subg. Pleuropogon)</description>
      <determination>5 Pleuropogon sabinei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Paleal keels each with 1 awn 3-9 mm long, or a triangular appendage; lemmas 4.5-10 mm long; plants of the Pacific Northwest and California (subg. Lophochlaena).</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lowest lemma in each spikelet 4.5-7.5 mm long; culms 15-95 cm tall; caryopses 2.5-3.1 mm long.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Paleal keels unawned, with a triangular appendage;  rhizomes absent or poorly developed; rachilla internodes with a glandular swelling at the base</description>
      <determination>1 Pleuropogon californicus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Paleal keels with an awn 3-9 mm long, without a triangular appendage; rhizomes strongly developed; rachilla internodes without a glandular swelling at the base</description>
      <determination>3 Pleuropogon oregonus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lowest lemma in each spikelet 8-10 mm ong; culms mostly 100-160 cm tall; caryopses 3.5-6 mm long.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemma awns 0.2-4 mm long; pedicels usually erect, rarely reflexed, the spikelets erect or ascending at maturity</description>
      <determination>2 Pleuropogon hooverianus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemma awns (5)9-20 mm long; pedicels reflexed, the spikelets pendent at maturity</description>
      <determination>4 Pleuropogon refractus</determination>
    </key_statement>
  </key>
</bio:treatment>