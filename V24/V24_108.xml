<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">MELICA</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">spectabilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus melica;species spectabilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromelica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spectabilis</taxon_name>
    <taxon_hierarchy>genus bromelica;species spectabilis</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Purple oniongrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, rhizomatous.</text>
      <biological_entity id="o19782" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 45-100 cm, form¬ing corms, corms connected to the rhizomes by a rootlike, 10-30 mm structure, which usually remains attached to the corm;</text>
      <biological_entity id="o19783" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19784" name="corm" name_original="corms" src="d0_s1" type="structure" />
      <biological_entity id="o19785" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character constraint="to rhizomes" constraintid="o19786" is_modifier="false" name="fusion" src="d0_s1" value="connected" value_original="connected" />
        <character char_type="range_value" from="10" from_unit="mm" modifier="by a rootlike" name="some_measurement" notes="" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19786" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o19787" name="structure" name_original="structure" src="d0_s1" type="structure">
        <character constraint="to corm" constraintid="o19788" is_modifier="false" modifier="usually" name="fixation" src="d0_s1" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o19788" name="corm" name_original="corm" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>internodes smooth.</text>
      <biological_entity id="o19789" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually glabrous, often pilose at the throat and collar;</text>
      <biological_entity id="o19790" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="at collar" constraintid="o19792" is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o19791" name="throat" name_original="throat" src="d0_s3" type="structure" />
      <biological_entity id="o19792" name="collar" name_original="collar" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-2 mm;</text>
      <biological_entity id="o19793" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-5 mm wide, abaxial surfaces scabridulous over the veins, adaxial surfaces usually glabrous.</text>
      <biological_entity id="o19794" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19795" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character constraint="over veins" constraintid="o19796" is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o19796" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o19797" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 5-26 cm;</text>
      <biological_entity id="o19798" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="26" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches 2-5 cm, usually appressed, sometimes divergent and flexuous, with 2-3 spikelets;</text>
      <biological_entity id="o19799" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="course" src="d0_s7" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o19800" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o19799" id="r3127" name="with" negation="false" src="d0_s7" to="o19800" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels not sharply bent;</text>
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o19801" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not sharply" name="shape" src="d0_s8" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o19802" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o19801" id="r3128" name="above" negation="false" src="d0_s9" to="o19802" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 7-19 mm, with 3-7 bisexual florets, base of the distal florets concealed at anthesis;</text>
      <biological_entity id="o19803" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19804" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="7" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o19805" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="prominence" src="d0_s10" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19806" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o19803" id="r3129" name="with" negation="false" src="d0_s10" to="o19804" />
      <relation from="o19805" id="r3130" name="part_of" negation="false" src="d0_s10" to="o19806" />
    </statement>
    <statement id="d0_s11">
      <text>rachilla internodes 1-2 mm, not swollen when fresh, not wrinkled when dry.</text>
      <biological_entity constraint="rachilla" id="o19807" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="when fresh" name="shape" src="d0_s11" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="when dry" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes usually less than 1/2 the length of the spikelets;</text>
      <biological_entity id="o19808" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="1/2" />
      </biological_entity>
      <biological_entity id="o19809" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 3.5-6.4 mm long, 1.5-3 mm wide, 1-3-veined;</text>
      <biological_entity constraint="lower" id="o19810" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s13" to="6.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 5-7 mm long, 2.3-3.5 mm wide, 5-7-veined;</text>
      <biological_entity constraint="upper" id="o19811" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 6-9 mm, glabrous, scabridulous, 5-11-veined, veins inconspicuous, apices rounded to acute, unawned;</text>
      <biological_entity id="o19812" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-11-veined" value_original="5-11-veined" />
      </biological_entity>
      <biological_entity id="o19813" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o19814" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s15" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas about 73 the length of the lemmas;</text>
      <biological_entity id="o19815" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="73" value_original="73" />
      </biological_entity>
      <biological_entity id="o19816" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.5-3 mm;</text>
      <biological_entity id="o19817" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>rudiments 1.5-3.5 mm, acute, distinct from the bisexual florets, sometimes surrounded by a small sterile floret similar in shape to the bisexual florets.</text>
      <biological_entity id="o19819" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s18" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o19820" name="floret" name_original="floret" src="d0_s18" type="structure">
        <character is_modifier="true" name="size" src="d0_s18" value="small" value_original="small" />
        <character is_modifier="true" name="reproduction" src="d0_s18" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o19821" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s18" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o19818" id="r3131" modifier="sometimes" name="surrounded by a" negation="false" src="d0_s18" to="o19820" />
      <relation from="o19818" id="r3132" name="to" negation="false" src="d0_s18" to="o19821" />
    </statement>
    <statement id="d0_s19">
      <text>2n= 18.</text>
      <biological_entity id="o19818" name="rudiment" name_original="rudiments" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
        <character constraint="from florets" constraintid="o19819" is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19822" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melica spectabilis grows in moist meadows, flats, and open woods, from 1200-2600 m, primarily in the Pacific Northwest and the Rocky Mountains. It is often confused with M. bulbosa, differing in its shorter glumes, "tailed" corm, and the more marked and evenly spaced purplish bands of its spikelets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Wash.;Utah;Alta.;B.C.;Idaho;Mont.;Wyo.;Calif.;Nev.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>