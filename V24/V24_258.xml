<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">188</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Harz" date="unknown" rank="tribe">BRACHYPODIEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">BRACHYPODIUM</taxon_name>
    <taxon_name authority="(L.) P. Beauv" date="unknown" rank="species">distachyon</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe brachypodieae;genus brachypodium;species distachyon</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Purple falsebrome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely tufted;</text>
    </statement>
    <statement id="d0_s2">
      <text>bright green or glaucous.</text>
      <biological_entity id="o30853" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 5-35 (45-50) cm, geniculate or stiffly erect, internodes glabrous;</text>
      <biological_entity id="o30854" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="50" to_unit="cm" />
        <character name="some_measurement" src="d0_s3" unit="cm" value="" value_original="" />
        <character is_modifier="false" name="shape" src="d0_s3" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o30855" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes conspicuously pubescent.</text>
      <biological_entity id="o30856" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves cauline;</text>
      <biological_entity id="o30857" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sheaths usually glabrous;</text>
      <biological_entity id="o30858" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.5-2 mm, pubescent;</text>
      <biological_entity id="o30859" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 10-40 cm long, 2-5 mm wide, flat, glaucous, sparsely hairy, hairs 0.5-1 mm, veins unequally prominent, margins thickened, sparsely hairy, hairs 0.8-1.3 mm.</text>
      <biological_entity id="o30860" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30861" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30862" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="unequally" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o30863" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30864" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes 2-7 cm, with 1-7 usually overlapping, appressed spikelets, the basal 1-2 spikelets sometimes diverging at maturity;</text>
      <biological_entity id="o30865" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30866" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="7" />
        <character is_modifier="true" modifier="usually" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o30867" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o30868" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
      </biological_entity>
      <relation from="o30865" id="r4852" name="with" negation="false" src="d0_s9" to="o30866" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.5-1 mm.</text>
      <biological_entity id="o30869" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 15-40 mm, laterally compressed, with 7-15 (17) florets.</text>
      <biological_entity id="o30870" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o30871" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="17" value_original="17" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
      <relation from="o30870" id="r4853" name="with" negation="false" src="d0_s11" to="o30871" />
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 5-6 mm, 5-7-veined;</text>
      <biological_entity constraint="lower" id="o30872" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 7-8 mm, 7-9-veined, veins prominent, apices acute;</text>
      <biological_entity constraint="upper" id="o30873" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity id="o30874" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o30875" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 7-10 mm, lanceolate, 7-veined, awned, awns 8-17 mm, usually straight, sometimes curved;</text>
      <biological_entity id="o30876" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-veined" value_original="7-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o30877" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas (6.5) 7-9 mm, with 2-numerous veins, 2-keeled, keels stiffly ciliate;</text>
      <biological_entity id="o30878" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="6.5" value_original="6.5" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o30879" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="numerous" />
      </biological_entity>
      <biological_entity id="o30880" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="stiffly" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o30878" id="r4854" name="with" negation="false" src="d0_s15" to="o30879" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules oblong, acute, sparsely hairy, ciliate;</text>
      <biological_entity id="o30881" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s16" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.5-1.1 mm.</text>
      <biological_entity id="o30882" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 5.7-7.8 mm, ellipsoid, apices hairy.</text>
      <biological_entity id="o30883" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" src="d0_s18" to="7.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>2n = 20, 28, 30.</text>
      <biological_entity id="o30884" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30885" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
        <character name="quantity" src="d0_s19" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachypodium distachyon is native to dry, open habitats in southern Europe. It is now established in California and is known from scattered locations elsewhere in the Flora region. It is also established in Australia, where it grows in dry, disturbed areas on sandy or rocky soils.</discussion>
  <discussion>Brachypodium distachyon is sometimes treated as the only member of Trachynia Link. It differs from other species of Brachypodium in being a cleistogamous annual with shorter pedicels and anthers, laterally compressed spikelets, and fewer spikelets per raceme. Molecular data (Catalan and Olmstead 2000) show it as the basal lineage within Brachypodium. It has been proposed as a model species for molecular work in the Poaceae.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;Colo.;Tex.;Calif.;Pacific Islands (Hawaii);Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>