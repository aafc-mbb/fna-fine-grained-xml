<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">740</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">HOLCUS</taxon_name>
    <taxon_name authority="L" date="unknown" rank="species">mollis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus holcus;species mollis</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Creeping velvetgrass</other_name>
  <other_name type="common_name">Houlque molle</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not cespitose, rhizomatous, rhizomes to 40 cm.</text>
      <biological_entity id="o9758" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9759" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-100 (150) cm, usually decumbent at the base;</text>
      <biological_entity id="o9760" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="150" value_original="150" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character constraint="at base" constraintid="o9761" is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o9761" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>lower internodes glabrous or sparsely pubescent.</text>
      <biological_entity constraint="lower" id="o9762" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or hairy;</text>
      <biological_entity id="o9763" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-5 mm, obtuse, erose;</text>
      <biological_entity id="o9764" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-20 cm long, 3-10 mm wide, pubescent.</text>
      <biological_entity id="o9765" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-20 (22) cm long, to 3 cm wide;</text>
      <biological_entity id="o9766" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="22" value_original="22" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches puberulent or ciliate;</text>
      <biological_entity id="o9767" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels to 5 mm long, pilose, hairs to 0.3 mm.</text>
      <biological_entity id="o9768" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9769" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-6 (7) mm;</text>
      <biological_entity id="o9770" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachillas hairy.</text>
      <biological_entity id="o9771" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes exceeding and enclosing the florets, subequal, nearly the same width, ovate, membranous, whitish green when young, straw-colored with age, veins ciliate, often purple, intercostal regions scabrous or glabrous, apices acuminate or acute, unawned;</text>
      <biological_entity id="o9772" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s12" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="nearly" name="character" src="d0_s12" value="width" value_original="width" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s12" value="whitish green" value_original="whitish green" />
        <character constraint="with age" constraintid="o9774" is_modifier="false" name="coloration" src="d0_s12" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
      <biological_entity id="o9773" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity id="o9774" name="age" name_original="age" src="d0_s12" type="structure" />
      <biological_entity id="o9775" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="often" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o9776" name="region" name_original="regions" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9777" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o9772" id="r1572" name="enclosing the" negation="false" src="d0_s12" to="o9773" />
    </statement>
    <statement id="d0_s13">
      <text>calluses densely to sparsely hairy;</text>
      <biological_entity id="o9778" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2-2.5 mm, glabrous, acute;</text>
      <biological_entity id="o9779" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper lemmas bifid, awned above midlength, awns 3-5 mm, scabrous, straight or geniculate at maturity;</text>
      <biological_entity constraint="upper" id="o9780" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="bifid" value_original="bifid" />
        <character constraint="above midlength" constraintid="o9781" is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o9781" name="midlength" name_original="midlength" src="d0_s15" type="structure" />
      <biological_entity id="o9782" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character constraint="at maturity" is_modifier="false" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers about 2 mm. 2n = 28 (35, 42, 49).</text>
      <biological_entity id="o9783" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9784" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
        <character name="quantity" src="d0_s16" value="[35" value_original="[35" />
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
        <character name="quantity" src="d0_s16" value="49]" value_original="49]" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Holcus mollis grows in moist soil and disturbed sites, including lawns and damp pastures. It is a European introduction that has persisted in the Flora region, becoming a problematic weed in ungrazed pastures, prairie remnants, and oak savannahs in portions of the Pacific Northwest. It is also sold as an ornamental. There are two subspecies: Holcus mollis L. subsp. mollis (stems not thickened and tuberous at the base; panicles lax, brownish or purplish) and H. mollis subsp. reuteri (Boiss.) Malag. (stems thickened and tuberous at the base; panicles narrow, whitish). North American introductions belong to subsp. mollis.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.H.;N.J.;Wash.;Del.;N.C.;Calif.;Oreg.;N.Y.;Pa.;Vt.;Idaho;B.C.;Nfld. and Labr. (Labr.)</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>