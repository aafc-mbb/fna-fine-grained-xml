<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">412</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely or densely cespitose, with short rhizomes or without rhizomes.</text>
      <biological_entity id="o7791" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7792" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o7791" id="r1255" name="with short rhizomes or without" negation="false" src="d0_s0" to="o7792" />
    </statement>
    <statement id="d0_s1">
      <text>Innovations mostly intravaginal.</text>
      <biological_entity id="o7793" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s1" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Blades more or less stiff, setaceous if lax, usually conduplicate, sometimes convolute or flat;</text>
      <biological_entity id="o7794" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape" src="d0_s2" value="setaceous" value_original="setaceous" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_vernation" src="d0_s2" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs usually distinct;</text>
    </statement>
    <statement id="d0_s4">
      <text>sclerenchyma usually only developed on the adaxial surface, sometimes forming pillars or girders at the major veins.</text>
      <biological_entity id="o7795" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7796" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <relation from="o7795" id="r1256" modifier="usually only" name="developed on the" negation="false" src="d0_s4" to="o7796" />
    </statement>
    <statement id="d0_s5">
      <text>Calluses wider than long, scabrous on the margins;</text>
      <biological_entity id="o7797" name="callus" name_original="calluses" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="wider than long" value_original="wider than long" />
        <character constraint="on margins" constraintid="o7798" is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7798" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>lemmas chartaceous, apices usually entire, rarely minutely bidentate, usually awned, sometimes unawned;</text>
      <biological_entity id="o7799" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o7800" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely minutely" name="shape" src="d0_s6" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary apices usually pubescent, sometimes sparsely pubescent, rarely glabrous.</text>
      <biological_entity constraint="ovary" id="o7801" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca sect. Festuca is most abundant in the Northern Hemisphere. Its species are native to all continents except Antarctica. There are perhaps 400 or more species in this section, with new ones constantly described.</discussion>
  
</bio:treatment>