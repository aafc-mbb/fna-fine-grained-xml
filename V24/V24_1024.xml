<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">724</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">pickeringii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species pickeringii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pickeringii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">debilis</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species pickeringii;variety debilis</taxon_hierarchy>
  </taxon_identification>
  <number>21</number>
  <other_name type="common_name">Pickering's reed bentgrass</other_name>
  <other_name type="common_name">Calamagrostide de pickering</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants without sterile culms;</text>
      <biological_entity id="o28409" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o28408" id="r4469" name="without" negation="false" src="d0_s0" to="o28409" />
    </statement>
    <statement id="d0_s1">
      <text>weakly cespitose, with rhizomes 2-8 cm long, about 1.5 mm thick.</text>
      <biological_entity id="o28408" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="weakly" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="1.5" value_original="1.5" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o28410" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (17) 25-55 (90) cm, solitary or in small clusters, unbranched, sparsely scabrous;</text>
      <biological_entity id="o28411" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="17" value_original="17" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="55" to_unit="cm" />
        <character constraint="in small clusters" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="in small clusters" value_original="in small clusters" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-3.</text>
      <biological_entity id="o28412" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars smooth;</text>
      <biological_entity id="o28413" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o28414" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2-3 (5) mm, truncate to obtuse, entire, sometimes weakly lacerate;</text>
      <biological_entity id="o28415" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (3) 6-17 (38) cm long, (2) 3-4 (7) mm wide, flat, stiff, abaxial surfaces smooth or scabrous, adaxial surfaces slightly scabrous, glabrous or sparsely hairy.</text>
      <biological_entity id="o28416" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s6" to="17" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28417" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28418" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (3.5) 5-12 (15) cm long, (0.5) 1-2 (3.5) cm wide, contracted, erect, greenish to purplish;</text>
      <biological_entity id="o28419" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="12" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s7" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches (1.5) 2-3 (4) cm, sparsely scabrous, usually spikelet-bearing on the distal 1/2 - 2/3, sometimes to the base.</text>
      <biological_entity id="o28420" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28421" name="1/2-2/3" name_original="1/2-2/3" src="d0_s8" type="structure" />
      <biological_entity id="o28422" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o28420" id="r4470" modifier="usually" name="on" negation="false" src="d0_s8" to="o28421" />
      <relation from="o28420" id="r4471" modifier="sometimes" name="to" negation="false" src="d0_s8" to="o28422" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (2.5) 3-4 (4.5) mm;</text>
      <biological_entity id="o28423" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla prolongations 0.2-1.3 (1.5) mm, sparsely bearded, hairs about 1.5 mm.</text>
      <biological_entity id="o28424" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o28425" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes keeled, usually scabrous on the keel tips, lateral-veins obscure, not raised, apices acute;</text>
      <biological_entity id="o28426" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character constraint="on keel tips" constraintid="o28427" is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="keel" id="o28427" name="tip" name_original="tips" src="d0_s11" type="structure" />
      <biological_entity id="o28428" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s11" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o28429" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs (0.3) 0.5-1 mm, 0.2-0.3 times as long as the lemmas, sparse;</text>
      <biological_entity constraint="callus" id="o28430" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character constraint="lemma" constraintid="o28431" is_modifier="false" name="length" src="d0_s12" value="0.2-0.3 times as long as the lemmas" />
        <character is_modifier="false" name="count_or_density" src="d0_s12" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o28431" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.5-3 (3.5) mm, (0) 0.5-1 mm shorter than the glumes;</text>
      <biological_entity id="o28432" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="0" value_original="0" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o28433" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o28433" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>awns 1.5-2 (2.5) mm, attached to the lower 2/5 – 3/5 of the lemmas, sometimes slightly exserted, distinct from the callus hairs, bent;</text>
      <biological_entity id="o28434" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s14" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o28435" from="2/5" name="quantity" src="d0_s14" to="3/5" />
        <character is_modifier="false" modifier="sometimes slightly" name="position" notes="" src="d0_s14" value="exserted" value_original="exserted" />
        <character constraint="from callus hairs" constraintid="o28436" is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o28435" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="callus" id="o28436" name="hair" name_original="hairs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-2 mm. 2n = 28.</text>
      <biological_entity id="o28437" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28438" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis pickeringii grows in bogs, open white spruce scrub, wet meadows, coastal peatlands and lake shores, heaths, frost pockets (hollows), pitch pine barrens, and on sandy beaches, at 0-1600 m. It is found from Newfoundland and Nova Scotia south to the mountains of New Hampshire, New York, and New Jersey.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;N.H.;N.J.;Mass.;N.B.;Nfld. and Labr.;N.S.;Ont.;N.Y.;Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>