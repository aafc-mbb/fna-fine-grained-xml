<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="(P. Beauv.) Griseb." date="unknown" rank="section">Ceratochloa</taxon_name>
    <taxon_name authority="(Piper) Hitchc." date="unknown" rank="species">maritimus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section ceratochloa;species maritimus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">carinatus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">maritimus</taxon_name>
    <taxon_hierarchy>genus bromus;species carinatus;variety maritimus</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Maritime brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely ces¬pitose.</text>
      <biological_entity id="o9555" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-70 cm tall, to 3 mm thick, sometimes geniculate at the base.</text>
      <biological_entity id="o9556" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character constraint="at base" constraintid="o9557" is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o9557" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually smooth or scabridulous, some¬times slightly pubescent distally, not pilose at the throat;</text>
      <biological_entity id="o9558" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="slightly; distally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character constraint="at throat" constraintid="o9559" is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9559" name="throat" name_original="throat" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o9560" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-6 mm, densely hairy to ciliolate, acute to obtuse, erose;</text>
      <biological_entity id="o9561" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="densely hairy" name="pubescence" src="d0_s5" to="ciliolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 6-13 cm long, 6-8 mm wide, flat, both surfaces glabrous, sometimes scabrous.</text>
      <biological_entity id="o9562" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s6" to="13" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o9563" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 9-20 cm long, 2-2.5 cm wide, dense;</text>
      <biological_entity id="o9564" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower branches shorter than 10 cm, 2-4 per node, erect, with 1-2 spikelets variously distributed.</text>
      <biological_entity constraint="lower" id="o9565" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s8" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" constraint="per node" constraintid="o9566" from="2" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o9566" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o9567" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
        <character is_modifier="false" modifier="variously" name="arrangement" src="d0_s8" value="distributed" value_original="distributed" />
      </biological_entity>
      <relation from="o9565" id="r1546" name="with" negation="false" src="d0_s8" to="o9567" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 20-40 mm, usually longer than the branches and pedicels, elliptic to lanceolate, strongly laterally compressed, crowded, overlapping, with 3-7 florets.</text>
      <biological_entity id="o9568" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="40" to_unit="mm" />
        <character constraint="than the branches" constraintid="o9569" is_modifier="false" name="length_or_size" src="d0_s9" value="usually longer" value_original="usually longer" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s9" to="lanceolate" />
        <character is_modifier="false" modifier="strongly laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o9569" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o9570" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o9571" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <relation from="o9568" id="r1547" name="with" negation="false" src="d0_s9" to="o9571" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes pubescent;</text>
      <biological_entity id="o9572" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 8-12 mm, (3) 5 (7) -veined;</text>
      <biological_entity constraint="lower" id="o9573" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="(3)5(7)-veined" value_original="(3)5(7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 10-13 mm, 7 (9) -veined, shorter than the lowest lemma;</text>
      <biological_entity constraint="upper" id="o9574" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="7(9)-veined" value_original="7(9)-veined" />
        <character constraint="than the lowest lemma" constraintid="o9575" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o9575" name="lemma" name_original="lemma" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 12-14 mm, lanceolate, laterally compressed, distinctly 9-11-veined, strongly keeled at least distally, more or less uniformly hairy, often with bronze hyaline margins, apices entire or with acute teeth shorter than 1 mm;</text>
      <biological_entity id="o9576" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s13" value="9-11-veined" value_original="9-11-veined" />
        <character is_modifier="false" modifier="strongly; at-least distally; distally" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="more or less uniformly" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9577" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="bronze hyaline" value_original="bronze hyaline" />
      </biological_entity>
      <biological_entity id="o9578" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="with acute teeth" value_original="with acute teeth" />
      </biological_entity>
      <biological_entity id="o9579" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o9576" id="r1548" modifier="often" name="with" negation="false" src="d0_s13" to="o9577" />
      <relation from="o9578" id="r1549" name="with" negation="false" src="d0_s13" to="o9579" />
    </statement>
    <statement id="d0_s14">
      <text>awns (2) 4-7 mm;</text>
      <biological_entity id="o9580" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 2-4 mm. 2n = 56.</text>
      <biological_entity id="o9581" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9582" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus maritimus grows in coastal sands from Lane County, Oregon, to Los Angeles County, California.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>