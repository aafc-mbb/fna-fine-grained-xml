<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(J.M. Coult.) Nash" date="unknown" rank="species">porteri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species porteri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ported</taxon_name>
    <taxon_hierarchy>genus bromopsis;species ported</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Nodding brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o9292" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-100 cm, erect;</text>
      <biological_entity id="o9293" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes (2) 3-4 (5), glabrous or pubescent;</text>
      <biological_entity id="o9294" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s3" value="5" value_original="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes mostly glabrous, puberulent near the nodes.</text>
      <biological_entity id="o9295" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="near nodes" constraintid="o9296" is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o9296" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths glabrous or pilose, midrib of the culm leaves not abruptly narrowed just below the collar;</text>
      <biological_entity id="o9297" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9298" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character constraint="just below collar" constraintid="o9300" is_modifier="false" modifier="not abruptly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="culm" id="o9299" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9300" name="collar" name_original="collar" src="d0_s5" type="structure" />
      <relation from="o9298" id="r1488" name="part_of" negation="false" src="d0_s5" to="o9299" />
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o9301" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules to 2.5 mm, glabrous, truncate or obtuse, erose or lacerate;</text>
      <biological_entity id="o9302" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades (3) 10-25 (35) cm long, 2-5 (6) mm wide, flat, not glaucous, both surfaces usually glabrous, sometimes the adaxial surface pilose.</text>
      <biological_entity id="o9303" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o9304" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9305" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 7-20 cm, open, nodding, often 1-sided;</text>
      <biological_entity id="o9306" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s9" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches slender, ascending to spreading, often recurved and flexuous.</text>
      <biological_entity id="o9307" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="spreading" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="course" src="d0_s10" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 12-38 mm, elliptic to lanceolate, terete to moderately laterally compressed, with (3) 5-11 (13) florets.</text>
      <biological_entity id="o9308" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="38" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o9309" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="13" value_original="13" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="11" />
      </biological_entity>
      <relation from="o9308" id="r1489" name="with" negation="false" src="d0_s11" to="o9309" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes usually pubescent, rarely glabrous;</text>
      <biological_entity id="o9310" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 5-7 (9) mm, usually 3-veined, sometimes 1-veined;</text>
      <biological_entity constraint="lower" id="o9311" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 6-10 mm, 3-veined, not mucronate;</text>
      <biological_entity constraint="upper" id="o9312" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 8-14 mm, elliptic, rounded over the midvein, usually pubescent or pilose, margins often with longer hairs, backs and margins rarely glabrous, apices acute or obtuse to truncate, entire;</text>
      <biological_entity id="o9313" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="14" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character constraint="over midvein" constraintid="o9314" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9314" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o9315" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity constraint="longer" id="o9316" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <biological_entity constraint="longer" id="o9317" name="back" name_original="backs" src="d0_s15" type="structure" />
      <biological_entity id="o9318" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9319" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o9315" id="r1490" name="with" negation="false" src="d0_s15" to="o9316" />
      <relation from="o9315" id="r1491" name="with" negation="false" src="d0_s15" to="o9317" />
      <relation from="o9315" id="r1492" name="with" negation="false" src="d0_s15" to="o9318" />
    </statement>
    <statement id="d0_s16">
      <text>awns (1) 2-3 (3.5) mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o9320" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o9321" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o9321" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers (1) 2-3 mm. 2n = 14.</text>
      <biological_entity id="o9322" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9323" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus porteri grows in montane meadows, grassy slopes, mesic steppes, forest edges, and open forest habitats, at 500-3500 m. It is found from British Columbia to Manitoba, and south to California, western Texas, and Mexico. It is closely related to B. anomalus, and has often been included in that species. It differs chiefly in its lack of auricles, and in having culm leaves with midribs that are not narrowed just below the collar.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Wyo.;Ariz.;Calif.;Utah;Nebr.;S.Dak.;Mont.;Idaho;N.Dak.;Nev.;Tex.;Man.;Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>