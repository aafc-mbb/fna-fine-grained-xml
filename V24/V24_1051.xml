<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephan L. Hatch;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">740</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ARRHENATHERUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus arrhenatherum</taxon_hierarchy>
  </taxon_identification>
  <number>14.54</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o22008" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-200 cm, basal internodes occasionally globose.</text>
      <biological_entity id="o22009" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="200" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22010" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s2" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open, not overlapping;</text>
      <biological_entity id="o22011" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o22012" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, sometimes ciliate;</text>
      <biological_entity id="o22013" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat or convolute.</text>
      <biological_entity id="o22014" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, narrow panicles;</text>
      <biological_entity id="o22015" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o22016" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches spreading until after anthesis, then becoming loosely appressed to the rachises.</text>
      <biological_entity id="o22017" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="after until anthesis" is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character constraint="to rachises" constraintid="o22018" is_modifier="false" modifier="becoming loosely" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o22018" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets pedicellate, laterally compressed, with 2 florets, lower florets staminate, upper florets pistillate or bisexual, a rudimentary floret occasionally present distally;</text>
      <biological_entity id="o22019" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o22020" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22021" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o22022" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o22023" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" modifier="occasionally; distally" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o22019" id="r3480" name="with" negation="false" src="d0_s9" to="o22020" />
    </statement>
    <statement id="d0_s10">
      <text>rachillas pubescent;</text>
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes, the florets usually falling together, rarely falling separately.</text>
      <biological_entity id="o22024" name="rachilla" name_original="rachillas" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22025" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o22026" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually; together" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
        <character is_modifier="false" modifier="rarely; separately" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
      </biological_entity>
      <relation from="o22024" id="r3481" name="above" negation="false" src="d0_s11" to="o22025" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, hyaline, unawned;</text>
      <biological_entity id="o22027" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes less than 3/4 the length of the upper glumes, 1-veined or 3-veined;</text>
      <biological_entity constraint="lower" id="o22028" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="3/4" />
        <character is_modifier="false" name="length" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="length" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o22029" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o22030" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>calluses short, blunt, pubescent;</text>
      <biological_entity id="o22031" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas membranous, 3-7-veined, acute, awned below midlength, awns twisted and geniculate;</text>
      <biological_entity constraint="lower" id="o22032" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-7-veined" value_original="3-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
        <character constraint="below midlength" is_modifier="false" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o22033" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s16" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas membranous to subcoriacous, glabrous or hairy, 7-veined, acute, usually unawned, sometimes awned from near the apices, awns short, straight, rarely awned similarly to the lower lemmas;</text>
      <biological_entity constraint="upper" id="o22034" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="membranous" name="texture" src="d0_s17" to="subcoriacous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="7-veined" value_original="7-veined" />
        <character is_modifier="false" name="shape" src="d0_s17" value="acute" value_original="acute" />
        <character constraint="from " constraintid="o22035" is_modifier="false" modifier="usually; sometimes" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o22035" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <biological_entity id="o22036" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character constraint="to lower lemmas" constraintid="o22037" is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22037" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>paleas subequal to the lemmas, 2-veined, 2-keeled, keels scabrous or hairy, apices notched;</text>
      <biological_entity id="o22038" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character constraint="to lemmas" constraintid="o22039" is_modifier="false" name="size" src="d0_s18" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" name="shape" src="d0_s18" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o22039" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o22040" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22041" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>lodicules 2, free, linear, membranous, glabrous, entire;</text>
      <biological_entity id="o22042" name="lodicule" name_original="lodicules" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s19" value="linear" value_original="linear" />
        <character is_modifier="false" name="texture" src="d0_s19" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 3, 3.4-6.5 mm;</text>
      <biological_entity id="o22043" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s20" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovaries pubescent.</text>
      <biological_entity id="o22044" name="ovary" name_original="ovaries" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s21" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Caryopses shorter than the lemmas, concealed at maturity, not grooved, dorsally compressed to terete, hairy;</text>
      <biological_entity id="o22045" name="caryopse" name_original="caryopses" src="d0_s22" type="structure">
        <character constraint="than the lemmas" constraintid="o22046" is_modifier="false" name="height_or_length_or_size" src="d0_s22" value="shorter" value_original="shorter" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s22" value="concealed" value_original="concealed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="dorsally compressed" name="shape" src="d0_s22" to="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22046" name="lemma" name_original="lemmas" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>hila long-linear, x = 7.</text>
      <biological_entity id="o22047" name="hilum" name_original="hila" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s23" value="long-linear" value_original="long-linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o22048" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Arrhenatherum is a Mediterranean and eastern Asian genus of six species; one has become established in North America.</discussion>
  <references>
    <reference>Brandenburg, D.M. 1985. Systematic studies in the Poaceae and Cyperaceae. Ph.D. dissertation, University of Oklahoma, Norman, Oklahoma, U.S.A. 249 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C;Wis.;W.Va.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;B.C.;N.B.;Nfld. and Labr. (Labr.);N.S.;Ont.;Que.;Wyo.;N.Mex.;La.;N.C.;Nebr.;Tenn.;Pa.;Calif.;Nev.;Va.;Colo.;Md.;Alaska;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Mont.;Oreg.;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Miss.;S.Dak.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>