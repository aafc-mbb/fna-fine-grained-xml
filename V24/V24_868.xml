<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">0</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Willk." date="unknown" rank="genus">CUTANDIA</taxon_name>
    <taxon_name authority="(Spreng.) K. Richt." date="unknown" rank="species">memphitica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus cutandia;species memphitica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Memphis grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 4-35 (42) cm, prostrate, geniculate, or erect, somewhat stiff.</text>
      <biological_entity id="o8499" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="42" value_original="42" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths inflated, glabrous;</text>
      <biological_entity id="o8500" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ligules 2-5 mm, truncate to acute, entire to erose;</text>
      <biological_entity id="o8501" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="acute" />
        <character char_type="range_value" from="entire" name="architecture" src="d0_s2" to="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades mostly 2-10 cm long, 1-4 mm wide, glabrous.</text>
      <biological_entity id="o8502" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 3-18 cm;</text>
      <biological_entity id="o8503" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches usually 1 (2) per node, branches and pedicels strongly divaricate at maturity;</text>
      <biological_entity id="o8504" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="per pedicels" constraintid="o8507" name="atypical_quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8505" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o8506" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o8507" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="strongly" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pedicels of lateral spikelets 0.3-0.6 (1.5) mm, sharply angular.</text>
      <biological_entity id="o8508" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="arrangement_or_shape" src="d0_s6" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8509" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <relation from="o8508" id="r1380" name="part_of" negation="false" src="d0_s6" to="o8509" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 7-10.5 mm, oblanceolate, with 2-3 (4) florets;</text>
      <biological_entity id="o8510" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="10.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o8511" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="4" value_original="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o8510" id="r1381" name="with" negation="false" src="d0_s7" to="o8511" />
    </statement>
    <statement id="d0_s8">
      <text>rachilla internodes 1.5-3 mm.</text>
      <biological_entity constraint="rachilla" id="o8512" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes acute to acuminate, 1-veined, veins excurrent to 0.4 mm;</text>
      <biological_entity id="o8513" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o8514" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 3.5-5 mm;</text>
      <biological_entity constraint="lower" id="o8515" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 4.5-6 mm;</text>
      <biological_entity constraint="upper" id="o8516" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 5.8-7.5 mm, smooth or scabridulous on the midvein, emarginate to shortly bifid, mid veins excurrent to 1.2 mm;</text>
      <biological_entity id="o8517" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s12" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="on midvein" constraintid="o8518" is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
        <character char_type="range_value" from="emarginate" name="architecture_or_shape" notes="" src="d0_s12" to="shortly bifid" />
      </biological_entity>
      <biological_entity id="o8518" name="midvein" name_original="midvein" src="d0_s12" type="structure" />
      <biological_entity constraint="mid" id="o8519" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1-2 mm. 2n = 14.</text>
      <biological_entity id="o8520" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8521" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cutandia memphitica is native to maritime sands, inland dunes, sandy gravel, and gypsum soils in the Mediterranean region and Middle East. In the Flora region, it was collected in sandy soil at a nursery at Devil's Canyon in the San Bernardino Mountains, California, in 1933. How it got there and whether it persists are not known.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>