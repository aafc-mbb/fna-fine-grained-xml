<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">DESCHAMPSIA</taxon_name>
    <taxon_name authority="(Hook.) Munro" date="unknown" rank="species">elongata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus deschampsia;species elongata</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Slender hairgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose.</text>
      <biological_entity id="o23855" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (10) 30-120 cm.</text>
      <biological_entity id="o23856" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sometimes forming a basal tuft;</text>
      <biological_entity id="o23857" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o23858" name="tuft" name_original="tuft" src="d0_s3" type="structure" />
      <relation from="o23857" id="r3783" name="forming a" negation="false" src="d0_s3" to="o23858" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o23859" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2.5-8 (9) mm, acute to acuminate;</text>
      <biological_entity id="o23860" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 7-30 cm long, 0.2-2 mm wide, usually involute.</text>
      <biological_entity id="o23861" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-30 (35) cm long, 0.5-1.5 (2) cm wide, erect or nodding;</text>
      <biological_entity id="o23862" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="35" value_original="35" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches erect to ascending.</text>
      <biological_entity id="o23863" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 3-6.7 mm, bisexual, narrowly V-shaped, appressed to the branches.</text>
      <biological_entity id="o23864" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6.7" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="v--shaped" value_original="v--shaped" />
        <character constraint="to branches" constraintid="o23865" is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o23865" name="branch" name_original="branches" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes equaling or exceeding the florets, narrowly lanceolate, usually pale green, sometimes purple-tipped, 3-veined, acuminate;</text>
      <biological_entity id="o23866" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s10" value="exceeding the florets" value_original="exceeding the florets" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes (3) 3.2-5.5 (6.7) mm;</text>
      <biological_entity constraint="lower" id="o23867" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes (3) 3.1-5.4 (6) mm;</text>
      <biological_entity constraint="upper" id="o23868" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s12" to="5.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 0.3-1.15 mm;</text>
      <biological_entity constraint="callus" id="o23869" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1.15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 1.7-4.3 mm, smooth, shiny, glabrous, apices weakly toothed or erose, awns 1.5-5.5 (6) mm, straight to slightly geniculate, attached from slightly below to slightly above the middle of the lemma, exceeding the florets by 1-2.5 mm;</text>
      <biological_entity id="o23870" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s14" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23871" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s14" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s14" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o23872" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
        <character constraint="slightly below slightly above middle lemmas" constraintid="o23873" is_modifier="false" modifier="below to slightly" name="fixation" src="d0_s14" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity constraint="middle" id="o23873" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity id="o23874" name="lemma" name_original="lemma" src="d0_s14" type="structure" />
      <biological_entity id="o23875" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s14" value="exceeding" value_original="exceeding" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o23873" id="r3784" name="part_of" negation="false" src="d0_s14" to="o23874" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.3-0.5 (0.7) mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 26.</text>
      <biological_entity id="o23876" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="0.7" value_original="0.7" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23877" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Deschampsia elongata grows in moist to wet habitats, from near sea level to alpine elevations, from Alaska and the Yukon south to northern Mexico and east to Montana, Wyoming, and Arizona. It also grows, as a disjunct, in Chile. The records from Maine and Colorado probably represent introductions.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Mass.;N.Mex.;Wash.;Utah;Calif.;Idaho;Mont.;Alta.;B.C.;Nunavut;Yukon;Wyo.;S.C.;Ariz.;Alaska;Nev.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>