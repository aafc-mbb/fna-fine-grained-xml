<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="(P. Beauv.) Griseb." date="unknown" rank="section">Ceratochloa</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="species">sitchensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section ceratochloa;species sitchensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Sitka brome</other_name>
  <other_name type="common_name">Alaska brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely ces¬pitose.</text>
      <biological_entity id="o4199" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 120-180 cm tall, 3-5 mm thick, erect.</text>
      <biological_entity id="o4200" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="120" from_unit="cm" name="height" src="d0_s2" to="180" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or sparsely pilose;</text>
      <biological_entity id="o4201" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o4202" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 3-4 mm, glabrous or hairy, obtuse, lacerate;</text>
      <biological_entity id="o4203" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 20^40 cm long, 2-9 mm wide, flat, sparsely pilose adaxially or on both surfaces.</text>
      <biological_entity id="o4204" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="20" value_original="20" />
        <character name="length" src="d0_s6" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character constraint="on surfaces" constraintid="o4205" is_modifier="false" modifier="sparsely; adaxially" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="on both surfaces" value_original="on both surfaces" />
      </biological_entity>
      <biological_entity id="o4205" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 25-35 cm, open;</text>
      <biological_entity id="o4206" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s7" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower branches to 20 cm, 2-4 (6) per node, spreading, often drooping, with 1-3 spikelets on the distal 1/2 sometimes confined to the tips.</text>
      <biological_entity constraint="lower" id="o4207" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
        <character name="atypical_quantity" src="d0_s8" value="6" value_original="6" />
        <character char_type="range_value" constraint="per node" constraintid="o4208" from="2" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s8" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity id="o4208" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o4209" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4210" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity id="o4211" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <relation from="o4207" id="r681" name="with" negation="false" src="d0_s8" to="o4209" />
      <relation from="o4209" id="r682" name="on" negation="false" src="d0_s8" to="o4210" />
      <relation from="o4210" id="r683" name="confined to the" negation="false" src="d0_s8" to="o4211" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 18-38 mm, elliptic to lanceolate, strongly laterally compressed, with (5) 6-9 florets.</text>
      <biological_entity id="o4212" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s9" to="38" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="false" modifier="strongly laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o4213" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
      <relation from="o4212" id="r684" name="with" negation="false" src="d0_s9" to="o4213" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes glabrous, sometimes scabrous;</text>
      <biological_entity id="o4214" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 6-10 mm, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o4215" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 8-11 mm, 5-7-veined;</text>
      <biological_entity constraint="upper" id="o4216" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 12-14 (15) mm, lanceolate, laterally compressed, 7-11-veined, strongly keeled at least distally, usually glabrous, sometimes hirtellous, margins sometimes sparsely pilose, apices entire or with acute teeth, teeth shorter than 1 mm;</text>
      <biological_entity id="o4217" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-11-veined" value_original="7-11-veined" />
        <character is_modifier="false" modifier="strongly; at-least distally; distally" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o4218" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o4219" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="with acute teeth" value_original="with acute teeth" />
      </biological_entity>
      <biological_entity id="o4220" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4221" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4219" id="r685" name="with" negation="false" src="d0_s13" to="o4220" />
    </statement>
    <statement id="d0_s14">
      <text>awns 5-10 mm;</text>
      <biological_entity id="o4222" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers to 6 mm. 2n = 42, 56.</text>
      <biological_entity id="o4223" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4224" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
        <character name="quantity" src="d0_s15" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus sitchensis grows on exposed rock bluffs and cliffs, and in meadows, often in the partial shade of forests along the ocean edge, and on road verges and other disturbed sites. Its range extends from the Aleutian Islands and Alaska panhandle through British Columbia to southern California.</discussion>
  <discussion>Bromus sitchensis resembles B. aleutensis, the two sometimes being treated as conspecific varieties. Bromus sitchensis is predominantly outcrossing, while B. aleutensis is predominantly self-fertilizing (C.L. Hitchcock 1969).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Alaska;Calif.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>