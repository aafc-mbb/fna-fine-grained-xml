<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Homalopoa</taxon_name>
    <taxon_name authority="Vasey &amp; Scribn." date="unknown" rank="species">reflexa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section homalopoa;species reflexa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>22</number>
  <other_name type="common_name">Nodding bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, short-lived;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely tufted, tuft bases narrow or not, not stoloniferous, not rhizomatous.</text>
      <biological_entity id="o23517" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mixed intra and extravaginal.</text>
      <biological_entity constraint="tuft" id="o23518" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character name="size_or_width" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 10-60 cm.</text>
      <biological_entity id="o23519" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/3 – 2/3 their length, terete, smooth;</text>
      <biological_entity id="o23520" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1.5-3.5 mm, smooth or sparsely scabrous;</text>
      <biological_entity id="o23521" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1.5-4 mm wide, flat, thin, soft, apices broadly prow-shaped.</text>
      <biological_entity id="o23522" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o23523" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-15 cm, nodding, open, with numerous spikelets and 1-2 branches per node;</text>
      <biological_entity id="o23524" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o23525" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o23526" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o23527" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o23524" id="r3737" name="with" negation="false" src="d0_s7" to="o23525" />
      <relation from="o23524" id="r3738" name="with" negation="false" src="d0_s7" to="o23526" />
      <relation from="o23525" id="r3739" name="per" negation="false" src="d0_s7" to="o23527" />
      <relation from="o23526" id="r3740" name="per" negation="false" src="d0_s7" to="o23527" />
    </statement>
    <statement id="d0_s8">
      <text>branches (2) 3-7 cm, spreading to reflexed, lower branches usually reflexed, flexuous, usually terete, smooth or sparsely scabrous, with (3) 6-18 spikelets.</text>
      <biological_entity id="o23528" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="reflexed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23529" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o23530" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="3" value_original="3" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
      <relation from="o23529" id="r3741" name="with" negation="false" src="d0_s8" to="o23530" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-6 mm, lanceolate to broadly lanceolate, usually partly to wholly purplish, with 3-5 florets;</text>
      <biological_entity id="o23531" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="broadly lanceolate" />
        <character is_modifier="false" modifier="usually partly; partly to wholly" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o23532" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o23531" id="r3742" name="with" negation="false" src="d0_s9" to="o23532" />
    </statement>
    <statement id="d0_s10">
      <text>rachilla internodes shorter than 1 mm, smooth.</text>
      <biological_entity constraint="rachilla" id="o23533" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes narrowly to broadly lanceolate, distinctly keeled, keels smooth or nearly so;</text>
      <biological_entity id="o23534" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o23535" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s11" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 1-veined;</text>
      <biological_entity constraint="lower" id="o23536" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o23537" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o23538" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o23538" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses webbed;</text>
      <biological_entity id="o23539" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2-3.5 mm, lanceolate, partly purple to fairly strongly purple, distinctly keeled, keels and marginal veins short to long-villous, keels hairy for 2h-ls their length, lateral-veins usually sparsely softly puberulent at least on 1 side, lateral-veins obscure to moderately prominent, intercostal regions smooth, minutely bumpy, glabrous, apices acute, slightly bronze-colored or not;</text>
      <biological_entity id="o23540" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="partly purple" name="coloration_or_density" src="d0_s15" to="fairly strongly purple" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o23541" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o23542" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o23543" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23544" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character constraint="on side" constraintid="o23545" is_modifier="false" modifier="usually sparsely softly" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23545" name="side" name_original="side" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23546" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s15" to="moderately prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o23547" name="region" name_original="regions" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="bumpy" value_original="bumpy" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23548" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="slightly" name="coloration" src="d0_s15" value="bronze-colored" value_original="bronze-colored" />
        <character name="coloration" src="d0_s15" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>palea keels scabrous, usually softly puberulent at midlength;</text>
      <biological_entity constraint="palea" id="o23549" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character constraint="at midlength" constraintid="o23550" is_modifier="false" modifier="usually softly" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23550" name="midlength" name_original="midlength" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.6-1 mm. 2n = 28.</text>
      <biological_entity id="o23551" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23552" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa reflexa grows in subalpine forests, meadows, and low alpine habitats, primarily in the central and southern Rocky Mountains. It usually grows on drier and more disturbed sites, and appears shorter-lived, than the frequently sympatric or parapatric P. leptocoma (p. 573), from which it differs in usually having hairs on the palea keels and lateral veins of the lemmas, and smooth panicle branches. In addition, P. reflexa is tetraploid, whereas P. leptocoma is hexaploid. Poa reflexa may resemble small plants of P. occidentalis (see previous) in habit.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Utah;Oreg.;Mont.;Wyo.;Ariz.;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>