<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">536</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Homalopoa</taxon_name>
    <taxon_name authority="Vasey &amp; Scribn." date="unknown" rank="species">bigelovii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section homalopoa;species bigelovii</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Bigelow’s bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually annual, rarely longer-lived;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely tufted, tuft bases narrow, usually without sterile shoots, not stolonigerous, not rhizomatous.</text>
      <biological_entity id="o20958" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20960" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o20959" id="r3309" modifier="usually" name="without" negation="false" src="d0_s1" to="o20960" />
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intravaginal.</text>
      <biological_entity constraint="tuft" id="o20959" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="not; not" name="architecture" notes="" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms (2) 5-60 (70) cm tall, 0.3-1 mm thick, usually erect, bases rarely geniculate;</text>
      <biological_entity id="o20961" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="height" src="d0_s3" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="5" from_unit="cm" name="height" src="d0_s3" to="60" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="thickness" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o20962" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, usually 1 exserted.</text>
      <biological_entity id="o20963" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character modifier="usually" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/4 - 1/2 their length, usually compressed and keeled, smooth or the keels scabrous;</text>
      <biological_entity id="o20964" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20965" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 2-6 mm, smooth or scabrous, usually decurrent, obtuse to acute;</text>
      <biological_entity id="o20966" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 1.5-5 mm wide, flat, thin, soft, finely scabrous, apices broadly prow-shaped, cauline blades (1) 4-15 cm, flag leaf-blades usually 1-4 cm.</text>
      <biological_entity id="o20967" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20968" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20969" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20970" name="blade-leaf" name_original="leaf-blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles (1) 5-15 cm, erect, cylindrical, contracted, sometimes interrupted, congested, with 2-3 (5) branches per node;</text>
      <biological_entity id="o20971" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o20972" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o20973" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o20971" id="r3310" name="with" negation="false" src="d0_s8" to="o20972" />
      <relation from="o20972" id="r3311" name="per" negation="false" src="d0_s8" to="o20973" />
    </statement>
    <statement id="d0_s9">
      <text>branches erect or steeply ascending, smooth or sparsely to densely scabrous.</text>
      <biological_entity id="o20974" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="steeply" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-7 mm, laterally compressed;</text>
      <biological_entity id="o20975" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 3-7;</text>
      <biological_entity id="o20976" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes to 1 mm, smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o20977" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes subequal, distinctly keeled, keels and sometimes the lateral-veins scabrous;</text>
      <biological_entity id="o20978" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o20979" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20980" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o20981" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o20982" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o20983" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o20983" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses webbed;</text>
      <biological_entity id="o20984" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 2.6-4.2 mm, lanceolate, distinctly keeled, smooth, keels, marginal veins, and sometimes the lateral-veins short to long-villous, keels hairy to near the apices, marginal veins to 2/3 their length, lateral-veins obscure to moderately prominent, intercostal regions glabrous or softly puberulent, upper margins white, apices acute;</text>
      <biological_entity id="o20985" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s17" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20986" name="keel" name_original="keels" src="d0_s17" type="structure" />
      <biological_entity constraint="marginal" id="o20987" name="vein" name_original="veins" src="d0_s17" type="structure" />
      <biological_entity id="o20988" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o20989" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character constraint="near apices" constraintid="o20990" is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o20990" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <biological_entity constraint="marginal" id="o20991" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="length" src="d0_s17" to="2/3" />
      </biological_entity>
      <biological_entity id="o20992" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s17" to="moderately prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o20993" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="upper" id="o20994" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o20995" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>palea keels softly puberulent to short-villous at midlength, scabrous near the apices, intercostal regions usually softly puberulent;</text>
      <biological_entity constraint="palea" id="o20996" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="at apices, regions" constraintid="o20997, o20998" from="softly puberulent" name="pubescence" src="d0_s18" to="short-villous" />
      </biological_entity>
      <biological_entity id="o20997" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="true" name="position" src="d0_s18" value="midlength" value_original="midlength" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20998" name="region" name_original="regions" src="d0_s18" type="structure">
        <character is_modifier="true" name="position" src="d0_s18" value="midlength" value_original="midlength" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o20999" name="apex" name_original="apices" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 1-3, 0.2-1 mm. 2n = 28, 28+1.</text>
      <biological_entity id="o21000" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" unit=",0.2-1 mm" />
        <character char_type="range_value" from="1" name="some_measurement" src="d0_s19" to="3" unit=",0.2-1 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21001" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
        <character char_type="range_value" from="28" name="quantity" src="d0_s19" upper_restricted="false" />
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa bigelovii grows in arid upland regions, particularly on shady, rocky slopes of the southwestern United States and northern Mexico. Plants from southeastern Arizona eastwards are usually glabrous between the lemma veins, whereas more western plants are usually puberulent between the lemma veins. Plants with 1 or 2 small anthers are found in the eastern portion of the species' range; they differ from P. chapmaniana (p. 534) in their persistently contracted panicles and broader leaf blades.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Calif.;Colo.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>