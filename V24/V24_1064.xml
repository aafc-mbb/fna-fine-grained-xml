<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">750</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Pers." date="unknown" rank="genus">TRISETUM</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">interruptum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus trisetum;species interruptum</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Prairie trisetum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, without sterile shoots;</text>
      <biological_entity id="o16483" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o16482" id="r2645" name="without" negation="false" src="d0_s0" to="o16483" />
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o16482" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (5) 10-40 (60) cm, erect or spreading, mostly glabrous, pilose below the nodes.</text>
      <biological_entity id="o16484" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o16485" is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o16485" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o16486" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s3" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths scabridulous or pilose;</text>
      <biological_entity id="o16487" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2.5 mm, truncate;</text>
      <biological_entity id="o16488" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-12 cm long, 1-4 mm wide, flat, folded, or involute distally when dry, ascending, glabrous or pubescent, margins frequently sparsely ciliate.</text>
      <biological_entity id="o16489" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16490" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="frequently sparsely" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-15 cm long, 0.3-1.5 cm wide, often interrupted, at least in the lower 1/3, green or tan;</text>
      <biological_entity id="o16491" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="lower" id="o16492" name="1/3" name_original="1/3" src="d0_s7" type="structure" />
      <relation from="o16491" id="r2646" modifier="at-least" name="in" negation="false" src="d0_s7" to="o16492" />
    </statement>
    <statement id="d0_s8">
      <text>branches short, usually erect to appressed, the spikelets crowded.</text>
      <biological_entity id="o16493" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character char_type="range_value" from="usually erect" name="orientation" src="d0_s8" to="appressed" />
      </biological_entity>
      <biological_entity id="o16494" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 3-6 mm, often in pairs with 1 subsessile and 1 pedicellate, with 2-3 florets;</text>
      <biological_entity id="o16496" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character modifier="with 1 subsessile" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16497" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <relation from="o16495" id="r2647" modifier="often" name="in" negation="false" src="d0_s9" to="o16496" />
      <relation from="o16495" id="r2648" name="with" negation="false" src="d0_s9" to="o16497" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation initially above the glumes, subsequently below;</text>
      <biological_entity id="o16495" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o16498" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o16495" id="r2649" modifier="initially; subsequently below; below" name="above" negation="false" src="d0_s10" to="o16498" />
    </statement>
    <statement id="d0_s11">
      <text>rachilla internodes usually 0.8-1 mm;</text>
      <biological_entity constraint="rachilla" id="o16499" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla hairs usually about 0.5 mm.</text>
      <biological_entity constraint="rachilla" id="o16500" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes subequal, 4-5 mm, about as long as the lowest lemmas, smooth or sparsely scabridulous;</text>
      <biological_entity id="o16501" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o16502" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.5-1 mm wide, lanceolate or elliptical, 3-veined, acuminate, sometimes apiculate;</text>
      <biological_entity constraint="lower" id="o16503" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="elliptical" value_original="elliptical" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes about twice as wide as the lower glumes, elliptical or oblanceolate, acuminate;</text>
      <biological_entity constraint="upper" id="o16504" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="glume" constraintid="o16505" is_modifier="false" name="width" src="d0_s15" value="2 times as wide as the lower glumes" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elliptical" value_original="elliptical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o16505" name="glume" name_original="glumes" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>callus hairs 0.1-0.2 (0.5) mm, sparse;</text>
      <biological_entity constraint="callus" id="o16506" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="count_or_density" src="d0_s16" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 3-4.5 mm, usually glabrous, sometimes minutely pustulate-scabridulous, apices bifid, teeth to 1.7 mm, awned, awns usually 4-8 mm, arising from midlength to just below the teeth and exceeding the lemma apices, geniculate, twisted basally, rarely 2-4 mm, straight, arcuate, or flexuous;</text>
      <biological_entity id="o16507" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes minutely" name="relief" src="d0_s17" value="pustulate-scabridulous" value_original="pustulate-scabridulous" />
      </biological_entity>
      <biological_entity id="o16508" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o16509" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o16510" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
        <character constraint="from midlength to" constraintid="o16511" is_modifier="false" name="orientation" src="d0_s17" value="arising" value_original="arising" />
        <character is_modifier="false" name="shape" src="d0_s17" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s17" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="course" src="d0_s17" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="course" src="d0_s17" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="course" src="d0_s17" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="midlength" id="o16511" name="to" name_original="to" src="d0_s17" type="structure" />
      <biological_entity id="o16512" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
      <biological_entity constraint="lemma" id="o16513" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <relation from="o16511" id="r2650" name="just below" negation="false" src="d0_s17" to="o16512" />
      <relation from="o16510" id="r2651" name="exceeding the" negation="false" src="d0_s17" to="o16513" />
    </statement>
    <statement id="d0_s18">
      <text>paleas usually 2/3 as long as the lemmas, hyaline;</text>
      <biological_entity id="o16514" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character constraint="as-long-as lemmas" constraintid="o16515" name="quantity" src="d0_s18" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s18" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o16515" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>anthers about 0.2 mm.</text>
      <biological_entity id="o16516" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="some_measurement" src="d0_s19" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 2-3 mm, longitudinally striate, sometimes with a few hairs distally.</text>
      <biological_entity id="o16518" name="hair" name_original="hairs" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="few" value_original="few" />
      </biological_entity>
      <relation from="o16517" id="r2652" modifier="sometimes" name="with" negation="false" src="d0_s20" to="o16518" />
    </statement>
    <statement id="d0_s21">
      <text>2n = 14.</text>
      <biological_entity id="o16517" name="caryopse" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="longitudinally" name="coloration_or_pubescence_or_relief" src="d0_s20" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16519" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Trisetum interruptum grows in open, dry or moist soil in deserts, plains, arid shrublands, and riparian woodlands, from the southern United States into Mexico. It is often weedy.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;La.;Colo.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>