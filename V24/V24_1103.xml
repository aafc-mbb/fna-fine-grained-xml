<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">777</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Host" date="unknown" rank="genus">AMMOPHILA</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">breviligulata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus ammophila;species breviligulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 50-130 cm.</text>
      <biological_entity id="o25443" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ligules 1-3 (4.6) mm, truncate to obtuse, ciliolate;</text>
      <biological_entity id="o25444" name="ligule" name_original="ligules" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="mm" value="4.6" value_original="4.6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 15-80 cm long, 4-8 mm wide when flat, usually involute, 0.5-2.5 mm in diameter when involute;</text>
    </statement>
    <statement id="d0_s3">
      <text>flag leaf-blades 5-39 cm.</text>
      <biological_entity id="o25445" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s2" to="80" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" modifier="when flat" name="width" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s2" value="involute" value_original="involute" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="when involute" name="diameter" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25446" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="distance" src="d0_s3" to="39" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 9-40 cm long, 10-25 mm wide, stramineous.</text>
      <biological_entity id="o25447" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="stramineous" value_original="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Glumes 8-15 mm, exceeding the florets;</text>
      <biological_entity id="o25448" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25449" name="floret" name_original="florets" src="d0_s5" type="structure" />
      <relation from="o25448" id="r4031" name="exceeding the" negation="false" src="d0_s5" to="o25449" />
    </statement>
    <statement id="d0_s6">
      <text>callus hairs 1-3 mm;</text>
      <biological_entity constraint="callus" id="o25450" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lemmas 8-11.5 (14) mm, somewhat thicker than the glumes, apices acute, entire, unawned, rarely mucronate, mucros to 0.2 mm, subterminal;</text>
      <biological_entity id="o25451" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="14" value_original="14" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="11.5" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o25452" is_modifier="false" name="width" src="d0_s7" value="somewhat thicker" value_original="somewhat thicker" />
      </biological_entity>
      <biological_entity id="o25452" name="glume" name_original="glumes" src="d0_s7" type="structure" />
      <biological_entity id="o25453" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o25454" name="mucro" name_original="mucros" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s7" value="subterminal" value_original="subterminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3-7 mm.</text>
      <biological_entity id="o25455" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The two subspecies overlap in their morphological characteristics and occupy similar habitats, the primary difference between them being their flowering time.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Va.;Del.;Wis.;N.H.;N.C.;Pa.;R.I.;B.C.;N.B.;Nfld. and Labr.;N.S.;Ont.;P.E.I.;Que.;Mass.;Maine;Vt.;Calif.;Ill.;Ind.;Md.;Ohio;Minn.;Mich.;S.C.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Flowering late July to  September; panicles (9.5)13-40 cm long; anthers 3.5-7 mm long</description>
      <determination>Ammophila breviligulata subsp. breviligulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Flowering late June to early July; panicles 9-17 cm long; anthers 3-4.5 mm long</description>
      <determination>Ammophila breviligulata subsp. champlainensis</determination>
    </key_statement>
  </key>
</bio:treatment>