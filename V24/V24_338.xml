<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Bromus</taxon_name>
    <taxon_name authority="Thunb." date="unknown" rank="species">japonicus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromus;species japonicus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">japonicus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">porrectus</taxon_name>
    <taxon_hierarchy>genus bromus;species japonicus;variety porrectus</taxon_hierarchy>
  </taxon_identification>
  <number>51</number>
  <other_name type="common_name">Australian brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o30393" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (22) 30-70 cm, erect or ascending.</text>
      <biological_entity id="o30394" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="22" value_original="22" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths usually densely pilose;</text>
      <biological_entity id="o30395" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>upper sheaths sometimes pubescent or glabrous;</text>
      <biological_entity constraint="upper" id="o30396" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2.2 mm, pilose, obtuse, lacerate;</text>
      <biological_entity id="o30397" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 10-20 cm long, 2-4 mm wide, usually pilose on both surfaces.</text>
      <biological_entity id="o30398" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o30399" is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o30399" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-22 cm long, 4-13 cm wide, open, nodding;</text>
      <biological_entity id="o30400" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s6" to="13" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches usually longer than the spikelets, ascending to spreading or somewhat drooping, slender, flexuous, sometimes sinuous, often with more than 1 spikelet.</text>
      <biological_entity id="o30401" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character constraint="than the spikelets" constraintid="o30402" is_modifier="false" name="length_or_size" src="d0_s7" value="usually longer" value_original="usually longer" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s7" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s7" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s7" value="sinuous" value_original="sinuous" />
      </biological_entity>
      <biological_entity id="o30402" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o30403" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o30401" id="r4786" modifier="often" name="with" negation="false" src="d0_s7" to="o30403" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 20-40 mm, lanceolate, terete to moderately laterally compressed;</text>
      <biological_entity id="o30404" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="40" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s8" to="moderately laterally compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>florets 6-12, bases concealed at maturity;</text>
      <biological_entity id="o30405" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="12" />
      </biological_entity>
      <biological_entity id="o30406" name="base" name_original="bases" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s9" value="concealed" value_original="concealed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla internodes concealed at maturity.</text>
      <biological_entity constraint="rachilla" id="o30407" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s10" value="concealed" value_original="concealed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes smooth or scabrous;</text>
      <biological_entity id="o30408" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 4.5-7 mm, (3) 5-veined;</text>
      <biological_entity constraint="lower" id="o30409" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="(3)5-veined" value_original="(3)5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 5-8 mm, 7-veined;</text>
      <biological_entity constraint="upper" id="o30410" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 7-9 mm long, 1.2-2.2 mm wide, lanceolate, coriaceous, smooth proximally, scabrous on the distal 1/2, obscurely (7) 9-veined, rounded over the midvein, margins hyaline, 0.3-0.6 mm wide, obtusely angled above the middle, not inrolled at maturity, apices acute, bifid, teeth shorter than 1 mm;</text>
      <biological_entity id="o30411" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s14" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s14" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="on distal 1/2" constraintid="o30412" is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="obscurely" name="architecture" notes="" src="d0_s14" value="(7)9-veined" value_original="(7)9-veined" />
        <character constraint="over midvein" constraintid="o30413" is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30412" name="1/2" name_original="1/2" src="d0_s14" type="structure" />
      <biological_entity id="o30413" name="midvein" name_original="midvein" src="d0_s14" type="structure" />
      <biological_entity id="o30414" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s14" to="0.6" to_unit="mm" />
        <character constraint="above middle margins" constraintid="o30415" is_modifier="false" modifier="obtusely" name="shape" src="d0_s14" value="angled" value_original="angled" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="shape_or_vernation" notes="" src="d0_s14" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="middle" id="o30415" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o30416" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o30417" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 8-13 mm, strongly divergent at maturity, sometimes erect, twisted, flattened at the base, arising 1.5 mm or more below the lemma apices;</text>
      <biological_entity id="o30418" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="13" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="strongly" name="arrangement" src="d0_s15" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="twisted" value_original="twisted" />
        <character constraint="at base" constraintid="o30419" is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s15" value="arising" value_original="arising" />
        <character constraint="below lemma apices" constraintid="o30420" name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o30419" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity constraint="lemma" id="o30420" name="apex" name_original="apices" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 1-1.5 mm.</text>
      <biological_entity id="o30421" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses equaling or shorter than the paleas, thin, weakly inrolled or flat.</text>
      <biological_entity id="o30423" name="palea" name_original="paleas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 14.</text>
      <biological_entity id="o30422" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character is_modifier="false" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
        <character constraint="than the paleas" constraintid="o30423" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s17" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30424" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus japonicus grows in fields, waste places, and road verges. It is native to central and southeastern Europe and Asia, and is distributed throughout much of the United States and southern Canada, with one record from the Yukon Territory.</discussion>
  
</bio:treatment>