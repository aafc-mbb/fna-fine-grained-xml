<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Claus Badenf;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="genus">PSATHYROSTACHYS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus psathyrostachys</taxon_hierarchy>
  </taxon_identification>
  <number>13.19</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, forming dense to loose clumps, sometimes stoloniferous, sometimes rhizomatous.</text>
      <biological_entity id="o21165" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21166" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s1" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o21165" id="r3339" name="forming" negation="false" src="d0_s1" to="o21166" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-120 cm, erect or decumbent.</text>
      <biological_entity id="o21167" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths of the basal leaves closed, becoming fibrillose, of the upper cauline leaves open;</text>
      <biological_entity id="o21168" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="condition" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s3" value="fibrillose" value_original="fibrillose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21169" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="upper cauline" id="o21170" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o21168" id="r3340" name="part_of" negation="false" src="d0_s3" to="o21169" />
      <relation from="o21168" id="r3341" name="part_of" negation="false" src="d0_s3" to="o21170" />
    </statement>
    <statement id="d0_s4">
      <text>auricles sometimes present;</text>
      <biological_entity id="o21171" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.3 mm, membranous;</text>
      <biological_entity id="o21172" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades with prominently ribbed veins on the adaxial surfaces.</text>
      <biological_entity id="o21173" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o21174" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="prominently" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21175" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o21173" id="r3342" name="with" negation="false" src="d0_s6" to="o21174" />
      <relation from="o21174" id="r3343" name="on" negation="false" src="d0_s6" to="o21175" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences spikes, with 2-3 spikelets per node;</text>
      <biological_entity id="o21177" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o21178" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o21176" id="r3344" name="with" negation="false" src="d0_s7" to="o21177" />
      <relation from="o21177" id="r3345" name="per" negation="false" src="d0_s7" to="o21178" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation in the rachises.</text>
      <biological_entity constraint="inflorescences" id="o21176" name="spike" name_original="spikes" src="d0_s7" type="structure" />
      <biological_entity id="o21179" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <relation from="o21176" id="r3346" name="in" negation="false" src="d0_s8" to="o21179" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets appressed to ascending, with 1-2 (3) florets, often with additional reduced florets distally.</text>
      <biological_entity id="o21180" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
      <biological_entity id="o21181" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o21182" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="additional" value_original="additional" />
        <character is_modifier="true" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o21180" id="r3347" name="with" negation="false" src="d0_s9" to="o21181" />
      <relation from="o21180" id="r3348" modifier="often" name="with" negation="false" src="d0_s9" to="o21182" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes equal to unequal, (3.5) 4.2-48.5 (65) mm including the awns, subulate, stiff, scabrous to pubescent, obscurely 1-veined, not united at the base;</text>
      <biological_entity id="o21183" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s10" to="48.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s10" to="pubescent" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o21184" name="awn" name_original="awns" src="d0_s10" type="structure" />
      <biological_entity id="o21185" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o21183" id="r3349" name="including the" negation="false" src="d0_s10" to="o21184" />
      <relation from="o21183" id="r3350" name="united at the" negation="true" src="d0_s10" to="o21185" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 5.5-14.3 mm, narrowly elliptic, rounded, glabrous or pubescent, 5-7-veined, veins often prominent distally, apices sharply acute to awned, sometimes with a minute tooth on either side of the awn base, awns 0.8-34 mm, straight, ascending to slightly divergent, sometimes violet-tinged;</text>
      <biological_entity id="o21186" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="14.3" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o21187" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often; distally" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o21188" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="sharply acute" name="shape" src="d0_s11" to="awned" />
      </biological_entity>
      <biological_entity id="o21189" name="tooth" name_original="tooth" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o21190" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity constraint="awn" id="o21191" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o21192" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="34" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="slightly" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="violet-tinged" value_original="violet-tinged" />
      </biological_entity>
      <relation from="o21188" id="r3351" modifier="sometimes" name="with" negation="false" src="d0_s11" to="o21189" />
      <relation from="o21189" id="r3352" name="on" negation="false" src="d0_s11" to="o21190" />
      <relation from="o21190" id="r3353" name="part_of" negation="false" src="d0_s11" to="o21191" />
    </statement>
    <statement id="d0_s12">
      <text>paleas equaling or slightly longer than the lemmas, membranous, scabrous or pilose on and sometimes also between the keels, bifid;</text>
      <biological_entity id="o21193" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character constraint="than the lemmas" constraintid="o21194" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture_or_shape" notes="o21195." src="d0_s12" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o21194" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o21195" name="keel" name_original="keels" src="d0_s12" type="structure" />
      <biological_entity id="o21196.o21195." name="keel-keel" name_original="keels" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 3, 2.5-6.8 (7) mm, yellow or violet;</text>
      <biological_entity id="o21197" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="violet" value_original="violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lodicules 2, acute, entire, ciliate.</text>
      <biological_entity id="o21198" name="lodicule" name_original="lodicules" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses pubescent distally, tightly enclosed by the lemmas and paleas at maturity, x = 7.</text>
      <biological_entity id="o21199" name="caryopse" name_original="caryopses" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21200" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o21201" name="palea" name_original="paleas" src="d0_s15" type="structure" />
      <biological_entity constraint="x" id="o21202" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="7" value_original="7" />
      </biological_entity>
      <relation from="o21199" id="r3354" modifier="tightly" name="enclosed by the" negation="false" src="d0_s15" to="o21200" />
      <relation from="o21199" id="r3355" modifier="tightly" name="enclosed by the" negation="false" src="d0_s15" to="o21201" />
    </statement>
  </description>
  <discussion>Psathyrostachys has eight species, all of which are native to arid regions of central Asia, from eastern Turkey to eastern Siberia, Russia, and Xinjiang Province, China. One species, P. juncea, was introduced to North America as a potential forage species, and is now established in the Flora region.</discussion>
  <discussion>Psathyrostachys is very similar to Leymus, particularly the cespitose species of Leymus. The major differences are that Psathyrostachys has disarticulating rachises and, usually, distinctly awned lemmas.</discussion>
  <references>
    <reference>Anamthawat-Jonsson, K. 2005. The Leymus Ns-genome. Czech J. Genet. PI. Breed. «(Special Issue):13-20</reference>
    <reference>Baden, C. 1991. A taxonomic revision of Psathyrostachys (Poaceae). Nordic J. Bot 11:3-26</reference>
    <reference>Bodvarsdottir, S.K. and K. Anamthawat-Jonsson. 2003. Isolation, characterization, and analysis of Leymus-specific DNA sequences. Genome 46:673-682.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;Colo.;N.Mex.;Tex.;Utah;Alaska;Alta.;Man.;Sask.;Yukon;Mont.;Wyo.;Wash.;Ariz.;N.Dak.;Nebr.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>