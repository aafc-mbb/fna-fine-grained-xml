<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="V.L. Marsh ex Soreng" date="unknown" rank="section">Secundae</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section secundae</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually densely, infrequently loosely, tufted, rarely weakly rhizomatous or stoloniferous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mixed intra and extravaginal to completely intravaginal.</text>
      <biological_entity id="o19338" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="infrequently loosely; loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="rarely weakly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
        <character char_type="range_value" from="extravaginal" name="position" src="d0_s2" to="completely intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 10-120 cm, capillary to stout, terete or weakly compressed;</text>
      <biological_entity id="o19339" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="120" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete.</text>
      <biological_entity id="o19340" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/10-1/3 their length, terete, smooth or scabrous, distal sheaths usually longer than their blades;</text>
      <biological_entity id="o19341" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19342" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than their blades" constraintid="o19343" is_modifier="false" name="length_or_size" src="d0_s5" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o19343" name="blade" name_original="blades" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-7 (10) mm, smooth or scabrous, apices truncate to acuminate;</text>
      <biological_entity id="o19344" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19345" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 0.4-3 (5) mm wide, flat, folded, or involute, thin to moderately thick, soft and soon withering or moderately firm and persisting, smooth or scabrous mainly over the veins and margins, apices narrowly prow-shaped.</text>
      <biological_entity id="o19346" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="width" src="d0_s7" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="involute" value_original="involute" />
        <character char_type="range_value" from="thin" name="width" src="d0_s7" to="moderately thick" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="soon" name="life_cycle" src="d0_s7" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="moderately; moderately" name="texture" src="d0_s7" value="firm" value_original="firm" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character constraint="over margins" constraintid="o19348" is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19347" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o19348" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o19349" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 2-25 (30) cm, erect or somewhat lax, narrowly lanceoloid to ovoid, usually contracted, sometimes open and pyramidal, sparse to congested, with 7-100 (120) spikelets;</text>
      <biological_entity id="o19350" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="25" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character char_type="range_value" from="narrowly lanceoloid" name="shape" src="d0_s8" to="ovoid" />
        <character is_modifier="false" modifier="usually" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="count_or_density" src="d0_s8" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o19351" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="120" value_original="120" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s8" to="100" />
      </biological_entity>
      <relation from="o19350" id="r3070" name="with" negation="false" src="d0_s8" to="o19351" />
    </statement>
    <statement id="d0_s9">
      <text>nodes with 1-4 (7) branches;</text>
      <biological_entity id="o19352" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity id="o19353" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="7" value_original="7" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o19352" id="r3071" name="with" negation="false" src="d0_s9" to="o19353" />
    </statement>
    <statement id="d0_s10">
      <text>branches 0.5-15 cm, erect to spreading, terete, sulcate or angled, smooth or the angles sparsely to densely scabrous, sometimes scabrous between the angles.</text>
      <biological_entity id="o19354" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19355" name="angle" name_original="angles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
        <character constraint="between angles" constraintid="o19356" is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19356" name="angle" name_original="angles" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets (4) 4.5-10 mm, lengths 3-5 times widths, terete to weakly laterally compressed or distinctly compressed, sometimes bulbiferous;</text>
      <biological_entity id="o19357" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s11" value="3-5" value_original="3-5" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="weakly laterally compressed or distinctly compressed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets (2) 3-5 (10), usually normal and bisexual, sometimes bulb-forming.</text>
      <biological_entity id="o19358" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character name="atypical_quantity" src="d0_s12" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s12" value="10" value_original="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s12" value="normal" value_original="normal" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes lanceolate to broadly lanceolate, shorter than to subequal to the adjacent lemmas, keels indistinct to distinct, smooth or scabrous;</text>
      <biological_entity id="o19359" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="broadly lanceolate" />
        <character constraint="than to subequal to the adjacent lemmas" constraintid="o19360" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19360" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o19361" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o19362" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>calluses terete or slightly dorsally compressed, glabrous or with a crown of hairs, hairs to 2 mm;</text>
      <biological_entity id="o19363" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly dorsally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="with a crown" value_original="with a crown" />
      </biological_entity>
      <biological_entity id="o19364" name="crown" name_original="crown" src="d0_s15" type="structure" />
      <biological_entity id="o19365" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <biological_entity id="o19366" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o19363" id="r3072" name="with" negation="false" src="d0_s15" to="o19364" />
      <relation from="o19364" id="r3073" name="part_of" negation="false" src="d0_s15" to="o19365" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas 3-7 mm, narrowly lanceolate to lanceolate or slightly oblanceolate, weakly to distinctly keeled, glabrous or the keels and marginal veins and sometimes the lateral-veins with hairs, obscure, intercostal regions glabrous or with hairs;</text>
      <biological_entity id="o19367" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s16" to="lanceolate or slightly oblanceolate" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s16" to="lanceolate or slightly oblanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19368" name="keel" name_original="keels" src="d0_s16" type="structure" />
      <biological_entity constraint="marginal" id="o19369" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity constraint="marginal" id="o19370" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure" />
      <biological_entity id="o19371" name="hair" name_original="hairs" src="d0_s16" type="structure" />
      <relation from="o19368" id="r3074" name="with hairs , obscure , intercostal regions glabrous or with" negation="false" src="d0_s16" to="o19371" />
      <relation from="o19369" id="r3075" name="with hairs , obscure , intercostal regions glabrous or with" negation="false" src="d0_s16" to="o19371" />
      <relation from="o19370" id="r3076" name="with hairs , obscure , intercostal regions glabrous or with" negation="false" src="d0_s16" to="o19371" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 1.2-3.5 mm, sometimes aborted late in development.</text>
      <biological_entity id="o19372" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa sect. Secundae includes nine North American species. Two of the species also grow as disjuncts in South America. One species grows on high arctic islands in the Eastern Hemisphere. All the species tend to grow in arid areas, sometimes on wetlands within such areas. One species is confined to dry bluffs along the Pacific coast. All the species are primarily cespitose, but hybridization with members of Poa sect. Poa results in the formation of rhizomatous plants. Typically, members of sect. Secundae have sheaths that are closed for V10-1/4 their length, contracted panicles, and anthers that are 1.2-3.5 mm long.</discussion>
  <discussion>There are two subsections in the Flora region: subsects. Secundae and Halophytae.</discussion>
  
</bio:treatment>