<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Nannf. ex Tzvelev" date="unknown" rank="section">Abbreviatae</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="species">abbreviata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section abbreviatae;species abbreviata</taxon_hierarchy>
  </taxon_identification>
  <number>61</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not or scarcely glaucous;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely tufted, not stoloniferous, not rhizomatous.</text>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching all or mainly intravaginal.</text>
      <biological_entity id="o7420" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="scarcely" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s3" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 5-15 (20) cm, slender, leafless above the basal tuft.</text>
      <biological_entity id="o7421" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character constraint="above basal tuft" constraintid="o7422" is_modifier="false" name="architecture" src="d0_s4" value="leafless" value_original="leafless" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7422" name="tuft" name_original="tuft" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/10 – 1/4 their length, terete;</text>
      <biological_entity id="o7423" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.4-5.5 mm, milky white to hyaline, smooth or scabrous, apices truncate to acute;</text>
      <biological_entity id="o7424" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="milky white" name="coloration" src="d0_s6" to="hyaline" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7425" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 0.8-1.5 (2) mm wide, involute, moderately thick, soft, apices narrowly prow-shaped.</text>
      <biological_entity id="o7426" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="width" src="d0_s7" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o7427" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 1.5-5 cm, erect, lanceoloid to ovoid, contracted, congested;</text>
      <biological_entity id="o7428" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="lanceoloid" name="shape" src="d0_s8" to="ovoid" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nodes with 1-3 branches;</text>
      <biological_entity id="o7429" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity id="o7430" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <relation from="o7429" id="r1198" name="with" negation="false" src="d0_s9" to="o7430" />
    </statement>
    <statement id="d0_s10">
      <text>branches to 1.5 cm, erect, slender, terete, sulcate or angled, smooth or the angles sparsely scabrous;</text>
      <biological_entity id="o7431" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7432" name="angle" name_original="angles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels usually shorter than the spikelets.</text>
      <biological_entity id="o7433" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character constraint="than the spikelets" constraintid="o7434" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o7434" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 4-6.5 mm, laterally compressed, rarely bulbiferous, frequently strongly anthocyanic;</text>
      <biological_entity id="o7435" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s12" value="bulbiferous" value_original="bulbiferous" />
        <character is_modifier="false" modifier="frequently strongly" name="coloration" src="d0_s12" value="anthocyanic" value_original="anthocyanic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-5, rarely bulb-forming;</text>
      <biological_entity id="o7436" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes usually shorter than 1 mm, smooth or scabrous.</text>
      <biological_entity constraint="rachilla" id="o7437" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes subequal to slightly longer than the adjacent lemmas, lanceolate to broadly lanceolate, distinctly keeled, keels smooth;</text>
      <biological_entity id="o7438" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="than the adjacent lemmas" constraintid="o7439" from="subequal" name="size" src="d0_s15" to="slightly longer" value="subequal to slightly longer" value_original="subequal to slightly longer" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s15" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o7439" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o7440" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes (1) 3-veined, lateral-veins often faint and short;</text>
      <biological_entity constraint="lower" id="o7441" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="(1)3-veined" value_original="(1)3-veined" />
      </biological_entity>
      <biological_entity id="o7442" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s16" value="faint" value_original="faint" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes exceeding or exceeded by the upper florets;</text>
      <biological_entity constraint="upper" id="o7443" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s17" value="exceeding" value_original="exceeding" />
        <character name="position_relational" src="d0_s17" value="exceeded by the upper florets" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calluses glabrous or webbed;</text>
      <biological_entity id="o7444" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>lemmas 3-4.6 mm, lanceolate to broadly lanceolate, distinctly keeled, thin, keels and marginal veins usually short to long-villous, hairs extending along 3/4 - 5/6 of the keel, infrequently glabrous, intercostal regions glabrous or softly puberulent to short-villous, apices acute;</text>
      <biological_entity id="o7445" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s19" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="width" src="d0_s19" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o7446" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s19" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o7447" name="vein" name_original="veins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s19" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o7448" name="hair" name_original="hairs" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7449" name="keel" name_original="keel" src="d0_s19" type="structure">
        <character char_type="range_value" from="3/4" is_modifier="true" name="quantity" src="d0_s19" to="5/6" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o7450" name="region" name_original="regions" src="d0_s19" type="structure">
        <character char_type="range_value" from="softly puberulent" name="pubescence" src="d0_s19" to="short-villous" />
      </biological_entity>
      <biological_entity id="o7451" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o7448" id="r1199" name="extending along" negation="false" src="d0_s19" to="o7449" />
    </statement>
    <statement id="d0_s20">
      <text>palea keels scabrous, often short-villous to softly puberulent at midlength, sometimes glabrous;</text>
      <biological_entity constraint="palea" id="o7452" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" constraint="at midlength" constraintid="o7453" from="short-villous" modifier="often" name="pubescence" src="d0_s20" to="softly puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7453" name="midlength" name_original="midlength" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>anthers 0.2-1.2 (1.8) mm.</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 42.</text>
      <biological_entity id="o7454" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="atypical_some_measurement" src="d0_s21" unit="mm" value="1.8" value_original="1.8" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7455" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa abbreviata is an alpine and circumarctic species which has two subspecies in the western Cordilleras, and one in the high arctic. It grows mainly on frost scars and mesic rocky slopes, usually on open ground. In rare cases where the lemmas and calluses of P. abbreviata ate glabrous, it can be confused with P. lettermanii (p. 580), but that species has shorter spikelets and glumes that are longer than the adjacent florets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Alaska;Idaho;Nev.;Utah;Alta.;B.C.;Greenland;N.W.T.;Nunavut;Yukon;Mont.;Wyo.;Calif.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas glabrous; calluses webbed</description>
      <determination>Poa abbreviata subsp. marshii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas usually with hairs over the veins; calluses glabrous or webbed, rarely both the lemmas and calluses glabrous.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers 0.2-0.8 mm long; lemma intercostal regions hairy</description>
      <determination>Poa abbreviata subsp. abbreviata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers 0.6-1.2(1.8) mm long; lemma intercostal regions glabrous or hairy</description>
      <determination>Poa abbreviata subsp. pattersonii</determination>
    </key_statement>
  </key>
</bio:treatment>