<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">90</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">MELICA</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">torreyana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus melica;species torreyana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Torrey's melic</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose, not rhizomatous.</text>
      <biological_entity id="o25103" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-100 cm, not forming corms;</text>
      <biological_entity id="o25104" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25105" name="corm" name_original="corms" src="d0_s1" type="structure" />
      <relation from="o25104" id="r3965" name="forming" negation="true" src="d0_s1" to="o25105" />
    </statement>
    <statement id="d0_s2">
      <text>internodes smooth.</text>
      <biological_entity id="o25106" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or sparsely pilose, sometimes pilose only at the throat, sometimes scabridulous;</text>
      <biological_entity id="o25107" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character constraint="at throat" constraintid="o25108" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="relief" notes="" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o25108" name="throat" name_original="throat" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-5 mm;</text>
      <biological_entity id="o25109" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-2.5 mm wide, sometimes pilose on both surfaces, sometimes scabridulous.</text>
      <biological_entity id="o25110" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o25111" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="relief" notes="" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o25111" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 6-25 cm;</text>
      <biological_entity id="o25112" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches 1-5 cm, usually appressed, occasionally divergent, with 5-37 spikelets;</text>
      <biological_entity id="o25113" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="occasionally" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o25114" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="37" />
      </biological_entity>
      <relation from="o25113" id="r3966" name="with" negation="false" src="d0_s7" to="o25114" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels straight;</text>
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o25115" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o25116" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o25115" id="r3967" name="above" negation="false" src="d0_s9" to="o25116" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3.5-7 mm, with 1 (2) bisexual florets.</text>
      <biological_entity id="o25117" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25118" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o25117" id="r3968" name="with" negation="false" src="d0_s10" to="o25118" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes 3-5 mm long, about 1 mm wide, 1-5-veined;</text>
      <biological_entity constraint="lower" id="o25119" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-5-veined" value_original="1-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3.3-7 mm long, 1-2 mm wide, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o25120" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 3.5-6 mm, scabridulous, sometimes hairy, distal hairs longer than those below, 7-veined, veins inconspicuous, apices rounded to emarginate, unawned or awned, awns 1-2 mm;</text>
      <biological_entity id="o25121" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25122" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity id="o25123" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o25124" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded to emarginate" value_original="rounded to emarginate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o25125" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas slightly shorter than the lemmas;</text>
      <biological_entity id="o25126" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="than the lemmas" constraintid="o25127" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o25127" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-2.5 mm;</text>
      <biological_entity id="o25128" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>rudiments 0.5-4 mm, clearly distinct from the bisexual florets, shorter than the terminal rachilla internode, truncate to acute.</text>
      <biological_entity id="o25130" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity constraint="rachilla" id="o25131" name="internode" name_original="internode" src="d0_s16" type="structure" constraint_original="terminal rachilla" />
    </statement>
    <statement id="d0_s17">
      <text>2n =18.</text>
      <biological_entity id="o25129" name="rudiment" name_original="rudiments" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character constraint="from florets" constraintid="o25130" is_modifier="false" modifier="clearly" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character constraint="than the terminal rachilla internode" constraintid="o25131" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s16" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s16" to="acute" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25132" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melica torreyana grows from sea level to 1200 m, in thickets and woods in California. It is common throughout chaparral areas and coniferous forests but, on serpentine soils, grows only in shady locations. The shape and size of the rudiments make M. torreyana unique among the species found in North America. Boyle (1945) obtained vigorous, almost completely sterile hybrids between M. imperfecta and M. torreyana, but found no examples of natural hybrids.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>