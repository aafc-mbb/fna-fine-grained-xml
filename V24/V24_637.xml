<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert I. Lonard;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="C.C. Gmel." date="unknown" rank="genus">VULPIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus vulpia</taxon_hierarchy>
  </taxon_identification>
  <number>14.04</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually annual, rarely perennial.</text>
      <biological_entity id="o14296" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-90 cm, erect or ascending from a decumbent base, usually glabrous.</text>
      <biological_entity id="o14297" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from base" constraintid="o14298" is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14298" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths open, usually glabrous;</text>
      <biological_entity id="o14299" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="open" value_original="open" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles absent;</text>
      <biological_entity id="o14300" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules usually shorter than 1 mm, membranous, usually truncate, ciliate;</text>
      <biological_entity id="o14301" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat or rolled, glabrous or pubescent.</text>
      <biological_entity id="o14302" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rolled" value_original="rolled" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences panicles or racemes, sometimes spikelike, usually with more than 1 spikelet associated with each node;</text>
      <biological_entity constraint="inflorescences" id="o14303" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="inflorescences" id="o14304" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o14305" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14306" name="node" name_original="node" src="d0_s6" type="structure" />
      <relation from="o14303" id="r2270" modifier="usually" name="with" negation="false" src="d0_s6" to="o14305" />
      <relation from="o14304" id="r2271" modifier="usually" name="with" negation="false" src="d0_s6" to="o14305" />
      <relation from="o14305" id="r2272" name="associated with each" negation="false" src="d0_s6" to="o14306" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1-3 per node, appressed or spreading, usually glabrous, scabrous.</text>
      <biological_entity id="o14307" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per node" constraintid="o14308" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o14308" name="node" name_original="node" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets pedicellate, laterally compressed, with 1-11 (17) florets, distal florets reduced;</text>
      <biological_entity id="o14309" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o14310" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="17" value_original="17" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
      <relation from="o14309" id="r2273" name="with" negation="false" src="d0_s8" to="o14310" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes and beneath the florets, occasionally also at the base of the pedicels.</text>
      <biological_entity constraint="distal" id="o14311" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o14312" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o14313" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o14314" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o14311" id="r2274" name="above the glumes and beneath" negation="false" src="d0_s9" to="o14312" />
      <relation from="o14313" id="r2275" name="part_of" negation="false" src="d0_s9" to="o14314" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes shorter than the adjacent lemmas, subulate to lanceolate, apices acute to acuminate, unawned or awn-tipped;</text>
      <biological_entity id="o14315" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="than the adjacent lemmas" constraintid="o14316" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o14316" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o14317" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes much shorter than the upper glumes, 1-veined;</text>
      <biological_entity constraint="lower" id="o14318" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="than the upper glumes" constraintid="o14319" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14319" name="glume" name_original="glumes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o14320" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachillas terminating in a reduced floret;</text>
      <biological_entity id="o14321" name="rachilla" name_original="rachillas" src="d0_s13" type="structure" />
      <biological_entity id="o14322" name="floret" name_original="floret" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o14321" id="r2276" name="terminating in a" negation="false" src="d0_s13" to="o14322" />
    </statement>
    <statement id="d0_s14">
      <text>calluses blunt, glabrous;</text>
      <biological_entity id="o14323" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas membranous, lanceolate, 3-5-veined, veins converging distally, margins involute over the edges of the caryopses, apices entire, acute to acuminate, mucronate or awned;</text>
      <biological_entity id="o14324" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o14325" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s15" value="converging" value_original="converging" />
      </biological_entity>
      <biological_entity id="o14326" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character constraint="over edges" constraintid="o14327" is_modifier="false" name="shape_or_vernation" src="d0_s15" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o14327" name="edge" name_original="edges" src="d0_s15" type="structure" />
      <biological_entity id="o14328" name="caryopse" name_original="caryopses" src="d0_s15" type="structure" />
      <biological_entity id="o14329" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="acuminate mucronate or awned" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="acuminate mucronate or awned" />
      </biological_entity>
      <relation from="o14327" id="r2277" name="part_of" negation="false" src="d0_s15" to="o14328" />
    </statement>
    <statement id="d0_s16">
      <text>paleas usually slightly shorter than to equaling the lemmas, sometimes longer;</text>
      <biological_entity id="o14330" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character constraint="than to equaling the lemmas" constraintid="o14331" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="usually slightly shorter" value_original="usually slightly shorter" />
        <character is_modifier="false" modifier="sometimes" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14331" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers usually 1, rarely 3 in chasmogamous specimens.</text>
      <biological_entity id="o14332" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character constraint="in specimens" constraintid="o14333" modifier="rarely" name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o14333" name="specimen" name_original="specimens" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses shorter than the lemmas, concealed at maturity, elongate, dorsally compressed, curved in cross-section, falling with the lemma and palea.</text>
      <biological_entity id="o14335" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o14336" name="cross-section" name_original="cross-section" src="d0_s18" type="structure" />
      <biological_entity id="o14337" name="lemma" name_original="lemma" src="d0_s18" type="structure" />
      <biological_entity id="o14338" name="paleum" name_original="palea" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>x = 7.</text>
      <biological_entity id="o14334" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character constraint="than the lemmas" constraintid="o14335" is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="shorter" value_original="shorter" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s18" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character constraint="in cross-section" constraintid="o14336" is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
        <character constraint="with palea" constraintid="o14338" is_modifier="false" name="life_cycle" notes="" src="d0_s18" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o14339" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vulpia, a genus of 30 species, is most abundant in Europe and the Mediterranean region (Cotton and Stace 1967). The Flora region has three native and three introduced species. Most species, including ours, are weedy, cleistogamous annuals, usually having one anther per floret. Festuca, in which Vulpia is sometimes included, consists of chasmogamous species having three anthers per floret. The two genera are closely related to each other. Sterile hybrids between Vulpia and Festuca, and Vulpia and Lolium, are known.</discussion>
  <references>
    <reference>Cotton, R. and C.A. Stace. 1967. Taxonomy of the genus Vulpia (Gramineae): I. Chromosome numbers and geographical distribution of the Old World species. Genetica 46:235-255</reference>
    <reference>Lonard, R.I. and F.W. Gould. 1974. The North American species of Vulpia (Gramineae). Madrono 22:217-230</reference>
    <reference>Stace, C.A. 1975. Wild hybrids in the British flora. Pp. 111-125 in S.M. Walters (ed.). European Floristic and Taxonomic Studies. E.W. Classey, Faringdon, England. 144 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Fla.;S.Dak.;Wyo.;Wash.;Ariz.;Colo.;Ga.;Iowa;Idaho;Maine;Mich.;Miss.;Nebr.;Nev.;Okla.;Pa.;S.C.;Wis.;W.Va.;Del.;D.C;Pacific Islands (Hawaii);Kans.;Minn.;N.Dak.;Mass.;N.H.;R.I.;Vt.;N.Mex.;Tex.;La.;Oreg.;N.C.;Tenn.;Calif.;Puerto Rico;Alaska;Ala.;Va.;Ark.;Ill.;Ind.;Md.;Ohio;Utah;Mo.;Mont.;Ky.;Alta.;B.C.;Man.;N.W.T.;Ont.;Que.;Sask.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <discussion>In the key and descriptions, the spikelet and lemma measurements exclude the awns.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes less than 1/2 the length of the upper glumes.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 5-veined, glabrous except the margins sometimes ciliate; rachilla internodes 0.75-1.9 mm long</description>
      <determination>1 Vulpia myuros</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 3(5)-veined, pubescent or glabrous, the margins ciliate; rachilla internodes 0.4-0.9 mm long</description>
      <determination>6 Vulpia ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes 1/2 or more the length of the upper glumes.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas 2.5-3.5 mm long, the apices more pubescent than the bases; caryopses 1.5-2.5 mm long</description>
      <determination>2 Vulpia sciurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas 2.7-9.5 mm long, if pubescent, the apices no more so than the bases but occasionally ciliate; caryopses 1.7-6.5 mm long.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicle branches 1-2 per node; spikelets with 4-17 florets; rachilla internodes 0.5-0.7 mm long; awn of the lowermost lemma in each spikelet 0.3-9 mm long; caryopses 1.7-3.7 mm long</description>
      <determination>3 Vulpia octoflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicle branches solitary; spikelets with 1-8 florets; rachilla internodes 0.6-1.2 mm long; awn of the lowermost lemma in each spikelet 2-20 mm long; caryopses 3.5-6.5 mm long.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicle branches appressed to erect at maturity, without axillary pulvini; paleas equal to or shorter than the lemmas</description>
      <determination>4 Vulpia bromoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicle branches spreading to reflexed at maturity, with axillary pulvini; paleas usually slightly longer than the lemmas</description>
      <determination>5 Vulpia microstachys</determination>
    </key_statement>
  </key>
</bio:treatment>