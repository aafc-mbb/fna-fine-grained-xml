<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">599</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">arida</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;species arida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">glaucifolia</taxon_name>
    <taxon_hierarchy>genus poa;species glaucifolia</taxon_hierarchy>
  </taxon_identification>
  <number>73</number>
  <other_name type="common_name">Plains bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>glaucous or not;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely to loosely tufted or the culms solitary, rhizomatous.</text>
      <biological_entity id="o5961" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching intra and extravaginal.</text>
      <biological_entity id="o5962" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s3" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 15-80 cm, erect or the bases decumbent, terete or weakly compressed;</text>
      <biological_entity id="o5963" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s4" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o5964" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nodes terete, 0-1 exserted.</text>
      <biological_entity id="o5965" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="1" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sheaths closed for 1/10 – 1/5 (1/4) their length, terete, smooth or sparsely scabrous, glabrous, bases of basal sheaths glabrous, distal sheath lengths (1.2) 1.5-9 (20) times blade lengths;</text>
      <biological_entity id="o5966" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5967" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5968" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o5969" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character constraint="blade" constraintid="o5970" is_modifier="false" name="length" src="d0_s6" value="(1.2)1.5-9(20) times blade lengths" value_original="(1.2)1.5-9(20) times blade lengths" />
      </biological_entity>
      <biological_entity id="o5970" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <relation from="o5967" id="r959" name="part_of" negation="false" src="d0_s6" to="o5968" />
    </statement>
    <statement id="d0_s7">
      <text>ligules (1) 1.5-4 (5) mm, smooth or sparsely to moderately scabrous, apices obtuse to acute;</text>
      <biological_entity id="o5971" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5972" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades strongly to gradually reduced in length distally, 1.5-5 mm wide, flat and moderately thin to folded and moderately thick and firm, abaxial surfaces smooth, adaxial surfaces smooth or sparsely to moderately scabrous, primarily over the veins, apices narrowly prow-shaped, flag leaf-blades (0.4) 1-7 (10) cm.</text>
      <biological_entity id="o5973" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly to gradually; distally" name="length" src="d0_s8" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s8" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5974" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5975" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5976" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o5977" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o5978" name="blade-leaf" name_original="leaf-blades" src="d0_s8" type="structure">
        <character name="atypical_distance" src="d0_s8" unit="cm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s8" to="7" to_unit="cm" />
      </biological_entity>
      <relation from="o5975" id="r960" modifier="primarily" name="over" negation="false" src="d0_s8" to="o5976" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles (2.5) 4-12 (18) cm, erect, usually narrowly lanceoloid, contracted, sometimes interrupted, infrequently loosely contracted, usually congested, with 25-100 spikelets;</text>
      <biological_entity id="o5979" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="12" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s9" value="lanceoloid" value_original="lanceoloid" />
        <character is_modifier="false" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="infrequently loosely" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s9" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o5980" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
      <relation from="o5979" id="r961" name="with" negation="false" src="d0_s9" to="o5980" />
    </statement>
    <statement id="d0_s10">
      <text>nodes with 1-5 branches;</text>
      <biological_entity id="o5981" name="node" name_original="nodes" src="d0_s10" type="structure" />
      <biological_entity id="o5982" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o5981" id="r962" name="with" negation="false" src="d0_s10" to="o5982" />
    </statement>
    <statement id="d0_s11">
      <text>branches 1-9 cm, erect to infrequently ascending, rarely spreading, terete to weakly angled, smooth or the angles sparsely to moderately scabrous, with 3-24 spikelets.</text>
      <biological_entity id="o5983" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="9" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s11" to="infrequently ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="weakly angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5984" name="angle" name_original="angles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5985" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="24" />
      </biological_entity>
      <relation from="o5984" id="r963" name="with" negation="false" src="d0_s11" to="o5985" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3.2-7 mm, lengths to 3.5 (3.8) times widths, laterally compressed;</text>
      <biological_entity id="o5986" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="0-3.5(3.8)" value_original="0-3.5(3.8)" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-7;</text>
      <biological_entity id="o5987" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes smooth, sometimes sparsely puberulent.</text>
      <biological_entity constraint="rachilla" id="o5988" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes lanceolate, distinctly keeled, smooth or sparsely scabrous;</text>
      <biological_entity id="o5989" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o5990" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses usually glabrous, infrequently webbed, hairs to 1/4 the lemma length;</text>
      <biological_entity id="o5991" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s17" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o5992" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="1/4" />
      </biological_entity>
      <biological_entity id="o5993" name="lemma" name_original="lemma" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lemmas 2.5-4.5 mm, lanceolate to narrowly lanceolate, distinctly to weakly keeled, keels and marginal veins short to long-villous, lateral-veins moderately prominent, glabrous or puberulent, intercostal regions usually glabrous, infrequently hairy, hairs to 0.3 mm, margins glabrous, apices acute or blunt;</text>
      <biological_entity id="o5994" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s18" to="narrowly lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s18" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o5995" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o5996" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o5997" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o5998" name="region" name_original="regions" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5999" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s18" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6000" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6001" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s18" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>palea keels scabrous, glabrous or short-villous at midlength, intercostal regions usually glabrous, sometimes puberulent to short-villous;</text>
      <biological_entity constraint="palea" id="o6002" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character constraint="at intercostal regions" constraintid="o6003" is_modifier="false" name="pubescence" src="d0_s19" value="short-villous" value_original="short-villous" />
        <character char_type="range_value" from="puberulent" modifier="sometimes" name="pubescence" notes="" src="d0_s19" to="short-villous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o6003" name="region" name_original="regions" src="d0_s19" type="structure">
        <character is_modifier="true" name="position" src="d0_s19" value="midlength" value_original="midlength" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 1.3-2.2 mm. 2n = 56, 56+1, 56-58, 63, 64, 70, 76, 84, ca. 90, 95+-5, 100, 103.</text>
      <biological_entity id="o6004" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s20" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6005" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="56" value_original="56" />
        <character char_type="range_value" from="56" name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" upper_restricted="false" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="1" value_original="1" />
        <character char_type="range_value" from="56" name="quantity" src="d0_s20" to="58" unit=",+,-8,,,,,,,-,00,03" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="63" value_original="63" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="64" value_original="64" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="70" value_original="70" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="76" value_original="76" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="84" value_original="84" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="90" value_original="90" />
        <character char_type="range_value" from="95" name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" upper_restricted="false" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="5" value_original="5" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="100" value_original="100" />
        <character name="quantity" src="d0_s20" unit=",+,-8,,,,,,,-,00,03" value="103" value_original="103" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa arida grows mainly on the eastern slope of the Rocky Mountains and in the northern Great Plains, primarily in riparian habitats of varying salinity or alkalinity. It is spreading eastward along heavily salted highway corridors. Reports of its occurrence west of the Continental Divide and in southwestern Texas are mostly attributable to misidentifications of P. arctica subsp. aperta (p. 530). P. arctica subsp. grayana (p. 532), and rhizomatous specimens of P. fendleriana (p. 556).</discussion>
  <discussion>Poa arida may reflect past hybridization between P. secunda (p. 586) and a species of Poa sect. Poa. Poa glaucifolia Scribn. &amp; T.A. Williams refers to specimens of the northern Great Plains that have a more lax growth form with broader leaves and occasionally somewhat open panicles, florets with a small web, and sometimes lacking hairs between the keel and marginal veins of the lemma. Plants with these characteristics have chromosome counts of 2ra = 56 and 70, whereas P. arida sensu stricto usually has 2n = 63, 64, or greater than 70. It is suspected that some of the variability reflects introgression from P. secunda.</discussion>
  <discussion>Poa arida is a named intersectional hybrid</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill.;Ind.;N.J.;Colo.;N.Mex.;Tex.;Ohio;Minn.;Mich.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Mont.;Alta.;Man.;Ont.;Que.;Sask.;Iowa;Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>