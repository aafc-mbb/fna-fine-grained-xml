<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">83</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">GLYCERIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Glyceria</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">septentrionalis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">septentrionalis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus glyceria;section glyceria;species septentrionalis;variety septentrionalis</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Ligules 5-10 mm;</text>
      <biological_entity id="o23140" name="ligule" name_original="ligules" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades 2-15 mm wide.</text>
      <biological_entity id="o23141" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Panicle branches 3-17 cm, with 1-9 spikelets.</text>
      <biological_entity constraint="panicle" id="o23142" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="17" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23143" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="9" />
      </biological_entity>
      <relation from="o23142" id="r3682" name="with" negation="false" src="d0_s2" to="o23143" />
    </statement>
    <statement id="d0_s3">
      <text>Rachilla internodes 1.1-1.8 mm.</text>
      <biological_entity constraint="rachilla" id="o23144" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Lemmas scabrous over the veins, prickles about 0.05 mm, scabrous or scabridulous between the veins, apices almost truncate to obtuse or acute, apical margins crenate to entire.</text>
      <biological_entity id="o23145" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character constraint="over veins" constraintid="o23146" is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o23146" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o23147" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.05" value_original="0.05" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character constraint="between veins" constraintid="o23148" is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23148" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o23149" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2n = 40.</text>
      <biological_entity constraint="apical" id="o23150" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="entire" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23151" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Glyceria septentrionalis var. septentrionalis grows throughout the range of the species, but is less common in the lower floodplain of the Mississippi River and Kentucky than var. arkansana. It is found in shallow water or wet soils. In reviewing specimens for this treatment, some were found to have acute lemmas that usually exceeded the paleas, and lemma midveins that were clearly longer than the other veins; others had truncate to obtuse lemmas that were usually shorter than or equaling the paleas, and lemma midveins that were barely longer than the lateral veins. Further study is needed to determine whether the two kinds merit separate recognition.</discussion>
  
</bio:treatment>