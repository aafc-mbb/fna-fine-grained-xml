<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">DESCHAMPSIA</taxon_name>
    <taxon_name authority="(L.) Trin." date="unknown" rank="species">flexuosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus deschampsia;species flexuosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lerchenfeldia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flexuosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">montana</taxon_name>
    <taxon_hierarchy>genus lerchenfeldia;species flexuosa;subspecies montana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aira</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flexuosa</taxon_name>
    <taxon_hierarchy>genus aira;species flexuosa</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Crinkled hairgrass</other_name>
  <other_name type="common_name">Wavy hairgrass</other_name>
  <other_name type="common_name">Deschampsie flexueuse</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose.</text>
      <biological_entity id="o7590" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-80 cm, erect or geniculate at the base, usually with 2 nodes.</text>
      <biological_entity id="o7591" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o7592" is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o7592" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o7593" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7591" id="r1218" modifier="usually" name="with" negation="false" src="d0_s2" to="o7593" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, sometimes forming a basal tuft;</text>
      <biological_entity id="o7594" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7595" name="tuft" name_original="tuft" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7596" name="tuft" name_original="tuft" src="d0_s3" type="structure" />
      <relation from="o7594" id="r1219" modifier="sometimes" name="forming a" negation="false" src="d0_s3" to="o7596" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths smooth, glabrous;</text>
      <biological_entity id="o7597" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1.5-3.6 mm, rounded to acute;</text>
      <biological_entity id="o7598" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 12-25 cm long, strongly rolled, 0.3-0.5 mm in diameter, abaxial surfaces smooth or scabridulous, glabrous or hairy, often scabridulous or hairy proximally and essentially smooth and glabrous distally, adaxial surfaces scabrous, flag leaf-blades 5-8 cm.</text>
      <biological_entity id="o7599" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s6" value="rolled" value_original="rolled" />
        <character char_type="range_value" constraint="in abaxial surfaces" constraintid="o7600" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="essentially" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7600" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7601" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7602" name="blade-leaf" name_original="leaf-blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="distance" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-15 cm long, (2) 4-12 cm wide, narrow to open, often nodding;</text>
      <biological_entity id="o7603" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches ascending to spreading, flexuous, smooth or scabridulous.</text>
      <biological_entity id="o7604" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-7 mm, ovate or U-shaped.</text>
      <biological_entity id="o7605" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="u--shaped" value_original="u--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes exceeded by or subequal to the adjacent florets, 1-veined, acute;</text>
      <biological_entity id="o7606" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="to florets" constraintid="o7607" is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7607" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 2.7-4.5 mm;</text>
      <biological_entity constraint="lower" id="o7608" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3.5-5 mm;</text>
      <biological_entity constraint="upper" id="o7609" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs to 1 mm;</text>
      <biological_entity constraint="callus" id="o7610" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3.3-5 mm, scabridulous or puberulent, hairs to 0.1 mm, apices acute, erose to 4-toothed, awns 3.7-7 mm, attached near the base of the lemma, strongly geniculate, geniculation below the lemma apices, distal segment 2.5-4.5 mm, pale;</text>
      <biological_entity id="o7611" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7612" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7613" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s14" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-toothed" value_original="4-toothed" />
      </biological_entity>
      <biological_entity id="o7614" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character constraint="near base" constraintid="o7615" is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o7615" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o7616" name="lemma" name_original="lemma" src="d0_s14" type="structure" />
      <biological_entity constraint="lemma" id="o7617" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity constraint="distal" id="o7618" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
      <relation from="o7615" id="r1220" name="part_of" negation="false" src="d0_s14" to="o7616" />
      <relation from="o7614" id="r1221" name="below" negation="false" src="d0_s14" to="o7617" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2-3 mm. 2n = 14, 26, 28, 32, 42.</text>
      <biological_entity id="o7619" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7620" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
        <character name="quantity" src="d0_s15" value="32" value_original="32" />
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Deschampsia flexuosa grows on dry, often rocky slopes, and in woods and thickets, often in disturbed sites. In the Flora region, it is primarily eastern in distribution, with records from west of the Great Lakes and Appalachians probably being introductions. It is also known from Mexico, Central America, South America, Borneo, the Philippines, and New Zealand.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;D.C.;Wis.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;N.C.;N.Dak.;Tenn.;Pa.;B.C.;Greenland;N.B.;Nfld. and Labr.;N.S.;Ont.;P.E.I.;Que.;Va.;Alaska;Ala.;Ark.;Ga.;Okla.;Md.;Mich.;Minn.;Ohio;S.C.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>