<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="J. Presl" date="unknown" rank="genus">PIPTOCHAETIUM</taxon_name>
    <taxon_name authority="(Trin.) Arechav." date="unknown" rank="species">setosum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus piptochaetium;species setosum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Bristly ricegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-40 cm, prostrate to ascending;</text>
      <biological_entity id="o14546" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodes 2, dark, glabrous.</text>
      <biological_entity id="o14547" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark" value_original="dark" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous, smooth;</text>
      <biological_entity id="o14548" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.5-2.5 mm, obtuse, membranous, glabrous;</text>
      <biological_entity id="o14549" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (3) 5-12.5 cm long, 0.8-1.5 mm wide, glabrous or hispidulous, margins scabridulous.</text>
      <biological_entity id="o14550" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o14551" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 3-15 cm long, 2-3 cm wide, with (5) 10-30 spikelets;</text>
      <biological_entity id="o14552" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14553" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s5" to="30" />
      </biological_entity>
      <relation from="o14552" id="r2310" name="with" negation="false" src="d0_s5" to="o14553" />
    </statement>
    <statement id="d0_s6">
      <text>branches appressed to ascending, glabrous or hispid;</text>
      <biological_entity id="o14554" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s6" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels 2-6 mm, hispidulous.</text>
      <biological_entity id="o14555" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes subequal, 5-7 mm long, 1-2 mm wide, purplish at the base;</text>
      <biological_entity id="o14556" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character constraint="at base" constraintid="o14557" is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o14557" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>lower glumes (3) 5 (7) -veined;</text>
      <biological_entity constraint="lower" id="o14558" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(3)5(7)-veined" value_original="(3)5(7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 5 (7) -veined;</text>
      <biological_entity constraint="upper" id="o14559" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5(7)-veined" value_original="5(7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2.5-3 mm long, 1.2-1.8 mm thick, globose to slightly laterally compressed, gibbous;</text>
      <biological_entity id="o14560" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="thickness" src="d0_s11" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s11" to="slightly laterally compressed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses 0.2-0.5 mm, obtuse, antrorsely strigose, hairs whitish to golden;</text>
      <biological_entity id="o14561" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o14562" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s12" to="golden" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas glabrous, longitudinally striate, constricted below the crown, chestnut-brown at maturity;</text>
      <biological_entity id="o14563" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="longitudinally" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
        <character constraint="below crown" constraintid="o14564" is_modifier="false" name="size" src="d0_s13" value="constricted" value_original="constricted" />
        <character constraint="at maturity" is_modifier="false" name="coloration" notes="" src="d0_s13" value="chestnut-brown" value_original="chestnut-brown" />
      </biological_entity>
      <biological_entity id="o14564" name="crown" name_original="crown" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>crowns 0.5-0.8 mm wide, straight, not strongly differentiated, distal margins papillose;</text>
      <biological_entity id="o14565" name="crown" name_original="crowns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not strongly" name="variability" src="d0_s14" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14566" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 10-16 mm, once or twice-geniculate;</text>
      <biological_entity id="o14567" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="16" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2 times-geniculate" value_original="2 times-geniculate " />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas to 3.2 mm;</text>
      <biological_entity id="o14568" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers about 0.5 mm.</text>
      <biological_entity id="o14569" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 2-2.5 mm, spherical to ellipsoid.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o14570" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="spherical" name="shape" src="d0_s18" to="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14571" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Piptochaetium setosum is native to central Chile. There is an established population in Marin County, California, that grows intermingled with P. stipoides, another South American species. The two species grow in the middle of a dirt track and in the adjacent meadow. The California plants of P. setosum are notable for their prostrate culms. This characteristic was not mentioned by Parodi (1944) or Cialdella and Arriaga (1998).</discussion>
  <discussion>The origin of the California population is not known. It has been suggested that the seeds might have been brought in by birds, as the area was a bird refuge at one time.</discussion>
  
</bio:treatment>