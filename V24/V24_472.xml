<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="J.R. Carlson &amp; Barkworth" date="unknown" rank="species">wawawaiensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species wawawaiensis</taxon_hierarchy>
  </taxon_identification>
  <number>33</number>
  <other_name type="common_name">Snakeriver wheatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, sometimes weakly rhizomatous.</text>
      <biological_entity id="o13357" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes weakly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (15) 50-130 cm, erect, mostly glabrous;</text>
      <biological_entity id="o13358" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="130" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually glab¬rous, sometimes slightly pubescent.</text>
      <biological_entity id="o13359" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes slightly" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves more or less evenly distributed;</text>
      <biological_entity id="o13360" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal sheaths glabrate, margins not evidently ciliate;</text>
      <biological_entity constraint="basal" id="o13361" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o13362" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not evidently" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles absent or to 1.2 mm;</text>
      <biological_entity id="o13363" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="0-1.2 mm" value_original="0-1.2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.1-1.1 mm;</text>
      <biological_entity id="o13364" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades to 28 cm long, 1.7-5 mm wide, involute when dry, adaxial surfaces usually densely pubescent, rarely sparsely pubescent.</text>
      <biological_entity id="o13365" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s7" to="28" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="when dry" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13366" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikes 5-20 cm long, 2.5-3 cm wide including the awns, erect to slightly nodding, with 1 spikelet per node;</text>
      <biological_entity id="o13367" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="20" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="slightly nodding" />
      </biological_entity>
      <biological_entity id="o13368" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o13369" name="spikelet" name_original="spikelet" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o13370" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o13367" id="r2131" name="including the" negation="false" src="d0_s8" to="o13368" />
      <relation from="o13367" id="r2132" name="with" negation="false" src="d0_s8" to="o13369" />
      <relation from="o13369" id="r2133" name="per" negation="false" src="d0_s8" to="o13370" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 5-12 mm long, about 0.2 mm thick, about 0.3 mm wide, glabrous beneath the spikelets.</text>
      <biological_entity id="o13371" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character name="thickness" src="d0_s9" unit="mm" value="0.2" value_original="0.2" />
        <character name="width" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
        <character constraint="beneath spikelets" constraintid="o13372" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13372" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 10-22 mm long, about twice as long as the internodes, 2-8.5 mm wide, appressed, with 4-10 florets;</text>
      <biological_entity id="o13373" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character constraint="internode" constraintid="o13374" is_modifier="false" name="length" src="d0_s10" value="2 times as long as the internodes" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o13374" name="internode" name_original="internodes" src="d0_s10" type="structure" />
      <biological_entity id="o13375" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <relation from="o13373" id="r2134" name="with" negation="false" src="d0_s10" to="o13375" />
    </statement>
    <statement id="d0_s11">
      <text>rachillas glabrous;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes, beneath each floret.</text>
      <biological_entity id="o13376" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13377" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o13378" name="floret" name_original="floret" src="d0_s12" type="structure" />
      <relation from="o13376" id="r2135" name="above" negation="false" src="d0_s12" to="o13377" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes 4-10 mm long, 0.5-1.3 mm wide, narrowly lanceolate, widest at or below midlength, glabrous, often glaucous, 1-3-veined, flat or weakly keeled, margins 0.1-0.2 mm wide, widest near midlength, apices usually acuminate, awned or unawned, awns to 6 mm;</text>
      <biological_entity id="o13379" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character constraint="at midlength" constraintid="o13380" is_modifier="false" name="width" src="d0_s13" value="widest" value_original="widest" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s13" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o13380" name="midlength" name_original="midlength" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="below midlength" name="position" src="d0_s13" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o13381" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s13" to="0.2" to_unit="mm" />
        <character constraint="near midlength" constraintid="o13382" is_modifier="false" name="width" src="d0_s13" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o13382" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
      <biological_entity id="o13383" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
        <character name="architecture_or_shape" src="d0_s13" value="unawned" value_original="unawned" />
      </biological_entity>
      <biological_entity id="o13384" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 6-12 mm, smooth or slightly scabrous, margins often sparsely pubescent proximally, apices awned, longest awns in the spikelets 9-28 mm, strongly divergent;</text>
      <biological_entity id="o13385" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o13386" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often sparsely; proximally" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13387" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="longest" id="o13388" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s14" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o13389" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="28" to_unit="mm" />
      </biological_entity>
      <relation from="o13388" id="r2136" name="in" negation="false" src="d0_s14" to="o13389" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 7.2-10.5 mm, keels scabrous distally, tapering to the 0.2-0.3 mm wide apices;</text>
      <biological_entity id="o13390" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="7.2" from_unit="mm" name="some_measurement" src="d0_s15" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13391" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character constraint="to apices" constraintid="o13392" is_modifier="false" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o13392" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3.5-6 mm. 2n = 28.</text>
      <biological_entity id="o13393" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13394" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus wawawaiensis grows primarily in shallow, rocky soils of slopes in coulees and reaches of the Salmon, Snake, and Yakima rivers of Washington, northern Oregon, and Idaho. There are also a few records from localities at some distance from the Snake River and its tributaries. These probably reflect deliberate introductions. C.V. Piper, who worked for the U.S. Department of Agriculture in southeastern Washington from 1892-1902, frequently distributed seed to farmers in the region from populations that he considered superior; he considered E. wawawaiensis to be a superior form of what is here called Pseudo-roegneria spicata (p. 281). Another source of introduced populations is 'Secar', a cultivar of E. wawawaiensis that is recommended as a forage grass for arid areas of the northwestern United States.</discussion>
  <discussion>Elymus wawawaiensis resembles a vigorous version of Pseudoroegneria spicata, and was long confused with that species. It differs in its more imbricate spikelets and narrower, stiff glumes. In its primary range, E. wawawaiensis is often sympatric with P. spicata, but the two tend to grow in different habitats, E. wawawaiensis growing in shallow, rocky soils and P. spicata in medium- to fine-textured loess soil. The two species also differ cytologically, E. wawawaiensis being an allo-tetraploid, and P. spicata consisting of diploids and autotetraploids.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho;Oreg.;Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>