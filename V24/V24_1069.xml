<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">754</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Pers." date="unknown" rank="genus">KOELERIA</taxon_name>
    <taxon_name authority="(Lam.) P. Beauv." date="unknown" rank="species">pyramidata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus koeleria;species pyramidata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Crested hairgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o20739" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (30) 40-90 cm, glabrous or puberulent above.</text>
      <biological_entity id="o20740" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths sparsely sericeous to densely pubescent, old sheaths persistent, disintegrating into wavy, curled, or arched fibers;</text>
      <biological_entity id="o20741" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparsely sericeous" name="pubescence" src="d0_s2" to="densely pubescent" />
      </biological_entity>
      <biological_entity id="o20742" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character constraint="into fibers" constraintid="o20743" is_modifier="false" name="dehiscence" src="d0_s2" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o20743" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
        <character is_modifier="true" name="shape" src="d0_s2" value="curled" value_original="curled" />
        <character is_modifier="true" name="shape" src="d0_s2" value="arched" value_original="arched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o20744" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 5-24 cm long, 1-5 mm wide, flat, margins of the basal blades often ciliate below, with hairs usually longer than 2 mm.</text>
      <biological_entity id="o20745" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="24" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20746" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; below" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20747" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o20748" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character modifier="usually longer than" name="some_measurement" src="d0_s4" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <relation from="o20746" id="r3273" name="part_of" negation="false" src="d0_s4" to="o20747" />
      <relation from="o20746" id="r3274" name="with" negation="false" src="d0_s4" to="o20748" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 5-22 cm long, 1-4 (5) cm wide;</text>
      <biological_entity id="o20749" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="22" to_unit="cm" />
        <character name="width" src="d0_s5" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches villous.</text>
      <biological_entity id="o20750" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 6-10 mm, with 2-4 (5) florets;</text>
      <biological_entity id="o20751" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20752" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o20751" id="r3275" name="with" negation="false" src="d0_s7" to="o20752" />
    </statement>
    <statement id="d0_s8">
      <text>rachillas with scattered pubescence.</text>
      <biological_entity id="o20753" name="rachilla" name_original="rachillas" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes acute, glabrous, smooth or scabrous;</text>
      <biological_entity id="o20754" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 4-5 mm, 1-veined;</text>
      <biological_entity constraint="lower" id="o20755" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 5-6.5 mm, 3-veined;</text>
      <biological_entity constraint="upper" id="o20756" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses broadly rounded, pubescent;</text>
      <biological_entity id="o20757" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 4-6 mm, glabrous or puberulent, rarely ciliate, usually green when young, stramineous at maturity, apices acuminate to shortly aristate;</text>
      <biological_entity id="o20758" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s13" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o20759" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s13" to="shortly aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 2-2.5 mm.</text>
      <biological_entity id="o20760" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2.5-3.8 mm. 2n = 14.</text>
      <biological_entity id="o20761" name="caryopse" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20762" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Koeleria pyramidata, as interpreted here, is confined to Europe. Some North American records for K. pyramidata are based on robust specimens of K. macrantha; others reflect an interpretation of K. pyramidata that includes K. macrantha.</discussion>
  
</bio:treatment>