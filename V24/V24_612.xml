<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Rydb." date="unknown" rank="species">saximontana</taxon_name>
    <taxon_name authority="Pavlick" date="unknown" rank="variety">robertsiana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species saximontana;variety robertsiana</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 16-37 cm, usually 2-3 times the height of the vegetative shoot leaves, usually sparsely scabrous or pubescent below the inflorescence.</text>
      <biological_entity id="o13543" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="37" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s0" value="2-3 times the height of" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s0" value="scabrous" value_original="scabrous" />
        <character constraint="below inflorescence" constraintid="o13545" is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o13544" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o13545" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Outer vegetative shoot sheaths brownish on the lower 1/2;</text>
      <biological_entity constraint="shoot" id="o13546" name="sheath" name_original="sheaths" src="d0_s1" type="structure" constraint_original="outer shoot">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
        <character constraint="on lower 1/2" constraintid="o13547" is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13547" name="1/2" name_original="1/2" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blades smooth or scabrous on the abaxial surfaces, ribs on the adaxial surfaces with hairs to 0.1 mm;</text>
      <biological_entity id="o13548" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="on abaxial surfaces" constraintid="o13549" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13549" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity id="o13550" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o13551" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity id="o13552" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o13550" id="r2166" name="on" negation="false" src="d0_s2" to="o13551" />
      <relation from="o13551" id="r2167" name="with" negation="false" src="d0_s2" to="o13552" />
    </statement>
    <statement id="d0_s3">
      <text>abaxial sclerenchyma in 5-7 narrow strands.</text>
      <biological_entity constraint="abaxial" id="o13553" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o13554" name="strand" name_original="strands" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o13553" id="r2168" name="in" negation="false" src="d0_s3" to="o13554" />
    </statement>
    <statement id="d0_s4">
      <text>Lemmas often scabrous on the distal 1/2.</text>
      <biological_entity id="o13555" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character constraint="on distal 1/2" constraintid="o13556" is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13556" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
    </statement>
  </description>
  <discussion>Festuca saximontana var. robertsiana grows in subalpine or lower alpine habitats. It has only been reported from British Columbia.</discussion>
  
</bio:treatment>