<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Rydb." date="unknown" rank="species">earlei</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species earlei</taxon_hierarchy>
  </taxon_identification>
  <number>17</number>
  <other_name type="common_name">Earle's fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, often with short rhizomes.</text>
      <biological_entity id="o26369" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26370" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o26369" id="r4155" modifier="often" name="with" negation="false" src="d0_s0" to="o26370" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (15) 20-40 (45) cm, glabrous, smooth.</text>
      <biological_entity id="o26371" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths closed for about 1/2 their length, glabrous, shredding into fibers, sometimes slowly;</text>
      <biological_entity id="o26372" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26373" name="fiber" name_original="fibers" src="d0_s2" type="structure" />
      <relation from="o26372" id="r4156" name="shredding into" negation="false" src="d0_s2" to="o26373" />
    </statement>
    <statement id="d0_s3">
      <text>collars glabrous;</text>
      <biological_entity id="o26374" name="collar" name_original="collars" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.5 (1) mm;</text>
      <biological_entity id="o26375" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 3 mm wide when flat, 0.5-1 (1.5) mm in diameter when conduplicate, veins (3) 5, ribs (1) 3 (5), abaxial surfaces smooth or slightly scabrous, adaxial surfaces sparsely scabrous;</text>
      <biological_entity id="o26376" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" modifier="when flat" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character modifier="when conduplicate" name="atypical_diameter" src="d0_s5" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="when conduplicate" name="diameter" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26377" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o26378" name="rib" name_original="ribs" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26379" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26380" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial sclerenchyma in 3-5 narrow strands less than twice as wide as high;</text>
      <biological_entity id="o26382" name="strand" name_original="strands" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="width_or_height" src="d0_s6" value="0-2 times as wide as high" />
      </biological_entity>
      <relation from="o26381" id="r4157" name="in" negation="false" src="d0_s6" to="o26382" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial sclerenchyma absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Inflor¬escences 3-5 (8) cm, contracted, with 1-3 branches per node;</text>
      <biological_entity constraint="abaxial" id="o26381" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o26383" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o26384" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o26383" id="r4158" name="per" negation="false" src="d0_s8" to="o26384" />
    </statement>
    <statement id="d0_s9">
      <text>branches stiff, erect, scabrous, lower branches with 2+ spikelets.</text>
      <biological_entity id="o26385" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26386" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o26387" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" upper_restricted="false" />
      </biological_entity>
      <relation from="o26386" id="r4159" name="with" negation="false" src="d0_s9" to="o26387" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets (4.5) 5-6.5 (7) mm, with 2-5 florets.</text>
      <biological_entity id="o26388" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26389" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o26388" id="r4160" name="with" negation="false" src="d0_s10" to="o26389" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes exceeded by the upper florets, lanceolate to ovatelanceolate, mostly smooth, sometimes scabrous distally;</text>
      <biological_entity id="o26390" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="ovatelanceolate" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o26391" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o26390" id="r4161" name="exceeded by the" negation="false" src="d0_s11" to="o26391" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 1.5-3 mm;</text>
      <biological_entity constraint="lower" id="o26392" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 2.5-3.8 mm;</text>
      <biological_entity constraint="upper" id="o26393" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3-4.5 mm, glabrous or puberulent near the apices, awns (0.3) 1-1.5 mm, terminal;</text>
      <biological_entity id="o26394" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="near apices" constraintid="o26395" is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o26395" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity id="o26396" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas as long as or slightly shorter than the lemmas, intercostal region puberulent distally;</text>
      <biological_entity id="o26397" name="palea" name_original="paleas" src="d0_s15" type="structure" />
      <biological_entity id="o26399" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o26398" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o26400" name="region" name_original="region" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o26397" id="r4162" name="as long as" negation="false" src="d0_s15" to="o26399" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.6-0.9 (1.4) mm;</text>
      <biological_entity id="o26401" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="1.4" value_original="1.4" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary apices densely and conspicuously pubescent.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = unknown.</text>
      <biological_entity constraint="ovary" id="o26402" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26403" name="chromosome" name_original="" src="d0_s18" type="structure" />
    </statement>
  </description>
  <discussion>Festuca earlei grows in rich subalpine and alpine meadows, at 2800-3800 m, in Utah, Colorado, Arizona, and New Mexico. It often grows with the non-rhizomatous species F. brachyphylla subsp. coloradensis (p. 428) and F. minutiflora (p. 434). It can be distinguished from the former by its pubescent ovary apices, and from the latter by its larger spikelets and lemmas. Because of its short rhizomes (which are often missing from herbarium specimens), F. earlei is sometimes confused with members of the F. rubra (p. 412) complex. It differs from them in having pubescent ovary apices and shorter anthers.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Ariz.;N.Mex.;Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>