<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Luerss." date="unknown" rank="subfamily">BAMBUSOIDEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="tribe">BAMBUSEAE</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="genus">ARUNDINARIA</taxon_name>
    <taxon_name authority="(Walter) Muhl." date="unknown" rank="species">tecta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily bambusoideae;tribe bambuseae;genus arundinaria;species tecta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arundinaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gigantea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">tecta</taxon_name>
    <taxon_hierarchy>genus arundinaria;species gigantea;subspecies tecta</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes normally horizontal for only a short distance before turning up to form a culm, hollow-centered, air canals present.</text>
      <biological_entity id="o103" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure">
        <character constraint="for distance" constraintid="o104" is_modifier="false" modifier="normally" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="position" src="d0_s0" value="hollow-centered" value_original="hollow-centered" />
      </biological_entity>
      <biological_entity id="o104" name="distance" name_original="distance" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="only" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o105" name="culm" name_original="culm" src="d0_s0" type="structure" />
      <biological_entity id="o106" name="canal" name_original="canals" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o104" id="r20" name="turning up-to-form a" negation="false" src="d0_s0" to="o105" />
    </statement>
    <statement id="d0_s1">
      <text>Culms usually shorter than 2.5 m tall, to 2 cm thick;</text>
      <biological_entity id="o107" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character modifier="usually shorter than" name="height" src="d0_s1" unit="m" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="0" from_unit="cm" name="thickness" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes terete in the vegetative parts.</text>
      <biological_entity id="o108" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="in parts" constraintid="o109" is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o109" name="part" name_original="parts" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culm leaves persistent to tardily deciduous;</text>
      <biological_entity constraint="culm" id="o110" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="persistent" name="duration" src="d0_s3" to="tardily deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths 11-18 cm;</text>
      <biological_entity id="o111" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>fimbriae 1.5-8.5 mm;</text>
      <biological_entity id="o112" name="fimbria" name_original="fimbriae" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2.5-4 cm.</text>
    </statement>
    <statement id="d0_s7">
      <text>Topknots of 9-12 leaves;</text>
      <biological_entity id="o113" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o114" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <relation from="o113" id="r21" name="consist_of" negation="false" src="d0_s7" to="o114" />
    </statement>
    <statement id="d0_s8">
      <text>blades 20-30 cm long, 1.8-3.2 cm wide, lanceolate to ovatelanceolate.</text>
      <biological_entity id="o115" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s8" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Primary branches usually 50+ cm, basally erect and distally arcuate, terete, with 3-4 compressed basal internodes, basal nodes developing secondary branches, lower elongated internodes terete in cross-section.</text>
      <biological_entity constraint="primary" id="o116" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s9" upper_restricted="false" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="basal" id="o117" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="true" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o118" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity constraint="secondary" id="o119" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="development" src="d0_s9" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="lower" id="o120" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity id="o121" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character is_modifier="true" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character constraint="in cross-section" constraintid="o122" is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o122" name="cross-section" name_original="cross-section" src="d0_s9" type="structure" />
      <relation from="o116" id="r22" name="with" negation="false" src="d0_s9" to="o117" />
    </statement>
    <statement id="d0_s10">
      <text>Foliage leaves: abaxial ligules fimbriate to lacerate, sometimes ciliate;</text>
      <biological_entity constraint="foliage" id="o123" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity constraint="abaxial" id="o124" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character char_type="range_value" from="fimbriate" name="shape" src="d0_s10" to="lacerate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blades 7-23 cm long, 1-2 cm wide, coriaceous, persistent, evergreen, bases rounded, abaxial surfaces densely pubescent or glabrous, strongly cross veined, adaxial surfaces pubescent.</text>
      <biological_entity constraint="foliage" id="o125" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o126" name="blade" name_original="blades" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s11" to="23" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="2" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s11" value="evergreen" value_original="evergreen" />
      </biological_entity>
      <biological_entity id="o127" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o128" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="cross" value_original="cross" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o129" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3-5 cm, with 6-12 florets, the first occasionally sterile.</text>
      <biological_entity id="o130" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s12" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" notes="" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o131" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <relation from="o130" id="r23" name="with" negation="false" src="d0_s12" to="o131" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes unequal, glabrous or pubescent;</text>
      <biological_entity id="o132" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lowest glume obtuse to acuminate or absent;</text>
      <biological_entity constraint="lowest" id="o133" name="glume" name_original="glume" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="acuminate" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.2-2 cm, glabrous or nearly so.</text>
      <biological_entity id="o134" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s15" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s15" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses oblong, beaked, a rudimentary hooked style-branch present below the beak.</text>
      <biological_entity id="o135" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o137" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o136" id="r24" name="present" negation="false" src="d0_s16" to="o137" />
    </statement>
    <statement id="d0_s17">
      <text>2n = unknown.</text>
      <biological_entity id="o136" name="style-branch" name_original="style-branch" src="d0_s16" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s16" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="true" name="shape" src="d0_s16" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="2n" id="o138" name="chromosome" name_original="" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Arundinaria tecta grows in swampy woods, moist pine barrens, live oak woods, and along the sandy margins of streams, preferring moister sites than A. gigantea. It grows only on the coastal plain of the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.J.;Okla.;Ala.;Ark.;Fla.;Ga.;La.;Miss.;N.C.;N.Y.;Pa.;S.C.;Tex.;Va.;Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>