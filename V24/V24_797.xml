<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">570</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Asch. &amp; Graebn." date="unknown" rank="section">Oreinos</taxon_name>
    <taxon_name authority="Haenke" date="unknown" rank="species">laxa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="subspecies">banffiana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section oreinos;species laxa;subspecies banffiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal branching mainly extravaginal.</text>
      <biological_entity id="o29676" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="position" src="d0_s0" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s0" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades thin, Panicles 2-8 cm, lax, loosely contracted, sparse, with 2-3 (5) branches per node;</text>
      <biological_entity id="o29677" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o29678" name="panicle" name_original="panicles" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s1" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="count_or_density" src="d0_s1" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o29679" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s1" value="5" value_original="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o29680" name="node" name_original="node" src="d0_s1" type="structure" />
      <relation from="o29678" id="r4675" name="with" negation="false" src="d0_s1" to="o29679" />
      <relation from="o29679" id="r4676" name="per" negation="false" src="d0_s1" to="o29680" />
    </statement>
    <statement id="d0_s2">
      <text>branches steeply ascending, fairly straight, usually sparsely scabrous, infrequently smooth.</text>
      <biological_entity id="o29681" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="steeply" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="fairly" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="infrequently" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Glumes lanceolate to broadly lanceolate;</text>
      <biological_entity id="o29682" name="glume" name_original="glumes" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="broadly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o29683" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>calluses glabrous;</text>
      <biological_entity id="o29684" name="callus" name_original="calluses" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lemmas with keels short-villous for at least 1/2 their length, usually the lateral-veins on at least 1 side of some florets sparsely softly puberulent, infrequently all the lateral-veins glabrous;</text>
      <biological_entity id="o29685" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character modifier="at least" name="length" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o29686" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o29687" name="lateral-vein" name_original="lateral-veins" src="d0_s6" type="structure" />
      <biological_entity id="o29688" name="side" name_original="side" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="at least" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="sparsely softly" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o29689" name="floret" name_original="florets" src="d0_s6" type="structure" />
      <biological_entity id="o29690" name="lateral-vein" name_original="lateral-veins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o29685" id="r4677" name="with" negation="false" src="d0_s6" to="o29686" />
      <relation from="o29687" id="r4678" name="on" negation="false" src="d0_s6" to="o29688" />
      <relation from="o29688" id="r4679" name="part_of" negation="false" src="d0_s6" to="o29689" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 0.8-1.1 mm. 2n = 84.</text>
      <biological_entity id="o29691" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29692" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa laxa subsp. banffiana grows primarily in mesic alpine locations of the Rocky Mountains in Canada and the United States. It is sometimes difficult to distinguish from P. glauca (p. 576).</discussion>
  
</bio:treatment>