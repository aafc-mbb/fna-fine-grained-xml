<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">248</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">HORDEUM</taxon_name>
    <taxon_name authority="Covas" date="unknown" rank="species">arizonicum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus hordeum;species arizonicum</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Arizona barley</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual to biennial, perennial under favorable conditions;</text>
      <biological_entity id="o1499" name="condition" name_original="conditions" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>forming small tufts.</text>
      <biological_entity id="o1498" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="annual" name="duration" src="d0_s0" to="biennial" />
        <character constraint="under conditions" constraintid="o1499" is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1500" name="tuft" name_original="tufts" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o1498" id="r239" name="forming" negation="false" src="d0_s1" to="o1500" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 21-75 cm, rarely geniculate, not bulbous;</text>
      <biological_entity id="o1501" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="21" from_unit="cm" name="some_measurement" src="d0_s2" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o1502" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Lower sheaths pub¬escent;</text>
      <biological_entity constraint="lower" id="o1503" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>upper sheaths glabrous;</text>
      <biological_entity constraint="upper" id="o1504" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.6-1.8 mm;</text>
      <biological_entity id="o1505" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>auricles absent;</text>
      <biological_entity id="o1506" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades to 13 cm long, to 4 mm wide, flat, glaucous, both surfaces scabrous, hairy, sometimes only sparsely hairy.</text>
      <biological_entity id="o1507" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s8" to="13" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o1508" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes only sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikes 5-12 cm long, 6-10 mm wide, erect, often partially enclosed at maturity, pale green.</text>
      <biological_entity id="o1509" name="spike" name_original="spikes" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s9" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="at maturity" is_modifier="false" modifier="often partially" name="position" src="d0_s9" value="enclosed" value_original="enclosed" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale green" value_original="pale green" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes bent, strongly divergent at maturity.</text>
      <biological_entity id="o1510" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="bent" value_original="bent" />
        <character constraint="at maturity" is_modifier="false" modifier="strongly" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Central spikelets: glumes 11-28 mm long, 0.2-0.4 mm wide, flattened near the base, setaceous above the middle;</text>
      <biological_entity constraint="central" id="o1511" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o1512" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s11" to="28" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="0.4" to_unit="mm" />
        <character constraint="near base" constraintid="o1513" is_modifier="false" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character constraint="above middle, spikelets" constraintid="o1514, o1515" is_modifier="false" name="shape" notes="" src="d0_s11" value="setaceous" value_original="setaceous" />
      </biological_entity>
      <biological_entity id="o1513" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o1514" name="middle" name_original="middle" src="d0_s11" type="structure" />
      <biological_entity id="o1515" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas 5-9 mm, glabrous, awned, awns 10-22 mm;</text>
      <biological_entity constraint="central" id="o1516" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <biological_entity id="o1517" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1518" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1-2.2 mm.</text>
      <biological_entity constraint="central" id="o1519" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <biological_entity id="o1520" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lateral spikelets sterile;</text>
      <biological_entity constraint="lateral" id="o1521" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>glumes to 27 mm long, 0.2-0.4 mm wide, flattened near the base;</text>
      <biological_entity id="o1522" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s15" to="27" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.4" to_unit="mm" />
        <character constraint="near base" constraintid="o1523" is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o1523" name="base" name_original="base" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas 2.5-5 mm, unawned or awned, awns to 3 mm, lemma and awn together to 7 mm, the transition from the lemma to the awn gradual.</text>
      <biological_entity id="o1524" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1525" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1528" name="lemma" name_original="lemma" src="d0_s16" type="structure" />
      <biological_entity id="o1529" name="awn" name_original="awn" src="d0_s16" type="structure" />
      <relation from="o1528" id="r240" name="to" negation="false" src="d0_s16" to="o1529" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 42.</text>
      <biological_entity id="o1526" name="lemma" name_original="lemma" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character constraint="from lemma" constraintid="o1528" is_modifier="false" name="duration" src="d0_s16" value="transition" value_original="transition" />
      </biological_entity>
      <biological_entity id="o1527" name="awn" name_original="awn" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character constraint="from lemma" constraintid="o1528" is_modifier="false" name="duration" src="d0_s16" value="transition" value_original="transition" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1530" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hordeum arizonicum grows in saline habitats, along irrigation ditches, canals, and ponds in the south¬western United States and northern Mexico. It is a segmental allopolyploid between H. jubatum and either H. pusillum or H. intercedens.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.;N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>