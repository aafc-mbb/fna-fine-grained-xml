<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">360</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Hochst." date="unknown" rank="genus">LEYMUS</taxon_name>
    <taxon_name authority="(Trin.) Pilg." date="unknown" rank="species">angustus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus leymus;species angustus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">angustus</taxon_name>
    <taxon_hierarchy>genus elymus;species angustus</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Altai wildrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants somewhat cespitose, rhizomatous.</text>
      <biological_entity id="o22565" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="somewhat" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 60-120 cm tall, 2.5-7 mm thick, solitary or few together, glabrous or pubescent below the nodes.</text>
      <biological_entity id="o22566" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="height" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="thickness" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="together" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o22567" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22567" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves exceeded by the spikes, basally concentrated;</text>
      <biological_entity id="o22568" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s2" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity id="o22569" name="spike" name_original="spikes" src="d0_s2" type="structure" />
      <relation from="o22568" id="r3590" name="exceeded by the" negation="false" src="d0_s2" to="o22569" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths smooth, scabridulous, or hairy;</text>
      <biological_entity id="o22570" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles to 1 mm;</text>
      <biological_entity id="o22571" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1 mm, rounded to obtuse, sometimes erose;</text>
      <biological_entity id="o22572" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 15-20 cm long, 5-7 mm wide, glaucous, stiff, involute, abaxial surfaces glabrous or hairy, sometimes scabridulous, adaxial surfaces scabrous, with 7-17 closely spaced subequal veins.</text>
      <biological_entity id="o22573" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22574" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22575" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o22576" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s6" to="17" />
        <character is_modifier="true" modifier="closely" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
        <character is_modifier="true" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <relation from="o22575" id="r3591" name="with" negation="false" src="d0_s6" to="o22576" />
    </statement>
    <statement id="d0_s7">
      <text>Spikes 10-25 cm long, 7-10 mm wide, with 2 (3) spikelets per node;</text>
      <biological_entity id="o22577" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22578" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o22579" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o22577" id="r3592" name="with" negation="false" src="d0_s7" to="o22578" />
      <relation from="o22578" id="r3593" name="per" negation="false" src="d0_s7" to="o22579" />
    </statement>
    <statement id="d0_s8">
      <text>internodes 8-10 mm, surfaces strigillose, hairs to 0.3 mm, edges ciliate, cilia to 1 mm.</text>
      <biological_entity id="o22580" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22581" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o22582" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22583" name="edge" name_original="edges" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22584" name="cilium" name_original="cilia" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 10-19 mm, with 2-3 florets.</text>
      <biological_entity id="o22585" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22586" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <relation from="o22585" id="r3594" name="with" negation="false" src="d0_s9" to="o22586" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes 10-13 mm long, 0.5-2.5 mm wide, exceeded by the florets, narrowly lanceolate, tapering from the base, stiff, keeled, the central portion thicker than the margins, (0) 1 (3) -veined at midlength, bases expanded, overlapping, concealing the base of the lowest floret, scabrous;</text>
      <biological_entity id="o22587" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character constraint="from base" constraintid="o22589" is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s10" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o22588" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <biological_entity id="o22589" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="central" id="o22590" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character constraint="than the margins" constraintid="o22591" is_modifier="false" name="width" src="d0_s10" value="thicker" value_original="thicker" />
        <character constraint="at midlength" constraintid="o22592" is_modifier="false" name="architecture" src="d0_s10" value="(0)1(3)-veined" value_original="(0)1(3)-veined" />
      </biological_entity>
      <biological_entity id="o22591" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o22592" name="midlength" name_original="midlength" src="d0_s10" type="structure" />
      <biological_entity id="o22593" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o22594" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="position" src="d0_s10" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o22595" name="floret" name_original="floret" src="d0_s10" type="structure" />
      <relation from="o22587" id="r3595" name="exceeded by" negation="false" src="d0_s10" to="o22588" />
      <relation from="o22594" id="r3596" name="part_of" negation="false" src="d0_s10" to="o22595" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 8-13 mm, densely hairy and not glaucous, hairs to 0.4 mm, or glabrous and glaucous, apices unawned or awned, awns to 2.5 mm;</text>
      <biological_entity id="o22596" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s11" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o22597" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o22598" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o22599" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3-4 mm, dehiscent.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 84.</text>
      <biological_entity id="o22600" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22601" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leymus angustus is a Eurasian species that, in its native range, grows in alkaline meadows, and on sand and gravel in river and lake valleys. Several cultivars of L. angustus have been developed for use as forage, particularly in Canada. Some of the better known are 'Prairieland', 'Eejay', and 'Pearl'. The distribution of L. angustus in the Flora region is not known.</discussion>
  <discussion>Chen and Zhu (2006) describe Leymus angustus as always being puberulent. Some accessions cultivated under this name by the U.S. Department of Agriculture (Plant Introduction Numbers 110,079; 406,461), have glabrous, glaucous lemmas and glumes that tend to exceed the lemmas, suggesting that they belong to another taxon, possibly L. karelinii (Turcz.) Tzvelev, a species for which 2n = 56.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;B.C.;Sask.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>