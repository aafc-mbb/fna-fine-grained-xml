<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Mansf. ex Tsitsin &amp; K.A. Petrova" date="unknown" rank="genus">×ELYHORDEUM</taxon_name>
    <taxon_name authority="(W.W. Mitch. &amp; H.J." date="unknown" rank="species">pilosilemma</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×elyhordeum;species pilosilemma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrohordeum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pilosilemma</taxon_name>
    <taxon_hierarchy>genus agrohordeum;species pilosilemma</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 40-75 cm, erect, glabrous.</text>
      <biological_entity id="o27615" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o27616" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 6-15 cm long, 2-4 mm wide, flat, adaxial surfaces glabrous or hairy.</text>
      <biological_entity id="o27617" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27618" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes 6-12 cm long, 5-7 mm wide including the awns, erect to arching, with 1-2 spikelets per node;</text>
      <biological_entity id="o27619" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="arching" />
      </biological_entity>
      <biological_entity id="o27620" name="awn" name_original="awns" src="d0_s3" type="structure" />
      <biological_entity id="o27621" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <biological_entity id="o27622" name="node" name_original="node" src="d0_s3" type="structure" />
      <relation from="o27619" id="r4351" name="including the" negation="false" src="d0_s3" to="o27620" />
      <relation from="o27619" id="r4352" name="with" negation="false" src="d0_s3" to="o27621" />
      <relation from="o27621" id="r4353" name="per" negation="false" src="d0_s3" to="o27622" />
    </statement>
    <statement id="d0_s4">
      <text>internodes 2-5 mm, concealed by the spikelets.</text>
      <biological_entity id="o27623" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character constraint="by spikelets" constraintid="o27624" is_modifier="false" name="prominence" src="d0_s4" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o27624" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 7-13 mm, with 2-3 florets and 2-3 glumes.</text>
      <biological_entity id="o27625" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27626" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o27627" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <relation from="o27625" id="r4354" name="with" negation="false" src="d0_s5" to="o27626" />
      <relation from="o27625" id="r4355" name="with" negation="false" src="d0_s5" to="o27627" />
    </statement>
    <statement id="d0_s6">
      <text>Glumes 10-18 mm including the awns, not indurate at the base, linear to linear-lanceolate, hispid, 1-3-veined;</text>
      <biological_entity id="o27628" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
        <character constraint="at base" constraintid="o27630" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="indurate" value_original="indurate" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s6" to="linear-lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o27629" name="awn" name_original="awns" src="d0_s6" type="structure" />
      <biological_entity id="o27630" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o27628" id="r4356" name="including the" negation="false" src="d0_s6" to="o27629" />
    </statement>
    <statement id="d0_s7">
      <text>lemmas 7-10 mm, evenly pilose, hairs about 0.2 mm, awns as long as to slightly longer than the lemma bodies;</text>
      <biological_entity id="o27631" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27632" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o27633" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character constraint="than the lemma bodies" constraintid="o27634" is_modifier="false" name="length_or_size" src="d0_s7" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o27634" name="body" name_original="bodies" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 1-1.5 mm, indehiscent.</text>
    </statement>
    <statement id="d0_s9">
      <text>In = 28.</text>
      <biological_entity id="o27635" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s8" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>×Elyhordeum pilosilemma is a hybrid between Elymus macrourus and Hordeum jubatum that occurs in many locations where the two parental species co-occur. It is very similar to ×E. jordalii, a hybrid between E. macrourus and H. brachyantherum.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>