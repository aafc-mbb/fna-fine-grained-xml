<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AEGILOPS</taxon_name>
    <taxon_name authority="Roth" date="unknown" rank="species">geniculata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus aegilops;species geniculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aegilops</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ovata</taxon_name>
    <taxon_hierarchy>genus aegilops;species ovata</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Ovate goatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-40 cm, geniculate at the base, usually with many tillers.</text>
      <biological_entity id="o2242" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character constraint="at base" constraintid="o2243" is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o2243" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o2244" name="tiller" name_original="tillers" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="many" value_original="many" />
      </biological_entity>
      <relation from="o2242" id="r370" modifier="usually" name="with" negation="false" src="d0_s0" to="o2244" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths with hyaline margins, the distal portion of the lower cauline sheaths ciliate;</text>
      <biological_entity id="o2245" name="sheath" name_original="sheaths" src="d0_s1" type="structure" />
      <biological_entity id="o2246" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2247" name="portion" name_original="portion" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="lower cauline" id="o2248" name="sheath" name_original="sheaths" src="d0_s1" type="structure" />
      <relation from="o2245" id="r371" name="with" negation="false" src="d0_s1" to="o2246" />
      <relation from="o2247" id="r372" name="part_of" negation="false" src="d0_s1" to="o2248" />
    </statement>
    <statement id="d0_s2">
      <text>blades 2-7.5 cm long, 2-5 mm wide.</text>
      <biological_entity id="o2249" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes 1-3 cm long, bases 0.4-0.7 cm wide, narrowly ovoid to ellipsoid, gradually tapering distally, with (2) 3-4 spikelets, the distal spikelet sterile;</text>
      <biological_entity id="o2250" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2251" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="narrowly ovoid" name="shape" src="d0_s3" to="ellipsoid" />
        <character is_modifier="false" modifier="gradually; distally" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o2252" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s3" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2253" name="spikelet" name_original="spikelet" src="d0_s3" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o2251" id="r373" name="with" negation="false" src="d0_s3" to="o2252" />
    </statement>
    <statement id="d0_s4">
      <text>rudimentary spikelets 1 (2);</text>
    </statement>
    <statement id="d0_s5">
      <text>disarticulation at the base of the spikes, above the rudimentary spikelets.</text>
      <biological_entity id="o2254" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2255" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o2256" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <biological_entity id="o2257" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o2254" id="r374" name="at" negation="false" src="d0_s5" to="o2255" />
      <relation from="o2255" id="r375" name="part_of" negation="false" src="d0_s5" to="o2256" />
    </statement>
    <statement id="d0_s6">
      <text>Fertile spikelets 7-10 mm long, 3-4 mm wide, urceolate;</text>
      <biological_entity id="o2258" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower spikelet with 3-4 florets, the lower 1-2 florets fertile;</text>
      <biological_entity constraint="lower" id="o2259" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <biological_entity id="o2260" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o2261" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o2262" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o2259" id="r376" name="with" negation="false" src="d0_s7" to="o2260" />
    </statement>
    <statement id="d0_s8">
      <text>apical spikelets 4-5 mm long, 1-2 mm wide, narrowly obovoid, with 1 floret, floret reduced, sterile.</text>
      <biological_entity constraint="apical" id="o2263" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity id="o2264" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2265" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o2263" id="r377" name="with" negation="false" src="d0_s8" to="o2264" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes of fertile spikelets 6-10 mm, ovate, smooth, scabrous, appressed-velutinous, (3) 4 (5) -awned, awns 2-4.5 cm;</text>
      <biological_entity id="o2266" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="appressed-velutinous" value_original="appressed-velutinous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="(3)4(5)-awned" value_original="(3)4(5)-awned" />
      </biological_entity>
      <biological_entity id="o2267" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o2268" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
      </biological_entity>
      <relation from="o2266" id="r378" name="part_of" negation="false" src="d0_s9" to="o2267" />
    </statement>
    <statement id="d0_s10">
      <text>glumes of apical spikelets about 3 mm, 4-awned, awns usually 1-3.5 cm;</text>
      <biological_entity id="o2269" name="glume" name_original="glumes" src="d0_s10" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="4-awned" value_original="4-awned" />
      </biological_entity>
      <biological_entity constraint="apical" id="o2270" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o2271" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
      </biological_entity>
      <relation from="o2269" id="r379" name="part_of" negation="false" src="d0_s10" to="o2270" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas of fertile spikelets 6-8 mm, adaxial surfaces often velutinous distally, apices 2-3-awned, awns 1-2.5 cm.</text>
      <biological_entity id="o2272" name="lemma" name_original="lemmas" src="d0_s11" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2273" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2274" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often; distally" name="pubescence" src="d0_s11" value="velutinous" value_original="velutinous" />
      </biological_entity>
      <biological_entity id="o2275" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="2-3-awned" value_original="2-3-awned" />
      </biological_entity>
      <biological_entity id="o2276" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="2.5" to_unit="cm" />
      </biological_entity>
      <relation from="o2272" id="r380" name="part_of" negation="false" src="d0_s11" to="o2273" />
    </statement>
    <statement id="d0_s12">
      <text>Caryopses 4-6 mm, falling free from the lemmas and paleas.</text>
      <biological_entity id="o2278" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o2279" name="palea" name_original="paleas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Haplomes MU.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 28.</text>
      <biological_entity id="o2277" name="caryopse" name_original="caryopses" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character constraint="from paleas" constraintid="o2279" is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2280" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the Flora region, Aegilops geniculata is known only from Mendocino County, California, where it usually occurs along roadsides. It is native from the Mediterranean area to central Asia. In California, it grows in silty clay.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Y.;Calif.;Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>