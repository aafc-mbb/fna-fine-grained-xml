<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(Shear) Hitchc." date="unknown" rank="species">grandis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species grandis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">orcuttianus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">grandis</taxon_name>
    <taxon_hierarchy>genus bromus;species orcuttianus;variety grandis</taxon_hierarchy>
  </taxon_identification>
  <number>17</number>
  <other_name type="common_name">Tall brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o5865" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 70-180 cm, erect;</text>
      <biological_entity id="o5866" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s2" to="180" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 3-7, pubescent or puberulent;</text>
      <biological_entity id="o5867" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes pubescent, puberulent, or glabrous.</text>
      <biological_entity id="o5868" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths densely pubescent, hairs to 1 mm, collars with hairs to 2 mm, midrib of the culm leaves not abruptly narrowed just below the collar;</text>
      <biological_entity id="o5869" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5870" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5871" name="collar" name_original="collars" src="d0_s5" type="structure" />
      <biological_entity id="o5872" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5873" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character constraint="just below collar" constraintid="o5875" is_modifier="false" modifier="not abruptly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="culm" id="o5874" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o5875" name="collar" name_original="collar" src="d0_s5" type="structure" />
      <relation from="o5871" id="r945" name="with" negation="false" src="d0_s5" to="o5872" />
      <relation from="o5873" id="r946" name="part_of" negation="false" src="d0_s5" to="o5874" />
    </statement>
    <statement id="d0_s6">
      <text>auricles sometimes present;</text>
      <biological_entity id="o5876" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 1-3 mm, densely pubescent to pilose, obtuse, lacerate;</text>
      <biological_entity id="o5877" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="densely pubescent" name="pubescence" src="d0_s7" to="pilose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades (13) 18-38 cm long, 3-12 mm wide, flat, sparsely to densely pubescent on both surfaces.</text>
      <biological_entity id="o5878" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="13" value_original="13" />
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s8" to="38" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character constraint="on surfaces" constraintid="o5879" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5879" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles 14-27 cm long, very open, nodding;</text>
      <biological_entity id="o5880" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s9" to="27" to_unit="cm" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches flexuous, usually widely spreading, with spikelets near the tips.</text>
      <biological_entity id="o5881" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="usually widely" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o5882" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o5883" name="tip" name_original="tips" src="d0_s10" type="structure" />
      <relation from="o5881" id="r947" name="with" negation="false" src="d0_s10" to="o5882" />
      <relation from="o5882" id="r948" name="near" negation="false" src="d0_s10" to="o5883" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 20-37 (45) mm, elliptic to lanceolate, terete to moderately laterally compressed, with 4-9 (10) florets.</text>
      <biological_entity id="o5884" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="45" value_original="45" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="37" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o5885" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="10" value_original="10" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <relation from="o5884" id="r949" name="with" negation="false" src="d0_s11" to="o5885" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes pubescent, with prominent veins, margins not bronze-tinged;</text>
      <biological_entity id="o5886" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5887" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o5888" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s12" value="bronze-tinged" value_original="bronze-tinged" />
      </biological_entity>
      <relation from="o5886" id="r950" name="with" negation="false" src="d0_s12" to="o5887" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 4-8.5 mm, (1) 3-veined;</text>
      <biological_entity constraint="lower" id="o5889" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="(1)3-veined" value_original="(1)3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 7-10 mm, 3 (5) -veined, not mucronate;</text>
      <biological_entity constraint="upper" id="o5890" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3(5)-veined" value_original="3(5)-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 9.5-14 mm, lanceolate, rounded over the midvein, backs pilose, pubescent, or glabrous, margins pilose, not bronze-tinged, apices subulate to acute, entire;</text>
      <biological_entity id="o5891" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" src="d0_s15" to="14" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character constraint="over midvein" constraintid="o5892" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5892" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o5893" name="back" name_original="backs" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5894" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s15" value="bronze-tinged" value_original="bronze-tinged" />
      </biological_entity>
      <biological_entity id="o5895" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s15" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 3-7 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o5896" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o5897" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o5897" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 3-6 mm. 2n = 14.</text>
      <biological_entity id="o5898" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5899" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus grandis grows on dry, wooded or open slopes, at elevations of 350-2500 m. Its range extends from central California into Baja California, Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>