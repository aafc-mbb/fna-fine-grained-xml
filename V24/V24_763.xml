<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">545</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subsection">Poa nervosa Complex</taxon_name>
    <taxon_name authority="Nutt." date="unknown" rank="species">cuspidata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection poa nervosa complex;species cuspidata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Early bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely tufted or with solitary shoots, shortly rhizomatous.</text>
      <biological_entity id="o3769" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly extravaginal.</text>
      <biological_entity id="o3768" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="with solitary shoots" value_original="with solitary shoots" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 15-60 cm, erect or the bases decumbent, not branching above the base, terete or weakly compressed;</text>
      <biological_entity id="o3770" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o3771" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character constraint="above base" constraintid="o3772" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o3772" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, 0-1 exserted.</text>
      <biological_entity id="o3773" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="1" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for about 1/2 their length, slightly compressed, distinctly keeled, glabrous, bases of basal sheaths glabrous, distal sheath lengths 4-60 times blade lengths;</text>
      <biological_entity id="o3774" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3775" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3776" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o3777" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o3778" is_modifier="false" name="length" src="d0_s5" value="4-60 times blade lengths" value_original="4-60 times blade lengths" />
      </biological_entity>
      <biological_entity id="o3778" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o3775" id="r609" name="part_of" negation="false" src="d0_s5" to="o3776" />
    </statement>
    <statement id="d0_s6">
      <text>collars of proximal leaves usually retrorsely scabrous or pubescent distally and about the throat;</text>
      <biological_entity id="o3779" name="collar" name_original="collars" src="d0_s6" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" modifier="usually retrorsely" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3780" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o3781" name="throat" name_original="throat" src="d0_s6" type="structure" />
      <relation from="o3779" id="r610" name="part_of" negation="false" src="d0_s6" to="o3780" />
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.5-4 mm, smooth or scabrous, apices truncate to acute;</text>
      <biological_entity id="o3782" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3783" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>innovation blades similar to the cauline blades;</text>
      <biological_entity constraint="innovation" id="o3784" name="blade" name_original="blades" src="d0_s8" type="structure" />
      <biological_entity constraint="cauline" id="o3785" name="blade" name_original="blades" src="d0_s8" type="structure" />
      <relation from="o3784" id="r611" name="to" negation="false" src="d0_s8" to="o3785" />
    </statement>
    <statement id="d0_s9">
      <text>cauline blades 1-4 mm wide, usually flat, sometimes slightly folded, smooth or sparsely scabrous, primarily over the veins, apices broadly prow-shaped, blades steeply reduced in length distally, flag leaf-blades 0.2-3 (6) cm.</text>
      <biological_entity constraint="cauline" id="o3786" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sometimes slightly" name="architecture_or_shape" src="d0_s9" value="folded" value_original="folded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3787" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o3788" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o3789" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="steeply; distally" name="length" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3790" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character name="atypical_distance" src="d0_s9" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="distance" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
      <relation from="o3786" id="r612" modifier="primarily" name="over" negation="false" src="d0_s9" to="o3787" />
    </statement>
    <statement id="d0_s10">
      <text>Panicles 5-15 cm, erect or lax, pyramidal, open, sparse, with 20-80 spikelets, proximal internodes usually 3+ cm;</text>
      <biological_entity id="o3791" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o3792" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s10" to="80" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3793" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <relation from="o3791" id="r613" name="with" negation="false" src="d0_s10" to="o3792" />
    </statement>
    <statement id="d0_s11">
      <text>nodes usually with 2 branches;</text>
      <biological_entity id="o3794" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <biological_entity id="o3795" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o3794" id="r614" name="with" negation="false" src="d0_s11" to="o3795" />
    </statement>
    <statement id="d0_s12">
      <text>branches (2) 3-7 (10) cm, spreading to reflexed, straight, angled, angles scabrous, with 2-8 (10) spikelets.</text>
      <biological_entity id="o3796" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s12" to="7" to_unit="cm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="reflexed" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o3797" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3798" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="10" value_original="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
      <relation from="o3797" id="r615" name="with" negation="false" src="d0_s12" to="o3798" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 5-8 mm, lengths to 3.5 times widths, laterally compressed, not sexually dimorphic;</text>
      <biological_entity id="o3799" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="0-3.5" value_original="0-3.5" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets 2-5;</text>
      <biological_entity id="o3800" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>rachilla internodes smooth.</text>
      <biological_entity constraint="rachilla" id="o3801" name="internode" name_original="internodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Glumes narrowly lanceolate to lanceolate, distinctly keeled;</text>
      <biological_entity id="o3802" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s16" to="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 1-3-veined;</text>
      <biological_entity constraint="lower" id="o3803" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calluses webbed, hairs over 1/3 the lemma length;</text>
      <biological_entity id="o3804" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o3805" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o3806" name="lemma" name_original="lemma" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="1/3" value_original="1/3" />
      </biological_entity>
      <relation from="o3805" id="r616" name="over" negation="false" src="d0_s18" to="o3806" />
    </statement>
    <statement id="d0_s19">
      <text>lemmas 3-6 mm, lanceolate, distinctly keeled, keels and marginal veins sparsely short to long-villous, lateral-veins moderately prominent, intercostal regions glabrous or the upper florets in the spikelets softly puberulent, margins glabrous, apices acute;</text>
      <biological_entity id="o3807" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3808" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s19" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3809" name="vein" name_original="veins" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s19" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o3810" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s19" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o3811" name="region" name_original="regions" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o3812" name="floret" name_original="florets" src="d0_s19" type="structure" />
      <biological_entity id="o3813" name="spikelet" name_original="spikelets" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s19" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3814" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3815" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o3812" id="r617" name="in" negation="false" src="d0_s19" to="o3813" />
    </statement>
    <statement id="d0_s20">
      <text>palea keels scabrous, softly puberulent at midlength;</text>
      <biological_entity constraint="palea" id="o3816" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character constraint="at midlength" constraintid="o3817" is_modifier="false" modifier="softly" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3817" name="midlength" name_original="midlength" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>anthers vestigial (0.1-0.2 mm) or 2-3.5 mm. 2n = 28.</text>
      <biological_entity id="o3818" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s21" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s21" value="2-3.5 mm" value_original="2-3.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3819" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa cuspidata is a common species of forest openings in the Appalachian Mountains. It is an eastern counterpart of P. arnowiae (see previous), P. tracyi (p. 543), and P. nervosa (see next). Like those species, it is sequentially gynomonoecious.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.J.;W.Va.;Pa.;La.;Del.;Ala.;D.C.;Tenn.;N.C.;S.C.;Va.;Ohio;Ga.;Ind.;N.Y.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>