<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">789</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">APERA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spica-venti</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus apera;species spica-venti</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Common windgrass</other_name>
  <other_name type="common_name">Loose silkybent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-80 (120) cm, stout, usually with several shoots, sparingly branched;</text>
      <biological_entity id="o15977" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="120" value_original="120" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="sparingly" name="architecture" notes="" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o15978" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="several" value_original="several" />
      </biological_entity>
      <relation from="o15977" id="r2552" modifier="usually" name="with" negation="false" src="d0_s0" to="o15978" />
    </statement>
    <statement id="d0_s1">
      <text>internodes shorter or longer than the sheaths.</text>
      <biological_entity id="o15979" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
        <character constraint="than the sheaths" constraintid="o15980" is_modifier="false" name="length_or_size" src="d0_s1" value="shorter or longer" value_original="shorter or longer" />
      </biological_entity>
      <biological_entity id="o15980" name="sheath" name_original="sheaths" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths often purplish;</text>
      <biological_entity id="o15981" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 3-6 (12) mm;</text>
      <biological_entity id="o15982" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually 6-16 (25) cm long, 2-5 (10) mm wide, flat.</text>
      <biological_entity id="o15983" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="16" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (5) 10-35 cm long, (2) 3-15 cm wide, usually open, pyramidal;</text>
      <biological_entity id="o15984" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character name="width" src="d0_s5" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches spreading, naked at the base for 5+ mm, with spikelets usually borne towards the distal ends;</text>
      <biological_entity id="o15985" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character constraint="at base" constraintid="o15986" is_modifier="false" name="architecture" src="d0_s6" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o15986" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o15987" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o15988" name="end" name_original="ends" src="d0_s6" type="structure" />
      <relation from="o15985" id="r2553" modifier="for 5+ mm" name="with" negation="false" src="d0_s6" to="o15987" />
      <relation from="o15987" id="r2554" name="borne towards the" negation="false" src="d0_s6" to="o15988" />
    </statement>
    <statement id="d0_s7">
      <text>pedicels 1-3 mm.</text>
      <biological_entity id="o15989" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2.4-3.2 mm, often purplish;</text>
      <biological_entity id="o15990" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachillas prolonged for about 0.5 mm.</text>
      <biological_entity id="o15991" name="rachilla" name_original="rachillas" src="d0_s9" type="structure">
        <character constraint="for about 0.5 mm" is_modifier="false" name="length" src="d0_s9" value="prolonged" value_original="prolonged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 1.5-2.5 mm;</text>
      <biological_entity constraint="lower" id="o15992" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 2.4-3.2 mm;</text>
      <biological_entity constraint="upper" id="o15993" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 1.6-3 mm, folded, scabridulous above midlength, awned, awns 5-12 mm;</text>
      <biological_entity id="o15994" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="folded" value_original="folded" />
        <character constraint="above awns" constraintid="o15995" is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o15995" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="true" name="position" src="d0_s12" value="midlength" value_original="midlength" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1-2 mm, greenish to yellowish, often purple-tinged.</text>
      <biological_entity id="o15996" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s13" to="yellowish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1-1.5 mm. 2n = 14.</text>
      <biological_entity id="o15997" name="caryopse" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15998" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Apera spica-venti grows as a weed in lawns, waste places, grain fields, sandy ground, and roadsides. Introduced from Europe, it is found in scattered locations in the Flora region.</discussion>
  
</bio:treatment>