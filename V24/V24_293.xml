<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Wagnon" date="unknown" rank="species">pseudolaevipes</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species pseudolaevipes</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>14</number>
  <other_name type="common_name">Woodland brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o5468" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-120 cm, erect or spreading;</text>
      <biological_entity id="o5469" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 4-6, pubescent or puberulent;</text>
      <biological_entity id="o5470" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes mostly glabrous, sometimes pubescent to puberulent just below the nodes.</text>
      <biological_entity id="o5471" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" constraint="just below nodes" constraintid="o5472" from="pubescent" modifier="sometimes" name="pubescence" src="d0_s4" to="puberulent" />
      </biological_entity>
      <biological_entity id="o5472" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths glabrous or pilose, often pilose near the auricles;</text>
      <biological_entity id="o5473" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character constraint="near auricles" constraintid="o5474" is_modifier="false" modifier="often" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o5474" name="auricle" name_original="auricles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>auricles usually present on the lower leaves, rarely absent;</text>
      <biological_entity id="o5475" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character constraint="on lower leaves" constraintid="o5476" is_modifier="false" modifier="usually" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5476" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" notes="" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules to 1.5 mm, usually pubescent, sometimes glabrous, truncate to obtuse, laciniate, ciliolate;</text>
      <biological_entity id="o5477" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 10-25 cm long, 3-9 mm wide, flat, glabrous, pilose on the margins or throughout.</text>
      <biological_entity id="o5478" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character constraint="on margins" constraintid="o5479" is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o5479" name="margin" name_original="margins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles 10-20 cm, open, usually nodding;</text>
      <biological_entity id="o5480" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches ascending to spreading or reflexed.</text>
      <biological_entity id="o5481" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="spreading or reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 15-35 mm, elliptic to lanceolate, terete to moderately laterally compressed, with 4-10 florets.</text>
      <biological_entity id="o5482" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o5483" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <relation from="o5482" id="r886" name="with" negation="false" src="d0_s11" to="o5483" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes usually pubescent, rarely glabrous, sometimes scabrous, margins often bronze-tinged;</text>
      <biological_entity id="o5484" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5485" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s12" value="bronze-tinged" value_original="bronze-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 4-7 mm, 3-veined;</text>
      <biological_entity constraint="lower" id="o5486" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 6.5-9 mm, (3) 5-veined;</text>
      <biological_entity constraint="upper" id="o5487" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="(3)5-veined" value_original="(3)5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 10-13 mm, elliptic to lanceolate, rounded over the midvein, backs usually pubescent, sometimes glabrous distally, margins often bronze-tinged, pubescent nearly throughout, apices acute to obtuse, entire, rarely slightly emarginate, lobes shorter than 1 mm;</text>
      <biological_entity id="o5488" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="13" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="lanceolate" />
        <character constraint="over midvein" constraintid="o5489" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5489" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o5490" name="back" name_original="backs" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5491" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s15" value="bronze-tinged" value_original="bronze-tinged" />
        <character is_modifier="false" modifier="nearly throughout; throughout" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5492" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely slightly" name="architecture_or_shape" src="d0_s15" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o5493" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 3-5 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o5494" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o5495" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o5495" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 3.5-5 mm. 2n = 14.</text>
      <biological_entity id="o5496" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5497" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus pseudolaevipes grows in dry, shaded or semishaded sites in chaparral, coastal sage scrub, and woodland-savannah zones, from near sea level to about 900 m, in central and southern California. It is not known from Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>