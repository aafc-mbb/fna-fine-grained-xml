<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Mansf. ex Tsitsin &amp; K.A. Petrova" date="unknown" rank="genus">×ELYHORDEUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×elyhordeum</taxon_hierarchy>
  </taxon_identification>
  <number>13.12</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, occasionally shortly rhizomatous.</text>
      <biological_entity id="o11555" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="occasionally shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences terminal, spikes or spikelike, with 1-3 (7) spikelets per node, lateral spikelets usually shortly pedicellate, central spikelets sessile or nearly so;</text>
      <biological_entity id="o11556" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o11557" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o11558" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s2" value="7" value_original="7" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o11559" name="node" name_original="node" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o11560" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually shortly" name="architecture" src="d0_s2" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o11557" id="r1835" name="with" negation="false" src="d0_s2" to="o11558" />
      <relation from="o11558" id="r1836" name="per" negation="false" src="d0_s2" to="o11559" />
    </statement>
    <statement id="d0_s3">
      <text>disarticulation tardy, at the rachis nodes and beneath the florets.</text>
      <biological_entity constraint="central" id="o11561" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s2" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o11562" name="floret" name_original="florets" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spikelets with 1-4 florets.</text>
      <biological_entity id="o11563" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity id="o11564" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <relation from="o11563" id="r1837" name="with" negation="false" src="d0_s4" to="o11564" />
    </statement>
    <statement id="d0_s5">
      <text>Glumes subulate to narrowly lanceolate, usually awned;</text>
      <biological_entity id="o11565" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lemmas usually awned;</text>
      <biological_entity id="o11566" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers sterile.</text>
      <biological_entity id="o11567" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Caryopses rarely formed.</text>
      <biological_entity id="o11568" name="caryopse" name_original="caryopses" src="d0_s8" type="structure" />
    </statement>
  </description>
  <discussion>×Elyhordeum is the name given to hybrids between Elymus and Hordeum. These hybrids are fairly common. All appear to be sterile, i.e., they do not produce good pollen or set seed. The descriptions should be treated with reservation because, in some instances, only type specimens have been examined. For that reason, no key is provided. Only named hybrids are described and illustrated.</discussion>
  <discussion>Interspecific hybrids between Elymus elymoides or E. multisetus and other species of Elymus resemble the ×Elyhordeum hybrids in having tardily disarticulating, spikelike inflorescences and awned glumes and lemmas, but are more likely to have solitary spikelets, even at the lowest node. Distinguishing between them and ×Elyhordeum hybrids, without knowledge of other species of Triticeae at a site, is challenging.</discussion>
  <discussion>Inflorescence measurements, unless stated otherwise, do not include the awns.</discussion>
  <references>
    <reference>Bowden, W.M. 1958. Natural and artificial ×Elymordeum hybrids. Canad. J. Bot. 36:101-123</reference>
    <reference>Bowden, W.M. 1960. Typification of Elymus macounii Vasey. Bull. Torrey Bot. Club. 87:205-208</reference>
    <reference>Jordal, L.K. 1951. A floristic and phytogeographic survey of the southern slopes of the Brooks Range, Alaska. Ph.D. dissertation, University of Michigan, Ann Arbor, Michigan, U.S.A. 411 pp.</reference>
    <reference>Mitchell, W.W. and H.J. Hodgson. 1965. A new ×Agrohordeum from Alaska. Bull. Torrey Bot. Club 92:403-407.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Oreg.;Kans.;N.Dak.;Nebr.;S.Dak.;Wyo.;N.Mex.;Calif.;Mont.;Alaska;Nev.;Colo.;Alta.;B.C.;Man.;N.S.;N.W.T..;Ont.;Que.;Sask.;Yukon;Ill.;Iowa;Idaho;Utah;Mo.;Minn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" value="N.W.T" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>