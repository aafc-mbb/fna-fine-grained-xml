<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lynn G. Clark;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Moench" date="unknown" rank="genus">LAMARCKIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus lamarckia</taxon_hierarchy>
  </taxon_identification>
  <number>14.11</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o28752" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-40 cm, erect or decumbent at the base, glabrous.</text>
      <biological_entity id="o28753" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o28754" is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28754" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open for at least 1/3 their length, glabrous, margins membranous and continuous with the ligules;</text>
      <biological_entity id="o28755" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character modifier="at least" name="length" src="d0_s3" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28756" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character constraint="with ligules" constraintid="o28757" is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o28757" name="ligule" name_original="ligules" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o28758" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, acute, glabrous, apices somewhat erose;</text>
      <biological_entity id="o28759" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28760" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat, glabrous.</text>
      <biological_entity id="o28761" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences panicles, dense, secund, golden-yellow to purplish;</text>
      <biological_entity constraint="inflorescences" id="o28762" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="secund" value_original="secund" />
        <character char_type="range_value" from="golden-yellow" name="coloration" src="d0_s7" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches appressed to the rachis;</text>
      <biological_entity constraint="primary" id="o28763" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="to rachis" constraintid="o28764" is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o28764" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches capillary, smooth, glabrous, flexuous, terminating in 1-4 fascicles of pedicellate spikelets, strongly bent below the junction with the fascicle base;</text>
      <biological_entity constraint="spikelet" id="o28765" name="branch" name_original="branches" src="d0_s9" type="structure" constraint_original="spikelet secondary; spikelet">
        <character is_modifier="false" name="shape" src="d0_s9" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="course" src="d0_s9" value="flexuous" value_original="flexuous" />
        <character constraint="below junction" constraintid="o28767" is_modifier="false" modifier="strongly" name="shape" src="d0_s9" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o28766" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o28767" name="junction" name_original="junction" src="d0_s9" type="structure" />
      <biological_entity constraint="fascicle" id="o28768" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o28765" id="r4513" name="part_of" negation="false" src="d0_s9" to="o28766" />
      <relation from="o28767" id="r4514" name="with" negation="false" src="d0_s9" to="o28768" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels of spikelets in each fascicle fused at the base, strigose.</text>
      <biological_entity id="o28769" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o28770" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o28771" name="fascicle" name_original="fascicle" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o28772" is_modifier="false" name="fusion" src="d0_s10" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o28772" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o28769" id="r4515" name="part_of" negation="false" src="d0_s10" to="o28770" />
      <relation from="o28769" id="r4516" name="in" negation="false" src="d0_s10" to="o28771" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets dimorphic, the terminal spikelet of each fascicle fertile, the others sterile;</text>
      <biological_entity id="o28773" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o28774" name="spikelet" name_original="spikelet" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o28775" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
      <relation from="o28774" id="r4517" name="part_of" negation="false" src="d0_s11" to="o28775" />
    </statement>
    <statement id="d0_s12">
      <text>disarticulation at the base of the fused pedicels.</text>
      <biological_entity id="o28776" name="other" name_original="others" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o28777" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o28778" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="fused" value_original="fused" />
      </biological_entity>
      <relation from="o28776" id="r4518" name="at" negation="false" src="d0_s12" to="o28777" />
      <relation from="o28777" id="r4519" name="part_of" negation="false" src="d0_s12" to="o28778" />
    </statement>
    <statement id="d0_s13">
      <text>Fertile spikelets terete to somewhat laterally compressed, with 2 florets, 1 bisexual, the other a rudiment, borne on long rachilla internodes;</text>
      <biological_entity id="o28779" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s13" value="terete to somewhat" value_original="terete to somewhat" />
        <character is_modifier="false" modifier="somewhat; laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o28780" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o28781" name="rudiment" name_original="rudiment" src="d0_s13" type="structure" />
      <biological_entity constraint="rachilla" id="o28782" name="internode" name_original="internodes" src="d0_s13" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s13" value="long" value_original="long" />
      </biological_entity>
      <relation from="o28779" id="r4520" name="with" negation="false" src="d0_s13" to="o28780" />
      <relation from="o28779" id="r4521" name="borne on" negation="false" src="d0_s13" to="o28782" />
    </statement>
    <statement id="d0_s14">
      <text>calluses short, blunt, glabrous;</text>
      <biological_entity id="o28783" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s14" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>glumes narrow, acuminate or short-awned, 1-veined;</text>
      <biological_entity id="o28784" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="short-awned" value_original="short-awned" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>bisexual lemmas scarcely veined, each with a delicate, straight, subapical awn;</text>
      <biological_entity id="o28785" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="scarcely" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o28786" name="awn" name_original="awn" src="d0_s16" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s16" value="delicate" value_original="delicate" />
        <character is_modifier="true" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o28785" id="r4522" name="with" negation="false" src="d0_s16" to="o28786" />
    </statement>
    <statement id="d0_s17">
      <text>paleas equal or subequal to the lemmas;</text>
      <biological_entity id="o28787" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character constraint="to lemmas" constraintid="o28788" is_modifier="false" name="size" src="d0_s17" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o28788" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lodicules 2, glabrous, toothed;</text>
      <biological_entity id="o28789" name="lodicule" name_original="lodicules" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s18" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 3;</text>
      <biological_entity id="o28790" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovaries glabrous;</text>
      <biological_entity id="o28791" name="ovary" name_original="ovaries" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>rudimentary florets highly reduced lemmas, each with a delicate, straight awn.</text>
      <biological_entity id="o28792" name="floret" name_original="florets" src="d0_s21" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s21" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o28793" name="lemma" name_original="lemmas" src="d0_s21" type="structure">
        <character is_modifier="true" modifier="highly" name="size" src="d0_s21" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o28794" name="awn" name_original="awn" src="d0_s21" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s21" value="delicate" value_original="delicate" />
        <character is_modifier="true" name="course" src="d0_s21" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o28792" id="r4523" name="with" negation="false" src="d0_s21" to="o28794" />
    </statement>
    <statement id="d0_s22">
      <text>Sterile spikelets linear, laterally compressed;</text>
      <biological_entity id="o28795" name="spikelet" name_original="spikelets" src="d0_s22" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s22" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s22" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>florets 5-10;</text>
      <biological_entity id="o28796" name="floret" name_original="florets" src="d0_s23" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s23" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>glumes similar to those of the fertile spikelets;</text>
      <biological_entity id="o28797" name="glume" name_original="glumes" src="d0_s24" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet" />
      <biological_entity id="o28798" name="spikelet" name_original="spikelets" src="d0_s24" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s24" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o28797" id="r4524" name="part_of" negation="false" src="d0_s24" to="o28798" />
    </statement>
    <statement id="d0_s25">
      <text>lemmas imbricate, obtuse, unawned;</text>
      <biological_entity id="o28799" name="lemma" name_original="lemmas" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s25" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s25" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>paleas absent.</text>
      <biological_entity id="o28800" name="palea" name_original="paleas" src="d0_s26" type="structure">
        <character is_modifier="false" name="presence" src="d0_s26" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Caryopses ovoid or ellipsoid, adhering to the lemmas and/or paleas.</text>
      <biological_entity id="o28802" name="lemma" name_original="lemmas" src="d0_s27" type="structure" />
      <biological_entity id="o28803" name="palea" name_original="paleas" src="d0_s27" type="structure" />
      <relation from="o28801" id="r4525" name="adhering to" negation="false" src="d0_s27" to="o28802" />
    </statement>
    <statement id="d0_s28">
      <text>x = 7.</text>
      <biological_entity id="o28801" name="caryopse" name_original="caryopses" src="d0_s27" type="structure">
        <character is_modifier="false" name="shape" src="d0_s27" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s27" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o28804" name="chromosome" name_original="" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lamarckia is a monotypic genus that has been introduced into North America from the Mediterranean and the Middle East.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.;Pacific Islands (Hawaii);Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>