<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">578</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Stenopoa</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="species">glauca</taxon_name>
    <taxon_name authority="(Nash) W.A. Weber" date="unknown" rank="subspecies">rupicola</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section stenopoa;species glauca;subspecies rupicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rupicola</taxon_name>
    <taxon_hierarchy>genus poa;species rupicola</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Timberline bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 5-15 cm.</text>
      <biological_entity id="o25965" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Panicles 1-5 cm, usually narrowly lanceoloid.</text>
      <biological_entity id="o25966" name="panicle" name_original="panicles" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s1" value="lanceoloid" value_original="lanceoloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikelets not bulbiferous;</text>
      <biological_entity id="o25967" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>florets normal.</text>
      <biological_entity id="o25968" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="normal" value_original="normal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calluses glabrous;</text>
      <biological_entity id="o25969" name="callus" name_original="calluses" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lemmas at least sparsely puberulent on the intercostal regions.</text>
      <biological_entity constraint="intercostal" id="o25971" name="region" name_original="regions" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 48, 48-50, 56, 56-58, ca. 100.</text>
      <biological_entity id="o25970" name="lemma" name_original="lemmas" src="d0_s5" type="structure">
        <character constraint="on intercostal regions" constraintid="o25971" is_modifier="false" modifier="at-least sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25972" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit=",-50,,-58," value="48" value_original="48" />
        <character char_type="range_value" from="48" name="quantity" src="d0_s6" to="50" unit=",-50,,-58," />
        <character name="quantity" src="d0_s6" unit=",-50,,-58," value="56" value_original="56" />
        <character char_type="range_value" from="56" name="quantity" src="d0_s6" to="58" unit=",-50,,-58," />
        <character name="quantity" src="d0_s6" unit=",-50,,-58," value="100" value_original="100" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa glauca subsp. rupicola is endemic to dry alpine areas of western North America. It is often confused in herbaria with subsp. glauca and P. interior (p. 576), but its calluses lack even a vestige of a web, and its lemmas have at least a few hairs between the lemma veins. It is often sympatric with both taxa outside of California. It is not common in the northern Rocky Mountains.</discussion>
  
</bio:treatment>