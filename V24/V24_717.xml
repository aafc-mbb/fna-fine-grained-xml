<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">514</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="V.L. Marsh ex Soreng" date="unknown" rank="section">Sylvestres</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">wolfii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section sylvestres;species wolfii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Wolf's bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous, not stoloniferous, loosely tufted.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly pseudointravaginal.</text>
      <biological_entity id="o31671" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="pseudointravaginal" value_original="pseudointravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 25-90 cm.</text>
      <biological_entity id="o31672" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s3" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/2 - 3/4 their length, smooth or sparsely scabrous, margins not ciliate;</text>
      <biological_entity id="o31673" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31674" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-2.1 mm, smooth or sparsely scabrous, truncate to obtuse, ciliolate;</text>
      <biological_entity id="o31675" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 0.6-3.5 mm wide, flat.</text>
      <biological_entity id="o31676" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 7.5-15 (18) cm, lax, pyramidal, open, sparse;</text>
      <biological_entity id="o31677" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="18" value_original="18" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="count_or_density" src="d0_s7" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nodes with 1-3 (5) branches;</text>
      <biological_entity id="o31678" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o31679" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o31678" id="r4973" name="with" negation="false" src="d0_s8" to="o31679" />
    </statement>
    <statement id="d0_s9">
      <text>branches 3-8 cm, ascending, straight to spreading, angled, angles prominent, scabrous.</text>
      <biological_entity id="o31680" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="8" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o31681" name="angle" name_original="angles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-6.5 mm, laterally compressed;</text>
      <biological_entity id="o31682" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2-5;</text>
      <biological_entity id="o31683" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes to 1 mm, smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o31684" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes 1/2-1/3 the length of the adjacent lemmas, distinctly keeled, keels scabrous;</text>
      <biological_entity id="o31685" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s13" to="1/3" />
        <character is_modifier="false" modifier="distinctly" name="length" notes="" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o31686" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o31687" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes subulate to narrowly lanceolate, (1) 3-veined;</text>
      <biological_entity constraint="lower" id="o31688" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="narrowly lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="(1)3-veined" value_original="(1)3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o31689" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o31690" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o31690" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses webbed;</text>
      <biological_entity id="o31691" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas (2.5) 3.2-4.7 mm, lanceolate, green, distinctly keeled, keels and marginal veins long-villous, hairs extending up almost the whole keel length, lateral-veins prominent, intercostal regions smooth, minutely bumpy, usually glabrous, rarely sparsely softly puberulent, apices acute, blunt, or pointed, white, not bronze;</text>
      <biological_entity id="o31692" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s17" to="4.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o31693" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o31694" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o31695" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <biological_entity id="o31696" name="keel" name_original="keel" src="d0_s17" type="structure" />
      <biological_entity id="o31697" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="length" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o31698" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s17" value="bumpy" value_original="bumpy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely softly" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o31699" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s17" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pointed" value_original="pointed" />
        <character is_modifier="false" name="shape" src="d0_s17" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pointed" value_original="pointed" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s17" value="bronze" value_original="bronze" />
      </biological_entity>
      <relation from="o31695" id="r4974" name="extending up" negation="false" src="d0_s17" to="o31696" />
    </statement>
    <statement id="d0_s18">
      <text>palea keels softly puberulent at midlength, apices scabrous;</text>
      <biological_entity constraint="palea" id="o31700" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character constraint="at midlength" constraintid="o31701" is_modifier="false" modifier="softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o31701" name="midlength" name_original="midlength" src="d0_s18" type="structure" />
      <biological_entity id="o31702" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers (0.5) 0.8-1.2 (1.5) mm.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 28.</text>
      <biological_entity id="o31703" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s19" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31704" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa wolfii is an uncommon species that grows in boggy areas of eastern deciduous forests, primarily west of the Appalachian divide. It differs from P. sylvestris (p. 512) in having fewer branches, larger spikelets, and lemmas that are usually glabrous between the veins.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Ohio;Mo.;Minn.;Wis.;Ark.;Ill.;Ind.;Iowa</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>