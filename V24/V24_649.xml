<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">452</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="C.C. Gmel." date="unknown" rank="genus">VULPIA</taxon_name>
    <taxon_name authority="(Nutt.) Munro" date="unknown" rank="species">microstachys</taxon_name>
    <taxon_name authority="(Scribn. ex Beal) Lonard &amp; Gould" date="unknown" rank="variety">pauciflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus vulpia;species microstachys;variety pauciflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vulpia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reflexa</taxon_name>
    <taxon_hierarchy>genus vulpia;species reflexa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vulpia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pacifica</taxon_name>
    <taxon_hierarchy>genus vulpia;species pacifica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reflexa</taxon_name>
    <taxon_hierarchy>genus festuca;species reflexa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pacifica</taxon_name>
    <taxon_hierarchy>genus festuca;species pacifica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">microstachys</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">simulans</taxon_name>
    <taxon_hierarchy>genus festuca;species microstachys;variety simulans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">microstachys</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pauciflora</taxon_name>
    <taxon_hierarchy>genus festuca;species microstachys;variety pauciflora</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Pacific fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Spikelets with 1-6 florets.</text>
      <biological_entity id="o29718" name="spikelet" name_original="spikelets" src="d0_s0" type="structure" />
      <biological_entity id="o29719" name="floret" name_original="florets" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="6" />
      </biological_entity>
      <relation from="o29718" id="r4682" name="with" negation="false" src="d0_s0" to="o29719" />
    </statement>
    <statement id="d0_s1">
      <text>Glumes and lemmas smooth or scabrous.</text>
      <biological_entity id="o29720" name="glume" name_original="glumes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29721" name="lemma" name_original="lemmas" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vulpia microstachys var. pauciflora grows in sandy, often disturbed sites, and is the most common and widespread variety of the complex. It is often intermingled with plants of the other varieties.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Wash.;B.C.;Utah;Calif.;Oreg.;Mont.;Ariz.;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>