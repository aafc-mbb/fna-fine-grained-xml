<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">MELICA</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">porteri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus melica;species porteri</taxon_hierarchy>
  </taxon_identification>
  <number>14</number>
  <other_name type="common_name">Porter's melic</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not or loosely cespitose, shortly rhizomatous.</text>
      <biological_entity id="o15461" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 55-100 cm, not forming corms;</text>
      <biological_entity id="o15462" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="55" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15463" name="corm" name_original="corms" src="d0_s1" type="structure" />
      <relation from="o15462" id="r2479" name="forming" negation="true" src="d0_s1" to="o15463" />
    </statement>
    <statement id="d0_s2">
      <text>internodes smooth, basal internodes not thickened.</text>
      <biological_entity id="o15464" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15465" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths often scabrous on the keels, otherwise smooth;</text>
      <biological_entity id="o15466" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="on keels" constraintid="o15467" is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_pubescence_or_relief" notes="" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o15467" name="keel" name_original="keels" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-7 mm;</text>
      <biological_entity id="o15468" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-5 mm wide, both surfaces glabrous, scabridulous.</text>
      <biological_entity id="o15469" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15470" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 13-25 cm;</text>
      <biological_entity id="o15471" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches 1-9 cm, straight and appressed or flexible and ascending to strongly divergent, with 1-12 spikelets;</text>
      <biological_entity id="o15472" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o15473" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <relation from="o15472" id="r2480" name="with" negation="false" src="d0_s7" to="o15473" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels sharply bent below the spikelets;</text>
      <biological_entity id="o15475" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation below the glumes.</text>
      <biological_entity id="o15474" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="below spikelets" constraintid="o15475" is_modifier="false" modifier="sharply" name="shape" src="d0_s8" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o15476" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o15474" id="r2481" name="below" negation="false" src="d0_s9" to="o15476" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 8-16 mm long, 1.5-5 mm wide, parallel-sided when mature, with 2-5 bisexual florets;</text>
      <biological_entity id="o15477" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="when mature" name="architecture" src="d0_s10" value="parallel-sided" value_original="parallel-sided" />
      </biological_entity>
      <biological_entity id="o15478" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o15477" id="r2482" name="with" negation="false" src="d0_s10" to="o15478" />
    </statement>
    <statement id="d0_s11">
      <text>rachilla internodes 1.9-2.1 mm.</text>
      <biological_entity constraint="rachilla" id="o15479" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes green, pale, or purplish-tinged;</text>
      <biological_entity id="o15480" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish-tinged" value_original="purplish-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 3.5-6 mm long, 2-3 mm wide, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o15481" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 5-8 mm long, 2-3 mm wide, 5-veined;</text>
      <biological_entity constraint="upper" id="o15482" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 6-10 mm, glabrous, chartaceous on the distal 1/3, 5-11-veined, veins conspicuous, apices rounded to acute, unawned;</text>
      <biological_entity id="o15483" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character constraint="on distal 1/3" constraintid="o15484" is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s15" value="5-11-veined" value_original="5-11-veined" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15484" name="1/3" name_original="1/3" src="d0_s15" type="structure" />
      <biological_entity id="o15485" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o15486" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s15" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas about 2/3 the length of the lemmas;</text>
      <biological_entity id="o15487" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o15488" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1-2.5 mm;</text>
      <biological_entity id="o15489" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>rudiments 1.8-5 mm, acute to acuminate, resembling the bisexual florets.</text>
      <biological_entity id="o15491" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s18" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o15490" id="r2483" name="resembling the" negation="false" src="d0_s18" to="o15491" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 18.</text>
      <biological_entity id="o15490" name="rudiment" name_original="rudiments" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s18" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15492" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melica porteri grows on rocky slopes and in open woods, often near streams. It grows from Colorado and Arizona to central Texas and northern Mexico. Living plants are sometimes confused with Bouteloua curtipendula; the similarity is superficial.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Utah;N.Mex.;Tex.;Kans.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches flexible, ascending to strongly divergent; glumes purplish-tinged</description>
      <determination>Melica porteri var. laxa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches straight, appressed; glumes green or pale</description>
      <determination>Melica porteri var. porteri</determination>
    </key_statement>
  </key>
</bio:treatment>