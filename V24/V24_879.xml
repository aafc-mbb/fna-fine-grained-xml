<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sandy Long;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">618</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Seidl" date="unknown" rank="genus">COLEANTHUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus coleanthus</taxon_hierarchy>
  </taxon_identification>
  <number>14.23</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, tufted.</text>
      <biological_entity id="o872" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 2-7 (10) cm, ascending or decumbent.</text>
      <biological_entity id="o873" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths closed, inflated;</text>
      <biological_entity id="o874" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="condition" src="d0_s2" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles absent;</text>
      <biological_entity id="o875" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ovate;</text>
      <biological_entity id="o876" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat.</text>
      <biological_entity id="o877" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences panicles;</text>
      <biological_entity constraint="inflorescences" id="o878" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>branches distant, verticillate.</text>
      <biological_entity id="o879" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="distant" value_original="distant" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="verticillate" value_original="verticillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets slightly laterally compressed, pedicellate, pedicels hairy, with 1 floret;</text>
      <biological_entity id="o880" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o881" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o882" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <relation from="o881" id="r145" name="with" negation="false" src="d0_s8" to="o882" />
    </statement>
    <statement id="d0_s9">
      <text>rachillas not prolonged beyond the base of the floret;</text>
      <biological_entity id="o884" name="base" name_original="base" src="d0_s9" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity id="o885" name="floret" name_original="floret" src="d0_s9" type="structure" />
      <relation from="o884" id="r146" name="part_of" negation="false" src="d0_s9" to="o885" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation below the floret.</text>
      <biological_entity id="o883" name="rachilla" name_original="rachillas" src="d0_s9" type="structure">
        <character constraint="beyond base" constraintid="o884" is_modifier="false" modifier="not" name="length" src="d0_s9" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o886" name="floret" name_original="floret" src="d0_s10" type="structure" />
      <relation from="o883" id="r147" name="below" negation="false" src="d0_s10" to="o886" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes absent;</text>
      <biological_entity id="o887" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses short, glabrous;</text>
      <biological_entity id="o888" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 1-veined, narrowed to awnlike apices;</text>
      <biological_entity id="o889" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="narrowed" name="shape" src="d0_s13" to="awnlike" />
      </biological_entity>
      <biological_entity id="o890" name="apex" name_original="apices" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 2-4-toothed, 2-keeled;</text>
      <biological_entity id="o891" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="2-4-toothed" value_original="2-4-toothed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lodicules absent;</text>
      <biological_entity id="o892" name="lodicule" name_original="lodicules" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 2;</text>
      <biological_entity id="o893" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovaries glabrous.</text>
      <biological_entity id="o894" name="ovary" name_original="ovaries" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses elliptic, exceeding the lemmas and paleas, falling free;</text>
      <biological_entity id="o895" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s18" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="life_cycle" src="d0_s18" value="falling" value_original="falling" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o896" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o897" name="palea" name_original="paleas" src="d0_s18" type="structure" />
      <relation from="o895" id="r148" name="exceeding the" negation="false" src="d0_s18" to="o896" />
      <relation from="o895" id="r149" name="exceeding the" negation="false" src="d0_s18" to="o897" />
    </statement>
    <statement id="d0_s19">
      <text>hila oval, x = 7.</text>
      <biological_entity id="o898" name="hilum" name_original="hila" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oval" value_original="oval" />
      </biological_entity>
      <biological_entity constraint="x" id="o899" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Coleanthus is a monospecific genus that is native to Eurasia and North America.</discussion>
  <references>
    <reference>Bernhardt, K.G., M, Koch, E. Ulbel, and J. Webhofer. 2004. The soil seed bank as a resource for in situ and ex situ conservation of extinct species. [Pagination unknown] in E. Robbrecht and A. Bogaerts (eds.). EuroGard III. Scripta Botanica Belgica Series, No. 29. National Botanic Garden, Meise, Belgium. 177 pp.</reference>
    <reference>Conert, HJ. 1992. Coleanthus. Pp. 434-437 in G. Hegi. Illustrierte Flora von Mitteleuropa, ed. 3. Band I, Teil 3, Lieferung 6 (pp. 401—480). Verlag Paul Parey, Berlin and Hamburg, Germany</reference>
    <reference>Hejny, S. 1969. Coleanthus subtilis (Tratt.) Seidl in der Tschechoslowakei. Folia Geobot. Phytotax. 4:345-399</reference>
    <reference>Neiajev, A.P. and A.A. Necajev. 1972. Coleanthus subtilis (Tratt.) Seidl in the Amur Basin. Folia Geobot. Phytotax. 7:339-347</reference>
    <reference>Woike, S. 1969. Beitrag zum Vorkommen von Coleanthus subtilis (Tratt.) Seidl (Femes Scheidenbliitgras) in Europa. Folia Geobot. Phytotax. 4:401-413.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>