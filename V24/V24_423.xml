<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="species">curvatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species curvatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">submuticus</taxon_name>
    <taxon_hierarchy>genus elymus;species virginicus;variety submuticus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">jenkinsii</taxon_name>
    <taxon_hierarchy>genus elymus;species virginicus;variety jenkinsii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">submuticus</taxon_name>
    <taxon_hierarchy>genus elymus;species submuticus</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Awnless wlldrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous, often glaucous.</text>
      <biological_entity id="o31631" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 60-110 cm, stiffly erect, or the base sometimes geniculate;</text>
      <biological_entity id="o31632" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s1" to="110" to_unit="cm" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o31633" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 6-9, concealed or exposed, glabrous.</text>
      <biological_entity id="o31634" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="9" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evenly distributed;</text>
      <biological_entity id="o31635" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous, often reddish-brown;</text>
      <biological_entity id="o31636" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles to 1 mm, sometimes absent;</text>
      <biological_entity id="o31637" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules shorter than 1 mm, ciliolate;</text>
      <biological_entity id="o31638" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 5-15 mm wide, the lower blades usually lax, shorter, narrower, and senescing earlier, the upper blades usually ascending and somewhat involute, adaxial surfaces smooth or scabridulous, occasionally scabrous.</text>
      <biological_entity id="o31639" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o31640" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s7" value="narrower" value_original="narrower" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="somewhat" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="upper" id="o31641" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s7" value="earlier" value_original="earlier" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31642" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o31640" id="r4965" name="senescing" negation="false" src="d0_s7" to="o31641" />
    </statement>
    <statement id="d0_s8">
      <text>Spikes 9-15 cm long, (0.5) 0.7-1.3 cm wide, erect, exserted or the bases slightly sheathed, with 2 spikelets per node;</text>
      <biological_entity id="o31643" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s8" to="1.3" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o31644" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o31645" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o31646" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o31644" id="r4966" name="with" negation="false" src="d0_s8" to="o31645" />
      <relation from="o31645" id="r4967" name="per" negation="false" src="d0_s8" to="o31646" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 2.5-4.5 mm long, about 0.25-5 mm thick at the thinnest sections, smooth or scabrous beneath the spikelets.</text>
      <biological_entity id="o31647" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" constraint="at sections" constraintid="o31648" from="0.25" from_unit="mm" name="thickness" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="beneath spikelets" constraintid="o31649" is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31648" name="section" name_original="sections" src="d0_s9" type="structure" />
      <biological_entity id="o31649" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 10-15 mm, appressed, often reddish-brown at maturity, with (2) 3-4 (5) florets, lowest florets functional;</text>
      <biological_entity id="o31650" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character constraint="at maturity" is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity id="o31651" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o31650" id="r4968" name="with" negation="false" src="d0_s10" to="o31651" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation below the glumes and beneath the florets, or the lowest floret falling with the glumes.</text>
      <biological_entity constraint="lowest" id="o31652" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="function" src="d0_s10" value="functional" value_original="functional" />
      </biological_entity>
      <biological_entity id="o31653" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity constraint="lowest" id="o31654" name="floret" name_original="floret" src="d0_s11" type="structure">
        <character constraint="below the glumes and beneath lowest floret" constraintid="o31656" is_modifier="false" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o31655" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity constraint="lowest" id="o31656" name="floret" name_original="floret" src="d0_s11" type="structure" />
      <relation from="o31652" id="r4969" name="below the glumes and beneath" negation="false" src="d0_s11" to="o31653" />
      <relation from="o31652" id="r4970" name="below the glumes and beneath" negation="false" src="d0_s11" to="o31654" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes equal or subequal, the basal 2-3 mm terete, indurate, strongly bowed out, without evident venation, glume bodies 7-15 mm long, 1.2-2.1 mm wide, linear-lanceolate, widening above the base, 3-5-veined, usually glabrous or scabrous, occasionally hispidulous, rarely hirsute on the veins, margins firm, awns 0-3 (5) mm;</text>
      <biological_entity id="o31657" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="position" src="d0_s12" value="basal" value_original="basal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s12" value="bowed" value_original="bowed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character constraint="above base" constraintid="o31659" is_modifier="false" name="width" src="d0_s12" value="widening" value_original="widening" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
        <character constraint="on veins" constraintid="o31660" is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="glume" id="o31658" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="evident" value_original="evident" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" notes="" src="d0_s12" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31659" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o31660" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o31661" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o31662" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o31657" id="r4971" name="without" negation="false" src="d0_s12" to="o31658" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 6-10 mm, glabrous or scabrous, rarely hirsute, awns (0.5) 1-3 (4) mm, rarely 5-10 mm on the lemmas of the distal spikelets, straight;</text>
      <biological_entity id="o31663" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o31664" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" constraint="on lemmas" constraintid="o31665" from="5" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="course" notes="" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o31665" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity constraint="distal" id="o31666" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o31665" id="r4972" name="part_of" negation="false" src="d0_s13" to="o31666" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 6-10 mm, obtuse, often emarginate;</text>
      <biological_entity id="o31667" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s14" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-3 mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>Anthesis late June to mid-august.</text>
      <biological_entity id="o31669" name="mid-august" name_original="mid-august" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 28, 42.</text>
      <biological_entity id="o31668" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character constraint="to mid-august" constraintid="o31669" is_modifier="false" name="life_cycle" src="d0_s16" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31670" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus curvatus grows in moist or damp soils of open forests, thickets, grasslands, ditches, and disturbed ground, especially on bottomland. It is widespread from British Columbia and Washington, through the Intermountain region and northern Rockies, to the northern Great Plains. It is infrequent or rare in the midwest, the Great Lakes region, and the northeast, and is virtually unknown in the southeast. It is similar to E. virginicus (p. 298), and has sometimes been included in that species as E. virginicus var. submuticus Hook., but is more distinct than the varieties of E. virginicus treated above. Although E. virginicus and E. curvatus overlap greatly in range, E. curvatus usually has a distinct growth form, and its anthesis is 1-2 weeks later (Brooks 1974). Its spikes range from being completely exserted, especially west of the Great Plains, to largely sheathed, especially east of the Mississippi River and in more stressed environments. This geographic trend parallels that within E. virginicus, but sheathed plants of E. curvatus can usually be distinguished by their short awns. Clear transitions to E. virginicus, usually var. jejunus, are rare, but, especially from Missouri to Wisconsin, there are occasional plants with 5-10 mm awns on a few lemmas, especially at the spike tips. Rarely, plants from Missouri and Iowa to Quebec have hispid to hirsute spikelets, suggesting introgression with E. virginicus var. intermedius. There are a few records of apparent hybrids with other species.</discussion>
  
</bio:treatment>