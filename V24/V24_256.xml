<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Harz" date="unknown" rank="tribe">BRACHYPODIEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe brachypodieae</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous or cespitose.</text>
      <biological_entity id="o14625" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms annual, not woody, ascending to erect or decumbent, sometimes branching above the base;</text>
      <biological_entity id="o14626" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect or decumbent" />
        <character constraint="above base" constraintid="o14627" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o14627" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes hollow.</text>
      <biological_entity id="o14628" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths open, margins overlapping for most of their length;</text>
      <biological_entity id="o14629" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o14630" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s4" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>collars without tufts of hair on the sides;</text>
      <biological_entity id="o14631" name="collar" name_original="collars" src="d0_s5" type="structure" />
      <biological_entity id="o14632" name="tuft" name_original="tufts" src="d0_s5" type="structure" />
      <biological_entity id="o14633" name="hair" name_original="hair" src="d0_s5" type="structure" />
      <biological_entity id="o14634" name="side" name_original="sides" src="d0_s5" type="structure" />
      <relation from="o14631" id="r2315" name="without" negation="false" src="d0_s5" to="o14632" />
      <relation from="o14632" id="r2316" name="part_of" negation="false" src="d0_s5" to="o14633" />
      <relation from="o14632" id="r2317" name="on" negation="false" src="d0_s5" to="o14634" />
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o14635" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules membranous, entire or toothed, sometimes shortly ciliate, those of the lower and upper cauline leaves usually similar;</text>
      <biological_entity id="o14636" name="ligule" name_original="ligules" src="d0_s7" type="structure" constraint="lower; lower">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="sometimes shortly" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14637" name="lower" name_original="lower" src="d0_s7" type="structure" />
      <biological_entity constraint="upper cauline" id="o14638" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o14636" id="r2318" name="part_of" negation="false" src="d0_s7" to="o14637" />
    </statement>
    <statement id="d0_s8">
      <text>pseudopetioles absent;</text>
      <biological_entity id="o14639" name="pseudopetiole" name_original="pseudopetioles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades linear to narrowly lanceolate, venation parallel, cross venation not evident, without arm or fusoid cells, cross-sections non-kranz, epidermes without microhairs, not papillate.</text>
      <biological_entity id="o14640" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="narrowly lanceolate" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o14641" name="arm" name_original="arm" src="d0_s9" type="structure" />
      <biological_entity constraint="fusoid" id="o14642" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <biological_entity id="o14643" name="cross-section" name_original="cross-sections" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="non-kranz" value_original="non-kranz" />
      </biological_entity>
      <biological_entity id="o14644" name="epiderme" name_original="epidermes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="relief" notes="" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o14645" name="microhair" name_original="microhairs" src="d0_s9" type="structure" />
      <relation from="o14640" id="r2319" name="without" negation="false" src="d0_s9" to="o14641" />
      <relation from="o14640" id="r2320" name="without" negation="false" src="d0_s9" to="o14642" />
      <relation from="o14644" id="r2321" name="without" negation="false" src="d0_s9" to="o14645" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal, spikelike racemes, spikelets subsessile, solitary at all or most nodes;</text>
      <biological_entity id="o14646" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14647" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o14648" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="subsessile" value_original="subsessile" />
        <character constraint="at nodes" constraintid="o14649" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o14649" name="node" name_original="nodes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pedicels to 2.5 mm.</text>
      <biological_entity id="o14650" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets terete to slightly laterally compressed, with (3) 5-24 florets, distal florets sometimes reduced, sterile;</text>
      <biological_entity id="o14651" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s12" to="slightly laterally compressed" />
      </biological_entity>
      <biological_entity id="o14652" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="3" value_original="3" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s12" to="24" />
      </biological_entity>
      <relation from="o14651" id="r2322" name="with" negation="false" src="d0_s12" to="o14652" />
    </statement>
    <statement id="d0_s13">
      <text>disarticulation above the glumes, beneath the florets;</text>
      <biological_entity constraint="distal" id="o14653" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o14654" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o14655" name="floret" name_original="florets" src="d0_s13" type="structure" />
      <relation from="o14653" id="r2323" name="above" negation="false" src="d0_s13" to="o14654" />
    </statement>
    <statement id="d0_s14">
      <text>rachillas prolonged beyond the base of the distal floret.</text>
      <biological_entity id="o14656" name="rachilla" name_original="rachillas" src="d0_s14" type="structure">
        <character constraint="beyond base" constraintid="o14657" is_modifier="false" name="length" src="d0_s14" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o14657" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="distal" id="o14658" name="floret" name_original="floret" src="d0_s14" type="structure" />
      <relation from="o14657" id="r2324" name="part_of" negation="false" src="d0_s14" to="o14658" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes unequal, 1/2 as long as to equaling the adjacent lemmas, lanceolate, lower glumes 3-7-veined, upper glumes 5-9-veined;</text>
      <biological_entity id="o14659" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="unequal" value_original="unequal" />
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o14660" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14661" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14662" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>florets subterete to slightly laterally compressed;</text>
      <biological_entity id="o14663" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character char_type="range_value" from="subterete" name="shape" src="d0_s16" to="slightly laterally compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses glabrous, not well developed;</text>
      <biological_entity id="o14664" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not well" name="development" src="d0_s17" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas lanceolate, usually membranous, rounded dorsally, (5) 7-9-veined, veins not converging distally, inconspicuous, apices entire, obtuse or acute, unawned or terminally awned;</text>
      <biological_entity id="o14665" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="(5)7-9-veined" value_original="(5)7-9-veined" />
      </biological_entity>
      <biological_entity id="o14666" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not; distally" name="arrangement" src="d0_s18" value="converging" value_original="converging" />
        <character is_modifier="false" name="prominence" src="d0_s18" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o14667" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="terminally" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>paleas shorter than to slightly longer than the lemmas;</text>
      <biological_entity id="o14668" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character constraint="shorter than to slightly longer than the lemmas" constraintid="o14669" is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="shorter than to slightly longer than the lemmas" />
      </biological_entity>
      <biological_entity id="o14670" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s19" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14669" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s19" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lodicules 2, not veined, distal margins ciliate or the apices puberulent;</text>
      <biological_entity id="o14671" name="lodicule" name_original="lodicules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s20" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14672" name="margin" name_original="margins" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s20" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14673" name="apex" name_original="apices" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 3;</text>
      <biological_entity id="o14674" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovaries with hairy apices;</text>
      <biological_entity id="o14675" name="ovary" name_original="ovaries" src="d0_s22" type="structure" />
      <biological_entity id="o14676" name="apex" name_original="apices" src="d0_s22" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s22" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o14675" id="r2325" name="with" negation="false" src="d0_s22" to="o14676" />
    </statement>
    <statement id="d0_s23">
      <text>styles 2, bases free.</text>
      <biological_entity id="o14677" name="style" name_original="styles" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14678" name="base" name_original="bases" src="d0_s23" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s23" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Caryopses with hairy apices, longitudinally grooved;</text>
      <biological_entity id="o14679" name="caryopse" name_original="caryopses" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="architecture" notes="" src="d0_s24" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o14680" name="apex" name_original="apices" src="d0_s24" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s24" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o14679" id="r2326" name="with" negation="false" src="d0_s24" to="o14680" />
    </statement>
    <statement id="d0_s25">
      <text>hila linear;</text>
      <biological_entity id="o14681" name="hilum" name_original="hila" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s25" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>embryos about 1/6 the length of the caryopses.</text>
      <biological_entity id="o14683" name="caryopse" name_original="caryopses" src="d0_s26" type="structure" />
    </statement>
    <statement id="d0_s27">
      <text>x = 5,7,9.</text>
      <biological_entity id="o14682" name="embryo" name_original="embryos" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="1/6" value_original="1/6" />
      </biological_entity>
      <biological_entity constraint="x" id="o14684" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="5" value_original="5" />
        <character name="quantity" src="d0_s27" value="7" value_original="7" />
        <character name="quantity" src="d0_s27" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The only genus of this tribe, Brachypodium, has sometimes been included in the Bromeae or Triticeae because of its large spikelets, apically hairy caryopses, and simple endosperm starch grains, but its smaller chromosomes and different base numbers cast doubt on its close relationship to either of these tribes, a doubt that is strongly supported by nucleic acid data. It now appears that Brachypodium is an isolated genus within the Pooideae, hence its treatment here as the sole genus in a distinct tribe.</discussion>
  <references>
    <reference>Soreng, R.J. and J.I. Davis. 1998. Phylogenetics and character evolution in the grass family (Poaceae): Simultaneous analysis of morphological and chloroplast DNA restriction site character sets. Bot. Rev. (Lancaster) 64:1-85.</reference>
  </references>
  
</bio:treatment>