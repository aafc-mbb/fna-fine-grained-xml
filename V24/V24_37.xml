<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">38</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ORYZA</taxon_name>
    <taxon_name authority="Griff," date="unknown" rank="species">rufipogon</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus oryza;species rufipogon</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Red rice</other_name>
  <other_name type="common_name">Brownbeard rice</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes rhizomatous, rhizomes elongated.</text>
      <biological_entity id="o932" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o933" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="length" src="d0_s1" value="elongated" value_original="elongated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 0.6-3.3 (5) m tall, 6-15 mm thick, decumbent or prostrate, rooting and branching at both the lower and submerged upper nodes.</text>
      <biological_entity id="o934" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="m" value="5" value_original="5" />
        <character char_type="range_value" from="0.6" from_unit="m" name="height" src="d0_s2" to="3.3" to_unit="m" />
        <character char_type="range_value" from="6" from_unit="mm" name="thickness" src="d0_s2" to="15" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="at lower" constraintid="o935" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o935" name="lower" name_original="lower" src="d0_s2" type="structure" />
      <biological_entity constraint="upper" id="o936" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="submerged" value_original="submerged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths smooth, glabrous;</text>
      <biological_entity id="o937" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles sometimes present, 1-7 mm, erect;</text>
      <biological_entity id="o938" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 7-45 mm, acute, finely splitting;</text>
      <biological_entity id="o939" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="45" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="finely" name="architecture_or_dehiscence" src="d0_s5" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 10-80 cm long, 7-24 mm wide, smooth or scabrous.</text>
      <biological_entity id="o940" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="80" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="24" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 12-30 cm long, 1-7 cm wide;</text>
      <biological_entity id="o941" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 2.5-12 (20) cm, ascending or divergent;</text>
      <biological_entity id="o942" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s8" to="12" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1-3 mm.</text>
      <biological_entity id="o943" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4.5-11 mm long, 1.6-3.5 mm wide, oblong or elliptic, deciduous, obliquely articulated with the pedicel, disarticulation scar lateral.</text>
      <biological_entity id="o944" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="duration" src="d0_s10" value="deciduous" value_original="deciduous" />
        <character constraint="with pedicel" constraintid="o945" is_modifier="false" modifier="obliquely" name="architecture" src="d0_s10" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o945" name="pedicel" name_original="pedicel" src="d0_s10" type="structure" />
      <biological_entity id="o946" name="scar" name_original="scar" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sterile florets 1.3-7 mm long, 1/4 - 1/2 (3/4) as long as the spikelets, 0.3-0.7 mm wide, acute.</text>
      <biological_entity id="o947" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o948" from="1/4" name="quantity" src="d0_s11" to="1/23/4" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" notes="" src="d0_s11" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o948" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Functional florets: lemmas 6-11 mm long, 1.4-2.3 mm wide, hispid, apices beaked, beaks to 1 mm, straight or curved, lemma-awn junctions marked by a purplish, pubescent constriction, awns 4-11 (16) cm;</text>
      <biological_entity constraint="functional" id="o949" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity id="o950" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s12" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o951" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o952" name="beak" name_original="beaks" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="lemma-awn" id="o953" name="junction" name_original="junctions" src="d0_s12" type="structure" />
      <biological_entity id="o954" name="constriction" name_original="constriction" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="true" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o955" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s12" to="11" to_unit="cm" />
      </biological_entity>
      <relation from="o953" id="r154" name="marked by" negation="false" src="d0_s12" to="o954" />
    </statement>
    <statement id="d0_s13">
      <text>paleas 0.5-1.2 mm wide, acuminate to awned to 2.3 mm;</text>
      <biological_entity constraint="functional" id="o956" name="floret" name_original="florets" src="d0_s13" type="structure" />
      <biological_entity id="o957" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s13" to="awned" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 3.5-7.4 mm, yellow or brown.</text>
      <biological_entity constraint="functional" id="o958" name="floret" name_original="florets" src="d0_s14" type="structure" />
      <biological_entity id="o959" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 5-7 mm long, 2.2-2.7 mm wide, broadly elliptic or oblong, reddish-brown to dark red;</text>
      <biological_entity id="o960" name="caryopse" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s15" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s15" to="2.7" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s15" to="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryos 1-1.5 mm.</text>
    </statement>
    <statement id="d0_s17">
      <text>Haplome A.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 24.</text>
      <biological_entity id="o961" name="embryo" name_original="embryos" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o962" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oryza rufipogon is native to southeast Asia and Australia, where it grows in shallow, standing or slow-moving water, along irrigation canals, and as a weed in rice fields. It is the ancestor of O. sativa (Londo et al. 2006). The vernacular name 'Red Rice' is used to refer to O. rufipogon, O. punctata, and weedy forms of O. sativa, some of which are probably derivatives of introgression from the first two species.</discussion>
  <discussion>Oryza rufipogon is a weedy taxon that hybridizes readily with O. sativa, forming partially fertile hybrids. This makes it a serious threat to rice growers. Some of the known populations have been eradicated, for instance those in Everglades National Park, Miami-Dade County, Florida, and in the Sacramento Valley, California. Nevertheless, weedy red rice has become an increasingly serious problem in rice fields throughout rice-growing regions of the world, including the contiguous United States, probably through the presence of fertile seed in commercial seed.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>