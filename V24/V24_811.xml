<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">579</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Asch. &amp; Graebn." date="unknown" rank="section">Tichopoa</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">compressa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section tichopoa;species compressa</taxon_hierarchy>
  </taxon_identification>
  <number>58</number>
  <other_name type="common_name">Canada bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually with solitary shoots, sometimes loosely tufted, extensively rhizomatous.</text>
      <biological_entity id="o7697" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7698" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sometimes loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="extensively" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o7697" id="r1236" name="with" negation="false" src="d0_s1" to="o7698" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-60 cm, wiry, bases usually geniculate, strongly compressed;</text>
      <biological_entity id="o7699" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o7700" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes strongly compressed, some proximal nodes usually exserted.</text>
      <biological_entity id="o7701" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7702" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s3" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/10 – 1/5 their length, distinctly compressed, bases of basal sheaths glabrous;</text>
      <biological_entity id="o7703" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o7704" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7705" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
      <relation from="o7704" id="r1237" name="part_of" negation="false" src="d0_s4" to="o7705" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-3 mm, moderately to densely scabrous, ciliolate, apices obtuse;</text>
      <biological_entity id="o7706" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o7707" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1.5-4 mm wide, flat, cauline blades subequal.</text>
      <biological_entity id="o7708" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o7709" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-10 cm, generally 1/6 - 1/3 as wide as long, erect, linear, lanceoloid to ovoid, often interrupted, sparse to congested, with 15-80 spikelets and mostly with 1-3 branches per node;</text>
      <biological_entity id="o7710" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="1/6" modifier="generally" name="quantity" src="d0_s7" to="1/3" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="lanceoloid" name="shape" src="d0_s7" to="ovoid" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="count_or_density" src="d0_s7" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o7711" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s7" to="80" />
      </biological_entity>
      <biological_entity id="o7712" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o7713" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o7710" id="r1238" name="with" negation="false" src="d0_s7" to="o7711" />
      <relation from="o7710" id="r1239" modifier="mostly" name="with" negation="false" src="d0_s7" to="o7712" />
      <relation from="o7712" id="r1240" name="per" negation="false" src="d0_s7" to="o7713" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.5-3 cm, erect to ascending, or infrequently spreading, angles densely scabrous, at least in part, with 1-15 spikelets.</text>
      <biological_entity id="o7714" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending or infrequently spreading" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending or infrequently spreading" />
      </biological_entity>
      <biological_entity id="o7715" name="angle" name_original="angles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7716" name="part" name_original="part" src="d0_s8" type="structure" />
      <biological_entity id="o7717" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
      <relation from="o7715" id="r1241" modifier="at-least" name="in" negation="false" src="d0_s8" to="o7716" />
      <relation from="o7715" id="r1242" name="with" negation="false" src="d0_s8" to="o7717" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (2.3) 3.5-7 mm, laterally compressed;</text>
      <biological_entity id="o7718" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>florets 3-7;</text>
      <biological_entity id="o7719" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla internodes usually shorter than 1 mm, smooth to muriculate.</text>
      <biological_entity constraint="rachilla" id="o7720" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s11" to="muriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes distinctly keeled;</text>
      <biological_entity id="o7721" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o7722" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses usually webbed, sometimes glabrous;</text>
      <biological_entity id="o7723" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="webbed" value_original="webbed" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2.3-3.5 mm, lanceolate, distinctly keeled, keels and marginal veins short-villous, intercostal regions glabrous, lateral-veins obscure, margins glabrous, apices acute;</text>
      <biological_entity id="o7724" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7725" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o7726" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o7727" name="region" name_original="regions" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7728" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o7729" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7730" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas scabrous over the keels;</text>
      <biological_entity id="o7731" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character constraint="over keels" constraintid="o7732" is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7732" name="keel" name_original="keels" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.3-1.8 mm. 2n = 35, 42, 49, 50, 56, 84.</text>
      <biological_entity id="o7733" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7734" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="35" value_original="35" />
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
        <character name="quantity" src="d0_s17" value="49" value_original="49" />
        <character name="quantity" src="d0_s17" value="50" value_original="50" />
        <character name="quantity" src="d0_s17" value="56" value_original="56" />
        <character name="quantity" src="d0_s17" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa compressa is common in much of the Flora region. It is sometimes considered to be native, but this seems doubtful. It is rare and thought to be introduced in Siberia and only local in the Russian Far East, but is common in Europe. In the Flora region, it is often seeded for soil stabilization, and has frequently escaped. It grows mainly in riparian areas, wet meadows, and disturbed ground. Its distinctly compressed nodes and culms, exserted lower culm nodes, rhizomatous growth habit, and scabrous panicle branches make it easily identifiable.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;W.Va.;Del.;D.C.;Wis.;Idaho;Mont.;Oreg.;Wyo.;Pacific Islands (Hawaii);Kans.;N.Dak.;Nebr.;S.Dak.;Mass.;Maine;N.H.;R.I.;Vt.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Alaska;Nev.;Va.;Colo.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Okla.;Ariz.;Calif.;Md.;Ohio;Utah;Mo.;Minn.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>