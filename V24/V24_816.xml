<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Nannf. ex Tzvelev" date="unknown" rank="section">Abbreviatae</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="species">abbreviata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">abbreviata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section abbreviatae;species abbreviata;subspecies abbreviata</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Dwarf bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Ligules 0.4-1.7 (3) mm, apices truncate to acute.</text>
      <biological_entity id="o6901" name="ligule" name_original="ligules" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s0" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6902" name="apex" name_original="apices" src="d0_s0" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s0" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spikelets not bulbiferous;</text>
      <biological_entity id="o6903" name="spikelet" name_original="spikelets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>florets normal.</text>
      <biological_entity id="o6904" name="floret" name_original="florets" src="d0_s2" type="structure">
        <character is_modifier="false" name="variability" src="d0_s2" value="normal" value_original="normal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calluses glabrous, rarely webbed;</text>
      <biological_entity id="o6905" name="callus" name_original="calluses" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lemmas with keels and marginal veins short to long-villous, hairs extending along 5/6 of the keel, intercostal regions softly puberulent to short-villous;</text>
      <biological_entity id="o6906" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o6907" name="keel" name_original="keels" src="d0_s4" type="structure" />
      <biological_entity constraint="marginal" id="o6908" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o6909" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character constraint="of keel" constraintid="o6910" name="quantity" src="d0_s4" value="5/6" value_original="5/6" />
      </biological_entity>
      <biological_entity id="o6910" name="keel" name_original="keel" src="d0_s4" type="structure" />
      <biological_entity constraint="intercostal" id="o6911" name="region" name_original="regions" src="d0_s4" type="structure">
        <character char_type="range_value" from="softly puberulent" name="pubescence" src="d0_s4" to="short-villous" />
      </biological_entity>
      <relation from="o6906" id="r1109" name="with" negation="false" src="d0_s4" to="o6907" />
      <relation from="o6906" id="r1110" name="with" negation="false" src="d0_s4" to="o6908" />
    </statement>
    <statement id="d0_s5">
      <text>anthers 0.2-0.8 mm. 2n = 42.</text>
      <biological_entity id="o6912" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6913" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa abbreviata subsp. abbreviata is a common high arctic subspecies that also grows at scattered locations in the Brooks Range, Alaska, and in the Rocky Mountains of the Northwest Territories. It has a circumpolar distribution, but is not common in the Eurasian continental arctic.</discussion>
  
</bio:treatment>