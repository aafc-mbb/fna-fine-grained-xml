<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">614</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BRIZA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">maxima</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus briza;species maxima</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Big quakinggrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o13315" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-80 cm.</text>
      <biological_entity id="o13316" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evenly distributed;</text>
      <biological_entity id="o13317" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths frequently less than 1/2 as long as the internodes, open to near the base, margins overlapping;</text>
      <biological_entity id="o13318" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="as-long-as internodes" constraintid="o13319" from="0" name="quantity" src="d0_s3" to="1/2" />
        <character constraint="to base, margins" constraintid="o13320, o13321" is_modifier="false" name="architecture" notes="" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o13319" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o13320" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o13321" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 3-7 mm, sides sometimes decurrent, margins entire to erose, acute;</text>
      <biological_entity id="o13322" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13323" name="side" name_original="sides" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o13324" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="architecture" src="d0_s4" to="erose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2.5-20 cm long, 2-8 mm wide, margins strigose or glabrous.</text>
      <biological_entity id="o13325" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13326" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 3.5-10 cm long, mostly 1-5 cm wide;</text>
      <biological_entity id="o13327" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="mostly" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels 5-20 mm.</text>
      <biological_entity id="o13328" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 10-20 mm, oval to elliptic, with 4-12 (15) florets.</text>
      <biological_entity id="o13329" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s8" to="elliptic" />
      </biological_entity>
      <biological_entity id="o13330" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="15" value_original="15" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <relation from="o13329" id="r2129" name="with" negation="false" src="d0_s8" to="o13330" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 5-5.5 mm, 5-veined;</text>
      <biological_entity constraint="lower" id="o13331" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 6-6.5 mm, 7-veined;</text>
      <biological_entity constraint="upper" id="o13332" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lowermost lemmas 7-9 mm, 7-9-veined, surfaces usually glabrous proximally, becoming villous distally, apices obtuse;</text>
      <biological_entity constraint="lowermost" id="o13333" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity id="o13334" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="becoming; distally" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o13335" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas about 4 mm, more or less ciliolate along the margins;</text>
      <biological_entity id="o13336" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="4" value_original="4" />
        <character constraint="along margins" constraintid="o13337" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s12" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o13337" name="margin" name_original="margins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 1.2-1.5 mm.</text>
      <biological_entity id="o13338" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 2-3 mm, obovoid.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 10, 14.</text>
      <biological_entity id="o13339" name="caryopse" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13340" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Briza maxima is native to the Mediterranean region. Cultivated as an ornamental, it is possibly one of the earliest grasses grown for other than edible purposes. It occasionally becomes naturalized in dry to somewhat moist but well-drained, fine or sandy soil on banks, rocky places, open woodlands, and cultivated areas such as roadsides and pastures. In the Flora region, it is known from scattered locations, mostly in Oregon and California, where it is an invader of coastal dune habitat.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;Colo.;N.Y.;Calif.;Mich.;Wis.;Alta.;B.C.;Ont.;Que.;Pacific Islands (Hawaii);Vt.;Ill.;Ga.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>