<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">737</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AVENA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">sativa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus avena;species sativa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Avena</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fatua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">sativa</taxon_name>
    <taxon_hierarchy>genus avena;species fatua;variety sativa</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Oats</other_name>
  <other_name type="common_name">Cultivated oats</other_name>
  <other_name type="common_name">Naked oats</other_name>
  <other_name type="common_name">Avoine</other_name>
  <other_name type="common_name">Avoine cultivée</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o6379" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 35-180 cm, prostrate to erect when young, becoming erect at maturity.</text>
      <biological_entity id="o6380" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s1" to="180" to_unit="cm" />
        <character char_type="range_value" from="prostrate" modifier="when young" name="orientation" src="d0_s1" to="erect" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth or scabridulous;</text>
      <biological_entity id="o6381" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 2-8 mm, truncate to acute;</text>
      <biological_entity id="o6382" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 8-45 cm long, 3-14 (25) mm wide, scabridulous.</text>
      <biological_entity id="o6383" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="25" value_original="25" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (6) 15-40 cm long, 5-15 cm wide, nodding.</text>
      <biological_entity id="o6384" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets (18) 25-32 mm, to 50 mm in naked oats, with 1-2 florets (to 7 in naked oats);</text>
      <biological_entity id="o6386" name="oat" name_original="oats" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6387" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <relation from="o6385" id="r1036" name="with" negation="false" src="d0_s6" to="o6387" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation not occurring, the florets remaining attached even when mature.</text>
      <biological_entity id="o6385" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="18" value_original="18" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="32" to_unit="mm" />
        <character char_type="range_value" constraint="in oats" constraintid="o6386" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6388" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="when mature" name="fixation" src="d0_s7" value="attached" value_original="attached" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes subequal, (18) 20-32 mm, 9-11-veined;</text>
      <biological_entity id="o6389" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="18" value_original="18" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="32" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="9-11-veined" value_original="9-11-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calluses glabrous;</text>
      <biological_entity id="o6390" name="callus" name_original="calluses" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 14-18 mm, usually indurate, membranous in naked oats, usually glabrous, sometimes sparsely strigose, apices erose to dentate, longest teeth 0.2-0.5 mm, usually unawned, sometimes awned, awns 15-30 mm, arising in the middle 1/3, weakly twisted, not or only weakly geniculate;</text>
      <biological_entity id="o6391" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
        <character constraint="in oats" constraintid="o6392" is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o6392" name="oat" name_original="oats" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6393" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="erose" name="architecture" src="d0_s10" to="dentate" />
      </biological_entity>
      <biological_entity constraint="longest" id="o6394" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually; sometimes" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o6395" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
        <character constraint="in middle 1/3" constraintid="o6396" is_modifier="false" name="orientation" src="d0_s10" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="weakly" name="architecture" notes="" src="d0_s10" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="not; only weakly" name="shape" src="d0_s10" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o6396" name="1/3" name_original="1/3" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lodicules with a lobe or tooth on the wings, this sometimes very small;</text>
      <biological_entity id="o6397" name="lodicule" name_original="lodicules" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sometimes very" name="size" notes="" src="d0_s11" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o6398" name="lobe" name_original="lobe" src="d0_s11" type="structure" />
      <biological_entity id="o6399" name="tooth" name_original="tooth" src="d0_s11" type="structure" />
      <biological_entity id="o6400" name="wing" name_original="wings" src="d0_s11" type="structure" />
      <relation from="o6397" id="r1037" name="with" negation="false" src="d0_s11" to="o6398" />
      <relation from="o6397" id="r1038" name="with" negation="false" src="d0_s11" to="o6399" />
      <relation from="o6399" id="r1039" name="on" negation="false" src="d0_s11" to="o6400" />
    </statement>
    <statement id="d0_s12">
      <text>anthers (1.7) 3-4.3 mm. 2n = 42.</text>
      <biological_entity id="o6401" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="1.7" value_original="1.7" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6402" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Avena sativa, a native of Eurasia, is widely cultivated in cool, temperate regions of the world, including North America. Fall-sown oats are planted in the Pacific and southern states in the United States; spring-sown oats are more important elsewhere in North America. It is sometimes planted as a fast-growing soil stabilizer along roadsides. Several forms are grown, of which the most distinctive are 'naked oats'. These differ from typical forms as indicated in the description, and in having caryopses that fall from the florets. Escapes from cultivation are common but rarely persist.</discussion>
  <discussion>Avena sativa hybridizes readily with A. fatua, forming hybrids with the fatua-type. lodicule. The hybrids are easily confused with fatuoid forms of A. sativa, which differ in having the sativa-type lodicule.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Va.;W.Va.;Mich.;D.C.;Wis.;Ariz.;N.Mex.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;Tex.;La.;N.Dak.;Nebr.;Tenn.;N.C.;S.C.;Pa.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;Calif.;Nev.;Puerto Rico;Colo.;Md.;Alaska;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Okla.;Idaho;Mont.;Oreg.;Ohio;Utah;Mo.;Minn.;Kans.;Miss.;Ky.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>