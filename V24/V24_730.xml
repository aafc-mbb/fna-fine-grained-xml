<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">521</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Micrantherae</taxon_name>
    <taxon_name authority="Schrad." date="unknown" rank="species">supina</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section micrantherae;species supina</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Supine bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous, loosely tufted.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intravaginal.</text>
      <biological_entity id="o16829" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 8-12 (20) cm, slender, bases decumbent, terete or weakly compressed;</text>
      <biological_entity id="o16830" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o16831" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, 1 exserted.</text>
      <biological_entity id="o16832" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/4-1/3 their length, terete, smooth, glabrous, bases of basal sheaths glabrous, distal sheath lengths 2-4 times blade lengths;</text>
      <biological_entity id="o16833" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16834" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16835" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o16836" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o16837" is_modifier="false" name="length" src="d0_s5" value="2-4 times blade lengths" value_original="2-4 times blade lengths" />
      </biological_entity>
      <biological_entity id="o16837" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o16834" id="r2700" name="part_of" negation="false" src="d0_s5" to="o16835" />
    </statement>
    <statement id="d0_s6">
      <text>collars smooth, glabrous;</text>
      <biological_entity id="o16838" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.6-1 mm, smooth, glabrous, truncate;</text>
      <biological_entity id="o16839" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 2-3 mm wide, flat, thin, soft, smooth, apices broadly prow-shaped, cauline blades subequal.</text>
      <biological_entity id="o16840" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o16841" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16842" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 2.5-5 cm, lengths 1-2 times widths, erect, loosely contracted or open, ovoid to pyramidal, sparse, with 10-25 (30) spikelets and 1-2 branches per node;</text>
      <biological_entity id="o16843" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1-2" value_original="1-2" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="pyramidal" />
        <character is_modifier="false" name="count_or_density" src="d0_s9" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o16844" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="30" value_original="30" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s9" to="25" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o16845" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o16846" name="node" name_original="node" src="d0_s9" type="structure" />
      <relation from="o16843" id="r2701" name="with" negation="false" src="d0_s9" to="o16844" />
      <relation from="o16845" id="r2702" name="per" negation="false" src="d0_s9" to="o16846" />
    </statement>
    <statement id="d0_s10">
      <text>branches 1-3 cm, spreading to reflexed, straight, terete, smooth or sparsely scabrous, with 2-5 (8) spikelets.</text>
      <biological_entity id="o16847" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o16848" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="8" value_original="8" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o16847" id="r2703" name="with" negation="false" src="d0_s10" to="o16848" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 4-6 mm, laterally compressed;</text>
      <biological_entity id="o16849" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets 3-7;</text>
      <biological_entity id="o16850" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachilla internodes smooth, glabrous, more or less concealed, distal internode less than 1/2 the length of the distal lemma.</text>
      <biological_entity constraint="rachilla" id="o16851" name="internode" name_original="internodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s13" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16852" name="internode" name_original="internode" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="1/2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16853" name="lemma" name_original="lemma" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Glumes distinctly keeled, keels smooth;</text>
      <biological_entity id="o16854" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o16855" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 1-veined;</text>
      <biological_entity constraint="lower" id="o16856" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses glabrous;</text>
      <biological_entity id="o16857" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 1.7-4 mm, lanceolate, distinctly keeled, smooth throughout, proximal lemmas glabrous throughout or the keels and marginal veins sparsely short-villous, distal lemmas glabrous or the keels and marginal veins short-villous to near the apices, lateral-veins moderately prominent, intercostal regions glabrous, margins smooth, glabrous, apices obtuse to acute;</text>
      <biological_entity id="o16858" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16859" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16860" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o16861" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16862" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16863" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character constraint="near apices" constraintid="o16865" is_modifier="false" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o16864" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character constraint="near apices" constraintid="o16865" is_modifier="false" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o16865" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <biological_entity id="o16866" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o16867" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16868" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16869" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s17" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>palea keels smooth, sometimes sparsely softly puberulent to short-villous;</text>
      <biological_entity constraint="palea" id="o16870" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="sometimes sparsely softly puberulent" name="pubescence" src="d0_s18" to="short-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers (1.25) 1.5-2.5 mm, cylindrical prior to dehiscence, those of the upper 1-2 florets commonly vestigial.</text>
      <biological_entity id="o16872" name="floret" name_original="florets" src="d0_s19" type="structure">
        <character is_modifier="true" name="position" src="d0_s19" value="upper" value_original="upper" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s19" to="2" />
        <character is_modifier="false" modifier="commonly" name="prominence" src="d0_s19" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <relation from="o16871" id="r2704" name="consist_of" negation="false" src="d0_s19" to="o16872" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o16871" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="mm" value="1.25" value_original="1.25" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s19" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="prior-to-dehiscence" src="d0_s19" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16873" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa supina is native to boreal to alpine regions of Eurasia. Beginning in the 1990s, the cultivar 'Supernova' has been introduced for seeding in wet to moist, cool, shady areas subject to heavy traffic. It has been tested in both Canada and the United States, and is expected to gradually escape cultivation, probably becoming established throughout the cool-temperate portion of the Flora region. Its current distribution is not known. Poa supina differs from P. annua (p. 519), of which is thought to be one of the parents, in having longer anthers and a more stoloniferous habit, as well as in being diploid. It is gynomonoecious.</discussion>
  
</bio:treatment>