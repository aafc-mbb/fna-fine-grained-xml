<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">662</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Desf." date="unknown" rank="genus">POLYPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus polypogon</taxon_hierarchy>
  </taxon_identification>
  <number>14.28</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o28834" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 4-120 cm, erect to decumbent, rooting at the lower nodes, sparingly branched near the base.</text>
      <biological_entity id="o28835" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
        <character constraint="at lower nodes" constraintid="o28836" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="near base" constraintid="o28837" is_modifier="false" modifier="sparingly" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28836" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o28837" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually no more than 5 per culm, basal and cauline;</text>
      <biological_entity id="o28838" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="no" value_original="no" />
        <character char_type="range_value" constraint="per culm" constraintid="o28839" from="5" name="quantity" src="d0_s3" upper_restricted="false" />
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o28839" name="culm" name_original="culm" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths open, smooth or scabridulous;</text>
      <biological_entity id="o28840" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles absent;</text>
      <biological_entity id="o28841" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous or hyaline, acute to broadly rounded, erose, ciliate;</text>
      <biological_entity id="o28842" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="broadly rounded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat to convolute.</text>
      <biological_entity id="o28843" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal panicles, dense, continuous or interrupted below;</text>
      <biological_entity id="o28844" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" notes="" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="below" name="architecture" src="d0_s8" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o28845" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>branches flexible, usually some longer than 1 cm;</text>
      <biological_entity id="o28846" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="pliable" value_original="flexible" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels absent and the spikelets borne on a stipe, or present and terminating in a stipe;</text>
      <biological_entity id="o28847" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28848" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28849" name="stipe" name_original="stipe" src="d0_s10" type="structure" />
      <biological_entity id="o28850" name="stipe" name_original="stipe" src="d0_s10" type="structure" />
      <relation from="o28848" id="r4529" name="borne on a" negation="false" src="d0_s10" to="o28849" />
      <relation from="o28848" id="r4530" name="terminating in" negation="false" src="d0_s10" to="o28850" />
    </statement>
    <statement id="d0_s11">
      <text>stipes scabrous, flaring distally;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation at the base of the stipes.</text>
      <biological_entity id="o28851" name="stipe" name_original="stipes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s11" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity id="o28852" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o28853" name="stipe" name_original="stipes" src="d0_s12" type="structure" />
      <relation from="o28851" id="r4531" name="at" negation="false" src="d0_s12" to="o28852" />
      <relation from="o28852" id="r4532" name="part_of" negation="false" src="d0_s12" to="o28853" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 1-5 mm, weakly laterally compressed, with 1 bisexual floret;</text>
      <biological_entity id="o28854" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="weakly laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o28855" name="floret" name_original="floret" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o28854" id="r4533" name="with" negation="false" src="d0_s13" to="o28855" />
    </statement>
    <statement id="d0_s14">
      <text>rachillas not prolonged beyond the base of the floret.</text>
      <biological_entity id="o28856" name="rachilla" name_original="rachillas" src="d0_s14" type="structure">
        <character constraint="beyond base" constraintid="o28857" is_modifier="false" modifier="not" name="length" src="d0_s14" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o28857" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o28858" name="floret" name_original="floret" src="d0_s14" type="structure" />
      <relation from="o28857" id="r4534" name="part_of" negation="false" src="d0_s14" to="o28858" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes exceeding the floret, lanceolate, bases not fused, apices entire to emarginate or bilobed, usually awned from the sinuses or apices, awns flexuous, glabrous, sometimes unawned;</text>
      <biological_entity id="o28859" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o28860" name="floret" name_original="floret" src="d0_s15" type="structure" />
      <biological_entity id="o28861" name="base" name_original="bases" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s15" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o28862" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s15" to="emarginate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="bilobed" value_original="bilobed" />
        <character constraint="from apices" constraintid="o28864" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o28863" name="sinuse" name_original="sinuses" src="d0_s15" type="structure" />
      <biological_entity id="o28864" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o28865" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o28859" id="r4535" name="exceeding the" negation="false" src="d0_s15" to="o28860" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas 1-3 (5) -veined, often awned, awns usually terminal or subterminal, sometimes arising from just above midlength;</text>
      <biological_entity id="o28866" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-3(5)-veined" value_original="1-3(5)-veined" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o28867" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s16" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s16" value="subterminal" value_original="subterminal" />
        <character constraint="just above midlength" constraintid="o28868" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s16" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o28868" name="midlength" name_original="midlength" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>paleas from 1/3 as long as to equaling the lemmas;</text>
      <biological_entity id="o28869" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character constraint="to lemmas" constraintid="o28870" modifier="from" name="quantity" src="d0_s17" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o28870" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="true" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lodicules 2, oblong-lanceolate to lanceolate;</text>
      <biological_entity id="o28871" name="lodicule" name_original="lodicules" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s18" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 3;</text>
      <biological_entity id="o28872" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovaries glabrous;</text>
      <biological_entity id="o28873" name="ovary" name_original="ovaries" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>styles separate.</text>
      <biological_entity id="o28874" name="style" name_original="styles" src="d0_s21" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s21" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Caryopses slightly flattened, broadly ellipsoid to oblong-ellipsoid;</text>
      <biological_entity id="o28875" name="caryopse" name_original="caryopses" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s22" to="oblong-ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>hila 1/6 – 1/4 as long as the caryopses, ovate, x = 7.</text>
      <biological_entity id="o28876" name="hilum" name_original="hila" src="d0_s23" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o28877" from="1/6" name="quantity" src="d0_s23" to="1/4" />
        <character is_modifier="false" name="shape" notes="" src="d0_s23" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o28877" name="caryopse" name_original="caryopses" src="d0_s23" type="structure" />
      <biological_entity constraint="x" id="o28878" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polypogon is a pantropical and warm-temperate genus of about 18 species. There are eight species in the Flora region; one species, P. interruptus, is native.</discussion>
  <discussion>Polypogon is similar to Agrostis, and occasionally hybridizes with it. It differs from Agrostis in having spikelets that disarticulate below the glumes, often at the base of a stipe.</discussion>
  <references>
    <reference>Cope, T.A. 1982. Flora of Pakistan, No. 143: Poaceae (E. Nasir and S.I. Ali, eds.). Pakistan Agricultural Research Council and University of Karachi, Islamabad and Karachi, Pakistan. 678 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho;Mont.;Wash.;Va.;Alta.;B.C.;Man.;Ont.;Que.;Sask.;Yukon;Del.;Wis.;Conn.;Mass.;Maine;N.H.;Pacific Islands (Hawaii);Fla.;Wyo.;N.J.;N.Mex.;Tex.;La.;Nebr.;Tenn.;N.C.;S.C.;Pa.;N.Y.;Calif.;Nev.;Puerto Rico;Colo.;Alaska;Ala.;Kans.;N.Dak.;Okla.;S.Dak.;Ark.;Ga.;Ariz.;Md.;Utah;Minn.;Mich.;Miss.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes with awns 3-12 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Glumes deeply lobed, the lobes more than 1/6 the length of the glume body</description>
      <determination>7 Polypogon maritimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Glumes not lobed or the lobes 1/10 or less the length of the glume body.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants annual; glume apices rounded, lobed, the lobes 0.1-0.2 mm long; ligules 2.5-16 mm long</description>
      <determination>6 Polypogon monspeliensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants perennial; glume apices acute to truncate, unlobed or the lobes shorter than 0.1 mm; ligules 1-6 mm long.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Glume awns (3)4-6 mm long; longest blades 13-17 cm long</description>
      <determination>5 Polypogon australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Glume awns 1.5-3.2 mm long; longest blades 5-9 cm long</description>
      <determination>2 Polypogon interruptus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes unawned or with awns to 3.2 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Glumes unawned</description>
      <determination>1 Polypogon viridis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Glumes awned, the awns 0.2-3.2 mm long.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Stipes 1.5-2.5 mm long; glumes tapering from about midlength to the acute, unlobed apices</description>
      <determination>3 Polypogon elongatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Stipes 0.2-1.5 mm long; glumes not tapering to the apices, the apices usually rounded to truncate, sometimes acute, often lobed.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Lemmas 1-2 mm long; paleas about 1/2 as long as the lemmas; the lower glumes longer than the upper glumes</description>
      <determination>8 Polypogon imberbis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Lemmas 0.7-1.5 mm long; paleas from 3/4 as long as to equaling the lemmas; glumes of each spikelet subequal to equal.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Plants annual; glumes acute to rounded, lobed, the lobes 0.1-0.2 mm long</description>
      <determination>4 Polypogon fugax</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Plants perennial, often flowering the first year; glumes acute to truncate, if lobed, the lobes to 0.1 mm long</description>
      <determination>2 Polypogon interruptus</determination>
    </key_statement>
  </key>
</bio:treatment>