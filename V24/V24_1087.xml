<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PHALARIS</taxon_name>
    <taxon_name authority="Retz." date="unknown" rank="species">minor</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus phalaris;species minor</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Lesser canarygrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o17100" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-100 cm, not swollen at the base.</text>
      <biological_entity id="o17101" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character constraint="at base" constraintid="o17102" is_modifier="false" modifier="not" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o17102" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Ligules 5-12 mm, truncate to rounded, often lacerate;</text>
      <biological_entity id="o17103" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="rounded" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 3-15 cm long, 2-10 mm wide, smooth, shiny.</text>
      <biological_entity id="o17104" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 1-8 cm long, 1-2 cm wide, dense, ovoid-lanceoloid, truncate to rounded at the base, rounded apically.</text>
      <biological_entity id="o17105" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid-lanceoloid" value_original="ovoid-lanceoloid" />
        <character char_type="range_value" constraint="at base" constraintid="o17106" from="truncate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="apically" name="shape" notes="" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o17106" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>spikelets borne singly, not clustered.</text>
      <biological_entity id="o17107" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets homogamous, with 2 florets, 1 bisexual;</text>
      <biological_entity id="o17109" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17108" id="r2737" name="with" negation="false" src="d0_s6" to="o17109" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation above the glumes, beneath the sterile florets.</text>
      <biological_entity id="o17108" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="homogamous" value_original="homogamous" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o17110" name="glume" name_original="glumes" src="d0_s7" type="structure" />
      <biological_entity id="o17111" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o17108" id="r2738" name="above" negation="false" src="d0_s7" to="o17110" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 3.5-6.5 mm long, 1.2-2 mm wide, keels winged distally, wings 0.3-0.5 mm wide, irregularly dentate or crenate, occasionally entire, varying within a panicle, lateral-veins conspicuous, smooth;</text>
      <biological_entity id="o17112" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17113" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o17114" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character constraint="within panicle" constraintid="o17115" is_modifier="false" name="variability" src="d0_s8" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity id="o17115" name="panicle" name_original="panicle" src="d0_s8" type="structure" />
      <biological_entity id="o17116" name="lateral-vein" name_original="lateral-veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sterile florets 1, 0.7-1.8 mm, linear, glabrous or almost so;</text>
      <biological_entity id="o17117" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s9" value="almost" value_original="almost" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bisexual florets 2-4 mm long, 1-1.8 mm wide, hairy, dull yellow when immature, becoming shiny gray-brown at maturity, acute to somewhat acuminate;</text>
      <biological_entity id="o17118" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="becoming" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s10" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute to somewhat" value_original="acute to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1-2 mm. 2n = 28, 29.</text>
      <biological_entity id="o17119" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17120" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="28" value_original="28" />
        <character name="quantity" src="d0_s11" value="29" value_original="29" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phalaris minor is native around the Mediterranean and in northwestern Asia, but is now found throughout the world. Even where it is native, it usually grows in disturbed ground, often around harbors and near refuse dumps. Although it has been found at numerous locations in the Flora region, it is only established in the southern portion of the region.</discussion>
  <discussion>The compact panicle with its truncate to rounded base, and the rather variable edges of the glume wings, usually distinguish Phalaris minor from other species in the genus. It sometimes forms a polyploid hybrid with P. aquatica, P. xdaviesii S.T. Blake, which is cultivated for forage in Australia, Africa, and South America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pa.;N.J.;Colo.;N.Mex.;Tex.;La.;Alaska;N.B.;Ala.;Oreg.;Pacific Islands (Hawaii);S.C.;Ariz.;Fla.;Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>