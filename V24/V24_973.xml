<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">688</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="genus">PARAPHOLIS</taxon_name>
    <taxon_name authority="(Dumort.) C.E. Hubb." date="unknown" rank="species">strigosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus parapholis;species strigosa</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Hairy sicklegrass</other_name>
  <other_name type="common_name">Strigose sicklegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 12-45 cm, erect to ascending, branching at the lower nodes.</text>
      <biological_entity id="o13526" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
        <character constraint="at lower nodes" constraintid="o13527" is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13527" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths of upper leaves usually with the margins all alike, not inflated, not enclosing the lowest spikelets;</text>
      <biological_entity id="o13528" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s1" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13529" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13530" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="alike" value_original="alike" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o13531" name="spikelet" name_original="spikelets" src="d0_s1" type="structure" />
      <relation from="o13528" id="r2162" name="part_of" negation="false" src="d0_s1" to="o13529" />
      <relation from="o13528" id="r2163" name="with" negation="false" src="d0_s1" to="o13530" />
      <relation from="o13528" id="r2164" name="enclosing the" negation="true" src="d0_s1" to="o13531" />
    </statement>
    <statement id="d0_s2">
      <text>ligules to 2.3 mm;</text>
      <biological_entity id="o13532" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 2 (12) cm long, 1-3 mm wide, flat to inrolled, adaxial surfaces scabrid.</text>
      <biological_entity id="o13533" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character name="length" src="d0_s3" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s3" to="inrolled" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13534" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrid" value_original="scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 5-18 cm, straight, not twisted, with 5-25 spikelets.</text>
      <biological_entity id="o13535" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o13536" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="25" />
      </biological_entity>
      <relation from="o13535" id="r2165" name="with" negation="false" src="d0_s4" to="o13536" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 4.5-7 mm, usually slightly longer than the internodes, usually not cleistogamous.</text>
      <biological_entity id="o13537" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character constraint="than the internodes" constraintid="o13538" is_modifier="false" name="length_or_size" src="d0_s5" value="usually slightly longer" value_original="usually slightly longer" />
        <character is_modifier="false" modifier="usually not" name="reproduction" src="d0_s5" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o13538" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Glumes 4-6 mm, lanceolate, acuminate, keels obscure;</text>
      <biological_entity id="o13539" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o13540" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 1.5-4 mm. 2n = 14, 28.</text>
      <biological_entity id="o13541" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13542" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" value_original="14" />
        <character name="quantity" src="d0_s7" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Parapholis strigosa has been found in disturbed areas of Humboldt Bay, California, and has also been reported from Del Norte, Mendicino, and Sonoma counties. It grows on moist soils above normal high tides, usually in well-compacted sandy loams. In general, it is found in less saline soils and at higher elevations than P. incurva.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>