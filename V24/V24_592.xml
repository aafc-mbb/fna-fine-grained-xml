<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">rubra</taxon_name>
    <taxon_name authority="(Rydb.) Pavlick" date="unknown" rank="subspecies">vallicola</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species rubra;subspecies vallicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Mountain red fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous, often with widely spaced single culms and only a few vegetative shoots, varying to loosely cespitose.</text>
      <biological_entity id="o10276" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="variability" notes="" src="d0_s0" value="varying" value_original="varying" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10277" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="widely" name="arrangement" src="d0_s0" value="spaced" value_original="spaced" />
        <character is_modifier="true" name="quantity" src="d0_s0" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o10278" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s0" value="few" value_original="few" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o10276" id="r1642" modifier="often" name="with" negation="false" src="d0_s0" to="o10277" />
      <relation from="o10276" id="r1643" modifier="often" name="with" negation="false" src="d0_s0" to="o10278" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (20) 25-70 (100) cm.</text>
      <biological_entity id="o10279" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths tightly enclosing the culms;</text>
      <biological_entity id="o10280" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o10281" name="culm" name_original="culms" src="d0_s2" type="structure" />
      <relation from="o10280" id="r1644" modifier="tightly" name="enclosing the" negation="false" src="d0_s2" to="o10281" />
    </statement>
    <statement id="d0_s3">
      <text>vegetative shoot blades 0.5-1.5 mm in diameter, conduplicate, deep green, abaxial surfaces smooth to slightly scabrous, adaxial surfaces scabrous on the ribs;</text>
      <biological_entity constraint="shoot" id="o10282" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
        <character char_type="range_value" constraint="in abaxial surfaces" constraintid="o10283" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10283" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" name="character" src="d0_s3" value="diameter" value_original="diameter" />
        <character is_modifier="true" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="true" name="depth" src="d0_s3" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="smooth to slightly" value_original="smooth to slightly" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10284" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="on ribs" constraintid="o10285" is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o10285" name="rib" name_original="ribs" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cauline blades conduplicate or flat;</text>
      <biological_entity constraint="cauline" id="o10286" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s4" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial sclerenchyma in narrow strands;</text>
      <biological_entity id="o10288" name="strand" name_original="strands" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o10287" id="r1645" name="in" negation="false" src="d0_s5" to="o10288" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial sclerenchyma absent.</text>
      <biological_entity constraint="abaxial" id="o10287" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 5-8 cm, closed or open.</text>
      <biological_entity id="o10289" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="8" to_unit="cm" />
        <character is_modifier="false" name="condition" src="d0_s7" value="closed" value_original="closed" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 8-11 mm, with 4-7 florets.</text>
      <biological_entity id="o10290" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10291" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <relation from="o10290" id="r1646" name="with" negation="false" src="d0_s8" to="o10291" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 2-3 mm;</text>
      <biological_entity constraint="lower" id="o10292" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 4-5 mm, acute to acuminate;</text>
      <biological_entity constraint="upper" id="o10293" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 5-6 mm, pale green with violet borders, smooth or scabrous near the apices, apices awned, awns 1-2.5 (4) mm;</text>
      <biological_entity id="o10294" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character constraint="with borders" constraintid="o10295" is_modifier="false" name="coloration" src="d0_s11" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s11" value="smooth" value_original="smooth" />
        <character constraint="near apices" constraintid="o10296" is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o10295" name="border" name_original="borders" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o10296" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o10297" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o10298" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 2-2.6 mm. 2n = unknown.</text>
      <biological_entity id="o10299" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10300" name="chromosome" name_original="" src="d0_s12" type="structure" />
    </statement>
  </description>
  <discussion>Festuca rubra subsp. vallicola grows in moist meadows, lake margins, and disturbed soil, at 1000-2000 m, in montane and subalpine habitats from the Yukon Territory/British Columbia border area south to Wyoming.</discussion>
  
</bio:treatment>