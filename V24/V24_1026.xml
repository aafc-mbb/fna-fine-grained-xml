<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">726</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">perplexa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species perplexa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">porteri</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">perplexa</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species porteri;subspecies perplexa</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Wood reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with sterile culms;</text>
      <biological_entity id="o25484" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o25483" id="r4032" name="with" negation="false" src="d0_s0" to="o25484" />
    </statement>
    <statement id="d0_s1">
      <text>weakly cespitose, with rhizomes 8+ cm long, 1.5-2 mm thick.</text>
      <biological_entity id="o25483" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="weakly" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" upper_restricted="false" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o25485" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" upper_restricted="false" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (80) 85-110 (120) cm, sometimes branched, smooth or slightly scabrous beneath the panicles;</text>
      <biological_entity id="o25486" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="80" value_original="80" />
        <character char_type="range_value" from="85" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="beneath panicles" constraintid="o25487" is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25487" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 4-6.</text>
      <biological_entity id="o25488" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth;</text>
      <biological_entity id="o25489" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>collars densely hairy;</text>
      <biological_entity id="o25490" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules (3) 4-6 (7) mm, lacerate;</text>
      <biological_entity id="o25491" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades (10) 15-30 (35) cm long, (3) 4.5-6.5 (7) mm wide, flat, scabridulous, adaxial surfaces glabrous or sparsely hairy.</text>
      <biological_entity id="o25492" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25493" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 10-18 (20) cm long, (1) 2-3 cm wide, open, erect to nodding, green, purple-tinged;</text>
      <biological_entity id="o25494" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="18" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="nodding" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches 5-6.5 cm, spikelets usually confined to the distal 1/4-1/2.</text>
      <biological_entity id="o25495" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s9" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25496" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets (3) 3.5-4 mm;</text>
      <biological_entity id="o25497" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla prolongations about 1 mm, hairs about 2 mm.</text>
      <biological_entity id="o25498" name="rachillum" name_original="rachilla" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25499" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes keeled, scabrous on the keels, lateral-veins usually prominent, apices acuminate;</text>
      <biological_entity id="o25500" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
        <character constraint="on keels" constraintid="o25501" is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25501" name="keel" name_original="keels" src="d0_s12" type="structure" />
      <biological_entity id="o25502" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o25503" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 2-3 mm, 0.7-1 times as long as the lemmas, somewhat sparse;</text>
      <biological_entity constraint="callus" id="o25504" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character constraint="lemma" constraintid="o25505" is_modifier="false" name="length" src="d0_s13" value="0.7-1 times as long as the lemmas" />
        <character is_modifier="false" modifier="somewhat" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o25505" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3-3.5 mm, 0.5-1 mm shorter than the glumes;</text>
      <biological_entity id="o25506" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o25507" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25507" name="glume" name_original="glumes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>awns 2-3 mm, attached to the lower 1/4-1/3 of the lemmas, not exserted, stout, distinguishable from the callus hairs, bent;</text>
      <biological_entity id="o25508" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s15" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s15" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o25509" from="1/4" name="quantity" src="d0_s15" to="1/3" />
        <character is_modifier="false" modifier="not" name="position" notes="" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s15" value="stout" value_original="stout" />
        <character constraint="from callus hairs" constraintid="o25510" is_modifier="false" name="prominence" src="d0_s15" value="distinguishable" value_original="distinguishable" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o25509" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity constraint="callus" id="o25510" name="hair" name_original="hairs" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 1-1.5 mm. 2n = 70.</text>
      <biological_entity id="o25511" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25512" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis perplexa grows on wet rocks and in dry woods at "Thatcher's Pinnacle" [Pinnacle Rock], Tompkins County, New York. There is also an unverified report of this species in Columbia County, New York. This apparently sterile species is intermediate between C. porteri (p. 721) and C. canadensis (see next) (Greene 1980). It is of conservation concern because of its limited distribution.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Y.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>