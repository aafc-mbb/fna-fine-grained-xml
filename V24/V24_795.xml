<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Asch. &amp; Graebn." date="unknown" rank="section">Oreinos</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section oreinos</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely to loosely tufted, sometimes shortly rhizomatous or stoloniferous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mostly extravaginal or mixed intra and extravaginal.</text>
      <biological_entity id="o9727" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="sometimes shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 5-100 cm tall, 0.5-1.5 mm thick, slender, sometimes weak, terete;</text>
      <biological_entity id="o9728" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="height" src="d0_s3" to="100" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete.</text>
      <biological_entity id="o9729" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths usually closed for 1/5 - 3/5 their length, hybrids sometimes closed for 1/10 – 1/5 their length, terete, smooth or sparsely scabrous;</text>
      <biological_entity id="o9730" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s5" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity id="o9731" name="hybrid" name_original="hybrids" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-4 (6) mm, smooth or sparsely scabrous, truncate to acute, sometimes lacerate;</text>
      <biological_entity id="o9732" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>innovation blades similar to the cauline blades;</text>
      <biological_entity constraint="innovation" id="o9733" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <biological_entity constraint="cauline" id="o9734" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <relation from="o9733" id="r1569" name="to" negation="false" src="d0_s7" to="o9734" />
    </statement>
    <statement id="d0_s8">
      <text>cauline blades 0.8-4 mm wide, flat, thin, lax, soft, adaxial surfaces smooth or sparsely scabrous, narrowly prow-tipped.</text>
      <biological_entity constraint="cauline" id="o9735" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9736" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s8" value="prow-tipped" value_original="prow-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 1.5-15 cm, lax or slightly lax, loosely contracted to open;</text>
      <biological_entity id="o9737" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nodes with 1-3 (5) branches;</text>
      <biological_entity id="o9738" name="node" name_original="nodes" src="d0_s10" type="structure" />
      <biological_entity id="o9739" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <relation from="o9738" id="r1570" name="with" negation="false" src="d0_s10" to="o9739" />
    </statement>
    <statement id="d0_s11">
      <text>branches 1-8 cm, steeply ascending to reflexed, capillary to slender, drooping to fairly straight, sulcate or angled, smooth or the angles scabrous, with 1-15 spikelets.</text>
      <biological_entity id="o9740" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="8" to_unit="cm" />
        <character char_type="range_value" from="steeply ascending" name="orientation" src="d0_s11" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="drooping" value_original="drooping" />
        <character is_modifier="false" modifier="fairly" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o9741" name="angle" name_original="angles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o9742" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
      <relation from="o9741" id="r1571" name="with" negation="false" src="d0_s11" to="o9742" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3.2-8 mm, lengths to 3.5 times widths, narrowly lanceolate to ovate, laterally compressed, not bulbiferous;</text>
      <biological_entity id="o9743" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="0-3.5" value_original="0-3.5" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-5, bisexual;</text>
      <biological_entity id="o9744" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o9745" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes subulate to broadly lanceolate, thin, distinctly keeled, keels smooth or scabrous;</text>
      <biological_entity id="o9746" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s15" to="broadly lanceolate" />
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o9747" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 1-3-veined;</text>
      <biological_entity constraint="lower" id="o9748" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses terete or slightly laterally compressed, glabrous or dorsally webbed;</text>
      <biological_entity id="o9749" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly laterally" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="dorsally" name="pubescence" src="d0_s17" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas 2.5-4.6 mm, lanceolate to broadly lanceolate, distinctly keeled, thin, keels and marginal veins short to long-villous, lateral-veins usually glabrous, infrequently sparsely softly puberulent, lateral-veins obscure or moderately prominent, intercostal regions glabrous;</text>
      <biological_entity id="o9750" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s18" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s18" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="width" src="d0_s18" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o9751" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o9752" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o9753" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently sparsely softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o9754" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o9755" name="region" name_original="regions" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>palea keels scabrous, usually glabrous, infrequently pectinately ciliate;</text>
      <biological_entity constraint="palea" id="o9756" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently pectinately" name="architecture_or_pubescence_or_shape" src="d0_s19" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 3, 0.2-1.1 (1.3) mm.</text>
      <biological_entity id="o9757" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
        <character name="atypical_some_measurement" src="d0_s20" unit="mm" value="1.3" value_original="1.3" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s20" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa sect. Oreinos is circumboreal. It includes seven species: four strictly Eurasian, one amphiatlantic, one primarly western North American with isolated occurrences in the Russian Far East, and one restricted to North America. The species are boreal, alpine to low arctic, and grow in bogs and on alpine slopes. They are primarily slender perennials with extra vaginal tillering.</discussion>
  
</bio:treatment>