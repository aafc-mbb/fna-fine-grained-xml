<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="(Lepage) Barkworth &amp; D. R. Dewey" date="unknown" rank="species">×palmerensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species ×palmerensis</taxon_hierarchy>
  </taxon_identification>
  <number>41</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose, shortly rhizomatous.</text>
      <biological_entity id="o29054" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades (2) 4-8 (15) mm wide, abaxial surfaces scabrous, adaxial surfaces pilose.</text>
      <biological_entity id="o29055" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character name="width" src="d0_s1" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29056" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29057" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikes 14-30 cm long, 1-2.5 cm wide including the awns, 0.3-0.8 (1.5) cm wide excluding the awns, drooping, with 1-2 spikelets per node;</text>
      <biological_entity id="o29058" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2.5" to_unit="cm" />
        <character name="width" src="d0_s2" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity id="o29059" name="awn" name_original="awns" src="d0_s2" type="structure" />
      <biological_entity id="o29060" name="awn" name_original="awns" src="d0_s2" type="structure" />
      <biological_entity id="o29061" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <biological_entity id="o29062" name="node" name_original="node" src="d0_s2" type="structure" />
      <relation from="o29058" id="r4573" name="including the" negation="false" src="d0_s2" to="o29059" />
      <relation from="o29058" id="r4574" name="excluding the" negation="false" src="d0_s2" to="o29060" />
      <relation from="o29058" id="r4575" name="with" negation="false" src="d0_s2" to="o29061" />
      <relation from="o29061" id="r4576" name="per" negation="false" src="d0_s2" to="o29062" />
    </statement>
    <statement id="d0_s3">
      <text>internodes 5-20 mm, scabrous on the angles.</text>
      <biological_entity id="o29063" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character constraint="on angles" constraintid="o29064" is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29064" name="angle" name_original="angles" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spikelets with 3-9 florets.</text>
      <biological_entity id="o29065" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity id="o29066" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
      <relation from="o29065" id="r4577" name="with" negation="false" src="d0_s4" to="o29066" />
    </statement>
    <statement id="d0_s5">
      <text>Glumes 4-10.5 mm long, 0.8-1.5 mm wide, oblong to lanceolate, scabrous, gradually or abruptly narrowing in the distal 1/3-1/4, 3 (4) -veined, margins scarious, apices unawned or awned, awns to 15 mm;</text>
      <biological_entity id="o29067" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character constraint="in distal 1/3-1/4" constraintid="o29068" is_modifier="false" modifier="gradually; abruptly" name="width" src="d0_s5" value="narrowing" value_original="narrowing" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="3(4)-veined" value_original="3(4)-veined" />
      </biological_entity>
      <biological_entity constraint="distal" id="o29068" name="1/3-1/4" name_original="1/3-1/4" src="d0_s5" type="structure" />
      <biological_entity id="o29069" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o29070" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29071" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachillas hairy;</text>
      <biological_entity id="o29072" name="rachilla" name_original="rachillas" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lemmas 8.5-15 mm, hairy, conspicuously keeled distally, awned, awns 3-10 mm;</text>
      <biological_entity id="o29073" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="conspicuously; distally" name="shape" src="d0_s7" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29074" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleas 8.5-15 mm, retuse or truncate;</text>
      <biological_entity id="o29075" name="palea" name_original="paleas" src="d0_s8" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1-2 mm.</text>
      <biological_entity id="o29076" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus ×palmerensis is the name for hybrids between E. macrourus (p. 324) and E. sibiricus (p. 310). It is known from disturbed sites around Palmer, Alaska, and in south-central Alaska. Bowden (1967) also reported it from Fort Liard, in the MacKenzie District, Northwest Territories. Lepage (1952) originally identified the parents as Agropyron sericeum [= E. macrourus] and E. canadensis (p. 303). Later, Lepage (1965) stated that the second parent was E. sibiricus. The above description includes ×Agroelymus hodgsonii Lepage, which, according to Bowden (1967), is a synonym.</discussion>
  <discussion>Elymus ×palmerensis is an Elymus named hybrid</discussion>
  <discussion>Elymus is notorious for its ability to hybridize. Most of its interspecific hybrids are partially fertile, permitting introgression between the parents. The descriptions provided below are restricted to the named interspecific hybrids. They should be treated with caution and some skepticism; some are based solely on the type specimen, because little other reliably identified material was available. Moreover, as the descriptions of the non-hybrid species indicate, many other interspecific hybrids exist.</discussion>
  <discussion>The parentage of all hybrids is best determined in the field. Perennial hybrids, such as those in Elymus, can persist in an area after one or both parents have died out, but the simplest assumption is that both are present. Interspecific hybrids of Elymus that have disarticulating rachises presumably have E. elymoides or E. multisetus as one of their parents.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>