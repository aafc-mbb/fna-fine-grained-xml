<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">MELICA</taxon_name>
    <taxon_name authority="Bol." date="unknown" rank="species">stricta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus melica;species stricta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Rock melic</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose, not rhizomatous.</text>
      <biological_entity id="o30724" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 9-85 cm, not forming corms;</text>
      <biological_entity id="o30725" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s1" to="85" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30726" name="corm" name_original="corms" src="d0_s1" type="structure" />
      <relation from="o30725" id="r4838" name="forming" negation="true" src="d0_s1" to="o30726" />
    </statement>
    <statement id="d0_s2">
      <text>basal internodes often thickened;</text>
      <biological_entity constraint="basal" id="o30727" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes smooth.</text>
      <biological_entity id="o30728" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths scabridulous;</text>
      <biological_entity id="o30729" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2.5-5 mm;</text>
      <biological_entity id="o30730" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1.5-5 mm wide, abaxial surfaces glabrous, scabridulous, adaxial surfaces sometimes strigose, sometimes glabrous or scabridulous.</text>
      <biological_entity id="o30731" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30732" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30733" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3-30 cm;</text>
      <biological_entity id="o30734" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 0.5-10 cm, appressed, with 1-5 spikelets;</text>
      <biological_entity id="o30735" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o30736" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o30735" id="r4839" name="with" negation="false" src="d0_s8" to="o30736" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels sharply bent below the spikelets;</text>
      <biological_entity id="o30738" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation below the glumes.</text>
      <biological_entity id="o30737" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character constraint="below spikelets" constraintid="o30738" is_modifier="false" modifier="sharply" name="shape" src="d0_s9" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o30739" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o30737" id="r4840" name="below" negation="false" src="d0_s10" to="o30739" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 6-23 mm long, 5-13 mm wide, broadly V-shaped when mature, with 2-4 bisexual florets;</text>
      <biological_entity id="o30740" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="23" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="when mature" name="shape" src="d0_s11" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o30741" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="true" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o30740" id="r4841" name="with" negation="false" src="d0_s11" to="o30741" />
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes 1.8-2.1 mm.</text>
      <biological_entity constraint="rachilla" id="o30742" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes 6-16 mm long, 3.5-5 mm wide, 4-7-veined;</text>
      <biological_entity constraint="lower" id="o30743" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s13" to="16" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="4-7-veined" value_original="4-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 6-18 mm long, 3-5 mm wide, 5-9-veined;</text>
      <biological_entity constraint="upper" id="o30744" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s14" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 6-16 mm, glabrous, scabridulous, 5-9-veined, veins inconspicuous, apices acute, unawned;</text>
      <biological_entity id="o30745" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity id="o30746" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o30747" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1/2 - 3/4 the length of the lemmas;</text>
      <biological_entity id="o30748" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s16" to="3/4" />
      </biological_entity>
      <biological_entity id="o30749" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1-3 mm;</text>
      <biological_entity id="o30750" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>rudiments 2-7 mm, resembling the lower florets, acute to acuminate.</text>
      <biological_entity id="o30751" name="rudiment" name_original="rudiments" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="7" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s18" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o30752" name="floret" name_original="florets" src="d0_s18" type="structure" />
      <relation from="o30751" id="r4842" name="resembling the" negation="false" src="d0_s18" to="o30752" />
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 4-5 mm. 2n = 18.</text>
      <biological_entity id="o30753" name="caryopse" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30754" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melica stricta grows from 1200-3350 m on rocky, often dry slopes, sometimes in alpine habitats. Its range extends from Oregon and California to Utah. Boyle (1945) recognized two varieties, more on their marked geographical separation than on their morphological divergence.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah;Calif.;Nev.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Paleas about 3/4 the length of the lemmas; anthers 2-3 mm long</description>
      <determination>Melica stricta var. albicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Paleas about 1/2 the length of the lemmas; anthers 1-2 mm long</description>
      <determination>Melica stricta var. stricta</determination>
    </key_statement>
  </key>
</bio:treatment>