<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Vasey) Barkworth" date="unknown" rank="species">hendersonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species hendersonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hendersonii</taxon_name>
    <taxon_hierarchy>genus stipa;species hendersonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oryzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hendersonii</taxon_name>
    <taxon_hierarchy>genus oryzopsis;species hendersonii</taxon_hierarchy>
  </taxon_identification>
  <number>25</number>
  <other_name type="common_name">Henderson's needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tightly cespitose, not rhizomatous.</text>
      <biological_entity id="o20541" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="tightly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-35 cm tall, 0.3-0.9 mm thick, pubescent below the nodes, glabrous or sparsely puberulent elsewhere;</text>
      <biological_entity id="o20542" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="height" src="d0_s1" to="35" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="thickness" src="d0_s1" to="0.9" to_unit="mm" />
        <character constraint="below nodes" constraintid="o20543" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; elsewhere" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20543" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes 1-2.</text>
      <biological_entity id="o20544" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths completely or mostly glabrous, margins sometimes ciliate distally;</text>
      <biological_entity constraint="basal" id="o20545" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20546" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous;</text>
      <biological_entity id="o20547" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.4-1 mm, hyaline, glabrous or pubescent, rounded;</text>
      <biological_entity id="o20548" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades tightly folded or convolute, to 1 mm wide or thick, abaxial surfaces scabrous, adaxial surfaces pubescent.</text>
      <biological_entity id="o20549" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="tightly" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convolute" value_original="convolute" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20550" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20551" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-12 cm long, 2-5 cm wide, erect;</text>
      <biological_entity id="o20552" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches and pedicels straight, appressed to strongly ascending, longest branches 2-7 cm.</text>
      <biological_entity id="o20553" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s8" to="strongly ascending" />
      </biological_entity>
      <biological_entity id="o20554" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s8" to="strongly ascending" />
      </biological_entity>
      <biological_entity constraint="longest" id="o20555" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 3.5-5.5 mm long, 1-1.5 mm wide, 5-veined;</text>
      <biological_entity id="o20556" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes obtuse, apices rounded to acute;</text>
      <biological_entity constraint="lower" id="o20557" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o20558" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes rounded to obtuse, subequal or to 1 mm shorter than the lower glumes;</text>
      <biological_entity constraint="upper" id="o20559" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s11" to="obtuse" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character name="size" src="d0_s11" value="0-1 mm" value_original="0-1 mm" />
        <character constraint="than the lower glumes" constraintid="o20560" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lower" id="o20560" name="glume" name_original="glumes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>florets 3.5-4.5 mm long, 1-1.5 mm wide, fusiform, laterally compressed;</text>
      <biological_entity id="o20561" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 0.3-0.5 mm, blunt;</text>
      <biological_entity id="o20562" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas coriaceous, glabrous, shiny, apical lobes about 0.2 mm long, thick;</text>
      <biological_entity id="o20563" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20564" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="length" src="d0_s14" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="width" src="d0_s14" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 6-10 mm, readily deciduous, not geniculate, scabrous;</text>
      <biological_entity id="o20565" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="readily" name="duration" src="d0_s15" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas about 3 mm, from3/4 as long as to equaling the lemmas, indurate, glabrous, apices rounded, flat;</text>
      <biological_entity id="o20566" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="3" value_original="3" />
        <character constraint="to lemmas" constraintid="o20567" name="quantity" src="d0_s16" unit="from" value="3/4" value_original="3/4" />
        <character is_modifier="false" name="texture" notes="" src="d0_s16" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20567" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o20568" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers about 2.5 mm, dehiscent, penicillate.</text>
      <biological_entity id="o20569" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s17" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 2.5-4 mm. 2n = 34.</text>
      <biological_entity id="o20570" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20571" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Achnatherum hendersonii grows in dry, rocky, shallow soil, in sagebrush or ponderosa pine associations. It is known from only three counties: Yakima and Kittitas counties, Washington, and Crook County, Oregon. Maze (1981) noted that, at one site, A. hendersonii was restricted to areas subject to frost heaving, although under cultivation, it can grow without such disturbance. He hypothesized that its survival in such sites is attributable to a competitive advantage gained by the structure of its root system. Unlike Poa secunda, which grew in the surrounding, undisturbed areas, the outer cortex and epidermis of the roots of A. hendersonii form a sheath around the stele and inner cortex. When the roots are pulled, this sheath slips and breaks but the internal structures remain intact. In Poa secunda, the outer part of the root is attached to the central core and, when the roots are pulled, they break. Achnatherum hendersonii also differs from P. secunda in having relatively few (9-12), evenly distributed roots that extend to 30 cm.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho;Wash.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>