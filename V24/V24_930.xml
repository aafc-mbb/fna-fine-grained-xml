<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">661</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AGROSTIS</taxon_name>
    <taxon_name authority="Schult." date="unknown" rank="species">elliottiana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus agrostis;species elliottiana</taxon_hierarchy>
  </taxon_identification>
  <number>27</number>
  <other_name type="common_name">Elliott's bent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o20186" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-45 cm, erect, sometimes geniculate at the base, with (3) 4-9 nodes.</text>
      <biological_entity id="o20187" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o20188" is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o20188" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o20189" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s1" value="3" value_original="3" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s1" to="9" />
      </biological_entity>
      <relation from="o20187" id="r3206" name="with" negation="false" src="d0_s1" to="o20189" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal or cauline;</text>
      <biological_entity id="o20190" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves withered at anthesis;</text>
      <biological_entity constraint="basal" id="o20191" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="life_cycle" src="d0_s3" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths smooth or scabridulous;</text>
      <biological_entity id="o20192" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (0.7) 1.5-3.5 mm, dorsal surfaces scabrous, apices acute, rounded, or truncate, lacerate;</text>
      <biological_entity id="o20193" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="0.7" value_original="0.7" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o20194" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20195" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 0.5-4 cm long, 0.5-1 mm wide, flat, becoming involute.</text>
      <biological_entity id="o20196" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="becoming" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3-20 cm long, (0.5) 2-12 cm wide, widely ovate, ultimately open and diffuse, the whole panicle detaching after maturity, blowing about as a tumbleweed, bases usually exserted, sometimes enclosed by the upper sheaths at maturity, lowest node with 1-6 branches;</text>
      <biological_entity id="o20197" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="ultimately" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s7" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity id="o20198" name="panicle" name_original="panicle" src="d0_s7" type="structure" />
      <biological_entity id="o20199" name="tumbleweed" name_original="tumbleweed" src="d0_s7" type="structure" />
      <biological_entity id="o20200" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o20201" name="sheath" name_original="sheaths" src="d0_s7" type="structure" />
      <biological_entity constraint="lowest" id="o20202" name="node" name_original="node" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o20203" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <relation from="o20198" id="r3207" name="as" negation="false" src="d0_s7" to="o20199" />
      <relation from="o20200" id="r3208" modifier="sometimes" name="enclosed by the" negation="false" src="d0_s7" to="o20201" />
      <relation from="o20200" id="r3209" name="at" negation="false" src="d0_s7" to="o20202" />
      <relation from="o20202" id="r3210" name="with" negation="false" src="d0_s7" to="o20203" />
    </statement>
    <statement id="d0_s8">
      <text>branches scabridulous, capillary, branching beyond midlength, initially ascending, becoming laxly spreading, spikelets clustered near the tips, lower branches 2-8 cm;</text>
      <biological_entity id="o20204" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
        <character constraint="beyond midlength" constraintid="o20205" is_modifier="false" name="architecture" src="d0_s8" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="initially" name="orientation" notes="" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="becoming laxly" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o20205" name="midlength" name_original="midlength" src="d0_s8" type="structure" />
      <biological_entity id="o20206" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character constraint="near tips" constraintid="o20207" is_modifier="false" name="arrangement_or_growth_form" src="d0_s8" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o20207" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o20208" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.3-7.5 mm;</text>
      <biological_entity id="o20209" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>secondary panicles sometimes present in the leaf-axils.</text>
      <biological_entity constraint="secondary" id="o20210" name="panicle" name_original="panicles" src="d0_s10" type="structure" />
      <biological_entity id="o20211" name="leaf-axil" name_original="leaf-axils" src="d0_s10" type="structure" />
      <relation from="o20210" id="r3211" modifier="sometimes" name="present in the" negation="false" src="d0_s10" to="o20211" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets narrowly elliptic to lanceolate, yellowish purple to greenish purple.</text>
      <biological_entity id="o20212" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s11" to="lanceolate" />
        <character char_type="range_value" from="yellowish purple" name="coloration_or_density" src="d0_s11" to="greenish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes equal, 1.5-2.2 mm, 1-veined, scabrous on the midvein, margins scabrous distally, acute;</text>
      <biological_entity id="o20213" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character constraint="on midvein" constraintid="o20214" is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20214" name="midvein" name_original="midvein" src="d0_s12" type="structure" />
      <biological_entity id="o20215" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs to 0.6 mm, dense;</text>
      <biological_entity constraint="callus" id="o20216" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="density" src="d0_s13" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 1-2 mm, smooth or scabrous to warty, translucent, 5-veined, veins prominent, apices acute, entire or 2-5-toothed, teeth minute, to 0.8 mm, usually awned from just below the apices, sometimes unawned, awns 3-10 mm, flexuous, not geniculate, deciduous;</text>
      <biological_entity id="o20217" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s14" value="scabrous to warty" value_original="scabrous to warty" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s14" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o20218" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o20219" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-5-toothed" value_original="2-5-toothed" />
      </biological_entity>
      <biological_entity id="o20220" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="minute" value_original="minute" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
        <character constraint="just below apices" constraintid="o20221" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o20221" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity id="o20222" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" modifier="sometimes" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas absent or minute;</text>
      <biological_entity id="o20223" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s15" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 1, 0.1-0.2 mm, lobes widely separated by the connective, usually retained at the apices of the caryopses.</text>
      <biological_entity id="o20224" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20225" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character constraint="by connective" constraintid="o20226" is_modifier="false" modifier="widely" name="arrangement" src="d0_s16" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o20226" name="connective" name_original="connective" src="d0_s16" type="structure" />
      <biological_entity id="o20227" name="apex" name_original="apices" src="d0_s16" type="structure" />
      <biological_entity id="o20228" name="caryopse" name_original="caryopses" src="d0_s16" type="structure" />
      <relation from="o20225" id="r3212" modifier="usually" name="retained at the" negation="false" src="d0_s16" to="o20227" />
      <relation from="o20225" id="r3213" modifier="usually" name="retained at the" negation="false" src="d0_s16" to="o20228" />
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1-1.4 mm;</text>
      <biological_entity id="o20229" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm liquid.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 28.</text>
      <biological_entity id="o20230" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="texture" src="d0_s18" value="liquid" value_original="liquid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20231" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agrostis elliottiana grows in fields and scrublands and along roadsides. It has a disjunct distribution, occurring in western North America in northern California and southern Arizona and New Mexico; in eastern North America from Kansas and Texas east to Pennsylvania and northern Florida; and in Yucatan, Mexico. Although it has been introduced elsewhere, notably in Maine, it is not known to have become established at those locations.</discussion>
  <discussion>Agrostis elliottiana resembles A. scabra (p. 646) and A. hyemalis (p. 647) in its diffuse panicle, but differs in its flexible awn and single anther. Small Californian plants have sometimes been called A. exigua Thurb.; they are otherwise identical to A. elliottiana.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>D.C.;Fla.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Calif.;Ala.;Ark.;Ill.;Ga.;Ind.;Ariz.;N.Mex.;Maine;Md.;Kans.;Okla.;Mass.;Ohio;Mo.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>