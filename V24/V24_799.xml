<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">572</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Asch. &amp; Graebn." date="unknown" rank="section">Oreinos</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laxa x glauca</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section oreinos;species laxa × glauca</taxon_hierarchy>
  </taxon_identification>
  <number>51</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not or only slightly glaucous;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely tufted, not stoloniferous, not rhizomatous.</text>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching mixed intra and extra vaginal.</text>
      <biological_entity id="o18709" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="only slightly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 6-18 cm, with 0 (1) exserted nodes, upper node in the lower 1/3 of the culms.</text>
      <biological_entity id="o18710" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="exserted" id="o18711" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="true" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="upper" id="o18712" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity constraint="lower" id="o18713" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o18714" name="culm" name_original="culms" src="d0_s4" type="structure" />
      <relation from="o18710" id="r2962" name="with" negation="false" src="d0_s4" to="o18711" />
      <relation from="o18712" id="r2963" name="in" negation="false" src="d0_s4" to="o18713" />
      <relation from="o18713" id="r2964" name="part_of" negation="false" src="d0_s4" to="o18714" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/10 – 1/5 their length, terete, smooth or very sparsely scabrous, distal sheath lengths 0.8-1.8 times blade lengths;</text>
      <biological_entity id="o18715" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18716" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o18717" is_modifier="false" name="length" src="d0_s5" value="0.8-1.8 times blade lengths" value_original="0.8-1.8 times blade lengths" />
      </biological_entity>
      <biological_entity id="o18717" name="blade" name_original="blade" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 1.25-2.5 mm, smooth, apices obtuse, often lacerate;</text>
      <biological_entity id="o18718" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.25" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18719" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades thin, sparsely scabrous adaxially, flag leaf-blades 1.6-3.8 cm.</text>
      <biological_entity id="o18720" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="sparsely; adaxially" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o18721" name="blade-leaf" name_original="leaf-blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.6" from_unit="cm" name="distance" src="d0_s7" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 1.5-3.5 cm, slightly lax, ovoid, contracted to loosely contracted, dense to moderately dense, with 2-6 branches per node;</text>
      <biological_entity id="o18722" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="contracted" name="condition_or_size" src="d0_s8" to="loosely contracted" />
        <character char_type="range_value" from="dense" name="density" src="d0_s8" to="moderately dense" />
      </biological_entity>
      <biological_entity id="o18723" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o18724" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o18722" id="r2965" name="with" negation="false" src="d0_s8" to="o18723" />
      <relation from="o18723" id="r2966" name="per" negation="false" src="d0_s8" to="o18724" />
    </statement>
    <statement id="d0_s9">
      <text>branches steeply ascending, fairly straight, sulcate or angled, smooth or infrequently the angles sparsely scabrous, not glaucous.</text>
      <biological_entity id="o18725" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="steeply" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="fairly" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18726" name="angle" name_original="angles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets laterally compressed;</text>
      <biological_entity id="o18727" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2-5;</text>
      <biological_entity id="o18728" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes smooth, glabrous, lower internodes 0.8-1 mm.</text>
      <biological_entity constraint="rachilla" id="o18729" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18730" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes equal, broadly lanceolate, thin;</text>
      <biological_entity id="o18731" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.75-1.05 mm wide, 3-veined;</text>
      <biological_entity constraint="lower" id="o18732" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.75" from_unit="mm" name="width" src="d0_s14" to="1.05" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 3.7-4.7 mm long, 0.9-1.3 mm wide, lengths 3.7-4.1 times widths;</text>
      <biological_entity constraint="upper" id="o18733" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s15" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s15" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s15" value="3.7-4.1" value_original="3.7-4.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses all glabrous, or some proximal florets within a spikelet sparsely webbed;</text>
      <biological_entity id="o18734" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18735" name="floret" name_original="florets" src="d0_s16" type="structure" />
      <biological_entity id="o18736" name="spikelet" name_original="spikelet" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="webbed" value_original="webbed" />
      </biological_entity>
      <relation from="o18735" id="r2967" name="within" negation="false" src="d0_s16" to="o18736" />
    </statement>
    <statement id="d0_s17">
      <text>lemmas 3.7-4.5 mm, broadly lanceolate, distinctly keeled, thin, keels and marginal veins short to long-villous, hairs extending 1/3-1/2 the keel length, lateral-veins usually glabrous, or infrequently sparsely softly puberulent, intercostal regions glabrous;</text>
      <biological_entity id="o18737" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o18738" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o18739" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o18740" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <biological_entity id="o18741" name="keel" name_original="keel" src="d0_s17" type="structure">
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s17" to="1/2" />
      </biological_entity>
      <biological_entity id="o18742" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently sparsely softly" name="length" src="d0_s17" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently sparsely softly" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o18743" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o18740" id="r2968" name="extending" negation="false" src="d0_s17" to="o18741" />
    </statement>
    <statement id="d0_s18">
      <text>palea keels finely scabrous;</text>
      <biological_entity constraint="palea" id="o18744" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 0.8-1.2 mm, poorly formed, sacs not fully maturing, not dehiscing, about 0.1 mm in diameter.</text>
      <biological_entity id="o18745" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s19" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>2n = ca. 65, 70.</text>
      <biological_entity id="o18746" name="sac" name_original="sacs" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="poorly; not fully" name="life_cycle" src="d0_s19" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="not" name="dehiscence" src="d0_s19" value="dehiscing" value_original="dehiscing" />
        <character name="diameter" src="d0_s19" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18747" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="65" value_original="65" />
        <character name="quantity" src="d0_s20" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa laxa × glauca is an eastern low arctic entity which has passed under the name P. flexuosa Sm., P. laxa subsp. flexuosa (Sm.) Hyl., and, more recently, P. laxiuscula (Blytt) Lange. It has also been confused with P. glauca (p. 576). It can be distinguished from P. laxa (see previous) by its more open sheaths and poorly developed, indehiscent anthers. It differs from P. glauca in its broad, thin glumes and lemmas; compact panicles; smooth or nearly smooth, non-glaucous branches; and poorly developed, indehiscent anthers. It also grows in wetter habitats than P. glauca, often around seeps. Its chloroplast DNA is more like that of the American P. laxa subsp. fernaldiana than that of the European subspp. flexuosa and laxa or of P. glauca.</discussion>
  <other_name type="invalid_name">Poa flexuosa; sensu American authors</other_name>
  
</bio:treatment>