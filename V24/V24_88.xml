<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">GLYCERIA</taxon_name>
    <taxon_name authority="G.L. Church" date="unknown" rank="section">Striatae</taxon_name>
    <taxon_name authority="(Lam.) Hitchc." date="unknown" rank="species">striata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus glyceria;section striatae;species striata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Glyceria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">striata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">stricta</taxon_name>
    <taxon_hierarchy>genus glyceria;species striata;variety stricta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Glyceria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">striata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">stricta</taxon_name>
    <taxon_hierarchy>genus glyceria;species striata;subspecies stricta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicularia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nervata</taxon_name>
    <taxon_hierarchy>genus panicularia;species nervata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Glyceria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nervata</taxon_name>
    <taxon_hierarchy>genus glyceria;species nervata</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Ridged glyceria</other_name>
  <other_name type="common_name">Glycerie striee</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o10123" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-80 (100) cm tall, (1.5) 2-3.5 mm thick, not or only slightly spongy, sometimes rooting at the lower nodes.</text>
      <biological_entity id="o10124" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="height" src="d0_s1" unit="cm" value="100" value_original="100" />
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s1" to="80" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="not; only slightly" name="texture" src="d0_s1" value="spongy" value_original="spongy" />
        <character constraint="at lower nodes" constraintid="o10125" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10125" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth to scabridulous, keeled, sometimes weakly so;</text>
      <biological_entity id="o10126" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s2" to="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-4 mm, usually rounded, sometimes acute to mucronate, erose-lacerate;</text>
      <biological_entity id="o10127" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="acute" modifier="sometimes" name="shape" src="d0_s3" to="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="erose-lacerate" value_original="erose-lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 12-30 cm long, 2-6 mm wide, abaxial surfaces smooth or scabridulous, adaxial surfaces scabridulous to scabrous.</text>
      <biological_entity id="o10128" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10129" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10130" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="scabridulous" name="relief" src="d0_s4" to="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 6-25 cm long, 2.5-21 cm wide, pyramidal, open, nodding;</text>
      <biological_entity id="o10131" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s5" to="21" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches 5-13 cm, straight to lax, lower branches usually strongly divergent to drooping at maturity, sometimes ascending, with 15-50 spikelets, these often confined to the distal 2/3;</text>
      <biological_entity id="o10132" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="13" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10133" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually strongly" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s6" value="drooping" value_original="drooping" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="position_or_shape" notes="" src="d0_s6" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s6" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o10134" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s6" to="50" />
      </biological_entity>
      <relation from="o10133" id="r1621" name="with" negation="false" src="d0_s6" to="o10134" />
    </statement>
    <statement id="d0_s7">
      <text>pedicels 0.5-7 mm.</text>
      <biological_entity id="o10135" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 1.8-4 mm long, 1.2-2.9 mm wide, laterally compressed, oval in side view, with 3-7 florets.</text>
      <biological_entity id="o10136" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="2.9" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s8" value="oval" value_original="oval" />
      </biological_entity>
      <biological_entity id="o10137" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <relation from="o10136" id="r1622" name="with" negation="false" src="d0_s8" to="o10137" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes ovate, 1-1.5 times longer than wide, narrowing from midlength or above, veins terminating below the apical margins, apices often splitting with age;</text>
      <biological_entity id="o10138" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1-1.5" value_original="1-1.5" />
        <character constraint="from " constraintid="o10140" is_modifier="false" name="width" src="d0_s9" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o10139" name="midlength" name_original="midlength" src="d0_s9" type="structure" />
      <biological_entity id="o10140" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity constraint="apical" id="o10141" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity constraint="apical" id="o10142" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="often" name="architecture_or_dehiscence" src="d0_s9" value="splitting" value_original="splitting" />
      </biological_entity>
      <relation from="o10140" id="r1623" name="terminating" negation="false" src="d0_s9" to="o10141" />
      <relation from="o10140" id="r1624" name="terminating" negation="false" src="d0_s9" to="o10142" />
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 0.5-1.2 mm, rounded to obtuse;</text>
      <biological_entity constraint="lower" id="o10143" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 0.6-1.2 mm, acute or rounded;</text>
      <biological_entity constraint="upper" id="o10144" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes 0.1-0.6 mm;</text>
      <biological_entity constraint="rachilla" id="o10145" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 1.2-2 mm, ovate in dorsal view, veins raised, scabridulous over and between the veins, apices acute, prow-shaped;</text>
      <biological_entity id="o10146" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="in dorsal view" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o10147" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="raised" value_original="raised" />
        <character constraint="over and between veins" constraintid="o10148" is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o10148" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o10149" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s13" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas slightly shorter than to equaling the lemmas, lengths 1.5-3 times widths, keeled, keels not winged, tips pointing towards each other, apices narrowly notched between the keels;</text>
      <biological_entity id="o10150" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="than to equaling the lemmas" constraintid="o10151" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="1.5-3" value_original="1.5-3" />
        <character is_modifier="false" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o10151" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o10152" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o10153" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character constraint="towards each other" is_modifier="false" name="orientation" src="d0_s14" value="pointing" value_original="pointing" />
      </biological_entity>
      <biological_entity id="o10154" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character constraint="between keels" constraintid="o10155" is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o10155" name="keel" name_original="keels" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2, (0.2) 0.4-0.6 mm, purple or yellow.</text>
      <biological_entity id="o10156" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 0.5-2 mm. 2n = 20 [reports of 28 questionable].</text>
      <biological_entity id="o10157" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10158" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Glyceria striata grows in bogs, along lakes and streams, and in other wet places. Its range extends from Alaska to Newfoundland and south into Mexico. Plants from the eastern portion of the range have sometimes been treated as G. striata var. striata, and those from the west as G. striata var. stricta (Scribn.) Fernald. Eastern plants tend to have somewhat narrower leaves and thin¬ner culms than western plants, but the variation appears continuous. In the west, larger specimens are easy to confuse with G. elata. The two species are sometimes found growing together without hybridizing; this and molecular data (Whipple et al. [in prep.]) support their recognition as separate species. The differences between the two in growth habit and stature are evident in the field; they are not always evident on herbarium specimens. In its overall aspect, G. striata also resembles G. pulchella, but it has somewhat more lax panicle branches in   addition to smaller spikelets and florets.</discussion>
  <discussion>Glyceria xgatineauensis Bowden is a sterile hybrid between G. striata and G. melicaria. It resembles G. melicaria but has longer (up to 12 cm), less appressed panicle branches and is a triploid with 2n = 30. It was described from a population near Eardley, Quebec. An additional specimen, tentatively identified as G. xgatineauensis, was collected in 1929 from French Creek in Upshur County, West Virginia.</discussion>
  <discussion>Glyceria xottawensis Bowden is a sterile hybrid between G. striata and G. canadensis. It is intermediate between the two parents, and is known only from the original populations near Ottawa. It has sometimes been included in G. xlaxa (Scribn.) Scribn. [=G. canadensis var. laxa]; that taxon often produces viable seed, indicating that it is not a hybrid.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;W.Va.;Del.;D.C.;Wis.;Ariz.;N.Mex.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Calif.;Nev.;Va.;Colo.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Iowa;Idaho;Md.;Ohio;Utah;Mo.;Minn.;Mich.;Alaska;Mont.;Miss.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>