<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="B.R. Baum" date="unknown" rank="genus">×ELYLEYMUS</taxon_name>
    <taxon_name authority="(Lepage) Barkworth &amp; D.R. Dewey" date="unknown" rank="species">turneri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×elyleymus;species turneri</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not cespitose, rhizomatous.</text>
      <biological_entity id="o23878" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 110-130 cm, pubescent below the spikes.</text>
      <biological_entity id="o23879" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="110" from_unit="cm" name="some_measurement" src="d0_s1" to="130" to_unit="cm" />
        <character constraint="below spikes" constraintid="o23880" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23880" name="spike" name_original="spikes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves glaucous;</text>
      <biological_entity id="o23881" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles present, at least on the innovations;</text>
      <biological_entity id="o23882" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23883" name="innovation" name_original="innovations" src="d0_s3" type="structure" />
      <relation from="o23882" id="r3785" modifier="at-least" name="on" negation="false" src="d0_s3" to="o23883" />
    </statement>
    <statement id="d0_s4">
      <text>blades 3-5 mm wide, abaxial surfaces scabrous or smooth, adaxial surfaces scabrous.</text>
      <biological_entity id="o23884" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23885" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23886" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences spikelike racemes, 11-20 cm long, lax, with (1) 2 spikelets per node, spikelets at the lower nodes unequally pedicellate, longer pedicels 1.5-3 mm;</text>
      <biological_entity id="o23887" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="length" notes="" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o23888" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o23889" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23890" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o23891" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity constraint="lower" id="o23892" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="unequally" name="architecture" src="d0_s5" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="longer" id="o23893" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o23887" id="r3786" name="with" negation="false" src="d0_s5" to="o23889" />
      <relation from="o23889" id="r3787" name="per" negation="false" src="d0_s5" to="o23890" />
      <relation from="o23891" id="r3788" name="at" negation="false" src="d0_s5" to="o23892" />
    </statement>
    <statement id="d0_s6">
      <text>internodes 6-15 (30) mm, those at the base of the spike longest, angles hispid, convex surfaces pubescent or pilose.</text>
      <biological_entity id="o23894" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="30" value_original="30" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23895" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity id="o23896" name="spike" name_original="spike" src="d0_s6" type="structure" />
      <biological_entity id="o23897" name="angle" name_original="angles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o23898" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o23894" id="r3789" name="at" negation="false" src="d0_s6" to="o23895" />
      <relation from="o23895" id="r3790" name="part_of" negation="false" src="d0_s6" to="o23896" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 15-25 mm, with 5-8 florets;</text>
      <biological_entity id="o23900" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <relation from="o23899" id="r3791" name="with" negation="false" src="d0_s7" to="o23900" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation above or below the glumes.</text>
      <biological_entity id="o23899" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23901" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <relation from="o23899" id="r3792" name="above or below" negation="false" src="d0_s8" to="o23901" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes unequal, 2-17 mm long, 0.5-1.2 mm wide, subulate to narrowly lanceolate, scabrous or pilose, (0) 1-3-veined;</text>
      <biological_entity id="o23902" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s9" to="narrowly lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="(0)1-3-veined" value_original="(0)1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla internodes pubescent;</text>
      <biological_entity constraint="rachilla" id="o23903" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 6-16 mm, pubescent, pilose, or villous, backs often glabrate, apices awned, awns 0.7-8 mm;</text>
      <biological_entity id="o23904" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o23905" name="back" name_original="backs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s11" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o23906" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23907" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas 6.5-10 mm, puberulent or pubescent between the veins;</text>
      <biological_entity id="o23908" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
        <character constraint="between veins" constraintid="o23909" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23909" name="vein" name_original="veins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (2.8) 3.5-5 mm. 2n = 28.</text>
      <biological_entity id="o23910" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23911" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>×Elyleymus turneri is treated here as the name for hybrids between Elymus lanceolatus and Leymus innovatus, in agreement with Bowden (1952). The type material was found on the banks of the Saskatchewan River, 2 miles below Fort Saskatchewan, Alberta. Lepage (1952) noted that Dr. Turner, the collector, reported that both Agropyon stnithii [= Pascopyrum smithii] and A. dasystachyum [= Elymus lanceolatus] grew in the vicinity, and argued for A. smithii as the Elymus parent. Bowden (1959) reported 2n= 28 in specimens from the type locality, and used this to argue for the Elymus parent being either A. dasystachyum [= Elymus lanceolatus] or A. trachycaulum [= Elymus trachycaulus].</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>