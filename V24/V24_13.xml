<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Christopher M.A. Stapleton;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Luerss." date="unknown" rank="subfamily">BAMBUSOIDEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="tribe">BAMBUSEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">BAMBUSA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily bambusoideae;tribe bambuseae;genus bambusa</taxon_hierarchy>
  </taxon_identification>
  <number>2.02</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually arborescent, in well defined or rather loose clumps;</text>
      <biological_entity id="o9438" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9439" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="well" name="prominence" src="d0_s0" value="defined" value_original="defined" />
        <character is_modifier="true" modifier="rather" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o9438" id="r1511" name="in" negation="false" src="d0_s0" to="o9439" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes pachymorphic, with short necks.</text>
      <biological_entity id="o9440" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o9441" name="neck" name_original="necks" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o9440" id="r1512" name="with" negation="false" src="d0_s1" to="o9441" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 0.5-30 (35) m tall, 0.5-18 (20) cm thick, woody, perennial, usually self-supporting;</text>
      <biological_entity id="o9442" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="m" value="35" value_original="35" />
        <character char_type="range_value" from="0.5" from_unit="m" name="height" src="d0_s2" to="30" to_unit="m" />
        <character name="thickness" src="d0_s2" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="thickness" src="d0_s2" to="18" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="duration" src="d0_s2" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s2" value="self-supporting" value_original="self-supporting" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes not swollen;</text>
      <biological_entity id="o9443" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>supranodal ridges obscure;</text>
      <biological_entity constraint="supranodal" id="o9444" name="ridge" name_original="ridges" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes terete, usually thinly covered initially with light-colored wax.</text>
      <biological_entity id="o9445" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character constraint="with wax" constraintid="o9446" is_modifier="false" modifier="usually thinly" name="position_relational" src="d0_s5" value="covered" value_original="covered" />
      </biological_entity>
      <biological_entity id="o9446" name="wax" name_original="wax" src="d0_s5" type="substance">
        <character is_modifier="true" name="coloration" src="d0_s5" value="light-colored" value_original="light-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Branch complements usually with a dominant primary central branch and 2 smaller codominant lateral branches, usually similar at all nodes;</text>
      <biological_entity constraint="branch" id="o9447" name="complement" name_original="complements" src="d0_s6" type="structure" />
      <biological_entity constraint="primary central" id="o9448" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="dominant" value_original="dominant" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="codominant" value_original="codominant" />
      </biological_entity>
      <biological_entity constraint="primary central" id="o9449" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="dominant" value_original="dominant" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="codominant" value_original="codominant" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o9450" name="branch" name_original="branch" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o9451" name="branch" name_original="branch" src="d0_s6" type="structure" />
      <biological_entity id="o9452" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <relation from="o9447" id="r1513" name="with" negation="false" src="d0_s6" to="o9448" />
      <relation from="o9447" id="r1514" name="with" negation="false" src="d0_s6" to="o9449" />
      <relation from="o9447" id="r1515" modifier="usually" name="at" negation="false" src="d0_s6" to="o9452" />
    </statement>
    <statement id="d0_s7">
      <text>bud-scales 2-keeled, thickened, initially closed at the back and front;</text>
      <biological_entity id="o9453" name="bud-scale" name_original="bud-scales" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o9454" name="back" name_original="back" src="d0_s7" type="structure" />
      <biological_entity id="o9455" name="front" name_original="front" src="d0_s7" type="structure" />
      <relation from="o9453" id="r1516" modifier="initially" name="closed at the" negation="false" src="d0_s7" to="o9454" />
      <relation from="o9453" id="r1517" modifier="initially" name="closed at the" negation="false" src="d0_s7" to="o9455" />
    </statement>
    <statement id="d0_s8">
      <text>branches all subtended by bracts, higher order branchlets at the lower nodes sometimes thornlike.</text>
      <biological_entity id="o9456" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="higher" value_original="higher" />
      </biological_entity>
      <biological_entity id="o9457" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o9458" name="branchlet" name_original="branchlets" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o9459" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s8" value="thornlike" value_original="thornlike" />
      </biological_entity>
      <relation from="o9456" id="r1518" name="subtended by" negation="false" src="d0_s8" to="o9457" />
      <relation from="o9458" id="r1519" name="at" negation="false" src="d0_s8" to="o9459" />
    </statement>
    <statement id="d0_s9">
      <text>Culm leaves usually promptly deciduous, initially lightly waxy, sometimes with short, stiff hairs, subsequently losing the wax and becoming glabrous;</text>
      <biological_entity constraint="culm" id="o9460" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually promptly" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="initially lightly" name="texture" src="d0_s9" value="ceraceous" value_original="waxy" />
        <character is_modifier="false" modifier="subsequently; becoming" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9461" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o9462" name="wax" name_original="wax" src="d0_s9" type="substance" />
      <relation from="o9460" id="r1520" modifier="sometimes" name="with" negation="false" src="d0_s9" to="o9461" />
      <relation from="o9460" id="r1521" modifier="subsequently" name="losing the" negation="false" src="d0_s9" to="o9462" />
    </statement>
    <statement id="d0_s10">
      <text>auricles usually well developed;</text>
      <biological_entity id="o9463" name="auricle" name_original="auricles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fimbriae usually present;</text>
      <biological_entity id="o9464" name="fimbria" name_original="fimbriae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>blades triangular to broadly triangular, usually erect.</text>
      <biological_entity id="o9465" name="blade" name_original="blades" src="d0_s12" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s12" to="broadly triangular" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Foliage leaves: sheaths usually deciduous from the lower nodes of the branches, persistent at the distal nodes;</text>
      <biological_entity constraint="foliage" id="o9466" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <biological_entity id="o9467" name="sheath" name_original="sheaths" src="d0_s13" type="structure">
        <character constraint="from lower nodes" constraintid="o9468" is_modifier="false" modifier="usually" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
        <character constraint="at distal nodes" constraintid="o9470" is_modifier="false" name="duration" notes="" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9468" name="node" name_original="nodes" src="d0_s13" type="structure" />
      <biological_entity id="o9469" name="branch" name_original="branches" src="d0_s13" type="structure" />
      <biological_entity constraint="distal" id="o9470" name="node" name_original="nodes" src="d0_s13" type="structure" />
      <relation from="o9468" id="r1522" name="part_of" negation="false" src="d0_s13" to="o9469" />
    </statement>
    <statement id="d0_s14">
      <text>blades to 30 cm long, to 6 cm wide, not distinctly cross veined.</text>
      <biological_entity constraint="foliage" id="o9471" name="leaf" name_original="leaves" src="d0_s14" type="structure" />
      <biological_entity id="o9472" name="blade" name_original="blades" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s14" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s14" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="not distinctly" name="shape" src="d0_s14" value="cross" value_original="cross" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Inflorescences usually spicate, rarely capitate, bracteate;</text>
      <biological_entity id="o9473" name="inflorescence" name_original="inflorescences" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s15" value="spicate" value_original="spicate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>prophylls 2-keeled, narrow.</text>
      <biological_entity id="o9474" name="prophyll" name_original="prophylls" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="size_or_width" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pseudospikelets 1-5 cm, with 3-12 florets;</text>
      <biological_entity id="o9476" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s17" to="12" />
      </biological_entity>
      <relation from="o9475" id="r1523" name="with" negation="false" src="d0_s17" to="o9476" />
    </statement>
    <statement id="d0_s18">
      <text>disarticulation above the glumes and below the florets, rapid;</text>
      <biological_entity id="o9475" name="pseudospikelet" name_original="pseudospikelets" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s17" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9477" name="glume" name_original="glumes" src="d0_s18" type="structure" />
      <biological_entity id="o9478" name="floret" name_original="florets" src="d0_s18" type="structure" />
      <relation from="o9475" id="r1524" name="above" negation="false" src="d0_s18" to="o9477" />
    </statement>
    <statement id="d0_s19">
      <text>rachilla internodes usually long.</text>
      <biological_entity constraint="rachilla" id="o9479" name="internode" name_original="internodes" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s19" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Glumes several, subtending the buds;</text>
      <biological_entity id="o9480" name="glume" name_original="glumes" src="d0_s20" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s20" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o9481" name="bud" name_original="buds" src="d0_s20" type="structure">
        <character is_modifier="true" name="position" src="d0_s20" value="subtending" value_original="subtending" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>lemmas narrowly ovate, acute, unawned;</text>
      <biological_entity id="o9482" name="lemma" name_original="lemmas" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s21" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s21" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>paleas not exceeding the lemmas, 2-keeled, not winged;</text>
      <biological_entity id="o9483" name="palea" name_original="paleas" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o9484" name="lemma" name_original="lemmas" src="d0_s22" type="structure" />
      <relation from="o9483" id="r1525" name="exceeding the" negation="true" src="d0_s22" to="o9484" />
    </statement>
    <statement id="d0_s23">
      <text>anthers 6;</text>
      <biological_entity id="o9485" name="anther" name_original="anthers" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>ovaries usually suboblong;</text>
      <biological_entity id="o9486" name="ovary" name_original="ovaries" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s24" value="suboblong" value_original="suboblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>styles short, with (2) 3-4 plumose branches.</text>
      <biological_entity id="o9488" name="branch" name_original="branches" src="d0_s25" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s25" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s25" to="4" />
        <character is_modifier="true" name="shape" src="d0_s25" value="plumose" value_original="plumose" />
      </biological_entity>
      <relation from="o9487" id="r1526" name="with" negation="false" src="d0_s25" to="o9488" />
    </statement>
    <statement id="d0_s26">
      <text>2n = 56-72.</text>
      <biological_entity id="o9487" name="style" name_original="styles" src="d0_s25" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s25" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9489" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character char_type="range_value" from="56" name="quantity" src="d0_s26" to="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bambusa is a tropical and subtropical genus of 75-100+ species. It is native to southern and southeastern Asia, but is widely cultivated and naturalized throughout the tropics. Bambusa vulgaris and B. multiplex grow widely in Florida and Texas, having spread to some extent after being planted as ornamentals. Other species are known only in cultivation. The American Bamboo Society lists over 40 species as being commercially available in North America in 2005. This treatment includes a few of the more commonly cultivated species.</discussion>
  <references>
    <reference>But, P.P.-H., L.-C. Chia, H.-L.F., and S.-Y. Hu. 1985. Hong Kong Bamboos. Urban Council, Hong Kong. 85 pp.</reference>
    <reference>Dransfield, S. and E.A. Widjaja (eds.). 1995. Plant Resources of South-East Asia [PROSEA] No. 7: Bamboos. Backhuys, Leiden, The Netherlands. 189 pp.</reference>
    <reference>Edelman, D.K., T.R. Soderstrom, and G.E Deitzer. 1985. Bamboo introduction and research in Puerto Rico. J. Amer. Bamboo Soc. 6: 43-57</reference>
    <reference>McClure, F.A. 1955. Bambusa Schreb. Fieldiana, Bot. 242: 52-60</reference>
    <reference>Pohl, R.W. 1994. Bambusa Schreber. Pp. 193-194 in G. Davidse, M. Sousa S., and A.O. Chater (eds.). Flora Mesoamericana, vol. 6: Alismataceae a Cyperaceae. Universidad Nacional Autonoma de Mexico, Instituto de Biologia, Mexico, D.F., Mexico. 543 pp.</reference>
    <reference>Soderstrom, T.R. and R.P. Ellis. 1988. The woody bamboos (Poaceae: Bambuseae) of Sri Lanka: A morphological-anatomical study. Smithsonian Contr. Bot. 72:1-75</reference>
    <reference>Stapleton, C.M.A. 1994. The bamboos of Nepal and Bhutan, Part I: Bambusa, Dendrocalamus, Melocanna, Cephalostacbyum, Teinostachyum, and Pseudostachyum (Gramineae: Poaceae, Bambusoideae). Edinburgh J. Bot. 51:1-32</reference>
    <reference>Stapleton, C.M.A. 2002. Bambusa ventricosa versus Bambusa tuldoides. Bamboo 23:17-18</reference>
    <reference>Widjaja, E.A. 1997. New taxa in Indonesian bamboos. Reinwardtia 11:57-152</reference>
    <reference>Wong, K.M. 1995. The Morphology, Anatomy, Biology, and Classification of Peninsular Malayan Bamboos. University of Malaya Botanical Monographs No. 1. University of Malaya, Kuala Lumpur, Malaysia. 189 pp.</reference>
    <reference>Xia, N.H. and C.M.A. Stapleton. 1997. Typification of Bambusa bambos (Gramineae, Bambusoideae). Kew Bull. 52:693-698.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Pacific Islands (Hawaii);S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Branchlets of the lower branches recurved, hardened, thornlike</description>
      <determination>1 Bambusa bambos</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Branchlets of the lower branches not thornlike.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Culm sheath auricles well developed, to 5 cm long</description>
      <determination>2 Bambusa vulgaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Culm sheath auricles absent or poorly developed.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Culm internodes antrorsely hispid; culms 0.5-7 m tall, broadly arched above</description>
      <determination>3 Bambusa multiplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Culm internodes glabrous; culms 6-15 m tall, erect</description>
      <determination>4 Bambusa oldhamii</determination>
    </key_statement>
  </key>
</bio:treatment>