<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">643</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AGROSTIS</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="species">vinealis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus agrostis;species vinealis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trinii</taxon_name>
    <taxon_hierarchy>genus agrostis;species trinii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hyperborea</taxon_name>
    <taxon_hierarchy>genus agrostis;species hyperborea</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Brown bent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose, rhizomatous, rhizomes to about 10 cm, slender, scaly, not stoloniferous.</text>
      <biological_entity id="o5163" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5164" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="to about 10 cm" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-60 cm, erect or geniculate at the base, slender, smooth, with 1-2 (4) nodes.</text>
      <biological_entity id="o5165" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5166" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="at the base" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s2" value="4" value_original="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths smooth;</text>
      <biological_entity id="o5167" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.6-5 mm, dorsal surfaces scabridulous, apices acute to obtuse, entire or lacerate to erose;</text>
      <biological_entity id="o5168" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o5169" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o5170" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse entire or lacerate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse entire or lacerate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-10 cm long, 1-3 mm wide, usually flat, sometimes involute, sometimes bristlelike, adaxial surfaces scabrous, abaxial surfaces sometimes scabrous.</text>
      <biological_entity id="o5171" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="bristlelike" value_original="bristlelike" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5172" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5173" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 2-15 cm long, (0.8) 1-5.5 (8) cm wide, lanceolate to oblong, somewhat open, often contracted after anthesis, lowest node with (1) 3-8 branches;</text>
      <biological_entity id="o5174" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="0.8" value_original="0.8" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="oblong" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character constraint="after lowest node" constraintid="o5175" is_modifier="false" modifier="often" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o5175" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o5176" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <relation from="o5175" id="r842" name="with" negation="false" src="d0_s6" to="o5176" />
    </statement>
    <statement id="d0_s7">
      <text>branches scabrous, readily visible, more or less erect, branched mostly at or below midlength, spikelets closely clustered, lower branches 3-5 cm;</text>
      <biological_entity id="o5177" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="readily" name="prominence" src="d0_s7" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="at midlength" constraintid="o5178" is_modifier="false" name="architecture" src="d0_s7" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o5178" name="midlength" name_original="midlength" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="below midlength" name="position" src="d0_s7" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o5179" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement_or_growth_form" src="d0_s7" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5180" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.5-2 mm.</text>
      <biological_entity id="o5181" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets lanceolate to narrowly oblong, greenish, purplish, or brownish.</text>
      <biological_entity id="o5182" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="narrowly oblong" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes equal to subequal, 2-4 mm, membranous, acute to acuminate;</text>
      <biological_entity id="o5183" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 1-veined, scabrous to scabridulous over the midvein;</text>
      <biological_entity constraint="lower" id="o5184" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" constraint="over midvein" constraintid="o5185" from="scabrous" name="relief" src="d0_s11" to="scabridulous" />
      </biological_entity>
      <biological_entity id="o5185" name="midvein" name_original="midvein" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes usually shorter than the lower glumes, 1 (3) -veined, almost smooth;</text>
      <biological_entity constraint="upper" id="o5186" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="than the lower glumes" constraintid="o5187" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1(3)-veined" value_original="1(3)-veined" />
        <character is_modifier="false" modifier="almost" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5187" name="glume" name_original="glumes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>callus hairs to 0.1 mm, sparse;</text>
      <biological_entity constraint="callus" id="o5188" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 1.5-2.4 mm, about 3/4 the length of the glumes, bases minutely pubescent, glabrous and smooth elsewhere, translucent to opaque, 5-veined, veins usually prominent distally, apices blunt, entire, usually awned from near the base, awns 2-4.5 mm, geniculate, rarely unawned;</text>
      <biological_entity id="o5189" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character name="quantity" src="d0_s14" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o5190" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o5191" name="base" name_original="bases" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="minutely" name="length" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="elsewhere" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="translucent" name="coloration" src="d0_s14" to="opaque" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o5192" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o5193" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character constraint="from base, awns" constraintid="o5194, o5195" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o5194" name="base" name_original="base" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5195" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas to about 0.2 mm;</text>
      <biological_entity id="o5196" name="palea" name_original="paleas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 3,1-1.8 mm.</text>
      <biological_entity id="o5197" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 0.8-1.3 mm;</text>
      <biological_entity id="o5198" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm solid.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 28.</text>
      <biological_entity id="o5199" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5200" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agrostis vinealis is native to Eurasia; it is not clear if populations in Greenland and Alaska represent a circumboreal distribution, or are introductions. It forms a fine, compact turf. It is similar to A. canina (see previous) in its habitat, except that it appears to be more heat tolerant and drought resistant. It used to be included in A. canina, but differs from that species in its subterranean rhizomes and lack of leafy stolons. Agrostis vinealis readily hybridizes with A. capillaris (p. 639) and A. stolonifera (p. 641), the hybrids being somewhat intermediate between the two parents.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;Greenland</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>