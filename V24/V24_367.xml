<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Signe Frederiksen;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="(Coss. &amp; Durieu) T. Durand" date="unknown" rank="genus">DASYPYRUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus dasypyrum</taxon_hierarchy>
  </taxon_identification>
  <number>13.03</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>shortly rhizomatous if perennial.</text>
      <biological_entity id="o26546" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="if perennial" is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-100 cm.</text>
      <biological_entity id="o26547" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o26548" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles present, often inconspicuous;</text>
      <biological_entity id="o26549" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-1 mm, membranous, truncate;</text>
      <biological_entity id="o26550" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-5 mm wide, flat, linear, central vein distinct on the abaxial side.</text>
      <biological_entity id="o26551" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="central" id="o26552" name="vein" name_original="vein" src="d0_s6" type="structure">
        <character constraint="on abaxial side" constraintid="o26553" is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26553" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal spikes, 4-12 cm long including the awns, 0.6-2 cm wide excluding the awns, compressed, dense, with 1 spikelet per node;</text>
      <biological_entity id="o26554" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" notes="" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26555" name="spike" name_original="spikes" src="d0_s7" type="structure" />
      <biological_entity id="o26556" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <biological_entity id="o26557" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <biological_entity id="o26558" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o26559" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o26554" id="r4190" name="including the" negation="false" src="d0_s7" to="o26556" />
      <relation from="o26554" id="r4191" name="excluding the" negation="false" src="d0_s7" to="o26557" />
      <relation from="o26554" id="r4192" name="with" negation="false" src="d0_s7" to="o26558" />
      <relation from="o26558" id="r4193" name="per" negation="false" src="d0_s7" to="o26559" />
    </statement>
    <statement id="d0_s8">
      <text>rachis internodes flat, margins ciliate, hairs white;</text>
      <biological_entity constraint="rachis" id="o26560" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o26561" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26562" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>middle internodes 1-3 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>disarticulation in the rachises, at the nodes beneath each spikelet.</text>
      <biological_entity constraint="middle" id="o26563" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26564" name="rachis" name_original="rachises" src="d0_s10" type="structure" />
      <biological_entity id="o26565" name="node" name_original="nodes" src="d0_s10" type="structure" />
      <biological_entity id="o26566" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
      <relation from="o26563" id="r4194" name="in" negation="false" src="d0_s10" to="o26564" />
      <relation from="o26565" id="r4195" name="beneath" negation="false" src="d0_s10" to="o26566" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 25-75 mm including the awns, 7-22 mm excluding the awns, more than 3 times the length of the rachis internodes, usually divergent, sometimes ascending, laterally compressed, with 2-4 florets, the lower 2 florets usually bisexual, the terminal florets sterile;</text>
      <biological_entity id="o26567" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s11" to="75" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
        <character is_modifier="false" name="length" src="d0_s11" value="3+ times the length of" />
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o26568" name="awn" name_original="awns" src="d0_s11" type="structure" />
      <biological_entity id="o26569" name="awn" name_original="awns" src="d0_s11" type="structure" />
      <biological_entity constraint="rachis" id="o26570" name="internode" name_original="internodes" src="d0_s11" type="structure" />
      <biological_entity id="o26571" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26572" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26573" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26574" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o26567" id="r4196" name="including the" negation="false" src="d0_s11" to="o26568" />
      <relation from="o26567" id="r4197" name="excluding the" negation="false" src="d0_s11" to="o26569" />
      <relation from="o26567" id="r4198" name="with" negation="false" src="d0_s11" to="o26571" />
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes below the lower florets shorter than those below the terminal florets.</text>
      <biological_entity constraint="rachilla" id="o26575" name="internode" name_original="internodes" src="d0_s12" type="structure" />
      <biological_entity constraint="lower" id="o26576" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o26575" id="r4199" name="below" negation="false" src="d0_s12" to="o26576" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes equal, to 40 mm including the awns, to 8 mm excluding the awns, coriaceous, usually 5-veined, strongly 2-keeled, keels with 1-3 mm hairs, margins unequal, stiff, translucent, apices tapering into scabrous awns;</text>
      <biological_entity id="o26577" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s13" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s13" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o26578" name="awn" name_original="awns" src="d0_s13" type="structure" />
      <biological_entity id="o26579" name="awn" name_original="awns" src="d0_s13" type="structure" />
      <biological_entity id="o26580" name="keel" name_original="keels" src="d0_s13" type="structure" />
      <biological_entity id="o26581" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26582" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="fragility" src="d0_s13" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s13" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o26583" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character constraint="into awns" constraintid="o26584" is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o26584" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o26577" id="r4200" name="including the" negation="false" src="d0_s13" to="o26578" />
      <relation from="o26577" id="r4201" name="excluding the" negation="false" src="d0_s13" to="o26579" />
      <relation from="o26580" id="r4202" name="with" negation="false" src="d0_s13" to="o26581" />
    </statement>
    <statement id="d0_s14">
      <text>bisexual lemmas 9-13 mm excluding the awns, lanceolate, keeled, usually 5-veined, apices acuminate, awned, awns to 60 mm;</text>
      <biological_entity id="o26585" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o26586" name="awn" name_original="awns" src="d0_s14" type="structure" />
      <biological_entity id="o26587" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26588" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="60" to_unit="mm" />
      </biological_entity>
      <relation from="o26585" id="r4203" name="excluding the" negation="false" src="d0_s14" to="o26586" />
    </statement>
    <statement id="d0_s15">
      <text>sterile lemmas smaller, awns to 10 mm;</text>
      <biological_entity id="o26589" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="size" src="d0_s15" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o26590" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas narrowly lanceolate, membranous, 2-veined, 2-keeled;</text>
      <biological_entity id="o26591" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" name="shape" src="d0_s16" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lodicules 2, free, membranous, ciliate or glabrous;</text>
      <biological_entity id="o26592" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="free" value_original="free" />
        <character is_modifier="false" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 4-7 mm, yellow;</text>
      <biological_entity id="o26593" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovaries pubescent;</text>
      <biological_entity id="o26594" name="ovary" name_original="ovaries" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 2, free to the base, x = 7.</text>
      <biological_entity id="o26596" name="base" name_original="base" src="d0_s20" type="structure" />
      <biological_entity constraint="x" id="o26597" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Haplome V.</text>
      <biological_entity id="o26595" name="style" name_original="styles" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character constraint="to base" constraintid="o26596" is_modifier="false" name="fusion" src="d0_s20" value="free" value_original="free" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dasypyrum is a Mediterranean genus of two species; only one has been collected in the Flora region. The hairy, 2-keeled glumes make the genus easily distinguishable from other genera in the Triticeae.</discussion>
  <references>
    <reference>Frederiksen, S. 1991. Taxonomic studies in Dasypyrum (Poaceae). Nordic J. Bot. 11:135-142.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>