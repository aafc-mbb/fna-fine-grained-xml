<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">SECALE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">cereale</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus secale;species cereale</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Rye</other_name>
  <other_name type="common_name">Seigle</other_name>
  <other_name type="common_name">Seigle cultivé</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or biennial.</text>
      <biological_entity id="o8024" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (35) 50-120 (300) cm.</text>
      <biological_entity id="o8025" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="35" value_original="35" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Blades (3) 4-12 mm wide, usually glabrous.</text>
      <biological_entity id="o8026" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character name="width" src="d0_s2" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes (2) 4.5-12 (19) cm, often nodding when ma¬ture;</text>
    </statement>
    <statement id="d0_s4">
      <text>disarticulation tardy, in the rachises, at the nodes, or not occurring.</text>
      <biological_entity id="o8027" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="when ma ¬ ture" name="orientation" src="d0_s3" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o8028" name="rachis" name_original="rachises" src="d0_s4" type="structure" />
      <biological_entity id="o8029" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Glumes 8-20 mm, keels scabrous, terminating in awns, awns 1-3 mm;</text>
      <biological_entity id="o8030" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8031" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8032" name="awn" name_original="awns" src="d0_s5" type="structure" />
      <biological_entity id="o8033" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o8031" id="r1301" name="terminating in" negation="false" src="d0_s5" to="o8032" />
    </statement>
    <statement id="d0_s6">
      <text>lemmas 14-18 mm, awns 7-50 mm;</text>
      <biological_entity id="o8034" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8035" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers about 7 mm. 2n = 14, 21, 28.</text>
      <biological_entity id="o8036" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="7" value_original="7" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8037" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" value_original="14" />
        <character name="quantity" src="d0_s7" value="21" value_original="21" />
        <character name="quantity" src="d0_s7" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Secale cereale is one of the world's most important cereal grasses; it is also widely used in North America for soil stabilization and, particularly in Canada, for whisky. When dry, the spike is often distinctly nodding. Frederiksen and Petersen (1998) placed cultivated plants with a non-disarticulating rachis into Secale cereale L. subsp. cereale, and wild or weedy plants with more fragile rachises into S. cereale subsp. ancestrale Zhuk.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;D.C.;Wis.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;N.Mex.;Tex.;La.;N.Dak.;Nebr.;Tenn.;N.C.;S.C.;Pa.;Alaska;Nev.;Va.;Colo.;Md.;Calif.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Mont.;Oreg.;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Miss.;Ky.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>