<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">628</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">DESCHAMPSIA</taxon_name>
    <taxon_name authority="Raup" date="unknown" rank="species">mackenzieana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus deschampsia;species mackenzieana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Mackenzie hairgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose.</text>
      <biological_entity id="o24261" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-80 cm, smooth, glabrous, sometimes decumbent at the base and rooting at the lower nodes.</text>
      <biological_entity id="o24262" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o24263" is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o24264" is_modifier="false" name="architecture" notes="" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o24263" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity constraint="lower" id="o24264" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves not forming a tuft;</text>
      <biological_entity constraint="basal" id="o24265" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24266" name="tuft" name_original="tuft" src="d0_s2" type="structure" />
      <relation from="o24265" id="r3843" name="forming a" negation="false" src="d0_s2" to="o24266" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths smooth;</text>
      <biological_entity id="o24267" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 4-7.5 mm, acute;</text>
      <biological_entity id="o24268" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-3 mm wide, convolute to involute, abaxial surfaces smooth, adaxial surfaces scabrous.</text>
      <biological_entity id="o24269" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="convolute" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24270" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24271" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-20 cm long, 8-14 cm wide;</text>
      <biological_entity id="o24272" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s6" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches ascending to laxly diverging or reflexed, somewhat scabrous, longest branches at the lower nodes usually undivided for 1/3-1/2 their length.</text>
      <biological_entity id="o24273" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="laxly diverging or reflexed" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="longest" id="o24274" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity constraint="lower" id="o24275" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s7" value="undivided" value_original="undivided" />
      </biological_entity>
      <relation from="o24274" id="r3844" name="at" negation="false" src="d0_s7" to="o24275" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 6-7.5 mm, bisexual.</text>
      <biological_entity id="o24276" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes acuminate, equaling or slightly longer than the distal floret;</text>
      <biological_entity id="o24277" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character constraint="than the distal floret" constraintid="o24278" is_modifier="false" name="length_or_size" src="d0_s9" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24278" name="floret" name_original="floret" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>callus hairs 1.5-2 mm;</text>
      <biological_entity constraint="callus" id="o24279" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 4.5-5.5 mm, awns attached on the lower 1/4 - 2/3, inconspicuous, weakly geniculate, from shorter than to exceeding the lemma by approximately 2 mm;</text>
      <biological_entity id="o24280" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24281" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character constraint="on lower 1/4-2/3" constraintid="o24282" is_modifier="false" name="fixation" src="d0_s11" value="attached" value_original="attached" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s11" value="geniculate" value_original="geniculate" />
        <character constraint="than to exceeding the lemma" constraintid="o24283" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lower" id="o24282" name="1/4-2/3" name_original="1/4-2/3" src="d0_s11" type="structure" />
      <biological_entity id="o24283" name="lemma" name_original="lemma" src="d0_s11" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s11" value="exceeding" value_original="exceeding" />
        <character modifier="from; approximately" name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1.5-2.7 mm. 2n = 52.</text>
      <biological_entity id="o24284" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24285" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Deschampsia mackenzieana grows on the sandy shores and dunes around Great Slave Lake, Northwest Territories, and Lake Athabasca, Saskatchewan. The decumbent culms of some plants may be a response to shifting substrate.</discussion>
  
</bio:treatment>