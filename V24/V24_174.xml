<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">131</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Swallen) Barkworth" date="unknown" rank="species">diegoense</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species diegoense</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">San diego needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o7233" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 110-140 cm tall, 2.5-4 mm thick, internodes densely and retrorsely pubescent for 3-9 mm below the nodes, particularly the lower nodes, glabrous or retrorsely puberulent elsewhere;</text>
      <biological_entity id="o7234" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="110" from_unit="cm" name="height" src="d0_s1" to="140" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7235" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character constraint="below nodes" constraintid="o7236" is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely; elsewhere" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7236" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity constraint="lower" id="o7237" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes 3, pubescent or glabrate.</text>
      <biological_entity id="o7238" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths mostly glabrous or puberulent, margins ciliate distally;</text>
      <biological_entity constraint="basal" id="o7239" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7240" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous or with hairs, hairs mostly to 0.5 mm, sides with tufts of 1.5-2 mm hairs;</text>
      <biological_entity id="o7241" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o7242" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o7243" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7244" name="side" name_original="sides" src="d0_s4" type="structure" />
      <biological_entity id="o7245" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
      <biological_entity id="o7246" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o7241" id="r1166" name="with" negation="false" src="d0_s4" to="o7242" />
      <relation from="o7244" id="r1167" name="with" negation="false" src="d0_s4" to="o7245" />
      <relation from="o7245" id="r1168" name="part_of" negation="false" src="d0_s4" to="o7246" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.4-2 mm, rounded to acute, abaxial surfaces hairy, hairs to 0.5 mm;</text>
      <biological_entity id="o7247" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7248" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7249" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper ligules 1-3 mm, similar in structure and pubescence;</text>
      <biological_entity constraint="upper" id="o7250" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7251" name="structure" name_original="structure" src="d0_s6" type="structure" />
      <relation from="o7250" id="r1169" name="in" negation="false" src="d0_s6" to="o7251" />
    </statement>
    <statement id="d0_s7">
      <text>blades 1-3.5 mm wide, abaxial surfaces smooth or scabrous, adaxial surfaces prominently ribbed, hairy, hairs 2-3 mm.</text>
      <biological_entity id="o7252" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7253" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7254" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7255" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 21-25 cm long, (2) 4-8 cm wide;</text>
      <biological_entity id="o7256" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="21" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s8" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches strongly divergent to ascending, straight, lower branches 5-7 cm.</text>
      <biological_entity id="o7257" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7258" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets appressed to the branches.</text>
      <biological_entity id="o7259" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character constraint="to branches" constraintid="o7260" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o7260" name="branch" name_original="branches" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal, 8-11.5 mm;</text>
      <biological_entity id="o7261" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="11.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 0.5-1 mm wide, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o7262" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o7263" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets 5.5-7.5 mm long, 0.7-1 mm thick, fusiform, terete;</text>
      <biological_entity id="o7264" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s14" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="thickness" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>calluses 0.25-1.2 mm, acute;</text>
      <biological_entity id="o7265" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas evenly hairy, hairs at midlength and at the apices 0.5-1 mm, apical lobes 0.2-0.4 mm, membranous, flexible;</text>
      <biological_entity id="o7266" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7267" name="hair" name_original="hairs" src="d0_s16" type="structure" />
      <biological_entity id="o7268" name="midlength" name_original="midlength" src="d0_s16" type="structure" />
      <biological_entity id="o7269" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o7270" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="fragility" src="d0_s16" value="pliable" value_original="flexible" />
      </biological_entity>
      <relation from="o7267" id="r1170" name="at" negation="false" src="d0_s16" to="o7268" />
      <relation from="o7267" id="r1171" name="at" negation="false" src="d0_s16" to="o7269" />
    </statement>
    <statement id="d0_s17">
      <text>awns 20-50 mm, persistent, twice-geniculate, all segments scabrous to scabridulous, terminal segment straight;</text>
      <biological_entity id="o7271" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s17" to="50" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character constraint="segment" constraintid="o7272" is_modifier="false" name="size_or_quantity" src="d0_s17" value="2 times-geniculate all segments" value_original="2 times-geniculate all segments" />
        <character constraint="segment" constraintid="o7273" is_modifier="false" name="size_or_quantity" src="d0_s17" value="2 times-geniculate all segments" value_original="2 times-geniculate all segments" />
        <character char_type="range_value" from="scabrous" name="relief" src="d0_s17" to="scabridulous" />
      </biological_entity>
      <biological_entity id="o7272" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity id="o7273" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity constraint="terminal" id="o7274" name="segment" name_original="segment" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>paleas 2.6-3.8 mm, 1/2 - 3/4 as long as the lemmas, pubescent, hairs not extending beyond the apices, veins terminating below the apices, apices rounded;</text>
      <biological_entity id="o7275" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s18" to="3.8" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o7276" from="1/2" name="quantity" src="d0_s18" to="3/4" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7276" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o7277" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o7278" name="apex" name_original="apices" src="d0_s18" type="structure" />
      <biological_entity id="o7279" name="vein" name_original="veins" src="d0_s18" type="structure" />
      <biological_entity id="o7280" name="apex" name_original="apices" src="d0_s18" type="structure" />
      <biological_entity id="o7281" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o7277" id="r1172" name="extending beyond the" negation="false" src="d0_s18" to="o7278" />
      <relation from="o7279" id="r1173" name="terminating below" negation="false" src="d0_s18" to="o7280" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 2.5-4 mm, dehiscent, not penicillate.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = unknown.</text>
      <biological_entity id="o7282" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s19" to="4" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s19" value="penicillate" value_original="penicillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7283" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Achnatherum diegoense grows in chaparral and coastal sage scrub, on rocky soil near streams or the coast, at 0-350 m, on the Channel Islands of Santa Barbara County, California, and, on the mainland, in Ventura and San Diego counties south into Baja California, Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>