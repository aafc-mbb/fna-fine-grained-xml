<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">virginicus</taxon_name>
    <taxon_name authority="(Ramaley) Bush" date="unknown" rank="variety">jejunus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species virginicus;variety jejunus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually glaucous, but not strongly so, becoming yellowish or reddish-brown at maturity.</text>
      <biological_entity id="o21581" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="not strongly; strongly; becoming" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (50) 70-100 (130) cm;</text>
      <biological_entity id="o21582" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually 6-8, often exposed;</text>
      <biological_entity id="o21583" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles and ligules often pronounced.</text>
      <biological_entity id="o21584" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s3" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <biological_entity id="o21585" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s3" value="pronounced" value_original="pronounced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blades 3-15 mm wide, flat, usually scabridulous, rarely pubescent.</text>
      <biological_entity id="o21586" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikes 4-20 cm, exserted;</text>
      <biological_entity id="o21587" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikelets usually glabrous to scabrous, sometimes glaucous;</text>
      <biological_entity id="o21588" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually glabrous" name="pubescence" src="d0_s6" to="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>glumes (0.5) 0.7-1.2 (1.8) mm wide, indurate and bowed out only in the basal 1 mm.</text>
    </statement>
    <statement id="d0_s8">
      <text>Anthesis mid-june to late July.</text>
      <biological_entity id="o21589" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character name="width" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="indurate" value_original="indurate" />
        <character constraint="in the basal 1 mm" is_modifier="false" modifier="only" name="shape" src="d0_s7" value="bowed" value_original="bowed" />
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="anthesis" value_original="anthesis" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus virginicus var. jejunus grows in moist to dry, sometimes alkaline or saline soil, in open, rocky, or alluvial woods, grasslands, glades, and disturbed places. It occupies the western range of the species, except for the Intermountain region. It is uncommon in the northeast, from Virginia to Newfoundland, and rare or absent in the southeast, beyond Texas and Indiana. It intergrades with var. virginicus (with spikes up to 22 cm long in intermediate plants) in the Great Plains, and with E. glabriflorus (p. 296) or E. macgregorii (p. 295) in some southern or eastern regions. Spike exsertion, and thus the ease of distinction from var. virginicus, can be influenced by the environment (Brooks 1974). Some northern populations, from Alberta and North Dakota to Michigan and Quebec, especially on damper soils, have blades that are villous-hirsute and often darker green (e.g., Booher and Tryon 1948). Blades are often only 4—8 mm wide in the Great Plains, and some exceptionally small northern plants (E. virginicus var. micromeris Schmoll) have blades (1)2-4(5.5) mm wide, spikes 3.2-7 cm long, glume awns 1-1.5 mm long, and lemma awns 3.5-13 mm long, suggesting a transition to E. curvatus (see discussion under the next species).</discussion>
  
</bio:treatment>