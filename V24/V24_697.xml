<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephan L. Hatch;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Host" date="unknown" rank="genus">BECKMANNIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus beckmannia</taxon_hierarchy>
  </taxon_identification>
  <number>14.12</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual and tufted, or perennial and rhizomatous.</text>
      <biological_entity id="o30359" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-150 cm, sometimes tuberous at the base, erect.</text>
      <biological_entity id="o30360" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character constraint="at base" constraintid="o30361" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o30361" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o30362" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o30363" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths open, glabrous, ribbed;</text>
      <biological_entity id="o30364" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o30365" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, acute;</text>
      <biological_entity id="o30366" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat, glabrous.</text>
      <biological_entity id="o30367" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences dense, spikelike panicles;</text>
      <biological_entity id="o30368" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o30369" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 1-sided, racemosely arranged, secondary branches few, at least some branches longer than 1 cm, with closely imbricate spikelets;</text>
      <biological_entity id="o30370" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="racemosely" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o30371" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o30373" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="closely" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <relation from="o30372" id="r4782" name="with" negation="false" src="d0_s8" to="o30373" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation below the glumes, the spikelets falling entire.</text>
      <biological_entity id="o30372" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o30374" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <biological_entity id="o30375" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o30372" id="r4783" name="below" negation="false" src="d0_s9" to="o30374" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets laterally compressed, circular, ovate or obovate in side view, subsessile, with 1-2 florets;</text>
      <biological_entity id="o30376" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="circular" value_original="circular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o30377" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <relation from="o30376" id="r4784" name="with" negation="false" src="d0_s10" to="o30377" />
    </statement>
    <statement id="d0_s11">
      <text>rachillas not prolonged beyond the base of the distal floret.</text>
      <biological_entity id="o30378" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character constraint="beyond base" constraintid="o30379" is_modifier="false" modifier="not" name="length" src="d0_s11" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o30379" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o30380" name="floret" name_original="floret" src="d0_s11" type="structure" />
      <relation from="o30379" id="r4785" name="part_of" negation="false" src="d0_s11" to="o30380" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, slightly shorter than the lemmas, inflated, keeled, D-shaped in side view, unawned;</text>
      <biological_entity id="o30381" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character constraint="than the lemmas" constraintid="o30382" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s12" value="d--shaped" value_original="d--shaped" />
      </biological_entity>
      <biological_entity id="o30382" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>calluses blunt, glabrous;</text>
      <biological_entity id="o30383" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas lanceolate, inconspicuously 5-veined, unawned;</text>
      <biological_entity id="o30384" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas subequal to the lemmas;</text>
      <biological_entity id="o30385" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="to lemmas" constraintid="o30386" is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o30386" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2, free;</text>
      <biological_entity id="o30387" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3;</text>
      <biological_entity id="o30388" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovaries glabrous.</text>
      <biological_entity id="o30389" name="ovary" name_original="ovaries" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses shorter than the lemmas, concealed at maturity, x = 7.</text>
      <biological_entity id="o30390" name="caryopse" name_original="caryopses" src="d0_s19" type="structure">
        <character constraint="than the lemmas" constraintid="o30391" is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="shorter" value_original="shorter" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s19" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o30391" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
      <biological_entity constraint="x" id="o30392" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Beckmannia is a genus of two species: an annual species usually with one fertile floret per spikelet that is native to North America and Asia, and a perennial species with two fertile florets per spikelet that is restricted to Eurasia.</discussion>
  <references>
    <reference>Reeder, J.R. 1953. Affinities of the grass genus Beckmannia Host. Bull. Torrey Bot. Club 80:187-196.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Wis.;Wyo.;N.Mex.;N.Y.;Pa.;Calif.;Nev.;Colo.;Alaska;Ill.;Iowa;Ariz.;Idaho;N.Dak.;Nebr.;S.Dak.;Maine;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Mont.;Alta.;B.C.;Greenland;Nfld. and Labr. (Nfld.);Man.;N.B.;N.S.;N.W.T.;Nunavut;Ont.;P.E.I.;Que.;Sask.;Yukon;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>