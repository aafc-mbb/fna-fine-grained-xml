<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">696</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="genus">LACHNAGROSTIS</taxon_name>
    <taxon_name authority="(G. Forst.) Trin." date="unknown" rank="species">filiformis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus lachnagrostis;species filiformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">retrofracta</taxon_name>
    <taxon_hierarchy>genus agrostis;species retrofracta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">avenacea</taxon_name>
    <taxon_hierarchy>genus agrostis;species avenacea</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Pacific bent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o4165" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-65 cm, erect, sometimes geniculate basally.</text>
      <biological_entity id="o4166" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes; basally" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous, sometimes scabridulous;</text>
      <biological_entity id="o4167" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 2.2-7.8 mm, obtuse or acute, lacerate;</text>
      <biological_entity id="o4168" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s4" to="7.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 8-20 cm long, 1-3 mm wide, usually flat, sometimes involute, finely scabrous.</text>
      <biological_entity id="o4169" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 7-30 cm long, (2) 5-25 cm wide, broadly ovate, loose and diffuse at maturity, bases sometimes not exserted at maturity;</text>
      <biological_entity id="o4170" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character constraint="at maturity" is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity id="o4171" name="base" name_original="bases" src="d0_s6" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="sometimes not" name="position" src="d0_s6" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches capillary, scabrous, often deflexed at maturity, branched above the middle, spikelets confined to the distal 1/3;</text>
      <biological_entity id="o4172" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character constraint="at maturity" is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="deflexed" value_original="deflexed" />
        <character constraint="above middle branches" constraintid="o4173" is_modifier="false" name="architecture" src="d0_s7" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="middle" id="o4173" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o4174" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s7" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower branches 5-15 cm;</text>
      <biological_entity constraint="lower" id="o4175" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.5-7 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes and below the panicles.</text>
      <biological_entity id="o4176" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4177" name="panicle" name_original="panicles" src="d0_s10" type="structure" />
      <relation from="o4176" id="r680" name="above the glumes and below" negation="false" src="d0_s10" to="o4177" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets greenish to yellowish, often tinged with purple;</text>
      <biological_entity id="o4178" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s11" to="yellowish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="tinged with purple" value_original="tinged with purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla prolongations about 1 mm, bristlelike, pilose distally, hairs about 0.6 mm.</text>
      <biological_entity id="o4179" name="rachillum" name_original="rachilla" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o4180" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes equal to subequal, 2.8-3.6 mm, scabrous on the midvein, at least distally, 1 (3) -veined, apices long-acuminate;</text>
      <biological_entity id="o4181" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3.6" to_unit="mm" />
        <character constraint="on midvein" constraintid="o4182" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture" notes="" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
      <biological_entity id="o4182" name="midvein" name_original="midvein" src="d0_s13" type="structure" />
      <biological_entity id="o4183" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>callus hairs to 0.5 mm, abundant;</text>
      <biological_entity constraint="callus" id="o4184" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="quantity" src="d0_s14" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.8-2.3 mm, translucent to opaque, usually densely hairy at least on the lower 1/2, hairs to 0.7 mm, 5-veined, apices truncate to obtuse, erose or 2-4-toothed, awned from the middle 1/3 of the lemmas, awns 4-7.5 mm, geniculate;</text>
      <biological_entity id="o4185" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="translucent" name="coloration" src="d0_s15" to="opaque" />
        <character constraint="on lower 1/2" constraintid="o4186" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4186" name="1/2" name_original="1/2" src="d0_s15" type="structure" />
      <biological_entity id="o4187" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o4188" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s15" to="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s15" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-4-toothed" value_original="2-4-toothed" />
        <character constraint="from middle" constraintid="o4189" is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o4189" name="middle" name_original="middle" src="d0_s15" type="structure">
        <character constraint="of lemmas" constraintid="o4190" name="quantity" src="d0_s15" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o4190" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o4191" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas (0.4) 1.2-1.5 mm, usually more than 2/3 the length of the lemmas, thin, veins usually not visible;</text>
      <biological_entity id="o4192" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character modifier="usually" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character modifier="usually" name="quantity" src="d0_s16" value="/3" value_original="/3" />
        <character is_modifier="false" name="length" notes="" src="d0_s16" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o4193" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o4194" name="vein" name_original="veins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s16" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, about 0.5 mm.</text>
      <biological_entity id="o4195" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses about 1.2 mm;</text>
      <biological_entity id="o4196" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character name="some_measurement" src="d0_s18" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>endosperm solid.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 56.</text>
      <biological_entity id="o4197" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4198" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lachnagrostis filiformis is native to New Guinea, Australia, New Zealand, and Easter Island. In North America, it grows in open, disturbed sites such as roadsides and burned areas, and has been spreading into vernal pools around San Diego, California. It was introduced to North America in the late nineteenth century, but is only known to be established in California. The most recent record located for Texas was collected in 1902. Records from South Carolina are from "waste areas around wool-combing mill; rare, perhaps only a waif." (Weakley, http: //www. herbarium.unc/ edu/flora.htm).</discussion>
  
</bio:treatment>