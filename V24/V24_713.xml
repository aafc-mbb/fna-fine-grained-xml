<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="V.L. Marsh ex Soreng" date="unknown" rank="section">Sylvestres</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">sylvestris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section sylvestres;species sylvestris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Woodland bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous, not stoloniferous, loosely tufted, sometimes appearing shortly rhizomatous, loosely to densely tufted.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly pseudointravaginal.</text>
      <biological_entity id="o12077" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="sometimes; shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="loosely to densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="pseudointravaginal" value_original="pseudointravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 30-120 cm, bases often decumbent.</text>
      <biological_entity id="o12078" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s3" to="120" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12079" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for (1/20) 1/2-7/8 their length, terete, throats frequently ciliate near the point of fusion;</text>
      <biological_entity id="o12080" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o12081" name="throat" name_original="throats" src="d0_s4" type="structure">
        <character constraint="near point" constraintid="o12082" is_modifier="false" modifier="frequently" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12082" name="point" name_original="point" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-2.7 mm, smooth or sparsely scabrous, truncate to obtuse;</text>
      <biological_entity id="o12083" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 0.7-5 mm wide, flat, thin, lax.</text>
      <biological_entity id="o12084" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (6.7) 9-20 cm, open, narrowly conical at maturity;</text>
      <biological_entity id="o12085" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="6.7" value_original="6.7" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character constraint="at maturity" is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="conical" value_original="conical" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nodes with (2) 3-10 branches per node;</text>
      <biological_entity id="o12086" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o12087" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o12088" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o12086" id="r1915" name="with" negation="false" src="d0_s8" to="o12087" />
      <relation from="o12087" id="r1916" name="per" negation="false" src="d0_s8" to="o12088" />
    </statement>
    <statement id="d0_s9">
      <text>branches (2) 3-7 cm, spreading to eventually reflexed, straight, angled, angles several, densely scabrous, with 1-11 spikelets.</text>
      <biological_entity id="o12089" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s9" to="eventually reflexed" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o12090" name="angle" name_original="angles" src="d0_s9" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s9" value="several" value_original="several" />
        <character is_modifier="false" modifier="densely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12091" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="11" />
      </biological_entity>
      <relation from="o12090" id="r1917" name="with" negation="false" src="d0_s9" to="o12091" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.5-4.4 mm, laterally compressed;</text>
      <biological_entity id="o12092" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.4" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2-3 (4);</text>
      <biological_entity id="o12093" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character name="atypical_quantity" src="d0_s11" value="4" value_original="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes longer than (1) 1.2 mm, smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o12094" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character modifier="longer than" name="atypical_some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character modifier="longer than" name="some_measurement" src="d0_s12" unit="mm" value="" value_original="" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes distinctly keeled, keels scabrous;</text>
      <biological_entity id="o12095" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o12096" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o12097" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o12098" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o12099" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o12099" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses webbed;</text>
      <biological_entity id="o12100" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 2.1-3.1 mm, broadly lanceolate, distinctly keeled, keels and marginal veins short-villous, extending to near the apices on the keels, lateral-veins prominent, softly puberulent to short-villous, intercostal regions usually sparsely softly puberulent, smooth, apices obtuse to acute;</text>
      <biological_entity id="o12101" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s17" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o12102" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o12103" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o12104" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <biological_entity id="o12105" name="keel" name_original="keels" src="d0_s17" type="structure" />
      <biological_entity id="o12106" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="softly puberulent" name="pubescence" src="d0_s17" to="short-villous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o12107" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually sparsely softly" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12108" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s17" to="acute" />
      </biological_entity>
      <relation from="o12102" id="r1918" name="near" negation="false" src="d0_s17" to="o12104" />
      <relation from="o12103" id="r1919" name="near" negation="false" src="d0_s17" to="o12104" />
      <relation from="o12104" id="r1920" name="on" negation="false" src="d0_s17" to="o12105" />
    </statement>
    <statement id="d0_s18">
      <text>palea keels softly puberulent at midlength, apices finely scabrous;</text>
      <biological_entity constraint="palea" id="o12109" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character constraint="at midlength" constraintid="o12110" is_modifier="false" modifier="softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o12110" name="midlength" name_original="midlength" src="d0_s18" type="structure" />
      <biological_entity id="o12111" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 1-1.8 mm. 2n = 28.</text>
      <biological_entity id="o12112" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12113" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa sylvestris grows in southeastern Canada and throughout much of the eastern United States, mainly at low elevations in woodlands, especially in riparian zones. It is easily distinguished from P. wolfii (p. 514) by its smaller, more numerous spikelets and lemmas that are usually sparsely hairy between the veins. Plants from the middle Appalachian Mountains have been confused with P. paludigena (p. 572); P. sylvestris is usually larger, has more than 2 branches per panicle node, is pubescent between the lemma veins and palea keels, and has larger anthers.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;D.C.;Wis.;W.Va.;Fla.;N.J.;Tex.;La.;Tenn.;N.Y.;Pa.;Kans.;Nebr.;Okla.;S.Dak.;Va.;Ont.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Md.;Ohio;Mo.;Minn.;Mich.;Miss.;Ky.;N.C.;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>