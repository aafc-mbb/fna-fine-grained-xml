<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephen J. Darbyshire;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SCHEDONORUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus schedonorus</taxon_hierarchy>
  </taxon_identification>
  <number>14.03</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o31091" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 2 m, slender to stout, erect to decumbent.</text>
      <biological_entity id="o31092" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="2" to_unit="m" />
        <character char_type="range_value" from="slender" name="size" src="d0_s2" to="stout" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open, rounded, smooth or scabrous;</text>
      <biological_entity id="o31093" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles present, usually falcate and clasping, sometimes an undulating flange;</text>
      <biological_entity id="o31094" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o31095" name="flange" name_original="flange" src="d0_s4" type="structure" />
      <relation from="o31094" id="r4891" modifier="sometimes" name="undulating" negation="false" src="d0_s4" to="o31095" />
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, glabrous;</text>
      <biological_entity id="o31096" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat, linear.</text>
      <biological_entity id="o31097" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal panicles, erect, not spikelike;</text>
      <biological_entity id="o31098" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o31099" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>branches glabrous, smooth or scabrous, most branches longer than 1 cm;</text>
      <biological_entity id="o31100" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31101" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels sometimes longer than 3 mm, thinner than 1 mm.</text>
      <biological_entity id="o31102" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character modifier="sometimes longer than" name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="width" src="d0_s9" value="thinner" value_original="thinner" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets pedicellate, laterally compressed, with 2-22 florets;</text>
      <biological_entity id="o31104" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="22" />
      </biological_entity>
      <relation from="o31103" id="r4892" name="with" negation="false" src="d0_s10" to="o31104" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes and between the florets.</text>
      <biological_entity id="o31103" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o31105" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o31103" id="r4893" name="above the glumes and between" negation="false" src="d0_s11" to="o31105" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes 2, shorter than the adjacent lemmas, more or less equally wide, lanceolate to oblong, rounded on the back, membranous, 3-9-veined, apices acute, unawned;</text>
      <biological_entity id="o31106" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character constraint="than the adjacent lemmas" constraintid="o31107" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="more or less equally" name="width" src="d0_s12" value="wide" value_original="wide" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="oblong" />
        <character constraint="on back" constraintid="o31108" is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" notes="" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-9-veined" value_original="3-9-veined" />
      </biological_entity>
      <biological_entity id="o31107" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o31108" name="back" name_original="back" src="d0_s12" type="structure" />
      <biological_entity id="o31109" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses glabrous or sparsely hairy;</text>
      <biological_entity id="o31110" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas lanceolate, ovate or oblong, rounded on the back, membranous, chartaceous, 3-7-veined, apices acute, sometimes hyaline, unawned or awned, awns to 18 mm, terminal or subterminal, straight;</text>
      <biological_entity id="o31111" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character constraint="on back" constraintid="o31112" is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" notes="" src="d0_s14" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s14" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
      <biological_entity id="o31112" name="back" name_original="back" src="d0_s14" type="structure" />
      <biological_entity id="o31113" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o31114" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s14" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas narrower than the lemmas, membranous, usually smooth, keels ciliolate, veins terminating at or beyond midlength;</text>
      <biological_entity id="o31115" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="than the lemmas" constraintid="o31116" is_modifier="false" name="width" src="d0_s15" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o31116" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o31117" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o31118" name="vein" name_original="veins" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2, lanceolate to ovate;</text>
      <biological_entity id="o31119" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s16" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3;</text>
      <biological_entity id="o31120" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovaries glabrous.</text>
      <biological_entity id="o31121" name="ovary" name_original="ovaries" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses shorter than the lemmas, concealed at maturity, dorsally compressed, oblong, broadly elliptic, or ovate, longitudinally sulcate, adherent to the paleas;</text>
      <biological_entity id="o31122" name="caryopse" name_original="caryopses" src="d0_s19" type="structure">
        <character constraint="than the lemmas" constraintid="o31123" is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="shorter" value_original="shorter" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s19" value="concealed" value_original="concealed" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s19" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s19" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="longitudinally" name="architecture" src="d0_s19" value="sulcate" value_original="sulcate" />
        <character constraint="to paleas" constraintid="o31124" is_modifier="false" name="fusion" src="d0_s19" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o31123" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
      <biological_entity id="o31124" name="palea" name_original="paleas" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>hila linear;</text>
      <biological_entity id="o31125" name="hilum" name_original="hila" src="d0_s20" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s20" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryos 1/5 – 1/3 as long as the caryopses.</text>
      <biological_entity id="o31127" name="caryopse" name_original="caryopses" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>x = 7.</text>
      <biological_entity id="o31126" name="embryo" name_original="embryos" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o31127" from="1/5" name="quantity" src="d0_s21" to="1/3" />
      </biological_entity>
      <biological_entity constraint="x" id="o31128" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Three species of the Eurasian genus Schedonorus are established in North America, having been widely introduced as forage and ornamental grasses.</discussion>
  <discussion>Schedonorus has traditionally been included in Festuca, despite all the evidence pointing to its close relationship to Lolium. This evidence includes morphological features, such as the falcate leaf auricles, flat, relatively wide leaf blades, glabrous ovaries, subterminal stylar attachment, and adhesion of the mature caryopses to the paleas, none of which are found in Festuca sensu stricto. Fertile, natural hybrids between species of Schedonorus and those of Lolium are common in Europe, and several artificial hybrids have been registered for commercial use, primarily as forage grasses. Schedonorus and Lolium could appropriately be treated as congeneric subgenera (e.g., Darbyshire 1993). The two are treated as separate genera here for consistency with the treatments by Soreng and Terrell (1997), Holub (1998), and Edgar and Connor (2000).</discussion>
  <references>
    <reference>Aiken, S.G., M.J. Dallwitz, C.L. Mcjannet, and L.L. Consaul. 1997. Biodiversity among Festuca (Poaceae) in North America: Diagnostic evidence from DELTA and clustering programs, and an INTKEY package for interactive, illustrated identification and information retrieval. Canad. J. Bot. 75:1527-1555</reference>
    <reference>Charmet, G., C. Ravel, and F. Balfourier. 1997. Phylogenetic analysis in the Festuca-Lolium complex using molecular markers and ITS rDNA. Theor. Appl. Genet. 94:1038-1046</reference>
    <reference>Darbyshire, S.J. 1993. Realignment of Festuca subgenus Schedonorus with the genus Lolium. Novon 3:239-243</reference>
    <reference>Dube, M. 1983. Addition de Festuca gigantea (L.) Vill. (Poaceae) a la flore du Canada. Naturaliste Canad. 110:213-215</reference>
    <reference>Edgar, E. and H.E. Connor. 2000. Flora of New Zealand, vol. 5. Manaaki Whenua Press, Lincoln, New Zealand. 650 pp.</reference>
    <reference>Holub, J. 1998. Reclassifications and new names in vascular plants 1. Preslia 70:97-122</reference>
    <reference>Jauhar, P.P. 1993. Cytogenetics of the Festuca-Lolium Complex. Monographs on Theoretical and Applied Genetics No. 18. Springer-Verlag, Berlin, Germany. 255 pp.</reference>
    <reference>Kiang, A.-S., V. Connolly, D.J. McConnell, and T.A. Kavanagh. 1994. Paternal inheritance of mitochondria and chloroplasts in Festuca pratensis-Lolium perenne intergeneric hybrids. Theor. Appl. Genet. 87:681-688</reference>
    <reference>Nihsen, M.E., E.L. Piper, C.P. West, R.J. Crawford, Jr., T.M. Denard, Z.B. Johnson, C.A. Roberts, D.A. Spiers, and C.E Rosenkrans, Jr. 2004. Growth rate and physiology of steers grazing tall fescue inoculated with novel endophytes. J. Animal Sci. 82:878-883</reference>
    <reference>Soreng, R.J. and E.E. Terrell. 1997 [publication date 1998]. Taxonomic notes on Schedonorus, a segregate genus from Festuca or Lolium, with a new nothogenus, xSchedololium, and new combinations. Phytologia 83:85-88</reference>
    <reference>Soreng, R.J., E.E. Terrell, J. Wiersema, and S.J. Darbyshire. 2001. Proposal to conserve the name Schedonorus arundinaceus (Schreb.) Dumort. against Schedonorus arundinaceus Roem. &amp; Schult. (Poaceae: Poeae). Taxon 50:915-917.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C;Wis.;W.Va.;Pacific Islands (Hawaii);Fla.;Okla.;Md.;Wyo.;N.H.;Tenn.;N.Mex.;Tex.;La.;N.C.;Ark.;Kans.;N.Dak.;Nebr.;S.Dak.;S.C.;Pa.;Calif.;Nev.;Mass.;Maine;R.I.;Vt.;Va.;Colo.;Alaska;Ala.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Mont.;Oreg.;Ohio;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;Ont.;P.E.I.;Que.;Sask.;Yukon;Mo.;Minn.;Mich.;Miss.;Utah;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemma awns 10-18 mm long, longer than the lemmas</description>
      <determination>2 Schedonorus giganteus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas unawned or the awns shorter than 4 mm, shorter than the lemmas.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Auricles glabrous; panicle branches at the lowest node 1 or 2, if paired the shorter with 1-2(3) spikelets, the longer with 2-6(9) spikelets; lemmas usually smooth, sometimes slightly scabrous distally, unawned or with a mucro to 0.2 mm long</description>
      <determination>1 Schedonorus pratensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Auricles ciliate, having at least 1 or 2 hairs along the margins (check several leaves); panicle branches at the lowest node usually paired, the shorter with 1-13 spikelets, the longer with 3-19 spikelets; lemmas usually scabrous or hispidulous, at least distally, rarely smooth, unawned or with an awn up to 4 mm long</description>
      <determination>3 Schedonorus arundinaceus</determination>
    </key_statement>
  </key>
</bio:treatment>