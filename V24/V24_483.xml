<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="(Scribn. &amp; Merr.) Á. Löve" date="unknown" rank="species">×yukonensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species ×yukonensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="Scribn. &amp; Merr." date="unknown" rank="species">yukonense</taxon_name>
    <taxon_hierarchy>genus Agropyron;species yukonense</taxon_hierarchy>
  </taxon_identification>
  <number>43</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous.</text>
      <biological_entity id="o28478" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms about 60 cm, erect.</text>
      <biological_entity id="o28479" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="cm" value="60" value_original="60" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves somewhat basally concentrated;</text>
      <biological_entity id="o28480" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat basally" name="arrangement_or_density" src="d0_s2" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 3.5-6 mm wide.</text>
      <biological_entity id="o28481" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 6-10 cm long, 0.7-1.7 cm wide, with 1 spikelet per node;</text>
      <biological_entity id="o28482" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s4" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28483" name="spikelet" name_original="spikelet" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o28484" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o28482" id="r4479" name="with" negation="false" src="d0_s4" to="o28483" />
      <relation from="o28483" id="r4480" name="per" negation="false" src="d0_s4" to="o28484" />
    </statement>
    <statement id="d0_s5">
      <text>internodes 6-11 mm.</text>
      <biological_entity id="o28485" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Glumes 4-5.5 mm, about 1/2 the length of the adjacent lemmas, lanceolate, flat, hairy, apices acute or awn-tipped, awns shorter than 1 mm;</text>
      <biological_entity id="o28486" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
        <character name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o28487" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="length" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28488" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="length" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28489" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>rachillas densely hairy;</text>
      <biological_entity id="o28490" name="rachilla" name_original="rachillas" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 6-9 mm, densely villous, unawned;</text>
      <biological_entity id="o28491" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 3.3-3.6 mm.</text>
      <biological_entity id="o28492" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The parents of Elymus ×yukonensis have not been identified. Morphological and geographic considerations suggest that they may be E. lanceolatus subsp. psammophilus (p. 327) and E. alaskanus (p. 326).</discussion>
  <discussion>Elymus ×yukonensis is an Elymus named hybrid</discussion>
  <discussion>Elymus is notorious for its ability to hybridize. Most of its interspecific hybrids are partially fertile, permitting introgression between the parents. The descriptions provided below are restricted to the named interspecific hybrids. They should be treated with caution and some skepticism; some are based solely on the type specimen, because little other reliably identified material was available. Moreover, as the descriptions of the non-hybrid species indicate, many other interspecific hybrids exist.</discussion>
  <discussion>The parentage of all hybrids is best determined in the field. Perennial hybrids, such as those in Elymus, can persist in an area after one or both parents have died out, but the simplest assumption is that both are present. Interspecific hybrids of Elymus that have disarticulating rachises presumably have E. elymoides or E. multisetus as one of their parents.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>