<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">767</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PHALARIS</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">aquatica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus phalaris;species aquatica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phalaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tuberosa</taxon_name>
    <taxon_hierarchy>genus phalaris;species tuberosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phalaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stenoptera</taxon_name>
    <taxon_hierarchy>genus phalaris;species stenoptera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phalaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tuberosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">stenoptera</taxon_name>
    <taxon_hierarchy>genus phalaris;species tuberosa;variety stenoptera</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Bulbous canarygrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, shortly rhizomatous.</text>
      <biological_entity id="o27471" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-200 cm, often swollen at the base, rooting at the lower nodes.</text>
      <biological_entity id="o27472" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="200" to_unit="cm" />
        <character constraint="at base" constraintid="o27473" is_modifier="false" modifier="often" name="shape" src="d0_s2" value="swollen" value_original="swollen" />
        <character constraint="at lower nodes" constraintid="o27474" is_modifier="false" name="architecture" notes="" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o27473" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="lower" id="o27474" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Ligules 2-12 mm, truncate, lacerate;</text>
      <biological_entity id="o27475" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 5-15 (20) cm long, 0.5-10 mm wide.</text>
      <biological_entity id="o27476" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 1.5-15 cm long, 1-2.5 cm wide, usually cylindric, sometimes ovoid, occasionally lobed at the base, spikelets borne singly, not clustered;</text>
      <biological_entity id="o27477" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character constraint="at base" constraintid="o27478" is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o27478" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o27479" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches not evident.</text>
      <biological_entity id="o27480" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets homogamous, with 2-3 (5) florets, usually with 1 bisexual floret, occasionally with 2, occasionally the terminal floret viviparous;</text>
      <biological_entity id="o27481" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <biological_entity id="o27482" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o27483" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o27481" id="r4337" name="with" negation="false" src="d0_s7" to="o27482" />
      <relation from="o27481" id="r4338" modifier="usually" name="with" negation="false" src="d0_s7" to="o27483" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation above the glumes, beneath the sterile florets.</text>
      <biological_entity constraint="terminal" id="o27484" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s7" value="viviparous" value_original="viviparous" />
      </biological_entity>
      <biological_entity id="o27485" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <biological_entity id="o27486" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o27484" id="r4339" name="above" negation="false" src="d0_s8" to="o27485" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 4.4-7.5 mm long, 1.2-1.5 mm wide, keels winged distally, wings 0.2-0.4 mm wide, usually entire, lateral-veins conspicuous, smooth;</text>
      <biological_entity id="o27487" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="length" src="d0_s9" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27488" name="keel" name_original="keels" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o27489" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27490" name="lateral-vein" name_original="lateral-veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sterile florets usually 1, hairy, if 2, lowest floret to 0.7 mm, upper or only sterile floret 1-3 mm;</text>
      <biological_entity id="o27491" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o27492" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o27493" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o27494" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="only" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bisexual florets 3.1-4.6 mm long, 1.2-1.5 mm wide, hairy, stramineous, acute;</text>
      <biological_entity id="o27495" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="length" src="d0_s11" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3-3.6 mm. 2n = 28.</text>
      <biological_entity id="o27496" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27497" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>A native of the Mediterranean region, Phalaris aquatica now grows in many parts of the world, frequently having been introduced because of its forage value. Even where it is native, it usually grows in disturbed areas, often those subject to seasonal flooding. It is now established in western North America, being most common along the coast, and as an invasive in disturbed wet prairies with clay soils.</discussion>
  <discussion>Phalaris aquatica can hybridize with other species of Phalaris. The stabilized polyploid hybrid with P. minor, P. xdaviesii S.T. Blake, is cultivated as a forage grass in Australia, Africa, and South America. The hybrid with P. arundinacea, P. xmonspeliensis Daveau, is also a good forage grass. The name 'Toowoomba Canary-grass' has been applied to P. xmonspeliensis in North America, but Ross (1989) stated that it should be applied to P. aquatica. Using 'Bulbous Canarygrass' as the English-language name for P. aquatica avoids confusion, at least in the Flora region. In addition, it is descriptive, and is the name used by the U.S. Department of Agriculture for P. aquatica.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.;D.C.;Miss.;Va.;Calif.;N.C.;Mont.;Pacific Islands (Hawaii);S.C.;Ariz.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>