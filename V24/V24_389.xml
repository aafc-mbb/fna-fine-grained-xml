<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché, Hana Pazdírková, and Christine Roberts</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">TRITICUM</taxon_name>
    <taxon_name authority="Schrank ex Schiibl." date="unknown" rank="species">dicoccum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus triticum;species dicoccum</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Emmer</other_name>
  <other_name type="common_name">Farro</other_name>
  <other_name type="common_name">Far</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 80-150 cm, decumbent;</text>
      <biological_entity id="o15205" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodes glabrous or pubescent;</text>
      <biological_entity id="o15206" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes mostly hollow, solid for 1 cm below the spikes.</text>
      <biological_entity id="o15207" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character constraint="below spikes" constraintid="o15208" is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity id="o15208" name="spike" name_original="spikes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Blades to 20 mm wide, pubescent.</text>
      <biological_entity id="o15209" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 5-10 cm, about as wide as thick to wider than thick, cylindrical to strongly flattened;</text>
      <biological_entity id="o15210" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick to wider than thick" />
        <character char_type="range_value" from="cylindrical" name="shape" src="d0_s4" to="strongly flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachises glabrous or shortly ciliate at the nodes and margins;</text>
      <biological_entity id="o15211" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at margins" constraintid="o15213" is_modifier="false" modifier="shortly" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15212" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o15213" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>internodes (0.5) 2-5 mm, disarticulating with pressure, dispersal units wedge-shaped.</text>
      <biological_entity id="o15214" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character constraint="with pressure" constraintid="o15215" is_modifier="false" name="architecture" src="d0_s6" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o15215" name="pressure" name_original="pressure" src="d0_s6" type="structure" />
      <biological_entity id="o15216" name="unit" name_original="units" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="wedge--shaped" value_original="wedge--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 10-16 mm, elliptical to ovate, with 3-4 florets, usually only the lower 2 seed-forming.</text>
      <biological_entity id="o15217" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="elliptical" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
      <biological_entity id="o15218" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15219" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o15217" id="r2425" name="with" negation="false" src="d0_s7" to="o15218" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 6-10 mm, coriaceous, tightly appressed to the lower florets, with 1 prominent keel, keel winged only in the distal 2/3 terminating in a tooth;</text>
      <biological_entity id="o15220" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
        <character constraint="to lower florets" constraintid="o15221" is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15221" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o15222" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o15223" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character constraint="in terminating" constraintid="o15224" is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o15224" name="terminating" name_original="terminating" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o15225" name="tooth" name_original="tooth" src="d0_s8" type="structure" />
      <relation from="o15220" id="r2426" name="with" negation="false" src="d0_s8" to="o15222" />
      <relation from="o15224" id="r2427" name="in" negation="false" src="d0_s8" to="o15225" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 9-12 mm, awned, lower 2 lemmas awned, awns to 17 cm, upper lemmas unawned or shortly awned;</text>
      <biological_entity id="o15226" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15227" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15228" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15229" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="17" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o15230" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleas not splitting at maturity.</text>
      <biological_entity id="o15231" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Endosperm flinty.</text>
    </statement>
    <statement id="d0_s12">
      <text>Haplomes AUB.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 28.</text>
      <biological_entity id="o15232" name="endosperm" name_original="endosperm" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o15233" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Triticum dicoccum is the domesticated derivative of T. dicoccoides. It was once grown fairly extensively in central and southern Europe, southern Russia, northern Africa, and Arabia, because it can withstand poor, waterlogged soils. It is rarely grown now. It was introduced to the Flora region as a feed grain and forage for livestock. Currently, its primary use in the region is for plant breeding; it is also sold for human consumption as farro in specialty food markets.</discussion>
  
</bio:treatment>