<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="Krivot." date="unknown" rank="section">Breviaristatae</taxon_name>
    <taxon_name authority="Swallen" date="unknown" rank="species">ligulata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section breviaristatae;species ligulata</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Guadalupe fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely to densely cespitose, with short rhizomes.</text>
      <biological_entity id="o25340" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely to densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o25341" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o25340" id="r4011" name="with" negation="false" src="d0_s0" to="o25341" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 45-80 cm, erect or the bases decumbent, scabrous near the inflorescence.</text>
      <biological_entity id="o25342" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o25343" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="near inflorescence" constraintid="o25344" is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25344" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths closed for less than 1/3 their length, glabrous or finely scabrous;</text>
      <biological_entity id="o25345" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>collars glabrous;</text>
      <biological_entity id="o25346" name="collar" name_original="collars" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 3-5 (8) mm;</text>
      <biological_entity id="o25347" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-3 mm wide when flat, 0.6-1.2 mm in diameter when conduplicate, persistent, abaxial surfaces glabrous, smooth to sparsely scabrous, adaxial surfaces scabrous, veins (5) 7-9, ribs 5-9;</text>
      <biological_entity id="o25348" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" modifier="when flat" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" modifier="when conduplicate" name="diameter" src="d0_s5" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25349" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="smooth to sparsely" value_original="smooth to sparsely" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25350" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25351" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o25352" name="rib" name_original="ribs" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial sclerenchyma in strands opposite the veins, rarely a discontinuous band;</text>
      <biological_entity id="o25354" name="strand" name_original="strands" src="d0_s6" type="structure" />
      <biological_entity id="o25355" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o25356" name="band" name_original="band" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="discontinuous" value_original="discontinuous" />
      </biological_entity>
      <relation from="o25353" id="r4012" name="in" negation="false" src="d0_s6" to="o25354" />
      <relation from="o25354" id="r4013" name="opposite" negation="false" src="d0_s6" to="o25355" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial sclerenchyma sometimes present;</text>
    </statement>
    <statement id="d0_s8">
      <text>girders sometimes present at the major veins;</text>
      <biological_entity id="o25357" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pillars usually present if the girders not developed.</text>
      <biological_entity constraint="abaxial" id="o25353" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character constraint="at veins" constraintid="o25357" is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not" name="development" src="d0_s9" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences 6-10 (16) cm, contracted or loosely open, with 1-2 (3) branches per node;</text>
      <biological_entity id="o25358" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s10" to="10" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s10" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o25359" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o25360" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o25358" id="r4014" name="with" negation="false" src="d0_s10" to="o25359" />
      <relation from="o25359" id="r4015" name="per" negation="false" src="d0_s10" to="o25360" />
    </statement>
    <statement id="d0_s11">
      <text>branches erect or spreading, lower branches sometimes reflexed, spikelets borne towards the ends of the branches.</text>
      <biological_entity id="o25361" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25362" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o25363" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o25364" name="end" name_original="ends" src="d0_s11" type="structure" />
      <biological_entity id="o25365" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <relation from="o25363" id="r4016" name="borne towards the" negation="false" src="d0_s11" to="o25364" />
      <relation from="o25363" id="r4017" name="borne towards the" negation="false" src="d0_s11" to="o25365" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 6-8.5 mm, with 2-3 (4) florets.</text>
      <biological_entity id="o25366" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25367" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="4" value_original="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
      <relation from="o25366" id="r4018" name="with" negation="false" src="d0_s12" to="o25367" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes scabrous, acute;</text>
      <biological_entity id="o25368" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 3-4 (5.5) mm;</text>
      <biological_entity constraint="lower" id="o25369" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="5.5" value_original="5.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 3.5-5.5 (6.5) mm;</text>
      <biological_entity constraint="upper" id="o25370" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="6.5" value_original="6.5" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 4-6.5 mm, ovatelanceolate, glabrous, smooth or sparsely scabrous towards the apices, unawned;</text>
      <biological_entity id="o25371" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character constraint="towards apices" constraintid="o25372" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25372" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>paleas as long as to slightly longer than the lemmas, intercostal region puberulent distally;</text>
      <biological_entity id="o25373" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character constraint="than the lemmas" constraintid="o25374" is_modifier="false" name="length_or_size" src="d0_s17" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o25374" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity constraint="intercostal" id="o25375" name="region" name_original="region" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 1.5-2.6 mm;</text>
      <biological_entity id="o25376" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary apices pubescent.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = unknown.</text>
      <biological_entity constraint="ovary" id="o25377" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25378" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Festuca ligulata grows on moist, shady slopes in the mountains of western Texas and north-central Mexico. It is listed as an endangered species under the Endangered Species Act of the United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>