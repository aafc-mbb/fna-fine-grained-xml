<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">786</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ALOPECURUS</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">carolinianus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus alopecurus;species carolinianus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Tufted foxtail</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o14484" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-50 cm, erect or decumbent.</text>
      <biological_entity id="o14485" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ligules 2.8-4.5 mm, obtuse;</text>
      <biological_entity id="o14486" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-15 cm long, 0.9-3 mm wide;</text>
      <biological_entity id="o14487" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>upper sheaths not or only slightly inflated.</text>
      <biological_entity constraint="upper" id="o14488" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="only slightly" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 1-7 cm long, 3-6 mm wide, always dense.</text>
      <biological_entity id="o14489" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="always" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Glumes 2.1-3.1 mm, connate at the base, membranous throughout, sparsely pubescent, not dilated below, keels not winged, ciliate, apices obtuse, pale green to pale-yellow;</text>
      <biological_entity id="o14490" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s7" to="3.1" to_unit="mm" />
        <character constraint="at base" constraintid="o14491" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="throughout" name="texture" notes="" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="not; below" name="shape" src="d0_s7" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o14491" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o14492" name="keel" name_original="keels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14493" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s7" to="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 1.9-2.7 mm, connate in the lower 1/2, glabrous, apices obtuse, awns 3-6.5 mm, geniculate, exceeding the lemmas by 1.6-4 mm;</text>
      <biological_entity id="o14494" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s8" to="2.7" to_unit="mm" />
        <character constraint="in lower 1/2" constraintid="o14495" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14495" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity id="o14496" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o14497" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o14498" name="lemma" name_original="lemmas" src="d0_s8" type="structure" />
      <relation from="o14497" id="r2301" name="exceeding the" negation="false" src="d0_s8" to="o14498" />
    </statement>
    <statement id="d0_s9">
      <text>anthers 0.3-1 mm, yellow or orange.</text>
      <biological_entity id="o14499" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Caryopses 1-1.5 mm. 2n = 14.</text>
      <biological_entity id="o14500" name="caryopse" name_original="caryopses" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14501" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Alopecurus carolinianus is native to the central plains, Mississippi valley, and southeastern United States, where it is common in wet meadows, ditches, wetland edges, and other moist, open habitats; it is occasionally a weed of rice fields. At the northern limit of its range it is clearly adventive, growing in gardens and nurseries. It also occurs in arid areas of the prairies and southwest, growing sporadically along sloughs and in ditches and vernal pools. Whether such populations are native or naturalized is not clear.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Del.;D.C.;Wis.;W.Va.;Fla.;Wyo.;Tenn.;N.J.;N.Mex.;Tex.;La.;Alta.;B.C.;Sask.;N.C.;S.C.;Pa.;N.Y.;Mass.;Va.;Colo.;Calif.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Conn.;Md.;Ohio;Utah;Mo.;Minn.;Mich.;Mont.;Miss.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>