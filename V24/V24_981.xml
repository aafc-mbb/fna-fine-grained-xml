<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">693</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="(Griseb.) Scribn. &amp; Merr." date="unknown" rank="genus">PODAGROSTIS</taxon_name>
    <taxon_name authority="(Trin. Scribn. &amp;c Merr.)" date="unknown" rank="species">aequivalvis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus podagrostis;species aequivalvis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aequivalvis</taxon_name>
    <taxon_hierarchy>genus agrostis;species aequivalvis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Arctic bent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous.</text>
      <biological_entity id="o13809" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 25-90 cm, erect;</text>
      <biological_entity id="o13810" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 2-4 (6).</text>
      <biological_entity id="o13811" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="atypical_quantity" src="d0_s2" value="6" value_original="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths smooth;</text>
      <biological_entity id="o13812" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.4-4 mm, scabridulous, truncate to subacute, entire or lacerate;</text>
      <biological_entity id="o13813" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="subacute entire or lacerate" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="subacute entire or lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 4-18 cm long, 1-2.5 mm wide, flat.</text>
      <biological_entity id="o13814" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 5-15 cm long, 2-10 cm wide, lanceolate to ovate, often drooping, sparsely branched, lowest nodes with 1-4 (5) branches;</text>
      <biological_entity id="o13815" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovate" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s6" value="drooping" value_original="drooping" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s6" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o13816" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o13817" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <relation from="o13816" id="r2194" name="with" negation="false" src="d0_s6" to="o13817" />
    </statement>
    <statement id="d0_s7">
      <text>branches usually scabridulous, sometimes smooth, erect to ascending or spreading, spikelets usually restricted to the distal 1/2;</text>
      <biological_entity id="o13818" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="ascending or spreading" />
      </biological_entity>
      <biological_entity id="o13819" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower branches 3-6 cm;</text>
      <biological_entity constraint="lower" id="o13820" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2-10 mm.</text>
      <biological_entity id="o13821" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets narrowly ovate to lanceolate, usually purplish bronze, sometimes greenish purple;</text>
      <biological_entity id="o13822" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purplish bronze" value_original="purplish bronze" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="greenish purple" value_original="greenish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla prolongations 0.5-1.9 mm, bristlelike, distal hairs shorter than 0.3 mm.</text>
      <biological_entity id="o13823" name="rachillum" name_original="rachilla" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bristlelike" value_original="bristlelike" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13824" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes 2.3-4.3 mm, veins sparsely scabridulous distally, apices acute to acuminate, sometimes apiculate;</text>
      <biological_entity id="o13825" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13826" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely; distally" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o13827" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s12" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes usually equal to the upper glumes, usually 3-veined, lateral-veins faint;</text>
      <biological_entity constraint="lower" id="o13828" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="to upper glumes" constraintid="o13829" is_modifier="false" modifier="usually" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13829" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o13830" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="faint" value_original="faint" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses glabrous or with sparse hairs shorter than 0.1 mm;</text>
      <biological_entity id="o13831" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="with sparse hairs" value_original="with sparse hairs" />
      </biological_entity>
      <biological_entity id="o13832" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s14" value="sparse" value_original="sparse" />
        <character modifier="shorter than" name="some_measurement" src="d0_s14" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <relation from="o13831" id="r2195" name="with" negation="false" src="d0_s14" to="o13832" />
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2.5-3.5 mm, smooth, opaque, (3) 5-veined, veins usually obscure, apices acute, entire or the veins minutely excurrent to about 0.3 mm, unawned;</text>
      <biological_entity id="o13833" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="(3)5-veined" value_original="(3)5-veined" />
      </biological_entity>
      <biological_entity id="o13834" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s15" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o13835" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13836" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character constraint="to about 0.3 mm" is_modifier="false" modifier="minutely" name="architecture" src="d0_s15" value="excurrent" value_original="excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2-3 mm;</text>
      <biological_entity id="o13837" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 0.8-1.3 mm.</text>
      <biological_entity id="o13838" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 1.2-1.5 mm;</text>
      <biological_entity id="o13839" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>endosperm solid.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o13840" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13841" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Podagrostis aequivalvis grows along lake, bog, and stream margins, and in forest fens. It is common in the coastal regions of Alaska and British Columbia, and occurs less frequently inland, as well as to about 1500 m in the Cascade Mountains south to Oregon.</discussion>
  
</bio:treatment>