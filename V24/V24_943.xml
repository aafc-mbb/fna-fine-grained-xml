<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">670</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">LAGURUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus lagurus</taxon_hierarchy>
  </taxon_identification>
  <number>14.30</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o28132" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 6-80 cm, erect or ascending, pilose or villous.</text>
      <biological_entity id="o28133" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves pilose or villous;</text>
      <biological_entity id="o28134" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths open nearly to the base;</text>
      <biological_entity id="o28135" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="to base" constraintid="o28136" is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o28136" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>auricles absent;</text>
      <biological_entity id="o28137" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules truncate, erose, ciliate, abaxial surfaces densely pubescent;</text>
      <biological_entity id="o28138" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28139" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat at maturity, convolute in the bud.</text>
      <biological_entity id="o28140" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character constraint="at maturity" is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character constraint="in bud" constraintid="o28141" is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity id="o28141" name="bud" name_original="bud" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal panicles, dense, ovoid.</text>
      <biological_entity id="o28142" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" notes="" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o28143" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets laterally compressed, with 1 floret;</text>
      <biological_entity id="o28144" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o28145" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28144" id="r4423" name="with" negation="false" src="d0_s9" to="o28145" />
    </statement>
    <statement id="d0_s10">
      <text>rachillas prolonged beyond the base of the floret, pubescent;</text>
      <biological_entity id="o28147" name="base" name_original="base" src="d0_s10" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity id="o28148" name="floret" name_original="floret" src="d0_s10" type="structure" />
      <relation from="o28147" id="r4424" name="part_of" negation="false" src="d0_s10" to="o28148" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes, beneath the floret.</text>
      <biological_entity id="o28146" name="rachilla" name_original="rachillas" src="d0_s10" type="structure">
        <character constraint="beyond base" constraintid="o28147" is_modifier="false" name="length" src="d0_s10" value="prolonged" value_original="prolonged" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28149" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o28150" name="floret" name_original="floret" src="d0_s11" type="structure" />
      <relation from="o28146" id="r4425" name="above" negation="false" src="d0_s11" to="o28149" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes 2, equal, exceeding the florets, lanceolate, membranous, pilose, hairs 2-3 mm, 1-veined, veins acuminate and awned, awns pilose;</text>
      <biological_entity id="o28151" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28152" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity id="o28153" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o28154" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o28155" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o28151" id="r4426" name="exceeding the" negation="false" src="d0_s12" to="o28152" />
    </statement>
    <statement id="d0_s13">
      <text>calluses blunt, pubescent;</text>
      <biological_entity id="o28156" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas lanceolate, pilose or scabridulous, 3-veined, 3-awned, apices bifid, lateral-veins extending as slender awns, not twisted below, central awn arising from the distal 1/3 of the lemma, twisted below;</text>
      <biological_entity id="o28157" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o28158" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o28159" name="lateral-vein" name_original="lateral-veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; below" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="slender" id="o28160" name="awn" name_original="awns" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o28161" name="awn" name_original="awn" src="d0_s14" type="structure">
        <character constraint="from distal 1/3" constraintid="o28162" is_modifier="false" name="orientation" src="d0_s14" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="below" name="architecture" notes="" src="d0_s14" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28162" name="1/3" name_original="1/3" src="d0_s14" type="structure" />
      <biological_entity id="o28163" name="lemma" name_original="lemma" src="d0_s14" type="structure" />
      <relation from="o28159" id="r4427" name="extending as" negation="false" src="d0_s14" to="o28160" />
      <relation from="o28162" id="r4428" name="part_of" negation="false" src="d0_s14" to="o28163" />
    </statement>
    <statement id="d0_s15">
      <text>paleas nearly as long as the lemmas, veins scabridulous, extending as awnlike points;</text>
      <biological_entity id="o28164" name="palea" name_original="paleas" src="d0_s15" type="structure" />
      <biological_entity id="o28165" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o28166" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o28167" name="point" name_original="points" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="awnlike" value_original="awnlike" />
      </biological_entity>
      <relation from="o28164" id="r4429" name="as long as" negation="false" src="d0_s15" to="o28165" />
      <relation from="o28166" id="r4430" name="extending as" negation="false" src="d0_s15" to="o28167" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2, narrowly elliptic, glabrous, apices minutely bilobed;</text>
      <biological_entity id="o28168" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28169" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s16" value="bilobed" value_original="bilobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3;</text>
      <biological_entity id="o28170" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovaries glabrous;</text>
      <biological_entity id="o28171" name="ovary" name_original="ovaries" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles fused or separate.</text>
      <biological_entity id="o28172" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s19" value="fused" value_original="fused" />
        <character is_modifier="false" name="arrangement" src="d0_s19" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses stipitate, subterete, ellipsoid;</text>
      <biological_entity id="o28173" name="caryopse" name_original="caryopses" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s20" value="subterete" value_original="subterete" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>hila 1/5-1/4 as long as the caryopses, ovate, x = 7.</text>
      <biological_entity id="o28174" name="hilum" name_original="hila" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o28175" from="1/5" name="quantity" src="d0_s21" to="1/4" />
        <character is_modifier="false" name="shape" notes="" src="d0_s21" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o28175" name="caryopse" name_original="caryopses" src="d0_s21" type="structure" />
      <biological_entity constraint="x" id="o28176" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lagurus is a monotypic genus endemic to the Mediterranean region. Lagurus ovatus has been introduced in North America, as well as South America, southern Africa, and Australia. It is sometimes cultivated as an ornamental, and the dried flowering culms are used in floral arrangements.</discussion>
  <references>
    <reference>Messeri, A. 1942. Studio sistematico e fitogeografico di Lagurus ovatus L. Nuovo Giorn. Bot. Ital., n.s., 49:133-204</reference>
    <reference>Vergagno Gambi, O. 1965. Osservazioni sullo sviluppo di Lagurus ovatus L. Giorn. Bot. Ital. 72:243-254.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;Alta.;Ont.;Que.;Calif.;N.C.;Fla.;Oreg.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>