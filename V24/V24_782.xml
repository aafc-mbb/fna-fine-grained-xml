<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">560</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Hitchc. ex Soreng" date="unknown" rank="subsection">Epiles</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">cusickii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">cusickii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection epiles;species cusickii;subspecies cusickii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">hansenii</taxon_name>
    <taxon_hierarchy>genus poa;species hansenii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely tufted.</text>
    </statement>
    <statement id="d0_s1">
      <text>Basal branching intravaginal.</text>
      <biological_entity id="o4956" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s1" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-60 (70) cm, mostly erect, with 0-1 well-exserted nodes.</text>
      <biological_entity id="o4957" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="70" value_original="70" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="well-exserted" id="o4958" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s2" to="1" />
      </biological_entity>
      <relation from="o4957" id="r807" name="with" negation="false" src="d0_s2" to="o4958" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for 1/4-1/3 their length, distal sheath lengths 3-10 times blade lengths;</text>
      <biological_entity id="o4959" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4960" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o4961" is_modifier="false" name="length" src="d0_s3" value="3-10 times blade lengths" value_original="3-10 times blade lengths" />
      </biological_entity>
      <biological_entity id="o4961" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>innovation blades 0.5-1 mm wide;</text>
      <biological_entity constraint="innovation" id="o4962" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades less than 1.5 mm wide, flat, folded, or involute, apices narrowly prow-shaped, flag leaf-blades (0.5) 1.5-5 cm.</text>
      <biological_entity constraint="cauline" id="o4963" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o4964" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o4965" name="blade-leaf" name_original="leaf-blades" src="d0_s5" type="structure">
        <character name="atypical_distance" src="d0_s5" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="distance" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles usually 5-10 (12) cm, contracted or loosely contracted, with 20-100 spikelets;</text>
      <biological_entity id="o4966" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o4967" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s6" to="100" />
      </biological_entity>
      <relation from="o4966" id="r808" name="with" negation="false" src="d0_s6" to="o4967" />
    </statement>
    <statement id="d0_s7">
      <text>nodes with 1-5 branches;</text>
      <biological_entity id="o4968" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o4969" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o4968" id="r809" name="with" negation="false" src="d0_s7" to="o4969" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1.7-4 (5) cm, slender to stout, moderately to densely scabrous, with 2-15 spikelets.</text>
      <biological_entity id="o4970" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="slender" name="size" src="d0_s8" to="stout" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o4971" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
      <relation from="o4970" id="r810" name="with" negation="false" src="d0_s8" to="o4971" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-10 mm.</text>
      <biological_entity id="o4972" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calluses glabrous;</text>
      <biological_entity id="o4973" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 4-7 mm, glabrous;</text>
      <biological_entity id="o4974" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers vestigial (0.1-0.2 mm) or 2-3.5 mm. 2n = 28.</text>
      <biological_entity id="o4975" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s12" value="2-3.5 mm" value_original="2-3.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4976" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa cusickii subsp. cusickii grows mainly in mesic desert upland and mountain meadows, on and around the Columbia plateaus of northern California, Oregon, southern Washington, and adjacent Idaho and Nevada. It is highly variable, with fairly open- to contracted-panicle populations, and from gynodioecious to dioecious populations. The modal and mean longest branch lengths of the narrower-panicled populations of subsp. cusickii serve to distinguish it from subsp. pallida in most cases. It appears to have hybridized with P. pringlei (p. 564) around Mount Shasta, California, and Mount Rose, Nevada. Poa stebbinsii (p. 564), an endemic in the high Sierra Nevada, is easily distin¬guished from P. cusickii subsp. cusickii by its long hyaline ligules.</discussion>
  
</bio:treatment>