<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Hochst." date="unknown" rank="genus">LEYMUS</taxon_name>
    <taxon_name authority="(Scribn. &amp; J.G. Sm.) Pilg." date="unknown" rank="species">flavescens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus leymus;species flavescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flavescens</taxon_name>
    <taxon_hierarchy>genus elymus;species flavescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">arenicolus</taxon_name>
    <taxon_hierarchy>genus elymus;species arenicolus</taxon_hierarchy>
  </taxon_identification>
  <number>16</number>
  <other_name type="common_name">Yellow wildrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes cespitose, strongly rhizomatous.</text>
      <biological_entity id="o16388" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 40-120 cm tall, 2-4 mm thick, pubescent beneath the nodes.</text>
      <biological_entity id="o16389" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="height" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
        <character constraint="beneath nodes" constraintid="o16390" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16390" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves exceeded by the spikes;</text>
      <biological_entity id="o16391" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16392" name="spike" name_original="spikes" src="d0_s2" type="structure" />
      <relation from="o16391" id="r2626" name="exceeded by the" negation="false" src="d0_s2" to="o16392" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths glabrous;</text>
      <biological_entity id="o16393" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent, sometimes with a few hairs in the auricular position;</text>
      <biological_entity id="o16394" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16395" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <relation from="o16394" id="r2627" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o16395" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-1.5 mm;</text>
      <biological_entity id="o16396" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-4 mm wide, usually involute, adaxial surfaces scabrous, sometimes with scattered hairs, hairs to 1 mm, with about 15 closely spaced, subequal, mostly prominently ribbed veins.</text>
      <biological_entity id="o16397" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16398" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o16399" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o16400" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16401" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="15" value_original="15" />
        <character is_modifier="true" modifier="closely" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
        <character is_modifier="true" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character is_modifier="true" modifier="mostly prominently" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <relation from="o16398" id="r2628" modifier="sometimes" name="with" negation="false" src="d0_s6" to="o16399" />
      <relation from="o16400" id="r2629" name="with" negation="false" src="d0_s6" to="o16401" />
    </statement>
    <statement id="d0_s7">
      <text>Spikes 10-20 cm long, 12-20 mm thick, with 12-20 nodes and 2 spikelets per node;</text>
      <biological_entity id="o16402" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="thickness" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16403" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s7" to="20" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16404" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o16405" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o16402" id="r2630" name="with" negation="false" src="d0_s7" to="o16403" />
      <relation from="o16404" id="r2631" name="per" negation="false" src="d0_s7" to="o16405" />
    </statement>
    <statement id="d0_s8">
      <text>internodes 7-10 mm, densely hairy.</text>
      <biological_entity id="o16406" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 13.5-25 mm, with 4-9 florets.</text>
      <biological_entity id="o16407" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="13.5" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16408" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
      <relation from="o16407" id="r2632" name="with" negation="false" src="d0_s9" to="o16408" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes 8.5-16 mm long, 0.5-2.5 mm wide, stiff, keeled distally, the central portion thicker than the margins, tapering from below midlength to the subulate apices, hairy, 0-1 (3) -veined, veins inconspicuous at midlength;</text>
      <biological_entity id="o16409" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="length" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity constraint="central" id="o16410" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character constraint="than the margins" constraintid="o16411" is_modifier="false" name="width" src="d0_s10" value="thicker" value_original="thicker" />
        <character constraint="from midlength" constraintid="o16412" is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="0-1(3)-veined" value_original="0-1(3)-veined" />
      </biological_entity>
      <biological_entity id="o16411" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o16412" name="midlength" name_original="midlength" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="below midlength" name="position" src="d0_s10" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o16413" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o16414" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character constraint="at midlength" constraintid="o16415" is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o16415" name="midlength" name_original="midlength" src="d0_s10" type="structure" />
      <relation from="o16412" id="r2633" name="to" negation="false" src="d0_s10" to="o16413" />
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 8.5-13.5 mm;</text>
      <biological_entity constraint="lower" id="o16416" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s11" to="13.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 10-16 mm;</text>
      <biological_entity constraint="upper" id="o16417" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses poorly developed;</text>
      <biological_entity id="o16418" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s13" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 10.5-15 mm, densely villous, hairs 2-3 mm, apices unawned or awned.</text>
      <biological_entity id="o16419" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="10.5" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o16420" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16421" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns to 2 mm;</text>
      <biological_entity id="o16422" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 4.5-7 mm, dehiscent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 28.</text>
      <biological_entity id="o16423" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16424" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leymus flavescens grows on sand dunes and open sandy flats, and ditch- and roadbanks, of the Snake and Columbia river valleys. The central Washington population is growing on a road cut; it seems to be well established there.</discussion>
  <discussion>Plants identified as Elytnus arenicolus Scribn. &amp; J.G. Sm. are included here, but they may represent hybrids between Leymus flavescens and L. triticoides. Leckenby, the collector of the type specimen, noted that they grew on sand or sand drifts along the Columbia River, but could not withstand flooding. He could find no seed.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;Wash.;Utah;Oreg.;Mont.;Idaho</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>