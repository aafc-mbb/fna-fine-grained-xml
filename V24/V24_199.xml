<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">PIPTATHERUM</taxon_name>
    <taxon_name authority="(Trin. &amp; Rupr.) Barkworth" date="unknown" rank="species">micranthum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus piptatherum;species micranthum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oryzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_hierarchy>genus oryzopsis;species micrantha</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Small-flowered piptatherum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, not rhizomatous.</text>
      <biological_entity id="o10321" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-85 cm, glabrous;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal branching extravaginal.</text>
      <biological_entity id="o10322" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="85" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o10323" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s3" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o10324" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.4-1.5 (2.5) mm, truncate;</text>
      <biological_entity id="o10325" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-16 cm long, 0.5-2.5 mm wide, usually involute.</text>
      <biological_entity id="o10326" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="16" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-20 cm, lower nodes with 1-3 branches;</text>
      <biological_entity id="o10327" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10328" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o10329" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o10328" id="r1651" name="with" negation="false" src="d0_s7" to="o10329" />
    </statement>
    <statement id="d0_s8">
      <text>branches 2-6 cm, divergent to reflexed at maturity, with 3-10 (15) spikelets, secondary branches appressed to the primary branches.</text>
      <biological_entity id="o10330" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o10331" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="15" value_original="15" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o10332" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="to primary branches" constraintid="o10333" is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="primary" id="o10333" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <relation from="o10330" id="r1652" name="with" negation="false" src="d0_s8" to="o10331" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 2.5-3.5 mm, acute;</text>
      <biological_entity id="o10334" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o10335" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o10336" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets 1.5-2.5 mm, dorsally compressed;</text>
      <biological_entity id="o10337" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 0.1-0.2 mm, glabrous or sparsely hairy, disarticulation scars circular;</text>
      <biological_entity id="o10338" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10339" name="scar" name_original="scars" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="circular" value_original="circular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas usually glabrous, sometimes sparsely pubescent, brownish, shiny, 5-veined, margins not overlapping at maturity;</text>
      <biological_entity id="o10340" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o10341" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="arrangement" src="d0_s14" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 4-8 mm, straight or almost so, caducous;</text>
      <biological_entity id="o10342" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character name="course" src="d0_s15" value="almost" value_original="almost" />
        <character is_modifier="false" name="duration" src="d0_s15" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.6-1.2 mm, not penicillate;</text>
      <biological_entity id="o10343" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovaries truncate to rounded, bearing 2 separate styles.</text>
      <biological_entity id="o10344" name="ovary" name_original="ovaries" src="d0_s17" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s17" to="rounded" />
      </biological_entity>
      <biological_entity id="o10345" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s17" value="separate" value_original="separate" />
      </biological_entity>
      <relation from="o10344" id="r1653" name="bearing" negation="false" src="d0_s17" to="o10345" />
    </statement>
    <statement id="d0_s18">
      <text>Caryopses about 1.2 mm long, about 0.8 mm wide;</text>
      <biological_entity id="o10346" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character name="length" src="d0_s18" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s18" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>hila linear, 3/4 - 9/10 as long as the caryopses.</text>
      <biological_entity id="o10348" name="caryopse" name_original="caryopses" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 22.</text>
      <biological_entity id="o10347" name="hilum" name_original="hila" src="d0_s19" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s19" value="linear" value_original="linear" />
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o10348" from="3/4" name="quantity" src="d0_s19" to="9/10" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10349" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Piptatherum micranthum grows on gravel benches, rocky slopes, and creek banks, from British Columbia to Manitoba and south to Arizona, New Mexico, and western Texas. The combination of small, dorsally compressed florets and appressed pedicels distinguishes this species from all other native North American Stipeae. Achnatherum contractum is the fertile derivative of hybridization between Piptatherum micranthum and A. hymenoides. It is placed in Achnatherum because it resembles that genus more than Piptatherum.</discussion>
  
</bio:treatment>