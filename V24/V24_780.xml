<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Hitchc. ex Soreng" date="unknown" rank="subsection">Epiles</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection epiles</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely or rarely moderately densely tufted, usually neither stoloniferous nor rhizomatous, rarely shortly rhizomatous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intra or extravaginal, or both.</text>
      <biological_entity id="o10159" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely moderately densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="rarely shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 5-60 (70) cm.</text>
      <biological_entity id="o10160" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="70" value_original="70" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/7 – 4/5 their length, terete, distal sheaths longer than their blades;</text>
      <biological_entity id="o10161" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10162" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than their blades" constraintid="o10163" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10163" name="blade" name_original="blades" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>innovation blades sometimes involute and firmer than the cauline blades, adaxial surfaces smooth or moderately to densely scabrous, glabrous or hispidulous on and between the veins;</text>
      <biological_entity constraint="innovation" id="o10164" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character constraint="than the cauline blades" constraintid="o10165" is_modifier="false" name="fragility_or_texture" src="d0_s5" value="firmer" value_original="firmer" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10165" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o10166" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s5" to="moderately densely scabrous glabrous or hispidulous" />
        <character char_type="range_value" constraint="on and between veins" constraintid="o10167" from="smooth or" name="pubescence" src="d0_s5" to="moderately densely scabrous glabrous or hispidulous" />
      </biological_entity>
      <biological_entity id="o10167" name="vein" name_original="veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline blades 0.5-3 mm wide, flat, folded, or involute, thin to thick, soft to firm, apices usually narrowly prow-shaped, cauline blades sometimes broadly prow-shaped.</text>
      <biological_entity constraint="cauline" id="o10168" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character char_type="range_value" from="thin" name="width" src="d0_s6" to="thick" />
        <character char_type="range_value" from="soft" name="texture" src="d0_s6" to="firm" />
      </biological_entity>
      <biological_entity id="o10169" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s6" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10170" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes broadly" name="shape" src="d0_s6" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 1-12 cm, erect or slightly nodding, contracted or open, usually narrowly lanceolate to ovate, sometimes pyramidal;</text>
      <biological_entity id="o10171" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character char_type="range_value" from="usually narrowly lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nodes with 1-3 (5) branches;</text>
      <biological_entity id="o10172" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o10173" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o10172" id="r1625" name="with" negation="false" src="d0_s8" to="o10173" />
    </statement>
    <statement id="d0_s9">
      <text>branches 0.5-4 (5) cm, terete to angled, smooth or sparsely to densely scabrous.</text>
      <biological_entity id="o10174" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s9" to="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets (3) 4-10 (12) mm, lengths to 3.5 times widths, lanceolate to broadly ovate, not bulbiferous;</text>
      <biological_entity id="o10175" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s10" value="0-3.5" value_original="0-3.5" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="broadly ovate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2-8.</text>
      <biological_entity id="o10176" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes lanceolate to broadly lanceolate;</text>
      <biological_entity id="o10177" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="broadly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o10178" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses usually glabrous, sometimes shortly and sparsely webbed;</text>
      <biological_entity id="o10179" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes shortly; shortly; sparsely" name="pubescence" src="d0_s14" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas (3) 3.5-8 mm, lanceolate to broadly lanceolate, usually glabrous, sometimes sub-puberulent, or short-villous in hybrids;</text>
      <biological_entity id="o10180" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s15" to="broadly lanceolate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s15" value="sub-puberulent" value_original="sub-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="short-villous" value_original="short-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="sub-puberulent" value_original="sub-puberulent" />
        <character constraint="in hybrids" constraintid="o10181" is_modifier="false" name="pubescence" src="d0_s15" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o10181" name="hybrid" name_original="hybrids" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>palea keels scabrous, glabrous or ciliate;</text>
      <biological_entity constraint="palea" id="o10182" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, vestigial (0.1-0.2 mm), aborted late in development, or 1.3-4.5 mm.</text>
      <biological_entity id="o10183" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="vestigial" value_original="vestigial" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The five species of Poa subsect. Epiles are cespitose, gynodioecious or dioecious, and have involute or folded leaf blades.</discussion>
  
</bio:treatment>