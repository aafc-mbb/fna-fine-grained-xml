<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Bromus</taxon_name>
    <taxon_name authority="Trin. ex C.A. Mey." date="unknown" rank="species">danthoniae</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromus;species danthoniae</taxon_hierarchy>
  </taxon_identification>
  <number>46</number>
  <other_name type="common_name">Three-awned brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o24468" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-40 cm, erect or ascending.</text>
      <biological_entity id="o24469" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o24470" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1.2-2.6 mm, puberulent, obtuse, laciniate;</text>
      <biological_entity id="o24471" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s3" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 2-15 cm long, 2-5 mm wide, pubescent on both surfaces.</text>
      <biological_entity id="o24472" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o24473" is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24473" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 2-12 cm long, 1-5 cm wide, dense, ovoid, stiffly erect, sometimes racemose;</text>
      <biological_entity id="o24474" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches shorter than the spikelets, ascending, slightly curved or straight.</text>
      <biological_entity id="o24475" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character constraint="than the spikelets" constraintid="o24476" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24476" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 10-40 (45) mm long, 4-10 mm wide, lanceolate to elliptic or oblong, laterally compressed;</text>
      <biological_entity id="o24477" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="mm" value="45" value_original="45" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="elliptic or oblong" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>florets 5-8 (10), bases concealed at maturity;</text>
      <biological_entity id="o24478" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character name="atypical_quantity" src="d0_s8" value="10" value_original="10" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o24479" name="base" name_original="bases" src="d0_s8" type="structure">
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachilla internodes concealed at maturity.</text>
      <biological_entity constraint="rachilla" id="o24480" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s9" value="concealed" value_original="concealed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes glabrous or pubescent;</text>
      <biological_entity id="o24481" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 5-8.5 mm, 3-5-veined, lanceolate;</text>
      <biological_entity constraint="lower" id="o24482" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 6.5-9.5 mm, 7-9 (11) -veined, elliptic;</text>
      <biological_entity constraint="upper" id="o24483" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="9.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="7-9(11)-veined" value_original="7-9(11)-veined" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 8-12 (13.5) mm long, 6-7 mm wide, oblanceolate, veins glabrous, scabridulous, or ciliolate, glabrous or pubescent elsewhere, 9-11-veined, rounded over the midvein, margins broadly hyaline, bluntly angled above the middle, not inrolled at maturity, apices subulate to acute or obtuse, toothed, teeth shorter than 1 mm;</text>
      <biological_entity id="o24484" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="length" src="d0_s13" unit="mm" value="13.5" value_original="13.5" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o24485" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="ciliolate" value_original="ciliolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="elsewhere" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="9-11-veined" value_original="9-11-veined" />
        <character constraint="over midvein" constraintid="o24486" is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24486" name="midvein" name_original="midvein" src="d0_s13" type="structure" />
      <biological_entity id="o24487" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="broadly" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character constraint="above middle lemmas" constraintid="o24488" is_modifier="false" modifier="bluntly" name="shape" src="d0_s13" value="angled" value_original="angled" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="shape_or_vernation" notes="" src="d0_s13" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="middle" id="o24488" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o24489" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s13" to="acute or obtuse" />
        <character is_modifier="false" name="shape" src="d0_s13" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o24490" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>awns usually 3 on the upper lemmas in each spikelet, arising 2-4 mm below the lemma apices, purple or deep red, central awn 5-25 mm, flattened at the base, divaricate and sometimes twisted at maturity, lateral awns 4-10 mm, erect or reflexed, sometimes absent or much reduced on the lower lemmas;</text>
      <biological_entity id="o24491" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character constraint="on upper lemmas" constraintid="o24492" name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s14" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o24494" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24492" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity id="o24493" name="spikelet" name_original="spikelet" src="d0_s14" type="structure" />
      <biological_entity constraint="lemma" id="o24494" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o24495" name="awn" name_original="awn" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
        <character constraint="at base" constraintid="o24496" is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character constraint="at lateral awns" constraintid="o24497" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character constraint="on lower lemmas" constraintid="o24498" is_modifier="false" modifier="much; much" name="size" src="d0_s14" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o24496" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="lateral" id="o24497" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s14" value="maturity" value_original="maturity" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o24498" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o24492" id="r3868" name="in" negation="false" src="d0_s14" to="o24493" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1-1.8 mm.</text>
      <biological_entity id="o24499" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses equaling or shorter than the paleas, thin, weakly inrolled or flat.</text>
      <biological_entity id="o24501" name="palea" name_original="paleas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o24500" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character is_modifier="false" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
        <character constraint="than the paleas" constraintid="o24501" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s16" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24502" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus danthoniae is native from the western Asia to southern Russia and Tibet. It was collected in 1904 in Ontario; no other North American collections are known.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>