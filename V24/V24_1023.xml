<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">724</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="(J. Presl) Steud." date="unknown" rank="species">nutkaensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species nutkaensis</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Pacific reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes with sterile culms;</text>
      <biological_entity id="o19007" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o19006" id="r3005" name="with" negation="false" src="d0_s0" to="o19007" />
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose, with rhizomes usually shorter than 3 cm, rarely to 6 cm long, 1.5-3 mm thick.</text>
      <biological_entity id="o19006" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="rarely" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19008" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s1" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="rarely" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (42) 55-105 (150) cm, stout, unbranched, smooth or slightly scabrous beneath the panicles;</text>
      <biological_entity id="o19009" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="42" value_original="42" />
        <character char_type="range_value" from="55" from_unit="cm" name="some_measurement" src="d0_s2" to="105" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="beneath panicles" constraintid="o19010" is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19010" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-2 (3).</text>
      <biological_entity id="o19011" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars smooth;</text>
      <biological_entity id="o19012" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19013" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (0.5) 1-4 (5.5) mm, usually truncate, entire, often hidden by the expanded collars;</text>
      <biological_entity id="o19014" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="by collars" constraintid="o19015" is_modifier="false" modifier="often" name="prominence" src="d0_s5" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o19015" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (4) 10-41 (56) cm long, (2) 4-10 (20) mm wide, flat, usually erect, abaxial surfaces smooth, adaxial surfaces smooth or slightly scabrous, glabrous or sparsely hairy.</text>
      <biological_entity id="o19016" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="41" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19017" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19018" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (8) 12-23 (33) cm long, (1.1) 2-4.5 (9) cm wide, contracted to somewhat loose, erect to slightly nodding, greenish yellow to purple-tinged;</text>
      <biological_entity id="o19019" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s7" to="23" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="1.1" value_original="1.1" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="4.5" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_fragility" src="d0_s7" value="loose" value_original="loose" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="slightly nodding" />
        <character char_type="range_value" from="greenish yellow" name="coloration" src="d0_s7" to="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 2.7-7 (10.5) cm long, sparsely scabrous, spikelets usually confined to the distal 1/2.</text>
      <biological_entity id="o19020" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="10.5" value_original="10.5" />
        <character char_type="range_value" from="2.7" from_unit="cm" name="length" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19021" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4.5-6.5 (8) mm;</text>
      <biological_entity id="o19022" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla prolongations 0.5-1 mm, hairs 1-1.5 mm.</text>
      <biological_entity id="o19023" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19024" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes keeled, smooth or infrequently scabrous on the keels, lateral-veins somewhat prominent, apices acuminate;</text>
      <biological_entity id="o19025" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character constraint="on keels" constraintid="o19026" is_modifier="false" modifier="infrequently" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19026" name="keel" name_original="keels" src="d0_s11" type="structure" />
      <biological_entity id="o19027" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="somewhat" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o19028" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs (1) 2-2.5 (3) mm, (0.2) 0.5-0.7 times as long as the lemmas, sparse;</text>
      <biological_entity constraint="callus" id="o19029" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character constraint="lemma" constraintid="o19030" is_modifier="false" name="length" src="d0_s12" value="(0.2)0.5-0.7 times as long as the lemmas" />
        <character is_modifier="false" name="count_or_density" src="d0_s12" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o19030" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas (3) 4-4.5 (5) mm, (0.4) 0.8-1.2 (1.9) mm shorter than the glumes;</text>
      <biological_entity id="o19031" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o19032" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19032" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>awns 1-3 mm, attached on the lower 1/3-1/2 of the lemmas, not exserted, easily distinguished from the callus hairs, straight or slightly bent;</text>
      <biological_entity id="o19033" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character constraint="on lower 1/3-1/2" constraintid="o19034" is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" modifier="not" name="position" notes="" src="d0_s14" value="exserted" value_original="exserted" />
        <character constraint="from callus hairs" constraintid="o19036" is_modifier="false" modifier="easily" name="prominence" src="d0_s14" value="distinguished" value_original="distinguished" />
        <character is_modifier="false" name="course" notes="" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19034" name="1/3-1/2" name_original="1/3-1/2" src="d0_s14" type="structure" />
      <biological_entity id="o19035" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="callus" id="o19036" name="hair" name_original="hairs" src="d0_s14" type="structure" />
      <relation from="o19034" id="r3006" name="part_of" negation="false" src="d0_s14" to="o19035" />
    </statement>
    <statement id="d0_s15">
      <text>anthers (1) 2.4-2.6 (3.3) mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o19037" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19038" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis nutkaensis grows in wetlands and openings in coniferous forests, on marine and freshwater beaches and dunes, and, sometimes, on cliffs. It is usually found within a few kilometers of the marine shoreline at or near sea level, but it sometimes occurs as high as 800 m on the Brooks Peninsula of Vancouver Island, British Columbia; at 700 m and 1100 m in the Siskiyou Mountains, Oregon; and at 1100 m on Bald Mountain, California. It grows along the Pacific coast of North America from the Aleutian Islands in Alaska to San Luis Obispo County, California, and also in the Kamchatka Peninsula of Russia.</discussion>
  <discussion>A hybrid between Calamagrostis nutkaensis and Ammophila arenaria (p. 777) was reported in the 1970s from the vicinity of Newport-Waldport in coastal Oregon, where both species grow (Kenton Chambers, pers. comm.). There is no voucher to support this report.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Alaska;Calif.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>