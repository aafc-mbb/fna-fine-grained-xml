<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">473</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Pari." date="unknown" rank="genus">PUCCINELLIA</taxon_name>
    <taxon_name authority="Swallen" date="unknown" rank="species">andersonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus puccinellia;species andersonii</taxon_hierarchy>
  </taxon_identification>
  <number>16</number>
  <other_name type="common_name">Anderson's alkali grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not mat-forming.</text>
      <biological_entity id="o7318" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-25 cm, usually decumbent or geniculate.</text>
      <biological_entity id="o7319" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ligules 1-2.8 (3.3) mm, acute, obtuse, or truncate, entire or slightly erose;</text>
      <biological_entity id="o7320" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="mm" value="3.3" value_original="3.3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s3" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually involute and 0.5-1 mm in diameter, sometimes flat and 0.8-2 mm wide.</text>
      <biological_entity id="o7321" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character constraint="in diameter , sometimes flat and 0.8-2 mm" is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (3.5) 5-8 cm, diffuse or contracted at maturity, lowest node almost always with 2 branches, lower branches ascending to horizontal in fruit, spikelets usually confined to the distal 1/3;</text>
      <biological_entity id="o7322" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="cm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
        <character constraint="at lowest node" constraintid="o7323" is_modifier="false" name="condition_or_size" src="d0_s5" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o7323" name="node" name_original="node" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o7324" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7325" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o7326" from="ascending" name="orientation" src="d0_s5" to="horizontal" />
      </biological_entity>
      <biological_entity id="o7326" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity id="o7327" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
      </biological_entity>
      <relation from="o7323" id="r1177" name="with" negation="false" src="d0_s5" to="o7324" />
    </statement>
    <statement id="d0_s6">
      <text>pedicels smooth or scabrous, with tumid epidermal-cells.</text>
      <biological_entity id="o7328" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7329" name="epidermal-cell" name_original="epidermal-cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="tumid" value_original="tumid" />
      </biological_entity>
      <relation from="o7328" id="r1178" name="with" negation="false" src="d0_s6" to="o7329" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 5-7 (9.5) mm, with (2) 4-5 (7) florets.</text>
      <biological_entity id="o7330" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="9.5" value_original="9.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7331" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="7" value_original="7" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o7330" id="r1179" name="with" negation="false" src="d0_s7" to="o7331" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes rounded over the back, veins obscure, apices acute;</text>
      <biological_entity id="o7332" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character constraint="over back" constraintid="o7333" is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7333" name="back" name_original="back" src="d0_s8" type="structure" />
      <biological_entity id="o7334" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o7335" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 1-2 mm;</text>
      <biological_entity constraint="lower" id="o7336" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 2-3 mm, often borne distinctly above the lower glumes;</text>
      <biological_entity constraint="upper" id="o7337" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7338" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o7337" id="r1180" modifier="often; distinctly" name="above" negation="false" src="d0_s10" to="o7338" />
    </statement>
    <statement id="d0_s11">
      <text>calluses glabrous or hairy;</text>
      <biological_entity id="o7339" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas (3) 3.2-4 (4.5) mm, herbaceous or membranous, glabrous or sparsely hairy on the bases of the veins, backs rounded, 5-veined, veins obscure, not extending to the margins, apical margins smooth or scabrous, not white, apices usually acute, occasionally rounded, irregularly serrate or erose;</text>
      <biological_entity id="o7340" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="on bases" constraintid="o7341" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7341" name="base" name_original="bases" src="d0_s12" type="structure" />
      <biological_entity id="o7342" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o7343" name="back" name_original="backs" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o7344" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o7345" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity constraint="apical" id="o7346" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7347" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s12" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o7341" id="r1181" name="part_of" negation="false" src="d0_s12" to="o7342" />
      <relation from="o7344" id="r1182" name="extending to" negation="false" src="d0_s12" to="o7345" />
    </statement>
    <statement id="d0_s13">
      <text>palea veins glabrous, smooth proximally, scabrous distally;</text>
      <biological_entity constraint="palea" id="o7348" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 0.8-1.2 mm. 2n = 56.</text>
      <biological_entity id="o7349" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7350" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Puccinellia andersonii is a widespread, coastal arctic species. It grows near the tideline and on otherwise barren, reworked marine sediments of eroded flood plains. Its decumbent growth form often gives it an unhealthy appearance. It is unique among Puccinellia species in the Flora region in having blunt, rather than pointed, scabrules in the apical region of its lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;Greenland;Man.;N.W.T.;Nunavut;Que.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>