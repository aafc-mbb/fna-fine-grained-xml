<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="V.L. Marsh ex Soreng" date="unknown" rank="section">Sylvestres</taxon_name>
    <taxon_name authority="Fernald &amp; Wiegand" date="unknown" rank="species">saltuensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section sylvestres;species saltuensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">saltuensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">microlepis</taxon_name>
    <taxon_hierarchy>genus poa;species saltuensis;variety microlepis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">languida</taxon_name>
    <taxon_hierarchy>genus poa;species languida</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Oldpasture bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous, not stoloniferous, loosely tufted.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly pseudointravaginal.</text>
      <biological_entity id="o10080" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="pseudointravaginal" value_original="pseudointravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 20-95 cm tall, 0.8-1.5 mm thick.</text>
      <biological_entity id="o10081" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s3" to="95" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/3-2/3 their length;</text>
      <biological_entity id="o10082" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-3 (4) mm, smooth or sparsely scabrous, truncate to obtuse;</text>
      <biological_entity id="o10083" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-3.6 (6) mm wide, flat, thin, lax, veins prominent.</text>
      <biological_entity id="o10084" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="width" src="d0_s6" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o10085" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-20 (24) cm long, less than 1/4 the plant height, lax;</text>
      <biological_entity id="o10086" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="24" value_original="24" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="1/4" />
      </biological_entity>
      <biological_entity id="o10087" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height" src="d0_s7" value="lax" value_original="lax" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nodes with 1-3 branches;</text>
      <biological_entity id="o10088" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o10089" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o10088" id="r1615" name="with" negation="false" src="d0_s8" to="o10089" />
    </statement>
    <statement id="d0_s9">
      <text>branches ascending to spreading, lax, angled, angles prominent, scabrous.</text>
      <biological_entity id="o10090" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="spreading" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o10091" name="angle" name_original="angles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-5.6 mm, laterally compressed;</text>
      <biological_entity id="o10092" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5.6" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 2-5;</text>
      <biological_entity id="o10093" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes glabrous, usually shorter than 1 mm.</text>
      <biological_entity constraint="rachilla" id="o10094" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character modifier="usually shorter than" name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes 2/3 – 3/4 as long as the adjacent lemmas, distinctly keeled;</text>
      <biological_entity id="o10095" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o10096" from="2/3" name="quantity" src="d0_s13" to="3/4" />
        <character is_modifier="false" modifier="distinctly" name="shape" notes="" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o10096" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o10097" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o10098" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o10099" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o10099" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses webbed;</text>
      <biological_entity id="o10100" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 2.4-4 mm, lanceolate to broadly lanceolate, distinctly keeled, usually glabrous, bases of marginal veins rarely sparsely softly puberulent, lateral-veins prominent, intercostal regions smooth, minutely bumpy, apices obtuse to sharply acute or acuminate;</text>
      <biological_entity id="o10101" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s17" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10102" name="base" name_original="bases" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="rarely sparsely softly" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o10103" name="vein" name_original="veins" src="d0_s17" type="structure" />
      <biological_entity id="o10104" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o10105" name="region" name_original="regions" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s17" value="bumpy" value_original="bumpy" />
      </biological_entity>
      <biological_entity id="o10106" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s17" to="sharply acute or acuminate" />
      </biological_entity>
      <relation from="o10102" id="r1616" name="part_of" negation="false" src="d0_s17" to="o10103" />
    </statement>
    <statement id="d0_s18">
      <text>palea keels scabrous;</text>
      <biological_entity constraint="palea" id="o10107" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s18" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 0.4-1.5 mm.</text>
      <biological_entity id="o10108" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s19" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa saltuensis grows in woodlands of the north-central and northeastern United States and adjacent Canada, extending south to Tennessee. The two subspecies are sometimes treated as species. The variation between the two overlaps and is correlated to some extent with ecology and geography. Poa marcida (p. 512), a western species once included in P. saltuensis, differs in having closed sheaths and attenuate lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wis.;W.Va.;N.H.;N.C.;Tenn.;Pa.;R.I.;N.B.;Nfld. and Labr. (Labr.);N.S.;Ont.;P.E.I.;Que.;Va.;Mass.;Maine;Vt.;Ill.;Ind.;Iowa;Md.;Ohio;Minn.;Mich.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Anthers 0.4-1 mm long; lemma apices obtuse to acute, firm or scarious for up to 0.25 mm</description>
      <determination>Poa saltuensis subsp. languida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Anthers 0.9-1.5 mm long; lemma apices acute to acuminate, scarious for 0.25-0.5  mm</description>
      <determination>Poa saltuensis subsp. saltuensis</determination>
    </key_statement>
  </key>
</bio:treatment>