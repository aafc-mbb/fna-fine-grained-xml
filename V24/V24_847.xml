<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">×gaspensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;species ×gaspensis</taxon_hierarchy>
  </taxon_identification>
  <number>75</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely to loosely tufted, rhizomatous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intra and extravaginal.</text>
      <biological_entity id="o29651" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 15-50 cm, erect or the bases decumbent.</text>
      <biological_entity id="o29652" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="50" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o29653" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/4-1/2 their length, terete;</text>
      <biological_entity id="o29654" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules acute;</text>
      <biological_entity id="o29655" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat, thin, apices broadly prow-shaped.</text>
      <biological_entity id="o29656" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o29657" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles erect, narrowly lanceoloid to ovoid, contracted, with 2-4 branches per node;</text>
      <biological_entity id="o29658" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="narrowly lanceoloid" name="shape" src="d0_s7" to="ovoid" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o29659" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o29660" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o29658" id="r4672" name="with" negation="false" src="d0_s7" to="o29659" />
      <relation from="o29659" id="r4673" name="per" negation="false" src="d0_s7" to="o29660" />
    </statement>
    <statement id="d0_s8">
      <text>branches ascending to spreading, sparsely scabrous.</text>
      <biological_entity id="o29661" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 3.5-6 mm, laterally compressed, with 3-4 florets.</text>
      <biological_entity id="o29662" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o29663" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o29662" id="r4674" name="with" negation="false" src="d0_s9" to="o29663" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes broadly lanceolate, distinctly keeled, distinctly scabrous on the distal 1/3;</text>
      <biological_entity id="o29664" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character constraint="on distal 1/3" constraintid="o29665" is_modifier="false" modifier="distinctly" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o29665" name="1/3" name_original="1/3" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 3-veined;</text>
      <biological_entity constraint="lower" id="o29666" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses shortly webbed;</text>
      <biological_entity id="o29667" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s12" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.5-4.5 mm, broadly lanceolate, keeled, keels and marginal veins long-villous, intercostal regions sofdy puberulent;</text>
      <biological_entity id="o29668" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o29669" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o29670" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o29671" name="region" name_original="regions" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>palea keels scabrous, long-villous at midlength;</text>
      <biological_entity constraint="palea" id="o29672" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character constraint="at midlength" constraintid="o29673" is_modifier="false" name="pubescence" src="d0_s14" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o29673" name="midlength" name_original="midlength" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.2-1.4 mm. 2n = unknown.</text>
      <biological_entity id="o29674" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29675" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Poa ×gaspensis is found in the coastal mountains of the Gaspe Pennisula. There are few plants that fit the description. It seems to consist of hybrids between P. pratensis subsp. alpigena (p. 525) and P. alpina (p. 518). Poa xgaspensis differs from P. alpina in its extravaginal branching, rhizomatous habit, and webbed calluses; from P. pratensis in its acute ligules and more pubescent lemmas; and from P. arctica (p. 529) in its sharply keeled, more scabrous glumes and its spikelet shape, which approaches those of P. alpina and P. pratensis.</discussion>
  <discussion>Poa ×gaspensis is a named intersectional hybrid</discussion>
  
</bio:treatment>