<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">437</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Hook." date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>37</number>
  <other_name type="common_name">Western fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely to loosely ces¬pitose, without rhizomes.</text>
      <biological_entity id="o8355" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8356" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o8355" id="r1357" modifier="loosely" name="without" negation="false" src="d0_s0" to="o8356" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (25) 40-80 (110) cm, glabrous, smooth.</text>
      <biological_entity id="o8357" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths closed for much less than 1/2 their length, glabrous, somewhat persistent or slowly shredding into fibers;</text>
      <biological_entity id="o8358" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o8359" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" modifier="much" name="quantity" src="d0_s2" to="1/2" />
        <character is_modifier="true" name="character" src="d0_s2" value="length" value_original="length" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" modifier="somewhat" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o8358" id="r1358" name="closed for" negation="false" src="d0_s2" to="o8359" />
    </statement>
    <statement id="d0_s3">
      <text>collars glabrous;</text>
      <biological_entity id="o8360" name="collar" name_original="collars" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.4 mm, usually longer at the sides;</text>
      <biological_entity id="o8361" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
        <character constraint="at sides" constraintid="o8362" is_modifier="false" modifier="usually" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8362" name="side" name_original="sides" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blades all alike, 0.3-0.7 mm in diameter, conduplicate, abaxial surfaces smooth or scabridulous, veins (3) 5, ribs 1-5;</text>
      <biological_entity id="o8363" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="alike" value_original="alike" />
        <character char_type="range_value" constraint="in abaxial surfaces" constraintid="o8364" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8364" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="true" name="character" src="d0_s5" value="diameter" value_original="diameter" />
        <character is_modifier="true" name="arrangement_or_vernation" src="d0_s5" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8365" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o8366" name="rib" name_original="ribs" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial sclerenchyma in 5-7 narrow strands, about as wide as the adjacent veins;</text>
      <biological_entity id="o8368" name="strand" name_original="strands" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o8369" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o8367" id="r1359" name="in" negation="false" src="d0_s6" to="o8368" />
      <relation from="o8367" id="r1360" name="as wide as" negation="false" src="d0_s6" to="o8369" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial sclerenchyma absent.</text>
      <biological_entity constraint="abaxial" id="o8367" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (5) 10-20 cm, open, with 1-2 branches per node;</text>
      <biological_entity id="o8370" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o8371" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o8372" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o8370" id="r1361" name="with" negation="false" src="d0_s8" to="o8371" />
      <relation from="o8371" id="r1362" name="per" negation="false" src="d0_s8" to="o8372" />
    </statement>
    <statement id="d0_s9">
      <text>branches 1-15 cm, lax, widely spreading to reflexed, lower branches usually reflexed at maturity, with 2+ spikelets.</text>
      <biological_entity id="o8373" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character char_type="range_value" from="widely spreading" name="orientation" src="d0_s9" to="reflexed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8374" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o8375" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" upper_restricted="false" />
      </biological_entity>
      <relation from="o8374" id="r1363" name="with" negation="false" src="d0_s9" to="o8375" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 6-12 mm, with 3-6 (7) florets.</text>
      <biological_entity id="o8376" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8377" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="7" value_original="7" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <relation from="o8376" id="r1364" name="with" negation="false" src="d0_s10" to="o8377" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes exceeded by the upper florets, ovate to ovatelanceolate, glabrous and smooth or slightly scabrous;</text>
      <biological_entity id="o8378" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="ovatelanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8379" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o8378" id="r1365" name="exceeded by the" negation="false" src="d0_s11" to="o8379" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 2-5 mm;</text>
      <biological_entity constraint="lower" id="o8380" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 3-6 mm;</text>
      <biological_entity constraint="upper" id="o8381" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas (4) 4.5-6.5 (8) mm, ovatelanceolate to attenuate, glabrous or finely puberulent, awns 3-12 mm, usually longer than the lemma bodies;</text>
      <biological_entity id="o8382" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s14" to="attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o8383" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character constraint="than the lemma bodies" constraintid="o8384" is_modifier="false" name="length_or_size" src="d0_s14" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o8384" name="body" name_original="bodies" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>paleas slightly shorter than the lemmas, intercostal region scabrous or puberulent distally;</text>
      <biological_entity id="o8385" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="than the lemmas" constraintid="o8386" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o8386" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity constraint="intercostal" id="o8387" name="region" name_original="region" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers (1) 1.5-2 (3) mm;</text>
      <biological_entity id="o8388" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary apices densely pubescent.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 28 [other numbers have been reported for this species, but probably based on misidentifications].</text>
      <biological_entity constraint="ovary" id="o8389" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8390" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca occidentalis grows in dry to moist, open woodlands, forest openings, and rocky slopes, up to 3100 m. It extends from southern Alaska and northern British Columbia to southwestern Alberta, south to southern California and eastward to Wyoming, and, as a disjunct, around the upper Great Lakes in Ontario, eastern Wisconsin, and Michigan. It is sometimes important as a forage grass, but is usually not sufficiently abundant.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.Dak.;Utah;Wash.;Mont.;Alaska;Mich.;Wis.;Idaho;Alta.;B.C.;Ont.;Calif.;Wyo.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>