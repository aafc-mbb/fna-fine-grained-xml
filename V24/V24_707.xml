<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes unisexual;</text>
    </statement>
    <statement id="d0_s2">
      <text>with or without rhizomes or stolons, densely to loosely tufted or the culms solitary.</text>
      <biological_entity id="o5202" name="rhizom" name_original="rhizomes" src="d0_s2" type="structure" />
      <biological_entity id="o5203" name="stolon" name_original="stolons" src="d0_s2" type="structure" />
      <relation from="o5201" id="r843" name="with or without" negation="false" src="d0_s2" to="o5202" />
      <relation from="o5201" id="r844" name="with or without" negation="false" src="d0_s2" to="o5203" />
    </statement>
    <statement id="d0_s3">
      <text>Basal branching intra and/or extravaginal or pseudointravaginal.</text>
      <biological_entity id="o5201" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s1" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s3" value="extravaginal" value_original="extravaginal" />
        <character is_modifier="false" name="position" src="d0_s3" value="pseudointravaginal" value_original="pseudointravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms spindly to stout, terete or weakly to strongly compressed;</text>
      <biological_entity id="o5204" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form_or_shape" src="d0_s4" value="spindly" value_original="spindly" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character char_type="range_value" from="terete or" name="shape" src="d0_s4" to="weakly strongly compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nodes 0-5, exserted.</text>
      <biological_entity id="o5205" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sheaths terete or weakly to strongly compressed, closed only at the base or up to full length, fusion of the margins not extended by a hyaline membrane, basal sheaths usually glabrous, rarely sparsely retrorsely strigose, hairs about 0.1 mm;</text>
      <biological_entity id="o5206" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character char_type="range_value" from="terete or" name="shape" src="d0_s6" to="weakly strongly compressed" />
        <character constraint="at " constraintid="o5208" is_modifier="false" name="condition" src="d0_s6" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity id="o5207" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o5208" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="up-to-full" value_original="up-to-full" />
        <character is_modifier="true" name="character" src="d0_s6" value="length" value_original="length" />
        <character constraint="by membrane" constraintid="o5209" is_modifier="false" modifier="not" name="size" src="d0_s6" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o5209" name="membrane" name_original="membrane" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5210" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely retrorsely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o5211" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.1-18 mm, thinly membranous and white to milky white or hyaline, truncate to acuminate, entire or erose to lacerate, smooth or ciliolate;</text>
      <biological_entity id="o5212" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="milky white or hyaline" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades flat, folded, or involute, thin to thick, smooth or sparsely to densely scabrous, adaxial surfaces glabrous or hairy, hispidulous or puberulent, apices narrowly to broadly prow-shaped.</text>
      <biological_entity id="o5213" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="involute" value_original="involute" />
        <character char_type="range_value" from="thin" name="width" src="d0_s8" to="thick" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5214" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5215" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s8" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 1-41 cm, erect to nodding or lax, tightly contracted to open, with 1-100+ spikelets;</text>
      <biological_entity id="o5216" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="41" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="nodding" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="tightly" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o5217" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
      <relation from="o5216" id="r845" name="with" negation="false" src="d0_s9" to="o5217" />
    </statement>
    <statement id="d0_s10">
      <text>branches 0.5-20 cm, erect to reflexed, terete or angled, smooth or sparsely to densely scabrous, usually glabrous, rarely hispidulous, with 1 to many spikelets.</text>
      <biological_entity id="o5218" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="20" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s10" to="sparsely densely scabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s10" value="hispidulous" value_original="hispidulous" />
        <character constraint="to spikelets" constraintid="o5219" modifier="with" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5219" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2-12 mm, subterete to strongly laterally compressed, sometimes bulbiferous;</text>
      <biological_entity id="o5220" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s11" to="strongly laterally compressed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets (1) 2-8 (13);</text>
      <biological_entity id="o5221" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character name="atypical_quantity" src="d0_s12" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s12" value="13" value_original="13" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachilla internodes smooth or scabrous, glabrous or pubescent.</text>
      <biological_entity constraint="rachilla" id="o5222" name="internode" name_original="internodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Glumes shorter than to slightly exceeding the adjacent lemmas, weakly to distinctly keeled, smooth or scabrous;</text>
      <biological_entity id="o5223" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character constraint="than to slightly exceeding the adjacent lemmas" constraintid="o5224" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="weakly to distinctly" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5224" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="slightly" name="position_relational" src="d0_s14" value="exceeding" value_original="exceeding" />
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>calluses blunt, usually terete or slightly laterally compressed, sometimes slightly dorsally compressed, glabrous, dorsally webbed, diffusely webbed, or with a crown of hairs;</text>
      <biological_entity id="o5225" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="sometimes slightly dorsally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="dorsally" name="pubescence" src="d0_s15" value="webbed" value_original="webbed" />
        <character is_modifier="false" modifier="diffusely" name="pubescence" src="d0_s15" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o5226" name="crown" name_original="crown" src="d0_s15" type="structure" />
      <biological_entity id="o5227" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <relation from="o5225" id="r846" name="with" negation="false" src="d0_s15" to="o5226" />
      <relation from="o5226" id="r847" name="part_of" negation="false" src="d0_s15" to="o5227" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas 1.7-11 mm, rounded to weakly or distinctly keeled, thinly membranous to chartaceous, glabrous or hairy on the keel and veins, sometimes the intercostal regions also hairy, 5-7 (11) -veined, margins smooth or scabrous, glabrous, apices obtuse to acuminate;</text>
      <biological_entity id="o5228" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s16" to="11" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s16" to="weakly or distinctly keeled" />
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o5230" is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5229" name="keel" name_original="keel" src="d0_s16" type="structure" />
      <biological_entity id="o5230" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity constraint="intercostal" id="o5231" name="region" name_original="regions" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-7(11)-veined" value_original="5-7(11)-veined" />
      </biological_entity>
      <biological_entity id="o5232" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5233" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s16" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>palea keels usually scabrous, infrequently smooth, glabrous or with hairs;</text>
      <biological_entity constraint="palea" id="o5234" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s17" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="infrequently" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o5235" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <relation from="o5234" id="r848" name="with" negation="false" src="d0_s17" to="o5235" />
    </statement>
    <statement id="d0_s18">
      <text>anthers (1-2) 3, 0.1-4.5 (5) mm.</text>
      <biological_entity id="o5236" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s18" to="2" />
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa subg. Poa is the largest subgenus of Poa. Its distribution is essentially the same as that of the genus. It includes all but one of the 70 species of Poa in the Flora region; P. etninens is included in subg. Arctopoa.</discussion>
  
</bio:treatment>