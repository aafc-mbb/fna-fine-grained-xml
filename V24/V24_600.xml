<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Vill." date="unknown" rank="species">glauca</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species glauca</taxon_hierarchy>
  </taxon_identification>
  <number>22</number>
  <other_name type="common_name">Blue fescue</other_name>
  <other_name type="common_name">Gray fescue</other_name>
  <other_name type="common_name">Fétuque glauque</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose, without rhizomes;</text>
      <biological_entity id="o5026" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o5025" id="r819" name="without" negation="false" src="d0_s0" to="o5026" />
    </statement>
    <statement id="d0_s1">
      <text>usually glaucous or pruinose.</text>
      <biological_entity id="o5025" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (15) 22-35 (50) cm, erect, glabrous, smooth.</text>
      <biological_entity id="o5027" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="22" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for about 1/2 their length, pubescent or glabrous, persistent;</text>
      <biological_entity id="o5028" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous;</text>
      <biological_entity id="o5029" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.1-0.4 mm;</text>
      <biological_entity id="o5030" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 0.6-1 mm in diameter, conduplicate, veins 5-7, ribs 1-3 (5), indistinct;</text>
      <biological_entity id="o5031" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in veins" constraintid="o5032" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5032" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
        <character is_modifier="true" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity id="o5033" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character name="atypical_quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial sclerenchyma forming continuous or interrupted bands;</text>
      <biological_entity id="o5035" name="band" name_original="bands" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="continuous" value_original="continuous" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <relation from="o5034" id="r820" name="forming" negation="false" src="d0_s7" to="o5035" />
    </statement>
    <statement id="d0_s8">
      <text>adaxial sclerenchyma absent.</text>
      <biological_entity constraint="abaxial" id="o5034" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 2.5-9 (11) cm, compact, erect, with 1-2 branches per node;</text>
      <biological_entity id="o5036" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="11" value_original="11" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="9" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="compact" value_original="compact" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o5037" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o5038" name="node" name_original="node" src="d0_s9" type="structure" />
      <relation from="o5036" id="r821" name="with" negation="false" src="d0_s9" to="o5037" />
      <relation from="o5037" id="r822" name="per" negation="false" src="d0_s9" to="o5038" />
    </statement>
    <statement id="d0_s10">
      <text>branches stiff, erect, smooth or scabrous, lower branches with 2+ spikelets.</text>
      <biological_entity id="o5039" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5040" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <biological_entity id="o5041" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <relation from="o5040" id="r823" name="with" negation="false" src="d0_s10" to="o5041" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 5.5-9 (11) mm, with 3-6 (7) florets.</text>
      <biological_entity id="o5042" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="11" value_original="11" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5043" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="7" value_original="7" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
      <relation from="o5042" id="r824" name="with" negation="false" src="d0_s11" to="o5043" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes exceeded by the upper florets, ovatelanceolate to ovate, glabrous or pubescent distally;</text>
      <biological_entity id="o5044" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5045" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o5044" id="r825" name="exceeded by" negation="false" src="d0_s12" to="o5045" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes (1.8) 2-3 (4) mm;</text>
      <biological_entity constraint="lower" id="o5046" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="1.8" value_original="1.8" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 2.8-4 (5.1) mm;</text>
      <biological_entity constraint="upper" id="o5047" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="5.1" value_original="5.1" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas (3.5) 4-6 (6.2) mm, lanceolate to ovatelanceolate, smooth or scabrous near the apices, sometimes pubescent distally, awns (0.6) 1-1.5 (2) mm, terminal;</text>
      <biological_entity id="o5048" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s15" to="ovatelanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="smooth" value_original="smooth" />
        <character constraint="near apices" constraintid="o5049" is_modifier="false" name="pubescence" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" notes="" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5049" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o5050" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="0.6" value_original="0.6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas about equal to the lemmas, intercostal region puberulent distally;</text>
      <biological_entity id="o5051" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character constraint="to lemmas" constraintid="o5052" is_modifier="false" name="variability" src="d0_s16" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o5052" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity constraint="intercostal" id="o5053" name="region" name_original="region" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers (1.8) 2-3 mm;</text>
      <biological_entity id="o5054" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="1.8" value_original="1.8" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary apices glabrous.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 42 [in European literature for the horticultural forms].</text>
      <biological_entity constraint="ovary" id="o5055" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5056" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca glauca is widely grown as an ornamental in the Flora region because of its attractive dense tufts of glaucous foliage. It is not known to have escaped cultivation. Several other Eurasian species of fescue with white or bluish foliage are also sold in the horticultural trade as "Festuca glauca". Determining the species involved is beyond the scope of this treatment.</discussion>
  
</bio:treatment>