<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Luerss." date="unknown" rank="subfamily">BAMBUSOIDEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="tribe">BAMBUSEAE</taxon_name>
    <taxon_name authority="Siebold &amp; Zucc." date="unknown" rank="genus">PHYLLOSTACHYS</taxon_name>
    <taxon_name authority="Carriere ex Rivière &amp; C. Rivière" date="unknown" rank="species">aurea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily bambusoideae;tribe bambuseae;genus phyllostachys;species aurea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Fishpole bamboo</other_name>
  <other_name type="common_name">Golden bamboo</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 10 m tall, 1-4 cm thick, straight;</text>
      <biological_entity id="o21494" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="height" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="cm" name="thickness" src="d0_s0" to="4" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>internodes glabrous, initially green, becoming gray, glaucous soon after sheath-fall, some culms in every clump with 1 to several short internodes;</text>
      <biological_entity id="o21495" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="initially" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character constraint="after sheath-fall" constraintid="o21496" is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o21496" name="sheath-fall" name_original="sheath-fall" src="d0_s1" type="structure" />
      <biological_entity id="o21497" name="culm" name_original="culms" src="d0_s1" type="structure" />
      <biological_entity id="o21498" name="clump" name_original="clump" src="d0_s1" type="structure">
        <character constraint="to internodes" constraintid="o21499" modifier="with" name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o21499" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="several" value_original="several" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o21497" id="r3393" name="in" negation="false" src="d0_s1" to="o21498" />
    </statement>
    <statement id="d0_s2">
      <text>nodal ridges moderately prominent;</text>
      <biological_entity constraint="nodal" id="o21500" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath scars not flared, fringed with short, persistent, white hairs.</text>
      <biological_entity constraint="sheath" id="o21501" name="scar" name_original="scars" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flared" value_original="flared" />
        <character constraint="with hairs" constraintid="o21502" is_modifier="false" name="shape" src="d0_s3" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o21502" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culm leaves: sheaths with a basal line of minute white hairs, otherwise glabrous, pale olive-green to rosy-buff, with a sparse scattering of small brown spots and wine-colored or pale green veins, not glaucous;</text>
      <biological_entity constraint="culm" id="o21503" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21504" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="pale olive-green" name="coloration" src="d0_s4" to="rosy-buff" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="wine-colored" value_original="wine-colored" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21505" name="line" name_original="line" src="d0_s4" type="structure" />
      <biological_entity id="o21506" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21507" name="scattering" name_original="scattering" src="d0_s4" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o21508" name="spot" name_original="spots" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="small brown" value_original="small brown" />
      </biological_entity>
      <biological_entity id="o21509" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <relation from="o21504" id="r3394" name="with" negation="false" src="d0_s4" to="o21505" />
      <relation from="o21505" id="r3395" name="part_of" negation="false" src="d0_s4" to="o21506" />
      <relation from="o21504" id="r3396" name="with" negation="false" src="d0_s4" to="o21507" />
      <relation from="o21507" id="r3397" name="part_of" negation="false" src="d0_s4" to="o21508" />
    </statement>
    <statement id="d0_s5">
      <text>auricles and fimbriae absent;</text>
      <biological_entity constraint="culm" id="o21510" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21511" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21512" name="fimbria" name_original="fimbriae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules short, slightly rounded, ciliate;</text>
      <biological_entity constraint="culm" id="o21513" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o21514" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades lanceolate, somewhat crinkled below, upper blades pendulous.</text>
      <biological_entity constraint="culm" id="o21515" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o21516" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="somewhat; below" name="shape" src="d0_s7" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21517" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Foliage leaves: auricles and fimbriae well developed or lacking;</text>
      <biological_entity constraint="foliage" id="o21518" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o21519" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s8" value="developed" value_original="developed" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o21520" name="fimbria" name_original="fimbriae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s8" value="developed" value_original="developed" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules very short, glabrous or sparsely ciliolate;</text>
      <biological_entity constraint="foliage" id="o21521" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o21522" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blades 4-15 cm long, 5-23 mm wide.</text>
      <biological_entity constraint="foliage" id="o21523" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 48.</text>
      <biological_entity id="o21524" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21525" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phyllostachys aurea is native to China, but it is widely cultivated in temperate and subtropical regions. In North America, it grows as far north as Vancouver, British Columbia, in the west and Buffalo, New York, in the east. The young shoots are very palatable, even when raw, but the mature culms are very hard when dried. They are sometimes used for fishpoles. This species differs from other species of Phyllostachys, including those with brighter yellow culms, in having a raised collar below the nodes and irregularly compressed basal culm nodes.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Miss.;Tex.;La.;Calif.;Ala.;Tenn.;N.C.;S.C.;Va.;Ark.;Ga.;Ky.;Fla.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>