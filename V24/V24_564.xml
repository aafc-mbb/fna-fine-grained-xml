<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="(Tzvelev) E.B. Alexeev" date="unknown" rank="subgenus">Subulatae</taxon_name>
    <taxon_name authority="(Tzvelev)" date="unknown" rank="section">Subulatae</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="species">subulata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus subulatae;section subulatae;species subulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Bearded fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, without rhizomes, with short extravaginal tillers.</text>
      <biological_entity id="o15331" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15332" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity constraint="extravaginal" id="o15333" name="tiller" name_original="tillers" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o15331" id="r2445" name="without" negation="false" src="d0_s0" to="o15332" />
      <relation from="o15331" id="r2446" name="with" negation="false" src="d0_s0" to="o15333" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (35) 50-100 (120) cm, erect or decumbent at the base, scabrous.</text>
      <biological_entity id="o15334" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="35" value_original="35" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o15335" is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence_or_relief" notes="" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15335" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths closed for less than 73 their length, glabrous or sparsely pubescent, shredding into fibers;</text>
      <biological_entity id="o15336" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15337" name="fiber" name_original="fibers" src="d0_s2" type="structure" />
      <relation from="o15336" id="r2447" name="shredding into" negation="false" src="d0_s2" to="o15337" />
    </statement>
    <statement id="d0_s3">
      <text>collars glabrous;</text>
      <biological_entity id="o15338" name="collar" name_original="collars" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.6 (1) mm;</text>
      <biological_entity id="o15339" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-10 mm wide, flat or loosely convolute, abaxial and adaxial surfaces scabrous or puberulent, veins 13-29, ribs obscure;</text>
      <biological_entity id="o15340" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s5" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o15341" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o15342" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s5" to="29" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sclerenchyma in narrow abaxial and adaxial strands;</text>
      <biological_entity id="o15343" name="rib" name_original="ribs" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o15344" name="strand" name_original="strands" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o15343" id="r2448" name="in" negation="false" src="d0_s6" to="o15344" />
    </statement>
    <statement id="d0_s7">
      <text>pillars or girders formed at the major veins.</text>
      <biological_entity id="o15345" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 10-40 cm, open, with 1-2 (5) branches per node;</text>
      <biological_entity id="o15346" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o15347" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o15348" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o15346" id="r2449" name="with" negation="false" src="d0_s8" to="o15347" />
      <relation from="o15347" id="r2450" name="per" negation="false" src="d0_s8" to="o15348" />
    </statement>
    <statement id="d0_s9">
      <text>branches lax, usually spreading, sometimes reflexed.</text>
      <biological_entity id="o15349" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 6-12 mm, with (2) 3-5 (6) florets.</text>
      <biological_entity id="o15350" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15351" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="6" value_original="6" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o15350" id="r2451" name="with" negation="false" src="d0_s10" to="o15351" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes sparsely scabrous towards the apices, acuminate to subulate;</text>
      <biological_entity id="o15352" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="towards apices" constraintid="o15353" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="acuminate" name="shape" notes="" src="d0_s11" to="subulate" />
      </biological_entity>
      <biological_entity id="o15353" name="apex" name_original="apices" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes (1.8) 2-3 (4) mm;</text>
      <biological_entity constraint="lower" id="o15354" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="1.8" value_original="1.8" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes (2) 3-6 mm;</text>
      <biological_entity constraint="upper" id="o15355" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses wider than long, glabrous, smooth or slightly scabrous;</text>
      <biological_entity id="o15356" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="wider than long" value_original="wider than long" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 5-9 mm, glabrous, sometimes sparsely scabrous, lanceolate, apices entire, acute to acuminate, awned, awns (2.5) 5-15 (20) mm, terminal, straight, sometimes curved or kinked;</text>
      <biological_entity id="o15357" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o15358" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15359" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s15" value="curved" value_original="curved" />
        <character name="course" src="d0_s15" value="kinked" value_original="kinked" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas about as long as or slightly longer than the lemmas, intercostal region puberulent distally;</text>
      <biological_entity id="o15360" name="palea" name_original="paleas" src="d0_s16" type="structure" />
      <biological_entity id="o15362" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o15361" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o15363" name="region" name_original="region" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o15360" id="r2452" name="as long as" negation="false" src="d0_s16" to="o15362" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.5-2.5 (3) mm;</text>
      <biological_entity id="o15364" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary apices pubescent.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 28.</text>
      <biological_entity constraint="ovary" id="o15365" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15366" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca subulata grows on stream banks and in open woods, meadows, shady forests, and thickets, to about 2800 m. Its range extends from the southern Alaska panhandle eastward to southwestern Alberta and western South Dakota, and southward to central California and Colorado.</discussion>
  <discussion>Festuca subulata differs from F. subuliflora (p. 406) in having blunter, glabrous calluses and glabrous, often scabrous or puberulent leaf blades that are obscurely ribbed.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Wash.;Utah;Alta.;B.C.;Idaho;Alaska;Mont.;Wyo.;S.Dak.;Calif.;Nev.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>