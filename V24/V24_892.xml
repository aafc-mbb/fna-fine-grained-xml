<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">626</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">DESCHAMPSIA</taxon_name>
    <taxon_name authority="(L.) P. Beauv." date="unknown" rank="species">cespitosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus deschampsia;species cespitosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Deschampsia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">genuina</taxon_name>
    <taxon_hierarchy>genus deschampsia;species caespitosa;variety genuina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Deschampsia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">arctica</taxon_name>
    <taxon_hierarchy>genus deschampsia;species caespitosa;variety arctica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Deschampsia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_hierarchy>genus deschampsia;species caespitosa</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely to tightly cespitose.</text>
      <biological_entity id="o23594" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely to tightly" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (7) 35-150 cm, erect, not rooting at the lower nodes.</text>
      <biological_entity id="o23595" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at lower nodes" constraintid="o23596" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23596" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, sometimes forming a dense 10-35 cm tuft;</text>
      <biological_entity id="o23597" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23598" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o23599" name="tuft" name_original="tuft" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character char_type="range_value" from="10" from_unit="cm" is_modifier="true" modifier="sometimes" name="some_measurement" src="d0_s3" to="35" to_unit="cm" />
      </biological_entity>
      <relation from="o23597" id="r3750" modifier="sometimes" name="forming a" negation="false" src="d0_s3" to="o23599" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o23600" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2-13 mm, scarious, decurrent, obtuse to acute;</text>
      <biological_entity id="o23601" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-30 cm long, usually at least some flat and 1-4 mm wide, the remainder folded or rolled and 0.5-1 mm in diameter, adaxial surfaces with 5-11 prominent ribs, ribs usually all papillose, scabridulous, or scabrous, sometimes puberulent, outer ribs sometimes more strongly so than the inner ribs.</text>
      <biological_entity id="o23602" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually at-least; at-least" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rolled" value_original="rolled" />
        <character char_type="range_value" constraint="in adaxial surfaces" constraintid="o23603" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23603" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
      <biological_entity id="o23604" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="11" />
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o23605" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="outer" id="o23606" name="rib" name_original="ribs" src="d0_s6" type="structure" />
      <biological_entity constraint="inner" id="o23607" name="rib" name_original="ribs" src="d0_s6" type="structure" />
      <relation from="o23603" id="r3751" name="with" negation="false" src="d0_s6" to="o23604" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 8-30 (40) cm, 4-30 cm wide, usually open and pyramidal, sometimes contracted and ovate;</text>
      <biological_entity id="o23608" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s7" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" modifier="sometimes" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches straight to slightly flexuous, usually strongly divergent, sometimes strongly ascending, lower branches often scabridulous or scabrous, particularly distally, with not or only moderately imbricate spikelets.</text>
      <biological_entity id="o23609" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s8" to="slightly flexuous" />
        <character is_modifier="false" modifier="usually strongly" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="sometimes strongly" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23610" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o23611" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="only moderately" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 2.5-7.6 mm, ovate to V-shaped, laterally compressed, usually bisexual, sometimes viviparous, bisexual spikelets usually with 2 (3) florets, rarely with 1.</text>
      <biological_entity id="o23612" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7.6" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="v-shaped" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s9" value="viviparous" value_original="viviparous" />
      </biological_entity>
      <biological_entity id="o23613" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o23614" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="3" value_original="3" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o23613" id="r3752" modifier="rarely" name="with" negation="false" src="d0_s9" to="o23614" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes lanceolate, acute;</text>
      <biological_entity id="o23615" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 2.7-7 mm, entire, 1-3-veined, midvein sometimes scabridulous, at least distally;</text>
      <biological_entity constraint="lower" id="o23616" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o23617" name="midvein" name_original="midvein" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 2-7.5 mm, 1-3-veined, lanceolate, midvein smooth or wholly or partly scabridulous;</text>
      <biological_entity constraint="upper" id="o23618" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o23619" name="midvein" name_original="midvein" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="wholly" name="relief" src="d0_s12" value="or" value_original="or" />
        <character is_modifier="false" modifier="partly" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 0.2-2.3 mm;</text>
      <biological_entity constraint="callus" id="o23620" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2-5 (7) mm, smooth, shiny, glabrous, usually purple over less than 1/2 their surface, purple or green proximally, if green, often with a purple band about midlength, usually green or pale distally, usually awned, awns (0.5) 1-8 mm, attached from near the base to about midlength, straight or geniculate, sometimes exceeding the glumes;</text>
      <biological_entity id="o23621" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="over surface" constraintid="o23622" is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s14" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23622" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s14" to="1/2" />
      </biological_entity>
      <biological_entity id="o23623" name="band" name_original="band" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="position" src="d0_s14" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o23624" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character constraint="from " constraintid="o23625" is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="course" notes="" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o23625" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o23626" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <relation from="o23621" id="r3753" modifier="often" name="with" negation="false" src="d0_s14" to="o23623" />
      <relation from="o23624" id="r3754" modifier="sometimes" name="exceeding the" negation="false" src="d0_s14" to="o23626" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-3 mm.</text>
      <biological_entity id="o23627" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 0.5-1 mm. 2n = 18, 24, 25, 26-28, about 39, 52.</text>
      <biological_entity constraint="2n" id="o23629" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit=",,,,about ," value="18" value_original="18" />
        <character name="quantity" src="d0_s16" unit=",,,,about ," value="24" value_original="24" />
        <character name="quantity" src="d0_s16" unit=",,,,about ," value="25" value_original="25" />
        <character char_type="range_value" from="26" name="quantity" src="d0_s16" to="28" unit=",,,,about ," />
        <character name="quantity" src="d0_s16" unit=",,,,about ," value="39" value_original="39" />
        <character name="quantity" src="d0_s16" unit=",,,,about ," value="52" value_original="52" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>The voucher specimens for these counts have not been examined.</text>
      <biological_entity id="o23628" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23630" name="count" name_original="counts" src="d0_s17" type="structure" />
      <relation from="o23628" id="r3755" modifier="not" name="for" negation="false" src="d0_s17" to="o23630" />
    </statement>
  </description>
  <discussion>Deschampsia cespitosa is circumboreal in the Northern Hemisphere, and also grows in New Zealand and Australia. It is an attractive taxon that grows in wet meadows and bogs, and along streams and lakes, from sea level to over 3000 m in cool-temperate, but not arctic, habitats.</discussion>
  <discussion>There are widely varying opinions concerning the taxonomic treatment of Deschampsia cespitosa. Tsvelev, Aiken, Murray, and Elven (per Murray, pers. com. 2005) recommend a narrow circumscription, and consider D. cespitosa to be introduced and mostly ruderal in regions other than Europe and western Siberia. Chiapella and Probatova (2003) adopted a much broader interpretation of D. cespitosa, treating many of the species recognized in, for example, Tsvelev (1995) as subspecies. There have been no interdisplinary, global studies of the complex. The circumscription adopted here is narrower than has been customary in North America. Some of the distribution records shown, particularly those from the northern part of the region, may reflect the broad interpretation of the species.</discussion>
  <discussion>Lawrence (1945) demonstrated that, in western North America, Deschampsia cespitosa exhibits both ecotypic differentiation and a high degree of plasticity. The following three subspecies intergrade.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Va.;Wis.;W.Va.;Conn.;Mass.;Maine;N.H.;N.Y.;R.I.;Vt.;Wyo.;N.J.;N.Mex.;N.C.;Pa.;Calif.;Nev.;Colo.;Alaska;Ill.;Ind.;Ariz.;Idaho;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Nunavut;Ont.;P.E.I.;Que.;Sask.;Yukon;Md.;Ohio;Utah;Minn.;Mich.;N.Dak.;S.Dak.;Mont.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicles contracted at anthesis, the branches appressed to ascending; glumes 4.5-5.8 mm long, midvein of the lower glumes scabrous distally</description>
      <determination>Deschampsia cespitosa subsp. holciformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicles open at anthesis, the branches strongly divergent to drooping; glumes 2-7.5 mm long; midvein of the lower glumes smooth or scabridulous distally.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants often glaucous; glumes 4.4-7.5 mm long; awns usually exceeding the lemmas; plants of the northwest coast of North America</description>
      <determination>Deschampsia cespitosa subsp. beringensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants not glaucous; glumes 2-6 mm long; awns exceeded by or exceeding the lemmas; plants widespread in North America</description>
      <determination>Deschampsia cespitosa subsp. cespitosa</determination>
    </key_statement>
  </key>
</bio:treatment>