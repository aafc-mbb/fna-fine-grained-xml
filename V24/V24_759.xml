<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely to loosely tufted or with solitary shoots, sometimes stoloniferous, sometimes rhizomatous.</text>
      <biological_entity id="o14895" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intra and/or extravaginal.</text>
      <biological_entity id="o14894" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="with solitary shoots" value_original="with solitary shoots" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms (5) 10-125 cm, terete or weakly compressed;</text>
      <biological_entity id="o14896" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="125" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete or slightly compressed.</text>
      <biological_entity id="o14897" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed from 1/7 their length to their entire length, terete to compressed, smooth or scabrous, glabrous or pubescent;</text>
      <biological_entity id="o14898" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s5" to="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-18 mm, milky white or colorless, usually translucent, truncate to acuminate, glabrous or ciliolate;</text>
      <biological_entity id="o14899" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="milky white" value_original="milky white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="colorless" value_original="colorless" />
        <character is_modifier="false" modifier="usually" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>innovation blades with the adaxial surfaces usually moderately to densely scabrous or hispidulous on and between the veins, sometimes smooth and glabrous;</text>
      <biological_entity constraint="innovation" id="o14900" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" notes="" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14901" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character constraint="on and between veins" constraintid="o14902" is_modifier="false" name="pubescence" src="d0_s7" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o14902" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <relation from="o14900" id="r2370" name="with" negation="false" src="d0_s7" to="o14901" />
    </statement>
    <statement id="d0_s8">
      <text>cauline blades flat, folded, or involute, thin or thick, lax or straight, smooth or scabrous, adaxial surfaces sometimes hairy, apices narrowly to broadly prow-shaped.</text>
      <biological_entity constraint="cauline" id="o14903" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14904" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14905" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s8" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 1-29 cm, contracted to open, usually with fewer than 100 spikelets;</text>
      <biological_entity id="o14906" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="29" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o14907" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
      <relation from="o14906" id="r2371" modifier="usually" name="with" negation="false" src="d0_s9" to="o14907" />
    </statement>
    <statement id="d0_s10">
      <text>nodes with 1-5 branches;</text>
      <biological_entity id="o14908" name="node" name_original="nodes" src="d0_s10" type="structure" />
      <biological_entity id="o14909" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o14908" id="r2372" name="with" negation="false" src="d0_s10" to="o14909" />
    </statement>
    <statement id="d0_s11">
      <text>branches 0.5-18 cm, terete or angled, smooth or scabrous, glabrous or hispidulous.</text>
      <biological_entity id="o14910" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s11" to="18" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angled" value_original="angled" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3-17 mm, lengths 3.5 times widths, laterally compressed, not sexually dimorphic, not bulbiferous;</text>
      <biological_entity id="o14911" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="3.5" value_original="3.5" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-10 (13) mm, normal;</text>
      <biological_entity id="o14912" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="13" value_original="13" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s13" value="normal" value_original="normal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes smooth or scabrous, glabrous or hairy.</text>
      <biological_entity constraint="rachilla" id="o14913" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes distinctly keeled, keels smooth or scabrous;</text>
      <biological_entity id="o14914" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o14915" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 1, 3 (or 5) -veined;</text>
      <biological_entity constraint="lower" id="o14916" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes 3-veined or 5-veined;</text>
      <biological_entity constraint="upper" id="o14917" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calluses terete or slightly laterally compressed, glabrous, webbed, or with a crown of hairs;</text>
      <biological_entity id="o14918" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly laterally" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o14919" name="crown" name_original="crown" src="d0_s18" type="structure" />
      <biological_entity id="o14920" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <relation from="o14918" id="r2373" name="with" negation="false" src="d0_s18" to="o14919" />
      <relation from="o14919" id="r2374" name="part_of" negation="false" src="d0_s18" to="o14920" />
    </statement>
    <statement id="d0_s19">
      <text>lemmas 2.6-11 mm, lanceolate, distinctly keeled, keels, veins, and intercostal regions glabrous or hairy, 5-7 (11) -veined;</text>
      <biological_entity id="o14921" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s19" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o14922" name="keel" name_original="keels" src="d0_s19" type="structure" />
      <biological_entity id="o14923" name="vein" name_original="veins" src="d0_s19" type="structure" />
      <biological_entity constraint="intercostal" id="o14924" name="region" name_original="regions" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="5-7(11)-veined" value_original="5-7(11)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>palea keels scabrous, glabrous or with hairs at midlength;</text>
      <biological_entity constraint="palea" id="o14925" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o14926" name="hair" name_original="hairs" src="d0_s20" type="structure" />
      <biological_entity id="o14927" name="midlength" name_original="midlength" src="d0_s20" type="structure" />
      <relation from="o14925" id="r2375" name="with" negation="false" src="d0_s20" to="o14926" />
      <relation from="o14926" id="r2376" name="at" negation="false" src="d0_s20" to="o14927" />
    </statement>
    <statement id="d0_s21">
      <text>anthers 3, vestigial (0.1-0.2 mm) or 1.3-4.5 (5) mm.</text>
      <biological_entity id="o14928" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
        <character is_modifier="false" name="prominence" src="d0_s21" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s21" value="1.3-4.5(5) mm" value_original="1.3-4.5(5) mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa sect. Madropoa is confined to North America. Its 20 species exhibit breeding systems ranging from sequential gynomonoecy to gynodioecy and dioecy. The gynomonoecious species usually grow in forests and have broad, flat leaves. The gynomonoecious and dioecious species grow mainly in more open habitats. They have normally developed anthers that are 1.3-4 mm long, and involute innovation blades that, in several species, are densely scabrous or hairy on the adaxial surfaces.</discussion>
  <discussion>There are two subsections in the Flora region: subsects. Madropoa and Epiles.</discussion>
  
</bio:treatment>