<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">735</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AVENA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">fatua</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus avena;species fatua</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Avena</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fatua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glahrescens</taxon_name>
    <taxon_hierarchy>genus avena;species fatua;variety glahrescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Avena</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fatua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glabrata</taxon_name>
    <taxon_hierarchy>genus avena;species fatua;variety glabrata</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Wild oats</other_name>
  <other_name type="common_name">Folle avoine</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o19307" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 8-160 cm, prostrate to erect when young, becoming erect at maturity.</text>
      <biological_entity id="o19308" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="160" to_unit="cm" />
        <character char_type="range_value" from="prostrate" modifier="when young" name="orientation" src="d0_s1" to="erect" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths of the basal leaves with scattered hairs, upper sheaths glabrous;</text>
      <biological_entity id="o19309" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o19310" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19311" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity constraint="upper" id="o19312" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19309" id="r3062" name="part_of" negation="false" src="d0_s2" to="o19310" />
      <relation from="o19309" id="r3063" name="with" negation="false" src="d0_s2" to="o19311" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 4-6 mm, acute;</text>
      <biological_entity id="o19313" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10-45 cm long, 3-15 mm wide, scabridulous.</text>
      <biological_entity id="o19314" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 7-40 cm long, 5-20 cm wide, nodding.</text>
      <biological_entity id="o19315" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 18-32 mm, with 2 (3) florets;</text>
      <biological_entity id="o19317" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <relation from="o19316" id="r3064" name="with" negation="false" src="d0_s6" to="o19317" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation beneath each floret;</text>
      <biological_entity id="o19318" name="floret" name_original="floret" src="d0_s7" type="structure" />
      <relation from="o19316" id="r3065" name="beneath" negation="false" src="d0_s7" to="o19318" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation scars of all florets round to ovate or triangular.</text>
      <biological_entity id="o19316" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s6" to="32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19319" name="scar" name_original="scars" src="d0_s8" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s8" to="ovate or triangular" />
      </biological_entity>
      <biological_entity id="o19320" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o19319" id="r3066" name="part_of" negation="false" src="d0_s8" to="o19320" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 18-32 mm, 9-11-veined;</text>
      <biological_entity id="o19321" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s9" to="32" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="9-11-veined" value_original="9-11-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses bearded, hairs to 1/4 the length of the lemmas;</text>
      <biological_entity id="o19322" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o19323" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="1/4" />
      </biological_entity>
      <biological_entity id="o19324" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 14-22 mm, usually densely strigose below midlength, sometimes sparsely strigose or glabrous, veins not extending beyond the apices, apices usually bifid, teeth 0.3-1.5 mm, awns 23-42 mm, arising in the middle 1/3 of the lemmas;</text>
      <biological_entity id="o19325" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
        <character constraint="below midlength veins" constraintid="o19326" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o19326" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s11" value="midlength" value_original="midlength" />
        <character is_modifier="true" modifier="sometimes sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="true" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19327" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o19328" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s11" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o19329" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19330" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s11" to="42" to_unit="mm" />
        <character constraint="in middle" constraintid="o19331" is_modifier="false" name="orientation" src="d0_s11" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o19331" name="middle" name_original="middle" src="d0_s11" type="structure">
        <character constraint="of lemmas" constraintid="o19332" name="quantity" src="d0_s11" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o19332" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <relation from="o19326" id="r3067" name="extending beyond the" negation="false" src="d0_s11" to="o19327" />
    </statement>
    <statement id="d0_s12">
      <text>lodicules without lobes on the wings;</text>
      <biological_entity id="o19333" name="lodicule" name_original="lodicules" src="d0_s12" type="structure" />
      <biological_entity id="o19334" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <biological_entity id="o19335" name="wing" name_original="wings" src="d0_s12" type="structure" />
      <relation from="o19333" id="r3068" name="without" negation="false" src="d0_s12" to="o19334" />
      <relation from="o19334" id="r3069" name="on" negation="false" src="d0_s12" to="o19335" />
    </statement>
    <statement id="d0_s13">
      <text>anthers about 3 mm. 2n = 42.</text>
      <biological_entity id="o19336" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19337" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Avena fatua is native to Europe and central Asia. It is known as a weed in most temperate regions of the world; it is considered a noxious weed in some parts of Canada and the United States.</discussion>
  <discussion>Avena fatua is sometimes confused with A. occidentalis, but differs in having shorter, wider spikelets, fewer florets, and a distal floret which does not have a heart-shaped disarticulation scar. Hybrids between A. fatua and A. sativa are common in plantings of cultivated oats. The hybrids resemble A. sativa, but differ in having the fatua-type lodicule; some also have a weak awn on the first lemma. They are easily confused with fatuoid forms of A. sativa.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;W.Va.;Del.;Wis.;Idaho;Mont.;Oreg.;Wyo.;Pacific Islands (Hawaii);Fla.;N.H.;N.Mex.;Tex.;La.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Tenn.;Pa.;Calif.;Nev.;Va.;Colo.;Alaska;Ala.;Vt.;Ill.;Ind.;Iowa;Okla.;Ariz.;N.Dak.;Nebr.;S.Dak.;Maine;Md.;Mass.;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>