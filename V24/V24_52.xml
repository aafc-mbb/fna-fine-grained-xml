<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ZIZANIA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">palustris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus zizania;species palustris</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o15826" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 3 m, erect, usually at least partly immersed.</text>
      <biological_entity id="o15827" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="3" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually at-least partly" name="prominence" src="d0_s1" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or with scattered hairs;</text>
      <biological_entity id="o15828" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="with scattered hairs" value_original="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o15829" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o15828" id="r2532" name="with" negation="false" src="d0_s2" to="o15829" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 3-16 mm, upper ligules trun¬cate, lanceolate or triangular, erose;</text>
      <biological_entity id="o15830" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o15831" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s3" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 20-60 cm long, 3-21 (40+) mm wide, glabrous, margins glabrate or scabrous.</text>
      <biological_entity id="o15832" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="60" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="21" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15833" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 24-60 cm long, 1-20 (40) cm wide;</text>
      <biological_entity id="o15834" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="24" from_unit="cm" name="length" src="d0_s5" to="60" to_unit="cm" />
        <character name="width" src="d0_s5" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches unisexual.</text>
      <biological_entity id="o15835" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate branches ascending or divergent;</text>
      <biological_entity id="o15836" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel apices 0.2-0.4 mm wide.</text>
      <biological_entity constraint="pedicel" id="o15837" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate spikelets 6-17 mm, lanceolate, acuminate or awned, awns to 2 mm.</text>
      <biological_entity id="o15838" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15839" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate branches mostly appressed or ascending, a few sometimes divergent;</text>
      <biological_entity id="o15840" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel apices 0.7-1.2 mm wide.</text>
      <biological_entity constraint="pedicel" id="o15841" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate spikelets 8-33 mm long, 1-2.6 mm wide, lanceolate or oblong, coriaceous or indurate, lustrous, glabrous or with lines of short hairs, apices usually hirsute and abruptly narrowed, awned, awns to 10 cm;</text>
      <biological_entity id="o15842" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="33" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="texture" src="d0_s12" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with lines" value_original="with lines" />
      </biological_entity>
      <biological_entity id="o15843" name="line" name_original="lines" src="d0_s12" type="structure" />
      <biological_entity id="o15844" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o15845" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s12" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15846" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s12" to="10" to_unit="cm" />
      </biological_entity>
      <relation from="o15842" id="r2533" name="with" negation="false" src="d0_s12" to="o15843" />
      <relation from="o15843" id="r2534" name="part_of" negation="false" src="d0_s12" to="o15844" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas and paleas remaining clasped at maturity;</text>
    </statement>
    <statement id="d0_s14">
      <text>aborted pistillate spikelets 0.6-2.6 mm wide.</text>
      <biological_entity id="o15847" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15848" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15849" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o15847" id="r2535" name="aborted" negation="false" src="d0_s14" to="o15849" />
      <relation from="o15848" id="r2536" name="aborted" negation="false" src="d0_s14" to="o15849" />
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 6-30 mm long, 0.6-2 mm wide.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 30.</text>
      <biological_entity id="o15850" name="caryopse" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s15" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15851" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Zizania palustris grows mostly to the north of Z. aquatica, but the two species overlap in the Great Lakes region, eastern Canada, and New England. It is cultivated as a crop in some provinces and states, with California being the largest producer. All records from the western part of the Flora region reflect deliberate plantings; none are known to have persisted. In cultivated strains, the pistillate spikelets remain on the plant at maturity.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Wis.;Ala.;Ark.;Ill.;Kans.;Md.;Mo.;N.H.;N.C.;N.Dak.;Iowa;Nebr.;Pa.;R.I.;Colo.;Calif.;Ariz.;Idaho;Ohio;S.Dak.;W.Va.;Vt.;Ind.;Maine;Mass.;Minn.;Mich.;Mont.;Oreg.;Tenn.;Ky.;B.C.;Man.;N.B.;N.S.;Ont.;P.E.I.;Que.;Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower pistillate branches with 9-30 spikelets; pistillate part of the inflorescence 10-40 cm or more wide, the branches ascending to widely divergent; plants 1-3 m tall; blades 10-40+ mm wide</description>
      <determination>Zizania palustris var. interior</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower pistillate branches with 2-8 spikelets; pistillate part of the inflorescence 1-8(15) cm wide, the branches appressed or ascending, or a few branches somewhat divergent; plants to 2 m tall; blades 3-21 mm wide</description>
      <determination>Zizania palustris var. palustris</determination>
    </key_statement>
  </key>
</bio:treatment>