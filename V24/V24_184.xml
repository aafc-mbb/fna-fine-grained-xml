<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">137</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(C.L. Hitchc. &amp; Spellenb.) Barkworth" date="unknown" rank="species">swallenii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species swallenii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oryzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">swallenii</taxon_name>
    <taxon_hierarchy>genus oryzopsis;species swallenii</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Swallen's needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tightly cespitose, not rhizomatous.</text>
      <biological_entity id="o28055" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="tightly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-25 cm tall, 0.5-1 mm thick, glabrous;</text>
      <biological_entity id="o28056" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="height" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 2-3.</text>
      <biological_entity id="o28057" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths mostly glabrous, pubescent at the base, throats glabrous;</text>
      <biological_entity constraint="basal" id="o28058" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o28059" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28059" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o28060" name="throat" name_original="throats" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars, including the sides, glabrous;</text>
      <biological_entity id="o28061" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28062" name="side" name_original="sides" src="d0_s4" type="structure" />
      <relation from="o28061" id="r4411" name="including the" negation="false" src="d0_s4" to="o28062" />
    </statement>
    <statement id="d0_s5">
      <text>basal ligules 0.2-0.3 mm, obtuse to rounded, glabrous;</text>
      <biological_entity constraint="basal" id="o28063" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper ligules to 0.5 mm, rounded to broadly acute;</text>
      <biological_entity constraint="upper" id="o28064" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="broadly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 0.4-0.7 mm wide, arcuate, abaxial surfaces smooth or scabridulous, adaxial surfaces with hairs shorter than 0.5 mm.</text>
      <biological_entity id="o28065" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="course_or_shape" src="d0_s7" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28066" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28067" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o28068" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <relation from="o28067" id="r4412" name="with" negation="false" src="d0_s7" to="o28068" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 3-6.5 cm long, 0.3-0.7 cm wide;</text>
      <biological_entity id="o28069" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s8" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches appressed, lower branches 1-5 cm, with 1-5 spikelets.</text>
      <biological_entity id="o28070" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28071" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28072" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o28071" id="r4413" name="with" negation="false" src="d0_s9" to="o28072" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes subequal, 4-5.5 mm, not saccate, apices narrowly acute to acuminate, midveins often prolonged into an awnlike tip;</text>
      <biological_entity id="o28073" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o28074" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <biological_entity id="o28075" name="midvein" name_original="midveins" src="d0_s10" type="structure">
        <character constraint="into tip" constraintid="o28076" is_modifier="false" modifier="often" name="length" src="d0_s10" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o28076" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="awnlike" value_original="awnlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 0.6-1 mm wide, apices narrowly acute;</text>
      <biological_entity constraint="lower" id="o28077" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28078" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets 2.5-3.5 mm long, 0.6-0.8 mm thick, fusiform, terete;</text>
      <biological_entity id="o28079" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s12" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="thickness" src="d0_s12" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 0.1-0.2 mm, blunt;</text>
      <biological_entity id="o28080" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas evenly hairy, hairs 0.3-0.5 mm, all similar in length, apical lobes 0.3-0.5 mm, thickly membranous;</text>
      <biological_entity id="o28081" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o28082" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o28083" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="thickly" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 5-6 mm, once-geniculate, readily deciduous, basal segment scabridulous;</text>
      <biological_entity id="o28084" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="once-geniculate" value_original="once-geniculate" />
        <character is_modifier="false" modifier="readily" name="duration" src="d0_s15" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28085" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2.2-2.5 mm, slightly shorter than the lemmas;</text>
      <biological_entity id="o28086" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character constraint="than the lemmas" constraintid="o28087" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o28087" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.5-2 mm, dehiscent, not penicillate.</text>
      <biological_entity id="o28088" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 2-3 mm, ovoid.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 34.</text>
      <biological_entity id="o28089" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28090" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Achnatherum swallenii grows on open, rocky sites, frequently with low sagebrush, in Idaho and western Wyoming, at 1500-2200 m. It is a dominant species in parts of eastern Idaho, although it is poorly represented in collections.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho;Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>