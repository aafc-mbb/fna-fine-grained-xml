<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">pumpellianus</taxon_name>
    <taxon_name authority="W. W. Mitch. &amp; Wilton" date="unknown" rank="subspecies">dicksonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species pumpellianus;subspecies dicksonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, sometimes shortly rhizomatous.</text>
      <biological_entity id="o23183" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes shortly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 65-135 cm, ascending, often geniculate;</text>
      <biological_entity id="o23184" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="65" from_unit="cm" name="some_measurement" src="d0_s1" to="135" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 3-7, glabrous or pubescent;</text>
      <biological_entity id="o23185" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous.</text>
      <biological_entity id="o23186" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or hairy;</text>
      <biological_entity id="o23187" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles sometimes present;</text>
      <biological_entity id="o23188" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-4 mm;</text>
      <biological_entity id="o23189" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 7-30 cm long, 2.5-8.5 mm wide, glabrous or hairy on both surfaces, sometimes only the adaxial surface hairy.</text>
      <biological_entity id="o23190" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o23191" is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23191" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o23192" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 10-24 cm, usually open, nodding to partially erect;</text>
      <biological_entity id="o23193" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="24" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character char_type="range_value" from="nodding" name="orientation" src="d0_s8" to="partially erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches spreading to drooping.</text>
      <biological_entity id="o23194" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s9" to="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 16-32 (45) mm.</text>
      <biological_entity id="o23195" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="45" value_original="45" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes glabrous or hairy;</text>
      <biological_entity id="o23196" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 5-10 mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o23197" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 7.5-13 mm, 3-veined;</text>
      <biological_entity constraint="upper" id="o23198" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 9-16 mm, sparsely to densely hairy throughout or along the marginal vein and keel;</text>
      <biological_entity id="o23199" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely; throughout" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="along the marginal vein" value_original="along the marginal vein" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o23200" name="vein" name_original="vein" src="d0_s14" type="structure" />
      <biological_entity id="o23201" name="keel" name_original="keel" src="d0_s14" type="structure" />
      <relation from="o23199" id="r3688" name="along" negation="false" src="d0_s14" to="o23200" />
    </statement>
    <statement id="d0_s15">
      <text>awns 2.5-7.5 mm;</text>
      <biological_entity id="o23202" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers (3.5) 4-6 (6.8) mm.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 28.</text>
      <biological_entity id="o23203" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23204" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus pumpellianus subsp. dicksonii grows in shallow, rocky soils of river banks and bluffs in the Yukon River drainage of Alaska. Apart from the more restricted distribution, it is not clear how this subspecies differs from the introduced B. riparius.</discussion>
  
</bio:treatment>