<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">552</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="subsection">Madropoa</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">confinis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection madropoa;species confinis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>37</number>
  <other_name type="common_name">Coastal bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely to loosely tufted, rhizomatous and stoloniferous, rhizomes and stolons to 1 m, slender.</text>
      <biological_entity id="o1103" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1104" name="whole-organism" name_original="" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly intravaginal, some extravaginal.</text>
      <biological_entity id="o1105" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
      <biological_entity id="o1106" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 7-30 (35) cm tall, 0.4-0.9 mm thick, slender, erect or the bases decumbent, terete or weakly compressed;</text>
      <biological_entity id="o1107" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="height" src="d0_s3" unit="cm" value="35" value_original="35" />
        <character char_type="range_value" from="7" from_unit="cm" name="height" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s3" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o1108" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, 0-1 exserted.</text>
      <biological_entity id="o1109" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="1" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/3-2/3 their length, terete, smooth, glabrous, bases of basal sheaths glabrous, distal sheath lengths (1) 1.4-4.5 times blade lengths;</text>
      <biological_entity id="o1110" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1111" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1112" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1113" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o1114" is_modifier="false" name="length" src="d0_s5" value="(1)1.4-4.5 times blade lengths" value_original="(1)1.4-4.5 times blade lengths" />
      </biological_entity>
      <biological_entity id="o1114" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o1111" id="r173" name="part_of" negation="false" src="d0_s5" to="o1112" />
    </statement>
    <statement id="d0_s6">
      <text>collars smooth, glabrous;</text>
      <biological_entity id="o1115" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.5-1.5 (2.2) mm, scabrous, truncate to acute;</text>
      <biological_entity id="o1116" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>innovation blades adaxially moderately to densely scabrous or hispidulous on and between the veins;</text>
      <biological_entity constraint="innovation" id="o1117" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character constraint="on and between veins" constraintid="o1118" is_modifier="false" name="pubescence" src="d0_s8" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o1118" name="vein" name_original="veins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>cauline blades slightly reduced in length distally, 0.5-1 (1.5) mm wide, involute, thin to moderately thick, usually filiform, soft, abaxial surfaces smooth, adaxial surfaces sparsely scabrous on and between the veins, apices narrowly prow-shaped, flag leaf-blades (0.5) 1-5 cm.</text>
      <biological_entity constraint="cauline" id="o1119" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="length" src="d0_s9" value="reduced" value_original="reduced" />
        <character name="width" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
        <character char_type="range_value" from="thin" name="width" src="d0_s9" to="moderately thick" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1120" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1121" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character constraint="on and between veins" constraintid="o1122" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1122" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o1123" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o1124" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character name="atypical_distance" src="d0_s9" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles 1-5 (7) cm, erect, ovoid, fairly tightly to loosely contracted, congested or moderately congested, with fewer than 50 spikelets;</text>
      <biological_entity id="o1125" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="fairly tightly; tightly to loosely" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="congested" value_original="congested" />
        <character is_modifier="false" modifier="moderately" name="architecture_or_arrangement" src="d0_s10" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o1126" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s10" to="50" />
      </biological_entity>
      <relation from="o1125" id="r174" name="with" negation="false" src="d0_s10" to="o1126" />
    </statement>
    <statement id="d0_s11">
      <text>nodes with 1-2 branches;</text>
      <biological_entity id="o1127" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <biological_entity id="o1128" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
      <relation from="o1127" id="r175" name="with" negation="false" src="d0_s11" to="o1128" />
    </statement>
    <statement id="d0_s12">
      <text>branches 0.5-3 cm, erect to ascending, slightly lax, terete or angled, angles sparsely to densely scabrous, with 2-12 spikelets.</text>
      <biological_entity id="o1129" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="ascending" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_arrangement" src="d0_s12" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o1130" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1131" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <relation from="o1130" id="r176" name="with" negation="false" src="d0_s12" to="o1131" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 3-6 (8) mm, lengths to 3 times widths, laterally compressed, compact, not sexually dimorphic;</text>
      <biological_entity id="o1132" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="0-3" value_original="0-3" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s13" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets 2-5;</text>
      <biological_entity id="o1133" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>rachilla internodes 0.8-1.1 mm, usually not readily visible from the sides, glabrous or sparsely puberulent.</text>
      <biological_entity constraint="rachilla" id="o1134" name="internode" name_original="internodes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character constraint="from sides" constraintid="o1135" is_modifier="false" modifier="usually not readily" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1135" name="side" name_original="sides" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Glumes slightly unequal, distinctly keeled, keels smooth or scabrous;</text>
      <biological_entity id="o1136" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s16" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o1137" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 2-4 mm, 1-3-veined, about 2/3 the length of the adjacent lemmas;</text>
      <biological_entity constraint="lower" id="o1138" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="1-3-veined" value_original="1-3-veined" />
        <character name="quantity" src="d0_s17" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o1139" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s17" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper glumes 2.9-5 mm;</text>
      <biological_entity constraint="upper" id="o1140" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>calluses usually diffusely webbed, hairs 1-2 mm, infrequently glabrous;</text>
      <biological_entity id="o1141" name="callus" name_original="calluses" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually diffusely" name="pubescence" src="d0_s19" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o1142" name="hair" name_original="hairs" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lemmas 2.5-4 (4.5) mm, lanceolate, distinctly keeled, moderately to densely finely scabrous, glabrous throughout or the keels and sometimes the marginal veins sparsely puberulent proximally, margins narrowly scarious, glabrous, apices acute;</text>
      <biological_entity id="o1143" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character name="atypical_some_measurement" src="d0_s20" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s20" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s20" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s20" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="moderately to densely finely" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1144" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o1145" name="vein" name_original="veins" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1146" name="margin" name_original="margins" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s20" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1147" name="apex" name_original="apices" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>paleas subequal to the lemmas, keels scabrous, intercostal regions glabrous;</text>
      <biological_entity id="o1148" name="palea" name_original="paleas" src="d0_s21" type="structure">
        <character constraint="to lemmas" constraintid="o1149" is_modifier="false" name="size" src="d0_s21" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o1149" name="lemma" name_original="lemmas" src="d0_s21" type="structure" />
      <biological_entity id="o1150" name="keel" name_original="keels" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s21" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o1151" name="region" name_original="regions" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>anthers vestigial (0.1-0.2 mm) or 1.5-2 mm. 2n = 42.</text>
      <biological_entity id="o1152" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s22" value="1.5-2 mm" value_original="1.5-2 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1153" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa confinis grows on sandy beaches and forest margins of the west coast, a habitat that is being lost to invasion by exotic species and development. It is closely related to P. diaboli (see next), from which it differs by a suite of characters. The two species are ecologically and geographically distinct. Poa confinis differs from P. pratensis (p. 522) in having glabrous or sparsely hairy lemmas and diffusely webbed calluses. It is gynodioecious.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>