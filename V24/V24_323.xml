<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">228</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Bromus</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">arvensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromus;species arvensis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>40</number>
  <other_name type="common_name">Field brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o11949" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 80-110 cm, erect.</text>
      <biological_entity id="o11950" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s1" to="110" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Lower sheaths with dense, soft, appressed hairs;</text>
      <biological_entity constraint="lower" id="o11951" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o11952" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o11951" id="r1901" name="with" negation="false" src="d0_s2" to="o11952" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-1.5 mm, hairy, obtuse, erose;</text>
      <biological_entity id="o11953" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s3" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10-20 cm long, 2-6 mm wide, coarsely pilose on both surfaces.</text>
      <biological_entity id="o11954" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o11955" is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11955" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 11-30 cm long, 4-20 cm wide, open, erect or nodding;</text>
      <biological_entity id="o11956" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches usually longer than the spikelets, ascending to widely spreading, slender, slightly curved or straight.</text>
      <biological_entity id="o11957" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character constraint="than the spikelets" constraintid="o11958" is_modifier="false" name="length_or_size" src="d0_s6" value="usually longer" value_original="usually longer" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="widely spreading" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o11958" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 10-25 mm, lanceolate, terete to moderately laterally compressed, often purple-tinged;</text>
      <biological_entity id="o11959" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s7" to="moderately laterally compressed" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>florets 4-10, bases concealed or visible at maturity;</text>
      <biological_entity id="o11960" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o11961" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s8" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachilla internodes concealed or visible at maturity.</text>
      <biological_entity constraint="rachilla" id="o11962" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="concealed" value_original="concealed" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s9" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes glabrous;</text>
      <biological_entity id="o11963" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 4-6 mm, 3-veined;</text>
      <biological_entity constraint="lower" id="o11964" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 5-8 mm, 5-veined;</text>
      <biological_entity constraint="upper" id="o11965" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 7-9 mm long, 1.1-1.5 mm wide, lanceolate, obscurely 7-veined, rounded over the midvein, glabrous, coriaceous, margins slightly angled, inrolled or not at maturity, apices acute, bifid, teeth shorter than 1 mm;</text>
      <biological_entity id="o11966" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
        <character constraint="over midvein" constraintid="o11967" is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="texture" src="d0_s13" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o11967" name="midvein" name_original="midvein" src="d0_s13" type="structure" />
      <biological_entity id="o11968" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s13" value="inrolled" value_original="inrolled" />
        <character name="shape_or_vernation" src="d0_s13" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o11969" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="at maturity" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o11970" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>awns 6-11 mm, straight, arising at varying distances below the lemma apices;</text>
      <biological_entity id="o11971" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character constraint="at distances" constraintid="o11972" is_modifier="false" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o11972" name="distance" name_original="distances" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o11973" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <relation from="o11972" id="r1902" name="below" negation="false" src="d0_s14" to="o11973" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2.5-5 mm.</text>
      <biological_entity id="o11974" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses shorter than the paleas, weakly to strongly inrolled.</text>
      <biological_entity id="o11976" name="palea" name_original="paleas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o11975" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character constraint="than the paleas" constraintid="o11976" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="weakly to strongly" name="shape_or_vernation" src="d0_s16" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11977" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus arvensis grows along roadsides and in fields and waste places at scattered locations in the Flora region. It is native to southern and south-central Europe.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C.;Wis.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Nev.;Va.;Colo.;Md.;Calif.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Miss.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Alta.;B.C.;Man.;Ont.;Que.;Sask.;Mont.;Oreg.;Ohio;Utah;Mo.;Minn.;Mich.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>