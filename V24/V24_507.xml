<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Hochst." date="unknown" rank="genus">LEYMUS</taxon_name>
    <taxon_name authority="(L.) Hochst." date="unknown" rank="species">arenarius</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus leymus;species arenarius</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">arenarius</taxon_name>
    <taxon_hierarchy>genus elymus;species arenarius</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">European dunegrass</other_name>
  <other_name type="common_name">Lymegrass</other_name>
  <other_name type="common_name">Elyme des sables d'europe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants weakly cespitose, rhizomatous, strongly glaucous.</text>
      <biological_entity id="o25849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="weakly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-150 cm tall, (2) 3-6 mm thick, usually glabrous throughout, occasionally pub¬escent distally to 5 mm below the spike.</text>
      <biological_entity id="o25850" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s1" to="150" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s1" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" constraint="below spike" constraintid="o25851" from="0" from_unit="mm" modifier="occasionally; distally" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25851" name="spike" name_original="spike" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves exceeded by the spikes;</text>
      <biological_entity id="o25852" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25853" name="spike" name_original="spikes" src="d0_s2" type="structure" />
      <relation from="o25852" id="r4074" name="exceeded by the" negation="false" src="d0_s2" to="o25853" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.3-2.5 mm;</text>
      <biological_entity id="o25854" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-11 mm wide, with 15-40 adaxial veins.</text>
      <biological_entity id="o25855" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25856" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s4" to="40" />
      </biological_entity>
      <relation from="o25855" id="r4075" name="with" negation="false" src="d0_s4" to="o25856" />
    </statement>
    <statement id="d0_s5">
      <text>Spikes 12-35 cm long, 15-25 mm wide, usually with 2 spikelets per node;</text>
      <biological_entity id="o25857" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25858" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25859" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o25857" id="r4076" modifier="usually" name="with" negation="false" src="d0_s5" to="o25858" />
      <relation from="o25858" id="r4077" name="per" negation="false" src="d0_s5" to="o25859" />
    </statement>
    <statement id="d0_s6">
      <text>internodes 8-12 mm, surfaces glabrous, edges ciliate.</text>
      <biological_entity id="o25860" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25861" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25862" name="edge" name_original="edges" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 12-30 mm, with 2-5 florets.</text>
      <biological_entity id="o25863" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25864" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o25863" id="r4078" name="with" negation="false" src="d0_s7" to="o25864" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 12-30 mm long, 2-3.5 mm wide, lanceolate, tapering from below midlength, stiff, glabrous towards the base and usually distally, sometimes pubescent distally, the central portion thicker than the margins, 3 (5) -veined at midlength, keeled or rounded over the midvein, midveins and sometimes the margins with hairs to about 1.3 mm, apices acuminate;</text>
      <biological_entity id="o25865" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character constraint="from midlength" constraintid="o25866" is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s8" value="stiff" value_original="stiff" />
        <character constraint="towards base" constraintid="o25867" is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually distally; distally; sometimes; distally" name="pubescence" notes="" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25866" name="midlength" name_original="midlength" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="below midlength" name="position" src="d0_s8" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o25867" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="central" id="o25868" name="portion" name_original="portion" src="d0_s8" type="structure">
        <character constraint="than the margins" constraintid="o25869" is_modifier="false" name="width" src="d0_s8" value="thicker" value_original="thicker" />
        <character constraint="at midlength" constraintid="o25870" is_modifier="false" name="architecture" src="d0_s8" value="3(5)-veined" value_original="3(5)-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character constraint="over midvein" constraintid="o25871" is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o25869" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o25870" name="midlength" name_original="midlength" src="d0_s8" type="structure" />
      <biological_entity id="o25871" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o25872" name="midvein" name_original="midveins" src="d0_s8" type="structure" />
      <biological_entity id="o25873" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o25874" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <biological_entity id="o25875" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="to about 1.3 mm" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o25872" id="r4079" name="with" negation="false" src="d0_s8" to="o25874" />
      <relation from="o25873" id="r4080" name="with" negation="false" src="d0_s8" to="o25874" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 12-25 mm, densely villous, hairs 0.3-0.7 mm, 5-7-veined, acute, occasionally awned, awns to 3 mm;</text>
      <biological_entity id="o25876" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o25877" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o25878" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 6-9 mm, dehiscent.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 56.</text>
      <biological_entity id="o25879" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25880" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leymus arenarius is native to Europe. It has become established in sandy habitats around the Great Lakes and the coast of Greenland. It has also been found at a few other widely scattered locations. It is sometimes cultivated, forming large, attractive, blue-green clumps, but its tendency to spread may be undesirable.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mich.;Wis.;N.Y.;Ill.;Ind.;Greenland;N.W.T.;Ont.;Que.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>