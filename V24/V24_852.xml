<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">604</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">DUPONTIA</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="species">fisheri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus dupontia;species fisheri</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Fisher's tundragrass</other_name>
  <other_name type="common_name">Dupontie de fisher</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes 1-3 mm thick.</text>
      <biological_entity id="o19143" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-80 cm, erect, glabrous.</text>
      <biological_entity id="o19144" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules of lower leaves 0.4-3 mm;</text>
      <biological_entity id="o19145" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19146" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o19145" id="r3026" name="part_of" negation="false" src="d0_s2" to="o19146" />
    </statement>
    <statement id="d0_s3">
      <text>ligules of flag leaves 1-4 (5.5) mm, usually lacerate;</text>
      <biological_entity id="o19147" name="ligule" name_original="ligules" src="d0_s3" type="structure" constraint="flag; flag">
        <character name="atypical_some_measurement" src="d0_s3" unit="mm" value="5.5" value_original="5.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o19148" name="flag" name_original="flag" src="d0_s3" type="structure" />
      <biological_entity id="o19149" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o19147" id="r3027" name="part_of" negation="false" src="d0_s3" to="o19148" />
      <relation from="o19147" id="r3028" name="part_of" negation="false" src="d0_s3" to="o19149" />
    </statement>
    <statement id="d0_s4">
      <text>blades 1-13 cm long, 1-4 mm wide.</text>
      <biological_entity id="o19150" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 2.5-18 cm long, 1-6 cm wide.</text>
      <biological_entity id="o19151" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 4-8.5 (9) mm;</text>
      <biological_entity id="o19152" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>rachilla internodes 1-1.5 mm.</text>
      <biological_entity constraint="rachilla" id="o19153" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes 4-8.5 (9) mm;</text>
      <biological_entity id="o19154" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas 3-6.5 mm;</text>
      <biological_entity id="o19155" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleas 2.8-6 mm;</text>
      <biological_entity id="o19156" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.5-3.5 mm.</text>
      <biological_entity id="o19157" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses 1.5-3 mm. 2n = 42, 44, 66, 84, 88, about 105, about 126, 132.</text>
      <biological_entity id="o19158" name="caryopse" name_original="caryopses" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19159" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="42" value_original="42" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="44" value_original="44" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="66" value_original="66" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="84" value_original="84" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="88" value_original="88" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="105" value_original="105" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="126" value_original="126" />
        <character name="quantity" src="d0_s12" unit=",,,,,about ,about ," value="132" value_original="132" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dupontia fisheri grows in wet meadows, wet tundra, marshes, and along streams and the edges of lagoons, ponds, and lake shores, in sand, silt, clay, moss, and rarely in bogs.</discussion>
  <discussion>Two subspecies of Dupontia are sometimes recognized in North America. Dupontia fisheri R. Br. subsp. fisheri supposedly differs from subsp. psilosantha (Rupr.) Hulten in being shorter than 40 cm, having erect panicle branches, 2-4 florets per spikelet, pubescent, obtuse lemmas, and 2n= 84, 88, or 132. Dupontia fisheri subsp. psilosantha is taller, has reflexed panicle branches, 1-2 florets per spikelet, more or less glabrous, acute lemmas, and 2n =42 or 44. Plants referable to subsp. psilosantha are restricted to coastal marshes, rarely penetrating inland along riparian habitats, from James Bay to the lower arctic archipelago. Plants referable to subsp. fisheri are less halophytic and more northerly in their distribution, being found in a variety of inland marshes and wet tundra habitats from northern Alaska to Ellesmere Island. Intermediates are readily found (e.g., hexaploid, 2N = 66 "hybrids" from Alaska) and the correlations among chromosome number, morphology, ecology, and distribution are relatively weak in North America and Greenland. For these reasons, no subspecific taxa are recognized in this treatment. Dupontia fisheri hybridizes with Poa eminens to form ×Dupoa labradorica (p. 602). The hybrids differ from D. fisheri in having glumes that usually do not exceed the florets, and in having some woolly or crinkly callus hairs. The hybrid genus ×Arctodupontia is formed from D. fisheri and Arctophila fulva, and differs from D. fisheri in having lemmas with truncate, lacerate to dentate apices, rather than acute to acuminate apices.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland;Nfld. and Labr. (Nfld.);Man.;N.W.T.;Nunavut;Ont.;Que.;Yukon;Alaska</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>