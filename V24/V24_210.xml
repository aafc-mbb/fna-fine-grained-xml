<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">STIPA</taxon_name>
    <taxon_name authority="K. Koch" date="unknown" rank="species">pulcherrima</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus stipa;species pulcherrima</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Beautiful feathergrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o2157" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-100 cm, glabrous or pubescent below the panicles.</text>
      <biological_entity id="o2158" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="below panicles" constraintid="o2159" is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2159" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths longer than the internodes, smooth, mostly glabrous, margins sometimes ciliate;</text>
      <biological_entity id="o2160" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o2161" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2161" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o2162" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-2 mm on the innovations, to 9 mm on the cauline leaves, stiffly membranous, pubescent, entire to erose;</text>
      <biological_entity id="o2163" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="on innovations" constraintid="o2164" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" constraint="on cauline leaves" constraintid="o2165" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="stiffly" name="texture" notes="" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="entire" name="architecture" src="d0_s4" to="erose" />
      </biological_entity>
      <biological_entity id="o2164" name="innovation" name_original="innovations" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o2165" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blades 2-5 mm wide when flat, 0.3-0.9 (1.5) mm in diameter if involute, abaxial surfaces glabrous, smooth or scabrous, adaxial surfaces glabrous, scabrous, or hirsute, hairs to 0.6 mm.</text>
      <biological_entity id="o2166" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" modifier="when flat" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character name="atypical_diameter" src="d0_s5" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" constraint="if abaxial surfaces" constraintid="o2167" from="0.3" from_unit="mm" name="diameter" src="d0_s5" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2167" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2168" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o2169" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-15 cm, contracted, usually partially enclosed in the upper sheath;</text>
      <biological_entity id="o2170" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o2171" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
      <relation from="o2170" id="r356" modifier="usually partially" name="enclosed in the" negation="false" src="d0_s6" to="o2171" />
    </statement>
    <statement id="d0_s7">
      <text>branches appressed to ascending, with 1-4 spikelets.</text>
      <biological_entity id="o2172" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s7" to="ascending" />
      </biological_entity>
      <biological_entity id="o2173" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o2172" id="r357" name="with" negation="false" src="d0_s7" to="o2173" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 60-80 (90) mm, long-attenuate, mostly hyaline;</text>
      <biological_entity id="o2174" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="90" value_original="90" />
        <character char_type="range_value" from="60" from_unit="mm" name="some_measurement" src="d0_s8" to="80" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="long-attenuate" value_original="long-attenuate" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>florets (18) 20-27 mm;</text>
      <biological_entity id="o2175" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="18" value_original="18" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses 3-6 mm;</text>
      <biological_entity id="o2176" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas with lines of hair over the veins and marginal veins, lines over the marginal veins longest, extending to the lemma apices;</text>
      <biological_entity id="o2177" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity id="o2178" name="line" name_original="lines" src="d0_s11" type="structure" />
      <biological_entity id="o2179" name="hair" name_original="hair" src="d0_s11" type="structure" />
      <biological_entity id="o2180" name="vein" name_original="veins" src="d0_s11" type="structure" />
      <biological_entity constraint="marginal" id="o2181" name="vein" name_original="veins" src="d0_s11" type="structure" />
      <biological_entity id="o2182" name="line" name_original="lines" src="d0_s11" type="structure" />
      <biological_entity constraint="marginal" id="o2183" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o2184" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <relation from="o2177" id="r358" name="with" negation="false" src="d0_s11" to="o2178" />
      <relation from="o2178" id="r359" name="part_of" negation="false" src="d0_s11" to="o2179" />
      <relation from="o2178" id="r360" name="over" negation="false" src="d0_s11" to="o2180" />
      <relation from="o2182" id="r361" name="over" negation="false" src="d0_s11" to="o2183" />
      <relation from="o2182" id="r362" name="extending to the" negation="false" src="d0_s11" to="o2184" />
    </statement>
    <statement id="d0_s12">
      <text>awns (250) 300-500 mm, twice-geniculate, first 2 segments glabrous or hairy, hairs to 2 mm, terminal segment plumose, hairs 5-6 mm, spreading;</text>
      <biological_entity id="o2185" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="250" value_original="250" />
        <character char_type="range_value" from="300" from_unit="mm" name="some_measurement" src="d0_s12" to="500" to_unit="mm" />
        <character constraint="segment" constraintid="o2186" is_modifier="false" name="size_or_quantity" src="d0_s12" value="2 times-geniculate first 2 segments" />
        <character constraint="segment" constraintid="o2187" is_modifier="false" name="size_or_quantity" src="d0_s12" value="2 times-geniculate first 2 segments" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2186" name="segment" name_original="segments" src="d0_s12" type="structure" />
      <biological_entity id="o2187" name="segment" name_original="segments" src="d0_s12" type="structure" />
      <biological_entity id="o2188" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2189" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity id="o2190" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas subequal to the lemmas, usually hairy on the keels, apices scarious;</text>
      <biological_entity id="o2191" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character constraint="to lemmas" constraintid="o2192" is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character constraint="on keels" constraintid="o2193" is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2192" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o2193" name="keel" name_original="keels" src="d0_s13" type="structure" />
      <biological_entity id="o2194" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 5-10 mm, not penicillate, yellowish or purplish;</text>
      <biological_entity id="o2195" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="penicillate" value_original="penicillate" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2.</text>
      <biological_entity id="o2196" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 10-18 mm. 2n = 44.</text>
      <biological_entity id="o2197" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2198" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Stipa pulcherrima is native from France and Germany to Armenia, Azerbaijan, Turkey, and Iran. Freitag (1985) regarded it as a subspecies of S. pennata L. Its long, plumose awns make it a striking ornamental.</discussion>
  
</bio:treatment>