<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">721</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">porteri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species porteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Porter's reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with sterile culms;</text>
      <biological_entity id="o29393" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o29392" id="r4634" name="with" negation="false" src="d0_s0" to="o29393" />
    </statement>
    <statement id="d0_s1">
      <text>loosely cespitose, with rhizomes 5-7+ cm long, 0.5-1 mm thick.</text>
      <biological_entity id="o29392" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="7" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29394" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="7" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (60) 75-120 cm, unbranched, slightly scabrous;</text>
      <biological_entity id="o29395" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="60" value_original="60" />
        <character char_type="range_value" from="75" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 2-4 (5).</text>
      <biological_entity id="o29396" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="5" value_original="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth or slightly scab¬rous;</text>
      <biological_entity id="o29397" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o29398" name="scab" name_original="scab" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>collars smooth or hairy;</text>
      <biological_entity id="o29399" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules (1) 2-5 (6) mm, truncate to obtuse, entire or lacerate;</text>
      <biological_entity id="o29400" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="obtuse entire or lacerate" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s6" to="obtuse entire or lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 8-40 cm long, (2) 3-8 (12) mm wide, flat, abaxial surfaces smooth or scabrous, adaxial surfaces smooth or slightly scabrous, occasionally with hairs.</text>
      <biological_entity id="o29401" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s7" to="40" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29402" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29403" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29404" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <relation from="o29403" id="r4635" modifier="occasionally" name="with" negation="false" src="d0_s7" to="o29404" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles (5) 10-18 (22) cm long, 0.8-3 (7) cm wide, contracted to open, often slightly nodding, sometimes erect, green to pale-purple;</text>
      <biological_entity id="o29405" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="18" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="often slightly" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="pale-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches (1.5) 2-7 (7.5) cm, scabrous, usually spikelet-bearing on the distal 1/2 - 2/3, sometimes to the base.</text>
      <biological_entity id="o29406" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o29407" name="1/2-2/3" name_original="1/2-2/3" src="d0_s9" type="structure" />
      <biological_entity id="o29408" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o29406" id="r4636" modifier="usually" name="on" negation="false" src="d0_s9" to="o29407" />
      <relation from="o29406" id="r4637" modifier="sometimes" name="to" negation="false" src="d0_s9" to="o29408" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-5 (6) mm;</text>
      <biological_entity id="o29409" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla prolongations 0.5-1 mm, hairs 1.5-2 (3.5) mm.</text>
      <biological_entity id="o29410" name="rachillum" name_original="rachilla" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29411" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes rounded to slightly keeled, keels scabrous towards the apices, lateral-veins obscure to slightly prominent, not raised, apices acute to acuminate;</text>
      <biological_entity id="o29412" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s12" to="slightly keeled" />
      </biological_entity>
      <biological_entity id="o29413" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character constraint="towards apices" constraintid="o29414" is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29414" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <biological_entity id="o29415" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s12" to="slightly prominent" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o29416" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 1.5-2 (3) mm, 0.4-0.7 times as long as the lemmas, sparse;</text>
      <biological_entity constraint="callus" id="o29417" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character constraint="lemma" constraintid="o29418" is_modifier="false" name="length" src="d0_s13" value="0.4-0.7 times as long as the lemmas" />
        <character is_modifier="false" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o29418" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3-4.5 mm, 0.5-1.5 (2) mm shorter than the glumes;</text>
      <biological_entity id="o29419" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o29420" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o29420" name="glume" name_original="glumes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>awns 3-4 (4.5) mm, attached to the lower 1/10 – 3/10 of the lemmas, exserted, stout, easily distinguished from the callus hairs, strongly bent;</text>
      <biological_entity id="o29421" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s15" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s15" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o29422" from="1/10" name="quantity" src="d0_s15" to="3/10" />
        <character is_modifier="false" name="position" notes="" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s15" value="stout" value_original="stout" />
        <character constraint="from callus hairs" constraintid="o29423" is_modifier="false" modifier="easily" name="prominence" src="d0_s15" value="distinguished" value_original="distinguished" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s15" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o29422" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity constraint="callus" id="o29423" name="hair" name_original="hairs" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 2-2.5 mm. 2n = 56, 84-+104.</text>
      <biological_entity id="o29424" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29425" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="56" value_original="56" />
        <character name="quantity" src="d0_s16" value="84" value_original="84" />
        <character name="quantity" src="d0_s16" value="104" value_original="104" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis porteri grows in dry chestnut/oak forests, often on rocky ridgetops, piedmont bluffs, and slopes, at (100)600-1300 m. It is now restricted to the northeastern and central United States. Historically, its range extended from Missouri and Arkansas east to New York and Alabama. Flowering appears to be a response to disturbance; plants in undisturbed habitats remain vegetative and may go unnoticed. Thus the species may be more widespread and abundant than reported.</discussion>
  <discussion>Calamagrostis porteri and C. rubescens (see next) appear to be closely related. They may be part of the general phenomenon of eastern and western vicariants. The apparently sterile C. perplexa (p. 726) is intermediate between C. porteri and C. canadensis (Greene 1980).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;W.Va.;Pa.;Ohio;Mo.;N.C.;Tenn.;Va.;N.Y.;Ark.;Ill.;Ga.;Ind.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Leaf blades glaucous on both surfaces; leaf collars glabrous</description>
      <determination>Calamagrostis porteri subsp. insperata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Leaf blades light green and glaucous on the adaxial surfaces, darker green on the abaxial surfaces; leaf collars usually with prominent tufts of hair, rarely glabrous</description>
      <determination>Calamagrostis porteri subsp. porteri</determination>
    </key_statement>
  </key>
</bio:treatment>