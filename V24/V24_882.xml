<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">619</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Roshev." date="unknown" rank="genus">EREMOPOA</taxon_name>
    <taxon_name authority="(Trin.) Roshev." date="unknown" rank="species">altaica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus eremopoa;species altaica</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Altai grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (5) 10-45 cm, solitary or tufted, erect, glabrous.</text>
      <biological_entity id="o16460" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
      <biological_entity id="o16461" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sheaths closed for about 1/10 their length, glabrous;</text>
      <biological_entity id="o16462" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-4 mm, glabrous;</text>
      <biological_entity id="o16463" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-10 cm long, 0.5-4 mm wide, flat, usually glabrous, sometimes scabrous.</text>
      <biological_entity id="o16464" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (3) 6-16 (20) cm long, 1-6 cm wide, diffuse;</text>
      <biological_entity id="o16465" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches 2-10 cm, whorled and strongly divergent, spikelets confined to the distal portion.</text>
      <biological_entity id="o16466" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o16467" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o16468" name="portion" name_original="portion" src="d0_s6" type="structure" />
      <relation from="o16467" id="r2641" name="confined to the" negation="false" src="d0_s6" to="o16468" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 2.8-6.2 mm, pedicellate, with (1) 2-3 (4) florets.</text>
      <biological_entity id="o16469" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s7" to="6.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o16470" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="4" value_original="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o16469" id="r2642" name="with" negation="false" src="d0_s7" to="o16470" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes lanceolate, glabrous, acute to acuminate;</text>
      <biological_entity id="o16471" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 0.9-1.5 (2) mm;</text>
      <biological_entity constraint="lower" id="o16472" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 1.5-2.5 mm;</text>
      <biological_entity constraint="upper" id="o16473" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 2.5-4 mm, narrowly lanceolate, glabrous or with a few silky hairs on the lower keel, 5-veined, apices entire, acute to acuminate;</text>
      <biological_entity id="o16474" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="with a few silky hairs" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o16475" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
        <character is_modifier="true" name="pubescence" src="d0_s11" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity constraint="lower" id="o16476" name="keel" name_original="keel" src="d0_s11" type="structure" />
      <biological_entity id="o16477" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
      <relation from="o16474" id="r2643" name="with" negation="false" src="d0_s11" to="o16475" />
      <relation from="o16475" id="r2644" name="on" negation="false" src="d0_s11" to="o16476" />
    </statement>
    <statement id="d0_s12">
      <text>paleas 2-3 mm, 2-keeled, glabrous;</text>
      <biological_entity id="o16478" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.3-1 mm.</text>
      <biological_entity id="o16479" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.9-2.6 mm. 2n = 14, 28, 42.</text>
      <biological_entity id="o16480" name="caryopse" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s14" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16481" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
        <character name="quantity" src="d0_s14" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eremopoa altaica is native to the high deserts and mountain steppes of Asia, from Turkey through Afghanistan and Pakistan to the Altai region, the Himalayas, and western China. It was once found in railway yards and roadsides at Brandon, Manitoba (Stevenson 1965), but misidentified as Eremopoa persica (Trin.) Roshev. Although the population persisted for several years, recent efforts to relocate it haved failed.</discussion>
  
</bio:treatment>