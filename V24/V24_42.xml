<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">LEERSIA</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">lenticularis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus leersia;species lenticularis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Catchfly grass</other_name>
  <other_name type="common_name">Oatmeal grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes moderately elongate, scaly.</text>
      <biological_entity id="o28805" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o28806" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-150 cm tall, 1-3 mm thick, usually ascending, unbranched or branched;</text>
      <biological_entity id="o28807" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes retrorsely hispidulous, adjacent portion of the internodes glabrous.</text>
      <biological_entity id="o28808" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o28809" name="portion" name_original="portion" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28810" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <relation from="o28809" id="r4526" name="part_of" negation="false" src="d0_s3" to="o28810" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or scabrous;</text>
      <biological_entity id="o28811" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1.5 mm;</text>
      <biological_entity id="o28812" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 4-35 cm long, 5-22 mm wide, spreading to somewhat ascending, abaxial surfaces glabrous or scabridulous, adaxial surfaces glabrous or pubescent, margins usually scabrous.</text>
      <biological_entity id="o28813" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading to somewhat" value_original="spreading to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28814" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28815" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28816" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-25 cm, exserted, with 1 (2) branches per node;</text>
      <biological_entity id="o28817" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="25" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o28818" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28819" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o28817" id="r4527" name="with" negation="false" src="d0_s7" to="o28818" />
      <relation from="o28818" id="r4528" name="per" negation="false" src="d0_s7" to="o28819" />
    </statement>
    <statement id="d0_s8">
      <text>branches 8-15 cm, spreading, secund, lower branches naked on the lower 73, spikelets strongly imbricate.</text>
      <biological_entity id="o28820" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28821" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="on lower branches" constraintid="o28822" is_modifier="false" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28822" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="73" value_original="73" />
      </biological_entity>
      <biological_entity id="o28823" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-5.5 mm long, 3-4 mm wide, broadly elliptic to suborbicular.</text>
      <biological_entity id="o28824" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s9" to="suborbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Lemmas coarsely ciliate on the keels, variously pubescent on the margins and body, mucronate;</text>
      <biological_entity id="o28825" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character constraint="on keels" constraintid="o28826" is_modifier="false" modifier="coarsely" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
        <character constraint="on body" constraintid="o28828" is_modifier="false" modifier="variously" name="pubescence" notes="" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o28826" name="keel" name_original="keels" src="d0_s10" type="structure" />
      <biological_entity id="o28827" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o28828" name="body" name_original="body" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>paleas ciliate on the keels;</text>
      <biological_entity id="o28829" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character constraint="on keels" constraintid="o28830" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o28830" name="keel" name_original="keels" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers 2.</text>
      <biological_entity id="o28831" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Caryopses 3.5-4 mm, reddish-brown.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 48.</text>
      <biological_entity id="o28832" name="caryopse" name_original="caryopses" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28833" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leersia lenticularis grows in river bottoms and moist woods of the midwestern and southeastern United States. It flowers from July to November. Ohio and Maryland list it as an endangered species.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Tenn.;Okla.;Miss.;Tex.;La.;Mo.;Minn.;Ala.;Wis.;Kans.;N.C.;S.C.;Va.;Ark.;Ill.;Ga.;Ind.;Iowa;Ky.;Fla.;Ohio</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>