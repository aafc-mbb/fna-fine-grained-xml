<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sandy Long;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">685</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">CYNOSURUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus cynosurus</taxon_hierarchy>
  </taxon_identification>
  <number>14.37</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes rhizomatous.</text>
      <biological_entity id="o15766" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 1.5-90 cm, erect.</text>
      <biological_entity id="o15767" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves 1-3;</text>
      <biological_entity constraint="cauline" id="o15768" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths open to the base;</text>
      <biological_entity id="o15769" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="to base" constraintid="o15770" is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o15770" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>auricles absent;</text>
      <biological_entity id="o15771" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules truncate, entire, erose, or ciliolate;</text>
      <biological_entity id="o15772" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat.</text>
      <biological_entity id="o15773" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal panicles, condensed, often spikelike, linear to almost globose, more or less unilateral;</text>
      <biological_entity id="o15774" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="condensed" value_original="condensed" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear to almost" value_original="linear to almost" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s8" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_orientation_or_position" src="d0_s8" value="unilateral" value_original="unilateral" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o15775" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>branches and pedicels stiff, straight, smooth and glabrous or almost so, sometimes slightly scabridulous, scabrules/hairs to 0.1 mm.</text>
      <biological_entity id="o15776" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s9" value="almost" value_original="almost" />
        <character is_modifier="false" modifier="sometimes slightly" name="relief" src="d0_s9" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o15777" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes slightly" name="relief" src="d0_s9" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o15778" name="hair" name_original="scabrules/hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets dimorphic, usually paired, subsessile to shortly pedicellate, laterally compressed, proximal spikelet of each pair sterile, almost completely concealing the fertile spikelet, distal spikelet on each branch sometimes solitary;</text>
      <biological_entity id="o15779" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s10" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s10" to="shortly pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15780" name="spikelet" name_original="spikelet" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o15781" name="pair" name_original="pair" src="d0_s10" type="structure" />
      <biological_entity id="o15782" name="spikelet" name_original="spikelet" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="almost completely" name="position" src="d0_s10" value="concealing" value_original="concealing" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o15784" name="branch" name_original="branch" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o15780" id="r2525" name="part_of" negation="false" src="d0_s10" to="o15781" />
      <relation from="o15783" id="r2526" name="on" negation="false" src="d0_s10" to="o15784" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes, beneath the florets.</text>
      <biological_entity constraint="distal" id="o15783" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
      <biological_entity id="o15785" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o15786" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o15783" id="r2527" name="above" negation="false" src="d0_s11" to="o15785" />
    </statement>
    <statement id="d0_s12">
      <text>Sterile spikelets persistent, with 6-18 florets, florets reduced to narrow, linear-lanceolate lemmas, sometimes awned, awns terminal;</text>
      <biological_entity id="o15787" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o15788" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s12" to="18" />
      </biological_entity>
      <biological_entity id="o15789" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="reduced" name="size" src="d0_s12" to="narrow" />
      </biological_entity>
      <biological_entity id="o15790" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15791" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
      </biological_entity>
      <relation from="o15787" id="r2528" name="with" negation="false" src="d0_s12" to="o15788" />
    </statement>
    <statement id="d0_s13">
      <text>glumes narrow, linear.</text>
      <biological_entity id="o15792" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fertile spikelets adaxial to the sterile spikelets, with 1-5 florets;</text>
      <biological_entity id="o15793" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="position" src="d0_s14" value="adaxial" value_original="adaxial" />
      </biological_entity>
      <biological_entity id="o15794" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o15795" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <relation from="o15793" id="r2529" name="with" negation="false" src="d0_s14" to="o15795" />
    </statement>
    <statement id="d0_s15">
      <text>rachillas glabrous, prolonged beyond the base of the distal floret;</text>
      <biological_entity id="o15796" name="rachilla" name_original="rachillas" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character constraint="beyond base" constraintid="o15797" is_modifier="false" name="length" src="d0_s15" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o15797" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity constraint="distal" id="o15798" name="floret" name_original="floret" src="d0_s15" type="structure" />
      <relation from="o15797" id="r2530" name="part_of" negation="false" src="d0_s15" to="o15798" />
    </statement>
    <statement id="d0_s16">
      <text>glumes 2, subequal and shorter than the spikelets, thin, lanceolate, 1-veined, acute, sometimes awned;</text>
      <biological_entity id="o15799" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character constraint="than the spikelets" constraintid="o15800" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15800" name="spikelet" name_original="spikelets" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>calluses short, blunt, glabrous;</text>
      <biological_entity id="o15801" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s17" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas glabrous or pubescent, 5-veined, acute or bidentate, unawned to conspicuously awned, awns terminal;</text>
      <biological_entity id="o15802" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s18" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15803" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s18" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>paleas about as long as the lemmas, bifid;</text>
      <biological_entity id="o15804" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s19" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o15805" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
      <relation from="o15804" id="r2531" name="as long as" negation="false" src="d0_s19" to="o15805" />
    </statement>
    <statement id="d0_s20">
      <text>lodicules 2, free, glabrous, ovate, bilobed;</text>
      <biological_entity id="o15806" name="lodicule" name_original="lodicules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="bilobed" value_original="bilobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 3;</text>
      <biological_entity id="o15807" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovaries broadly ellipsoid, glabrous;</text>
      <biological_entity id="o15808" name="ovary" name_original="ovaries" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s22" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>styles separate.</text>
      <biological_entity id="o15809" name="style" name_original="styles" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Caryopses oblong-ellipsoid, subterete, slightly dorsally compressed, sometimes adherent to the paleas: hila 1/5-1/2 as long as the caryopses, oblong to linear.</text>
      <biological_entity id="o15810" name="caryopse" name_original="caryopses" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="oblong-ellipsoid" value_original="oblong-ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="slightly dorsally" name="shape" src="d0_s24" value="compressed" value_original="compressed" />
        <character constraint="to paleas" constraintid="o15811" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s24" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o15811" name="palea" name_original="paleas" src="d0_s24" type="structure" />
      <biological_entity id="o15813" name="caryopse" name_original="caryopses" src="d0_s24" type="structure" />
    </statement>
    <statement id="d0_s25">
      <text>x = 7.</text>
      <biological_entity id="o15812" name="hilum" name_original="hila" src="d0_s24" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o15813" from="1/5" name="quantity" src="d0_s24" to="1/2" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s24" to="linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o15814" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynosurus is a genus of eight species that grow in open, grassy, often weedy habitats. It is native around the Mediterranean and in western Asia. The affinities of the genus are obscure. Two species are established in the Flora region.</discussion>
  <references>
    <reference>Ennos, R.A. 1985. The mating system and genetic structure in a perennial grass, Cynosurus cristatus L. Heredity 55:121-126</reference>
    <reference>Jirasek, V. and J. Chrtek. 1964. Zur frage des taxonomischen Wertes der Gattung Cynosurus L. Novit. Bot. Delect. Seminum Horti Bot. Univ. Carol. Prag. 20:23-27</reference>
    <reference>Lodge, R.W. 1959. Biological flora of the British Isles: Cynosurus cristatus L. J. Ecol. 47:511-518.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.;Oreg.;Conn.;N.J.;N.Y.;Wash.;Del.;Wis.;W.Va.;N.H.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;B.C.;Nfld. and Labr. (Labr.);N.S.;Ont.;Que.;Va.;Colo.;Calif.;Ala.;Ark.;Vt.;Ga.;Okla.;Idaho;Maine;Md.;Mass.;Ohio;Mo.;Mich.;R.I.;Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants perennial; panicles linear; fertile lemmas unawned or with awns shorter than 3 mm</description>
      <determination>1 Cynosurus cristatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants annual; panicles ovoid to almost globose; fertile lemmas with awns 5-25 mm long</description>
      <determination>2 Cynosurus echinatus</determination>
    </key_statement>
  </key>
</bio:treatment>