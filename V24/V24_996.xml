<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">704</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">AMPHIBROMUS</taxon_name>
    <taxon_name authority="(Trin.) Swallen" date="unknown" rank="species">scabrivalvis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus amphibromus;species scabrivalvis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Rough amphibromus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o9947" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-100+ cm, terete, erect or decumbent;</text>
      <biological_entity id="o9948" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes up to 10, lowest 3 or 4 nodes often underground, aerial nodes usually producing cleistogamous panicles;</text>
      <biological_entity id="o9949" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="position" src="d0_s3" value="lowest" value_original="lowest" />
        <character name="quantity" src="d0_s3" unit="or 4nodes" value="3" value_original="3" />
        <character is_modifier="false" modifier="often" name="location" src="d0_s3" value="underground" value_original="underground" />
      </biological_entity>
      <biological_entity id="o9950" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
      </biological_entity>
      <biological_entity id="o9951" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <relation from="o9950" id="r1593" name="producing" negation="false" src="d0_s3" to="o9951" />
    </statement>
    <statement id="d0_s4">
      <text>lowest internodes usually swollen.</text>
      <biological_entity constraint="lowest" id="o9952" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o9953" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9954" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s5" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sheaths usually longer than the internodes, smooth;</text>
      <biological_entity id="o9955" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character constraint="than the internodes" constraintid="o9956" is_modifier="false" name="length_or_size" src="d0_s6" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o9956" name="internode" name_original="internodes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ligules 6-15 mm;</text>
      <biological_entity id="o9957" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower blades 5-25 (40) cm long, 2-6 mm wide, flat, rather lax;</text>
      <biological_entity constraint="lower" id="o9958" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="rather" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>upper blades reduced, sometimes to 1 cm.</text>
      <biological_entity constraint="upper" id="o9959" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="sometimes" name="some_measurement" src="d0_s9" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cleistogamous panicles with 6-10 mm spikelets bearing 1-3 florets, lemmas unawned, sometimes mucronate, anthers about 0.7 mm.</text>
      <biological_entity id="o9960" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o9961" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9962" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o9963" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o9964" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
      <relation from="o9960" id="r1594" name="with" negation="false" src="d0_s10" to="o9961" />
      <relation from="o9961" id="r1595" name="bearing" negation="false" src="d0_s10" to="o9962" />
    </statement>
    <statement id="d0_s11">
      <text>Terminal panicles 6-27 cm, often partially enclosed in the uppermost sheaths;</text>
      <biological_entity constraint="terminal" id="o9965" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s11" to="27" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o9966" name="sheath" name_original="sheaths" src="d0_s11" type="structure" />
      <relation from="o9965" id="r1596" modifier="often partially" name="enclosed in the" negation="false" src="d0_s11" to="o9966" />
    </statement>
    <statement id="d0_s12">
      <text>branches 2-8 cm, ascending to drooping, often sinuous;</text>
      <biological_entity id="o9967" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="8" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s12" to="drooping" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s12" value="sinuous" value_original="sinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicels absent or to 10 mm.</text>
      <biological_entity id="o9968" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s13" value="0-10 mm" value_original="0-10 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spikelets 12-25 mm, with 3-9 florets.</text>
      <biological_entity id="o9969" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9970" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s14" to="9" />
      </biological_entity>
      <relation from="o9969" id="r1597" name="with" negation="false" src="d0_s14" to="o9970" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes 1/2 - 2/3 the length of the adjacent lemmas;</text>
      <biological_entity id="o9971" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s15" to="2/3" />
      </biological_entity>
      <biological_entity id="o9972" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 3.5-6.7 mm, 1-3-veined;</text>
      <biological_entity constraint="lower" id="o9973" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes 5-8 mm, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o9974" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas 5-11 mm, 7-9-veined, hispid or tuberculate, deeply bilobed, awned, awns 8-17 mm, arising near midlength;</text>
      <biological_entity id="o9975" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s18" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="7-9-veined" value_original="7-9-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="relief" src="d0_s18" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s18" value="bilobed" value_original="bilobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o9976" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s18" to="17" to_unit="mm" />
        <character constraint="near midlength" constraintid="o9977" is_modifier="false" name="orientation" src="d0_s18" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o9977" name="midlength" name_original="midlength" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>paleas 4-6 mm, chartaceous, margins scabrous to ciliolate distally;</text>
      <biological_entity id="o9978" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s19" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o9979" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character char_type="range_value" from="scabrous" modifier="distally" name="pubescence" src="d0_s19" to="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 0.7-2 mm. 2n = unknown.</text>
      <biological_entity id="o9980" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s20" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9981" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Amphibromus scabrivalvis is native to open grasslands of South America. It was discovered growing in strawberry patches in Tangipahoa Parish, Louisiana, in the late 1950s. Despite efforts to eradicate it, the species persists there; it is not known to have spread elsewhere. North American plants belong to Amphibromus scabrivalvis (Trin.) Swallen var. scabrivalvis.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>