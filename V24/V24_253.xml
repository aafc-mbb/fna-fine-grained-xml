<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Surrey W.L. Jacobs;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="S.W.L. Jacobs &amp; J. Everett" date="unknown" rank="genus">AUSTROSTIPA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus austrostipa</taxon_hierarchy>
  </taxon_identification>
  <number>10.15</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants facultative perennials;</text>
      <biological_entity id="o747" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s0" value="facultative" value_original="facultative" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>to 2.5 m, sometimes shrublike, often with knotty bases.</text>
      <biological_entity id="o746" name="whole_organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s1" unit="m" value="2.5" value_original="2.5" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="shrublike" value_original="shrublike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o748" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms often persistent, sometimes geniculate at the base, sometimes with stiff branches at the upper nodes;</text>
      <biological_entity id="o750" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o751" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="upper" id="o752" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o749" id="r121" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o751" />
      <relation from="o751" id="r122" name="at" negation="false" src="d0_s2" to="o752" />
    </statement>
    <statement id="d0_s3">
      <text>basal branching usually extravaginal;</text>
      <biological_entity id="o749" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character constraint="at base" constraintid="o750" is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s3" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>prophylls not evident.</text>
      <biological_entity id="o753" name="prophyll" name_original="prophylls" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves not overwintering;</text>
      <biological_entity id="o754" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s5" value="overwintering" value_original="overwintering" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sheaths open to the base, margins sometimes extending beyond the base of the ligule, the extensions often conspicuously hairy, sometimes grading into the ligule;</text>
      <biological_entity id="o755" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character constraint="to base" constraintid="o756" is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o756" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o757" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o758" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o759" name="ligule" name_original="ligule" src="d0_s6" type="structure" />
      <biological_entity id="o760" name="extension" name_original="extensions" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often conspicuously" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o761" name="ligule" name_original="ligule" src="d0_s6" type="structure" />
      <relation from="o757" id="r123" name="extending beyond" negation="false" src="d0_s6" to="o758" />
      <relation from="o758" id="r124" name="part_of" negation="false" src="d0_s6" to="o759" />
      <relation from="o760" id="r125" modifier="sometimes" name="into" negation="false" src="d0_s6" to="o761" />
    </statement>
    <statement id="d0_s7">
      <text>auricles not present;</text>
      <biological_entity id="o762" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules membranous;</text>
      <biological_entity id="o763" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades flat, convolute, or terete, usually scabrous, sometimes pubescent, those of the flag leaves longer than 10 mm, bases usually as wide as the top of the sheaths, sometimes narrower.</text>
      <biological_entity id="o764" name="blade" name_original="blades" src="d0_s9" type="structure" constraint="flag; flag">
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o765" name="flag" name_original="flag" src="d0_s9" type="structure" />
      <biological_entity id="o766" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o767" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="width" notes="" src="d0_s9" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o768" name="top" name_original="top" src="d0_s9" type="structure" />
      <biological_entity id="o769" name="sheath" name_original="sheaths" src="d0_s9" type="structure" />
      <relation from="o764" id="r126" name="part_of" negation="false" src="d0_s9" to="o765" />
      <relation from="o764" id="r127" name="part_of" negation="false" src="d0_s9" to="o766" />
      <relation from="o767" id="r128" name="as wide as" negation="false" src="d0_s9" to="o768" />
      <relation from="o768" id="r129" name="top of" negation="false" src="d0_s9" to="o769" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences panicles, sometimes contracted, rachises persistent or disarticulating whole at maturity;</text>
      <biological_entity constraint="inflorescences" id="o770" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o771" name="rachis" name_original="rachises" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s10" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels smooth or scabrous, sometimes hairy, hairs to 3 mm;</text>
      <biological_entity id="o772" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes, sometimes also at the base of the panicle.</text>
      <biological_entity id="o773" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o774" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o775" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o776" name="panicle" name_original="panicle" src="d0_s12" type="structure" />
      <relation from="o773" id="r130" name="above" negation="false" src="d0_s12" to="o774" />
      <relation from="o775" id="r131" name="part_of" negation="false" src="d0_s12" to="o776" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets with 1 floret;</text>
      <biological_entity id="o777" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <biological_entity id="o778" name="floret" name_original="floret" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <relation from="o777" id="r132" name="with" negation="false" src="d0_s13" to="o778" />
    </statement>
    <statement id="d0_s14">
      <text>rachillas not prolonged.</text>
      <biological_entity id="o779" name="rachilla" name_original="rachillas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="length" src="d0_s14" value="prolonged" value_original="prolonged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes usually longer than the florets, narrow, more or less keeled, hyaline to chartaceous, 1-5 (7) -veined, apices usually acute or acuminate, rarely muticous or mucronate, remaining open after the floret falls;</text>
      <biological_entity id="o780" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than the florets" constraintid="o781" is_modifier="false" name="length_or_size" src="d0_s15" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-5(7)-veined" value_original="1-5(7)-veined" />
      </biological_entity>
      <biological_entity id="o781" name="floret" name_original="florets" src="d0_s15" type="structure" />
      <biological_entity id="o782" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
        <character constraint="after floret" constraintid="o783" is_modifier="false" name="architecture" src="d0_s15" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o783" name="floret" name_original="floret" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>florets at least 5 times longer than wide;</text>
      <biological_entity id="o784" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses strigose, usually sharp;</text>
      <biological_entity id="o785" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s17" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas usually coriaceous or indurate, 3-5 (7) -veined, margins thin, usually convolute, rarely involute, apices glabrous or pilose, with 0-2 minute to small, membranous lobes, awned, lemma-awn junction evident;</text>
      <biological_entity id="o786" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s18" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="texture" src="d0_s18" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="3-5(7)-veined" value_original="3-5(7)-veined" />
      </biological_entity>
      <biological_entity id="o787" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="width" src="d0_s18" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s18" value="convolute" value_original="convolute" />
        <character is_modifier="false" modifier="rarely" name="shape_or_vernation" src="d0_s18" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o788" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o789" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s18" to="2" />
        <character char_type="range_value" from="minute" is_modifier="true" name="size" src="d0_s18" to="small" />
        <character is_modifier="true" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="lemma-awn" id="o790" name="junction" name_original="junction" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o788" id="r133" name="with" negation="false" src="d0_s18" to="o789" />
    </statement>
    <statement id="d0_s19">
      <text>awns once or twice-geniculate;</text>
      <biological_entity id="o791" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="2 times-geniculate" value_original="2 times-geniculate " />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas from shorter than to subequal to the lemmas, flat between the veins, 0-2-veined;</text>
      <biological_entity id="o792" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character constraint="than to subequal to the lemmas" constraintid="o793" is_modifier="false" name="height_or_length_or_size" src="d0_s20" value="shorter" value_original="shorter" />
        <character constraint="between veins" constraintid="o794" is_modifier="false" modifier="from" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s20" value="0-2-veined" value_original="0-2-veined" />
      </biological_entity>
      <biological_entity id="o793" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character is_modifier="true" name="size" src="d0_s20" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o794" name="vein" name_original="veins" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>lodicules 3 or 2;</text>
      <biological_entity id="o795" name="lodicule" name_original="lodicules" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s21" unit="or" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>anthers 3, frequently penicillate;</text>
      <biological_entity id="o796" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="3" value_original="3" />
        <character is_modifier="false" modifier="frequently" name="shape" src="d0_s22" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>ovaries glabrous.</text>
      <biological_entity id="o797" name="ovary" name_original="ovaries" src="d0_s23" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s23" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Caryopses fusiform, terete;</text>
      <biological_entity id="o798" name="caryopse" name_original="caryopses" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s24" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>hila linear, nearly as long as the caryopses.</text>
      <biological_entity id="o800" name="caryopse" name_original="caryopses" src="d0_s25" type="structure" />
      <relation from="o799" id="r134" modifier="nearly" name="as long as" negation="false" src="d0_s25" to="o800" />
    </statement>
    <statement id="d0_s26">
      <text>x = unknown.</text>
      <biological_entity id="o799" name="hilum" name_original="hila" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s25" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o801" name="chromosome" name_original="" src="d0_s26" type="structure" />
    </statement>
  </description>
  <discussion>Austrostipa is a genus of 63 species, all of which are native to Australasia. Two species are cultivated in the Flora region.</discussion>
  <references>
    <reference>Jacobs, S.W.L. and J. Everett. 1996. Austrostipa, a new genus, and new names for Australasian species formerly included in Stipa (Gramineae). Telopea 6:579-595.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants shrubby; panicle branches and pedicels plumose, hairs 1.5 mm or longer</description>
      <determination>1 Austrostipa elegantissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants bamboolike; panicle branches and pedicels not plumose, if hairy then the hairs to 0.3 mm long</description>
      <determination>2 Austrostipa ramosissima</determination>
    </key_statement>
  </key>
</bio:treatment>