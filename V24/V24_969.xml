<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">685</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">CYNOSURUS</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">cristatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus cynosurus;species cristatus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Crested dogtail</other_name>
  <other_name type="common_name">Cynosure accrêté</other_name>
  <other_name type="common_name">Crételle des prés</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o19587" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (5) 15-75 (90) cm.</text>
      <biological_entity id="o19588" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="75" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths smooth, glabrous;</text>
      <biological_entity id="o19589" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-2.5 mm, truncate, erose or ciliolate;</text>
      <biological_entity id="o19590" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-15 (19) cm long, 0.5-2 (4.3) mm wide, glabrous or pubescent.</text>
      <biological_entity id="o19591" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="19" value_original="19" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="4.3" value_original="4.3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles (1) 3.5-14 cm long, 0.4-1 cm wide, linear, spikelike, unilateral.</text>
      <biological_entity id="o19592" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="14" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="architecture_or_orientation_or_position" src="d0_s6" value="unilateral" value_original="unilateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 3-7 mm, subsessile or shortly pedicellate, pedicels to 1 mm.</text>
      <biological_entity id="o19593" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o19594" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sterile spikelets strongly laterally compressed, with 6-11 (18) florets;</text>
      <biological_entity id="o19595" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="strongly laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o19596" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="18" value_original="18" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
      <relation from="o19595" id="r3108" name="with" negation="false" src="d0_s8" to="o19596" />
    </statement>
    <statement id="d0_s9">
      <text>glumes and lemmas similar, linear-lanceolate, keeled, keels ciliate, apices acuminate to awned, awns to 1 mm.</text>
      <biological_entity id="o19597" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o19598" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o19599" name="keel" name_original="keels" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19600" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s9" to="awned" />
      </biological_entity>
      <biological_entity id="o19601" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fertile spikelets with 2-5 florets, glumes and lemmas dissimilar;</text>
      <biological_entity id="o19602" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o19603" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o19604" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o19605" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o19602" id="r3109" name="with" negation="false" src="d0_s10" to="o19603" />
      <relation from="o19602" id="r3110" name="with" negation="false" src="d0_s10" to="o19604" />
      <relation from="o19602" id="r3111" name="with" negation="false" src="d0_s10" to="o19605" />
    </statement>
    <statement id="d0_s11">
      <text>glumes 2.8-5.1 mm long, 0.6-0.9 mm wide, 1-veined, laterally compressed, hyaline, keeled, acute;</text>
      <biological_entity id="o19606" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s11" to="5.1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes 0.4-0.6 mm;</text>
      <biological_entity constraint="rachilla" id="o19607" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 3-4.5 mm long, 0.6-1.1 mm wide, dorsally compressed, membranous to subcoriaceous, not keeled, margins hyaline, ciliolate, apices obtuse to acute, unawned or awned, awns to 3 mm;</text>
      <biological_entity id="o19608" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s13" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s13" to="subcoriaceous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o19609" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o19610" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o19611" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.8-3 mm. 2n = 14.</text>
      <biological_entity id="o19612" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19613" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynosurus cristatus is a European native that is now established in North America. It grows in a wide range of soils in dry or damp habitats. In Europe it is used for fodder and pasture, especially for sheep, but in North America it is regarded as a weedy species. It is self-incompatible.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.;Oreg.;Conn.;N.J.;N.Y.;Wash.;Del.;Wis.;W.Va.;N.H.;N.C.;Tenn.;Pa.;R.I.;B.C.;Nfld. and Labr. (Labr.);N.S.;Ont.;Que.;Va.;Colo.;Calif.;Vt.;Idaho;Maine;Md.;Mass.;Ohio;Mich.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>