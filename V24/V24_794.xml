<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">568</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Asch. &amp; Graebn." date="unknown" rank="section">Pandemos</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">trivialis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section pandemos;species trivialis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>49</number>
  <other_name type="common_name">Rough bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, short-lived;</text>
    </statement>
    <statement id="d0_s1">
      <text>somewhat loosely to densely tufted, usually weakly stolon¬iferous.</text>
      <biological_entity id="o15079" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" modifier="loosely to densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intravaginal.</text>
      <biological_entity id="o15080" name="stolon" name_original="stolon" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 25-120 cm, decumbent to erect, sometimes trailing and rooting at the nodes, terete or weakly compressed;</text>
      <biological_entity id="o15081" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s3" to="120" to_unit="cm" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="trailing" value_original="trailing" />
        <character constraint="at nodes" constraintid="o15082" is_modifier="false" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o15082" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>nodes terete or slightly compressed, (0) 1-3 exserted.</text>
      <biological_entity id="o15083" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character name="atypical_quantity" src="d0_s4" value="0" value_original="0" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for about 1/3-1/2 their length, compressed, usually densely scabrous, bases of basal sheaths glabrous, distal sheath lengths 0.5-4 times blade lengths;</text>
      <biological_entity id="o15084" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="usually densely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15085" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15086" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o15087" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o15088" is_modifier="false" name="length" src="d0_s5" value="0.5-4 times blade lengths" value_original="0.5-4 times blade lengths" />
      </biological_entity>
      <biological_entity id="o15088" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o15085" id="r2404" name="part_of" negation="false" src="d0_s5" to="o15086" />
    </statement>
    <statement id="d0_s6">
      <text>collars smooth or scabrous, glabrous;</text>
      <biological_entity id="o15089" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 3-10 mm, scabrous, acute to acuminate;</text>
      <biological_entity id="o15090" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 1-5 mm wide, flat, lax, soft, sparsely scabrous over the veins, margins scabrous, apices narrowly prow-shaped.</text>
      <biological_entity id="o15091" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="soft" value_original="soft" />
        <character constraint="over veins" constraintid="o15092" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15092" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o15093" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15094" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 8-25 cm, erect or lax, pyramidal, open, with 35-100+ spikelets;</text>
      <biological_entity id="o15095" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s9" to="25" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s9" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o15096" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="35" is_modifier="true" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
      <relation from="o15095" id="r2405" name="with" negation="false" src="d0_s9" to="o15096" />
    </statement>
    <statement id="d0_s10">
      <text>nodes with 3-7 branches;</text>
      <biological_entity id="o15097" name="node" name_original="nodes" src="d0_s10" type="structure" />
      <biological_entity id="o15098" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
      <relation from="o15097" id="r2406" name="with" negation="false" src="d0_s10" to="o15098" />
    </statement>
    <statement id="d0_s11">
      <text>branches 2-8 (10) cm, ascending to spreading, flexuous to fairly straight, angled, angles densely scabrous, crowded, with 5-35 spikelets in the distal 1/2-3/4.</text>
      <biological_entity id="o15099" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="8" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="spreading" />
        <character char_type="range_value" from="flexuous" name="course" src="d0_s11" to="fairly straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o15100" name="angle" name_original="angles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o15101" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="35" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15102" name="1/2-3/4" name_original="1/2-3/4" src="d0_s11" type="structure" />
      <relation from="o15100" id="r2407" name="with" negation="false" src="d0_s11" to="o15101" />
      <relation from="o15101" id="r2408" name="in" negation="false" src="d0_s11" to="o15102" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 2.3-3.5 mm, lengths to 3 times widths, laterally compressed;</text>
      <biological_entity id="o15103" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="0-3" value_original="0-3" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-4, bisexual;</text>
      <biological_entity id="o15104" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes smooth or muriculate.</text>
      <biological_entity constraint="rachilla" id="o15105" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s14" value="muriculate" value_original="muriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes distinctly keeled, keels scabrous;</text>
      <biological_entity id="o15106" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15107" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes subulate to narrowly lanceolate, usually arched to sickle-shaped, 1-veined, distinctly shorter than the lowest lemmas;</text>
      <biological_entity constraint="lower" id="o15108" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s16" to="narrowly lanceolate usually arched" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s16" to="narrowly lanceolate usually arched" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-veined" value_original="1-veined" />
        <character constraint="than the lowest lemmas" constraintid="o15109" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="distinctly shorter" value_original="distinctly shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o15109" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>calluses webbed, hairs over 2/3 the lemma length;</text>
      <biological_entity id="o15110" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o15111" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <biological_entity id="o15112" name="lemma" name_original="lemma" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2/3" value_original="2/3" />
      </biological_entity>
      <relation from="o15111" id="r2409" name="over" negation="false" src="d0_s17" to="o15112" />
    </statement>
    <statement id="d0_s18">
      <text>lemmas 2.3-3.5 mm, lanceolate, distinctly keeled, keels usually sparsely puberulent to 3/5 their length, marginal veins usually glabrous, infrequently the proximal 1/4 softly puberulent, intercostal regions smooth, glabrous, upper lemmas sometimes glabrous, lateral-veins prominent, margins glabrous, apices acute;</text>
      <biological_entity id="o15113" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s18" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15114" name="keel" name_original="keels" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="0" name="length" src="d0_s18" to="3/5" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o15115" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently" name="position" src="d0_s18" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s18" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o15116" name="region" name_original="regions" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o15117" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15118" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o15119" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15120" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>palea keels smooth, muriculate, tuberculate, or minutely scabrous;</text>
      <biological_entity constraint="palea" id="o15121" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" name="relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s19" value="muriculate" value_original="muriculate" />
        <character is_modifier="false" name="relief" src="d0_s19" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="relief" src="d0_s19" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s19" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 1.3-2 mm. 2n = 14.</text>
      <biological_entity id="o15122" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s20" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15123" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa trivialis is an introduced European species. Only Poa trivialis subsp. trivialis is present in the Flora region. Several cultivars have been planted for pastures and lawns, and have often escaped cultivation. Poa trivialis sometimes grows with P. paludigena (p. 572), but has distinctly longer ligules and anthers. It is easily recognized by its flat blades, long ligules, sickle-shaped lower glumes, prominent callus webs, and lemmas with pubescent keels and pronounced lateral veins.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C.;Wis.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Wyo.;B.C.;Greenland;N.B.;Nfld. and Labr. (Labr.);N.S.;Ont.;P.E.I.;Que.;Sask.;Yukon;N.Mex.;Tex.;La.;N.C.;Nebr.;Tenn.;Pa.;Calif.;Nev.;Va.;Colo.;Md.;Alaska;Ala.;Ill.;Ga.;Ind.;Iowa;Idaho;Mont.;Oreg.;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Ky.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>