<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subsection">Poa nervosa Complex</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">rhizomata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection poa nervosa complex;species rhizomata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>32</number>
  <other_name type="common_name">Rhizome bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually unisexual;</text>
    </statement>
    <statement id="d0_s2">
      <text>loosely tufted or with solitary shoots, shortly rhizom¬atous.</text>
      <biological_entity id="o30674" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching all or mainly extra vaginal.</text>
      <biological_entity id="o30673" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s1" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s2" value="with solitary shoots" value_original="with solitary shoots" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="mainly" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 20-65 cm, erect or the bases decumbent, not branching above the base, terete or weakly compressed;</text>
      <biological_entity id="o30675" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s4" to="65" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o30676" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character constraint="above base" constraintid="o30677" is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o30677" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>nodes terete, 1-2 exserted.</text>
      <biological_entity id="o30678" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sheaths closed for 1/2-1/3 their length, slightly compressed, keels moderately distinct, smooth or sparsely to moderately scabrous, glabrous, bases of basal sheaths glabrous, distal sheath lengths 1.5-4.4 (5.7) times blade lengths;</text>
      <biological_entity id="o30679" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o30680" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s6" to="sparsely moderately scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30681" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o30682" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o30683" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character constraint="blade" constraintid="o30684" is_modifier="false" name="length" src="d0_s6" value="1.5-4.4(5.7) times blade lengths" value_original="1.5-4.4(5.7) times blade lengths" />
      </biological_entity>
      <biological_entity id="o30684" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <relation from="o30681" id="r4829" name="part_of" negation="false" src="d0_s6" to="o30682" />
    </statement>
    <statement id="d0_s7">
      <text>collars smooth, glabrous;</text>
      <biological_entity id="o30685" name="collar" name_original="collars" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules of cauline leaves 2-8 mm, smooth or scabrous, acute to acuminate, innovation ligules 2-5 mm;</text>
      <biological_entity id="o30686" name="ligule" name_original="ligules" src="d0_s8" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30687" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="innovation" id="o30688" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o30686" id="r4830" name="part_of" negation="false" src="d0_s8" to="o30687" />
    </statement>
    <statement id="d0_s9">
      <text>innovation blades to 20 cm, otherwise similar to the cauline blades;</text>
      <biological_entity constraint="innovation" id="o30689" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30690" name="blade" name_original="blades" src="d0_s9" type="structure" />
      <relation from="o30689" id="r4831" modifier="otherwise" name="to" negation="false" src="d0_s9" to="o30690" />
    </statement>
    <statement id="d0_s10">
      <text>cauline blades gradually reduced in length distally, 1-3.5 mm wide, usually flat or folded, soft, thin, somewhat lax, smooth or sparsely scabrous, primarily over the veins and margins, distinctly keeled, apices narrowly to broadly prow-shaped, flag leaf-blades (1.4) 3-6 (8) cm.</text>
      <biological_entity constraint="cauline" id="o30691" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="length" src="d0_s10" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s10" value="folded" value_original="folded" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="soft" value_original="soft" />
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distinctly" name="shape" notes="" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o30692" name="vein" name_original="veins" src="d0_s10" type="structure" />
      <biological_entity id="o30693" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o30694" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s10" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o30695" name="blade-leaf" name_original="leaf-blades" src="d0_s10" type="structure">
        <character name="atypical_distance" src="d0_s10" unit="cm" value="1.4" value_original="1.4" />
        <character char_type="range_value" from="3" from_unit="cm" name="distance" src="d0_s10" to="6" to_unit="cm" />
      </biological_entity>
      <relation from="o30691" id="r4832" modifier="primarily" name="over" negation="false" src="d0_s10" to="o30692" />
      <relation from="o30691" id="r4833" modifier="primarily" name="over" negation="false" src="d0_s10" to="o30693" />
    </statement>
    <statement id="d0_s11">
      <text>Panicles (2) 4-10 cm, nodding, ovoid, sparse, with 20-50 spikelets, proximal internodes usually 1.8-3 cm;</text>
      <biological_entity id="o30696" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s11" to="10" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="count_or_density" src="d0_s11" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o30697" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s11" to="50" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30698" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s11" to="3" to_unit="cm" />
      </biological_entity>
      <relation from="o30696" id="r4834" name="with" negation="false" src="d0_s11" to="o30697" />
    </statement>
    <statement id="d0_s12">
      <text>nodes with 1-2 (4) branches;</text>
      <biological_entity id="o30699" name="node" name_original="nodes" src="d0_s12" type="structure" />
      <biological_entity id="o30700" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="4" value_original="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <relation from="o30699" id="r4835" name="with" negation="false" src="d0_s12" to="o30700" />
    </statement>
    <statement id="d0_s13">
      <text>branches 1.5-4.5 cm, ascending to spreading, lax, terete to weakly angled, angles sparsely to moderately scabrous, with 2-7 spikelets.</text>
      <biological_entity id="o30701" name="branch" name_original="branches" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s13" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s13" to="spreading" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s13" value="lax" value_original="lax" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s13" to="weakly angled" />
      </biological_entity>
      <biological_entity id="o30702" name="angle" name_original="angles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o30703" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
      <relation from="o30702" id="r4836" name="with" negation="false" src="d0_s13" to="o30703" />
    </statement>
    <statement id="d0_s14">
      <text>Spikelets (4) 6-9 (12) mm, lengths to 3.5 times widths, laterally compressed, not sexually dimorphic;</text>
      <biological_entity id="o30704" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="0-3.5" value_original="0-3.5" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>florets 3-8, usually unisexual;</text>
      <biological_entity id="o30705" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="8" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s15" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>rachilla internodes smooth or sparsely scabrous, usually glabrous, infrequently sparsely puberulent.</text>
      <biological_entity constraint="rachilla" id="o30706" name="internode" name_original="internodes" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently sparsely" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Glumes 3/5 – 4/5 as long as the adjacent lemmas, narrowly lanceolate to lanceolate, distinctly keeled, keels scabrous;</text>
      <biological_entity id="o30707" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o30708" from="3/5" name="quantity" src="d0_s17" to="4/5" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" notes="" src="d0_s17" to="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o30708" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s17" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o30709" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s17" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lower glumes 1-3 (5) -veined;</text>
      <biological_entity constraint="lower" id="o30710" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="1-3(5)-veined" value_original="1-3(5)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>calluses webbed, hairs over 1/2 the lemma length;</text>
      <biological_entity id="o30711" name="callus" name_original="calluses" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o30712" name="hair" name_original="hairs" src="d0_s19" type="structure" />
      <biological_entity id="o30713" name="lemma" name_original="lemma" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o30712" id="r4837" name="over" negation="false" src="d0_s19" to="o30713" />
    </statement>
    <statement id="d0_s20">
      <text>lemmas 4-6.5 mm, lanceolate, 5-7-veined, distinctly keeled, keels and marginal veins sparsely short to long-villous, lateral-veins moderately prominent, intercostal regions sparsely scabrous, glabrous, margins glabrous, apices acute;</text>
      <biological_entity id="o30714" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s20" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s20" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s20" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o30715" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s20" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o30716" name="vein" name_original="veins" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sparsely" name="height_or_length_or_size" src="d0_s20" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o30717" name="lateral-vein" name_original="lateral-veins" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s20" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o30718" name="region" name_original="regions" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30719" name="margin" name_original="margins" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30720" name="apex" name_original="apices" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>palea keels scabrous;</text>
      <biological_entity constraint="palea" id="o30721" name="keel" name_original="keels" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s21" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>anthers vestigial (0.1-0.2 mm) or 2.5-4 mm. 2n =28.</text>
      <biological_entity id="o30722" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s22" value="2.5-4 mm" value_original="2.5-4 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30723" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa rhizomata is a rare species that grows in upper elevation, mixed coniferous forests on ultramafic (gabro or peridotite) rocks of the Klamath-Siskiyou region. It is subdioecious.</discussion>
  <discussion>Poa rhizomata resembles P. pratensis (p. 522), differing in having acute ligules, sparse inflorescences, florets that are usually unisexual florets, and generally larger spikelets. It also resembles P. chambersii (see next), but has more open sheaths, longer ligules, more pubescent lemmas, and a more well-developed web. It used to include P. piperi (p. 554), which differs in having involute, adaxially hairy leaves and glabrous lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>