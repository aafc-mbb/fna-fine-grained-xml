<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Edward E. Terrell;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">LOLIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus lolium</taxon_hierarchy>
  </taxon_identification>
  <number>14.05</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes shortly rhizomatous.</text>
      <biological_entity id="o8459" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-150 cm, slender to stout, erect to decumbent, rarely prostrate.</text>
      <biological_entity id="o8460" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" from="slender" name="size" src="d0_s2" to="stout" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect to decumbent" value_original="erect to decumbent" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open, rounded, glabrous, sometimes scabrous;</text>
      <biological_entity id="o8461" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules to 4 mm, membranous, glabrous;</text>
      <biological_entity id="o8462" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles sometimes present;</text>
      <biological_entity id="o8463" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat, linear.</text>
      <biological_entity id="o8464" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences distichous spikes, with solitary spikelets oriented radial to the rachises, perpendicular to the rachis concavities.</text>
      <biological_entity id="o8465" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character constraint="to rachises" constraintid="o8468" is_modifier="false" name="arrangement" src="d0_s7" value="radial" value_original="radial" />
        <character constraint="to rachis concavities" constraintid="o8469" is_modifier="false" name="orientation" notes="" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o8466" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="distichous" value_original="distichous" />
      </biological_entity>
      <biological_entity id="o8467" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="oriented" value_original="oriented" />
      </biological_entity>
      <biological_entity id="o8468" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
      <biological_entity constraint="rachis" id="o8469" name="concavity" name_original="concavities" src="d0_s7" type="structure" />
      <relation from="o8465" id="r1376" name="with" negation="false" src="d0_s7" to="o8467" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets laterally compressed, with 2-22 florets, distal florets reduced;</text>
      <biological_entity id="o8470" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o8471" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="22" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8472" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o8470" id="r1377" name="with" negation="false" src="d0_s8" to="o8471" />
    </statement>
    <statement id="d0_s9">
      <text>rachillas glabrous;</text>
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes, beneath the florets.</text>
      <biological_entity id="o8473" name="rachilla" name_original="rachillas" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8474" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <biological_entity id="o8475" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o8473" id="r1378" name="above" negation="false" src="d0_s10" to="o8474" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes usually 1, 2 in the terminal spikelets, lanceolate to oblong, rounded over the midvein, membranous to indurate, 3-9-veined, unawned;</text>
      <biological_entity id="o8476" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character constraint="in terminal spikelets" constraintid="o8477" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s11" to="oblong" />
        <character constraint="over midvein" constraintid="o8478" is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="membranous" name="texture" notes="" src="d0_s11" to="indurate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-9-veined" value_original="3-9-veined" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8477" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o8478" name="midvein" name_original="midvein" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes absent from all but the terminal spikelet;</text>
      <biological_entity constraint="lower" id="o8479" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="from terminal spikelet" constraintid="o8480" is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8480" name="spikelet" name_original="spikelet" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes from shorter than to exceeding the distal florets;</text>
      <biological_entity constraint="upper" id="o8481" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="than to exceeding the distal florets" constraintid="o8482" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8482" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s13" value="exceeding" value_original="exceeding" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses short, blunt, glabrous;</text>
      <biological_entity id="o8483" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s14" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas lanceolate, ovate or oblong, rounded over the midvein, membranous, chartaceous, 3-7-veined, apices sometimes hyaline, unawned or awned, awns subterminal, more or less straight;</text>
      <biological_entity id="o8484" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character constraint="over midvein" constraintid="o8485" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" notes="" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
      <biological_entity id="o8485" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o8486" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8487" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas membranous, usually smooth, keels ciliolate;</text>
      <biological_entity id="o8488" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8489" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lodicules 2, free, lanceolate to ovate;</text>
      <biological_entity id="o8490" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="free" value_original="free" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s17" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3;</text>
      <biological_entity id="o8491" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovaries glabrous.</text>
      <biological_entity id="o8492" name="ovary" name_original="ovaries" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses dorsally compressed, oblong, broadly elliptic or ovate, longitudinally sulcate;</text>
      <biological_entity id="o8493" name="caryopse" name_original="caryopses" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s20" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="longitudinally" name="architecture" src="d0_s20" value="sulcate" value_original="sulcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>hila linear, in the furrow;</text>
      <biological_entity id="o8494" name="hilum" name_original="hila" src="d0_s21" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s21" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o8495" name="furrow" name_original="furrow" src="d0_s21" type="structure" />
      <relation from="o8494" id="r1379" name="in" negation="false" src="d0_s21" to="o8495" />
    </statement>
    <statement id="d0_s22">
      <text>embryos 1/5 - 1/3 as long as the caryopses.</text>
      <biological_entity id="o8497" name="caryopse" name_original="caryopses" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>x = 7.</text>
      <biological_entity id="o8496" name="embryo" name_original="embryos" src="d0_s22" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o8497" from="1/5" name="quantity" src="d0_s22" to="1/3" />
      </biological_entity>
      <biological_entity constraint="x" id="o8498" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As interpreted here, Lolium comprises five species that are native to Europe, temperate Asia, and northern Africa. All have been introduced to the Flora region, often as forage grasses; most have become established.</discussion>
  <discussion>Lolium used to be included in the Triticeae, but evidence from genetics, morphology, and other studies shows its closest relationship to be to the species included here in Schedonorus. Artificial hybrids have been produced among L. perenne, L. multiflorum, Schedonorus pratensis, and S. arundinaceus. Cultivars of these crosses have been registered for commercial use and are sometimes used for forage. Natural hybrids are not uncommon in Europe.</discussion>
  <references>
    <reference>Aiken, S.G., M.J. Dallwitz, C.L. Mcjannet, and LX. Consaul. 1997. Fescue Grasses of North America: Interactive Identification and Information Retrieval. DELTA, CSIRO Division of Entomology, Canberra, Australia. CD-ROM</reference>
    <reference>Dannhardt, G. and L. Steindl. 1985. Alkaloids of Lolium temulentum: Isolation, identification and pharmacological activity. Pi. Med. (Stuttgart) 1985:212-214</reference>
    <reference>Dore, W.G. 1950. Persian darnel in Canada. Sci. Agric. (Ottawa) 30:157-164</reference>
    <reference>Soreng, R.J. and E.E. Terrell. 1997 [publication date 1998]. Taxonomic notes on Schedonorus, a segregate genus from Festuca or Lolium, with a new nothogenus, xSchedololium, and new combinations. Phytologia 83:85-88</reference>
    <reference>Terrell, E.E. 1968. A Taxonomic Revision of the Genus Lolium. Technical Bulletin, United States Department of Agriculture No. 1392. U.S. Government Printing Office, Washington, D.C., U.S.A. 65 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;Mont.;Utah;Conn.;N.J.;N.Y.;Wash.;Va.;W.Va.;Mich.;D.C;Wis.;Idaho;Oreg.;Wyo.;Pacific Islands (Hawaii);Alaska;Ala.;Ark.;Ariz.;Fla.;Ga.;Iowa;Ill.;Kans.;Ky.;La.;Mass.;Md.;Maine;Minn.;Mo.;Miss.;N.C.;N.Dak.;N.Mex.;Ohio;Okla.;Pa.;R.I.;S.C.;S.Dak.;Tenn.;Tex.;Vt.;N.H.;Nebr.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;Calif.;Nev.;Puerto Rico;Colo.;Ind.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants either long-lived perennials with 2-10 florets per spikelet, or annuals or short-lived perennials with 10-22 florets per spikelet.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants long-lived perennials, with 2-10 florets per spikelet; lemmas unawned or awned, awns to about 8 mm long</description>
      <determination>1 Lolium perenne</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants annuals or short-lived perennials, with 10-22 florets per spikelet; lemmas usually awned, awns to 15 mm long, rarely unawned</description>
      <determination>2 Lolium multiflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants annuals, with 2-10(11) florets per spikelet.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets somewhat sunken in the rachises and partly concealed by the glumes</description>
      <determination>3 Lolium rigidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets not sunken in the rachises and not concealed by the glumes.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemmas 3.5-8.5 mm long; paleas from 1.2 mm shorter than to 0.8 mm longer than the lemmas; mature florets and caryopses 2-3 times longer than wide</description>
      <determination>4 Lolium temulentum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemmas (5.2)7-12 mm long; paleas usually 0.5-1.8 mm longer than the lemmas; mature florets and caryopses 3.7-5 times longer than wide</description>
      <determination>5 Lolium persicum</determination>
    </key_statement>
  </key>
</bio:treatment>