<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ZIZANIA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(Fassett) Dore" date="unknown" rank="variety">interior</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus zizania;species palustris;variety interior</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zizania</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">interior</taxon_name>
    <taxon_hierarchy>genus zizania;species interior</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zizania</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aquatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">interior</taxon_name>
    <taxon_hierarchy>genus zizania;species aquatica;variety interior</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Interior wildrice</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1-3 m.</text>
      <biological_entity id="o27373" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 10-40+ mm wide.</text>
      <biological_entity id="o27374" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="40" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pistillate part of inflorescences 10-40+ cm wide;</text>
      <biological_entity id="o27375" name="part" name_original="part" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s2" to="40" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o27376" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <relation from="o27375" id="r4322" name="part_of" negation="false" src="d0_s2" to="o27376" />
    </statement>
    <statement id="d0_s3">
      <text>branches ascending to widely divergent;</text>
      <biological_entity id="o27377" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower pistillate branches with 9-30 spikelets.</text>
      <biological_entity constraint="lower" id="o27378" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27379" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s4" to="30" />
      </biological_entity>
      <relation from="o27378" id="r4323" name="with" negation="false" src="d0_s4" to="o27379" />
    </statement>
  </description>
  <discussion>Zizania palustris var. interior grows on muddy shores and in shallow water, mainly in the north central United States and adjacent Canada. It resembles Z. aquatica in its vegetative characters, and Z. palustris var. palustris in its pistillate spikelets. Crossing experiments (Duvall and Biesboer 1988) suggest that hybridization between the two may occur. Isozyme data (Warwick and Aiken 1986) do not support the hypothesis that Z. palustris var. interior is a hybrid.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ohio;N.Dak.;Calif.;Minn.;Mich.;Ala.;Wis.;S.Dak.;Tenn.;Iowa;Kans.;Nebr.;Ill.;Ind.;Mo.;Ky.;Man.;N.B.;N.S.;Ont.;P.E.I.;Que.;Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>