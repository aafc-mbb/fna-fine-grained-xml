<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Swallen) Barkworth" date="unknown" rank="species">latiglume</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species latiglume</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">latiglumis</taxon_name>
    <taxon_hierarchy>genus stipa;species latiglumis</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Wide-glumed needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tightly cespitose, not rhizomatous.</text>
      <biological_entity id="o5113" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="tightly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-110 cm tall, 0.7-1.2 mm thick, lower internodes retrorsely pilose, upper internodes glabrous;</text>
      <biological_entity id="o5114" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s1" to="110" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="thickness" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5115" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5116" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 2-4.</text>
      <biological_entity id="o5117" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths usually retrorsely pubescent, brown to gray-brown, flat when mature;</text>
      <biological_entity constraint="basal" id="o5118" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually retrorsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s3" to="gray-brown" />
        <character is_modifier="false" modifier="when mature" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars usually glabrous, sometimes with a few hairs at the sides;</text>
      <biological_entity id="o5119" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5120" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5121" name="side" name_original="sides" src="d0_s4" type="structure" />
      <relation from="o5119" id="r836" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o5120" />
      <relation from="o5120" id="r837" name="at" negation="false" src="d0_s4" to="o5121" />
    </statement>
    <statement id="d0_s5">
      <text>basal ligules 0.2-2.5 mm, truncate to rounded;</text>
      <biological_entity constraint="basal" id="o5122" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper ligules 1.2-3 mm, rounded to acute, ciliate;</text>
      <biological_entity constraint="upper" id="o5123" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 0.7-3 mm wide, straight to lax, abaxial surfaces smooth, glabrous, adaxial surfaces pubescent, scabrous.</text>
      <biological_entity id="o5124" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5125" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5126" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 15-30 cm long, 0.8-2 cm wide;</text>
      <biological_entity id="o5127" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches appressed to strongly ascending, longest branches 2.5-6.5 cm.</text>
      <biological_entity id="o5128" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="strongly ascending" />
      </biological_entity>
      <biological_entity constraint="longest" id="o5129" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes subequal, 12-15 mm long, 1.3-1.9 mm wide, 3-veined;</text>
      <biological_entity id="o5130" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>florets 8-9 mm long, 0.9-1.4 mm thick, fusiform, terete;</text>
      <biological_entity id="o5131" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="thickness" src="d0_s11" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses 0.7-1 mm, blunt to sharp;</text>
      <biological_entity id="o5132" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s12" to="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas evenly hairy, hairs 0.5-1.5 mm at midlength, apical hairs 1-2 mm, apical lobes to 1 mm, membranous;</text>
      <biological_entity id="o5133" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5134" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="at apical hairs" constraintid="o5135" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5135" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="position" src="d0_s13" value="midlength" value_original="midlength" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5136" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>awns 33-45 mm, persistent, twice-geniculate, first 2 segments pilose, with hairs 0.5-2 mm, terminal segment mostly scabrous, straight;</text>
      <biological_entity id="o5137" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="33" from_unit="mm" name="some_measurement" src="d0_s14" to="45" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character constraint="segment" constraintid="o5138" is_modifier="false" name="size_or_quantity" src="d0_s14" value="2 times-geniculate first 2 segments pilose" />
        <character constraint="segment" constraintid="o5139" is_modifier="false" name="size_or_quantity" src="d0_s14" value="2 times-geniculate first 2 segments pilose" />
      </biological_entity>
      <biological_entity id="o5138" name="segment" name_original="segments" src="d0_s14" type="structure" />
      <biological_entity id="o5139" name="segment" name_original="segments" src="d0_s14" type="structure" />
      <biological_entity id="o5140" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5141" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o5137" id="r838" name="with" negation="false" src="d0_s14" to="o5140" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 4-5 mm, 3/5 – 4/5 as long as the lemmas, pubescent;</text>
      <biological_entity id="o5142" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o5143" from="3/5" name="quantity" src="d0_s15" to="4/5" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5143" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers not seen.</text>
      <biological_entity id="o5144" name="anther" name_original="anthers" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Caryopses not seen.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 70.</text>
      <biological_entity id="o5145" name="caryopse" name_original="caryopses" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o5146" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Achnatherum latiglume usually grows on dry slopes in yellow pine forests of southern California. Pohl (1954) demonstrated that it is an alloploid derivative of A. nelsonii and A. lemmonii. He reported being told that it was a fairly common species in the Yosemite Valley, and suggested that the isolated occurrences in Riverside and Fresno counties might represent separate origins of the species.</discussion>
  <discussion>Achnatherum latiglume resembles A. nevadense and A. occidental, but the latter two species have sharper calluses, and their paleas tend to be thinner and somewhat shorter relative to the lemmas than those of A. latiglume.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>