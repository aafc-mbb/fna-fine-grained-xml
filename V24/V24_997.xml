<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">704</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">AMPHIBROMUS</taxon_name>
    <taxon_name authority="(Hook, f.) Baill." date="unknown" rank="species">nervosus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus amphibromus;species nervosus</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Common swamp wallabygrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, occasionally rhizomatous.</text>
      <biological_entity id="o3105" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-125 cm tall, 1-3 mm thick, erect, terete to flattened, glabrous;</text>
      <biological_entity id="o3106" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="height" src="d0_s2" to="125" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s2" to="flattened" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 2-5;</text>
      <biological_entity id="o3107" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lowest internodes not swollen.</text>
      <biological_entity constraint="lowest" id="o3108" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths smooth or scabridulous, ribbed;</text>
      <biological_entity id="o3109" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 10-20 mm, acute to acuminate;</text>
      <biological_entity id="o3110" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 10-30 cm long, 1.5-3.5 mm wide, flat or involute, smooth to scabrous, abaxial surfaces scabridulous, adaxial surfaces deeply ribbed, scabrous.</text>
      <biological_entity id="o3111" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="smooth to scabrous" value_original="smooth to scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3112" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3113" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Terminal panicles 15-40 cm, erect, sparse, the lower portion sometimes partially enclosed in the uppermost sheaths;</text>
      <biological_entity constraint="terminal" id="o3114" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s8" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="count_or_density" src="d0_s8" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3115" name="portion" name_original="portion" src="d0_s8" type="structure" />
      <biological_entity constraint="uppermost" id="o3116" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o3115" id="r493" modifier="sometimes partially" name="enclosed in the" negation="false" src="d0_s8" to="o3116" />
    </statement>
    <statement id="d0_s9">
      <text>branches usually 7-15 cm, ascending or appressed, often flexuous;</text>
      <biological_entity id="o3117" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s9" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels usually 10-20 mm.</text>
      <biological_entity id="o3118" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 10-16 mm, with 4-6 florets.</text>
      <biological_entity id="o3119" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3120" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
      <relation from="o3119" id="r494" name="with" negation="false" src="d0_s11" to="o3120" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal to subequal, green, sometimes purplish in the center, with hyaline margins;</text>
      <biological_entity id="o3121" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="unequal" name="size" src="d0_s12" to="subequal" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character constraint="in center" constraintid="o3122" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o3122" name="center" name_original="center" src="d0_s12" type="structure" />
      <biological_entity id="o3123" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o3121" id="r495" name="with" negation="false" src="d0_s12" to="o3123" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 2.5-5.5 mm, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o3124" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 3-6.5 mm, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o3125" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 5-7.2 mm, 5-7-veined, scabrous, apices not appearing constricted, usually 4-toothed, 2 lateral teeth smaller than the 2 central teeth, awned from the lower 2/5 – 3/5 of the lemmas, awns 12-22 mm, geniculate and twisted;</text>
      <biological_entity id="o3126" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3127" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character constraint="than the 2 central teeth" constraintid="o3129" is_modifier="false" name="size" src="d0_s15" value="smaller" value_original="smaller" />
        <character constraint="from lower 2/5-3/5" constraintid="o3130" is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3128" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character is_modifier="true" modifier="usually" name="shape" src="d0_s15" value="4-toothed" value_original="4-toothed" />
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="central" id="o3129" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3130" name="2/5-3/5" name_original="2/5-3/5" src="d0_s15" type="structure" />
      <biological_entity id="o3131" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o3132" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s15" to="22" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="twisted" value_original="twisted" />
      </biological_entity>
      <relation from="o3127" id="r496" name="appearing" negation="false" src="d0_s15" to="o3128" />
      <relation from="o3130" id="r497" name="part_of" negation="false" src="d0_s15" to="o3131" />
    </statement>
    <statement id="d0_s16">
      <text>paleas 3/4 as long as to subequal to the lemmas, papillose;</text>
      <biological_entity id="o3133" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3/4" value_original="3/4" />
        <character constraint="to lemmas" constraintid="o3134" is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="relief" notes="" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o3134" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers of chasmogamous florets 2.2-3 mm, those of cleistogamous florets 0.3-1.4 mm. 2n = unknown.</text>
      <biological_entity id="o3135" name="anther" name_original="anthers" src="d0_s17" type="structure" constraint="floret" constraint_original="floret; floret">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3136" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o3137" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3138" name="chromosome" name_original="" src="d0_s17" type="structure" />
      <relation from="o3135" id="r498" name="part_of" negation="false" src="d0_s17" to="o3136" />
      <relation from="o3135" id="r499" name="part_of" negation="false" src="d0_s17" to="o3137" />
    </statement>
  </description>
  <discussion>Amphibromus nervosus is the most common species in the genus. It has frequently been misidentified as A. neesii, but has a lower awn insertion. Such misidentification is the basis of the report of A. neesii in North America; examination of the voucher specimens showed them to be A. nervosus. They were collected in 1990 from a vernal pool in Sacramento County, California. Its seeds had been found earlier as a contaminant in Trifolium subterraneum seed being imported from Australia. The discovery of living plants is of particular concern, because of their ability to invade and survive in vernal pools.</discussion>
  
</bio:treatment>