<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Homalopoa</taxon_name>
    <taxon_name authority="Scribn. &amp; Merr." date="unknown" rank="species">paucispicula</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section homalopoa;species paucispicula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">leptocoma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">paucispicula</taxon_name>
    <taxon_hierarchy>genus poa;species leptocoma;variety paucispicula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">leptocoma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">paucispicula</taxon_name>
    <taxon_hierarchy>genus poa;species leptocoma;subspecies paucispicula</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Few-flower bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>slightly or loosely tufted, not stoloniferous, not rhizomatous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching mainly extravaginal.</text>
      <biological_entity id="o5237" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 10-30 cm.</text>
      <biological_entity id="o5238" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths closed for 1/4 - 3/5 their length, terete;</text>
      <biological_entity id="o5239" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm, smooth or sparsely scabrous, truncate to obtuse;</text>
      <biological_entity id="o5240" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-3 mm wide, flat, thin, soft, apices broadly prow-shaped.</text>
      <biological_entity id="o5241" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o5242" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2.5-10 cm, lax to nearly erect, open, sparse, with 1-2 branches per node;</text>
      <biological_entity id="o5243" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="count_or_density" src="d0_s7" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o5244" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o5245" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o5243" id="r849" name="with" negation="false" src="d0_s7" to="o5244" />
      <relation from="o5244" id="r850" name="per" negation="false" src="d0_s7" to="o5245" />
    </statement>
    <statement id="d0_s8">
      <text>branches (2) 3-6 cm, ascending to spreading, lower branches infrequently reflexed, lax or drooping, capillary, terete to slightly sulcate, usually smooth, rarely some branches within a panicle sparsely scabrous, with 1-3 (5) spikelets.</text>
      <biological_entity id="o5246" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5247" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="infrequently" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5248" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity id="o5249" name="panicle" name_original="panicle" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5250" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o5248" id="r851" modifier="rarely" name="within" negation="false" src="d0_s8" to="o5249" />
      <relation from="o5248" id="r852" name="with" negation="false" src="d0_s8" to="o5250" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-6 mm, laterally compressed, broadly lanceolate to ovate, usually dark purple, with 3-5 florets;</text>
      <biological_entity id="o5251" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s9" to="ovate" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <biological_entity id="o5252" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o5251" id="r853" name="with" negation="false" src="d0_s9" to="o5252" />
    </statement>
    <statement id="d0_s10">
      <text>rachilla internodes smooth, glabrous.</text>
      <biological_entity constraint="rachilla" id="o5253" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes lanceolate to broadly lanceolate, thin, distinctly keeled, keels smooth or nearly so;</text>
      <biological_entity id="o5254" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="broadly lanceolate" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o5255" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s11" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 1-veined;</text>
      <biological_entity constraint="lower" id="o5256" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes shorter than or subequal to the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o5257" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="than or subequal to the lowest lemmas" constraintid="o5258" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o5258" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses sparsely webbed;</text>
      <biological_entity id="o5259" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="webbed" value_original="webbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 3-4 mm, broadly lanceolate, usually strongly purple, distinctly keeled, thin, keels and marginal veins short to long-villous, keels hairy for 1/2-1/3 their length, lateral-veins glabrous, intercostal regions smooth, glabrous, margins glabrous, not infolded, apices acute, sometimes slightly bronze-colored;</text>
      <biological_entity id="o5260" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually strongly" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o5261" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o5262" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o5263" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5264" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o5265" name="region" name_original="regions" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5266" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="infolded" value_original="infolded" />
      </biological_entity>
      <biological_entity id="o5267" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes slightly" name="coloration" src="d0_s15" value="bronze-colored" value_original="bronze-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>palea keels sparsely scabrous at midlength;</text>
      <biological_entity constraint="palea" id="o5268" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character constraint="at midlength" constraintid="o5269" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5269" name="midlength" name_original="midlength" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.4-1 mm. 2n = 28, 42.</text>
      <biological_entity id="o5270" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5271" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa paucispicula grows in arctic and alpine regions, from the north coast of Alaska and the western Northwest Territories south to Washington, Idaho, and Wyoming; it also grows in arctic far east Russia. It is a delicate species that prefers open, mesic, rocky slopes. It has sometimes been included in P. leptocoma (p. 573), a member of Poa sect. Oreinos. It differs from P. leptocoma in having smoother branches, fewer spikelets, and broader glumes. Chloroplast DNA studies confirm that it is not closely related to species of sect. Oreinos; ITS data support its relationship to P. leptocoma.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.;Alaska;Wash.;Alta.;B.C.;N.W.T.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>