<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="genus">PSATHYROSTACHYS</taxon_name>
    <taxon_name authority="(Fisch.) Nevski" date="unknown" rank="species">juncea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus psathyrostachys;species juncea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">junceus</taxon_name>
    <taxon_hierarchy>genus elymus;species junceus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Russian wildrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose.</text>
      <biological_entity id="o11021" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (20) 30-80 (120) cm, erect or decumbent at the base, mostly glabrous, pubescent below the spikes.</text>
      <biological_entity id="o11022" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o11023" is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="mostly" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="below spikes" constraintid="o11024" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o11023" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o11024" name="spike" name_original="spikes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Basal sheaths glabrous, grayish-brown, old sheaths more or less persistent;</text>
      <biological_entity constraint="basal" id="o11025" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish-brown" value_original="grayish-brown" />
      </biological_entity>
      <biological_entity id="o11026" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles 0.2-1.5 mm;</text>
      <biological_entity id="o11027" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1) 2.5-18 (30) cm long, (1) 5-20 mm wide, flat or involute, abaxial surfaces smooth or scabridulous, often glaucous.</text>
      <biological_entity id="o11028" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="18" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11029" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikes (3) 6-11 (16) cm long, 5-17 mm wide, erect, with (2) 3 spikelets per node;</text>
      <biological_entity id="o11030" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="11" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="17" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o11031" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o11032" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o11030" id="r1758" name="with" negation="false" src="d0_s5" to="o11031" />
      <relation from="o11031" id="r1759" name="per" negation="false" src="d0_s5" to="o11032" />
    </statement>
    <statement id="d0_s6">
      <text>rachises hirsute on the margins, puberulent elsewhere;</text>
      <biological_entity id="o11033" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character constraint="on margins" constraintid="o11034" is_modifier="false" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="elsewhere" name="pubescence" notes="" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11034" name="margin" name_original="margins" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>internodes 3.5-6 mm.</text>
      <biological_entity id="o11035" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 7-10 (12) mm excluding the awns, strongly overlapping, lateral spikelets slightly larger than the central spikelets.</text>
      <biological_entity id="o11036" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o11037" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o11038" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character constraint="than the central spikelets" constraintid="o11039" is_modifier="false" name="size" src="d0_s8" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity constraint="central" id="o11039" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <relation from="o11036" id="r1760" name="excluding the" negation="false" src="d0_s8" to="o11037" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes (3.5) 4.2-9.4 mm, subulate, scabrous or with 0.3-0.8 mm hairs;</text>
      <biological_entity id="o11040" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s9" to="9.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
        <character constraint="with hairs" constraintid="o11041" is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="with 0.3-0.8 mm hairs" value_original="with 0.3-0.8 mm hairs" />
      </biological_entity>
      <biological_entity id="o11041" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 5.5-7.5 mm, lanceolate, glabrous or with 0.3-0.8 mm hairs, sharply acute or awned, awns 0.8-3.5 mm;</text>
      <biological_entity id="o11042" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character constraint="with hairs" constraintid="o11043" is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="with 0.3-0.8 mm hairs" value_original="with 0.3-0.8 mm hairs" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11043" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11044" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>paleas 5.8-7.6 mm, scabrous, acute;</text>
      <biological_entity id="o11045" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s11" to="7.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 2.5-5.1 mm;</text>
      <biological_entity id="o11046" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lodicules 1.3-1.5 mm.</text>
      <biological_entity id="o11047" name="lodicule" name_original="lodicules" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 4.3-5 mm. 2n = 14, rarely 28.</text>
      <biological_entity id="o11048" name="caryopse" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11049" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit=",rarely" value="14" value_original="14" />
        <character name="quantity" src="d0_s14" unit=",rarely" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Psathyrostachys juncea is native to central Asia, primarily to the Russian and Mongolian steppes. It has become established at various locations, from Alaska to Arizona and New Mexico. It is drought-resistant and tolerant of saline soils. In its native range, it grows on stony slopes and roadsides, at elevations to 5500 m.</discussion>
  <discussion>Psathyrostachys juncea closely resembles Leymus cinereus, differing primarily in having shorter ligules and a rachis that breaks up at maturity. Immature plants can be identified by the more uniform appearance of the spikelets. Psathyrostachys juncea also tends to have smaller spikelets with fewer florets than L. cinereus. Plants with pilose florets have been treated as a distinct taxon; such recognition is not merited.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;Colo.;N.Mex.;Tex.;Utah;Alaska;Alta.;Man.;Sask.;Yukon;Mont.;Wyo.;Wash.;Ariz.;N.Dak.;Nebr.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>