<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Christopher M.A. Stapleton;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">15</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Luerss." date="unknown" rank="subfamily">BAMBUSOIDEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="tribe">BAMBUSEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily bambusoideae;tribe bambuseae</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, shrubby to arborescent, self-supporting to climbing;</text>
      <biological_entity id="o14142" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="shrubby" value_original="shrubby" />
        <character char_type="range_value" from="self-supporting" name="growth_form" src="d0_s1" to="climbing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes well developed, pachymorphic or leptomorphic, rarely both.</text>
      <biological_entity id="o14143" name="rhizom" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms perennial, woody, to 30 m tall, internodes usually hollow, initially unbranched and bearing thickened overlapping culm leaves, subsequently developing foliage leaves on the complex branch systems from buds at the internode bases.</text>
      <biological_entity id="o14144" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="texture" src="d0_s3" value="woody" value_original="woody" />
        <character char_type="range_value" from="0" from_unit="m" name="height" src="d0_s3" to="30" to_unit="m" />
      </biological_entity>
      <biological_entity id="o14145" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
        <character is_modifier="false" modifier="initially" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="culm" id="o14146" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="foliage" id="o14147" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="subsequently" name="development" src="d0_s3" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="branch" id="o14148" name="system" name_original="systems" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o14149" name="bud" name_original="buds" src="d0_s3" type="structure" />
      <biological_entity constraint="internode" id="o14150" name="base" name_original="bases" src="d0_s3" type="structure" />
      <relation from="o14145" id="r2249" name="bearing" negation="false" src="d0_s3" to="o14146" />
      <relation from="o14147" id="r2250" name="on" negation="false" src="d0_s3" to="o14148" />
      <relation from="o14148" id="r2251" name="from" negation="false" src="d0_s3" to="o14149" />
      <relation from="o14149" id="r2252" name="at" negation="false" src="d0_s3" to="o14150" />
    </statement>
    <statement id="d0_s4">
      <text>Culm leaves thickened, usually early deciduous;</text>
      <biological_entity constraint="culm" id="o14151" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles and/or fimbriae often present;</text>
      <biological_entity id="o14152" name="auricle" name_original="auricles" src="d0_s5" type="structure" />
      <biological_entity id="o14153" name="fimbria" name_original="fimbriae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial ligules usually lacking;</text>
      <biological_entity constraint="abaxial" id="o14154" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial ligules present;</text>
      <biological_entity constraint="adaxial" id="o14155" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades poorly to well developed, erect or reflexed, the base as wide as or narrower than the sheath apex, sometimes pseudopetiolate.</text>
      <biological_entity id="o14156" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="poorly to well" name="development" src="d0_s8" value="developed" value_original="developed" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o14157" name="base" name_original="base" src="d0_s8" type="structure">
        <character constraint="than the sheath apex" constraintid="o14158" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
      <biological_entity constraint="sheath" id="o14158" name="apex" name_original="apex" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Foliage leaves: auricles and fimbriae present or absent;</text>
      <biological_entity constraint="foliage" id="o14159" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o14160" name="auricle" name_original="auricles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14161" name="fimbria" name_original="fimbriae" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial ligules present;</text>
      <biological_entity constraint="foliage" id="o14162" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o14163" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pseudopetioles nearly always present;</text>
      <biological_entity constraint="foliage" id="o14164" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o14165" name="pseudopetiole" name_original="pseudopetioles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly always" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>blades deciduous, venation parallel, cross venation often evident, particularly at the base.</text>
      <biological_entity constraint="foliage" id="o14166" name="leaf" name_original="leaves" src="d0_s12" type="structure" />
      <biological_entity id="o14167" name="blade" name_original="blades" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s12" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o14168" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o14167" id="r2253" modifier="particularly" name="at" negation="false" src="d0_s12" to="o14168" />
    </statement>
    <statement id="d0_s13">
      <text>Inflorescences determinate or indeterminate, bracteate or ebracteate, racemose to paniculate, composed of pseudospikelets or spikelets.</text>
      <biological_entity id="o14169" name="inflorescence" name_original="inflorescences" src="d0_s13" type="structure">
        <character is_modifier="false" name="development" src="d0_s13" value="determinate" value_original="determinate" />
        <character is_modifier="false" name="development" src="d0_s13" value="indeterminate" value_original="indeterminate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="ebracteate" value_original="ebracteate" />
        <character char_type="range_value" from="racemose" name="arrangement" src="d0_s13" to="paniculate" />
      </biological_entity>
      <biological_entity id="o14170" name="pseudospikelet" name_original="pseudospikelets" src="d0_s13" type="structure" />
      <biological_entity id="o14171" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o14169" id="r2254" name="composed of" negation="false" src="d0_s13" to="o14170" />
      <relation from="o14169" id="r2255" name="composed of" negation="false" src="d0_s13" to="o14171" />
    </statement>
    <statement id="d0_s14">
      <text>Spikelets or pseudospikelets with 1 to many florets, the lower floret (s) often sterile, the others bisexual;</text>
      <biological_entity id="o14172" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity id="o14173" name="pseudospikelet" name_original="pseudospikelets" src="d0_s14" type="structure">
        <character constraint="to florets" constraintid="o14174" modifier="with" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14174" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14175" name="floret" name_original="floret" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o14176" name="other" name_original="others" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>glumes often subtending the buds;</text>
      <biological_entity id="o14177" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <biological_entity id="o14178" name="bud" name_original="buds" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="often" name="position" src="d0_s15" value="subtending" value_original="subtending" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas often unawned, usually multiveined;</text>
      <biological_entity id="o14179" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s16" value="multiveined" value_original="multiveined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lodicules usually 3, with vascular tissue;</text>
      <biological_entity id="o14180" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o14181" name="tissue" name_original="tissue" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="vascular" value_original="vascular" />
      </biological_entity>
      <relation from="o14180" id="r2256" name="with" negation="false" src="d0_s17" to="o14181" />
    </statement>
    <statement id="d0_s18">
      <text>anthers usually 3 or 6, sometimes fewer or up to 7, very occasionally many;</text>
      <biological_entity id="o14182" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s18" unit="or" value="6" value_original="6" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s18" value="fewer" value_original="fewer" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s18" to="7" />
        <character is_modifier="false" modifier="very occasionally" name="quantity" src="d0_s18" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovaries glabrous or pubescent;</text>
      <biological_entity id="o14183" name="ovary" name_original="ovaries" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles or style-branches 1-4;</text>
      <biological_entity id="o14184" name="style" name_original="styles" src="d0_s20" type="structure" />
      <biological_entity id="o14185" name="style-branch" name_original="style-branches" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s20" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Caryopses with or without a thickened fleshy pericarp.</text>
      <biological_entity id="o14186" name="caryopse" name_original="caryopses" src="d0_s21" type="structure" />
      <biological_entity id="o14187" name="pericarp" name_original="pericarp" src="d0_s21" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s21" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="texture" src="d0_s21" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o14186" id="r2257" name="with or without" negation="false" src="d0_s21" to="o14187" />
    </statement>
  </description>
  <discussion>The Bambuseae include about 80-90 genera and around 1400 species. They are most abundant in Asia and South America, but are also found in Africa, Australia, Central America and North America. One genus, Arundinaria, is native to the Flora region, where it is represented by 3 species. Many other genera and species are cultivated in the region for their ornamental value. These often persist for decades; some have become established beyond the original planting. Identification of these introduced bamboos is hampered by the lack of taxonomic studies in their countries of origin, particularly studies of vegetative features, and the large number of taxa that have not yet been described. Identification is further hindered by their infrequent flowering.</discussion>
  <discussion>Woody bamboos differ from all other grasses in the division of vegetative growth into two phases. Growth commences with the production of unbranched new culms which bear protective leaves called culm leaves (or culm sheaths) at their nodes. Later in the same season, or in the following season, buds at the nodes of the new culms develop into branches, which produce foliage leaves with photosynthetic blades. The leaves on the culms and foliage branches are homologous, each consisting of a sheath, adaxial ligule, and blade, but the culm leaves have less well-developed blades and quickly become non-photosynthetic or fall away, while the foliage leaves have more persistent, photosynthetic, well-developed blades.</discussion>
  <discussion>Another feature characteristic of some, but not all, bamboos is the 'pseudospikelet', which differs from a determinate spikelet in its indeterminate growth from lateral buds in the axils of the basal glumelike bracts. This can lead to extensive ramification of the pseudospikelet into a capitate cluster [see McClure (1966) for a more detailed discussion].</discussion>
  <discussion>Woody bamboos are also distinguished by perennial lignified culms which often have complex branch complements at the nodes, and often by cyclical flowering at intervals of up to 150 years. In cyclical flowering, an entire population or even species will flower in a given year, after which the parents usually die, regeneration being through slow-growing and vulnerable seedlings.</discussion>
  <discussion>Most woody bamboos are tropical or subtropical in their distribution, but about 25 genera are found in temperate regions, mainly in eastern Asia. Two genera are native to North America; Arundinaria is the only genus native to the Flora region. It is thought to have crossed the Bering Strait from eastern Asia, and may represent the sole remnant of a much larger pre-glacial bamboo population. The other genus, Otatea (McClure &amp; E.W. Sm.) C.E. Calderon &amp; Soderstr., is probably a relatively recent entrant into Mexico from South America.</discussion>
  <discussion>The taxonomy of woody bamboos has developed slowly because of the scarcity of flowering material, and the distribution of the species in predominantly inaccessible or less-developed parts of the world. Concentration on floral characters in earlier classification systems has made taxonomy and identification even more difficult. Cytological information has been of little value beyond the separation of two major Asian groups, one tropical with 2n = 56-72 and the other temperate with 2n = 48. There appears to be more variation among American bamboos.</discussion>
  <discussion>Molecular data have thrown doubt upon the phylogenetic validity of many earlier attempts to classify the woody bamboos by floral characters alone. Nevertheless, they are illuminating interesting patterns of evolution and dispersal. A wider range of morphological characters is increasingly being studied and applied. Because many species have only been described in recent years, while many cultivated bamboos have been grown under misapplied or speculative names, the taxonomy of bamboos will require continued study for many years to come. Fieldwork, herbarium study, and laboratory investigations will all be necessary, along with extensive international collaboration. It is highly likely that a substantial proportion of forest bamboos will become extinct before they have been properly documented.</discussion>
  <discussion>The American Bamboo Society lists over 450 taxa of bamboo, representing 240 species in 40 genera, as being commercially available in the United States. Although bamboos are increasingly widely cultivated in the Flora region, they are most common in the coastal and southern states. Most of the cultivated species are Asian in origin, but in recent years numerous Central and South American taxa have been introduced. Most introduced taxa will persist indefinitely without cultivation. In favorable climates, many will spread beyond the original plantings to become naturalized, especially those with long rhizomes. Wider dispersal occurs when sections of the rhizome are transported from one location to another, as may happen during floods. Clump-forming bamboos may eventually spread through seed dispersal.</discussion>
  <discussion>Bamboos are multipurpose plants of immense utility to mankind. Traditional uses, such as building, basketry, and fodder, have been supplemented by industrial-scale paper-pulp production, the canning of edible shoots, and production of advanced board products, laminates, and cloth.</discussion>
  <discussion>This treatment is limited to a full treatment of the native genus, Arundinaria, and descriptions and representative illustrations for the three genera and seven species thought to have become established in the Flora region.</discussion>
  <references>
    <reference>Bystriakova, N., V. Kapos, C.M.A. Stapleton, and J. Lysenko. 2003. Bamboo Biodiversity: Information for Planning Conservation and Management in the Asia-Pacific Region. UNEP-WCMC Biodiversity Series, no. 14. United Nations Environment Programme-World Conservation Monitoring Centre and International Network for Bamboo and Rattan, Cambridge, England. 71 pp.</reference>
    <reference>Calderon, C.E. and T.R. Soderstrom. 1980. The genera of Bambusoideae (Poaceae) of the American continent: Keys and comments. Smithsonian Contr. Bot. 44:1-27</reference>
    <reference>Judziewicz, E.J., L.G. Clark, X. Londono and M.J. Stern. 1999. American Bamboos. Smithsonian Institution Press, Washington, D.C., U.S.A. 392 pp.</reference>
    <reference>McClure, F.A. 1966. The Bamboos: A Fresh Perspective. Harvard University Press, Cambridge, Massachusetts, U.S.A.. 347 pp.</reference>
    <reference>McClure, F.A. 1973. Genera of bamboos native to the New World (Gramineae: Bambusoideae). Smithsonian Contr. Bot. 9:1-148</reference>
    <reference>Soderstrom, T.R. and R. Ellis. 1987. The position of bamboo genera and allies in a system of grass classification. Pp. 225-238 in T.R. Soderstrom, K.W. Hilu, C.S. Campbell, and M.E. Barkworth (eds.). Grass Systematics and Evolution. Smithsonian Institution Press, Washington, D.C, U.S.A. 473 pp.</reference>
    <reference>Stapleton, C.M.A. 1997. Morphology of woody bamboos. Pp. 251-267 in G.P. Chapman (ed.). The Bamboos. Academic Press, San Diego, California, U.S.A. 370 pp.</reference>
    <reference>Stapleton, C.M.A. 1998. Form and function in the bamboo rhizome. J. Amer. Bamboo Soc.12: 21-29</reference>
    <reference>Stapleton, C.M.A., G.N. Chonghaile, and T.R. Hodkinson. 2004. Sarocalamus, a new Sino-Himalayan bamboo genus (Poaceae-Bambusoideae). Novon 14:345-349.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Rhizomes pachymorphic, short, thicker than the culms; culms forming separate, well-defined clusters</description>
      <determination>2.02 Bambusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Rhizomes leptomorphic, long, thinner than the culms; culms solitary, loosely clumped, or both.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Culm internodes grooved their whole length, often doubly sulcate above the branches; branches usually without compressed internodes at the base; spikelets sessile</description>
      <determination>2.03 Phyllostachys</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Culm internodes mostly terete, flattened or shallowly sulcate above the branches; branches usually with 1-5 compressed internodes at the base, sometimes without any compressed internodes; spikelets pedicellate.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Culm leaves persistent or deciduous, usually with auricles; culms to 3 cm thick; culm buds open, margins not fused; plants native in the Flora region</description>
      <determination>2.01 Arundinaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Culm leaves persistent, usually without auricles; culms to 1.5 cm thick; culm buds closed, margins fused; plants cultivated in the Flora region, occasionally escaped</description>
      <determination>2.04 Pseudosasa</determination>
    </key_statement>
  </key>
</bio:treatment>