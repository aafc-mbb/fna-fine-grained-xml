<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Francisco M. Vazquez;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="F.M. Vazquez &amp; Barkworth" date="unknown" rank="genus">CELTICA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus celtica</taxon_hierarchy>
  </taxon_identification>
  <number>10.06</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o24415" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 100-250 cm, erect, smooth, glabrous, uppermost node often exposed;</text>
      <biological_entity id="o24416" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s2" to="250" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal branching intravaginal;</text>
      <biological_entity constraint="uppermost" id="o24417" name="node" name_original="node" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s3" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>prophylls exceeding the subtending leaf-sheaths, awned, ciliate or glabrous.</text>
      <biological_entity id="o24418" name="prophyll" name_original="prophylls" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="awned" value_original="awned" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o24419" name="sheath" name_original="leaf-sheaths" src="d0_s4" type="structure" />
      <relation from="o24418" id="r3860" name="exceeding the" negation="false" src="d0_s4" to="o24419" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o24420" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s5" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cleistogenes not developed;</text>
      <biological_entity id="o24421" name="cleistogene" name_original="cleistogenes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s6" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths open to the base, smooth, glabrous except at the throat;</text>
      <biological_entity id="o24422" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character constraint="to base" constraintid="o24423" is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s7" value="smooth" value_original="smooth" />
        <character constraint="except " constraintid="o24424" is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24423" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o24424" name="throat" name_original="throat" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>auricles absent;</text>
      <biological_entity id="o24425" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules membranous, rounded, abaxial surfaces densely pubescent, margins ciliate;</text>
      <biological_entity id="o24426" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24427" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24428" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blades flat to involute, abaxial surfaces smooth, adaxial surfaces scabrous or hirtellous.</text>
      <biological_entity id="o24429" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24430" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24431" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences panicles, nodding, open.</text>
      <biological_entity constraint="inflorescences" id="o24432" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 25-32 mm, with 1 floret;</text>
      <biological_entity id="o24433" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s12" to="32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24434" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <relation from="o24433" id="r3861" name="with" negation="false" src="d0_s12" to="o24434" />
    </statement>
    <statement id="d0_s13">
      <text>rachillas not prolonged beyond the base of the floret;</text>
      <biological_entity id="o24436" name="base" name_original="base" src="d0_s13" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity id="o24437" name="floret" name_original="floret" src="d0_s13" type="structure" />
      <relation from="o24436" id="r3862" name="part_of" negation="false" src="d0_s13" to="o24437" />
    </statement>
    <statement id="d0_s14">
      <text>disarticulation above the glumes, beneath the floret.</text>
      <biological_entity id="o24435" name="rachilla" name_original="rachillas" src="d0_s13" type="structure">
        <character constraint="beyond base" constraintid="o24436" is_modifier="false" modifier="not" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o24438" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o24439" name="floret" name_original="floret" src="d0_s14" type="structure" />
      <relation from="o24435" id="r3863" name="above" negation="false" src="d0_s14" to="o24438" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes lanceolate, exceeding the floret, 3-veined;</text>
      <biological_entity id="o24440" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o24441" name="floret" name_original="floret" src="d0_s15" type="structure" />
      <relation from="o24440" id="r3864" name="exceeding the" negation="false" src="d0_s15" to="o24441" />
    </statement>
    <statement id="d0_s16">
      <text>florets 14-16 mm, terete to laterally compressed;</text>
      <biological_entity id="o24442" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s16" to="16" to_unit="mm" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s16" to="laterally compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses sharp, strigose;</text>
      <biological_entity id="o24443" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas coriaceous, evenly pubescent, hairs 1-2 mm, margins flat, overlapping at maturity, apices bifid, awned from between the teeth, teeth scarious;</text>
      <biological_entity id="o24444" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="texture" src="d0_s18" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24445" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24446" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s18" value="flat" value_original="flat" />
        <character constraint="at apices, teeth" constraintid="o24447, o24448" is_modifier="false" name="arrangement" src="d0_s18" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o24447" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s18" value="maturity" value_original="maturity" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
        <character is_modifier="false" name="texture" src="d0_s18" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o24448" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s18" value="maturity" value_original="maturity" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
        <character is_modifier="false" name="texture" src="d0_s18" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o24449" name="tooth" name_original="teeth" src="d0_s18" type="structure" />
      <relation from="o24447" id="r3865" name="from" negation="false" src="d0_s18" to="o24449" />
      <relation from="o24448" id="r3866" name="from" negation="false" src="d0_s18" to="o24449" />
    </statement>
    <statement id="d0_s19">
      <text>awns persistent, twice-geniculate, first segment twisted, terminal segment straight;</text>
      <biological_entity id="o24450" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
        <character constraint="segment" constraintid="o24451" is_modifier="false" name="size_or_quantity" src="d0_s19" value="2 times-geniculate first segment twisted" />
        <character constraint="segment" constraintid="o24452" is_modifier="false" name="size_or_quantity" src="d0_s19" value="2 times-geniculate first segment twisted" />
      </biological_entity>
      <biological_entity id="o24451" name="segment" name_original="segment" src="d0_s19" type="structure" />
      <biological_entity id="o24452" name="segment" name_original="segment" src="d0_s19" type="structure" />
      <biological_entity constraint="terminal" id="o24453" name="segment" name_original="segment" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas subequal to or longer than the lemmas, membranous, dorsally pubescent, veins forming 2 awnlike extensions;</text>
      <biological_entity id="o24454" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="than the lemmas" constraintid="o24455" from="subequal" name="size" src="d0_s20" to="or longer" value="subequal to or longer" value_original="subequal to or longer" />
        <character is_modifier="false" name="texture" src="d0_s20" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="dorsally" name="pubescence" src="d0_s20" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24455" name="lemma" name_original="lemmas" src="d0_s20" type="structure" />
      <biological_entity id="o24456" name="vein" name_original="veins" src="d0_s20" type="structure" />
      <biological_entity id="o24457" name="extension" name_original="extensions" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s20" value="awnlike" value_original="awnlike" />
      </biological_entity>
      <relation from="o24456" id="r3867" name="forming" negation="false" src="d0_s20" to="o24457" />
    </statement>
    <statement id="d0_s21">
      <text>lodicules 3, glabrous, lanceolate, posterior lodicule larger than the lateral lodicules;</text>
      <biological_entity id="o24458" name="lodicule" name_original="lodicules" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s21" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="posterior" id="o24459" name="lodicule" name_original="lodicule" src="d0_s21" type="structure">
        <character constraint="than the lateral lodicules" constraintid="o24460" is_modifier="false" name="size" src="d0_s21" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o24460" name="lodicule" name_original="lodicules" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>anthers 3, penicillate;</text>
      <biological_entity id="o24461" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s22" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>ovaries glabrous;</text>
      <biological_entity id="o24462" name="ovary" name_original="ovaries" src="d0_s23" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s23" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>styles 2.</text>
      <biological_entity id="o24463" name="style" name_original="styles" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Caryopses fusiform;</text>
      <biological_entity id="o24464" name="caryopse" name_original="caryopses" src="d0_s25" type="structure">
        <character is_modifier="false" name="shape" src="d0_s25" value="fusiform" value_original="fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>hila linear, about as long as the caryopses.</text>
      <biological_entity id="o24466" name="caryopse" name_original="caryopses" src="d0_s26" type="structure" />
    </statement>
    <statement id="d0_s27">
      <text>x = 12.</text>
      <biological_entity id="o24465" name="hilum" name_original="hila" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s26" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o24467" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Celtica is a monospecific genus that was formerly included in Stipa or Macrochloa.</discussion>
  <references>
    <reference>Darke, R. 1999. The Color Encyclopedia of Ornamental Grasses: Sedges, Rushes, Restios, Cat-Tails, and Selected Bamboos. Timber Press, Portland, Oregon, U.S.A. 325 pp.</reference>
    <reference>Vazquez, F.M. and M.E. Barkworth. 2004. Resurrection and emendation of Macrochloa (Gramineae: Stipeae). Bot. J. Linn. Soc. 144:483-495.</reference>
  </references>
  
</bio:treatment>