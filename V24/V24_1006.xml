<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">714</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="(Scribn.) Scribn." date="unknown" rank="species">tweedyi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species tweedyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Cascade reedgrass</other_name>
  <other_name type="common_name">Tweedy's reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants without sterile culms;</text>
      <biological_entity id="o21375" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o21374" id="r3378" name="without" negation="false" src="d0_s0" to="o21375" />
    </statement>
    <statement id="d0_s1">
      <text>loosely cespitose, with rhizomes 1-10 cm long, 2-4 mm thick.</text>
      <biological_entity id="o21374" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21376" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (47) 60-120 (150) cm, unbranched, smooth, rarely slightly scabrous;</text>
      <biological_entity id="o21377" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="47" value_original="47" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 2-3.</text>
      <biological_entity id="o21378" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars smooth;</text>
      <biological_entity id="o21379" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o21380" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (1) 3.5-6 (8) mm, obtuse, lacerate;</text>
      <biological_entity id="o21381" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (3) 4-20 (38) cm long, (2) 3-8 (13) mm wide, culm blades wider than 6 mm, flat, abaxial surfaces smooth, adaxial surfaces smooth or slightly scabrous, glabrous or sparsely hairy.</text>
      <biological_entity id="o21382" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="culm" id="o21383" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character modifier="wider than" name="some_measurement" src="d0_s6" unit="mm" value="6" value_original="6" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21384" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21385" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 7-16 (19) cm long, (1) 1.5-2 cm wide, erect, usually contracted, sometimes interrupted near the base, pale-purple to purple;</text>
      <biological_entity id="o21386" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="19" value_original="19" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="16" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character constraint="near base" constraintid="o21387" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character char_type="range_value" from="pale-purple" name="coloration" notes="" src="d0_s7" to="purple" />
      </biological_entity>
      <biological_entity id="o21387" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>branches (0.2) 2.4-6.7 (7.7) cm, smooth, sometimes sparsely scabrous distally, spikelet-bearing to the base.</text>
      <biological_entity id="o21388" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="some_measurement" src="d0_s8" to="6.7" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21389" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o21388" id="r3379" name="to" negation="false" src="d0_s8" to="o21389" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (4.5) 5.5-8 (9) mm;</text>
      <biological_entity id="o21390" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla prolongations (0.5) 1-2 (4) mm, hairs 1.5-3 mm.</text>
      <biological_entity id="o21391" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21392" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes keeled, smooth or the keels scabrous, lateral-veins prominent, apices acute;</text>
      <biological_entity id="o21393" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o21394" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21395" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o21396" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 0.8-1 mm, 0.2-0.3 times as long as the lemmas, sparse;</text>
      <biological_entity constraint="callus" id="o21397" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character constraint="lemma" constraintid="o21398" is_modifier="false" name="length" src="d0_s12" value="0.2-0.3 times as long as the lemmas" />
        <character is_modifier="false" name="count_or_density" src="d0_s12" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o21398" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas (4) 4.5-6.5 (7.5) mm, 0-1.5 mm shorter than the glumes, scabridulous;</text>
      <biological_entity id="o21399" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o21400" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o21400" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>awns 6-8 mm, attached to the lower 1/5 – 3/10 of the lemmas, exserted more than 2 mm, stout, easily distinguished from the callus hairs, bent;</text>
      <biological_entity id="o21401" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s14" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o21402" from="1/5" name="quantity" src="d0_s14" to="3/10" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" upper_restricted="false" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character constraint="from callus hairs" constraintid="o21403" is_modifier="false" modifier="easily" name="prominence" src="d0_s14" value="distinguished" value_original="distinguished" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o21402" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="callus" id="o21403" name="hair" name_original="hairs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2-3.5 mm. 2n = unknown.</text>
      <biological_entity id="o21404" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21405" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Calamagrostis tweedyi grows in montane to subalpine moist meadows and coniferous forests, often in association with Carex geyeri, at 900-2000 m. Its range extends from Washington and Oregon to western Montana.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.;Idaho;Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>