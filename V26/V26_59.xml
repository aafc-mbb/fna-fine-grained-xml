<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">veratrum</taxon_name>
    <taxon_name authority="Durand" date="1855" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia, ser.</publication_title>
      <place_in_publication>2, 3: 103. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus veratrum;species californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102048</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–2.5 m, ± glabrous proximally, tomentose distally.</text>
      <biological_entity id="o5774" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character is_modifier="false" modifier="more or less; proximally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves ovate, distalmost lanceolate to lance-linear, 20–40 × 15–25 cm, reduced distally, tomentose-ciliate, curly-hairy abaxially, glabrous or veins sparsely short-hairy adaxially.</text>
      <biological_entity id="o5775" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o5776" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="lance-linear" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s1" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="tomentose-ciliate" value_original="tomentose-ciliate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s1" value="curly-hairy" value_original="curly-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5777" name="vein" name_original="veins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely; adaxially" name="pubescence" src="d0_s1" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences dense-paniculate, with spreading to stiffly erect branches to near tip or distal 1/3–1/2 unbranched, 30–70 cm, tomentose;</text>
      <biological_entity id="o5778" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="dense-paniculate" value_original="dense-paniculate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s2" to="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o5779" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" is_modifier="true" name="orientation" src="d0_s2" to="stiffly erect" />
      </biological_entity>
      <biological_entity id="o5780" name="tip" name_original="tip" src="d0_s2" type="structure" />
      <relation from="o5778" id="r836" name="with" negation="false" src="d0_s2" to="o5779" />
      <relation from="o5779" id="r837" name="to" negation="false" src="d0_s2" to="o5780" />
    </statement>
    <statement id="d0_s3">
      <text>bracts ovate-elliptic to lanceolate, shorter than to obviously longer than flowers.</text>
      <biological_entity id="o5781" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate-elliptic" name="shape" src="d0_s3" to="lanceolate" />
        <character constraint="than to obviously longer than flowers" constraintid="o5782" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5782" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="obviously" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Tepals creamy white, greenish basally, lanceolate to elliptic or oblong-ovate, not or very slightly clawed, 8–17 mm, margins entire to denticulate, glabrous to abaxially tomentose;</text>
      <biological_entity id="o5783" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="elliptic or oblong-ovate" />
        <character is_modifier="false" modifier="not; very slightly" name="shape" src="d0_s4" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5784" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s4" to="denticulate" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="abaxially tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>gland 1, basal, green, V-shaped;</text>
      <biological_entity id="o5785" name="gland" name_original="gland" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary glabrous or with few hairs;</text>
      <biological_entity id="o5786" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="with few hairs" value_original="with few hairs" />
      </biological_entity>
      <biological_entity id="o5787" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <relation from="o5786" id="r838" name="with" negation="false" src="d0_s6" to="o5787" />
    </statement>
    <statement id="d0_s7">
      <text>pedicel 1–6 mm.</text>
      <biological_entity id="o5788" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules narrowly ovoid, 2–3 cm, glabrous.</text>
      <biological_entity id="o5789" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds flat, winged, 10–12 mm.</text>
      <biological_entity id="o5790" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Californian false hellebore</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Different geographic elements of Veratrum californicum have been described as separate species or varieties. The variation seems to be clinal, with most variants not consistent in their appearance or distribution. We have recognized two varieties that appear to be fairly consistent in their distributions and characteristics.</discussion>
  <discussion>Western Native Americans (Blackfeet, Paiute, Shoshone, Thompson, and Washoe) used this species as an antirheumatic, poison, contraceptive, and emetic, as well as a skin, respiratory, blood, cold, snake bite, throat, and toothache aid (D. E. Moerman 1986).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Panicle branched more than 2/3 length of tip; bracts in unbranched portion ovate-elliptic, seldom exceeding flowers.</description>
      <determination>4a Veratrum californicum var. californicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Panicle unbranched in distal 1/3–1/2; bracts in proximal unbranched portion lanceolate, frequently 2–3 times longer than flowers.</description>
      <determination>4b Veratrum californicum var. caudatum</determination>
    </key_statement>
  </key>
</bio:treatment>