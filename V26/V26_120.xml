<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="(Torrey) J. D. Freeman" date="1975" rank="species">angustipetalum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>27: 55. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species angustipetalum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101984</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trillium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">sessile</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="variety">angustipetalum</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 151. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Trillium;species sessile;variety angustipetalum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trillium</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) A. Heller" date="unknown" rank="species">giganteum</taxon_name>
    <taxon_name authority="(Torrey) R. R. Gates" date="unknown" rank="variety">angustipetalum</taxon_name>
    <taxon_hierarchy>genus Trillium;species giganteum;variety angustipetalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tall, very robust.</text>
      <biological_entity id="o34214" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height" src="d0_s0" value="tall" value_original="tall" />
        <character is_modifier="false" modifier="very" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes erect, brownish, thick, somewhat compressed-thickened, praemorse, not brittle.</text>
      <biological_entity id="o34215" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s1" value="compressed-thickened" value_original="compressed-thickened" />
        <character is_modifier="false" name="shape" src="d0_s1" value="praemorse" value_original="praemorse" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Scapes 1–2, round in cross-section, 2.5–6 dm.</text>
      <biological_entity id="o34216" name="scape" name_original="scapes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character constraint="in cross-section" constraintid="o34217" is_modifier="false" name="shape" src="d0_s2" value="round" value_original="round" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="6" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o34217" name="cross-section" name_original="cross-section" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Bracts held well above ground, spreading horizontally, subsessile;</text>
      <biological_entity id="o34218" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o34219" name="ground" name_original="ground" src="d0_s3" type="structure" />
      <relation from="o34218" id="r4612" name="held" negation="false" src="d0_s3" to="o34219" />
    </statement>
    <statement id="d0_s4">
      <text>blade very sparsely mottled with dark greenish brown or rarely all green, mottling becoming obscure with age, broadly ovate, 10–22 × 8.7–15 cm, not glossy, often narrowed to falsely petiolate, very short, and narrowly cuneate base 10–20 mm, apex obtuse.</text>
      <biological_entity id="o34220" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="mottled with dark greenish brown or mottled with rarely all green" />
        <character name="coloration" src="d0_s4" value="rarely" value_original="rarely" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o34221" is_modifier="false" modifier="becoming" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="22" to_unit="cm" />
        <character char_type="range_value" from="8.7" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="falsely" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o34221" name="age" name_original="age" src="d0_s4" type="structure" />
      <biological_entity id="o34222" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34223" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flower erect, odor spicy-musty, musty, or fetid;</text>
      <biological_entity id="o34224" name="flower" name_original="flower" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="odor" src="d0_s5" value="spicy-musty" value_original="spicy-musty" />
        <character is_modifier="false" name="odor" src="d0_s5" value="fetid" value_original="fetid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals conspicuous, spreading, often resting on bracts, maroon to green, linear to oblong-lanceolate, 35–47 × 8–10 mm, margins flat, entire, apex acute;</text>
      <biological_entity id="o34225" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="maroon" modifier="often" name="coloration" src="d0_s6" to="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="oblong-lanceolate" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s6" to="47" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34226" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <biological_entity id="o34227" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o34228" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o34225" id="r4613" modifier="often" name="resting on" negation="false" src="d0_s6" to="o34226" />
    </statement>
    <statement id="d0_s7">
      <text>petals long-lasting, erect, ± connivent, ± concealing stamens and ovary and partially obscuring stamens, dark purple to red-purple, not spirally twisted, veins obscure, linear, 5–10 × 0.7–1.4 cm, 8–10 times longer than wide, glossy, thick-textured, base linear, margins entire, at first flat but inrolling with age, apex variously acute-obtuse;</text>
      <biological_entity id="o34229" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="long-lasting" value_original="long-lasting" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o34230" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s7" value="concealing" value_original="concealing" />
        <character char_type="range_value" from="dark purple" modifier="partially" name="coloration_or_density" src="d0_s7" to="red-purple" />
        <character is_modifier="false" modifier="not spirally" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o34231" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s7" value="concealing" value_original="concealing" />
        <character char_type="range_value" from="dark purple" modifier="partially" name="coloration_or_density" src="d0_s7" to="red-purple" />
        <character is_modifier="false" modifier="not spirally" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o34232" name="stamen" name_original="stamens" src="d0_s7" type="structure" />
      <biological_entity id="o34233" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s7" to="1.4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="8-10" value_original="8-10" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="texture" src="d0_s7" value="thick-textured" value_original="thick-textured" />
      </biological_entity>
      <biological_entity id="o34234" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o34235" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character constraint="with age" constraintid="o34236" is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o34236" name="age" name_original="age" src="d0_s7" type="structure" />
      <biological_entity id="o34237" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s7" value="acute-obtuse" value_original="acute-obtuse" />
      </biological_entity>
      <relation from="o34230" id="r4614" modifier="partially" name="obscuring" negation="false" src="d0_s7" to="o34232" />
      <relation from="o34231" id="r4615" modifier="partially" name="obscuring" negation="false" src="d0_s7" to="o34232" />
    </statement>
    <statement id="d0_s8">
      <text>stamens erect, 12–22 mm;</text>
      <biological_entity id="o34238" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments dark maroon, 2–4 mm, slender, widest at base;</text>
      <biological_entity id="o34239" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark maroon" value_original="dark maroon" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character constraint="at base" constraintid="o34240" is_modifier="false" name="width" src="d0_s9" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o34240" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers erect, straight, purple, 12–18 mm, dehiscence introrse;</text>
      <biological_entity id="o34241" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>connectives purple, slightly extended 1–1.5 mm beyond anther sacs;</text>
      <biological_entity id="o34242" name="connectif" name_original="connectives" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s11" value="extended" value_original="extended" />
        <character char_type="range_value" constraint="beyond anther sacs" constraintid="o34243" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="anther" id="o34243" name="sac" name_original="sacs" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>ovary dark, ovoid-ellipsoid, 6-angled toward apex, 7.5–12 mm;</text>
      <biological_entity id="o34244" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark" value_original="dark" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character constraint="toward apex" constraintid="o34245" is_modifier="false" name="shape" src="d0_s12" value="6-angled" value_original="6-angled" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34245" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas erect, divergent-recurved, distinct, purple, sessile, awl-shaped, thickly subulate, 5 mm, thick, fleshy.</text>
      <biological_entity id="o34246" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="divergent-recurved" value_original="divergent-recurved" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s13" value="awl--shaped" value_original="awl--shaped" />
        <character is_modifier="false" modifier="thickly" name="shape" src="d0_s13" value="subulate" value_original="subulate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="width" src="d0_s13" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits dark purple, fragrance unknown, subglobose, 6-angled, almost winged, fleshy.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 10.</text>
      <biological_entity id="o34247" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="fragrance" src="d0_s14" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="6-angled" value_original="6-angled" />
        <character is_modifier="false" modifier="almost" name="architecture" src="d0_s14" value="winged" value_original="winged" />
        <character is_modifier="false" name="texture" src="d0_s14" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34248" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer (Mar–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Big-tree (Sequoiadendron) groves and other mixed coniferous-deciduous flatwoods, slightly damper depressions under maples and deciduous shrubs, coastal mountains, oak (Quercus) groves in ravines and otherwise quite arid, almost treeless chaparral, wooded canyon slopes, dense woods near streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="groves" />
        <character name="habitat" value="other mixed coniferous-deciduous flatwoods" />
        <character name="habitat" value="damper depressions" modifier="slightly" constraint="under maples and deciduous shrubs" />
        <character name="habitat" value="maples" />
        <character name="habitat" value="deciduous shrubs" />
        <character name="habitat" value="coastal mountains" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="quercus" />
        <character name="habitat" value="groves" constraint="in ravines and otherwise quite arid" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="almost treeless chaparral" />
        <character name="habitat" value="wooded canyon slopes" />
        <character name="habitat" value="dense woods" />
        <character name="habitat" value="near streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Narrow-petaled trillium</other_name>
  <discussion>Trillium angustipetalum occurs in the Sierra Nevada from Fresno County north to Placer County (J. D. Freeman 1975). It is disjunct in the coastal mountains and hills of Santa Barbara and San Luis Obispo counties.</discussion>
  <discussion>B. D. Ness (1993) listed Trillium kurabayashii as a synonym of T. angustipetalum. In bract orientation, color, and texture, and in petal shape, the two are quite different and certainly not the same species. Cytologist Masataka Kurabayashi found chromosomal differences between the two species (reported by J. D. Freeman 1975).</discussion>
  
</bio:treatment>