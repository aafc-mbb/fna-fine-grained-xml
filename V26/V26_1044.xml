<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="(Lindley) Szlachetko" date="1995" rank="subfamily">Vanilloideae</taxon_name>
    <taxon_name authority="Blume" date="1837" rank="tribe">Vanilleae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="subtribe">Pogoniinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1808" rank="genus">isotria</taxon_name>
    <taxon_name authority="(Pursh) Rafinesque" date="1838" rank="species">medeoloides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Tellur.</publication_title>
      <place_in_publication>4: 47. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe pogoniinae;genus isotria;species medeoloides;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101723</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arethusa</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">medeoloides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 591. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arethusa;species medeoloides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pogonia</taxon_name>
    <taxon_name authority="Austin ex A. Gray" date="unknown" rank="species">affinis</taxon_name>
    <taxon_hierarchy>genus Pogonia;species affinis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, singly or rarely in small clumps, short-rhizomatous, 4–25 cm.</text>
      <biological_entity id="o16540" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="singly" value_original="singly" />
        <character name="arrangement" src="d0_s0" value="rarely" value_original="rarely" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o16541" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <relation from="o16540" id="r2258" name="in" negation="false" src="d0_s0" to="o16541" />
    </statement>
    <statement id="d0_s1">
      <text>Stems pale grayish green, glaucous;</text>
      <biological_entity id="o16542" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="pale grayish" value_original="pale grayish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales 2–4, near soil line, white to light green.</text>
      <biological_entity id="o16543" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s2" to="light green" />
      </biological_entity>
      <biological_entity id="o16544" name="soil" name_original="soil" src="d0_s2" type="structure" />
      <biological_entity id="o16545" name="line" name_original="line" src="d0_s2" type="structure" />
      <relation from="o16543" id="r2259" name="near" negation="false" src="d0_s2" to="o16544" />
      <relation from="o16543" id="r2260" name="near" negation="false" src="d0_s2" to="o16545" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves pale grayish green;</text>
      <biological_entity id="o16546" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale grayish" value_original="pale grayish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic-obovate, 17–85 × 8–40 mm, apex acuminate, glaucous when young.</text>
      <biological_entity id="o16547" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-obovate" value_original="elliptic-obovate" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s4" to="85" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16548" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers subsessile, without nectar and fragrance;</text>
      <biological_entity id="o16549" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o16550" name="nectar" name_original="nectar" src="d0_s5" type="structure" />
      <relation from="o16549" id="r2261" name="without" negation="false" src="d0_s5" to="o16550" />
    </statement>
    <statement id="d0_s6">
      <text>sepals arching, green to light green, linear-oblanceolate, 12–25 × 2–3 mm;</text>
      <biological_entity id="o16551" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="arching" value_original="arching" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="light green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals light green, oblanceolate, 12–18 × 2–4 mm;</text>
      <biological_entity id="o16552" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip light yellowish green to pale greenish white, streaked with green, obovate, 11–16 × 4–5 mm, lateral lobes narrowly triangular, margins involute, middle lobe rounded, margins slightly revolute, undulate;</text>
      <biological_entity id="o16553" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="light yellowish" value_original="light yellowish" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="pale greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="streaked with green" value_original="streaked with green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o16554" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o16555" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="middle" id="o16556" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o16557" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s8" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>callus yellowish green, longitudinal, becoming elongate, with fleshy, wartlike papillae toward apex;</text>
      <biological_entity id="o16558" name="callus" name_original="callus" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o16559" name="papilla" name_original="papillae" src="d0_s9" type="structure">
        <character is_modifier="true" name="texture" src="d0_s9" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="shape" src="d0_s9" value="wartlike" value_original="wartlike" />
      </biological_entity>
      <biological_entity id="o16560" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <relation from="o16558" id="r2262" name="with" negation="false" src="d0_s9" to="o16559" />
      <relation from="o16559" id="r2263" name="toward" negation="false" src="d0_s9" to="o16560" />
    </statement>
    <statement id="d0_s10">
      <text>column 8–10 mm;</text>
      <biological_entity id="o16561" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary 10–15 mm;</text>
      <biological_entity id="o16562" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rostellar flap reduced.</text>
      <biological_entity constraint="rostellar" id="o16563" name="flap" name_original="flap" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 12–28 × 2–10 mm;</text>
      <biological_entity id="o16564" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s13" to="28" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel of mature capsule elongating to 5–17 (–20, rarely) mm.</text>
      <biological_entity id="o16566" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s14" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o16565" id="r2264" name="part_of" negation="false" src="d0_s14" to="o16566" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o16565" name="pedicel" name_original="pedicel" src="d0_s14" type="structure" constraint="capsule" constraint_original="capsule; capsule">
        <character constraint="to 5-17 mm" is_modifier="false" name="length" src="d0_s14" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16567" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; capsules mature fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="capsules maturing time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic soils, in dry to mesic 2d-growth, deciduous or deciduous-coniferous forests, typically light to moderate leaf litter, open herb layer (occasionally in dense ferns), moderate to light shrub layer, relatively open canopy, frequently on flats or slope bases near canopy breaks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic soils" />
        <character name="habitat" value="mesic" />
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="deciduous-coniferous forests" />
        <character name="habitat" value="typically light to moderate leaf litter" />
        <character name="habitat" value="open herb layer" />
        <character name="habitat" value="dense ferns" />
        <character name="habitat" value="moderate to light shrub layer" />
        <character name="habitat" value="open canopy" modifier="relatively" />
        <character name="habitat" value="flats" modifier="frequently on" />
        <character name="habitat" value="slope bases" constraint="near canopy breaks" />
        <character name="habitat" value="canopy breaks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Del., Ga., Ill., Maine, Md., Mass., Mich., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Small whorled pogonia orchid</other_name>
  <discussion>Isotria medeoloides should be sought in Alabama, Arkansas, Indiana, Kentucky, and West Virginia.</discussion>
  <discussion>Isotria medeoloides is self-pollinating (L. A. Mehrhoff 1983). Nonflowering plants commonly have a white, arrested floral bud (1–2 mm).</discussion>
  <discussion>of conservation concern</discussion>
  <references>
    <reference>Brumback, W. E. and C. W. Fyler. 1988. Monitoring of Isotria medeoloides in New Hampshire. Wild Fl. Notes 3: 32–40.  </reference>
    <reference>Dyer, R. W. 1986. Small whorled pogonia. Audubon Wildlife Rep. 1986: 780–789.  </reference>
    <reference>Mehrhoff, L. A. 1989. Reproductive vigor and environmental factors in populations of an endangered North American orchid, Isotria medeoloides (Pursh) Rafinesque. Biol. Conservation 47: 281–296.  </reference>
    <reference>Mehrhoff, L. A. 1989b. The dynamics of declining populations of an endangered orchid, Isotria medeoloides. Ecology 70: 783–786.  </reference>
    <reference>Oettingen, S. L. 1992. Small Whorled Pogonia (Isotria medeoloides) Recovery Plan: First Revision. Washington.  </reference>
    <reference>Paulos, P. G. 1985. Small Whorled Pogonia Recovery Plan. [Newton Corner, Mass.]</reference>
  </references>
  
</bio:treatment>