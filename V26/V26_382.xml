<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gerald B. Straley†,Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="treatment_page">219</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HEMEROCALLIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 324. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 151. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus HEMEROCALLIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hemeros, day, and kallos, beauty, alluding to the showy flowers, which bloom and wilt in one day</other_info_on_name>
    <other_info_on_name type="fna_id">114981</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, clump-forming, rhizomatous, from fibrous or fleshy contractile roots often enlarged at ends;</text>
      <biological_entity id="o31611" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="clump-forming" value_original="clump-forming" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o31612" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="contractile" value_original="contractile" />
        <character constraint="at ends" constraintid="o31613" is_modifier="false" modifier="often" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o31613" name="end" name_original="ends" src="d0_s0" type="structure" />
      <relation from="o31611" id="r4258" name="from" negation="false" src="d0_s0" to="o31612" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes spreading.</text>
      <biological_entity id="o31614" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves many, basal, sessile, 2-ranked, bases sheathing;</text>
      <biological_entity id="o31615" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31616" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
      <biological_entity id="o31617" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade long-linear, keeled, apex acuminate.</text>
      <biological_entity id="o31618" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="long-linear" value_original="long-linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o31619" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2, in terminal helicoid-cyme, or solitary.</text>
      <biological_entity id="o31620" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" notes="" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o31621" name="helicoid-cyme" name_original="helicoid-cyme" src="d0_s4" type="structure" />
      <relation from="o31620" id="r4259" name="in" negation="false" src="d0_s4" to="o31621" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers mostly diurnal and ephemeral, slightly irregular, showy;</text>
      <biological_entity id="o31622" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s5" value="diurnal" value_original="diurnal" />
        <character is_modifier="false" name="duration" src="d0_s5" value="ephemeral" value_original="ephemeral" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_course" src="d0_s5" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 6, connate basally into short, funnelform to campanulate tube, distinct parts imbricate, spreading, inner broader than outer;</text>
      <biological_entity id="o31623" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character constraint="into short" is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o31624" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o31625" name="part" name_original="parts" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="inner" id="o31626" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character constraint="than outer tepals" constraintid="o31627" is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
      </biological_entity>
      <biological_entity constraint="outer" id="o31627" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <relation from="o31623" id="r4260" name="to" negation="false" src="d0_s6" to="o31624" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 6, adnate to throat of perianth-tube;</text>
      <biological_entity id="o31628" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o31629" name="throat" name_original="throat" src="d0_s7" type="structure" />
      <biological_entity id="o31630" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure" />
      <relation from="o31629" id="r4261" name="part_of" negation="false" src="d0_s7" to="o31630" />
    </statement>
    <statement id="d0_s8">
      <text>filaments curved upward, distinct, unequal;</text>
      <biological_entity id="o31631" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers dorsifixed, 2-locular, linear-oblong, dehiscence introrse;</text>
      <biological_entity id="o31632" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary superior, green, 3-locular, conic, septal nectaries present;</text>
      <biological_entity id="o31633" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="superior" value_original="superior" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity constraint="septal" id="o31634" name="nectary" name_original="nectaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style curved upwards;</text>
      <biological_entity id="o31635" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="upwards" name="course" src="d0_s11" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma indistinctly 3-lobed or capitate.</text>
      <biological_entity id="o31636" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s12" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, leathery, dehiscence loculicidal.</text>
      <biological_entity id="o31637" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="texture" src="d0_s13" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds rarely produced (sterile) or many.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 11.</text>
      <biological_entity id="o31638" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="x" id="o31639" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; temperate zones worldwide; temperate e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="temperate zones worldwide" establishment_means="introduced" />
        <character name="distribution" value="temperate e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <other_name type="common_name">Daylily</other_name>
  <other_name type="common_name">hémérocalle</other_name>
  <discussion>Species 15–30 (2 in the flora).</discussion>
  <discussion>Hemerocallis is important economically as medicinal, poisonous, edible, and/or horticultural plants, which have been in Chinese culture for thousands of years (W. Erhardt 1992). Hemerocallin, a root neurotoxin, can be both poisonous and useful medicinally as an analgesic, diuretic, arsenic-poisoning antidote, and treatment for schistosomiasis (J. A. Duke and E. S. Ayensu 1985; W. Erhardt 1992; Hu S. Y. 1968). In Asia, flowers (buds and perianths), shoots, and tuberous roots (following suitable preparation) are important foods (G. Kunkel 1984). Daylilies are among the most popular North American garden plants. Registered cultivars of Hemerocallis now exceed 38,000, including more than 13,000 named clones of H. fulva (G. Grosvenor 1999; R. M. Kitchingman 1985; R. W. Munson Jr. 1989; W. B. Zomlefer 1998).</discussion>
  <discussion>Hemerocallis has been included in a broadly circumscribed segregate family Hemerocallidaceae with 13–18 genera mainly from the Southern Hemisphere, especially Australia (W. B. Zomlefer 1998; H. T. Clifford et al. 1998), or placed alone in a monotypic Hemerocallidaceae (A. L. Takhtajan 1997).</discussion>
  <discussion>The dwarf, yellow-flowered Hemerocallis minor P. Miller, grass-leaf daylily, has been reported as a local escape in Oregon.</discussion>
  <references>
    <reference>Erhardt, W. 1992. Hemerocallis: Daylilies. Portland.</reference>
    <reference>Hu S. Y. 1968. The species of Hemerocallis. Amer. Hort. Mag. 47: 86–111.</reference>
    <reference>Stout, A. B. 1934. Daylilies. New York. [Reprinted 1986, London.]</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 7–15 dm; flowers not fragrant; tepals tawny orange, reticulate-veined; inner tepal margins wavy; capsules not or only rarely developing.</description>
      <determination>1 Hemerocallis fulva</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 5–10 dm; flowers fragrant; tepals lemon yellow, parallel-veined; inner tepal margins smooth; capsules fully developing.</description>
      <determination>2 Hemerocallis lilioasphodelus</determination>
    </key_statement>
  </key>
</bio:treatment>