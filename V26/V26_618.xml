<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="treatment_page">311</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="genus">hastingsia</taxon_name>
    <taxon_name authority="(Durand) S. Watson" date="1879" rank="species">alba</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 242. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hastingsia;species alba</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220006095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schoenolirion</taxon_name>
    <taxon_name authority="Durand" date="unknown" rank="species">album</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia, ser.</publication_title>
      <place_in_publication>2, 3: 103. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Schoenolirion;species album;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb ellipsoid, 26–56 × 17–31 mm, sometimes with blackish, fibrous tunic.</text>
      <biological_entity id="o29195" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="26" from_unit="mm" name="length" src="d0_s0" to="56" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s0" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29196" name="tunic" name_original="tunic" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <relation from="o29195" id="r3941" modifier="sometimes" name="with" negation="false" src="d0_s0" to="o29196" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves (28–) 35–41 (–53) cm × 7–14 mm, mature plants with blackish, shriveled foliage persisting at base of scape;</text>
      <biological_entity id="o29197" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="28" from_unit="cm" name="atypical_length" src="d0_s1" to="35" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="41" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="53" to_unit="cm" />
        <character char_type="range_value" from="35" from_unit="cm" name="length" src="d0_s1" to="41" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s1" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29198" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="mature" value_original="mature" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29199" name="foliage" name_original="foliage" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="shape" src="d0_s1" value="shriveled" value_original="shriveled" />
        <character constraint="at base" constraintid="o29200" is_modifier="false" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity id="o29200" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o29201" name="scape" name_original="scape" src="d0_s1" type="structure" />
      <relation from="o29198" id="r3942" name="with" negation="false" src="d0_s1" to="o29199" />
      <relation from="o29200" id="r3943" name="part_of" negation="false" src="d0_s1" to="o29201" />
    </statement>
    <statement id="d0_s2">
      <text>blade glaucous-green, aging to light green.</text>
      <biological_entity id="o29202" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="glaucous-green" value_original="glaucous-green" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="light green" value_original="light green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape 40–89 cm, often with 1–3 ascending branches, 3–5 mm thick at base.</text>
      <biological_entity id="o29203" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s3" to="89" to_unit="cm" />
        <character char_type="range_value" constraint="at base" constraintid="o29205" from="3" from_unit="mm" name="thickness" notes="" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29204" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o29205" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o29203" id="r3944" modifier="often" name="with" negation="false" src="d0_s3" to="o29204" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: terminal raceme (5–) 14–27 (–40) cm;</text>
      <biological_entity id="o29206" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o29207" name="raceme" name_original="raceme" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="14" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s4" to="27" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers (24–) 44–51 (–78) per 10 cm of raceme.</text>
      <biological_entity id="o29208" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o29209" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="24" name="atypical_quantity" src="d0_s5" to="44" to_inclusive="false" />
        <character char_type="range_value" from="51" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="78" />
        <character char_type="range_value" constraint="of raceme" constraintid="o29210" from="44" name="quantity" src="d0_s5" to="51" />
      </biological_entity>
      <biological_entity id="o29210" name="raceme" name_original="raceme" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds dark gray-green to black, 4–6 mm. 2n = 52.</text>
      <biological_entity id="o29211" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" from="dark gray-green" name="coloration" src="d0_s6" to="black" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29212" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul, fruiting late Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="late Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine, granite, and diorite sites, open rocky seepage areas with year-round water supply, bogs, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="diorite sites" modifier="serpentine granite and" />
        <character name="habitat" value="open rocky seepage areas" />
        <character name="habitat" value="year-round water supply" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>At high elevations, Hastingsia alba is often stunted and has smaller bulbs, shorter scapes with shorter and narrower leaves, and spreading-rotate tepal tips.</discussion>
  
</bio:treatment>