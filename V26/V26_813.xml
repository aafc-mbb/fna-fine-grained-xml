<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter Goldblatt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="treatment_page">396</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Sweet" date="1827" rank="genus">HERBERTIA</taxon_name>
    <place_of_publication>
      <publication_title>Brit. Fl. Gard.</publication_title>
      <place_in_publication>3: plate 222. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus HERBERTIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for William Herbert, 1778–1847, prominent British botanist and specialist in bulbous plants</other_info_on_name>
    <other_info_on_name type="fna_id">115152</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Herbert" date="unknown" rank="genus">Trifurcia</taxon_name>
    <taxon_hierarchy>genus Trifurcia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from tunicate, ovoid bulbs;</text>
      <biological_entity id="o20428" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o20429" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tunicate" value_original="tunicate" />
        <character is_modifier="true" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <relation from="o20428" id="r2800" name="from" negation="false" src="d0_s0" to="o20429" />
    </statement>
    <statement id="d0_s1">
      <text>tunic brown, dry, brittle, papery.</text>
      <biological_entity id="o20430" name="tunic" name_original="tunic" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s1" value="dry" value_original="dry" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character is_modifier="false" name="texture" src="d0_s1" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or branched.</text>
      <biological_entity id="o20431" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves few, basal larger;</text>
      <biological_entity id="o20432" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20433" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pleated, linearlanceolate.</text>
      <biological_entity id="o20434" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="pleated" value_original="pleated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences rhipidiate, few-flowered;</text>
      <biological_entity id="o20435" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="rhipidiate" value_original="rhipidiate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green, unequal, inner spathe exceeding outer, apex brown, acute, usually dry.</text>
      <biological_entity id="o20436" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20437" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity constraint="outer" id="o20438" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o20439" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="usually" name="condition_or_texture" src="d0_s6" value="dry" value_original="dry" />
      </biological_entity>
      <relation from="o20437" id="r2801" name="exceeding" negation="false" src="d0_s6" to="o20438" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers short-lived, erect, unscented, actinomorphic;</text>
      <biological_entity id="o20440" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="odor" src="d0_s7" value="unscented" value_original="unscented" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals spreading, distinct, blue to mauve with white markings, unequal, outer whorl more than 2 times inner;</text>
      <biological_entity id="o20441" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" constraint="with markings" constraintid="o20442" from="blue" name="coloration" src="d0_s8" to="mauve" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o20442" name="marking" name_original="markings" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="outer" id="o20443" name="whorl" name_original="whorl" src="d0_s8" type="structure">
        <character constraint="whorl" constraintid="o20444" is_modifier="false" name="size_or_quantity" src="d0_s8" value="2+ times inner whorl" value_original="2+ times inner whorl" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20444" name="whorl" name_original="whorl" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>filaments connate;</text>
      <biological_entity id="o20445" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers diverging, appressed to style-branches;</text>
      <biological_entity id="o20446" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="diverging" value_original="diverging" />
        <character constraint="to style-branches" constraintid="o20447" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o20447" name="style-branch" name_original="style-branches" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>style slender, branching at apex of filament column;</text>
      <biological_entity id="o20448" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character constraint="at apex" constraintid="o20449" is_modifier="false" name="architecture" src="d0_s11" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o20449" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity constraint="filament" id="o20450" name="column" name_original="column" src="d0_s11" type="structure" />
      <relation from="o20449" id="r2802" name="part_of" negation="false" src="d0_s11" to="o20450" />
    </statement>
    <statement id="d0_s12">
      <text>branches diverging from base, flattened, divided apically into 2 slender lobes, apically stigmatic.</text>
      <biological_entity id="o20451" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character constraint="from base" constraintid="o20452" is_modifier="false" name="orientation" src="d0_s12" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="flattened" value_original="flattened" />
        <character constraint="into slender lobes" constraintid="o20453" is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
        <character is_modifier="false" modifier="apically" name="structure_in_adjective_form" notes="" src="d0_s12" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o20452" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity constraint="slender" id="o20453" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid, apex truncate.</text>
      <biological_entity id="o20454" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o20455" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds many, prismatic;</text>
      <biological_entity id="o20456" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s14" value="prismatic" value_original="prismatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat brown.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 7.</text>
      <biological_entity id="o20457" name="seed-coat" name_original="seed-coat" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="x" id="o20458" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, temperate South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="temperate South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Pleat-leaf iris</other_name>
  <discussion>Species ca. 5 (1 in the flora).</discussion>
  <discussion>The name Alophia has been misapplied to this genus because of an erroneous interpretation of the identity of the type species of Alophia (P. Goldblatt 1975).</discussion>
  
</bio:treatment>