<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">176</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lilium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">grayi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 256. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lilium;species grayi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101731</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs often yellowish, rhizomatous, unbranched, 2.2–2.6 × 3.8–5 cm, 0.5–0.6 times taller than long, 2 years’ growth evident as annual bulbs, scaleless sections between these 1.2–2.5 cm;</text>
      <biological_entity id="o25046" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s0" to="2.6" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="width" src="d0_s0" to="5" to_unit="cm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="0.5-0.6 times taller than long" />
        <character name="quantity" src="d0_s0" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25047" name="growth" name_original="growth" src="d0_s0" type="structure">
        <character constraint="as bulbs" constraintid="o25048" is_modifier="false" name="prominence" src="d0_s0" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o25048" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o25049" name="section" name_original="sections" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="scaleless" value_original="scaleless" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 1–2-segmented, longest 0.9–2.2 cm;</text>
      <biological_entity id="o25050" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="1-2-segmented" value_original="1-2-segmented" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s1" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stem roots present.</text>
      <biological_entity constraint="stem" id="o25051" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems to 1.3 m.</text>
      <biological_entity id="o25052" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s3" to="1.3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Buds rounded in cross-section.</text>
      <biological_entity id="o25053" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o25054" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o25054" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves in 3–5 whorls or partial whorls, 3–12 leaves per whorl, ± horizontal to occasionally slightly ascending, drooping at tips, 4.1–12.7 × 1.5–3.6 cm, 1.9–5 times longer than wide;</text>
      <biological_entity id="o25055" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o25056" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o25057" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="partial" value_original="partial" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity id="o25058" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="less horizontal" name="orientation" notes="" src="d0_s5" to="occasionally slightly ascending" />
        <character constraint="at tips" constraintid="o25060" is_modifier="false" name="orientation" src="d0_s5" value="drooping" value_original="drooping" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" notes="" src="d0_s5" to="12.7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" notes="" src="d0_s5" to="3.6" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1.9-5" value_original="1.9-5" />
      </biological_entity>
      <biological_entity id="o25059" name="whorl" name_original="whorl" src="d0_s5" type="structure" />
      <biological_entity id="o25060" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <relation from="o25055" id="r3395" name="in" negation="false" src="d0_s5" to="o25056" />
      <relation from="o25055" id="r3396" name="in" negation="false" src="d0_s5" to="o25057" />
      <relation from="o25058" id="r3397" name="per" negation="false" src="d0_s5" to="o25059" />
    </statement>
    <statement id="d0_s6">
      <text>blade elliptic, occasionally narrowly so or barely lanceolate, margins not undulate, apex acute, usually barely acuminate in distal leaves;</text>
      <biological_entity id="o25061" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="occasionally narrowly; narrowly; barely" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o25062" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o25063" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character constraint="in distal leaves" constraintid="o25064" is_modifier="false" modifier="usually barely" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25064" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>principal veins impressed adaxially, veins and margins noticeably roughened abaxially with tiny ± deltoid epidermal spicules, especially apically and on proximal leaves.</text>
      <biological_entity constraint="principal" id="o25065" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s7" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o25066" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character constraint="with epidermal, spicules" constraintid="o25068, o25069" is_modifier="false" modifier="noticeably" name="relief_or_texture" src="d0_s7" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o25067" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character constraint="with epidermal, spicules" constraintid="o25068, o25069" is_modifier="false" modifier="noticeably" name="relief_or_texture" src="d0_s7" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o25068" name="epidermal" name_original="epidermal" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="tiny" value_original="tiny" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity id="o25069" name="spicule" name_original="spicules" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="tiny" value_original="tiny" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25070" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o25066" id="r3398" modifier="especially apically; apically" name="on" negation="false" src="d0_s7" to="o25070" />
      <relation from="o25067" id="r3399" modifier="especially apically; apically" name="on" negation="false" src="d0_s7" to="o25070" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences racemose, 1–9 (–16) -flowered.</text>
      <biological_entity id="o25071" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-9(-16)-flowered" value_original="1-9(-16)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers nodding, not fragrant;</text>
      <biological_entity id="o25072" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s9" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth campanulate;</text>
      <biological_entity id="o25073" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals and petals barely recurved 2/3–9/10 along length from base, yellow-orange proximally, pale-red distally, spotted maroon, pale-red or sometimes red-orange abaxially, not distinctly clawed;</text>
      <biological_entity id="o25074" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="barely" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="proximally" name="coloration" notes="" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s11" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="spotted maroon" value_original="spotted maroon" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="not distinctly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o25075" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="barely" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="from base" constraintid="o25076" from="2/3" name="length" src="d0_s11" to="9/10" />
        <character is_modifier="false" modifier="proximally" name="coloration" notes="" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s11" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="spotted maroon" value_original="spotted maroon" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="not distinctly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o25076" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>sepals not ridged abaxially, 3.2–5.6 × 1.3–2 cm;</text>
      <biological_entity id="o25077" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; abaxially" name="shape" src="d0_s12" value="ridged" value_original="ridged" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="length" src="d0_s12" to="5.6" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 3.1–5.5 × 1.2–2 cm;</text>
      <biological_entity id="o25078" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.1" from_unit="cm" name="length" src="d0_s13" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s13" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens included;</text>
      <biological_entity id="o25079" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments ± parallel to style, barely spreading, diverging 3°–9° from axis, red;</text>
      <biological_entity id="o25080" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character constraint="to style" constraintid="o25081" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character is_modifier="false" modifier="barely" name="orientation" notes="" src="d0_s15" value="spreading" value_original="spreading" />
        <character constraint="from axis" constraintid="o25082" is_modifier="false" modifier="3°-9°" name="orientation" src="d0_s15" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o25081" name="style" name_original="style" src="d0_s15" type="structure" />
      <biological_entity id="o25082" name="axis" name_original="axis" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers magenta, 0.4–1.2 cm;</text>
      <biological_entity id="o25083" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="magenta" value_original="magenta" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s16" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pollen brown-rust;</text>
      <biological_entity id="o25084" name="pollen" name_original="pollen" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown-rust" value_original="brown-rust" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pistil 2.4–3.8 cm;</text>
      <biological_entity id="o25085" name="pistil" name_original="pistil" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.4" from_unit="cm" name="some_measurement" src="d0_s18" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary 0.8–1.7 cm;</text>
      <biological_entity id="o25086" name="ovary" name_original="ovary" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s19" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>style red;</text>
      <biological_entity id="o25087" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>pedicel 2.6–6.5 cm.</text>
      <biological_entity id="o25088" name="pedicel" name_original="pedicel" src="d0_s21" type="structure">
        <character char_type="range_value" from="2.6" from_unit="cm" name="some_measurement" src="d0_s21" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 2.1–3.7 × 1.5–2.1 cm, 1.5–2.1 times longer than wide.</text>
      <biological_entity id="o25089" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="2.1" from_unit="cm" name="length" src="d0_s22" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s22" to="2.1" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s22" value="1.5-2.1" value_original="1.5-2.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds not counted.</text>
    </statement>
    <statement id="d0_s24">
      <text>2n = 24.</text>
      <biological_entity id="o25090" name="seed" name_original="seeds" src="d0_s23" type="structure" />
      <biological_entity constraint="2n" id="o25091" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (late Jun–mid Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="late Jun-mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy balds, openings in red spruce (Picea rubens Sargent)–Fraser fir (Abies fraseri (Pursh) Poiret) forests, moist hardwood bogs, seeps, and meadows at lower elevations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy balds" />
        <character name="habitat" value="openings" constraint="in red spruce ( picea rubens sargent ) -- fraser fir ( abies fraseri ( pursh ) poiret ) forests" />
        <character name="habitat" value="picea rubens" modifier="spruce" />
        <character name="habitat" value="sargent" />
        <character name="habitat" value="fraser fir" />
        <character name="habitat" value="abies fraseri" />
        <character name="habitat" value="pursh" />
        <character name="habitat" value="poiret" />
        <character name="habitat" value="red spruce ( picea rubens sargent ) -- fraser fir ( abies fraseri ( pursh ) poiret ) forests" />
        <character name="habitat" value="moist hardwood bogs" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="meadows" modifier="and" constraint="at lower elevations" />
        <character name="habitat" value="lower elevations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Gray’s lily</other_name>
  <discussion>The narrowly endemic Gray’s lily blooms predictably on or about July 4 in the balds and forest openings of the Roan Mountain massif shared by North Carolina and Tennessee. In its unadulterated form it also occupies the higher elevations of the Blue Ridge Mountains, including Grandfather Mountain in North Carolina and Mount Rogers and Whitetop Mountain in Virginia. A few populations occur at lower elevations (below 900 m) in streamside meadows along the Blue Ridge Parkway in northern North Carolina (Alleghany County), but in similar settings farther north in Virginia introgression with L. canadense occurs.</discussion>
  <discussion>Lilium ×pseudograyi Grove (as species) is a name given to frequent hybrids between L. grayi and L. canadense that are scattered at somewhat lower elevations (usually 700–1000 m) in the southern Appalachians. The generally small stature of these hybrids is misleading and encourages the label of bona fide L. grayi, but in most respects they are intermediate. Sepal lengths of 4.8–6.2 cm and floral tube lengths of 3.2–4 cm predominate, and these are between the ranges of the two parent species. The freshwater wetland or moist hardwood habitat of these hybrids also reveals the contribution of L. canadense to their genome.</discussion>
  <discussion>J. K. Small (1933) made reference to depredations by lily enthusiasts who sought Gray’s lily because of its supposed rarity, and this continues today, though to a lesser degree. Of greater threat, perhaps, is succession on the high grassy balds that gradually shades and crowds the plants; like most lilies, this one requires open conditions for vigor and reproduction.</discussion>
  <discussion>Although fritillaries (Speyeria spp., family Nymphalidae) pilfer nectar from flowers of Gray’s lily, ruby-throated hummingbirds [Archilochus colubris (Linnaeus), family Trochilidae] are its only reliable pollinator. This red, tubular-flowered lily represents the zenith of pollinator-mediated evolution in the eastern true lilies, and is a high-elevation derivative of the ancestral stock that also produced Lilium canadense. The level of floral convergence with independently derived western Lilium species such as L. bolanderi and L. maritimum is remarkable and must be due to selection pressures exerted by hummingbirds during the floral evolution of these species.</discussion>
  
</bio:treatment>