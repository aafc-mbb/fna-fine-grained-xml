<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">TRILLIUM</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cernuum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 339. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus trillium;species cernuum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220013789</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trillium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cernuum</taxon_name>
    <taxon_name authority="Eames &amp; Wiegand" date="unknown" rank="variety">macranthum</taxon_name>
    <taxon_hierarchy>genus Trillium;species cernuum;variety macranthum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes short, thick.</text>
      <biological_entity id="o28235" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–2 (–3), round in cross-section, 1.5–4+ dm, slender, glabrous.</text>
      <biological_entity id="o28236" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
        <character constraint="in cross-section" constraintid="o28237" is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="4" to_unit="dm" upper_restricted="false" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28237" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts often overlapping, sessile or with a barely noticeable, petiolelike base, umbrellalike;</text>
      <biological_entity id="o28238" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with a barely noticeable , petiolelike base" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="umbrellalike" value_original="umbrellalike" />
      </biological_entity>
      <biological_entity id="o28239" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="barely" name="prominence" src="d0_s2" value="noticeable" value_original="noticeable" />
        <character is_modifier="true" name="shape" src="d0_s2" value="petiole-like" value_original="petiolelike" />
      </biological_entity>
      <relation from="o28238" id="r3819" name="with" negation="false" src="d0_s2" to="o28239" />
    </statement>
    <statement id="d0_s3">
      <text>blade bright green without red tones, broadly rhombic-ovate to suborbicular, 5–15 × 6–15+ cm, base attenuate, apex acuminate.</text>
      <biological_entity id="o28240" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="without base, apex" constraintid="o28241, o28242" is_modifier="false" name="coloration" src="d0_s3" value="bright green" value_original="bright green" />
      </biological_entity>
      <biological_entity id="o28241" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red tones" value_original="red tones" />
        <character char_type="range_value" from="broadly rhombic-ovate" is_modifier="true" name="shape" src="d0_s3" to="suborbicular" />
        <character char_type="range_value" from="5" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="15" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o28242" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red tones" value_original="red tones" />
        <character char_type="range_value" from="broadly rhombic-ovate" is_modifier="true" name="shape" src="d0_s3" to="suborbicular" />
        <character char_type="range_value" from="5" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="15" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flower usually hidden beneath bracts, nodding, odorless;</text>
      <biological_entity id="o28243" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character constraint="beneath bracts" constraintid="o28244" is_modifier="false" modifier="usually" name="prominence" src="d0_s4" value="hidden" value_original="hidden" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="odor" src="d0_s4" value="odorless" value_original="odorless" />
      </biological_entity>
      <biological_entity id="o28244" name="bract" name_original="bracts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>sepals spreading, green, lanceolate-ovate, 9–30 mm, slightly shorter than to equaling petals, margins slightly raised, apex acuminate;</text>
      <biological_entity id="o28245" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="30" to_unit="mm" />
        <character constraint="than to equaling petals" constraintid="o28246" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o28246" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="true" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o28247" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o28248" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals usually strongly recurved from above base, extending behind plane of sepal bases for more than 1/2 their length, white or rarely pale-pink, adaxial veins not conspicuous, oblong-lanceolate, 1.5–2.5 × 0.9–1.5 cm, thin-textured, margins entire, apex acuminate;</text>
      <biological_entity id="o28249" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character constraint="from " constraintid="o28250" is_modifier="false" modifier="usually strongly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="pale-pink" value_original="pale-pink" />
      </biological_entity>
      <biological_entity id="o28250" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="sepal" id="o28251" name="base" name_original="bases" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o28252" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thin-textured" value_original="thin-textured" />
      </biological_entity>
      <biological_entity id="o28253" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28254" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o28250" id="r3820" name="part_of" negation="false" src="d0_s6" to="o28251" />
    </statement>
    <statement id="d0_s7">
      <text>stamens ± straight, 6–15 mm, shorter than pistil, slender;</text>
      <biological_entity id="o28255" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character constraint="than pistil" constraintid="o28256" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o28256" name="pistil" name_original="pistil" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments white, ± equaling anthers, slender;</text>
      <biological_entity id="o28257" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o28258" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers straight, pale lavender-pink or gray, 2–6.5 mm, dehiscence introrse to latrorse;</text>
      <biological_entity id="o28259" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale lavender-pink" value_original="pale lavender-pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="gray" value_original="gray" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="dehiscence" value_original="dehiscence" />
        <character char_type="range_value" from="introrse" name="dehiscence" src="d0_s9" to="latrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary prominent, white to pinkish, pyramidal, strongly 6-angled, 3–12 × 3–10 mm, widest above basal attachment;</text>
      <biological_entity id="o28260" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="6-angled" value_original="6-angled" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
        <character constraint="above basal attachment" constraintid="o28261" is_modifier="false" name="width" src="d0_s10" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28261" name="attachment" name_original="attachment" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stigmas erect, recurved, distinct, white, not lobed adaxially, 3–8 mm, widest at base, fleshy, basally thickened, gradually tapered;</text>
      <biological_entity id="o28262" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="not; adaxially" name="shape" src="d0_s11" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character constraint="at base" constraintid="o28263" is_modifier="false" name="width" src="d0_s11" value="widest" value_original="widest" />
        <character is_modifier="false" name="texture" notes="" src="d0_s11" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o28263" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel strongly recurved or declined below or angled between bracts, 1.5–3 cm.</text>
      <biological_entity id="o28264" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="below" name="orientation" src="d0_s12" value="declined" value_original="declined" />
        <character constraint="between bracts" constraintid="o28265" is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28265" name="bract" name_original="bracts" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits dark red, with fruity fragrance, ovoid, to 3 cm diam., fleshy, juicy.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 10.</text>
      <biological_entity id="o28266" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s13" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s13" value="juicy" value_original="juicy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28267" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, mostly deciduous forest southward, mixed deciduous-coniferous forests, swamps, moist coniferous forests northward</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich" />
        <character name="habitat" value="deciduous forest southward" modifier="mostly" />
        <character name="habitat" value="mixed deciduous-coniferous forests" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="coniferous forests" modifier="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Conn., Del., D.C., Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Nodding trillium</other_name>
  <other_name type="common_name">trille penché</other_name>
  <discussion>Previous authors commonly recognized var. cernuum and var. macranthum, based primarily upon size differences. Plants attributed to var. cernuum are found from Delaware and eastern Pennsylvania northward to Newfoundland, while those attributed to var. macranthum are found mainly farther inland into the Midwest. Although there is a tendency for the eastern seaboard plants to be somewhat smaller and more delicate, and the midwestern and far northern plants to be more robust, there is much variation, largely dependent on soil nutrients. There are regional size trends, but based on my observations of this species in Newfoundland, Michigan, Wisconsin, and Minnesota, I do not believe that the two varieties can be maintained.</discussion>
  <discussion>In the Gray Herbarium, there is a collection by Richardson labeled “Mackenzie River,” which has been cited by H. M. Raup (1947) and others. W. J. Hooker ([1829–]1833–1840) reported Trillium cernuum “from Saskatchewan to Mackenzie River.” Raup stated that “it is the only evidence for the occurrence of...Trillium in the entire Mackenzie Basin.” Other writers have simply quoted that statement. In view of the relatively great disjunction from the known Saskatchewan stations and in the absence of any other supporting specimens from that area, I believe that there might be locality error on the Richardson sheet. However, it is not beyond possibility that T. cernuum could occur there. This apparent disjunct station is not mapped here.</discussion>
  
</bio:treatment>