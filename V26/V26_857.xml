<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">419</other_info_on_meta>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">nolina</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">microcarpa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts.</publication_title>
      <place_in_publication>14: 247. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus nolina;species microcarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101804</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nolina</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="species">caudata</taxon_name>
    <taxon_hierarchy>genus Nolina;species caudata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent, cespitose;</text>
      <biological_entity id="o23212" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes from vertical, subterranean, branched caudices.</text>
      <biological_entity id="o23213" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity id="o23214" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="true" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o23213" id="r3159" name="from" negation="false" src="d0_s1" to="o23214" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades wiry, lax, concavo-convex, 80–130 cm  5–12 mm, not glaucous;</text>
      <biological_entity id="o23215" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concavo-convex" value_original="concavo-convex" />
        <character char_type="range_value" from="80" from_unit="cm" name="distance" src="d0_s2" to="130" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate, with close-set, cartilaginous teeth;</text>
      <biological_entity id="o23216" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o23217" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="close-set" value_original="close-set" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s3" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <relation from="o23216" id="r3160" name="with" negation="false" src="d0_s3" to="o23217" />
    </statement>
    <statement id="d0_s4">
      <text>apex lacerate;</text>
      <biological_entity id="o23218" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>inflorescence leaf-blades curling distally, 10–50 cm.</text>
      <biological_entity constraint="inflorescence" id="o23219" name="blade-leaf" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="course" src="d0_s5" value="curling" value_original="curling" />
        <character char_type="range_value" from="10" from_unit="cm" name="distance" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 3–15 dm, 1.2–2.5 cm diam.</text>
      <biological_entity id="o23220" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s6" to="15" to_unit="dm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="diameter" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, 4–12 dm × 10–30 cm, surpassing leaves;</text>
      <biological_entity id="o23221" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s7" to="12" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s7" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23222" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o23221" id="r3161" name="surpassing" negation="false" src="d0_s7" to="o23222" />
    </statement>
    <statement id="d0_s8">
      <text>bracts caducous, rarely persistent;</text>
      <biological_entity id="o23223" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bractlets 2–5 mm, slightly erose.</text>
      <biological_entity id="o23224" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals white, 1.5–3.3 mm;</text>
      <biological_entity id="o23225" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23226" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fertile stamens: filaments 1.6–1.9 mm, anthers 1.2–1.4 mm;</text>
      <biological_entity id="o23227" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o23228" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23229" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>infertile stamens: filaments 0.9–1.2 mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o23230" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="infertile" value_original="infertile" />
      </biological_entity>
      <biological_entity id="o23231" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23232" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>fruiting pedicel erect, proximal to joint 1–2 mm, distal to joint 3–6 mm.</text>
      <biological_entity id="o23233" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="infertile" value_original="infertile" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character constraint="to joint" constraintid="o23235" is_modifier="false" name="position" src="d0_s13" value="proximal" value_original="proximal" />
        <character constraint="to joint" constraintid="o23236" is_modifier="false" name="position_or_shape" notes="" src="d0_s13" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o23234" name="pedicel" name_original="pedicel" src="d0_s13" type="structure" />
      <biological_entity id="o23235" name="joint" name_original="joint" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23236" name="joint" name_original="joint" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o23233" id="r3162" name="fruiting" negation="false" src="d0_s13" to="o23234" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules hyaline, thin-walled, inflated, 4.2–6 × 5.4–6.4 mm, indistinctly notched at apex.</text>
      <biological_entity id="o23237" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="5.4" from_unit="mm" name="width" src="d0_s14" to="6.4" to_unit="mm" />
        <character constraint="at apex" constraintid="o23238" is_modifier="false" modifier="indistinctly" name="shape" src="d0_s14" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o23238" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds loosely invested in capsules, compressed, 2.2–3.4 × 1.5–3 mm. 2n = 38.</text>
      <biological_entity id="o23239" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s15" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23240" name="capsule" name_original="capsules" src="d0_s15" type="structure" />
      <biological_entity constraint="2n" id="o23241" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
      </biological_entity>
      <relation from="o23239" id="r3163" name="invested in" negation="false" src="d0_s15" to="o23240" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides, desert grasslands, oak and pinyon pine-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pinyon pine-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Sacahuista</other_name>
  <discussion>D. S. Correll and M. C. Johnston (1970) included Nolina microcarpa in their flora of Texas; however, they reported that they had seen no specimen from that state, nor have I. This species is found primarily from western New Mexico through central Arizona. It forms large clumps up to 2 m in diameter and inflorescences that generally are exserted from the basal leaf rosettes. Considerable variation occurs, some of it geographically restricted to southeastern Arizona and southwestern New Mexico. Most such plants from the latter areas have been referred to N. texana or N. caudata, but are here included in N. microcarpa. In the Grand Canyon area, there are variants that have been referred to N. parryi because in width the leaves approach those of N. parryi and they are serrulate. These plants, however, are acaulescent and also are here included in N. microcarpa. B. J. Albee et al. (1988) reported N. microcarpa on rocky slopes in canyons in Washington County, Utah, but the more recent online version of that work excludes it from Utah.</discussion>
  
</bio:treatment>