<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="treatment_page">536</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="(Rafinesque) Rafinesque" date="1833" rank="species">lacera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">lacera</taxon_name>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species lacera;variety lacera;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102302</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves usually persisting through anthesis, obovate.</text>
      <biological_entity id="o14260" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="through anthesis" is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="shape" src="d0_s0" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences secund or loosely spiraled, lax, with distinct space between proximal flowers (ratio of spike length/flower number equal to or greater than 2.3), sparsely pubescent, some trichomes capitate, glands obviously stalked.</text>
      <biological_entity id="o14261" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s1" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="between flowers" constraintid="o14263" id="o14262" name="space" name_original="space" src="d0_s1" type="structure" constraint_original="between  flowers, ">
        <character is_modifier="true" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14263" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o14264" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o14265" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s1" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o14261" id="r1978" name="with" negation="false" src="d0_s1" to="o14262" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers: lip 3–6 mm, broadest at apex;</text>
      <biological_entity id="o14266" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o14267" name="lip" name_original="lip" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character constraint="at apex" constraintid="o14268" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o14268" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>central portion green to yellowish green, with narrow, crisp white apron clearly defined to obscure.</text>
      <biological_entity id="o14269" name="flower" name_original="flowers" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 30.</text>
      <biological_entity constraint="central" id="o14270" name="portion" name_original="portion" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="yellowish green" />
        <character char_type="range_value" from="clearly defined" modifier="with narrow , crisp white apron" name="prominence" src="d0_s3" to="obscure" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14271" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to moist meadows, open woods, barrens, old fields, roadsides, 0–700 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to moist meadows" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="700 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Ark., Conn., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6a.</number>
  <discussion>Hybrids of Spiranthes lacera var. lacera with S. romanzoffiana are known as Spiranthes ×simpsonii Catling &amp; Sheviak.</discussion>
  
</bio:treatment>