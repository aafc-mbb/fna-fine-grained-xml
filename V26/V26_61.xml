<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">veratrum</taxon_name>
    <taxon_name authority="Durand" date="1855" rank="species">californicum</taxon_name>
    <taxon_name authority="(A. Heller) C. L. Hitchcock in C. L. Hitchcock et al." date="1969" rank="variety">caudatum</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>1: 809. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus veratrum;species californicum;variety caudatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102324</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veratrum</taxon_name>
    <taxon_name authority="A. Heller" date="unknown" rank="species">caudatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 588. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Veratrum;species caudatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Panicle unbranched in distal 1/3–1/2, densely flowered;</text>
      <biological_entity id="o8182" name="panicle" name_original="panicle" src="d0_s0" type="structure">
        <character constraint="in distal 1/3-1/2" constraintid="o8183" is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="densely" name="architecture" notes="" src="d0_s0" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8183" name="1/3-1/2" name_original="1/3-1/2" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>bracts in proximal unbranched portion lanceolate, 2–3 times longer than flowers, 2–3 cm.</text>
      <biological_entity id="o8184" name="bract" name_original="bracts" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o8185" name="bract" name_original="bracts" src="d0_s1" type="structure" />
      <biological_entity id="o8186" name="portion" name_original="portion" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character constraint="flower" constraintid="o8187" is_modifier="false" name="length_or_size" src="d0_s1" value="2-3 times longer than flowers" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8187" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <relation from="o8184" id="r1174" name="in" negation="false" src="d0_s1" to="o8185" />
    </statement>
    <statement id="d0_s2">
      <text>Tepals lanceolate, 12–17 mm.</text>
      <biological_entity id="o8188" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet marshy meadows and coniferous forest openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet marshy meadows" />
        <character name="habitat" value="coniferous forest openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4b.</number>
  <discussion>Veratrum californicum var. caudatum occurs mostly west of the Cascade Mountains in the Pacific Northwest.</discussion>
  
</bio:treatment>