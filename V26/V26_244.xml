<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1818" rank="genus">clintonia</taxon_name>
    <taxon_name authority="(Menzies ex Schultes) Kunth" date="1850" rank="species">uniflora</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>5: 159. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus clintonia;species uniflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101531</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilacina</taxon_name>
    <taxon_name authority="Ker Gawler" date="unknown" rank="species">borealis</taxon_name>
    <taxon_name authority="Menzies ex Schultes" date="unknown" rank="variety">uniflora</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer et al., Syst. Veg.</publication_title>
      <place_in_publication>7(1): 307. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Smilacina;species borealis;variety uniflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilacina</taxon_name>
    <taxon_name authority="(Menzies ex Schultes) Hooker" date="unknown" rank="species">uniflora</taxon_name>
    <taxon_hierarchy>genus Smilacina;species uniflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1.5–2.5 dm;</text>
      <biological_entity id="o22377" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes elongate.</text>
      <biological_entity id="o22378" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves 2–3;</text>
      <biological_entity constraint="cauline" id="o22379" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade pale green adaxially, oblanceolate to obovate, 8–20 × 2.5–6.5 cm.</text>
      <biological_entity id="o22380" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s3" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1 (–2) -flowered, erect;</text>
      <biological_entity id="o22381" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1(-2)-flowered" value_original="1(-2)-flowered" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bract 1, narrowly linear.</text>
      <biological_entity id="o22382" name="bract" name_original="bract" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals creamy white, broadly obovate to oblanceolate, 18–25 × 5–7 mm;</text>
      <biological_entity id="o22383" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22384" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="creamy white" value_original="creamy white" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 11–18 mm;</text>
      <biological_entity id="o22385" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22386" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers oblong-linear, 3.5–5.5 mm.</text>
      <biological_entity id="o22387" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22388" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Berries deep lustrous blue, subglobose to pyriform, 10–18-seeded, 6–12 mm.</text>
      <biological_entity id="o22389" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s9" to="pyriform" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-18-seeded" value_original="10-18-seeded" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 3–4 mm. 2n = 28.</text>
      <biological_entity id="o22390" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22391" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Queen’s-cup</other_name>
  <other_name type="common_name">bride’s- bonnet</other_name>
  
</bio:treatment>