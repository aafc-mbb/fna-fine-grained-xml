<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="genus">scoliopus</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">hallii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 272. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus scoliopus;species hallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101888</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems very short, 2.5–10 mm.</text>
      <biological_entity id="o14013" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2 (–3), subsessile;</text>
      <biological_entity id="o14014" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="3" />
        <character name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade rarely mottled with purple, oblongelliptic, 8–14 × 2.5–5 cm.</text>
      <biological_entity id="o14015" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="mottled with purple" value_original="mottled with purple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 1–5;</text>
      <biological_entity id="o14016" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>outer tepals yellowish green with purple lines, lanceolate to oblanceolate, 6.5–10 mm;</text>
      <biological_entity constraint="outer" id="o14017" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character constraint="with lines" constraintid="o14018" is_modifier="false" name="coloration" src="d0_s4" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14018" name="line" name_original="lines" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>inner tepals linear-spatulate, 6–9 × 0.3 mm;</text>
      <biological_entity constraint="inner" id="o14019" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-spatulate" value_original="linear-spatulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character name="width" src="d0_s5" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments 2–4 mm;</text>
      <biological_entity id="o14020" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 1.9–2.3 mm;</text>
      <biological_entity id="o14021" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s7" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 2–2.5 mm;</text>
      <biological_entity id="o14022" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas spreading to recurved;</text>
      <biological_entity id="o14023" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s9" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicel very slender, elongating to 6–10 cm.</text>
      <biological_entity id="o14024" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character constraint="to 6-10 cm" is_modifier="false" name="length" src="d0_s10" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 16–18.5 × 5.5–7.5 mm, including beak.</text>
      <biological_entity id="o14026" name="beak" name_original="beak" src="d0_s11" type="structure" />
      <relation from="o14025" id="r1947" name="including" negation="false" src="d0_s11" to="o14026" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 14.</text>
      <biological_entity id="o14025" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s11" to="18.5" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14027" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous woods, mossy mountain stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous woods" />
        <character name="habitat" value="mossy mountain stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Oregon fetid adder’s-tongue</other_name>
  
</bio:treatment>