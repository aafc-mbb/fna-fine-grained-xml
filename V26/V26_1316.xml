<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">640</other_info_on_meta>
    <other_info_on_meta type="treatment_page">641</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Eulophiinae</taxon_name>
    <taxon_name authority="Lindley" date="1832" rank="genus">oeceoclades</taxon_name>
    <taxon_name authority="(Lindley) Lindley" date="1833" rank="species">maculata</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Orchid. Pl.,</publication_title>
      <place_in_publication>237. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe eulophiinae;genus oeceoclades;species maculata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101809</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Angraecum</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">maculatum</taxon_name>
    <place_of_publication>
      <publication_title>Coll. Bot., plate</publication_title>
      <place_in_publication>15 and text on facing page. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Angraecum;species maculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eulophia</taxon_name>
    <taxon_name authority="(Lindley) Reichenbach f." date="unknown" rank="species">maculata</taxon_name>
    <taxon_hierarchy>genus Eulophia;species maculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eulophidium</taxon_name>
    <taxon_name authority="(Lindley) Pfitzer" date="unknown" rank="species">maculatum</taxon_name>
    <taxon_hierarchy>genus Eulophidium;species maculatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–43 cm.</text>
      <biological_entity id="o5366" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="43" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous, white, stout.</text>
      <biological_entity id="o5367" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pseudobulbs dark green, ovoid to orbicular, 2–5 × 1–3 cm, often concealed by imbricate fibrous sheaths or 1 closely appressed sheath, 1-leaved at apex.</text>
      <biological_entity id="o5368" name="pseudobulb" name_original="pseudobulbs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s2" to="orbicular" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
        <character constraint="by sheaths, sheath" constraintid="o5369, o5370" is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="concealed" value_original="concealed" />
        <character constraint="at apex" constraintid="o5371" is_modifier="false" name="architecture" notes="" src="d0_s2" value="1-leaved" value_original="1-leaved" />
      </biological_entity>
      <biological_entity id="o5369" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="true" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o5370" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="true" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o5371" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade nearly erect, olive green with dark green mottling, oblongelliptic, abaxially keeled, 12–32 × 3.5–5.5 cm, succulent leathery, apex acute.</text>
      <biological_entity id="o5372" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5373" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="olive green with dark green mottling" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s3" to="32" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s3" to="5.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o5374" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences lax, 10–42 cm;</text>
      <biological_entity id="o5375" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="42" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles erect, slender, with few remote sheaths.</text>
      <biological_entity id="o5376" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o5377" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s5" value="remote" value_original="remote" />
      </biological_entity>
      <relation from="o5376" id="r788" name="with" negation="false" src="d0_s5" to="o5377" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5–15, 9–15 mm wide;</text>
      <biological_entity id="o5378" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="15" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals and petals light-brown to pinkish green;</text>
      <biological_entity id="o5379" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s7" to="pinkish green" />
      </biological_entity>
      <biological_entity id="o5380" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s7" to="pinkish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 8–12 × 2–3 mm;</text>
      <biological_entity id="o5381" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal linear-oblong, apex acute;</text>
      <biological_entity constraint="dorsal" id="o5382" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <biological_entity id="o5383" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals slightly falcate;</text>
      <biological_entity constraint="lateral" id="o5384" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals oblongelliptic, 12 × 4 mm, apex acute;</text>
      <biological_entity id="o5385" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblongelliptic" value_original="oblongelliptic" />
        <character name="length" src="d0_s11" unit="mm" value="12" value_original="12" />
        <character name="width" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o5386" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip white with 2 parallel pink blotches, 9–14 × 8–12 mm, lateral lobes erect, white with thin, radiating purple lines, orbiculate, spur curved, 5 × 2 mm;</text>
      <biological_entity id="o5387" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character constraint="with lateral lobes, spur" constraintid="o5388, o5389" is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character name="length" notes="" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character name="width" notes="" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5388" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="parallel" value_original="parallel" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="pink blotches" value_original="pink blotches" />
        <character char_type="range_value" from="9" from_unit="mm" is_modifier="true" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="width" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5389" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="parallel" value_original="parallel" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="pink blotches" value_original="pink blotches" />
        <character char_type="range_value" from="9" from_unit="mm" is_modifier="true" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="width" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o5390" name="line" name_original="lines" src="d0_s12" type="structure">
        <character is_modifier="true" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="radiating" value_original="radiating" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o5388" id="r789" name="with" negation="false" src="d0_s12" to="o5390" />
      <relation from="o5389" id="r790" name="with" negation="false" src="d0_s12" to="o5390" />
    </statement>
    <statement id="d0_s13">
      <text>column curved, white, 5–7 mm.</text>
      <biological_entity id="o5391" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules pendent, elliptic-oblong, to 3.5 cm.</text>
      <biological_entity id="o5392" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s14" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In humus under shady forests, in orchards and residential areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" modifier="in" constraint="under shady forests" />
        <character name="habitat" value="shady forests" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="residential areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Oeceoclades maculata was first discovered in the United States in Miami-Dade County, Florida in 1974, and is spreading rapidly in central and southern Florida. It is not known whether it escaped from cultivation or arrived in Florida via windblown seeds from the Greater Antilles or the nearby Bahama Archipelago.</discussion>
  
</bio:treatment>