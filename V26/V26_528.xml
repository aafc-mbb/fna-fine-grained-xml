<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">277</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Kunth" date="1843" rank="genus">nothoscordum</taxon_name>
    <taxon_name authority="(Linnaeus) Britton in N. L. Britton and A. Brown" date="1896" rank="species">bivalve</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown, Ill. Fl. N. U.S.</publication_title>
      <place_in_publication>1: 415. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus nothoscordum;species bivalve</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101807</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ornithogalum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">bivalve</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 306. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ornithogalum;species bivalve;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">bivalve</taxon_name>
    <taxon_hierarchy>genus Allium;species bivalve;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">striatum</taxon_name>
    <taxon_hierarchy>genus Allium;species striatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nothoscordum</taxon_name>
    <taxon_name authority="(Jacquin) Kunth" date="unknown" rank="species">striatum</taxon_name>
    <taxon_hierarchy>genus Nothoscordum;species striatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nothoscordum</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">texanum</taxon_name>
    <taxon_hierarchy>genus Nothoscordum;species texanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs globose to subglobose, to 1–1.5 cm diam.;</text>
      <biological_entity id="o3702" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s0" to="subglobose" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats brown;</text>
      <biological_entity constraint="outer" id="o3703" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o3704" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape solitary (rarely 2), terete, (10–) 20–40 cm × 1–3 mm.</text>
      <biological_entity id="o3705" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" src="d0_s3" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 1–4, sheaths enveloping neck of bulb, hyaline, subtruncate apically;</text>
      <biological_entity id="o3706" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o3707" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s4" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o3708" name="neck" name_original="neck" src="d0_s4" type="structure" />
      <biological_entity id="o3709" name="bulb" name_original="bulb" src="d0_s4" type="structure" />
      <relation from="o3707" id="r537" name="enveloping" negation="false" src="d0_s4" to="o3708" />
      <relation from="o3707" id="r538" name="part_of" negation="false" src="d0_s4" to="o3709" />
    </statement>
    <statement id="d0_s5">
      <text>blade filiform or linear, to 30 cm × 1–4 (–5) mm, margins entire.</text>
      <biological_entity id="o3710" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3711" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel mostly 3–6 (–10) -flowered, often asymmetrical, 1–3 cm diam.;</text>
      <biological_entity id="o3712" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s6" value="3-6(-10)-flowered" value_original="3-6(-10)-flowered" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s6" value="asymmetrical" value_original="asymmetrical" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts persistent, 2, 1–2 cm × 4–8 mm, base coherent, imbricate, margins scarious or hyaline, apex acuminate.</text>
      <biological_entity id="o3713" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3714" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="coherent" value_original="coherent" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o3715" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o3716" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers not fragrant;</text>
      <biological_entity id="o3717" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s8" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals whitish to cream, at least outer ones with red or purplish red midvein, elliptic, (8–) 10–12 (–15) × 3–4.5 mm, apex acute or acuminate;</text>
      <biological_entity id="o3718" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="cream" />
      </biological_entity>
      <biological_entity id="o3719" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s9" value="outer" value_original="outer" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3720" name="midvein" name_original="midvein" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="purplish red" value_original="purplish red" />
      </biological_entity>
      <biological_entity id="o3721" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o3719" id="r539" name="with" negation="false" src="d0_s9" to="o3720" />
    </statement>
    <statement id="d0_s10">
      <text>filaments simple, adnate to tepals, 5–6 mm;</text>
      <biological_entity id="o3722" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character constraint="to tepals" constraintid="o3723" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3723" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o3724" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary crestless;</text>
      <biological_entity id="o3725" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="crestless" value_original="crestless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style persistent, equaling stamens;</text>
      <biological_entity id="o3726" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o3727" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma unlobed, 3/4 perianth;</text>
      <biological_entity id="o3728" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="unlobed" value_original="unlobed" />
        <character name="quantity" src="d0_s14" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o3729" name="perianth" name_original="perianth" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel erect, 1–2 cm, elongating as flowers develop, but often of several lengths even in fruit.</text>
      <biological_entity id="o3730" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s15" to="2" to_unit="cm" />
        <character constraint="as flowers" constraintid="o3731" is_modifier="false" name="length" src="d0_s15" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o3731" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o3732" name="fruit" name_original="fruit" src="d0_s15" type="structure" />
      <relation from="o3731" id="r540" modifier="of several lengths" name="in" negation="false" src="d0_s15" to="o3732" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules subglobose or obovoid, 6–8 × 6–8 mm.</text>
      <biological_entity id="o3733" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s16" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 4–7 per locule.</text>
      <biological_entity id="o3734" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o3735" from="4" name="quantity" src="d0_s17" to="7" />
      </biological_entity>
      <biological_entity id="o3735" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods, prairies, barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Fla., Ga., Ill., Ind., Kans., Ky., La., Miss., Mo., N.Mex., N.C., Ohio, Okla., S.C., Tenn., Tex., Va.; s to South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="s to South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>