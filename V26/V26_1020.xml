<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="treatment_page">501</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="subfamily">Cypripedioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cypripedium</taxon_name>
    <taxon_name authority="Kellogg ex S. Watson" date="1882" rank="species">fasciculatum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 380. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily cypripedioideae;genus cypripedium;species fasciculatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101548</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cypripedium</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">knightiae</taxon_name>
    <taxon_hierarchy>genus Cypripedium;species knightiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 6–35 cm in flower, taller in fruit;</text>
      <biological_entity id="o27991" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="in flower" constraintid="o27992" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character constraint="in fruit" constraintid="o27993" is_modifier="false" name="height" notes="" src="d0_s0" value="taller" value_original="taller" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27992" name="flower" name_original="flower" src="d0_s0" type="structure" />
      <biological_entity id="o27993" name="fruit" name_original="fruit" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>inflorescences nodding in flower, erect in fruit.</text>
      <biological_entity id="o27994" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character constraint="in flower" constraintid="o27995" is_modifier="false" name="orientation" src="d0_s1" value="nodding" value_original="nodding" />
        <character constraint="in fruit" constraintid="o27996" is_modifier="false" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o27995" name="flower" name_original="flower" src="d0_s1" type="structure" />
      <biological_entity id="o27996" name="fruit" name_original="fruit" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2, initially near middle of stem, subopposite, widespreading, distal portion elongating greatly in fruit;</text>
      <biological_entity id="o27997" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="widespreading" value_original="widespreading" />
      </biological_entity>
      <biological_entity constraint="middle" id="o27998" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o27999" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o28000" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character constraint="in fruit" constraintid="o28001" is_modifier="false" name="length" src="d0_s2" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o28001" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <relation from="o27997" id="r3789" modifier="initially" name="near" negation="false" src="d0_s2" to="o27998" />
      <relation from="o27998" id="r3790" name="part_of" negation="false" src="d0_s2" to="o27999" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to ovate-orbiculate, 4–12 × 2.5–7.5 cm.</text>
      <biological_entity id="o28002" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate-orbiculate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s3" to="7.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers (1–) 2–4;</text>
      <biological_entity id="o28003" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals dull yellow, marked and suffused often intensely with reddish-brown or dark purple;</text>
      <biological_entity id="o28004" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often intensely; intensely; intensely; often intensely; intensely" name="coloration" src="d0_s5" value="suffused often intensely with reddish-brown or suffused often intensely with dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>dorsal sepal lanceolate, 13–25 × 3–8 mm;</text>
      <biological_entity constraint="dorsal" id="o28005" name="sepal" name_original="sepal" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals connate, synsepal 11–23 × 4–9 mm;</text>
      <biological_entity constraint="lateral" id="o28006" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o28007" name="synsepal" name_original="synsepal" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s7" to="23" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals porrect-spreading, same color as sepals, flat, ovatelanceolate to lance-acuminate, 10–23 × 6–17 mm;</text>
      <biological_entity id="o28008" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="porrect-spreading" value_original="porrect-spreading" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="flat" value_original="flat" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s8" to="lance-acuminate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="23" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28009" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <relation from="o28008" id="r3791" name="as" negation="false" src="d0_s8" to="o28009" />
    </statement>
    <statement id="d0_s9">
      <text>lip color of sepals, usually less intensely marked, mostly obovoid, 8–14 (–25) mm, orifice basal, 4–5 mm;</text>
      <biological_entity id="o28010" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually less; less intensely; intensely; mostly" name="coloration" notes="" src="d0_s9" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28011" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o28012" name="orifice" name_original="orifice" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="basal" value_original="basal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode ellipsoid to oblong.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 20.</text>
      <biological_entity id="o28013" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s10" to="oblong" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28014" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry coniferous forests and thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to dry coniferous forests" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Mont., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Clustered lady’s-slipper</other_name>
  <other_name type="common_name">brownie lady’s-slipper</other_name>
  <references>
    <reference>  Brownell, V. R. and P. M. Catling. 1987. Notes on the distribution and taxonomy of Cypripedium fasciculatum Kellogg ex Watson (Orchidaceae). Lindleyana 2: 53–57.</reference>
  </references>
  
</bio:treatment>