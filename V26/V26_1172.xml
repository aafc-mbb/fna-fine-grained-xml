<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="treatment_page">574</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Rydberg" date="1901" rank="genus">piperia</taxon_name>
    <taxon_name authority="Rand. Morgan &amp; Ackerman" date="1990" rank="species">yadonii</taxon_name>
    <place_of_publication>
      <publication_title>Lindleyana</publication_title>
      <place_in_publication>5: 209, figs. 1A–G, 2. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus piperia;species yadonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101824</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–50 (–80) cm.</text>
      <biological_entity id="o32992" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems attenuate toward tuberoid, 0.5–3.5 mm diam.;</text>
      <biological_entity id="o32993" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="toward tuberoid" constraintid="o32994" is_modifier="false" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" notes="" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32994" name="tuberoid" name_original="tuberoid" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>bracts (4–) 7–20 (–26).</text>
      <biological_entity id="o32995" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s2" to="7" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="26" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s2" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves prostrate;</text>
      <biological_entity id="o32996" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 11–17 × 2.2–3.9 cm.</text>
      <biological_entity id="o32997" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s4" to="3.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences densely flowered, cylindric, (2–) 5–15 (–30) cm;</text>
      <biological_entity id="o32998" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis shorter than peduncle;</text>
      <biological_entity id="o32999" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character constraint="than peduncle" constraintid="o33000" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o33000" name="peduncle" name_original="peduncle" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts (3–) 5–8 (–11) mm.</text>
      <biological_entity id="o33001" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers green-and-white, fragrance faint, harsh to honeylike, diurnal;</text>
      <biological_entity id="o33002" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green-and-white" value_original="green-and-white" />
        <character is_modifier="false" name="fragrance" src="d0_s8" value="faint" value_original="faint" />
        <character char_type="range_value" from="harsh" name="odor" src="d0_s8" to="honeylike" />
        <character is_modifier="false" name="duration" src="d0_s8" value="diurnal" value_original="diurnal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 3–5.5 × 1–2.5 mm;</text>
      <biological_entity id="o33003" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dorsal sepal green with white margins, elliptic-lanceolate;</text>
      <biological_entity constraint="dorsal" id="o33004" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character constraint="with margins" constraintid="o33005" is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
      </biological_entity>
      <biological_entity id="o33005" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lateral sepals spreading to recurved, white, lanceolate;</text>
      <biological_entity constraint="lateral" id="o33006" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s11" to="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals erect-recurved, green with broad outer, narrow inner white borders, lanceolate, falcate, 3–5 × 1.5 mm, inner margins often forming U, apices often connivent;</text>
      <biological_entity id="o33007" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect-recurved" value_original="erect-recurved" />
        <character constraint="with outer" constraintid="o33008" is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o33008" name="outer" name_original="outer" src="d0_s12" type="structure">
        <character is_modifier="true" name="width" src="d0_s12" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity constraint="inner" id="o33009" name="border" name_original="borders" src="d0_s12" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="inner" id="o33010" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o33011" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s12" value="connivent" value_original="connivent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lip recurved, triangular-lanceolate, 2.5–5 × 1.2–2.5 mm;</text>
      <biological_entity id="o33012" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>spur deflexed, 2–5 mm;</text>
      <biological_entity id="o33013" name="spur" name_original="spur" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="deflexed" value_original="deflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>viscidia ovate to broadly elliptic, 0.4–0.5 × 0.3–0.4 mm;</text>
      <biological_entity id="o33014" name="viscidium" name_original="viscidia" src="d0_s15" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s15" to="broadly elliptic" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s15" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>rostellum blunt.</text>
      <biological_entity id="o33015" name="rostellum" name_original="rostellum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 5–9 mm.</text>
      <biological_entity id="o33016" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds cinnamon brown.</text>
      <biological_entity id="o33017" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="cinnamon brown" value_original="cinnamon brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May–early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Monterey pine forest, maritime chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="monterey pine forest" />
        <character name="habitat" value="maritime chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Piperia yadonii, a narrow endemic (Monterey Bay area), bears a superficial resemblance to P. elegans. Much of the P. yadonii habitat has been preempted for residential and golf course development.</discussion>
  
</bio:treatment>