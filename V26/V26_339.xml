<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter C. Holmes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1762" rank="genus">ALSTROEMERIA</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Alströmeria,</publication_title>
      <place_in_publication>8. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus ALSTROEMERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Clas Alströmer, 1736–1794, Swedish naturalist and pupil of Linnaeus</other_info_on_name>
    <other_info_on_name type="fna_id">101212</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from fascicles of fusiform tubers.</text>
      <biological_entity id="o25722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o25723" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <relation from="o25722" id="r3488" modifier="from fascicles" name="part_of" negation="false" src="d0_s0" to="o25723" />
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly simple;</text>
      <biological_entity id="o25724" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>fertile stems to 1 m or more;</text>
      <biological_entity id="o25725" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="1" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sterile stems shorter, more leafy.</text>
      <biological_entity id="o25726" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves alternate;</text>
      <biological_entity id="o25727" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole often twisted so as to invert leaf;</text>
      <biological_entity id="o25728" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character constraint="as-to-invert leaf" constraintid="o25729" is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o25729" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade parallel-veined, linear to ovate, margins entire.</text>
      <biological_entity id="o25730" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="parallel-veined" value_original="parallel-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
      <biological_entity id="o25731" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, umbellate [or 1-flowered].</text>
      <biological_entity id="o25732" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers slightly zygomorphic;</text>
      <biological_entity id="o25733" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals 6, distinct, red, orange, purple, green, or white, frequently spotted, to 5 cm;</text>
      <biological_entity id="o25734" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="frequently" name="coloration" src="d0_s9" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, inserted on perianth base, declinate, usually unequal;</text>
      <biological_entity id="o25735" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="declinate" value_original="declinate" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="perianth" id="o25736" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o25735" id="r3489" name="inserted on" negation="false" src="d0_s10" to="o25736" />
    </statement>
    <statement id="d0_s11">
      <text>ovary inferior;</text>
      <biological_entity id="o25737" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style slender;</text>
      <biological_entity id="o25738" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma 3-lobed, filiform.</text>
      <biological_entity id="o25739" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsular, 3-valved, dehiscence loculicidal.</text>
      <biological_entity id="o25740" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; North America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Lily-of-the-Incas</other_name>
  <other_name type="common_name">Peruvian-lily</other_name>
  <discussion>Species ca. 60 (1 in the flora).</discussion>
  <references>
    <reference>Aker, S. and W. Healy. 1990. The phytogeography of the genus Alstroemeria. Herbertia 46: 76–87.</reference>
    <reference>Uphoff, J. C. T. 1952. A review of the genus Alstroemeria. Pl. Life 8: 37–53.</reference>
  </references>
  
</bio:treatment>