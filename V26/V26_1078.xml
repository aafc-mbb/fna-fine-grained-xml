<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="treatment_page">529</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">sacoila</taxon_name>
    <taxon_name authority="(Aublet) Garay" date="1982" rank="species">lanceolata</taxon_name>
    <taxon_name authority="(Luer) Sauleda" date="1984" rank="variety">paludicola</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>56: 308. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus sacoila;species lanceolata;variety paludicola;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102292</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="(Aublet) León" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_name authority="Luer" date="unknown" rank="variety">paludicola</taxon_name>
    <place_of_publication>
      <publication_title>Florida Orchidist</publication_title>
      <place_in_publication>14: 19. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiranthes;species lanceolata;variety paludicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves present at flowering time.</text>
      <biological_entity id="o11385" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o11386" name="time" name_original="time" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
      </biological_entity>
      <relation from="o11385" id="r1604" name="present at" negation="false" src="d0_s0" to="o11386" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences 3–15 cm;</text>
      <biological_entity id="o11387" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>floral bracts red, lanceolate, 12–15 mm.</text>
      <biological_entity constraint="floral" id="o11388" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 9–29, deep red;</text>
      <biological_entity id="o11389" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s3" to="29" />
        <character is_modifier="false" name="depth" src="d0_s3" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>dorsal sepal 9–11 × 3.5–4.5 mm;</text>
      <biological_entity constraint="dorsal" id="o11390" name="sepal" name_original="sepal" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral sepals 12–14 × 33–42 mm, connate 1–2 mm over basal portion of mentum, distinct portion of mentum 0.8–1.2 mm;</text>
      <biological_entity constraint="lateral" id="o11391" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="33" from_unit="mm" name="width" src="d0_s5" to="42" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
        <character char_type="range_value" constraint="over basal portion" constraintid="o11392" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11392" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o11393" name="mentum" name_original="mentum" src="d0_s5" type="structure" />
      <biological_entity id="o11394" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11395" name="mentum" name_original="mentum" src="d0_s5" type="structure" />
      <relation from="o11392" id="r1605" name="part_of" negation="false" src="d0_s5" to="o11393" />
      <relation from="o11394" id="r1606" name="part_of" negation="false" src="d0_s5" to="o11395" />
    </statement>
    <statement id="d0_s6">
      <text>petals 9–10.5 × 2.2–2.8 mm;</text>
      <biological_entity id="o11396" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s6" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lip red to pale-red, 11–13 × 3.8–4.5 mm;</text>
      <biological_entity id="o11397" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s7" to="pale-red" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="width" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>column 6–7.5 mm from attachment of dorsal sepal to tip, foot extending back 3–4 mm;</text>
      <biological_entity id="o11398" name="column" name_original="column" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from attachment" constraintid="o11399" from="6" from_unit="mm" name="location" src="d0_s8" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11399" name="attachment" name_original="attachment" src="d0_s8" type="structure" />
      <biological_entity constraint="dorsal" id="o11400" name="sepal" name_original="sepal" src="d0_s8" type="structure" />
      <biological_entity id="o11401" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o11402" name="foot" name_original="foot" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11403" name="back" name_original="back" src="d0_s8" type="structure" />
      <relation from="o11399" id="r1607" name="part_of" negation="false" src="d0_s8" to="o11400" />
      <relation from="o11399" id="r1608" name="to" negation="false" src="d0_s8" to="o11401" />
      <relation from="o11402" id="r1609" name="extending" negation="false" src="d0_s8" to="o11403" />
    </statement>
    <statement id="d0_s9">
      <text>pollinaria 3–3.5 mm;</text>
      <biological_entity id="o11404" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>viscidium 2.9–3.1 mm;</text>
      <biological_entity id="o11405" name="viscidium" name_original="viscidium" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s10" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicellate ovary 12–15 mm;</text>
      <biological_entity id="o11406" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rostellum 2.3–2.8 mm.</text>
      <biological_entity id="o11407" name="rostellum" name_original="rostellum" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 12 × 6 mm.</text>
      <biological_entity id="o11408" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character name="length" src="d0_s13" unit="mm" value="12" value_original="12" />
        <character name="width" src="d0_s13" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds monoembryonic.</text>
      <biological_entity id="o11409" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monoembryonic" value_original="monoembryonic" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jan–early Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Apr" from="late Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet hardwood forests on sandy or organic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet hardwood forests" />
        <character name="habitat" value="sandy" modifier="on" />
        <character name="habitat" value="organic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Swamp beaked orchid</other_name>
  <discussion>Sacoila lanceolata var. paludicola is very uncommon. It is local in Collier County and introduced in Miami-Dade and Broward counties, Florida.</discussion>
  
</bio:treatment>