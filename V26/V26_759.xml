<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="Böcher" date="1966" rank="species">groenlandicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>61: 286, figs. 3a, e–i, 5–7. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species groenlandicum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101905</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, green to olive or dark olive when dry, to 3.3 dm, not glaucous;</text>
      <biological_entity id="o33544" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="olive or dark olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="3.3" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes scarcely discernable.</text>
      <biological_entity id="o33545" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely" name="prominence" src="d0_s1" value="discernable" value_original="discernable" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, often with purplish tinge apically, 1–2 mm wide, glabrous, margins entire, similar in color and texture to stem body.</text>
      <biological_entity id="o33546" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o33547" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="purplish tinge" value_original="purplish tinge" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="true" modifier="apically" name="width" src="d0_s2" value="wide" value_original="wide" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="stem" id="o33548" name="body" name_original="body" src="d0_s2" type="structure" />
      <relation from="o33546" id="r4517" modifier="often" name="with" negation="false" src="d0_s2" to="o33547" />
      <relation from="o33546" id="r4518" name="to" negation="false" src="d0_s2" to="o33548" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o33549" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33550" name="base" name_original="bases" src="d0_s3" type="structure">
        <character constraint="in tufts" constraintid="o33551" is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o33551" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o33552" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes tinged purplish, at least on hyaline margins, glabrous, keels entire;</text>
      <biological_entity id="o33553" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="tinged purplish" value_original="tinged purplish" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33554" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o33555" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o33553" id="r4519" modifier="at-least" name="on" negation="false" src="d0_s5" to="o33554" />
    </statement>
    <statement id="d0_s6">
      <text>outer 25.4–44 mm, 10.9–23.3 mm longer than inner, usually tapering evenly towards apex, margins basally connate 1–2.5 mm;</text>
      <biological_entity constraint="outer" id="o33556" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="25.4" from_unit="mm" name="some_measurement" src="d0_s6" to="44" to_unit="mm" />
        <character char_type="range_value" from="10.9" from_unit="mm" name="some_measurement" src="d0_s6" to="23.3" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o33557" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o33558" is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o33557" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o33558" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o33559" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner with keel evenly curved to straight, hyaline margins 0.3–0.6 mm wide, apex acute to acuminate, ending 0.3–1.6 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o33560" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o33561" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character char_type="range_value" from="evenly curved" name="course" src="d0_s7" to="straight" />
      </biological_entity>
      <biological_entity id="o33562" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33563" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o33564" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o33560" id="r4520" name="with" negation="false" src="d0_s7" to="o33561" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals pale blue to whitish, bases yellow;</text>
      <biological_entity id="o33565" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33566" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s8" to="whitish" />
      </biological_entity>
      <biological_entity id="o33567" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals 5.7–10.3 mm, apex rounded, aristate;</text>
      <biological_entity id="o33568" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o33569" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" src="d0_s9" to="10.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33570" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments connate ± entirely, glabrous or slightly stipitate-glandular basally;</text>
      <biological_entity id="o33571" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33572" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly; basally" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o33573" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33574" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o33575" name="foliage" name_original="foliage" src="d0_s11" type="structure" />
      <relation from="o33574" id="r4521" name="to" negation="false" src="d0_s11" to="o33575" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules dark-brown to black, slightly turbinate to subglobose, 2.8–6.5 mm;</text>
      <biological_entity id="o33576" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s12" to="black" />
        <character char_type="range_value" from="slightly turbinate" name="shape" src="d0_s12" to="subglobose" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicel usually ascending.</text>
      <biological_entity id="o33577" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose to obconic, lacking obvious depression, 0.7–1.2 mm, granular.</text>
      <biological_entity id="o33578" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 32.</text>
      <biological_entity id="o33579" name="depression" name_original="depression" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief_or_texture" src="d0_s14" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33580" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Southerly exposures in steppe/heath vegetation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="southerly exposures" constraint="in steppe\/heath vegetation" />
        <character name="habitat" value="steppe\/heath vegetation" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36.</number>
  
</bio:treatment>