<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1899" rank="species">idahoense</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 445. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species idahoense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101908</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, green to ashy olive when dry, to 4.5 dm, not glaucous;</text>
      <biological_entity id="o11737" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="ashy olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="4.5" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes scarcely discernable.</text>
      <biological_entity id="o11738" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely" name="prominence" src="d0_s1" value="discernable" value_original="discernable" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, obviously winged, 1–2.5 mm wide, glabrous, margins entire to denticulate apically, similar in color and texture to stem body.</text>
      <biological_entity id="o11739" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11740" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" modifier="apically" name="shape" src="d0_s2" to="denticulate" />
      </biological_entity>
      <biological_entity constraint="stem" id="o11741" name="body" name_original="body" src="d0_s2" type="structure" />
      <relation from="o11740" id="r1644" name="to" negation="false" src="d0_s2" to="o11741" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o11742" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11743" name="base" name_original="bases" src="d0_s3" type="structure">
        <character constraint="in tufts" constraintid="o11744" is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11744" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o11745" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes usually green, glabrous, keels entire to denticulate;</text>
      <biological_entity id="o11746" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11747" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer 14–55 (–65) mm, 13–16 mm longer than inner, basally connate 2.6–7 mm, tapering evenly towards apex;</text>
      <biological_entity constraint="outer" id="o11748" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="65" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s6" to="55" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o11749" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character constraint="towards apex" constraintid="o11750" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11749" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o11750" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner with keel evenly curved to straight, hyaline margins 0.1–0.5 mm wide, apex acuminate to acute, ending 0.5–2.1 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o11751" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o11752" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character char_type="range_value" from="evenly curved" name="course" src="d0_s7" to="straight" />
      </biological_entity>
      <biological_entity id="o11753" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11754" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s7" to="acute" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o11755" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o11751" id="r1645" name="with" negation="false" src="d0_s7" to="o11752" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals light to deep bluish violet or occasionally purple, bases yellow;</text>
      <biological_entity id="o11756" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11757" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s8" to="deep bluish violet or occasionally purple" />
      </biological_entity>
      <biological_entity id="o11758" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals 8–20 mm, apex rounded, truncate, or emarginate, aristate;</text>
      <biological_entity id="o11759" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o11760" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11761" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o11762" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11763" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o11764" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11765" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o11766" name="foliage" name_original="foliage" src="d0_s11" type="structure" />
      <relation from="o11765" id="r1646" name="to" negation="false" src="d0_s11" to="o11766" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules beige to light or dark-brown, occasionally with purple blotches on apex, ± globose to pyriform, 3–6 mm;</text>
      <biological_entity id="o11767" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="beige" name="coloration" src="d0_s12" to="light or dark-brown" />
        <character constraint="on apex" constraintid="o11768" is_modifier="false" modifier="occasionally" name="coloration" src="d0_s12" value="purple blotches" value_original="purple blotches" />
        <character char_type="range_value" from="less globose" name="shape" notes="" src="d0_s12" to="pyriform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11768" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pedicel erect to ascending.</text>
      <biological_entity id="o11769" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose to obconic, lacking obvious depression, 0.8–1.8 mm, usually granular.</text>
      <biological_entity id="o11770" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="obconic" />
      </biological_entity>
      <biological_entity id="o11771" name="depression" name_original="depression" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief_or_texture" src="d0_s14" value="granular" value_original="granular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems less than 1.5 mm wide.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 1.5 mm or more wide.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Outer tepals 18–20 mm; outer spathe at least 2 times inner; margins of stem and keels of spathes entire.</description>
      <determination>34a Sisyrinchium idahoense var. macounii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Outer tepals 10–17 mm; outer spathe less than 2 times inner; margins of stem and keels of spathes denticulate.</description>
      <determination>34b Sisyrinchium idahoense var. segetum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer tepals 8–13 mm; inner spathe 12–20 mm; stem 1.5–2 mm wide.</description>
      <determination>34c Sisyrinchium idahoense var. occidentale</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer tepals 13–17(–20) mm; inner spathe 20–30 mm; stem 2–2.5 mm wide.</description>
      <determination>34d Sisyrinchium idahoense var. idahoense</determination>
    </key_statement>
  </key>
</bio:treatment>