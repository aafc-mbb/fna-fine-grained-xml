<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">gladiolus</taxon_name>
    <taxon_name authority="Linneaus" date="1753" rank="species">communis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 36. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus gladiolus;species communis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220005610</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gladiolus</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">byzantinus</taxon_name>
    <taxon_hierarchy>genus Gladiolus;species byzantinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gladiolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">communis</taxon_name>
    <taxon_name authority="(Miller) A. P. Hamilton" date="unknown" rank="subspecies">byzantinus</taxon_name>
    <taxon_hierarchy>genus Gladiolus;species communis;subspecies byzantinus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–100 cm.</text>
      <biological_entity id="o26633" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms tunicate, ca. 20 mm diam.;</text>
      <biological_entity id="o26634" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character name="diameter" src="d0_s1" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tunic coriaceous, fragmenting into irregular pieces, rarely ultimately becoming fibrous.</text>
      <biological_entity id="o26635" name="tunic" name_original="tunic" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="rarely ultimately becoming" name="texture" notes="" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o26636" name="piece" name_original="pieces" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" value_original="irregular" />
      </biological_entity>
      <relation from="o26635" id="r3619" name="fragmenting into" negation="false" src="d0_s2" to="o26636" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple.</text>
      <biological_entity id="o26637" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves ± reaching base of spike;</text>
      <biological_entity id="o26638" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26639" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o26640" name="spike" name_original="spike" src="d0_s4" type="structure" />
      <relation from="o26638" id="r3620" name="more or less reaching" negation="false" src="d0_s4" to="o26639" />
      <relation from="o26638" id="r3621" name="part_of" negation="false" src="d0_s4" to="o26640" />
    </statement>
    <statement id="d0_s5">
      <text>blade plane, lanceolate, sometimes narrowly so, 5–22 mm wide.</text>
      <biological_entity id="o26641" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="sometimes narrowly; narrowly" name="width" src="d0_s5" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes 10–20-flowered;</text>
      <biological_entity id="o26642" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-20-flowered" value_original="10-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spathes unequal, outer 25–35 (–50) mm, inner ± 2/3 outer.</text>
      <biological_entity id="o26643" name="spathe" name_original="spathes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="position" src="d0_s7" value="outer" value_original="outer" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s7" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s7" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="position" src="d0_s7" value="outer" value_original="outer" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers unscented, weakly distichous;</text>
      <biological_entity id="o26644" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="odor" src="d0_s8" value="unscented" value_original="unscented" />
        <character is_modifier="false" modifier="weakly" name="arrangement" src="d0_s8" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth-tube obliquely funnel-shaped, 10–12 mm;</text>
      <biological_entity id="o26645" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s9" value="funnel--shaped" value_original="funnel--shaped" />
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals reddish purple with narrow median white streak on outer 3 tepals, unequal, dorsal tepal 30–40 × 14–19 mm, inner lateral tepals joined to outer tepals for ca. 3 mm, 28–35 mm, outer 3 tepals connate for ca. 2 mm, outer lateral tepals 18–25 mm, outer median tepal 24–28 mm;</text>
      <biological_entity id="o26646" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="with tepals" constraintid="o26647" is_modifier="false" name="coloration" src="d0_s10" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o26647" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="position" src="d0_s10" value="median" value_original="median" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="white streak" value_original="white streak" />
        <character is_modifier="true" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o26648" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s10" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner lateral" id="o26649" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="joined" value_original="joined" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26650" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="28" from_unit="mm" modifier="for 3 mm" name="some_measurement" src="d0_s10" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26651" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o26652" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="for outer lateral tepals" constraintid="o26653" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="outer lateral" id="o26653" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer median" id="o26654" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s10" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 12–15 mm;</text>
      <biological_entity id="o26655" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 10–13 mm;</text>
      <biological_entity id="o26656" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style branching opposite distal 1/3 of anthers;</text>
      <biological_entity id="o26657" name="style" name_original="style" src="d0_s13" type="structure">
        <character constraint="opposite distal 1/3" constraintid="o26658" is_modifier="false" name="architecture" src="d0_s13" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26658" name="1/3" name_original="1/3" src="d0_s13" type="structure" />
      <biological_entity id="o26659" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o26658" id="r3622" name="part_of" negation="false" src="d0_s13" to="o26659" />
    </statement>
    <statement id="d0_s14">
      <text>branches ca. 2 mm.</text>
      <biological_entity id="o26660" name="branch" name_original="branches" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong, 18–24 mm.</text>
      <biological_entity id="o26661" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s15" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds broadly winged, 4–6 mm diam.</text>
      <biological_entity id="o26662" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, abandoned gardens, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="abandoned gardens" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Ga., Ky., La., Miss., N.C., S.C., Tenn., Tex.; sw Europe, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">False corn-flag</other_name>
  <other_name type="common_name">Turkish corn-flag</other_name>
  <discussion>Gladiolus communis is a garden escape. In regional floras it is sometimes confused with the southern African G. papilio Hooker; the resemblance is entirely superficial. Plants of G. communis found in North America have traditionally been treated as G. byzantinus, which differs little from G. communis except in degree of robustness. Distinction at even subspecific rank does not seem warranted.</discussion>
  
</bio:treatment>