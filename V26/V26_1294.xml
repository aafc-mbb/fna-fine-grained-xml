<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">628</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Malaxideae</taxon_name>
    <taxon_name authority="Solander ex Swartz" date="1788" rank="genus">malaxis</taxon_name>
    <taxon_name authority="(Ridley) Kuntze" date="1891" rank="species">porphyrea</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>2: 673. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe malaxideae;genus malaxis;species porphyrea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101765</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Microstylis</taxon_name>
    <taxon_name authority="Ridley" date="unknown" rank="species">porphyrea</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>24: 320. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Microstylis;species porphyrea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Microstylis</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_hierarchy>genus Microstylis;species purpurea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 16–45 cm.</text>
      <biological_entity id="o27079" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pseudobulbs 5–10 mm diam.</text>
      <biological_entity id="o27080" name="pseudobulb" name_original="pseudobulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1, within proximal 1/3 of stem;</text>
      <biological_entity id="o27081" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27082" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o27083" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o27081" id="r3679" name="within" negation="false" src="d0_s2" to="o27082" />
      <relation from="o27082" id="r3680" name="part_of" negation="false" src="d0_s2" to="o27083" />
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate-ovate or ovatelanceolate, (3–) 4.1–8.5 (–10) × 0.2–0.45 (–0.65) cm, apex obtuse to acute.</text>
      <biological_entity id="o27084" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate-ovate" value_original="orbiculate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s3" to="4.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="0.45" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.65" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.45" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27085" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemes, 6–25 cm;</text>
      <biological_entity constraint="inflorescences" id="o27086" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis slightly angled;</text>
      <biological_entity id="o27087" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bracts triangular to lanceolate, 1–2 × 0.5 mm, apex acute;</text>
      <biological_entity constraint="floral" id="o27088" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o27089" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels not crowded, 2.5–5 mm.</text>
      <biological_entity id="o27090" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 16–133, resupinate, deep maroon or greenish maroon;</text>
      <biological_entity id="o27091" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="133" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish maroon" value_original="greenish maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals glabrous, not papillose;</text>
      <biological_entity id="o27092" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dorsal sepal lanceolate, 1.8–2.4 × 0.8–1.2 mm, margins revolute, apex acute;</text>
      <biological_entity constraint="dorsal" id="o27093" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s10" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27094" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s10" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o27095" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lateral sepals lanceolate-elliptic, falcate, 1.8–2.4 × 0.8–1.2 mm, apex acute;</text>
      <biological_entity constraint="lateral" id="o27096" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s11" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27097" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals strongly recurved, linear to filiform, slightly falcate, 1.8–2.2 × 0.2–0.4 mm, apex obtuse;</text>
      <biological_entity id="o27098" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="filiform" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27099" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lip triangular to triangular-lanceolate, 1.8–2.3 × 1.3–1.8 mm, base with auricles narrow and nearly parallel, apex broadly acuminate;</text>
      <biological_entity id="o27100" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s13" to="triangular-lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s13" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27101" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s13" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o27102" name="auricle" name_original="auricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o27103" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o27101" id="r3681" name="with" negation="false" src="d0_s13" to="o27102" />
    </statement>
    <statement id="d0_s14">
      <text>column 0.5–0.8 × 0.5–0.8 mm;</text>
      <biological_entity id="o27104" name="column" name_original="column" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s14" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollinia yellow.</text>
      <biological_entity id="o27105" name="pollinium" name_original="pollinia" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ascending, ellipsoid, 5 × 3 mm.</text>
      <biological_entity id="o27106" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s16" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s16" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous and mixed forests on dry slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" modifier="open" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="dry slopes" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Both Malaxis porphyrea and M. wendtii have been included incorrectly in M. ehrenbergii (D. S. Correll 1950; C. A. Luer 1975). The confusion among the taxa is discussed by T. K. Todsen (1997). Malaxis ehrenbergii is a Mexican species with its northernmost limit in southern Hidalgo, about 1200 km to the south of the ranges of the other two taxa, the southernmost ranges of which are in northern Chihuahua and Sonora. The following criteria are used to separate M. wendtii from M. porphyrea: the sepals of M. wendtii are papillose while the sepals of M. porphyrea are essentially glabrous, although they have prominent cell structures that may appear to be very small papillae; the lip of M. wendtii is linear to linear-lanceolate with a narrowly acuminate apex, and the auricles at the base of the lip are broad and diverging, while the lip of M. porphyrea is nearly triangular to triangular-lanceolate with a broadly acuminate apex, and the auricles at the base of the lip are narrow and nearly parallel. Also, M. wendtii has a denser inflorescence and the flowers are not quite as intensely colored as in M. porphyrea (R. A. Coleman, W. Jennings, pers. comm.).</discussion>
  
</bio:treatment>