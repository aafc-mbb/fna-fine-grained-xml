<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Boissier" date="1844" rank="genus">chionodoxa</taxon_name>
    <taxon_name authority="Boissier" date="1844" rank="species">luciliae</taxon_name>
    <place_of_publication>
      <publication_title>Diagn. Pl. Orient.</publication_title>
      <place_in_publication>1(5): 61. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus chionodoxa;species luciliae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220002773</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scilla</taxon_name>
    <taxon_name authority="(Boissier) Speta" date="unknown" rank="species">luciliae</taxon_name>
    <taxon_hierarchy>genus Scilla;species luciliae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 cm;</text>
      <biological_entity id="o28126" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulbs ovoid, 1.5–2 cm.</text>
      <biological_entity id="o28127" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–4, often recurved, broadly linear, 7–20 × 1–2 cm.</text>
      <biological_entity id="o28128" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scapes usually solitary.</text>
      <biological_entity id="o28129" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–2-flowered.</text>
      <biological_entity id="o28130" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: perianth deep blue with white central zone, tube 2.5–4 mm;</text>
      <biological_entity id="o28131" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o28132" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="depth" src="d0_s5" value="deep" value_original="deep" />
        <character constraint="with central zone" constraintid="o28133" is_modifier="false" name="coloration" src="d0_s5" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity constraint="central" id="o28133" name="zone" name_original="zone" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o28134" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 12–15 (–20) mm;</text>
      <biological_entity id="o28135" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28136" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments white;</text>
      <biological_entity id="o28137" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28138" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel erect, equaling or shorter than perianth.</text>
      <biological_entity id="o28139" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28140" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than perianth" constraintid="o28141" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o28141" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules ± globose, 4–6 mm.</text>
      <biological_entity id="o28142" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds with white appendages.</text>
      <biological_entity id="o28144" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <relation from="o28143" id="r3806" name="with" negation="false" src="d0_s10" to="o28144" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o28143" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o28145" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early to mid spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lawns, open mesic areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lawns" />
        <character name="habitat" value="open mesic areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[1600–2000 m in Turkey]</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1600" from_unit="m" constraint=" in Turkey]" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mass., Mich., Utah; Europe (w Turkey).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Europe (w Turkey)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Chinodoxa luciliae, a spring-blooming, garden species, has been reported established on lawns in Houghton and Borrago counties, Michigan (E. G. Voss 1972–1985, vol. 1). Because C. forbesii Baker, distinguished by its taller scapes (to 30 cm), with more numerous (4–10), slightly larger, and slightly pendent flowers, is more widely cultivated, often as “C. luciliae,” both it and C. luciliae are expected to be naturalized elsewhere.</discussion>
  
</bio:treatment>