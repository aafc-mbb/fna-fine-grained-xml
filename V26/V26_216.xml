<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">gunnisonii</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>348. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species gunnisonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101470</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually bulbose;</text>
      <biological_entity id="o5861" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb coat, when present, membranous.</text>
      <biological_entity constraint="bulb" id="o5862" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when present" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not branching, straight, 2.4–5.5 dm.</text>
      <biological_entity id="o5863" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character char_type="range_value" from="2.4" from_unit="dm" name="some_measurement" src="d0_s2" to="5.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering, 18–35 cm;</text>
      <biological_entity id="o5864" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o5865" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s3" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear.</text>
      <biological_entity id="o5866" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5867" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences subumbellate, 1–3-flowered.</text>
      <biological_entity id="o5868" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o5869" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o5870" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals marked similar to petals, lanceolate, usually much shorter, glabrous, apex acute;</text>
      <biological_entity id="o5871" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually much" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5872" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity id="o5873" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o5871" id="r847" name="marked similar" negation="false" src="d0_s8" to="o5872" />
    </statement>
    <statement id="d0_s9">
      <text>petals white to purple, greenish adaxially, clawed, often with narrow, transverse purple band distal to gland and purple blotch on claw, obovate, cuneate, usually obtuse and rounded distally;</text>
      <biological_entity id="o5874" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o5875" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="transverse" value_original="transverse" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character constraint="to gland" constraintid="o5876" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o5876" name="gland" name_original="gland" src="d0_s9" type="structure" />
      <biological_entity id="o5877" name="blotch" name_original="blotch" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5878" name="claw" name_original="claw" src="d0_s9" type="structure" />
      <relation from="o5874" id="r848" modifier="often" name="with" negation="false" src="d0_s9" to="o5875" />
      <relation from="o5877" id="r849" name="on" negation="false" src="d0_s9" to="o5878" />
    </statement>
    <statement id="d0_s10">
      <text>glands transversely oblong, not depressed, densely bearded with distally branching hairs, outermost of which somewhat connate at base to form discontinuous, deeply fringed membranes;</text>
      <biological_entity id="o5879" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="depressed" value_original="depressed" />
        <character constraint="with hairs" constraintid="o5880" is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" notes="" src="d0_s10" value="outermost" value_original="outermost" />
        <character constraint="at base" constraintid="o5881" is_modifier="false" modifier="somewhat" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5880" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="distally" name="architecture" src="d0_s10" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o5881" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="discontinuous" value_original="discontinuous" />
      </biological_entity>
      <biological_entity id="o5882" name="membrane" name_original="membranes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="deeply" name="shape" src="d0_s10" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments shorter than anthers;</text>
      <biological_entity id="o5883" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character constraint="than anthers" constraintid="o5884" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5884" name="anther" name_original="anthers" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers lanceolate, apex acute to apiculate.</text>
      <biological_entity id="o5885" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o5886" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules erect, linear-oblong, 3-angled, 3–6 cm, apex acute.</text>
      <biological_entity id="o5887" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s13" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5888" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds flat, inflated.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o5889" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5890" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>wc North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="wc North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="past_name">gunnisoni</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals white to purple, occasionally yellow; w United States.</description>
      <determination>53a Calochortus gunnisonii var. gunnisonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals pale yellow; New Mexico only.</description>
      <determination>53b Calochortus gunnisonii var. perpulcher</determination>
    </key_statement>
  </key>
</bio:treatment>