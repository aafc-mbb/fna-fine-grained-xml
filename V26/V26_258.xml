<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="treatment_page">159</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erythronium</taxon_name>
    <taxon_name authority="S. Watson" date="1877" rank="species">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 277. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus erythronium;species purpurascens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101602</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs slender, 25–40 mm.</text>
      <biological_entity id="o27688" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s0" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 6–15 cm;</text>
      <biological_entity id="o27689" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade green, lanceolate to narrowly ovate, margins ± wavy.</text>
      <biological_entity id="o27690" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="narrowly ovate" />
      </biological_entity>
      <biological_entity id="o27691" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape 7–20 cm.</text>
      <biological_entity id="o27692" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–6-flowered.</text>
      <biological_entity id="o27693" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-6-flowered" value_original="1-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals white, bright-yellow on proximal 1/3, pinkish purple in age, lanceolate, 10–20 mm, not auriculate at base;</text>
      <biological_entity id="o27694" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o27695" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character constraint="on proximal 1/3" constraintid="o27696" is_modifier="false" name="coloration" src="d0_s5" value="bright-yellow" value_original="bright-yellow" />
        <character constraint="in age" constraintid="o27697" is_modifier="false" name="coloration" notes="" src="d0_s5" value="pinkish purple" value_original="pinkish purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character constraint="at base" constraintid="o27698" is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27696" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o27697" name="age" name_original="age" src="d0_s5" type="structure" />
      <biological_entity id="o27698" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stamens 8–12 mm;</text>
      <biological_entity id="o27699" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o27700" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments yellow, slender;</text>
      <biological_entity id="o27701" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o27702" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers cream to yellow;</text>
      <biological_entity id="o27703" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o27704" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s8" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style yellow, 4–5 mm;</text>
      <biological_entity id="o27705" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27706" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma ± unlobed.</text>
      <biological_entity id="o27707" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o27708" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules obovoid, 2–4 cm.</text>
      <biological_entity id="o27709" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer, soon after snowmelt (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous forests, meadows, rocky places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open coniferous forests" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="rocky places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Sierra Nevada fawn- lily</other_name>
  
</bio:treatment>