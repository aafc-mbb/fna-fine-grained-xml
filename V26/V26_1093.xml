<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="Rafinesque" date="1833" rank="species">tuberosa</taxon_name>
    <place_of_publication>
      <publication_title>Herb. Raf.,</publication_title>
      <place_in_publication>45. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species tuberosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101967</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="Ames" date="unknown" rank="species">grayi</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species grayi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">simplex</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species simplex;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tuberosa</taxon_name>
    <taxon_name authority="(Ames) Fernald" date="unknown" rank="variety">grayi</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species tuberosa;variety grayi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–30 cm.</text>
      <biological_entity id="o29107" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots solitary, vertical, tuberous, turbinate, mostly to 1 cm diam.</text>
      <biological_entity id="o29108" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="shape" src="d0_s1" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="mostly" name="diameter" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves fugacious, 3–5, basal, spreading, oval-oblanceolate, 2–6 × 1–2 cm.</text>
      <biological_entity id="o29109" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="fugacious" value_original="fugacious" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <biological_entity constraint="basal" id="o29110" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oval-oblanceolate" value_original="oval-oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes loosely spiraled, 4–7 flowers per cycle of spiral;</text>
      <biological_entity id="o29111" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o29112" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o29113" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
      <relation from="o29112" id="r3928" modifier="of spiral" name="per" negation="false" src="d0_s3" to="o29113" />
    </statement>
    <statement id="d0_s4">
      <text>rachis glabrous.</text>
      <biological_entity id="o29114" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers pure white, gaping from near middle, tubular portion less than 3 mm;</text>
      <biological_entity id="o29115" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character constraint="from middle flowers" constraintid="o29116" is_modifier="false" name="architecture" src="d0_s5" value="gaping" value_original="gaping" />
      </biological_entity>
      <biological_entity constraint="middle" id="o29116" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o29117" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals distinct to base, 5 × 1 mm;</text>
      <biological_entity id="o29118" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="to base" constraintid="o29119" is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character name="length" notes="" src="d0_s6" unit="mm" value="5" value_original="5" />
        <character name="width" notes="" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29119" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals slightly spreading;</text>
      <biological_entity constraint="lateral" id="o29120" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals linear to lance-oblong, 5 × 1 mm, apex acute to obtuse;</text>
      <biological_entity id="o29121" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lance-oblong" />
        <character name="length" src="d0_s8" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29122" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip 5 × 2.5 mm, ovate to oblong, apex dilated with broad crisped, finely lacerate margin;</text>
      <biological_entity id="o29123" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character name="length" src="d0_s9" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s9" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="oblong" />
      </biological_entity>
      <biological_entity id="o29124" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character constraint="with margin" constraintid="o29125" is_modifier="false" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o29125" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s9" value="crisped" value_original="crisped" />
        <character is_modifier="true" modifier="finely" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>veins several, branches very short;</text>
      <biological_entity id="o29126" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o29127" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>basal calli long-pointed, mostly to 1 mm;</text>
      <biological_entity constraint="basal" id="o29128" name="callus" name_original="calli" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="long-pointed" value_original="long-pointed" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidium linearlanceolate;</text>
      <biological_entity id="o29129" name="viscidium" name_original="viscidium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary mostly 3 mm.</text>
      <biological_entity id="o29130" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds monoembryonic.</text>
      <biological_entity id="o29131" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monoembryonic" value_original="monoembryonic" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to open woods, outcrops, old fields, roadsides, cemeteries</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to open woods" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="cemeteries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Mass., Mich., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>The nomenclatural history of Spiranthes tuberosa is rather complex, and among the names applied to it are Spiranthes beckii Lindley and Ibidium beckii (Lindley) House. See D. S. Correll (1950) for a discussion.</discussion>
  <discussion>This species is easily recognized by its pure white flowers, broad crisped lip, and fugacious leaves.</discussion>
  
</bio:treatment>