<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1899" rank="species">fuscatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 225. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species fuscatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101904</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">arenicola</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species arenicola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">farwellii</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species farwellii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">incrustatum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species incrustatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">rufipes</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species rufipes;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">tenellum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species tenellum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, yellowish green to olive green when dry, to 5.3 dm, not glaucous.</text>
      <biological_entity id="o34487" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="yellowish green" modifier="when dry" name="coloration" src="d0_s0" to="olive green" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="5.3" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, with 1 node, 1–1.7 mm wide, glabrous, margins entire, similar in color and texture to stem body;</text>
      <biological_entity id="o34488" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s1" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34489" name="node" name_original="node" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o34490" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="stem" id="o34491" name="body" name_original="body" src="d0_s1" type="structure" />
      <relation from="o34488" id="r4639" name="with" negation="false" src="d0_s1" to="o34489" />
      <relation from="o34490" id="r4640" name="to" negation="false" src="d0_s1" to="o34491" />
    </statement>
    <statement id="d0_s2">
      <text>internode (20–) 23–47 cm, longer than leaves, with 1–5 branches.</text>
      <biological_entity id="o34492" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="23" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="23" from_unit="cm" name="some_measurement" src="d0_s2" to="47" to_unit="cm" />
        <character constraint="than leaves" constraintid="o34493" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o34493" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o34494" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <relation from="o34492" id="r4641" name="with" negation="false" src="d0_s2" to="o34494" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades glabrous, bases becoming fibrous, persistent in tufts.</text>
      <biological_entity id="o34495" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34496" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character constraint="in tufts" constraintid="o34497" is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o34497" name="tuft" name_original="tufts" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o34498" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes green, obviously wider than supporting branch, glabrous, keels entire;</text>
      <biological_entity id="o34499" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character constraint="than supporting branch" constraintid="o34500" is_modifier="false" name="width" src="d0_s5" value="obviously wider" value_original="obviously wider" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34500" name="branch" name_original="branch" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s5" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity id="o34501" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer 12–18 mm, 0.9 mm shorter to 1.7 mm longer than inner, tapering evenly towards apex, margins basally connate 2.1–4 mm;</text>
      <biological_entity constraint="outer" id="o34502" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.9" value_original="0.9" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1.7" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o34503" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o34504" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o34503" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o34504" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o34505" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner with keel straight, hyaline margins 0.2–0.4 mm wide, apex acute to obtuse, ending at or to 0.5 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o34506" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o34507" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o34508" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34509" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
      <biological_entity id="o34510" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
        <character is_modifier="true" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o34506" id="r4642" name="with" negation="false" src="d0_s7" to="o34507" />
      <relation from="o34509" id="r4643" name="ending at" negation="false" src="d0_s7" to="o34510" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals blue to bluish violet, bases yellow;</text>
      <biological_entity id="o34511" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34512" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="bluish violet" />
      </biological_entity>
      <biological_entity id="o34513" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals 7.9–11.6 mm, apex emarginate, aristate;</text>
      <biological_entity id="o34514" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o34515" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="7.9" from_unit="mm" name="some_measurement" src="d0_s9" to="11.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34516" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o34517" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34518" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o34519" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34520" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o34521" name="foliage" name_original="foliage" src="d0_s11" type="structure" />
      <relation from="o34520" id="r4644" name="to" negation="false" src="d0_s11" to="o34521" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules light to medium brown, ± globose, 2.9–4.3 mm;</text>
      <biological_entity id="o34522" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s12" to="medium brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicel erect to ascending.</text>
      <biological_entity id="o34523" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose to obconic, lacking obvious depression, 1–1.5 mm, rugulose.</text>
      <biological_entity id="o34524" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 32.</text>
      <biological_entity id="o34525" name="depression" name_original="depression" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34526" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy areas, moist pine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy areas" />
        <character name="habitat" value="moist pine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S.; Ala., Conn., Del., Fla., Ga., Md., Mass., Miss., N.J., N.Y., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  
</bio:treatment>