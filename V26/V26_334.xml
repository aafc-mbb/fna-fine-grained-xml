<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Salisbury ex Reichenbach" date="unknown" rank="genus">lloydia</taxon_name>
    <taxon_name authority="(Linnaeus) Salisbury ex Reichenbach" date="1830" rank="species">serotina</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Germ. Excurs.,</publication_title>
      <place_in_publication>102. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lloydia;species serotina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200027764</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bulbocodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">serotinum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 294. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bulbocodium;species serotinum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, 0.5–1.5 (–1.9) dm.</text>
      <biological_entity id="o20788" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1.9" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves slightly dimorphic;</text>
      <biological_entity id="o20789" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal 2, blade linear, 4–10 (–20) cm × 0.8–1 mm;</text>
      <biological_entity constraint="basal" id="o20790" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20791" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–4, blade lanceolate, 1–5 × 0.1–0.2 cm.</text>
      <biological_entity constraint="cauline" id="o20792" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o20793" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 1–2;</text>
      <biological_entity id="o20794" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tepals 0.9–1.2 (–1.4) cm;</text>
      <biological_entity id="o20795" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s5" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens erect;</text>
      <biological_entity id="o20796" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 3–8 mm;</text>
      <biological_entity id="o20797" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.6–1 (–1.2) mm.</text>
      <biological_entity id="o20798" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 6–8 mm.</text>
      <biological_entity id="o20799" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth white with greenish to purplish veins.</description>
      <determination>1a Lloydia serotina var. serotina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth yellow with yellowish to greenish veins.</description>
      <determination>1b Lloydia serotina var. flava</determination>
    </key_statement>
  </key>
</bio:treatment>