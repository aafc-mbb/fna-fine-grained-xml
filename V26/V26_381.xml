<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">asphodelus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">fistulosus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 309. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus asphodelus;species fistulosus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101425</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annuals or short-lived perennials;</text>
      <biological_entity constraint="plants" id="o18088" name="annual" name_original="annuals" src="d0_s0" type="structure" />
      <biological_entity constraint="plants" id="o18089" name="perennial" name_original="perennials" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>root crowns thickened with many fibrous-roots.</text>
      <biological_entity constraint="root" id="o18090" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character constraint="with fibrous-roots" constraintid="o18091" is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o18091" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched or unbranched, 20–70 cm.</text>
      <biological_entity id="o18092" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 5–35 cm × 2–4 mm;</text>
      <biological_entity id="o18093" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade cylindrical or slightly flattened adaxially, hollow, glabrous except on margins.</text>
      <biological_entity id="o18094" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="slightly; adaxially" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18095" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <relation from="o18094" id="r2475" name="except on" negation="false" src="d0_s4" to="o18095" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences simple racemes to open panicles with ascending branches, 15–70 cm;</text>
      <biological_entity id="o18096" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o18097" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18098" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o18099" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o18097" id="r2476" name="to" negation="false" src="d0_s5" to="o18098" />
      <relation from="o18098" id="r2477" name="with" negation="false" src="d0_s5" to="o18099" />
    </statement>
    <statement id="d0_s6">
      <text>bracts white, 4–7 mm.</text>
      <biological_entity id="o18100" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers diurnal, closing in evening and during cloudy weather;</text>
      <biological_entity id="o18101" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="diurnal" value_original="diurnal" />
        <character constraint="in evening and during cloudy weather" is_modifier="false" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals white to pale-pink, oblong, obtuse, 5–12 × 2–3 mm, vein dark-pink or brown;</text>
      <biological_entity id="o18102" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pale-pink" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18103" name="vein" name_original="vein" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-pink" value_original="dark-pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>longest stamens equaling style.</text>
      <biological_entity constraint="longest" id="o18104" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o18105" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 5–7 mm, transversely wrinkled.</text>
      <biological_entity id="o18106" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 3–4 mm, rugose.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28, 56.</text>
      <biological_entity id="o18107" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18108" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., N.Mex., Tex.; sw Europe; sw Asia; n Africa; widely naturalized (Australia, Mexico, New Zealand); expected elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="widely naturalized (Australia)" establishment_means="native" />
        <character name="distribution" value="widely naturalized (Mexico)" establishment_means="native" />
        <character name="distribution" value="widely naturalized (New Zealand)" establishment_means="native" />
        <character name="distribution" value="expected elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Onion-weed</other_name>
  <discussion>Asphodelus fistulosus has become a noxious weed in California and in other places with Mediterranean climates worldwide.</discussion>
  
</bio:treatment>