<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Californicae</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1829" rank="species">tenax</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>15: plate 1218. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series californicae;species tenax;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101717</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="species">gormanii</taxon_name>
    <taxon_hierarchy>genus Iris;species gormanii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenax</taxon_name>
    <taxon_name authority="(Piper) R. C. Foster" date="unknown" rank="variety">gormanii</taxon_name>
    <taxon_hierarchy>genus Iris;species tenax;variety gormanii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenax</taxon_name>
    <taxon_name authority="L. W. Lenz" date="unknown" rank="subspecies">klamathensis</taxon_name>
    <taxon_hierarchy>genus Iris;species tenax;subspecies klamathensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes many-branched, not creeping, forming dense clumps, slender, 0.3–0.8 cm diam.;</text>
      <biological_entity id="o24209" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="many-branched" value_original="many-branched" />
        <character is_modifier="false" modifier="not" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="diameter" src="d0_s0" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24210" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o24209" id="r3290" name="forming" negation="false" src="d0_s0" to="o24210" />
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous.</text>
      <biological_entity id="o24211" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, somewhat angular, solid, 1.5–2.7 dm.</text>
      <biological_entity id="o24212" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_shape" src="d0_s2" value="angular" value_original="angular" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="2.7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal somewhat lax, overtopping stem, blade light green, paling to pink or straw color basally, finely ribbed, linear-acute to linear, ensiform, 4.5 dm × 0.5 cm, margins not thickened;</text>
      <biological_entity id="o24213" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o24214" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o24215" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o24216" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character char_type="range_value" from="paling" name="coloration" src="d0_s3" to="pink" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s3" value="straw color" value_original="straw color" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s3" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="linear-acute" name="shape" src="d0_s3" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ensiform" value_original="ensiform" />
        <character name="length" src="d0_s3" unit="dm" value="4.5" value_original="4.5" />
        <character name="width" src="d0_s3" unit="cm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o24217" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o24214" id="r3291" name="overtopping" negation="false" src="d0_s3" to="o24215" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 1–3, sheathing for 1/2 length then spreading, foliaceous, blade linearlanceolate, not inflated, narrow, reduced, to 15 cm.</text>
      <biological_entity id="o24218" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o24219" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
        <character constraint="for 1/2 length then spreading" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o24220" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence units 1–2-flowered;</text>
      <biological_entity constraint="inflorescence" id="o24221" name="unit" name_original="units" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes distant basally by 3 cm in some cases, keeled, linearlanceolate to lanceolate-acuminate, 5–7 cm × 2–4 mm, unequal, outer longer than inner, herbaceous, margins scarious.</text>
      <biological_entity id="o24222" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="distant" value_original="distant" />
        <character constraint="by 3 cm" is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="position" src="d0_s6" value="outer" value_original="outer" />
        <character constraint="than inner" constraintid="o24223" is_modifier="false" name="length_or_size" src="d0_s6" value="outer longer" value_original="outer longer" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o24223" name="inner" name_original="inner" src="d0_s6" type="structure" />
      <biological_entity id="o24224" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth color variable, purple, pink, lavender, cream, yellow, or rarely white;</text>
      <biological_entity id="o24225" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24226" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="variable" value_original="variable" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube funnelform, 0.6–2 cm;</text>
      <biological_entity id="o24227" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24228" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals veined with color of limb, obovate, 5.8–6 × 1.6–2 cm, base gradually attenuate into white claw with slight yellow ridge, apex emarginate to bluntly rounded;</text>
      <biological_entity id="o24229" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24230" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="with " constraintid="o24231" is_modifier="false" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5.8" from_unit="cm" name="length" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24231" name="limb" name_original="limb" src="d0_s9" type="structure" />
      <biological_entity id="o24232" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="into claw" constraintid="o24233" is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o24233" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o24234" name="ridge" name_original="ridge" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="slight" value_original="slight" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o24235" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="emarginate" name="shape" src="d0_s9" to="bluntly rounded" />
      </biological_entity>
      <relation from="o24233" id="r3292" name="with" negation="false" src="d0_s9" to="o24234" />
    </statement>
    <statement id="d0_s10">
      <text>petals same color as sepals, not prominently veined, lanceolate to oblanceolate, 5 × 1 cm;</text>
      <biological_entity id="o24236" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24237" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not prominently" name="coloration" notes="" src="d0_s10" value="veined" value_original="veined" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblanceolate" />
        <character name="length" src="d0_s10" unit="cm" value="5" value_original="5" />
        <character name="width" src="d0_s10" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24238" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <relation from="o24237" id="r3293" name="as" negation="false" src="d0_s10" to="o24238" />
    </statement>
    <statement id="d0_s11">
      <text>ovary 1–2 cm, slightly wider distally, base very gradually attenuate;</text>
      <biological_entity id="o24239" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24240" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="slightly; distally" name="width" src="d0_s11" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o24241" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very gradually" name="shape" src="d0_s11" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 2.2–3.2 cm, crests subquadrate, 8–12 mm, margins crenate or incised;</text>
      <biological_entity id="o24242" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o24243" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.2" from_unit="cm" name="some_measurement" src="d0_s12" to="3.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24244" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24245" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas triangular, margins entire;</text>
      <biological_entity id="o24246" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24247" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o24248" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 1–5 cm, longer in second flower (when present) than in first.</text>
      <biological_entity id="o24249" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o24250" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="5" to_unit="cm" />
        <character constraint="in flower" constraintid="o24251" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o24251" name="flower" name_original="flower" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong, triangular in cross-section, prominently ribbed, beaked, 3–5 cm.</text>
      <biological_entity id="o24252" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character constraint="in cross-section" constraintid="o24253" is_modifier="false" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" notes="" src="d0_s15" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s15" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24253" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, D-shaped to irregular, wrinkled.</text>
      <biological_entity id="o24254" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="d--shaped" value_original="d--shaped" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s16" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry soils in fields and open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry soils" constraint="in fields and open woods" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Tough-leaf iris</other_name>
  <other_name type="common_name">Oregon flag</other_name>
  <discussion>Plants described as subsp. klamathensis differ from others of the species in having floral tubes 11–20 mm instead of 6–10 mm, style crests somewhat longer and narrower, and flower color and markings more like those of Iris bracteata or I. innominata. The latter species has an even longer floral tube, 15–30 mm, which could be evidence of introgression. Hybrids are known to occur in the area common to both species, in Douglas County, Oregon.</discussion>
  <discussion>Iris tenax hybridizes with I. bracteata, I. chrysophylla, I. douglasiana, I. hartwegii, I. innominata, I. macrosiphon, I. purdyi, and I. tenuissima.</discussion>
  
</bio:treatment>