<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="treatment_page">629</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Malaxideae</taxon_name>
    <taxon_name authority="Solander ex Swartz" date="1788" rank="genus">malaxis</taxon_name>
    <taxon_name authority="(Linnaeus) Swartz" date="1800" rank="species">monophyllos</taxon_name>
    <taxon_name authority="(A. Gray) F. Morris &amp; E. A. Eames" date="1929" rank="variety">brachypoda</taxon_name>
    <place_of_publication>
      <publication_title>Our Wild Orchids,</publication_title>
      <place_in_publication>358. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe malaxideae;genus malaxis;species monophyllos;variety brachypoda;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102273</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Microstylis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">brachypoda</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>3: 228. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Microstylis;species brachypoda;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malaxis</taxon_name>
    <taxon_name authority="(A. Gray) Fernald" date="unknown" rank="species">brachypoda</taxon_name>
    <taxon_hierarchy>genus Malaxis;species brachypoda;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–30 cm.</text>
      <biological_entity id="o8488" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pseudobulbs 4–8 mm diam.</text>
      <biological_entity id="o8489" name="pseudobulb" name_original="pseudobulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1 (–2, rarely), petiolate base sheathing stem;</text>
      <biological_entity id="o8490" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8491" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o8492" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light green, ovate-elliptic, keeled abaxially, 1.5–9.5 × 1–5 cm, apex acute.</text>
      <biological_entity id="o8493" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8494" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: floral bracts lanceolate, 1.5–2 mm;</text>
      <biological_entity id="o8495" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="floral" id="o8496" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicels 2–4.5 mm.</text>
      <biological_entity id="o8497" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o8498" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers resupinate, green or greenish white;</text>
      <biological_entity id="o8499" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>dorsal sepal ovatelanceolate, 1.5–2.5 × 1–1.4 mm, margins revolute, apex acuminate;</text>
      <biological_entity constraint="dorsal" id="o8500" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s7" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8501" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s7" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o8502" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals oblong-lanceolate, slightly falcate, 1.5–2.5 × 0.5–1.2 mm, apex acuminate;</text>
      <biological_entity constraint="lateral" id="o8503" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8504" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals strongly reflexed, filiform to narrowly linearlanceolate, 1.4–2.5 × 0.3–0.4 (–0.5) mm, apex rounded;</text>
      <biological_entity id="o8505" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character constraint="to apex" constraintid="o8506" is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o8506" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" is_modifier="true" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s9" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" is_modifier="true" name="width" src="d0_s9" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip broadly triangular, concave, 3-lobed, middle lobe ovate, apex acuminate, lateral lobes auriculate, thickened, curved upward;</text>
      <biological_entity id="o8507" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o8508" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8509" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8510" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>disc with 2 low, thickened, elongate calli, 1.3–2.2 × 1.2–1.8 (–2) mm;</text>
      <biological_entity id="o8511" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" notes="" src="d0_s11" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" notes="" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8512" name="callus" name_original="calli" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="position" src="d0_s11" value="low" value_original="low" />
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o8511" id="r1211" name="with" negation="false" src="d0_s11" to="o8512" />
    </statement>
    <statement id="d0_s12">
      <text>column 0.4–0.6 × 0.4–0.6 mm. 2n = 28.</text>
      <biological_entity id="o8513" name="column" name_original="column" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s12" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8514" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Colo., Ill., Ind., Maine, Mass., Minn., N.H., N.Y., Pa., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <other_name type="common_name">North American white adder’s-mouth</other_name>
  <other_name type="common_name">malaxis à pédicelles courts</other_name>
  
</bio:treatment>