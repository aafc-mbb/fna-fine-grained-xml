<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">69</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Willdenow" date="1808" rank="genus">CHAMAELIRIUM</taxon_name>
    <place_of_publication>
      <publication_title>Ges. Naturf. Freunde Berlin Mag. Neuesten Entdeck. Gesammten Naturk.</publication_title>
      <place_in_publication>2: 18. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus CHAMAELIRIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chamae, on the ground, and lirion, white lily</other_info_on_name>
    <other_info_on_name type="fna_id">106446</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from stout, nodose rhizomes;</text>
      <biological_entity id="o1995" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o1996" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="true" name="shape" src="d0_s0" value="nodose" value_original="nodose" />
      </biological_entity>
      <relation from="o1995" id="r264" name="from" negation="false" src="d0_s0" to="o1996" />
    </statement>
    <statement id="d0_s1">
      <text>roots contractile, fibrous, fleshy.</text>
      <biological_entity id="o1997" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="contractile" value_original="contractile" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems persistent in pistillate plants, erect to nodding, simple, hollow, glabrous.</text>
      <biological_entity id="o1998" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="in plants" constraintid="o1999" is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s2" to="nodding" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1999" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, evergreen, crowded in basal rosettes, reduced distally;</text>
      <biological_entity id="o2000" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="evergreen" value_original="evergreen" />
        <character constraint="in basal rosettes" constraintid="o2001" is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2001" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blades glabrous, margins entire to minutely undulate;</text>
      <biological_entity id="o2002" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2003" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s4" to="minutely undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal blades petiolate, spatulate to oblanceolate;</text>
      <biological_entity constraint="basal" id="o2004" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline blades sessile, linear to lanceolate.</text>
      <biological_entity constraint="cauline" id="o2005" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, ebracteate;</text>
    </statement>
    <statement id="d0_s8">
      <text>in staminate plants racemose, rarely spiciform, 7–15 cm, apex nodding;</text>
      <biological_entity id="o2006" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s8" value="spiciform" value_original="spiciform" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>in pistillate plants racemose or spiciform, to 35 cm in fruit.</text>
      <biological_entity id="o2007" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o2008" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="spiciform" value_original="spiciform" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers unisexual, staminate and pistillate on different plants, occasionally bisexual, ebracteate, weakly syncarpous;</text>
      <biological_entity id="o2009" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o2010" is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" notes="" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s10" value="syncarpous" value_original="syncarpous" />
      </biological_entity>
      <biological_entity id="o2010" name="plant" name_original="plants" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>tepals persistent, 6, spreading or ascending, distinct, white to greenish white, becoming yellow, 1-veined, narrowly linear-spatulate, nectaries absent;</text>
      <biological_entity id="o2011" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish white" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="linear-spatulate" value_original="linear-spatulate" />
      </biological_entity>
      <biological_entity id="o2012" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6;</text>
      <biological_entity id="o2013" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, flattened, unequal;</text>
      <biological_entity id="o2014" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers basifixed, 2-locular, oblong-oblanceolate, extrorse;</text>
      <biological_entity id="o2015" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s14" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s14" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary superior, 3-locular, deeply 3-lobed;</text>
      <biological_entity id="o2016" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s15" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>septal glands absent;</text>
      <biological_entity constraint="septal" id="o2017" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles persistent, 3, recurved, distinct, linear-clavate;</text>
      <biological_entity id="o2018" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s17" value="linear-clavate" value_original="linear-clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas papillate along adaxial surface.</text>
      <biological_entity id="o2019" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character constraint="along adaxial surface" constraintid="o2020" is_modifier="false" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2020" name="surface" name_original="surface" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsular, erect, 3-locular, dehiscence loculicidal.</text>
      <biological_entity id="o2021" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s19" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s19" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 2–4 per locule, elliptic to linear-oblong, with broad, winglike aril.</text>
      <biological_entity id="o2023" name="locule" name_original="locule" src="d0_s20" type="structure" />
      <biological_entity id="o2024" name="aril" name_original="aril" src="d0_s20" type="structure">
        <character is_modifier="true" name="width" src="d0_s20" value="broad" value_original="broad" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s20" value="winglike" value_original="winglike" />
      </biological_entity>
      <relation from="o2022" id="r265" name="with" negation="false" src="d0_s20" to="o2024" />
    </statement>
    <statement id="d0_s21">
      <text>x = 12.</text>
      <biological_entity id="o2022" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o2023" from="2" name="quantity" src="d0_s20" to="4" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s20" to="linear-oblong" />
      </biological_entity>
      <biological_entity constraint="x" id="o2025" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Species 1.</discussion>
  <discussion>Chamaelirium and the east Asian Chionographis Maximowicz (H. Hara 1968) form a classic Arcto-Tertiary, disjunct generic pair whose distinctive nature among the melanthoid genera (J. D. Ambrose 1975, 1980) has lent support to their recognition as a separate tribe, Chionographideae Nakai, within the Melanthiaceae sensu stricto (M. N. Tamura 1998; W. B. Zomlefer 1997b), or as the separate family Chionographidaceae (A. L. Takhtajan 1994, 1997). Both genera have distinctive tetraporate pollen grains (M. Takahashi and S. Kawano 1989), both are based on x = 12 (H. Hara and S. Kurosawa 1962), and they share other karyological similarities (N. Y. Tanaka and N. Tanaka 1985).</discussion>
  <discussion>Floral sexuality is exceptionally diversified in the Chamaelirium–Chionographis lineage. Chamaelirium is dioecious or occasionally polygamodioecious, while Chionographis is often monoclinous or andromonoecious, sometimes gynodioecious, and rarely androdioecious (M. Maki 1992, 1992b, 1993; M. Maki and M. Masusa 1993, 1994).</discussion>
  <discussion>Chamaelirium luteum has been the subject of numerous studies on dioecism associated with dimorphism. Staminate and pistillate plants differ markedly in morphology, life history, and spatial distribution. Mature pistillate plants are more robust than staminate plants, and there are proportionally more staminate than pistillate plants in a given population (T. R. Meagher 1978, 1980, 1981, 1982, 1984; T. R. Meagher and J. Antonovics 1982, 1982b; T. R. Meagher and E. Thompson 1987).</discussion>
  <references>
    <reference>  Silliman, F. E. 1957. Chamaelirium luteum (L.) Gray: A Biological Study. Ph.D. dissertation. University of North Carolina.</reference>
  </references>
  
</bio:treatment>