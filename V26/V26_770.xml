<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Lophiris</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">tenuis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 380. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section lophiris;species tenuis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101718</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes superficial or very shallow, cordlike portion 1–2 dm × 2 mm, expanding to 10–15 mm diam., nodes with brown, scalelike leaves and few to no roots.</text>
      <biological_entity id="o15418" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character name="width" src="d0_s0" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s0" value="expanding" value_original="expanding" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15419" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character is_modifier="true" name="position" src="d0_s0" value="superficial" value_original="superficial" />
        <character is_modifier="true" modifier="very" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character name="width" src="d0_s0" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s0" value="expanding" value_original="expanding" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15420" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <biological_entity id="o15421" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s0" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o15422" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few to no" value_original="few to no" />
      </biological_entity>
      <relation from="o15420" id="r2096" name="with" negation="false" src="d0_s0" to="o15421" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2-branched, 3–3.2 dm.</text>
      <biological_entity id="o15423" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-2-branched" value_original="1-2-branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="3.2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal with blade pale green, 3.2 dm × 1.5 cm, margins scarious basally, apex acute, proximal 2 semi sheathing, blade 5 cm, scarious;</text>
      <biological_entity id="o15424" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o15425" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="length" notes="" src="d0_s2" unit="dm" value="3.2" value_original="3.2" />
        <character name="width" notes="" src="d0_s2" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o15426" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o15427" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="basally" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o15428" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15429" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o15430" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="cm" value="5" value_original="5" />
        <character is_modifier="false" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o15425" id="r2097" name="with" negation="false" src="d0_s2" to="o15426" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 1–2, sheathing branch and stem for ca. 1/2 their length, blade ca. 5 cm, semimembranous.</text>
      <biological_entity id="o15431" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o15432" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <biological_entity id="o15433" name="branch" name_original="branch" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o15434" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o15435" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="cm" value="5" value_original="5" />
        <character is_modifier="false" name="texture" src="d0_s3" value="semimembranous" value_original="semimembranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–3-branched, each unit with single flower, all borne at approximately the same level;</text>
      <biological_entity id="o15436" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-3-branched" value_original="2-3-branched" />
      </biological_entity>
      <biological_entity id="o15437" name="unit" name_original="unit" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="approximately" name="position_relational" notes="" src="d0_s4" value="level" value_original="level" />
      </biological_entity>
      <biological_entity id="o15438" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
      <relation from="o15437" id="r2098" name="with" negation="false" src="d0_s4" to="o15438" />
    </statement>
    <statement id="d0_s5">
      <text>spathes 2–3 cm × 5 mm, subequal, scarious except basally and along midrib.</text>
      <biological_entity id="o15439" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" value_original="subequal" />
        <character constraint="except midrib" constraintid="o15440" is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o15440" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth white or blue tinged with deep violet lines;</text>
      <biological_entity id="o15441" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15442" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character constraint="with lines" constraintid="o15443" is_modifier="false" name="coloration" src="d0_s6" value="blue tinged" value_original="blue tinged" />
      </biological_entity>
      <biological_entity id="o15443" name="line" name_original="lines" src="d0_s6" type="structure">
        <character is_modifier="true" name="depth" src="d0_s6" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="violet" value_original="violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube funnelform, 0.3 cm;</text>
      <biological_entity id="o15444" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15445" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="cm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals oblong-spatulate, 2.8 × 1 cm, apex deeply emarginate, signal an inconspicuous crest with low, yellow, undissected ridge;</text>
      <biological_entity id="o15446" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15447" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-spatulate" value_original="oblong-spatulate" />
        <character name="length" src="d0_s8" unit="cm" value="2.8" value_original="2.8" />
        <character name="width" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15448" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s8" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o15449" name="crest" name_original="crest" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o15450" name="ridge" name_original="ridge" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="low" value_original="low" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="shape" src="d0_s8" value="undissected" value_original="undissected" />
      </biological_entity>
      <relation from="o15449" id="r2099" name="with" negation="false" src="d0_s8" to="o15450" />
    </statement>
    <statement id="d0_s9">
      <text>petals bluish white, oblanceolate-spatulate, base gradually attenuate into claw;</text>
      <biological_entity id="o15451" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15452" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bluish white" value_original="bluish white" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate-spatulate" value_original="oblanceolate-spatulate" />
      </biological_entity>
      <biological_entity id="o15453" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="into claw" constraintid="o15454" is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o15454" name="claw" name_original="claw" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>ovary elliptical, 0.4–0.7 cm;</text>
      <biological_entity id="o15455" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15456" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptical" value_original="elliptical" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s10" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 1.8 cm, crests broadly obovate, 0.7 cm, margins erose;</text>
      <biological_entity id="o15457" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15458" name="style" name_original="style" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="cm" value="1.8" value_original="1.8" />
      </biological_entity>
      <biological_entity id="o15459" name="crest" name_original="crests" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s11" unit="cm" value="0.7" value_original="0.7" />
      </biological_entity>
      <biological_entity id="o15460" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas triangular-acuminate, margins entire;</text>
      <biological_entity id="o15461" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15462" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular-acuminate" value_original="triangular-acuminate" />
      </biological_entity>
      <biological_entity id="o15463" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicel 0.4–1 cm, not lifting flower clear of spathes.</text>
      <biological_entity id="o15464" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15465" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s13" to="1" to_unit="cm" />
        <character constraint="of spathes" constraintid="o15467" is_modifier="false" modifier="not" name="coloration" src="d0_s13" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity id="o15466" name="flower" name_original="flower" src="d0_s13" type="structure" />
      <biological_entity id="o15467" name="spathe" name_original="spathes" src="d0_s13" type="structure" />
      <relation from="o15465" id="r2100" name="lifting" negation="true" src="d0_s13" to="o15466" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules depressed-globose, roundly triangular, 0.9–1.5 × 1.2 cm.</text>
      <biological_entity id="o15468" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="depressed-globose" value_original="depressed-globose" />
        <character is_modifier="false" modifier="roundly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="length" src="d0_s14" to="1.5" to_unit="cm" />
        <character name="width" src="d0_s14" unit="cm" value="1.2" value_original="1.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds pale-brown, with whitish raphe, D-shaped, pitted.</text>
      <biological_entity id="o15470" name="raphe" name_original="raphe" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s15" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o15469" id="r2101" name="with" negation="false" src="d0_s15" to="o15470" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o15469" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="d--shaped" value_original="d--shaped" />
        <character is_modifier="false" name="relief" src="d0_s15" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15471" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, wooded slopes in leafy soil with Douglas fir and dense shrubs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" constraint="in leafy soil with douglas" />
        <character name="habitat" value="wooded slopes" constraint="in leafy soil with douglas" />
        <character name="habitat" value="leafy soil" constraint="with douglas" />
        <character name="habitat" value="fir" />
        <character name="habitat" value="dense shrubs" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Clackamas iris</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Iris tenuis was originally placed in ser. Californicae, but R. C. Foster (1937) stated, “It is with some hesitation that I leave it in association with them. The broad, pale green leaves are much like those of a giant I. cristata.” F. H. Smith and Q. D. Clarkson (1956) said, “It clearly does not belong in the subsection with the other members of the Californicae,” and Clarkson in a later treatment (1958) created a new subsection, the Oregonae, for it. L. W. Lenz (1959b) moved this species into subsect. Evansia (= sect. Lophiris), with which it shows many relationships.</discussion>
  
</bio:treatment>