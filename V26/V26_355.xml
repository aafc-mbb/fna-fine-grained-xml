<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James V. LaFrankie</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">206</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="unknown" rank="genus">MAIANTHEMUM</taxon_name>
    <place_of_publication>
      <publication_title>Prim. Fl. Holsat.,</publication_title>
      <place_in_publication>14. 1780</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus MAIANTHEMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin Maius, May, and Greek anthemon, flower</other_info_on_name>
    <other_info_on_name type="fna_id">119474</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Sigillaria</taxon_name>
    <taxon_hierarchy>genus Sigillaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Desfontaines" date="unknown" rank="genus">Smilacina</taxon_name>
    <taxon_hierarchy>genus Smilacina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial or aquatic, 1–12.5 dm, from rhizomes.</text>
      <biological_entity id="o30484" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="12.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o30485" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o30484" id="r4106" name="from" negation="false" src="d0_s0" to="o30485" />
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes persistent, sympodial, spreading and filiform, or densely clumped, cylindrical, and fleshy.</text>
      <biological_entity id="o30486" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="sympodial" value_original="sympodial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s1" value="clumped" value_original="clumped" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, arching or erect.</text>
      <biological_entity id="o30487" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2–15, cauline, distichous, clasping or short-petiolate;</text>
      <biological_entity id="o30488" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30489" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually ovate, glabrous or weakly pubescent, base rounded or cordiform, margins flat or undulate, denticulate or entire, apex acute or caudate.</text>
      <biological_entity id="o30490" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="weakly" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30491" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordiform" value_original="cordiform" />
      </biological_entity>
      <biological_entity id="o30492" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30493" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="caudate" value_original="caudate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminally paniculate or racemose, 5–250-flowered.</text>
      <biological_entity id="o30494" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="terminally" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-250-flowered" value_original="5-250-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 3-merous (6 tepals, 6 stamens) or, by reduction, 2-merous (4 tepals, 4 stamens);</text>
      <biological_entity id="o30495" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-merous" value_original="3-merous" />
        <character name="architecture" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="2-merous" value_original="2-merous" />
      </biological_entity>
      <biological_entity id="o30496" name="reduction" name_original="reduction" src="d0_s6" type="structure" />
      <relation from="o30495" id="r4107" name="by" negation="false" src="d0_s6" to="o30496" />
    </statement>
    <statement id="d0_s7">
      <text>perianth spreading;</text>
      <biological_entity id="o30497" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals distinct, white, ovate or triangular, equal, 0.5–5 mm;</text>
      <biological_entity id="o30498" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens inserted at tepal base;</text>
      <biological_entity id="o30499" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity constraint="tepal" id="o30500" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o30499" id="r4108" name="inserted at" negation="false" src="d0_s9" to="o30500" />
    </statement>
    <statement id="d0_s10">
      <text>anthers 4-locular, dehiscence introrse;</text>
      <biological_entity id="o30501" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 2–3-carpellate, septal walls with nectariferous canals;</text>
      <biological_entity id="o30502" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-3-carpellate" value_original="2-3-carpellate" />
      </biological_entity>
      <biological_entity constraint="septal" id="o30503" name="wall" name_original="walls" src="d0_s11" type="structure" />
      <biological_entity constraint="nectariferous" id="o30504" name="canal" name_original="canals" src="d0_s11" type="structure" />
      <relation from="o30503" id="r4109" name="with" negation="false" src="d0_s11" to="o30504" />
    </statement>
    <statement id="d0_s12">
      <text>style shorter than 1.5 mm;</text>
      <biological_entity id="o30505" name="style" name_original="style" src="d0_s12" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma 2–3-lobed, less than 1 mm wide;</text>
      <biological_entity id="o30506" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-3-lobed" value_original="2-3-lobed" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel subtended by 1 or more bracts.</text>
      <biological_entity id="o30507" name="pedicel" name_original="pedicel" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits baccate, variously mottled when immature, bright red at maturity, usually lobed, 4–12 mm wide, pulp thin.</text>
      <biological_entity id="o30508" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="baccate" value_original="baccate" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s15" value="mottled" value_original="mottled" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s15" value="bright red" value_original="bright red" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30509" name="pulp" name_original="pulp" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–12, globose, 3–6 mm diam.;</text>
      <biological_entity id="o30510" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="12" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>testa pale-brown, thin;</text>
      <biological_entity id="o30511" name="testa" name_original="testa" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm scaly.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 18.</text>
      <biological_entity id="o30512" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s18" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity constraint="x" id="o30513" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Central America, n Europe, e Asia to the Himalayas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="e Asia to the Himalayas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <discussion>Species 30 (5 in the flora).</discussion>
  <discussion>Outside the flora area, Maianthemum may be epiphytic, with foliage stems pendent or erect; two species are dioecious; tepals may be fused, the perianth campanulate; and tepals and anthers may be pink, green, or violet.</discussion>
  <discussion>The two species of Maianthemum that bear 4-tepaled flowers, M. canadense and M. dilatatum, are very similar to one another and to the boreal Eurasian species M. bifolium (Linnaeus) F. W. Schmidt, and the three have sometimes been considered varieties of a single species. In most recent floristic works, the genus Maianthemum has been limited to these three species because of their reduced flowers, whereas the remaining species, which bear 6-tepaled flowers more typical of the Liliaceae, have been placed in the genus Smilacina. J. V. LaFrankie (1986) placed all the species in a single genus.</discussion>
  <discussion>The monophyly of Maianthemum sensu lato is supported by morphological consistencies including baccate fruits that are marked when immature, a consistent and unique karyotype (S. Kawano and H. H. Iltis 1966; S. Kawano et al. 1967; N. H. Valentine and H. M. Hassan 1971; M. N. Tamura 1995), and molecular analysis (P. J. Rudall et al. 2000; J. Yamashita and M. N. Tamura 2000). The 2-merous floral condition in Maianthemum sensu stricto is the result of anatomical reduction from the 3-merous state (F. H. Utech and S. Kawano 1976).</discussion>
  <references>
    <reference>Ihara, M. Suzuki, and H. H. Iltis. 1967. Biosystematic studies on Maianthemum (Liliaceae). I. Somatic chromosome number and morphology. Bot. Mag. (Tokyo) 80: 345–352.  </reference>
    <reference>Kawano, S. and H. H. Iltis. 1966. Cytotaxonomy of the genus Smilacina. II. Chromosome morphology and evolutionary consideration of the New World species. Cytologia 31: 12–28.  </reference>
    <reference>Kawano, S. and M. Suzuki. 1971. Biosystematic studies on Maianthemum (Liliaceae-Polygonateae [sic]). VI. Variation in gross morphology of M. bifolium and M. canadense with special reference to their taxonomic status. Bot. Mag. (Tokyo) 84: 349–361.    La</reference>
    <reference>Kawano, S., M. Suzuki and S. Kojima. 1971. Biosystematic studies on Maianthemum (Liliaceae-Polygonateae [sic]). V. Variation in gross morphology, karyology and ecology of North American populations of M. dilatatum sensu lato. Bot. Mag. (Tokyo) 84: 299–318.</reference>
    <reference>Frankie, J. V. 1984. Anatomy of stem abcission in the genus Smilacina (Liliaceae). J. Arnold Arbor. 65: 563–570.  La</reference>
    <reference>Frankie, J. V. 1986. Transfer of the species of Smilacina Desf. to Maianthemum Wiggers (Liliaceae). Taxon 35: 584–589.  La</reference>
    <reference>Frankie, J. V. 1986b. Morphology and taxonomy of the New World species of Maianthemum (Liliaceae). J. Arnold Arbor. 67: 371–439.  </reference>
    <reference>Takahashi, M. and K. Sohma. 1983. Pollen morphology of the genus Smilacina (Liliaceae). Sci. Rep. Tohoku Imp. Univ., Ser. 4, Biol. 38: 191–218.  </reference>
    <reference>Utech, F. H. and S. Kawano. 1976. Biosystematic studies on Maianthemum (Liliaceae). VIII. Floral anatomy of M. dilatatum, M. bifolium, M. canadense. Bot. Mag. (Tokyo) 89: 145–157.  </reference>
    <reference>Valentine, D. H. and H. M. Hassan. 1971. Cytotaxonomy of the genus Maianthemum. J. Indian Bot. Soc. 50: 437–446.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Rhizomes 8–14 mm wide; inflorescences paniculate, branches well developed; tepals inconspicuous, 0.5–1 mm.</description>
      <determination>3 Maianthemum racemosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Rhizomes 1–4.5 mm wide; inflorescences racemose, simple or complex, flowers 1–4 per node; tepals conspicuous, longer than 1 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rhizomes 15–60 cm × 3–4.5 mm, roots scattered; leaves 8–11 on fertile shoots; tepals 6; immature fruits green striped with black.</description>
      <determination>4 Maianthemum stellatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rhizomes 1–30 cm × 1–2 mm, roots restricted to nodes; leaves 2–4 on fertile shoots; tepals 4 or 6; immature fruits green mottled or spotted with red.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade base tapered; racemes simple; tepals 6.</description>
      <determination>5 Maianthemum trifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade base with narrow or broad sinus; racemes complex, with 1–4 flowers per node; tepals 4.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 10–25 cm; proximal leaves sessile, blade ovate, base with narrow sinus; distal leaf blade cordate; petiole 1–7 mm; flowers (1–)2(–3) per node.</description>
      <determination>1 Maianthemum canadense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 20–45 cm; proximal leaves short-petiolate, blade triangular to cordate, base with broad, open sinus; distal leaf blade deeply cordate; petiole 7–10 cm; flowers (1–)3(–4) per node.</description>
      <determination>2 Maianthemum dilatatum</determination>
    </key_statement>
  </key>
</bio:treatment>