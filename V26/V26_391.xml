<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Trattinnick" date="unknown" rank="genus">hosta</taxon_name>
    <taxon_name authority="Engler in H. G. A. Engler and K. Prantl" date="1887" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>2[II,5]: 40, plates 17, 18. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hosta;species lancifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242101669</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming open clumps 35–50 × 30 cm;</text>
      <biological_entity id="o874" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="length" src="d0_s0" to="50" to_unit="cm" />
        <character name="width" src="d0_s0" unit="cm" value="30" value_original="30" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o875" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o874" id="r124" name="forming" negation="false" src="d0_s0" to="o875" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes sometime stoloniferous.</text>
      <biological_entity id="o876" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometime" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole erect, green, dotted purple at base, 17–25 cm;</text>
      <biological_entity id="o877" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o878" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character constraint="at base" constraintid="o879" is_modifier="false" name="coloration" src="d0_s2" value="dotted purple" value_original="dotted purple" />
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o879" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade deep olive green, lanceolate to ovatelanceolate, 10–17 × 5–7.5 cm, apex narrowly acuminate;</text>
      <biological_entity id="o880" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o881" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="depth" src="d0_s3" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="olive green" value_original="olive green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovatelanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="17" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o882" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins in 5–6 lateral pairs.</text>
      <biological_entity id="o883" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o884" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="lateral" id="o885" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <relation from="o884" id="r125" name="in" negation="false" src="d0_s4" to="o885" />
    </statement>
    <statement id="d0_s5">
      <text>Scape 40–50 cm.</text>
      <biological_entity id="o886" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: racemes 10–20-flowered, lax, slender, 17–20 cm;</text>
      <biological_entity id="o887" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o888" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-20-flowered" value_original="10-20-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bracts short, glossy green, narrowly navicular;</text>
      <biological_entity id="o889" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o890" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="navicular" value_original="navicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sterile bracts 3–5, large, leafy.</text>
      <biological_entity id="o891" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o892" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="size" src="d0_s8" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 4–4.5 cm, not fragrant;</text>
      <biological_entity id="o893" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s9" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth tubular-campanulate;</text>
      <biological_entity id="o894" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular-campanulate" value_original="tubular-campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals purplish violet, lobes spreading, recurved;</text>
      <biological_entity id="o895" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish violet" value_original="purplish violet" />
      </biological_entity>
      <biological_entity id="o896" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers purple.</text>
      <biological_entity id="o897" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules rarely developing (pod sterile).</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 60.</text>
      <biological_entity id="o898" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rarely" name="development" src="d0_s13" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="2n" id="o899" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall (September).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
        <character name="flowering time" char_type="atypical_range" to="September" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed open areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Conn., Ill., Md., N.J., N.Y., Ohio, Pa., expected elsewhere; Japan (of garden origin); cultivated worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="expected elsewhere" establishment_means="introduced" />
        <character name="distribution" value="Japan (of garden origin)" establishment_means="native" />
        <character name="distribution" value="cultivated worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Narrowleaf plantain-lily</other_name>
  <discussion>Hosta lancifolia produces an excellent ground cover with deep olive green leaves and vigorous vegetative growth. Since this species is of ancient horticultural origin and does not occur in the wild, it has been reduced to cultivar status in horticultural nomenclature as Hosta ‘Lancifolia’ (W. G. Schmid 1991).</discussion>
  
</bio:treatment>