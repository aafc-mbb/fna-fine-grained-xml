<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1812" rank="genus">hymenocallis</taxon_name>
    <taxon_name authority="Traub" date="1962" rank="species">henryae</taxon_name>
    <taxon_name authority="J. N. Henry &amp; Gerald L. Smith" date="1999" rank="variety">glaucifolia</taxon_name>
    <place_of_publication>
      <publication_title>Herbertia</publication_title>
      <place_in_publication>54: 114. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hymenocallis;species henryae;variety glaucifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102252</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often growing in dense clumps.</text>
      <biological_entity id="o4081" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4082" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4081" id="r595" name="growing in" negation="false" src="d0_s0" to="o4082" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 4–7, erect, 3–6 dm × 1.9–2.9 cm;</text>
      <biological_entity id="o4083" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s1" to="7" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s1" to="6" to_unit="dm" />
        <character char_type="range_value" from="1.9" from_unit="cm" name="width" src="d0_s1" to="2.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade blue-green in appearance due to glaucousness.</text>
      <biological_entity id="o4084" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="in appearance" constraintid="o4085" is_modifier="false" name="coloration" src="d0_s2" value="blue-green" value_original="blue-green" />
      </biological_entity>
      <biological_entity id="o4085" name="appearance" name_original="appearance" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Tepals to 16 cm.</text>
      <biological_entity id="o4086" name="tepal" name_original="tepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cypress depressions at edges of pine flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cypress depressions" constraint="at edges of pine flatwoods" />
        <character name="habitat" value="edges" constraint="of pine flatwoods" />
        <character name="habitat" value="pine flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14b.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Hymenocallis henryae var. glaucifolia occurs only in Liberty County and represents an apparent divergence of the populations of H. henryae east of the Apalachicola River. In these plants, a heavy glaucousness causes the surface of the leaves to appear blue-green. This characteristic, along with the clumping habit and tepal length to 16 cm, supports recognition of these Liberty County populations as a distinct variety.</discussion>
  
</bio:treatment>