<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Laevigatae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">versicolor</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 39. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series laevigatae;species versicolor;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200028227</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes pale pinkish white, freely branching, forming large clumps, 1–2.5 cm diam., clothed with remnants of old leaves;</text>
      <biological_entity id="o29285" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale pinkish" value_original="pale pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29286" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o29287" name="remnant" name_original="remnants" src="d0_s0" type="structure" />
      <biological_entity id="o29288" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <relation from="o29285" id="r3953" name="forming" negation="false" src="d0_s0" to="o29286" />
      <relation from="o29285" id="r3954" name="clothed with" negation="false" src="d0_s0" to="o29287" />
      <relation from="o29285" id="r3955" name="clothed with" negation="false" src="d0_s0" to="o29288" />
    </statement>
    <statement id="d0_s1">
      <text>roots fleshy.</text>
      <biological_entity id="o29289" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–2-branched, solid, 2–6 dm.</text>
      <biological_entity id="o29290" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-2-branched" value_original="1-2-branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal with blade green to grayish green, often purplish basally, centrally thickened in mature leaves, prominently veined, narrowly ensiform, 1–8 dm × 1–3 cm;</text>
      <biological_entity id="o29291" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o29292" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often; basally" name="coloration" notes="" src="d0_s3" value="purplish" value_original="purplish" />
        <character constraint="in leaves" constraintid="o29294" is_modifier="false" modifier="centrally" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="prominently" name="architecture" notes="" src="d0_s3" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="ensiform" value_original="ensiform" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s3" to="8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29293" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="grayish green" />
      </biological_entity>
      <biological_entity id="o29294" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o29292" id="r3956" name="with" negation="false" src="d0_s3" to="o29293" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 1–2, blade linearlanceolate, seldom equaling stem.</text>
      <biological_entity id="o29295" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o29296" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity id="o29297" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o29298" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="seldom" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences compact, units 2–4-flowered;</text>
      <biological_entity id="o29299" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o29300" name="unit" name_original="units" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes never foliaceous, 3–6 cm, unequal, outer shorter than inner, thickly chartaceous to scarious, margins shiny, darker in color.</text>
      <biological_entity id="o29301" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="position" src="d0_s6" value="outer" value_original="outer" />
        <character constraint="than inner" constraintid="o29302" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="outer shorter" value_original="outer shorter" />
        <character char_type="range_value" from="thickly chartaceous" name="texture" src="d0_s6" to="scarious" />
      </biological_entity>
      <biological_entity id="o29302" name="inner" name_original="inner" src="d0_s6" type="structure" />
      <biological_entity id="o29303" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s6" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="darker" value_original="darker" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth violet-blue to rarely white;</text>
      <biological_entity id="o29304" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o29305" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="violet-blue" name="coloration" src="d0_s7" to="rarely white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube funnelform, constricted above ovary, 1–1.2 cm;</text>
      <biological_entity id="o29306" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o29307" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character constraint="above ovary" constraintid="o29308" is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29308" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to reniform, 4–7.2 × 1.8–4 cm, base abruptly attenuate, signal a pubescent, greenish or greenish yellow patch surrounded by heavily veined purple on white at base of blade;</text>
      <biological_entity id="o29309" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29310" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="reniform" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s9" to="7.2" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29311" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o29312" name="patch" name_original="patch" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s9" value="surrounded" value_original="surrounded" />
        <character constraint="by" is_modifier="false" modifier="heavily" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character constraint="at base" constraintid="o29313" is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o29313" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o29314" name="blade" name_original="blade" src="d0_s9" type="structure" />
      <relation from="o29313" id="r3957" name="part_of" negation="false" src="d0_s9" to="o29314" />
    </statement>
    <statement id="d0_s10">
      <text>petals lanceolate to oblong-lanceolate, 2–5 × 0.5–2 cm, much shorter than sepals, firm, apex rarely emarginate;</text>
      <biological_entity id="o29315" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29316" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblong-lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s10" to="2" to_unit="cm" />
        <character constraint="than sepals" constraintid="o29317" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" name="texture" src="d0_s10" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o29317" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <biological_entity id="o29318" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary rounded-triangular in cross-section, somewhat inflated, 0.8–2 cm;</text>
      <biological_entity id="o29319" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o29320" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character constraint="in cross-section" constraintid="o29321" is_modifier="false" name="shape" src="d0_s11" value="rounded-triangular" value_original="rounded-triangular" />
        <character is_modifier="false" modifier="somewhat" name="shape" notes="" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29321" name="cross-section" name_original="cross-section" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style 3–3.5 cm, base not auriculate, margins entire or toothed, crests reflexed, 0.7–1.5 cm;</text>
      <biological_entity id="o29322" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o29323" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s12" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29324" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o29325" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s12" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o29326" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s12" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas unlobed, triangular or rounded-triangular, margins entire;</text>
      <biological_entity id="o29327" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o29328" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded-triangular" value_original="rounded-triangular" />
      </biological_entity>
      <biological_entity id="o29329" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 2–8 cm, frequently exceeding spathe.</text>
      <biological_entity id="o29330" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o29331" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s14" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29332" name="spathe" name_original="spathe" src="d0_s14" type="structure" />
      <relation from="o29331" id="r3958" modifier="frequently" name="exceeding" negation="false" src="d0_s14" to="o29332" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules often persistent over winter, ovoid to oblong-ellipsoid, conspicuously beaked, obtusely triangular in cross-section, 1.5–6 cm, tardily dehiscent.</text>
      <biological_entity id="o29333" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character constraint="over winter" is_modifier="false" modifier="often" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="oblong-ellipsoid" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
        <character constraint="in cross-section" constraintid="o29334" is_modifier="false" modifier="obtusely" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s15" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o29334" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, D-shaped, 5–8 mm, shiny, thin, hard, regularly pebbled, not corky.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 108.</text>
      <biological_entity id="o29335" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="d--shaped" value_original="d--shaped" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s16" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="regularly" name="relief" src="d0_s16" value="pebbled" value_original="pebbled" />
        <character is_modifier="false" modifier="not" name="pubescence_or_texture" src="d0_s16" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29336" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="108" value_original="108" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy places, along roadsides, shores, and along mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" modifier="marshy places along" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="mountains" modifier="and along" />
        <character name="habitat" value="marshy" modifier="marshy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Conn., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., Ohio, Pa., R.I., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Blue flag</other_name>
  <other_name type="common_name">iris versicolore</other_name>
  <discussion>E. Anderson (1936) showed rather conclusively that Iris versicolor arose as an amphidiploid between I. virginica (n = 35) and I. hookeri (I. setosa var. canadensis) (n = 19). Back-cross hybrids have been produced both ways: I. virginica × I. versicolor producing Iris ×robusta E. S. Anderson, and I. versicolor × I. hookeri producing I. ×sancti-cyri J. Rousseau.</discussion>
  <discussion>Iris versicolor is becoming a weed in New Brunswick, Prince Edward Island, and Nova Scotia. Livestock will not eat iris foliage, but feed voraciously on the competition, thus giving the irises plenty of room to expand.</discussion>
  
</bio:treatment>