<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">176</other_info_on_meta>
    <other_info_on_meta type="treatment_page">178</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lilium</taxon_name>
    <taxon_name authority="Thunberg" date="1794" rank="species">lancifolium</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>2: 333. 1794</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lilium;species lancifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200027720</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Ker Gawler" date="unknown" rank="species">tigrinum</taxon_name>
    <taxon_hierarchy>genus Lilium;species tigrinum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs widely ovoid, 3.5 (–8) × 4–8 cm, ca. 0.7–0.8 times taller than wide;</text>
      <biological_entity id="o34324" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="8" to_unit="cm" />
        <character name="length" src="d0_s0" unit="cm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s0" to="8" to_unit="cm" />
        <character is_modifier="false" name="height_or_width" src="d0_s0" value="0.7-0.8 times taller than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales broad, unsegmented, longest ca. 3–4 cm;</text>
      <biological_entity id="o34325" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stem roots usually present.</text>
      <biological_entity constraint="stem" id="o34326" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems white-lanate, purplish, to 2 m.</text>
      <biological_entity id="o34327" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="white-lanate" value_original="white-lanate" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s3" to="2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Buds usually flat-sided, somewhat triangular in cross-section.</text>
      <biological_entity id="o34328" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="flat-sided" value_original="flat-sided" />
        <character constraint="in cross-section" constraintid="o34329" is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o34329" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves scattered, horizontal and drooping at tips, 10–15 (–18) × ca. 1.5 cm, ca. 3–6 (–10) times longer than wide, distal bearing 1 (–3) dark purple axillary bulbils;</text>
      <biological_entity id="o34330" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character constraint="at tips" constraintid="o34331" is_modifier="false" name="orientation" src="d0_s5" value="drooping" value_original="drooping" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s5" to="15" to_unit="cm" />
        <character name="width" notes="" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="3-6(-10)" value_original="3-6(-10)" />
      </biological_entity>
      <biological_entity id="o34331" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o34332" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="axillary" id="o34333" name="bulbil" name_original="bulbils" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <relation from="o34332" id="r4623" name="bearing" negation="false" src="d0_s5" to="o34333" />
    </statement>
    <statement id="d0_s6">
      <text>blade lanceolate, often narrowly so, margins not undulate, papillose, apex white-lanate, narrowly acute (rounded in distal leaves).</text>
      <biological_entity id="o34334" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o34335" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often narrowly; narrowly; not" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o34336" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="white-lanate" value_original="white-lanate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences racemose, 3–6 (–25) -flowered.</text>
      <biological_entity id="o34337" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-6(-25)-flowered" value_original="3-6(-25)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent, not fragrant;</text>
      <biological_entity id="o34338" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s8" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth Turk’s-cap-shaped;</text>
      <biological_entity id="o34339" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="s-cap--shaped" value_original="s-cap--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals and petals reflexed ca. 1/5–1/4 along length from base, orange with many purple-brown spots, not distinctly clawed, 7–10 × 1–2 cm, adaxial base bearing pubescent strip;</text>
      <biological_entity id="o34340" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character constraint="with adaxial base" constraintid="o34343" is_modifier="false" name="coloration" notes="" src="d0_s10" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity id="o34341" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="from base" constraintid="o34342" from="1/5" name="length" src="d0_s10" to="1/4" />
        <character constraint="with adaxial base" constraintid="o34343" is_modifier="false" name="coloration" notes="" src="d0_s10" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity id="o34342" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o34343" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="many" value_original="many" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="purple-brown spots" value_original="purple-brown spots" />
        <character is_modifier="true" modifier="not distinctly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="7" from_unit="cm" is_modifier="true" name="length" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="width" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34344" name="strip" name_original="strip" src="d0_s10" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o34343" id="r4624" name="bearing" negation="false" src="d0_s10" to="o34344" />
    </statement>
    <statement id="d0_s11">
      <text>sepals not ridged abaxially;</text>
      <biological_entity id="o34345" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not; abaxially" name="shape" src="d0_s11" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens strongly exserted;</text>
      <biological_entity id="o34346" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments very widely spreading, diverging ca. 25° from axis;</text>
      <biological_entity id="o34347" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="very widely" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character constraint="from axis" constraintid="o34348" is_modifier="false" modifier="25°" name="orientation" src="d0_s13" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o34348" name="axis" name_original="axis" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers purplish, ca. 2 cm;</text>
      <biological_entity id="o34349" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
        <character name="some_measurement" src="d0_s14" unit="cm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollen rust;</text>
      <biological_entity id="o34350" name="pollen" name_original="pollen" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="rust" value_original="rust" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistil 6–9 cm;</text>
      <biological_entity id="o34351" name="pistil" name_original="pistil" src="d0_s16" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s16" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel sometimes dichotomous, stout and relatively short, to 10 cm.</text>
      <biological_entity id="o34352" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s17" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s17" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules 3–4 cm.</text>
      <biological_entity id="o34353" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s18" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds not counted.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 24, 36.</text>
      <biological_entity id="o34354" name="seed" name_original="seeds" src="d0_s19" type="structure" />
      <biological_entity constraint="2n" id="o34355" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="24" value_original="24" />
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer (mid Jul–early Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" constraint="mid Jul-early Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, railroad banks, near dwellings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad banks" />
        <character name="habitat" value="near dwellings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–ca. 1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="" from="0" from_unit="" constraint=" -ca" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., N.S., Ont., Que.; Ala., Conn., Del., D.C., Ill., Ind., Iowa, Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Pa., R.I., Vt., Va., W.Va., Wis.; Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Tiger lily</other_name>
  <other_name type="common_name">lis tigré</other_name>
  <discussion>Throughout most of modern botanical history this Chinese lily has been known as Lilium tigrinum, but recent nomenclatural reassessment affirms that Thunberg’s description, published sixteen years earlier than Ker Gawler’s, applies to this species. Though many North America species are known vernacularly as tiger lilies, the name is properly applied only to this one. Along with L. candidum, it is considered to be among the earliest domesticated lilies (H. D. Woodcock and W. T. Stearn 1950), no doubt because it is handsome, easy to grow, and the bulbs are edible and substantial. It is widely planted in North America, usually as a sterile triploid that is best propagated from the bulbils.</discussion>
  <discussion>Perhaps the hardiest garden lily, Lilium lancifolium is a widespread but sporadic garden escape, and roadside lilies near habitation in eastern and northeastern North America are often this species. Despite its general use in gardens, it seems to be naturalized only in the better-watered eastern portion of the continent.</discussion>
  <discussion>In North America, the tiger lily is rather easily diagnosed by its truly lanceolate and widely sessile alternating leaves that bear dark bulbils on the upper stem. The mature buds are usually high-shouldered and taper rather evenly to a flattish apex with three greenish, terminal, rounded processes. No native lily consistently displays any of these features.</discussion>
  
</bio:treatment>