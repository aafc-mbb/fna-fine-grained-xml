<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="treatment_page">400</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="genus">nemastylis</taxon_name>
    <taxon_name authority="Small" date="1931" rank="species">floridana</taxon_name>
    <place_of_publication>
      <publication_title>J. New York Bot. Gard.</publication_title>
      <place_in_publication>32: 266, fig. 2. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus nemastylis;species floridana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101790</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, 40–150 cm.</text>
      <biological_entity id="o32792" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bulbs 7–9 (–15) mm diam.</text>
      <biological_entity id="o32793" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s1" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems slender, usually 3–6-branched, rarely simple.</text>
      <biological_entity id="o32794" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="3-6-branched" value_original="3-6-branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (3–) 5–7, basal leaves 2–3, longest;</text>
      <biological_entity id="o32795" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s3" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity constraint="basal" id="o32796" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="length" src="d0_s3" value="longest" value_original="longest" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ± linear, 3–10 mm wide;</text>
      <biological_entity id="o32797" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline leaves progressively reduced distally, becoming bractlike.</text>
      <biological_entity constraint="cauline" id="o32798" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Rhipidia usually 2-flowered;</text>
      <biological_entity id="o32799" name="rhipidium" name_original="rhipidia" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer spathe ± 1/2 or slightly less than inner, inner 25–30 mm, apex dry.</text>
      <biological_entity constraint="outer" id="o32800" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character modifier="slightly less" name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o32801" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o32803" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32804" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s7" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Tepals dark blue, lanceolate, 20 × 7.5 mm;</text>
      <biological_entity id="o32805" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character name="length" src="d0_s8" unit="mm" value="20" value_original="20" />
        <character name="width" src="d0_s8" unit="mm" value="7.5" value_original="7.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments connate in proximal 1/2 or entirely, ca. 2 mm;</text>
      <biological_entity id="o32806" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="in proximal 1/2" constraintid="o32807" is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character modifier="entirely" name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o32807" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers ca. 8 mm;</text>
      <biological_entity id="o32808" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary ovoid, ca. 2.5 mm;</text>
      <biological_entity id="o32809" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style branching at apex of filament tube;</text>
      <biological_entity id="o32810" name="style" name_original="style" src="d0_s12" type="structure">
        <character constraint="at apex" constraintid="o32811" is_modifier="false" name="architecture" src="d0_s12" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o32811" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity constraint="filament" id="o32812" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <relation from="o32811" id="r4410" name="part_of" negation="false" src="d0_s12" to="o32812" />
    </statement>
    <statement id="d0_s13">
      <text>branches ca. 5 mm.</text>
      <biological_entity id="o32813" name="branch" name_original="branches" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid, truncate, 6–9 mm.</text>
      <biological_entity id="o32814" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds angular-prismatic, ca. 2 mm. 2n = 56.</text>
      <biological_entity id="o32815" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="angular-prismatic" value_original="angular-prismatic" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32816" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, calcareous, pine flatwoods, wet hummocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous" modifier="wet" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="wet hummocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Fall-flowering ixia</other_name>
  <other_name type="common_name">Florida celestial</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>The flowers of Nemastylis floridana open ca. 4 p.m. and close ca. 6 p.m.</discussion>
  
</bio:treatment>