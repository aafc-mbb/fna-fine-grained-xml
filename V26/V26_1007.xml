<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">dioscoreaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dioscorea</taxon_name>
    <taxon_name authority="Pax" date="1892" rank="species">sansibarensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>15: 146. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dioscoreaceae;genus dioscorea;species sansibarensis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242101570</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dioscorea</taxon_name>
    <taxon_name authority="Jumelle &amp; H. Perrier" date="unknown" rank="species">macabiha</taxon_name>
    <taxon_hierarchy>genus Dioscorea;species macabiha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dioscorea</taxon_name>
    <taxon_name authority="Harms" date="unknown" rank="species">macroura</taxon_name>
    <taxon_hierarchy>genus Dioscorea;species macroura;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dioscorea</taxon_name>
    <taxon_name authority="Rendle" date="unknown" rank="species">welwitschii</taxon_name>
    <taxon_hierarchy>genus Dioscorea;species welwitschii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tuberous;</text>
      <biological_entity id="o6269" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>tubers buried just below soil surface, irregularly lobed, globose.</text>
      <biological_entity id="o6270" name="tuber" name_original="tubers" src="d0_s1" type="structure">
        <character constraint="just below soil, surface" constraintid="o6271, o6272" is_modifier="false" name="location" src="d0_s1" value="buried" value_original="buried" />
        <character is_modifier="false" modifier="irregularly" name="shape" notes="" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o6271" name="soil" name_original="soil" src="d0_s1" type="structure" />
      <biological_entity id="o6272" name="surface" name_original="surface" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems twining counterclockwise, climbing to more than 7 m, terete, grooved, or weakly angled with variable number of longitudinal ridges raised less than 1 mm adaxially, producing axillary bulbils frequently greater than 5 cm diam. in leaf-axils.</text>
      <biological_entity id="o6273" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="twining" value_original="twining" />
        <character constraint="to 7+ m" is_modifier="false" name="growth_form" src="d0_s2" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="grooved" value_original="grooved" />
        <character constraint="with number" constraintid="o6274" is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character char_type="range_value" constraint="in leaf-axils" constraintid="o6277" from="5" from_unit="cm" name="diameter" src="d0_s2" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6274" name="number" name_original="number" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="variable" value_original="variable" />
      </biological_entity>
      <biological_entity id="o6275" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6276" name="bulbil" name_original="bulbils" src="d0_s2" type="structure" />
      <biological_entity id="o6277" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure" />
      <relation from="o6274" id="r905" name="part_of" negation="false" src="d0_s2" to="o6275" />
      <relation from="o6273" id="r906" modifier="frequently" name="producing" negation="false" src="d0_s2" to="o6276" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate at basal nodes, opposite distally, 6–27 × 7–42 cm;</text>
      <biological_entity id="o6278" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at basal nodes" constraintid="o6279" is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s3" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s3" to="27" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s3" to="42" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6279" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 6–20 cm, as long as or slightly longer than blade, base clasping, basal lobes stipulate, 1–8 mm wide;</text>
      <biological_entity id="o6280" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6282" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6281" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6283" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6284" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="stipulate" value_original="stipulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o6280" id="r907" name="as long as" negation="false" src="d0_s4" to="o6282" />
    </statement>
    <statement id="d0_s5">
      <text>blade 7–11-veined, reniform to somewhat deltate, glabrous, margins irregularly 3–5-lobed, apex conspicuously caudate.</text>
      <biological_entity id="o6285" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="7-11-veined" value_original="7-11-veined" />
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform to somewhat" value_original="reniform to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6286" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o6287" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s5" value="caudate" value_original="caudate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate inflorescences 1–2 (–4) per axil, spicate or terminally paniculate, cymose;</text>
      <biological_entity id="o6288" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="4" />
        <character char_type="range_value" constraint="per axil" constraintid="o6289" from="1" name="quantity" src="d0_s6" to="2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="spicate" value_original="spicate" />
        <character is_modifier="false" modifier="terminally" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
      </biological_entity>
      <biological_entity id="o6289" name="axil" name_original="axil" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>cymes subsessile, bearing (1–) 2–4 flowers subtended by ovate bracteoles, internodes ca. 1 cm;</text>
      <biological_entity id="o6290" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o6291" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s7" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o6292" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o6293" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6290" id="r908" name="bearing" negation="false" src="d0_s7" to="o6291" />
      <relation from="o6291" id="r909" name="subtended by" negation="false" src="d0_s7" to="o6292" />
    </statement>
    <statement id="d0_s8">
      <text>rachis 20–40 cm, subtended by ovate bracts ca. 1 mm.</text>
      <biological_entity id="o6294" name="rachis" name_original="rachis" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s8" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6295" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6294" id="r910" name="subtended by" negation="false" src="d0_s8" to="o6295" />
    </statement>
    <statement id="d0_s9">
      <text>Pistillate inflorescences 1–3 per axil, to 80 cm, internodes 2–2.5 cm.</text>
      <biological_entity id="o6296" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="per axil" constraintid="o6297" from="1" name="quantity" src="d0_s9" to="3" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s9" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6297" name="axil" name_original="axil" src="d0_s9" type="structure" />
      <biological_entity id="o6298" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: perianth yellowish;</text>
      <biological_entity id="o6299" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6300" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals in 2 similar whorls, narrowly spreading at anthesis, lanceolate, 3–6 mm;</text>
      <biological_entity id="o6301" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6302" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="narrowly" name="orientation" notes="" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6303" name="whorl" name_original="whorls" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o6302" id="r911" name="in" negation="false" src="d0_s11" to="o6303" />
    </statement>
    <statement id="d0_s12">
      <text>fertile stamens 6 in 2 subequal whorls;</text>
      <biological_entity id="o6304" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6305" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
        <character constraint="in whorls" constraintid="o6306" name="quantity" src="d0_s12" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o6306" name="whorl" name_original="whorls" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s12" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers less than ½ length of filaments, thecae distinct, not spreading.</text>
      <biological_entity id="o6307" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6308" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <biological_entity id="o6309" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="true" name="character" src="d0_s13" value="length" value_original="length" />
      </biological_entity>
      <biological_entity id="o6310" name="theca" name_original="thecae" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: perianth white with purple veins;</text>
      <biological_entity id="o6311" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6312" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character constraint="with veins" constraintid="o6313" is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o6313" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals as in staminate flowers;</text>
      <biological_entity id="o6314" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6315" name="tepal" name_original="tepals" src="d0_s15" type="structure" />
      <biological_entity id="o6316" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o6315" id="r912" name="in" negation="false" src="d0_s15" to="o6316" />
    </statement>
    <statement id="d0_s16">
      <text>staminodes 6, smaller than fertile stamens;</text>
      <biological_entity id="o6317" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6318" name="staminode" name_original="staminodes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
        <character constraint="than fertile stamens" constraintid="o6319" is_modifier="false" name="size" src="d0_s16" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o6319" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>style-branches irregularly 2-fid.</text>
      <biological_entity id="o6320" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6321" name="style-branch" name_original="style-branches" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules not reflexed at maturity, longer than wide.</text>
      <biological_entity id="o6322" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="orientation" src="d0_s18" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="length_or_size" src="d0_s18" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds bilaterally winged.</text>
      <biological_entity id="o6323" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Not known to flower in the flora area. Roadsides, hammocks, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="not known to flower" constraint="in the flora area" />
        <character name="habitat" value="the flora area" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; tropical Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="tropical Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Zanzibar yam</other_name>
  <discussion>Dioscorea sansibarensis, an ornamental species native to tropical Africa, is widely cultivated for its large, unusually shaped leaves. It has recently been reported growing amongst native vegetation in Miami-Dade and Collier counties in southern Florida. Axillary bulbils shed from mature plants were observed to be growing vigorously. Both the tubers and axillary bulbils of this species are toxic.</discussion>
  
</bio:treatment>