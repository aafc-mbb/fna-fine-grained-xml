<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="mention_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">509</other_info_on_meta>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="(Lindley) Szlachetko" date="1995" rank="subfamily">Vanilloideae</taxon_name>
    <taxon_name authority="Blume" date="1837" rank="tribe">Vanilleae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">Vanillinae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">vanilla</taxon_name>
    <taxon_name authority="Jackson" date="1808" rank="species">planifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Repos.</publication_title>
      <place_in_publication>8: plate 538. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe vanillinae;genus vanilla;species planifolia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102047</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myrobroma</taxon_name>
    <taxon_name authority="Salisbury" date="unknown" rank="species">fragrans</taxon_name>
    <taxon_hierarchy>genus Myrobroma;species fragrans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vanilla</taxon_name>
    <taxon_name authority="(Salisbury) Ames" date="unknown" rank="species">fragrans</taxon_name>
    <taxon_hierarchy>genus Vanilla;species fragrans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots usually 1 per node, aerial portions 2–3 mm diam.</text>
      <biological_entity id="o34682" name="root" name_original="roots" src="d0_s0" type="structure">
        <character constraint="per node" constraintid="o34683" name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o34683" name="node" name_original="node" src="d0_s0" type="structure" />
      <biological_entity id="o34684" name="portion" name_original="portions" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems occasionally branched, leafy, thick, 5–10 mm diam., smooth.</text>
      <biological_entity id="o34685" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent;</text>
      <biological_entity id="o34686" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade flat, oblongelliptic to ovate, longer than internodes, 15–25 × 5–8 cm, fleshy-leathery, apex acute to acuminate.</text>
      <biological_entity id="o34687" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s3" to="ovate" />
        <character constraint="than internodes" constraintid="o34688" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy-leathery" value_original="fleshy-leathery" />
      </biological_entity>
      <biological_entity id="o34688" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o34689" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 15-flowered racemes, short-pedunculate, to 5 cm excluding peduncle;</text>
      <biological_entity id="o34690" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o34691" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="15-flowered" value_original="15-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-pedunculate" value_original="short-pedunculate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34692" name="peduncle" name_original="peduncle" src="d0_s4" type="structure" />
      <relation from="o34691" id="r4662" name="excluding" negation="false" src="d0_s4" to="o34692" />
    </statement>
    <statement id="d0_s5">
      <text>floral bracts broadly-triangular-ovate, 7–10 × 7–10 cm, leathery.</text>
      <biological_entity constraint="floral" id="o34693" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="broadly-triangular-ovate" value_original="broadly-triangular-ovate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s5" to="10" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals and petals erect-spreading, yellow-green, fleshy, rigid;</text>
      <biological_entity id="o34694" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o34695" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s6" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o34696" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s6" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals oblanceolate, 3.5–5.5 × 1.1–1.3 cm, margins straight, apex acute to obtuse;</text>
      <biological_entity id="o34697" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o34698" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s7" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s7" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34699" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o34700" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals elliptic-oblanceolate, abaxially keeled, thinner than sepals, 3.5–5.5 × 1.1–1.3 cm, apex acute to obtuse;</text>
      <biological_entity id="o34701" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34702" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character constraint="than sepals" constraintid="o34703" is_modifier="false" name="width" src="d0_s8" value="thinner" value_original="thinner" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s8" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s8" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34703" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o34704" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip adnate to column for 1.5–2 cm, yellow-green, becoming dark yellow toward apex, lamina gulletlike, cuneate, rhomboid, 4–5 × ± 3 cm, with apical retuse lobule;</text>
      <biological_entity id="o34705" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34706" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character constraint="to column" constraintid="o34707" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="for 1.5-2 cm" name="coloration" notes="" src="d0_s9" value="yellow-green" value_original="yellow-green" />
        <character constraint="toward apex" constraintid="o34708" is_modifier="false" modifier="becoming" name="coloration" src="d0_s9" value="dark yellow" value_original="dark yellow" />
      </biological_entity>
      <biological_entity id="o34707" name="column" name_original="column" src="d0_s9" type="structure" />
      <biological_entity id="o34708" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o34709" name="lamina" name_original="lamina" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="gulletlike" value_original="gulletlike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboid" value_original="rhomboid" />
        <character name="area" src="d0_s9" unit="× moreorless 3 cm" value="3" value_original="3" />
        <character char_type="range_value" from="4" name="area" src="d0_s9" to="5" unit="× moreorless 3 cm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o34710" name="lobule" name_original="lobule" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="retuse" value_original="retuse" />
      </biological_entity>
      <relation from="o34709" id="r4663" name="with" negation="false" src="d0_s9" to="o34710" />
    </statement>
    <statement id="d0_s10">
      <text>disc with central tuft of retrorse scales, several lines of short, fleshy hairs extending to apex;</text>
      <biological_entity id="o34711" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34712" name="disc" name_original="disc" src="d0_s10" type="structure" />
      <biological_entity constraint="central" id="o34713" name="tuft" name_original="tuft" src="d0_s10" type="structure" />
      <biological_entity id="o34714" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity id="o34715" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o34716" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="true" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o34717" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <relation from="o34712" id="r4664" name="with" negation="false" src="d0_s10" to="o34713" />
      <relation from="o34713" id="r4665" name="part_of" negation="false" src="d0_s10" to="o34714" />
      <relation from="o34715" id="r4666" name="part_of" negation="false" src="d0_s10" to="o34716" />
      <relation from="o34715" id="r4667" name="extending to" negation="false" src="d0_s10" to="o34717" />
    </statement>
    <statement id="d0_s11">
      <text>column white, slender, 3–3.5 cm, margins slightly sinuate, adaxially bearded;</text>
      <biological_entity id="o34718" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34719" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s11" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34720" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollinia yellow;</text>
      <biological_entity id="o34721" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34722" name="pollinium" name_original="pollinia" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicellate ovary 3–5 cm.</text>
      <biological_entity id="o34723" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34724" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s13" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries cylindric, 15–25 × 0.8–1 cm.</text>
      <biological_entity id="o34725" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s14" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s14" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cypress swamps, hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cypress swamps" />
        <character name="habitat" value="hammocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies (Trinidad); Central America; n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Trinidad)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Vanilla</other_name>
  <other_name type="common_name">commercial vanilla</other_name>
  <other_name type="common_name">vanilla vine</other_name>
  <discussion>The long, slender black fruits of Vanilla planifolia are the vanilla “beans” of commerce.</discussion>
  <discussion>The natural distribution of Vanilla planifolia is most likely tropical evergreen forests of eastern Mexico and the Caribbean watersheds of Guatemala, Belize, and Honduras (M. Soto Arenas, pers. comm.). It has been cultivated and escaped or persisted in many areas of the tropics, including south Florida. It is known in the flora area from Long Pine Key, Everglades National Park, Miami, Miami-Dade County, Florida (P. M. Brown 2002).</discussion>
  <discussion>Pollinators are euglossine bees (J. D. Ackerman 1983), which do not occur in Florida. Natural pollination has been recorded in Florida, although very rarely (C. A. Luer 1972).</discussion>
  
</bio:treatment>