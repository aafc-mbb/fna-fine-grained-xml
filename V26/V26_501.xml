<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">nevii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 231. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species nevii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101379</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_name authority="(S. Watson) Ownbey &amp; Mingrone" date="unknown" rank="variety">nevii</taxon_name>
    <taxon_hierarchy>genus Allium;species douglasii;variety nevii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–5+, not clustered on stout, primary rhizome, ovoid, 1–2 × 1–2 cm;</text>
      <biological_entity id="o7363" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="5" upper_restricted="false" />
        <character constraint="on primary rhizome" constraintid="o7364" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o7364" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, light-brown, membranous, ± reticulate, cells quadrate to polygonal, vertically oblong, without fibers;</text>
      <biological_entity constraint="outer" id="o7365" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s1" to="polygonal" />
        <character is_modifier="false" modifier="vertically" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o7366" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" modifier="more or less" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o7367" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o7365" id="r1043" name="enclosing" negation="false" src="d0_s1" to="o7366" />
      <relation from="o7365" id="r1044" name="without" negation="false" src="d0_s1" to="o7367" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white or pink, cells obscure, quadrate.</text>
      <biological_entity constraint="inner" id="o7368" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o7369" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually persistent, withering from tip at anthesis, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o7370" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="from tip" constraintid="o7371" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o7371" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character modifier="at anthesis" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7372" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o7373" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o7374" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o7372" id="r1045" name="extending" negation="false" src="d0_s3" to="o7373" />
      <relation from="o7372" id="r1046" name="extending" negation="false" src="d0_s3" to="o7374" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat or ± channeled, falcate, 12–25 cm × 2–3 mm, margins entire.</text>
      <biological_entity id="o7375" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7376" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, 1–3, erect, solid, terete or somewhat flattened, 2-edged, not expanded proximal to inflorescence, 15–30 cm × 1–3 mm.</text>
      <biological_entity id="o7377" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-edged" value_original="2-edged" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s5" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" notes="" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7378" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact, 10–30-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o7379" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-30-flowered" value_original="10-30-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o7380" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2, 5–6-veined, ovate, ± equal, apex acute.</text>
      <biological_entity constraint="spathe" id="o7381" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-6-veined" value_original="5-6-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o7382" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate to substellate, 6–8 mm;</text>
      <biological_entity id="o7383" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="substellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals spreading, rose-colored, lanceolate, ± equal, becoming papery in fruit, margins entire, apex acuminate;</text>
      <biological_entity id="o7384" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose-colored" value_original="rose-colored" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o7385" is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o7385" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o7386" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7387" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens ± equaling tepals, or exserted;</text>
      <biological_entity id="o7388" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o7389" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers blue-gray;</text>
      <biological_entity id="o7390" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blue-gray" value_original="blue-gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen light blue or gray;</text>
      <biological_entity id="o7391" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light blue" value_original="light blue" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary distinctly crested;</text>
      <biological_entity id="o7392" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, 2 per lobe, low, rounded, margins entire;</text>
      <biological_entity id="o7393" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character constraint="per lobe" constraintid="o7394" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7394" name="lobe" name_original="lobe" src="d0_s14" type="structure" />
      <biological_entity id="o7395" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style exserted, linear;</text>
      <biological_entity id="o7396" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, unlobed;</text>
      <biological_entity id="o7397" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 8–12 mm.</text>
      <biological_entity id="o7398" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o7399" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o7400" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7401" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy, rocky soils, wet meadows, or along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>76.</number>
  
</bio:treatment>