<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>487, plate 38, figs. 8, 9. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101334</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs usually solitary, not clustered on stout primary rhizome, ± globose, 1–1.5 × 1.2–1.5 cm;</text>
      <biological_entity id="o1598" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character constraint="on primary rhizome" constraintid="o1599" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s0" value="globose" value_original="globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o1599" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing single bulb, dark-brown, prominently reticulate, membranous, cells irregularly arranged, vertically elongate, rectangular to ± contorted, without fibers;</text>
      <biological_entity constraint="outer" id="o1600" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="prominently" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o1601" name="bulb" name_original="bulb" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o1602" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="vertically" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s1" to="more or less contorted" />
      </biological_entity>
      <biological_entity id="o1603" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o1600" id="r215" name="enclosing" negation="false" src="d0_s1" to="o1601" />
      <relation from="o1602" id="r216" name="without" negation="false" src="d0_s1" to="o1603" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white, cells obscure, quadrate.</text>
      <biological_entity constraint="inner" id="o1604" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1605" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, green at anthesis, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o1606" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o1607" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o1608" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o1609" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o1607" id="r217" name="extending" negation="false" src="d0_s3" to="o1608" />
      <relation from="o1607" id="r218" name="extending" negation="false" src="d0_s3" to="o1609" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, subterete to channeled, 16–21 cm × 2–4 mm, margins entire.</text>
      <biological_entity id="o1610" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s4" to="channeled" />
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s4" to="21" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1611" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, solid, terete, 5–12 cm × 1–4 mm.</text>
      <biological_entity id="o1612" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, loose to ± compact, 10–25-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o1613" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="loose" name="architecture" src="d0_s6" to="more or less compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-25-flowered" value_original="10-25-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o1614" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2, 2–11-veined, lanceovate to ovate, ± equal, apex acute to acuminate.</text>
      <biological_entity constraint="spathe" id="o1615" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-11-veined" value_original="2-11-veined" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o1616" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate, (8–) 10–14 mm;</text>
      <biological_entity id="o1617" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, pink to reddish at tip and along midvein, otherwise white, lanceolate, ± equal, becoming papery and ± rigid in fruit, margins entire, apex acute;</text>
      <biological_entity id="o1618" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="at tip and along midvein" constraintid="o1619" from="pink" name="coloration" src="d0_s9" to="reddish" />
        <character is_modifier="false" modifier="otherwise" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character constraint="in fruit" constraintid="o1620" is_modifier="false" modifier="more or less" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o1619" name="midvein" name_original="midvein" src="d0_s9" type="structure" />
      <biological_entity id="o1620" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o1621" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1622" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o1623" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers purple;</text>
      <biological_entity id="o1624" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o1625" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crested;</text>
      <biological_entity id="o1626" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, prominent, flat, triangular, margins entire to coarsely toothed;</text>
      <biological_entity id="o1627" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o1628" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s14" to="coarsely toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o1629" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o1630" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, scarcely thickened, obscurely 3-lobed;</text>
      <biological_entity id="o1631" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 10–15 mm.</text>
      <biological_entity id="o1632" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s17" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat shining;</text>
      <biological_entity id="o1633" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o1634" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1635" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky, gravelly slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" modifier="open" />
        <character name="habitat" value="gravelly slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <discussion>Allium bigelovii is an anomalous species that does not appear to be closely related to any other from North America. With its prominent ovarian crests, a relationship with the group of species around A. fimbriatum, A. nevadense, and A. sanbornii is suggested. Allium bigelovii differs from this group, however, in having two leaves and a seed coat with smooth cells. In addition its bulb-coat reticulation is unlike that of any other North American species.</discussion>
  
</bio:treatment>