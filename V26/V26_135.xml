<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="V. G. Soukup" date="1980" rank="species">parviflorum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>32: 330, fig. 1. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species parviflorum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102003</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes horizontal to ± erect, brownish, thick, praemorse, not brittle.</text>
      <biological_entity id="o11027" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s0" to="more or less erect" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s0" value="praemorse" value_original="praemorse" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s0" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–3, round in cross-section, 1.7–3 dm, slender to robust, glabrous.</text>
      <biological_entity id="o11028" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character constraint="in cross-section" constraintid="o11029" is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character char_type="range_value" from="1.7" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="3" to_unit="dm" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="robust" value_original="robust" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11029" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts held well above ground, sessile;</text>
      <biological_entity id="o11030" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11031" name="ground" name_original="ground" src="d0_s2" type="structure" />
      <relation from="o11030" id="r1556" name="held" negation="false" src="d0_s2" to="o11031" />
    </statement>
    <statement id="d0_s3">
      <text>blade green with widely scattered mottling, mottling becoming obscure with age, ovate to broadly ovate, 6.5–16 × 5–8 cm, not glossy, apex obtuse.</text>
      <biological_entity id="o11032" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="with widely scattered mottling" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o11033" is_modifier="false" modifier="becoming" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="broadly ovate" />
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s3" to="16" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s3" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o11033" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o11034" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flower erect, odor spicy, of cloves;</text>
      <biological_entity id="o11035" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="odor" src="d0_s4" value="spicy" value_original="spicy" />
      </biological_entity>
      <biological_entity id="o11036" name="clove" name_original="cloves" src="d0_s4" type="structure" />
      <relation from="o11035" id="r1557" name="part_of" negation="false" src="d0_s4" to="o11036" />
    </statement>
    <statement id="d0_s5">
      <text>sepals displayed above bracts, spreading, divergent, green, lanceolate, 16–25 × 4–8 mm, margins entire, flat, apex variously obtuse to rounded;</text>
      <biological_entity id="o11037" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11038" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o11039" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o11040" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="variously obtuse" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <relation from="o11037" id="r1558" name="displayed above" negation="false" src="d0_s5" to="o11038" />
    </statement>
    <statement id="d0_s6">
      <text>petals long-lasting, erect to erect-spreading, ± connivent, ± concealing stamens and ovary, white, occasionally purple stained basally, sometimes weakly spirally twisted, linear to linearlanceolate, 2.2–4.5 × 0.4–1 cm, thin-textured, base occasionally cuneate, margins entire, apex obtuse;</text>
      <biological_entity id="o11041" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="long-lasting" value_original="long-lasting" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="erect-spreading" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o11042" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="occasionally; basally" name="coloration" src="d0_s6" value="purple stained" value_original="purple stained" />
        <character is_modifier="false" modifier="sometimes weakly spirally" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s6" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thin-textured" value_original="thin-textured" />
      </biological_entity>
      <biological_entity id="o11043" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="occasionally; basally" name="coloration" src="d0_s6" value="purple stained" value_original="purple stained" />
        <character is_modifier="false" modifier="sometimes weakly spirally" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s6" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thin-textured" value_original="thin-textured" />
      </biological_entity>
      <biological_entity id="o11044" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o11045" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11046" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens erect, 10–15.5 mm;</text>
      <biological_entity id="o11047" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments white, greenish white, or purple stained, 1–3 mm, much shorter than anthers, slender;</text>
      <biological_entity id="o11048" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple stained" value_original="purple stained" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple stained" value_original="purple stained" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character constraint="than anthers" constraintid="o11049" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o11049" name="anther" name_original="anthers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>anthers erect, straight, greenish white, 9–11+ mm, ± slender, dehiscence latrorse;</text>
      <biological_entity id="o11050" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="latrorse" value_original="latrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>connective greenish, straight, barely extended (to 0.4 mm) beyond anther sacs;</text>
      <biological_entity id="o11051" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="beyond anther sacs" constraintid="o11052" is_modifier="false" modifier="barely" name="size" src="d0_s10" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="anther" id="o11052" name="sac" name_original="sacs" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary green or green and purple basally, ovoid, obscurely 6-gonal, 4–8 mm;</text>
      <biological_entity id="o11053" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green and purple" value_original="green and purple" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s11" value="6-gonal" value_original="6-gonal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas erect, ± divergent, distinct, green, outer surface purple, subulate, 3–4.5 mm, ± fleshy, thickened.</text>
      <biological_entity id="o11054" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11055" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits dark reddish purple or maroon, fragrance not reported, subglobose, ± 1 cm, ± juicy.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 10.</text>
      <biological_entity id="o11056" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="maroon" value_original="maroon" />
        <character is_modifier="false" modifier="not" name="fragrance" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character modifier="more or less" name="some_measurement" src="d0_s13" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s13" value="juicy" value_original="juicy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11057" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (late Mar–early May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="late Mar-early May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mature fir (Abies), spruce (Picea), and hardwood forests in rich humus with mosses, open, somewhat grassy large groves of old oaks, with considerable underbrush and rather few herbaceous companions, in tangled, wet stream-bank alders (Alnus sp.), grasses, open clay hillside soils among shrubs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mature fir" />
        <character name="habitat" value="picea" modifier="spruce" />
        <character name="habitat" value="hardwood forests" modifier="and" constraint="in rich humus with mosses" />
        <character name="habitat" value="rich humus" constraint="with mosses" />
        <character name="habitat" value="mosses" />
        <character name="habitat" value="open" />
        <character name="habitat" value="grassy large groves" modifier="somewhat" constraint="of old oaks" />
        <character name="habitat" value="old oaks" />
        <character name="habitat" value="considerable underbrush" modifier="with" />
        <character name="habitat" value="few herbaceous companions" modifier="rather" />
        <character name="habitat" value="in tangled" />
        <character name="habitat" value="wet stream-bank" />
        <character name="habitat" value="alnus" modifier="alders" />
        <character name="habitat" value=")" />
        <character name="habitat" value="grasses" />
        <character name="habitat" value="clay hillside soils" modifier="open" constraint="among shrubs" />
        <character name="habitat" value="shrubs" modifier="among" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Small-flowered trillium</other_name>
  <discussion>Trillium parviflorum varies from very short, slender, small-bracted plants to tall, broad, umbrella-bracted giants. Regardless of plant or bract sizes, flower and petal sizes are remarkably constant, all plants having small, linear-lanceolate petals. This is not usually the case with T. albidum, the species with which this plant is most likely to be confused. In T. albidum also the plants can be enormous, but when they are, the petals are very long, broad, and conspicuously obovate-diamond-shaped. In large clonal clumps of T. albidum, the larger and more mature offsets show the typical petal shape, while the smaller and presumably youngest offsets sometimes produce smaller, narrower petals, more like those of T. parviflorum.</discussion>
  <discussion>Some western botanists, more experienced with local populations than I, do not consider Trillium parviflorum to be distinct from T. albidum. They point out that since there is an extensive region of apparent intergradation (as indeed there is, well supported by herbarium vouchers), there exists a morphological cline from the long- and wide-petaled T. albidum to the narrower- and generally shorter-petaled T. parviflorum, and that T. parviflorum, therefore, should not be considered a separate species but rather a subspecies or a variety. Since no one to date has treated T. parviflorum at the subspecific or varietal level, and since in my own limited experience it does appear as a distinct species in Washington north of the Columbia River, I include it here as treated by Soukup. I have seen populations of considerable variation north of Corvallis, Oregon, and agree that there is much overlap with T. albidum. Obviously there is need for a much more extensive study of this situation.</discussion>
  <discussion>A factor that exacerbates this problem (and many others in Trillium), is that nutrition, age, and even favorable position in the habitat can greatly influence plant and floral organ sizes. In many species, including T. albidum, when a single vigorous clonal clump produces many offsets, the oldest offsets may have flowers with very large petals, sepals, ovary, etc., while the younger offsets may have organs only half the size. In most sessile trilliums particularly, population averages are often more useful than isolated individual measurements, a difficult situation, indeed.</discussion>
  
</bio:treatment>