<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="treatment_page">273</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Eastwood" date="1934" rank="species">yosemitense</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>1: 132. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species yosemitense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101416</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–12+, not basally clustered on stout primary rhizome, ovoid, 2–3 × 1.5–2 cm;</text>
      <biological_entity id="o7127" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="12" upper_restricted="false" />
        <character constraint="on primary rhizome" constraintid="o7128" is_modifier="false" modifier="not basally" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s0" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o7128" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, brown, membranous, lacking cellular reticulation or cells arranged in only 2–3 rows distal to roots, ± quadrate, without fibers;</text>
      <biological_entity constraint="outer" id="o7129" name="coat" name_original="coats" src="d0_s1" type="structure" />
      <biological_entity id="o7130" name="reticulation" name_original="reticulation" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="cellular" value_original="cellular" />
      </biological_entity>
      <biological_entity id="o7131" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character constraint="in rows" constraintid="o7132" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o7132" name="row" name_original="rows" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" modifier="only" name="quantity" src="d0_s1" to="3" />
        <character constraint="to roots" constraintid="o7133" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o7133" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o7134" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o7129" id="r1014" name="enclosing" negation="false" src="d0_s1" to="o7130" />
      <relation from="o7131" id="r1015" name="without" negation="false" src="d0_s1" to="o7134" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white, cells very obscurely quadrate or not visible.</text>
      <biological_entity constraint="inner" id="o7135" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7136" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="very obscurely" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="not; not" name="prominence" src="d0_s2" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually deciduous with scape, withering from tip at anthesis, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o7137" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with scape" constraintid="o7138" is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character constraint="from tip" constraintid="o7139" is_modifier="false" name="life_cycle" notes="" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o7138" name="scape" name_original="scape" src="d0_s3" type="structure" />
      <biological_entity id="o7139" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character modifier="at anthesis" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7140" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o7141" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o7142" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o7140" id="r1016" name="extending" negation="false" src="d0_s3" to="o7141" />
      <relation from="o7140" id="r1017" name="extending" negation="false" src="d0_s3" to="o7142" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat or very broadly channeled, ± falcate, 15–40 cm × 2–18 mm, margins entire.</text>
      <biological_entity id="o7143" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7144" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape usually forming abcission layer and deciduous with leaves after seeds mature, frequently breaking at this level after pressing, solitary, erect, solid, terete, 6–23 cm × 1–3 mm.</text>
      <biological_entity id="o7145" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character constraint="with leaves" constraintid="o7148" is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="frequently" name="architecture_or_arrangement_or_growth_form" notes="" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="23" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7146" name="abcission" name_original="abcission" src="d0_s5" type="structure" />
      <biological_entity id="o7147" name="layer" name_original="layer" src="d0_s5" type="structure" />
      <biological_entity id="o7148" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7149" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o7150" name="pressing" name_original="pressing" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s5" value="level" value_original="level" />
      </biological_entity>
      <relation from="o7145" id="r1018" name="forming" negation="false" src="d0_s5" to="o7146" />
      <relation from="o7145" id="r1019" name="forming" negation="false" src="d0_s5" to="o7147" />
      <relation from="o7148" id="r1020" name="after" negation="false" src="d0_s5" to="o7149" />
      <relation from="o7145" id="r1021" name="breaking at" negation="false" src="d0_s5" to="o7150" />
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact, 20–100-flowered, globose to hemispheric, bulbils unknown;</text>
      <biological_entity id="o7151" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="20-100-flowered" value_original="20-100-flowered" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="hemispheric" />
      </biological_entity>
      <biological_entity id="o7152" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–4, 7–9-veined, ovate, ± equal, apex acuminate.</text>
      <biological_entity constraint="spathe" id="o7153" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="7-9-veined" value_original="7-9-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o7154" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate, 7–15 mm;</text>
      <biological_entity id="o7155" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, white to pink with darker midveins, linear-oblong, ± equal, becoming membranous in fruit, margins entire, apex acute;</text>
      <biological_entity id="o7156" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="with midveins" constraintid="o7157" from="white" name="coloration" src="d0_s9" to="pink" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o7158" is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7157" name="midvein" name_original="midveins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o7158" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o7159" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7160" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens ± equaling tepals;</text>
      <biological_entity id="o7161" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o7162" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow or purple;</text>
      <biological_entity id="o7163" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o7164" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crested;</text>
      <biological_entity id="o7165" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 3, 2-lobed, minute, margins entire;</text>
      <biological_entity id="o7166" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="size" src="d0_s14" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o7167" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style short-exserted, linear;</text>
      <biological_entity id="o7168" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="short-exserted" value_original="short-exserted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, scarcely thickened, unlobed;</text>
      <biological_entity id="o7169" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 7–34 mm.</text>
      <biological_entity id="o7170" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s17" to="34" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o7171" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells ± smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o7172" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7173" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soil along cracks and margins of large metamorphic outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soil" constraint="along cracks and margins" />
        <character name="habitat" value="cracks" />
        <character name="habitat" value="margins" />
        <character name="habitat" value="large metamorphic outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>88.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Allium yosemitense is known only from the central Sierra Nevada.</discussion>
  
</bio:treatment>