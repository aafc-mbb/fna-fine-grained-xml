<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Neottieae</taxon_name>
    <taxon_name authority="Bentham" date="" rank="subtribe">Limodorinae</taxon_name>
    <taxon_name authority="Zinn" date="unknown" rank="genus">epipactis</taxon_name>
    <taxon_name authority="(Hoffmann ex Bernhardi) Besser" date="1809" rank="species">atrorubens</taxon_name>
    <place_of_publication>
      <publication_title>Prim. Fl. Galiciae Austriac.</publication_title>
      <place_in_publication>2: 220. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe neottieae;subtribe limodorinae;genus epipactis;species atrorubens;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242101584</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Serapias</taxon_name>
    <taxon_name authority="Hoffmann ex Bernhardi" date="unknown" rank="species">atrorubens</taxon_name>
    <place_of_publication>
      <publication_title>in G. F. Hoffmann, Deutchl. Fl.</publication_title>
      <place_in_publication>4: 182. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Serapias;species atrorubens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epipactis</taxon_name>
    <taxon_name authority="(Linnaeus) Crantz" date="unknown" rank="species">helleborine</taxon_name>
    <taxon_name authority="(Hoffmann ex Bernhardi) Syme" date="unknown" rank="subspecies">atrorubens</taxon_name>
    <taxon_hierarchy>genus Epipactis;species helleborine;subspecies atrorubens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epipactis</taxon_name>
    <taxon_name authority="(Crantz) W. D. J. Koch" date="unknown" rank="species">rubiginosa</taxon_name>
    <taxon_hierarchy>genus Epipactis;species rubiginosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–60 (–106) cm, sparsely hairy.</text>
      <biological_entity id="o20315" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="106" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5–10;</text>
      <biological_entity id="o20316" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade orbiculate, elliptic, or narrowly lanceolate, 4–10 × 1.5–4.5 cm.</text>
      <biological_entity id="o20317" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences lax racemes, usually secund;</text>
      <biological_entity id="o20318" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s3" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o20319" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral bracts lanceolate, 10–35 mm, often exceeding flowers.</text>
      <biological_entity constraint="floral" id="o20320" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20321" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <relation from="o20320" id="r2780" modifier="often" name="exceeding" negation="false" src="d0_s4" to="o20321" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 8–18, dark wine to cranberry red, small;</text>
      <biological_entity id="o20322" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="18" />
        <character char_type="range_value" from="dark wine" name="coloration" src="d0_s5" to="cranberry red" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals dark wine red, rarely dull rose or greenish abaxially;</text>
      <biological_entity id="o20323" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark wine" value_original="dark wine" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose" value_original="rose" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals 6–7 mm;</text>
      <biological_entity constraint="lateral" id="o20324" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals similar to sepals in color, ovate, 4–7 × 2–4.5 mm;</text>
      <biological_entity id="o20325" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20326" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <relation from="o20325" id="r2781" name="to" negation="false" src="d0_s8" to="o20326" />
    </statement>
    <statement id="d0_s9">
      <text>lip indistinctly veined, 5.5–6.5 mm, constricted at middle into 2 parts, proximal part greenish with red edges, deeply concave, adaxial surface spotted with violet-red or brown, distal part brighter and darker, broadly triangular or transversely elliptic, minutely toothed with small reflexed tip;</text>
      <biological_entity id="o20327" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
        <character constraint="at middle part" constraintid="o20328" is_modifier="false" name="size" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity constraint="middle" id="o20328" name="part" name_original="part" src="d0_s9" type="structure" />
      <biological_entity id="o20329" name="part" name_original="parts" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20330" name="part" name_original="part" src="d0_s9" type="structure">
        <character constraint="with edges" constraintid="o20331" is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="deeply" name="shape" notes="" src="d0_s9" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o20331" name="edge" name_original="edges" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20332" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="spotted with violet-red or spotted with brown" />
        <character name="coloration" src="d0_s9" value="," value_original="," />
      </biological_entity>
      <biological_entity constraint="distal" id="o20333" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s9" value="brighter" value_original="brighter" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="darker" value_original="darker" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character constraint="with tip" constraintid="o20334" is_modifier="false" modifier="minutely" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o20334" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="small" value_original="small" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <relation from="o20328" id="r2782" name="into" negation="false" src="d0_s9" to="o20329" />
    </statement>
    <statement id="d0_s10">
      <text>calli 2, brownish, brighter, darker in color than lip, rugose;</text>
      <biological_entity id="o20335" name="callus" name_original="calli" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="brighter" value_original="brighter" />
        <character constraint="in lip" constraintid="o20337" is_modifier="false" name="coloration" src="d0_s10" value="darker" value_original="darker" />
        <character is_modifier="false" name="relief" notes="" src="d0_s10" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o20337" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="true" name="character" src="d0_s10" value="color" value_original="color" />
      </biological_entity>
      <biological_entity id="o20336" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="true" name="character" src="d0_s10" value="color" value_original="color" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>column very short, 2–3 mm;</text>
      <biological_entity id="o20338" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther yellow;</text>
      <biological_entity id="o20339" name="anther" name_original="anther" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollinia 2 pairs, yellow, mealy;</text>
      <biological_entity id="o20340" name="pollinium" name_original="pollinia" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="mealy" value_original="mealy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary tomentose.</text>
      <biological_entity id="o20341" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid, 7–9 mm, moderately to densely tomentose.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 40.</text>
      <biological_entity id="o20342" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s15" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20343" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Partial shade in abandoned serpentine/asbestos quarry</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="partial shade" constraint="in abandoned serpentine\/asbestos" />
        <character name="habitat" value="abandoned serpentine\/asbestos" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Vt.; Europe; Asia (Iran).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Iran)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Dark red helleborine</other_name>
  <discussion>Epipactis atrorubens is apparently naturalized in Vermont where a small population persists (P. M. Brown 1997).</discussion>
  
</bio:treatment>