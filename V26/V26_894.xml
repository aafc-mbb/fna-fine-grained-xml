<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="mention_page">432</other_info_on_meta>
    <other_info_on_meta type="mention_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Engelmann ex Trelease" date="1902" rank="species">angustissima</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>13: 58, plate 23, fig. 1, plate 24, fig.1, plate 83, fig. 6, plate 93, fig. 1. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species angustissima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102054</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or forming small to large colonies of rosettes, acaulescent or rarely caulescent, to 3 m diam.;</text>
      <biological_entity id="o32111" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="forming small to large colonies" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o32112" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <relation from="o32111" id="r4322" name="part_of" negation="false" src="d0_s0" to="o32112" />
    </statement>
    <statement id="d0_s1">
      <text>rosettes usually small.</text>
      <biological_entity id="o32113" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems procumbent, 0.1–0.4 m, or erect, 1–2 m.</text>
      <biological_entity id="o32114" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s2" to="0.4" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s2" to="2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade rigidly spreading, including distal leaves, linear, lanceolate, concavo-convex, or plano-keeled, widest near middle, 20–80 (–150) × 0.4–2 cm, rigid or flexible, not glaucous, margins entire, becoming filiferous, white, becoming brownish, gray, or green, apex long-acuminate, spinose, spine acicular, short, 3–7 mm.</text>
      <biological_entity id="o32115" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rigidly" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concavo-convex" value_original="concavo-convex" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plano-keeled" value_original="plano-keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concavo-convex" value_original="concavo-convex" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plano-keeled" value_original="plano-keeled" />
        <character constraint="near middle leaves" constraintid="o32117" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s3" to="150" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" notes="" src="d0_s3" to="80" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32116" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="middle" id="o32117" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" notes="" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32118" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="becoming" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o32119" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
      </biological_entity>
      <biological_entity id="o32120" name="spine" name_original="spine" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o32115" id="r4323" name="including" negation="false" src="d0_s3" to="o32116" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemose, occasionally paniculate proximally, arising well beyond rosettes, (4–) 8–20 dm, glabrous or finely pubescent;</text>
      <biological_entity id="o32121" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="occasionally; proximally" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character constraint="beyond rosettes" constraintid="o32122" is_modifier="false" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="4" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="8" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="8" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="20" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o32122" name="rosette" name_original="rosettes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>branches, when present, 0.5–1 dm;</text>
      <biological_entity id="o32123" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" modifier="when present" name="some_measurement" src="d0_s5" to="1" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts erect, linear, proximal 10–20 × 1–2 cm, distal 3–8 × 1–2 cm;</text>
      <biological_entity id="o32124" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o32125" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32126" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle scapelike, 0.3–2.5 m, 1–2 cm diam.</text>
      <biological_entity id="o32127" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s7" to="2.5" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent, 3–5.5 (–6.5) cm;</text>
      <biological_entity id="o32128" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth campanulate to globose;</text>
      <biological_entity id="o32129" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s9" to="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals distinct, white to cream or greenish white, often tinged pink or brown, broad to narrowly elliptic to lanceolate-elliptic or orbiculate, 3–6.5 × 1.3–2.5 cm;</text>
      <biological_entity id="o32130" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="cream or greenish white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="tinged pink" value_original="tinged pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s10" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 0.7–2.8 cm;</text>
      <biological_entity id="o32131" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s11" to="2.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3.2–6 mm;</text>
      <biological_entity id="o32132" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistil 1.5–3.5 cm;</text>
      <biological_entity id="o32133" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s13" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style white to pale green, 3–10 (–13) mm;</text>
      <biological_entity id="o32134" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pale green" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas lobed.</text>
      <biological_entity id="o32135" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits erect, capsular, dehiscent, moderately to deeply constricted, 3.5–7.5 × 2–3 cm, dehiscence septicidal.</text>
      <biological_entity id="o32136" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="moderately to deeply" name="size" src="d0_s16" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="dehiscence" src="d0_s16" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="dehiscence" src="d0_s16" to="3" to_unit="cm" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds dull black, thin, 7–9 × 5–7 mm.</text>
      <biological_entity id="o32137" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s17" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s17" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s17" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Our treatment of Yucca angustissima reflects the concepts of J. L. Reveal (1977c). Each variety is well isolated geographically, but they overlap with one another morphologically. S. L. Welsh et al. (1993) treated the taxa in this complex at species level, with the exception of Y. angustissima var. aria, considered a high-altitude extreme of the typical variety. K. H. Clary (1997) presented DNA evidence that supports Welsh et al.’s treatment of this complex, in that Y. angustissima, Y. kanabensis, and Y. toftiae sort out distinctly from one another in her consensus tree. However, Welsh et al. indicated significant intergradation among these taxa, which makes their recognition at varietal rank seem most appropriate.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules deeply constricted, 3.5–5.5(–5.7) cm; inflorescence and peduncle 0.8–1.5(–1.8) m.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules moderately constricted, 4.5–7.5 cm; inflorescence and peduncle 1–2.5(–4.5) m.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 4.5–5.5 cm; leaf blade 25–45 cm; style 10–13 mm.</description>
      <determination>18a Yucca angustissima var. angustissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 3.5–4.5(–5.2) cm; leaf blade 40–60 cm; style 7–10 mm.</description>
      <determination>18b Yucca angustissima var. avia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers 3–5.2 cm; leaf blade 20–75 cm; pistil 1.5–3.2 cm; style 3–10 mm; stem plus peduncle and inflorescence 2.3–4.5 m.</description>
      <determination>18c Yucca angustissima var. toftiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers 5.5–6.5 cm; leaf blade 45–80(–150) cm; pistil 3–3.5 cm; style 5–8 mm; stem plus peduncle and inflorescence 2–3.5 m</description>
      <determination>18d Yucca angustissima var. kanabensis</determination>
    </key_statement>
  </key>
</bio:treatment>