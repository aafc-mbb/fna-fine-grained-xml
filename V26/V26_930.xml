<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">451</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Engelmann in S. Watson" date="unknown" rank="species">utahensis</taxon_name>
    <taxon_name authority="(McKelvey) Gentry" date="1982" rank="subspecies">kaibabensis</taxon_name>
    <place_of_publication>
      <publication_title>Agaves Continental N. Amer.,</publication_title>
      <place_in_publication>259. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species utahensis;subspecies kaibabensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agave</taxon_name>
    <taxon_name authority="McKelvey" date="unknown" rank="species">kaibabensis</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>30: 227, plate 1. 1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Agave;species kaibabensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agave</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">utahensis</taxon_name>
    <taxon_name authority="(McKelvey) Breitung" date="unknown" rank="variety">kaibabensis</taxon_name>
    <taxon_hierarchy>genus Agave;species utahensis;variety kaibabensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rosettes (2–) 3–6 × 4–10 dm.</text>
      <biological_entity id="o14135" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_length" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves (25–) 27–50 × 3–5.5 cm;</text>
      <biological_entity id="o14136" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="atypical_length" src="d0_s1" to="27" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="27" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate;</text>
      <biological_entity id="o14137" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>marginal teeth (2–) 3–5 mm, 2–4 cm apart;</text>
      <biological_entity constraint="marginal" id="o14138" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apical spine stout, subulate, 2–4 cm.</text>
      <biological_entity constraint="apical" id="o14139" name="spine" name_original="spine" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape 3.5–5 m.</text>
      <biological_entity id="o14140" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="m" name="some_measurement" src="d0_s5" to="5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences narrowly paniculate;</text>
      <biological_entity id="o14141" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>longer lateral branches (1–) 4–10 cm.</text>
      <biological_entity constraint="longer lateral" id="o14142" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 4–12 per cluster, (2.6–) 2.9–4.3 cm;</text>
      <biological_entity id="o14143" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per cluster , (2.6-)2.9-4.3 cm" from="4" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1.5–2.9 cm. 2n = 60.</text>
      <biological_entity id="o14144" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2.9" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14145" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous or sandstone outcrops in desert scrub, pinyon-juniper, or adjacent conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="adjacent conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9a</number>
  <other_name type="common_name">Kaibab agave</other_name>
  
</bio:treatment>