<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Gentry" date="1970" rank="species">mckelveyana</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>42: 225, figs. 4–6. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species mckelveyana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101310</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent, solitary or sparsely suckering;</text>
      <biological_entity id="o14679" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sparsely" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes 2–4.5 × 3–6 dm, rather open.</text>
      <biological_entity id="o14680" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s1" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s1" to="6" to_unit="dm" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s1" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading, broadest at middle, 17.5–40 × 2.8–5 cm;</text>
      <biological_entity id="o14681" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character constraint="at middle leaves" constraintid="o14682" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
        <character char_type="range_value" from="17.5" from_unit="cm" name="length" notes="" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o14682" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.8" from_unit="cm" name="width" notes="" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light glaucous green or yellowish green to dark green, usually cross-zoned, linearlanceolate to lanceolate, rigid, adaxially concave, abaxially convex;</text>
      <biological_entity id="o14683" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s3" to="dark green" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="cross-zoned" value_original="cross-zoned" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins straight or undulate to crenate, armed, teeth single, firmly attached, well defined, 4–8 mm, 1–3 cm apart;</text>
      <biological_entity id="o14684" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character name="course" src="d0_s4" value="undulate to crenate" value_original="undulate to crenate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o14685" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" modifier="firmly" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s4" value="defined" value_original="defined" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apical spine reddish-brown to gray, subulate, 1.5–4 cm.</text>
      <biological_entity constraint="apical" id="o14686" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s5" to="gray" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 2–5 m.</text>
      <biological_entity id="o14687" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s6" to="5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, not bulbiferous, open;</text>
      <biological_entity id="o14688" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="bulbiferous" value_original="bulbiferous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, triangular, 0.5–2 cm;</text>
      <biological_entity id="o14689" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 10–19, ascending, comprising distal (1/4–) 1/3–1/2 of inflorescence, longer than 10 cm.</text>
      <biological_entity constraint="lateral" id="o14690" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="19" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o14691" name="inflorescence" name_original="inflorescence" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s9" to="1/3-1/2" />
      </biological_entity>
      <relation from="o14690" id="r2017" name="comprising" negation="false" src="d0_s9" to="o14691" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers 6–12 per cluster, erect, 3–4 cm;</text>
      <biological_entity id="o14692" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per cluster" from="6" name="quantity" src="d0_s10" to="12" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth light yellow, tube shallow, broadly campanulate, 2–4.5 × 8–10.5 (–18) mm, limb lobes wilting soon after anthesis, spreading, equal or subequal, 11–13 mm;</text>
      <biological_entity id="o14693" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light yellow" value_original="light yellow" />
      </biological_entity>
      <biological_entity id="o14694" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character is_modifier="false" name="depth" src="d0_s11" value="shallow" value_original="shallow" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="10.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="18" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o14695" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="life_cycle" src="d0_s11" value="wilting" value_original="wilting" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens long-exserted;</text>
      <biological_entity id="o14696" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted near or at top of perianth-tube, erect, yellow, 2.5–3.3 cm;</text>
      <biological_entity id="o14697" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o14698" name="perianth-tube" name_original="perianth-tube" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="position" src="d0_s13" value="top" value_original="top" />
      </biological_entity>
      <relation from="o14697" id="r2018" name="inserted near or at" negation="false" src="d0_s13" to="o14698" />
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, (7–) 9–16 mm;</text>
      <biological_entity id="o14699" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1.6–2.2 cm, neck slightly constricted, 1–2.5 mm.</text>
      <biological_entity id="o14700" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" src="d0_s15" to="2.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14701" name="neck" name_original="neck" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules short-pedicellate, narrowly oblong to oblong, 3–4.5 cm, apex beaked.</text>
      <biological_entity id="o14702" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="short-pedicellate" value_original="short-pedicellate" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s16" to="oblong" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s16" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14703" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 5–6.5 mm.</text>
      <biological_entity id="o14704" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly or rocky places with desert scrub, chaparral and pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" constraint="with desert scrub" />
        <character name="habitat" value="sandy to rocky places" constraint="with desert scrub" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">McKelvey agave</other_name>
  <discussion>Agave mckelveyana may hybridize with A. deserti var. simplex and A. utahensis var. utahensis in Arizona.</discussion>
  
</bio:treatment>