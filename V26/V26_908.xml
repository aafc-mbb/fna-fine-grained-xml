<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="mention_page">439</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="McKelvey" date="1947" rank="species">campestris</taxon_name>
    <place_of_publication>
      <publication_title>Yuccas Southw. U.S.</publication_title>
      <place_in_publication>2: 173, plates 62, 63. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species campestris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102058</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming small or large, open colonies, acaulescent or occasionally caulescent and arborescent, rhizomatous;</text>
      <biological_entity id="o5566" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5567" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o5566" id="r811" name="forming" negation="false" src="d0_s0" to="o5567" />
    </statement>
    <statement id="d0_s1">
      <text>rosettes usually small.</text>
      <biological_entity id="o5568" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0.6–1 m.</text>
      <biological_entity id="o5569" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.6" from_unit="m" name="some_measurement" src="d0_s2" to="1" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade linear, planoconvex or plano-keeled, widest near middle, 40–65 × 0.3–0.7 (–1.5) cm, rigid, margins entire, filiferous, white, apex spinose, spine acicular, 7 mm.</text>
      <biological_entity id="o5570" name="blade-leaf" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plano-keeled" value_original="plano-keeled" />
        <character constraint="near margins, apex, spine" constraintid="o5571, o5572, o5573" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character name="distance" notes="" src="d0_s3" unit="mm" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o5571" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="middle" value_original="middle" />
        <character char_type="range_value" from="40" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="65" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acicular" value_original="acicular" />
      </biological_entity>
      <biological_entity id="o5572" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="middle" value_original="middle" />
        <character char_type="range_value" from="40" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="65" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acicular" value_original="acicular" />
      </biological_entity>
      <biological_entity id="o5573" name="spine" name_original="spine" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="middle" value_original="middle" />
        <character char_type="range_value" from="40" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="65" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acicular" value_original="acicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences paniculate, arising within or occasionally beyond rosettes, narrowly ellipsoid, 6–10 dm, distance from leaf tips to proximal inflorescence branches less than twice leaf length when fully expanded, glabrous;</text>
      <biological_entity id="o5574" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character constraint="beyond rosettes" constraintid="o5575" is_modifier="false" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s4" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s4" to="10" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o5575" name="rosette" name_original="rosettes" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o5576" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity constraint="inflorescence" id="o5577" name="branch" name_original="branches" src="d0_s4" type="structure" constraint_original="proximal inflorescence">
        <character constraint="leaf" constraintid="o5578" is_modifier="false" modifier="when fully expanded" name="length" src="d0_s4" value="0-2 times leaf length" value_original="0-2 times leaf length" />
      </biological_entity>
      <biological_entity id="o5578" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o5574" id="r812" name="from" negation="false" src="d0_s4" to="o5576" />
      <relation from="o5576" id="r813" name="to" negation="false" src="d0_s4" to="o5577" />
    </statement>
    <statement id="d0_s5">
      <text>branches to 13 cm;</text>
      <biological_entity id="o5579" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts erect;</text>
      <biological_entity id="o5580" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle scapelike, 0.5–1 m, less than 2.5 cm diam.</text>
      <biological_entity id="o5581" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s7" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent;</text>
      <biological_entity id="o5582" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth globose;</text>
      <biological_entity id="o5583" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals connate, dull green, sometimes tinged pink, 4.1–6.5 × 1.5–2.5 cm;</text>
      <biological_entity id="o5584" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="tinged pink" value_original="tinged pink" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" src="d0_s10" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments shorter than pistil, flaccid;</text>
      <biological_entity id="o5585" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character constraint="than pistil" constraintid="o5586" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="texture" src="d0_s11" value="flaccid" value_original="flaccid" />
      </biological_entity>
      <biological_entity id="o5586" name="pistil" name_original="pistil" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers 3.2 mm;</text>
      <biological_entity id="o5587" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3.2" value_original="3.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistil ovoid to obovoid, 2.5–3 × 0.5–0.9 cm;</text>
      <biological_entity id="o5588" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="obovoid" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s13" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s13" to="0.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style bright green;</text>
      <biological_entity id="o5589" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright green" value_original="bright green" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas lobed.</text>
      <biological_entity id="o5590" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits erect, capsular, dehiscent, symmetrical or rarely constricted, 4.5–5.5 (–6.3) × 3–5 cm, dehiscence septicidal.</text>
      <biological_entity id="o5591" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="symmetrical" value_original="symmetrical" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s16" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="dehiscence" src="d0_s16" to="6.3" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="dehiscence" src="d0_s16" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="dehiscence" src="d0_s16" to="5" to_unit="cm" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds glossy black, thin, 11–14 × 8–11 mm.</text>
      <biological_entity id="o5592" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s17" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s17" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s17" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep sands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep sands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Plains yucca</other_name>
  <discussion>Yucca campestris is endemic to the plains region in the southern panhandle counties of western Texas. S. D. McKelvey (1938–1947) discussed its variation in relation to its distribution, and K. H. Clary’s (1997) DNA studies support its recognition as a distinct species. J. M. Webber (1953) considered Y. campestris to be a hybrid between Y. constricta and Y. elata, and reported a distribution from west Texas into southern New Mexico, and possibly into northwestern New Mexico. Additional study within these regions may help resolve the relationships and origin of this species.</discussion>
  
</bio:treatment>