<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech,Shoichi Kawano</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="treatment_page">147</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">UVULARIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 304. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 144. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus UVULARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin uvula, alluding to the flowers hanging like that organ, and to formerly supposed efficacy in treating diseases of it</other_info_on_name>
    <other_info_on_name type="fna_id">134281</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Oakesia S. Watson" date="unknown" rank="genus">Oakesia</taxon_name>
    <taxon_hierarchy>genus Oakesia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Oakesiella Small" date="unknown" rank="genus">Oakesiella</taxon_name>
    <taxon_hierarchy>genus Oakesiella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, caulescent, glabrous or pubescent, from short or elongate rhizomes bearing several fibrous or thickened roots.</text>
      <biological_entity id="o7846" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7847" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o7848" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="several" value_original="several" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character is_modifier="true" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o7846" id="r1119" name="from" negation="false" src="d0_s0" to="o7847" />
      <relation from="o7847" id="r1120" name="bearing" negation="false" src="d0_s0" to="o7848" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or 1-branched, excluding flower-bearing branches, strongly angled or rounded, with sheathing, papery bracts proximally.</text>
      <biological_entity id="o7849" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-branched" value_original="1-branched" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7850" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="flower-bearing" value_original="flower-bearing" />
      </biological_entity>
      <biological_entity id="o7851" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
        <character is_modifier="true" name="texture" src="d0_s1" value="papery" value_original="papery" />
      </biological_entity>
      <relation from="o7849" id="r1121" name="excluding" negation="false" src="d0_s1" to="o7850" />
      <relation from="o7849" id="r1122" name="with" negation="false" src="d0_s1" to="o7851" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, sessile or perfoliate;</text>
      <biological_entity id="o7852" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="perfoliate" value_original="perfoliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong-linear to oblong-ovate, membranaceous to leathery.</text>
      <biological_entity id="o7853" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-linear" name="shape" src="d0_s3" to="oblong-ovate" />
        <character char_type="range_value" from="membranaceous" name="texture" src="d0_s3" to="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1 per branch, terminal but appearing axillary;</text>
      <biological_entity id="o7854" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="per branch" constraintid="o7855" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o7855" name="branch" name_original="branch" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>peduncles pendent.</text>
      <biological_entity id="o7856" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth narrowly campanulate;</text>
      <biological_entity id="o7857" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7858" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals promptly deciduous, imbricate, distinct, linear to narrowly oblong, apex obtuse or acute, nectariferous;</text>
      <biological_entity id="o7859" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7860" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="promptly" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="narrowly oblong" />
      </biological_entity>
      <biological_entity id="o7861" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="function" src="d0_s7" value="nectariferous" value_original="nectariferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens distinct to weakly connate at perianth base;</text>
      <biological_entity id="o7862" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7863" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="at perianth base" constraintid="o7864" from="distinct" name="fusion" src="d0_s8" to="weakly connate" />
      </biological_entity>
      <biological_entity constraint="perianth" id="o7864" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>filaments dimorphic, glabrous;</text>
      <biological_entity id="o7865" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7866" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers linear-oblong, extrorse;</text>
      <biological_entity id="o7867" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7868" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s10" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>connectives present;</text>
      <biological_entity id="o7869" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7870" name="connectif" name_original="connectives" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, 3-locular, sessile or stipitate, rounded to sharply triangular;</text>
      <biological_entity id="o7871" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7872" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s12" to="sharply triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1, 3-lobed;</text>
      <biological_entity id="o7873" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7874" name="style" name_original="style" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas 3, lobed, outwardly arching at maturity.</text>
      <biological_entity id="o7875" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7876" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lobed" value_original="lobed" />
        <character constraint="at maturity" is_modifier="false" modifier="outwardly" name="orientation" src="d0_s14" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsular, greenish to yellowish-brown, sessile or stipitate, leathery or submembranaceous, tardily dehiscent, dehiscence loculicidal.</text>
      <biological_entity id="o7877" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s15" to="yellowish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="texture" src="d0_s15" value="submembranaceous" value_original="submembranaceous" />
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–3 per locule, brownish red, globose to ovoid;</text>
      <biological_entity id="o7878" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o7879" from="1" name="quantity" src="d0_s16" to="3" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s16" value="brownish red" value_original="brownish red" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="ovoid" />
      </biological_entity>
      <biological_entity id="o7879" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>arils various.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 6, 7.</text>
      <biological_entity id="o7880" name="aril" name_original="arils" src="d0_s17" type="structure">
        <character is_modifier="false" name="variability" src="d0_s17" value="various" value_original="various" />
      </biological_entity>
      <biological_entity constraint="x" id="o7881" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="6" value_original="6" />
        <character name="quantity" src="d0_s18" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Bellwort</other_name>
  <other_name type="common_name">merrybells</other_name>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>Uvularia, a genus of attractive, spring-blooming plants, has been divided into sect. Oakesiella (Small) Wilbur with sessile leaves and sect. Uvularia with perfoliate leaves. Differences in other morphological (R. L. Wilbur 1963) and cytological (F. H. Utech 1978d) characters support recognition of these infrageneric groups.</discussion>
  <discussion>D. K. Wijesinghe and D. F. Whigham (1997, 2001) compared the different, below-ground, vegetative morphologies of three Uvularia species and their relationship to their differing clonal populations and dynamics.</discussion>
  <references>
    <reference>Anderson, E. and T. W. Whitaker. 1934. Speciation in Uvularia. J. Arnold Arbor. 15: 28–42.  </reference>
    <reference>Dietz, R. A. 1952. Variation in the perfoliate uvularias. Ann. Missouri Bot. Gard. 39: 219–247.  </reference>
    <reference>Hayashi, K. et al. 1998. Molecular systematics of the genus Uvularia and selected Liliales based upon matK and rbcL gene sequence data. Pl. Spec. Biol. 13: 129–146.  </reference>
    <reference>Kawano, S. and H. H. Iltis. 1964. Cytotaxonomic and geographic notes on Uvularia (Liliaceae). Bull. Torrey Bot. Club 91: 13–23.  </reference>
    <reference>Soper, J. H. 1952. Phytogeographic studies in Ontario I. The genus Uvularia in southern Ontario. Rhodora 54: 58–67.  </reference>
    <reference>Wijesinghe, D. K. and D. F. Whigham. 2001. Nutrient foraging in woodland herbs: A comparison of three species of Uvularia (Liliaceae) with contrasting belowground morphologies. Amer. J. Bot. 88: 1071–1079.  </reference>
    <reference>Wilbur, R. L. 1963. A revision of the North American genus Uvularia (Liliaceae). Rhodora 65: 158–188.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems rounded; leaf blades perfoliate, margins smooth; capsules obovoid to truncate; tepals smooth or densely papillose adaxially.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems angled distally; leaf blades sessile, margins minutely papillose-denticulate; capsules generally ellipsoid; tepals smooth adaxially.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves pubescent (rarely glabrous) on abaxial veins; leaves below lowest branch typically 1; tepals smooth adaxially; capsule with 2 truncate beaks per lobe.</description>
      <determination>4 Uvularia grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves glaucous, smooth abaxially; leaves below lowest branch (2–)3–4; tepals orange-papillose adaxially; capsule with 2 attenuate beaks per lobe.</description>
      <determination>5 Uvularia perfoliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Rhizomes short, 0.5–1 cm, bearing numerous, clustered, fleshy roots; stem nodes and abaxial leaf veins puberulent.</description>
      <determination>1 Uvularia puberula</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Rhizomes elongate, 10–15 cm, bearing scattered, fibrous roots; stem nodes and abaxial leaf surfaces glabrous.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles ebracteate; tepal apex rounded to acute; ovary and capsule stipitate.</description>
      <determination>2 Uvularia sessilifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles bearing 1, ovate, leafy bract; tepal apex acuminate; ovary and capsule sessile or subsessile.</description>
      <determination>3 Uvularia floridana</determination>
    </key_statement>
  </key>
</bio:treatment>