<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">592</other_info_on_meta>
    <other_info_on_meta type="treatment_page">593</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Dressler" date="1979" rank="tribe">Triphoreae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">triphora</taxon_name>
    <taxon_name authority="(Swartz) Ames &amp; Schlechter in O. Ames" date="1922" rank="species">gentianoides</taxon_name>
    <place_of_publication>
      <publication_title>in O. Ames, Orchidaceae</publication_title>
      <place_in_publication>7: 5. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe triphoreae;genus triphora;species gentianoides;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242102022</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">gentianoides</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>119. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Limodorum;species gentianoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Triphora</taxon_name>
    <taxon_name authority="(Reichenbach f.) Ames" date="unknown" rank="species">cubensis</taxon_name>
    <taxon_hierarchy>genus Triphora;species cubensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 8–26 cm.</text>
      <biological_entity id="o34094" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="26" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots from cylindric tuberoids, 30–60 × 8–10 mm.</text>
      <biological_entity id="o34095" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="length" notes="" src="d0_s1" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" notes="" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34096" name="tuberoid" name_original="tuberoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <relation from="o34095" id="r4598" name="from" negation="false" src="d0_s1" to="o34096" />
    </statement>
    <statement id="d0_s2">
      <text>Stems green, often tinged with brown or purplish, unbranched or rarely 2–3-branched from base.</text>
      <biological_entity id="o34097" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s2" value="tinged with brown or tinged with purplish" />
        <character name="coloration" src="d0_s2" value="," value_original="," />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character constraint="from base" constraintid="o34098" is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="2-3-branched" value_original="2-3-branched" />
      </biological_entity>
      <biological_entity id="o34098" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves 3–10, ascending, usually partially sheathing stem;</text>
      <biological_entity id="o34099" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o34100" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="usually partially" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green, often tinged with brown or purple, bractlike, ovate, 1–1.8 × 0.5–1 cm, margins entire.</text>
      <biological_entity id="o34101" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="tinged with brown or tinged with purple" />
        <character name="coloration" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34102" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–10 (–15) -flowered corymbose racemes.</text>
      <biological_entity id="o34103" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o34104" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="3-10(-15)-flowered" value_original="3-10(-15)-flowered" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers not resupinate, erect, closed to slightly gaping, pale green, sometimes tinged with brown or reddish-brown;</text>
    </statement>
    <statement id="d0_s7">
      <text>flowering successive, anthesis overlapping;</text>
      <biological_entity id="o34105" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="condition" src="d0_s6" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s6" value="gaping" value_original="gaping" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="tinged with brown or tinged with reddish-brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="development" src="d0_s7" value="successive" value_original="successive" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="anthesis" value_original="anthesis" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>dorsal sepal linearlanceolate, 6–11 × 2–2.5 mm;</text>
      <biological_entity constraint="dorsal" id="o34106" name="sepal" name_original="sepal" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral sepals linearlanceolate, falcate, 6–11 × 2–2.5 mm;</text>
      <biological_entity constraint="lateral" id="o34107" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals linearlanceolate, falcate, 8–11 × 1–2 mm;</text>
      <biological_entity id="o34108" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lip facing stem, white to pale green, ovate to obovate, clawed, 3-lobed, 8–10 × 3–4 mm, middle lobe ovate, lateral lobes lanceovate, margins erose to nearly lacerate;</text>
      <biological_entity id="o34109" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pale green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34110" name="stem" name_original="stem" src="d0_s11" type="structure" />
      <biological_entity constraint="middle" id="o34111" name="lobe" name_original="lobe" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o34112" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceovate" value_original="lanceovate" />
      </biological_entity>
      <biological_entity id="o34113" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s11" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o34109" id="r4599" name="facing" negation="false" src="d0_s11" to="o34110" />
    </statement>
    <statement id="d0_s12">
      <text>disc with 3 green, raised, denticulate crests;</text>
      <biological_entity id="o34114" name="disc" name_original="disc" src="d0_s12" type="structure" />
      <biological_entity id="o34115" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="true" name="prominence" src="d0_s12" value="raised" value_original="raised" />
        <character is_modifier="true" name="shape" src="d0_s12" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <relation from="o34114" id="r4600" name="with" negation="false" src="d0_s12" to="o34115" />
    </statement>
    <statement id="d0_s13">
      <text>column yellowish white, clavate, slender, 7 mm;</text>
      <biological_entity id="o34116" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollinia 2, yellow.</text>
      <biological_entity id="o34117" name="pollinium" name_original="pollinia" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules erect, ellipsoid, 10–20 × 5–10 mm.</text>
      <biological_entity id="o34118" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s15" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s15" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pinelands in shade or full sun, persisting in sandy lawns, or rarely in shade of live- oak hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pinelands" constraint="in shade or full sun" />
        <character name="habitat" value="shade" />
        <character name="habitat" value="full sun" />
        <character name="habitat" value="sandy lawns" />
        <character name="habitat" value="shade" constraint="of live" />
        <character name="habitat" value="oak hammocks" modifier="live" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; se Mexico; West Indies (Cuba, Dominican Republic, Haiti); Central America (Belize, Guatemala, Honduras, Panama); South America (Ecuador, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="se Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="West Indies (Haiti)" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
        <character name="distribution" value="Central America (Panama)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>The flowers are presumed to be self-fertilizing. The anther color has been reported as purple for Venezuelan material (G. C. K. Dunsterville and L. A. Garay [1959]–1976, vol. 3).</discussion>
  
</bio:treatment>