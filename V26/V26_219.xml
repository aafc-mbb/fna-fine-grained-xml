<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="Greene" date="1890" rank="species">plummerae</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 70. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species plummerae</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101490</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually bulbose;</text>
      <biological_entity id="o30424" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb coat fibrous-reticulate.</text>
      <biological_entity constraint="bulb" id="o30425" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s1" value="fibrous-reticulate" value_original="fibrous-reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems slender, usually branching, 3–6 dm.</text>
      <biological_entity id="o30426" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering;</text>
      <biological_entity id="o30427" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o30428" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to lanceolate, 2–4 dm.</text>
      <biological_entity id="o30429" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30430" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s4" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–6-flowered;</text>
      <biological_entity id="o30431" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts resembling distal cauline leaves.</text>
      <biological_entity id="o30432" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <biological_entity constraint="distal cauline" id="o30433" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o30432" id="r4095" name="resembling" negation="false" src="d0_s6" to="o30433" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o30434" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth open, broadly campanulate;</text>
      <biological_entity id="o30435" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals lanceolate, 3–5 cm, glabrous or with a few hairs at base, apex long-tapering;</text>
      <biological_entity id="o30436" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="with a few hairs" value_original="with a few hairs" />
      </biological_entity>
      <biological_entity id="o30437" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o30438" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o30439" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="long-tapering" value_original="long-tapering" />
      </biological_entity>
      <relation from="o30436" id="r4096" name="with" negation="false" src="d0_s9" to="o30437" />
      <relation from="o30437" id="r4097" name="at" negation="false" src="d0_s9" to="o30438" />
    </statement>
    <statement id="d0_s10">
      <text>petals pale-pink to rose, drying purplish, broadly cuneate to obovate, 3–4 cm, glabrous distally or nearly so, rarely fringed, with conspicuous, long, yellow hairs in broad median band, margins dentate;</text>
      <biological_entity id="o30440" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s10" to="rose" />
        <character is_modifier="false" name="condition" src="d0_s10" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="distally; distally; nearly" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o30441" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="median" id="o30442" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="true" name="width" src="d0_s10" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o30443" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
      </biological_entity>
      <relation from="o30440" id="r4098" name="with" negation="false" src="d0_s10" to="o30441" />
      <relation from="o30441" id="r4099" name="in" negation="false" src="d0_s10" to="o30442" />
    </statement>
    <statement id="d0_s11">
      <text>glands round, slightly depressed, ± glabrous, bordered by ring of dense, obscuring, orange hairs;</text>
      <biological_entity id="o30444" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="depressed" value_original="depressed" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="by ring" constraintid="o30445" is_modifier="false" name="architecture" src="d0_s11" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o30445" name="ring" name_original="ring" src="d0_s11" type="structure" />
      <biological_entity id="o30446" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="orange" value_original="orange" />
      </biological_entity>
      <relation from="o30445" id="r4100" name="part_of" negation="false" src="d0_s11" to="o30446" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 9–11 mm, ± equaling anthers;</text>
      <biological_entity id="o30447" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30448" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers lanceolate-linear, apex acute to somewhat short-tipped.</text>
      <biological_entity id="o30449" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate-linear" value_original="lanceolate-linear" />
      </biological_entity>
      <biological_entity id="o30450" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s13" value="short-tipped" value_original="short-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, linear, angled, 4–8 cm, apex acute.</text>
      <biological_entity id="o30451" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s14" value="angled" value_original="angled" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s14" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30452" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds light beige.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 18.</text>
      <biological_entity id="o30453" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light beige" value_original="light beige" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30454" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky slopes, often in brush, chaparral, pine forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky slopes" />
        <character name="habitat" value="brush" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="forest" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Plummer’s mariposa-lily</other_name>
  
</bio:treatment>