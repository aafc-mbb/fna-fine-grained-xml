<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="treatment_page">128</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">nudus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 263. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species nudus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101484</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calochortus</taxon_name>
    <taxon_name authority="Purdy" date="unknown" rank="species">nudus</taxon_name>
    <taxon_name authority="(Purdy) Jepson" date="unknown" rank="variety">shastensis</taxon_name>
    <taxon_hierarchy>genus Calochortus;species nudus;variety shastensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calochortus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">shastensis</taxon_name>
    <taxon_hierarchy>genus Calochortus;species shastensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually not branching, 1–3 dm.</text>
      <biological_entity id="o11213" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal persistent, 0.5–2 dm;</text>
      <biological_entity id="o11214" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o11215" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate, tapering at base, flat, glabrous adaxially.</text>
      <biological_entity id="o11216" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11217" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="at base" constraintid="o11218" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11218" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1–several-flowered.</text>
      <biological_entity id="o11219" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-several-flowered" value_original="1-several-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers erect;</text>
      <biological_entity id="o11220" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o11221" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals lanceolate, 10–12 mm, glabrous, apex acuminate;</text>
      <biological_entity id="o11222" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11223" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals pale lavender, widely obovate, 14–16 mm, adaxial surface ± glabrous, not ciliate;</text>
      <biological_entity id="o11224" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale lavender" value_original="pale lavender" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11225" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>glands not deeply depressed, bordered proximally by wide, ciliate membranes;</text>
      <biological_entity id="o11226" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not deeply" name="shape" src="d0_s8" value="depressed" value_original="depressed" />
        <character constraint="by membranes" constraintid="o11227" is_modifier="false" name="architecture" src="d0_s8" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o11227" name="membrane" name_original="membranes" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wide" value_original="wide" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers linear-oblong, apex obtuse or acute.</text>
      <biological_entity id="o11228" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <biological_entity id="o11229" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules erect, 3-winged, 1–2 cm.</text>
      <biological_entity id="o11230" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-winged" value_original="3-winged" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds light-brown, irregular.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o11231" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s11" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11232" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist grassy areas, meadows, lake and bog margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" modifier="moist" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="margins" modifier="and bog" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Shasta star-tulip</other_name>
  <discussion>Calochortus nudus hybridizes with C. minimus.</discussion>
  
</bio:treatment>