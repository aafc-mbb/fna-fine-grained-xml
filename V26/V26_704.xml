<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="genus">triteleia</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">lemmoniae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 141. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus triteleia;species lemmoniae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102033</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brodiaea</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">lemmoniae</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>20: 376. 1885 (as lemmonae)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brodiaea;species lemmoniae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 10–40 cm × 2–6 mm.</text>
      <biological_entity id="o21422" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scape 8–30 cm, smooth or scabrous near base;</text>
      <biological_entity id="o21423" name="scape" name_original="scape" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character constraint="near base" constraintid="o21424" is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21424" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>bracts purplish.</text>
      <biological_entity id="o21425" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: perianth bright-yellow to deep orange, fading to purple, 9–12 mm, tube turbinate, 2.5–3 mm, lobes ascending to slightly spreading, 7–9 mm, 2–3 times longer than tube;</text>
      <biological_entity id="o21426" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o21427" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character char_type="range_value" from="bright-yellow" name="coloration" src="d0_s3" to="deep orange fading" />
        <character char_type="range_value" from="bright-yellow" name="coloration" src="d0_s3" to="deep orange fading" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21428" name="tube" name_original="tube" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21429" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="slightly spreading" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
        <character constraint="tube" constraintid="o21430" is_modifier="false" name="length_or_size" src="d0_s3" value="2-3 times longer than tube" />
      </biological_entity>
      <biological_entity id="o21430" name="tube" name_original="tube" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>stamens attached at 1 level, equal;</text>
      <biological_entity id="o21431" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o21432" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character constraint="at level" constraintid="o21433" is_modifier="false" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character is_modifier="false" name="variability" notes="" src="d0_s4" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o21433" name="level" name_original="level" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments linear, 3 mm, apical appendages absent;</text>
      <biological_entity id="o21434" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o21435" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21436" name="appendage" name_original="appendages" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers yellow, 2 mm;</text>
      <biological_entity id="o21437" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o21438" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary 3 times longer than stipe;</text>
      <biological_entity id="o21439" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o21440" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character constraint="stipe" constraintid="o21441" is_modifier="false" name="length_or_size" src="d0_s7" value="3 times longer than stipe" />
      </biological_entity>
      <biological_entity id="o21441" name="stipe" name_original="stipe" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pedicel 0.7–2.5 cm.</text>
      <biological_entity id="o21442" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o21443" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (late May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Yellow pine belts, mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="yellow pine belts" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="past_name">lemmonae</other_name>
  <other_name type="common_name">Lemmon’s triteleia</other_name>
  <other_name type="common_name">Oak Creek triteleia</other_name>
  <discussion>Triteleia lemmoniae is the only representative of the genus in Arizona. Molecular data suggest that it is related to T. montana, found in the Sierra Nevada of California (J. C. Pires 2000), and not to the morphologically similar T. hyacinthina (R. F. Hoover 1941).</discussion>
  
</bio:treatment>