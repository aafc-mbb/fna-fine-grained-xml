<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erythronium</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1809" rank="species">revolutum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>13: Erythronium no. 3. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus erythronium;species revolutum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101605</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs narrowly ovoid, 35–50 mm, sometimes producing sessile offsets.</text>
      <biological_entity id="o32692" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s0" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32693" name="offset" name_original="offsets" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o32692" id="r4391" modifier="sometimes" name="producing" negation="false" src="d0_s0" to="o32693" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 10–25 mm;</text>
      <biological_entity id="o32694" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade distinctly mottled with irregular streaks of brown or white, broadly lanceolate to ovate, margins entire to ± wavy.</text>
      <biological_entity id="o32695" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="with streaks" constraintid="o32696" is_modifier="false" modifier="distinctly" name="coloration" src="d0_s2" value="mottled" value_original="mottled" />
      </biological_entity>
      <biological_entity id="o32696" name="streak" name_original="streaks" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s2" to="more or less wavy" />
      </biological_entity>
      <biological_entity id="o32697" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character char_type="range_value" from="broadly lanceolate" is_modifier="true" name="shape" src="d0_s2" to="ovate" />
      </biological_entity>
      <relation from="o32696" id="r4392" name="part_of" negation="false" src="d0_s2" to="o32697" />
    </statement>
    <statement id="d0_s3">
      <text>Scape 15–40 cm.</text>
      <biological_entity id="o32698" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–3-flowered.</text>
      <biological_entity id="o32699" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals uniformly clear violet-pink at anthesis, with yellow banding at base, lanceolate to narrowly elliptic, 25–40 mm, inner with small auricles at base;</text>
      <biological_entity id="o32700" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o32701" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s5" value="clear violet-pink" value_original="clear violet-pink" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s5" to="narrowly elliptic" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32702" name="banding" name_original="banding" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o32703" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="inner" id="o32704" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
      <biological_entity id="o32705" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o32706" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o32701" id="r4393" name="with" negation="false" src="d0_s5" to="o32702" />
      <relation from="o32702" id="r4394" name="at" negation="false" src="d0_s5" to="o32703" />
      <relation from="o32704" id="r4395" name="with" negation="false" src="d0_s5" to="o32705" />
      <relation from="o32705" id="r4396" name="at" negation="false" src="d0_s5" to="o32706" />
    </statement>
    <statement id="d0_s6">
      <text>stamens ± appressed to style, 12–22 mm;</text>
      <biological_entity id="o32707" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o32708" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character constraint="to style" constraintid="o32709" is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32709" name="style" name_original="style" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>filaments white to pink (darkening with age), flattened, ± lanceolate, 2–3 mm wide;</text>
      <biological_entity id="o32710" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o32711" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers bright-yellow;</text>
      <biological_entity id="o32712" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o32713" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style white to pink, 12–18 mm;</text>
      <biological_entity id="o32714" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32715" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma with slender recurved lobes 4–6 mm.</text>
      <biological_entity id="o32716" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32717" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity constraint="slender" id="o32718" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o32717" id="r4397" name="with" negation="false" src="d0_s10" to="o32718" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules oblong to obovoid, 3–6 cm.</text>
      <biological_entity id="o32719" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="obovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s11" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring (Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" constraint="Mar-Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded stream banks, river terraces, wet places in forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded stream banks" />
        <character name="habitat" value="river terraces" />
        <character name="habitat" value="wet places" constraint="in forests" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600(–1000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash., generally within 100 km of Pacific Coast.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="generally within 100 km of Pacific Coast" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Pink fawn-lily</other_name>
  
</bio:treatment>