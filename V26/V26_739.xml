<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="Greene" date="1899" rank="species">langloisii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 32. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species langloisii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101909</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">canbyi</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species canbyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">flaccidum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species flaccidum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">furcatum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species furcatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, olive to bronze-olive when dry, to 3.2 dm, not glaucous.</text>
      <biological_entity id="o1368" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="olive" modifier="when dry" name="coloration" src="d0_s0" to="bronze-olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="3.2" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, with 1 or 2 nodes, often purplish, 0.5–2 (–2.2) mm wide, glabrous, margins entire to denticulate, similar in color and texture to stem body;</text>
      <biological_entity id="o1370" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_some_measurement" src="d0_s1" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s1" value="wide" value_original="wide" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s1" to="denticulate" />
      </biological_entity>
      <biological_entity id="o1371" name="body" name_original="body" src="d0_s1" type="structure" constraint="stem" constraint_original="to stem">
        <character is_modifier="true" name="character" src="d0_s1" value="texture" value_original="texture" />
      </biological_entity>
      <relation from="o1369" id="r182" name="with" negation="false" src="d0_s1" to="o1370" />
      <relation from="o1369" id="r183" name="with" negation="false" src="d0_s1" to="o1371" />
    </statement>
    <statement id="d0_s2">
      <text>first internode 3.2–7.2 cm, usually shorter than leaves;</text>
      <biological_entity id="o1369" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o1372" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.2" from_unit="cm" name="some_measurement" src="d0_s2" to="7.2" to_unit="cm" />
        <character constraint="than leaves" constraintid="o1373" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o1373" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>distalmost node with 1–3 branches.</text>
      <biological_entity constraint="distalmost" id="o1374" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o1375" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o1374" id="r184" name="with" negation="false" src="d0_s3" to="o1375" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o1376" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1377" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o1378" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o1378" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o1379" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes purplish-tinged basally and sometimes along margins, obviously wider than supporting branch, glabrous, keels entire to occasionally denticulate;</text>
      <biological_entity id="o1380" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s6" value="purplish-tinged" value_original="purplish-tinged" />
        <character constraint="than supporting branch , glabrous , keels" constraintid="o1382, o1383" is_modifier="false" name="width" notes="" src="d0_s6" value="obviously wider" value_original="obviously wider" />
      </biological_entity>
      <biological_entity id="o1381" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o1382" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s6" to="occasionally denticulate" />
      </biological_entity>
      <biological_entity id="o1383" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s6" to="occasionally denticulate" />
      </biological_entity>
      <relation from="o1380" id="r185" modifier="sometimes" name="along" negation="false" src="d0_s6" to="o1381" />
    </statement>
    <statement id="d0_s7">
      <text>outer 12.5–25 mm, 0.9–2.7 mm longer than inner, tapering evenly towards apex, margins basally connate 2.2–3.8 (–5) mm;</text>
      <biological_entity constraint="outer" id="o1384" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="12.5" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="2.7" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o1385" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o1386" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1385" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o1386" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o1387" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="3.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved or straight, hyaline margins 0.2–0.4 mm wide, apex acuminate to acute or occasionally obtuse, ending 0–1.5 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o1388" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o1389" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o1390" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1391" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="acute or occasionally obtuse" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o1392" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <relation from="o1388" id="r186" name="with" negation="false" src="d0_s8" to="o1389" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals pale blue to bluish violet or white, bases yellow;</text>
      <biological_entity id="o1393" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1394" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s9" to="bluish violet or white" />
      </biological_entity>
      <biological_entity id="o1395" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer tepals 6.1–10 mm, apex rounded to truncate, aristate;</text>
      <biological_entity id="o1396" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o1397" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="6.1" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1398" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="truncate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ± entirely, glabrous or sparsely stipitate-glandular basally;</text>
      <biological_entity id="o1399" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1400" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; basally" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o1401" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1402" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o1403" name="foliage" name_original="foliage" src="d0_s12" type="structure" />
      <relation from="o1402" id="r187" name="to" negation="false" src="d0_s12" to="o1403" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules medium brown to black, ± globose, 3.1–4.7 mm;</text>
      <biological_entity id="o1404" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="medium" value_original="medium" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="black" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s13" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel spreading or ascending.</text>
      <biological_entity id="o1405" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds globose to obconic, lacking obvious depression, 0.9–1.1 mm, rugulose.</text>
      <biological_entity id="o1406" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 32.</text>
      <biological_entity id="o1407" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s15" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1408" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist prairies, roadsides, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist prairies" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., La., Miss., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion>Sisyrinchium implicatum E. P. Bicknell probably belongs here; the type (Hilgard s.n., Apr 1858, Calhoun Co., Mississippi, MO) was not seen.</discussion>
  <discussion>Sisyrinchium langloisii is to be expected in northeastern Mexico.</discussion>
  
</bio:treatment>