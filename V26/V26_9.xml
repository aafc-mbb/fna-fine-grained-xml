<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">42</other_info_on_meta>
    <other_info_on_meta type="treatment_page">43</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kunth" date="unknown" rank="family">pontederiaceae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="unknown" rank="genus">heteranthera</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="1798" rank="species">reniformis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Peruv.</publication_title>
      <place_in_publication>1: 43. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pontederiaceae;genus heteranthera;species reniformis</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="fna_id">220006312</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterandra</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavón Palisot de Beauvois" date="unknown" rank="species">reniformis</taxon_name>
    <taxon_hierarchy>genus Heterandra;species reniformis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or facultatively perennial.</text>
      <biological_entity id="o17311" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="facultatively" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Vegetative stems submersed with elongate internodes, or emersed and procumbent.</text>
      <biological_entity id="o17312" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
        <character constraint="with internodes" constraintid="o17313" is_modifier="false" name="location" src="d0_s1" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="location" notes="" src="d0_s1" value="emersed" value_original="emersed" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
      </biological_entity>
      <biological_entity id="o17313" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems 1–9 cm, distal internode 0.5–4 cm.</text>
      <biological_entity id="o17314" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17315" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sessile leaves forming basal rosette, blade linear to oblanceolate, thin, 2.4–3.7 cm × 3–8 mm.</text>
      <biological_entity id="o17316" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17317" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity id="o17318" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s3" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o17316" id="r2379" name="forming" negation="false" src="d0_s3" to="o17317" />
    </statement>
    <statement id="d0_s4">
      <text>Petiolate leaves floating or emersed;</text>
      <biological_entity id="o17319" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="location" src="d0_s4" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s4" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipule 1–5 cm;</text>
      <biological_entity id="o17320" name="stipule" name_original="stipule" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 2–13 cm;</text>
      <biological_entity id="o17321" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade reniform, 1–4 × 1–5 cm, length equal to or less than width, apex obtuse.</text>
      <biological_entity id="o17322" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="character" src="d0_s7" value="less than width" value_original="less than width" />
      </biological_entity>
      <biological_entity id="o17323" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences spicate, 2–8-flowered, elongating in 1 day, usually shorter than spathes, terminal flower sometimes extending beyond spathe apex;</text>
      <biological_entity id="o17324" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="spicate" value_original="spicate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-8-flowered" value_original="2-8-flowered" />
        <character constraint="in day" constraintid="o17325" is_modifier="false" name="length" src="d0_s8" value="elongating" value_original="elongating" />
        <character constraint="than spathes" constraintid="o17326" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s8" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o17325" name="day" name_original="day" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17326" name="spathe" name_original="spathes" src="d0_s8" type="structure" />
      <biological_entity constraint="terminal" id="o17327" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity constraint="spathe" id="o17328" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o17327" id="r2380" name="extending beyond" negation="false" src="d0_s8" to="o17328" />
    </statement>
    <statement id="d0_s9">
      <text>spathes 0.8–5.5 cm, glabrous;</text>
      <biological_entity id="o17329" name="spathe" name_original="spathes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s9" to="5.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peduncle 0.5–4.2 cm, glabrous.</text>
      <biological_entity id="o17330" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="4.2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers opening ca. 3 hours after sunrise, wilting by early afternoon;</text>
      <biological_entity id="o17331" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="by afternoon" constraintid="o17334" is_modifier="false" name="life_cycle" notes="" src="d0_s11" value="wilting" value_original="wilting" />
      </biological_entity>
      <biological_entity id="o17332" name="hour" name_original="hours" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o17333" name="sunrise" name_original="sunrise" src="d0_s11" type="structure" />
      <biological_entity id="o17334" name="afternoon" name_original="afternoon" src="d0_s11" type="structure" />
      <relation from="o17331" id="r2381" name="opening" negation="false" src="d0_s11" to="o17332" />
      <relation from="o17331" id="r2382" name="after" negation="false" src="d0_s11" to="o17333" />
    </statement>
    <statement id="d0_s12">
      <text>perianth white, salverform, tube 5–10 mm, limbs zygomorphic, lobes narrowly elliptic, 3–6.5 mm, distal central lobe with yellow or green region at base, sometimes with distal brown spot;</text>
      <biological_entity id="o17335" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="salverform" value_original="salverform" />
      </biological_entity>
      <biological_entity id="o17336" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17337" name="limb" name_original="limbs" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
      <biological_entity id="o17338" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal central" id="o17339" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <biological_entity id="o17340" name="region" name_original="region" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o17341" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity constraint="distal" id="o17342" name="brown-spot" name_original="brown spot" src="d0_s12" type="structure" />
      <relation from="o17339" id="r2383" name="with" negation="false" src="d0_s12" to="o17340" />
      <relation from="o17340" id="r2384" name="at" negation="false" src="d0_s12" to="o17341" />
      <relation from="o17339" id="r2385" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o17342" />
    </statement>
    <statement id="d0_s13">
      <text>stamens unequal, lateral stamens 0.9–2.2 mm, filaments linear, pubescent with white multicellular hairs toward apex;</text>
      <biological_entity id="o17343" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o17344" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17345" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character constraint="with hairs" constraintid="o17346" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17346" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o17347" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <relation from="o17346" id="r2386" name="toward" negation="false" src="d0_s13" to="o17347" />
    </statement>
    <statement id="d0_s14">
      <text>central stamen 2.2–4.7 mm, filament sparsely pubescent with multicellular hairs;</text>
      <biological_entity constraint="central" id="o17348" name="stamen" name_original="stamen" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17349" name="filament" name_original="filament" src="d0_s14" type="structure">
        <character constraint="with hairs" constraintid="o17350" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17350" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="multicellular" value_original="multicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style pubescent with multicellular hairs.</text>
      <biological_entity id="o17351" name="style" name_original="style" src="d0_s15" type="structure">
        <character constraint="with hairs" constraintid="o17352" is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17352" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="multicellular" value_original="multicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 8–14-winged, 0.5–0.9 × 0.3–0.5 mm. 2n = 48.</text>
      <biological_entity id="o17353" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="8-14-winged" value_original="8-14-winged" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s16" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17354" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadside ditches, edges of streams and ponds, freshwater tidal mudflats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="edges" constraint="of streams and ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="tidal mudflats" modifier="freshwater" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Del., Fla., Ga., Ill., Ind., Ky., La., Md., Miss., Mo., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Tex., Va., W.Va.; Mexico; throughout Central America; scattered in South America (Argentina, Brazil, Paraguay); naturalized in Italy.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="throughout Central America" establishment_means="native" />
        <character name="distribution" value="scattered in South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="scattered in South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="scattered in South America (Paraguay)" establishment_means="native" />
        <character name="distribution" value="naturalized in Italy" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>