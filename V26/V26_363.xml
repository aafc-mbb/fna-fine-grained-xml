<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="treatment_page">210</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">POLYGONATUM</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 3. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus POLYGONATUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek poly- , many, and gony, knee, in reference to the jointed rhizome</other_info_on_name>
    <other_info_on_name type="fna_id">126394</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from long, knotty, creeping, occasionally branched rhizomes;</text>
      <biological_entity id="o14514" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14515" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o14514" id="r2000" name="from" negation="false" src="d0_s0" to="o14515" />
    </statement>
    <statement id="d0_s1">
      <text>leaf-scars conspicuous;</text>
      <biological_entity id="o14516" name="leaf-scar" name_original="leaf-scars" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots fibrous to fleshy.</text>
      <biological_entity id="o14517" name="root" name_original="roots" src="d0_s2" type="structure">
        <character char_type="range_value" from="fibrous" name="texture" src="d0_s2" to="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple, leafy, erect to arching, glabrous or pubescent.</text>
      <biological_entity id="o14518" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="arching" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves simple, alternate [opposite or verticillate], dispersed along stem, short-petiolate, subsessile, or clasping;</text>
      <biological_entity id="o14519" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character constraint="along stem" constraintid="o14520" is_modifier="false" name="density" src="d0_s4" value="dispersed" value_original="dispersed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o14520" name="stem" name_original="stem" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate or lanceolate to linear, margins entire.</text>
      <biological_entity id="o14521" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o14522" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemose, with 1–10 (–15) flowers on axillary peduncles, except for distal and proximalmost axils;</text>
      <biological_entity id="o14523" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o14524" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="15" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o14525" name="peduncle" name_original="peduncles" src="d0_s6" type="structure" />
      <biological_entity constraint="distal and proximalmost" id="o14526" name="axil" name_original="axils" src="d0_s6" type="structure" />
      <relation from="o14523" id="r2001" name="with" negation="false" src="d0_s6" to="o14524" />
      <relation from="o14524" id="r2002" name="on" negation="false" src="d0_s6" to="o14525" />
      <relation from="o14523" id="r2003" name="except for" negation="false" src="d0_s6" to="o14526" />
    </statement>
    <statement id="d0_s7">
      <text>peduncle glabrous [pubescent].</text>
      <biological_entity id="o14527" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers nodding or pendulous;</text>
      <biological_entity id="o14528" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals 6, persistent, marcescent, connate basally into cylindrical tube, distinct, tips short, valvate;</text>
      <biological_entity id="o14529" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="condition" src="d0_s9" value="marcescent" value_original="marcescent" />
        <character constraint="into tube" constraintid="o14530" is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o14530" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity id="o14531" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s9" value="valvate" value_original="valvate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, included, adnate to perianth-tube;</text>
      <biological_entity id="o14532" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character constraint="to perianth-tube" constraintid="o14533" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o14533" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>filament surfaces various;</text>
      <biological_entity constraint="filament" id="o14534" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="various" value_original="various" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers basifixed [dorsifixed], dehiscence introrse;</text>
      <biological_entity id="o14535" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s12" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary superior, 3-locular, ovoid to globose;</text>
      <biological_entity id="o14536" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>septal nectaries present;</text>
      <biological_entity constraint="septal" id="o14537" name="nectary" name_original="nectaries" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style slender, included;</text>
      <biological_entity id="o14538" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="slender" value_original="slender" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate to obscurely 3-lobed;</text>
      <biological_entity id="o14539" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character char_type="range_value" from="capitate" name="shape" src="d0_s16" to="obscurely 3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel articulate, glabrous.</text>
      <biological_entity id="o14540" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits baccate, dark blue to black [red], globose, glaucous, pulpy.</text>
      <biological_entity id="o14541" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="baccate" value_original="baccate" />
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s18" to="black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s18" value="pulpy" value_original="pulpy" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds yellowish tan to brownish olive, globose or subglobose, 3–4.5 mm. x = 10 [9–15].</text>
      <biological_entity id="o14542" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="yellowish tan" name="coloration" src="d0_s19" to="brownish olive" />
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s19" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o14543" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="10" value_original="10" />
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s19" to="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions, North America, Eurasia, especially China and Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="especially China and Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <other_name type="common_name">Solomon’s seal</other_name>
  <other_name type="common_name">sceau-de-Salomon</other_name>
  <discussion>Species 57 (3 in the flora).</discussion>
  <discussion>Polygonatum is a member of the Polygonatae, a tribe now assigned to the Ruscaceae sensu lato (P. J. Rudall et al. 2000) or a more narrowly defined Convallariaceae (J. G. Conran and M. N. Tamura 1998; J. Yamashita and M. N. Tamura 2000). It is a complex genus with some species that are highly variable and wide-ranging, and others that are not. The flora provides examples of both. Polyploids arising from different base numbers and hybridization contribute to generic variation. The North American species all have x = 10, in contrast to variable base numbers among the Eurasian species (S. Kawano and H. H. Iltis 1963; E. Therman 1950, 1953, 1953b; M. N. Tamura 1993, 1995).</discussion>
  <discussion>In Polygonatum, filament morphology and level of tube insertion provide useful distinctions at the species level (M. N. Tamura 1993). Bee vibration, associated with the pendulous perianth tubes, which include the anthers and style, is an essential feature of the pollination system (A. S. Corbet et al. 1988).</discussion>
  <discussion>Young shoots and leaves can be boiled and eaten like asparagus, while the rhizomes reportedly have been dried and ground into flour or boiled and eaten like potatoes (L. Peterson 1978). Numerous eastern North American tribes, including the Cherokee, Chippewa, Iroquois, and Menominee, used plants of the genus as an analgesic or stimulant, or as a dermatological, cathartic, gynecological, and gastrointestinal aid (D. E. Moerman 1986). The berries contain an anthraquinone that causes vomiting and diarrhea.</discussion>
  <discussion>The arching stems and fine foliage of Polygonatum add architectural interest and focus to woodland shade gardens. In addition to the native species, the Eurasian P. odoratum (Miller) Druce and the hybrid, P. ×hybridum Brügger (P. multiflorum × P. odoratum), are commonly cultivated. The European P. multiflorum (Linnaeus) Allioni has been reported as locally naturalized in eastern Ontario and southern Quebec (B. Boivin 1967c).</discussion>
  <references>
    <reference>Abramova, L. I. 1975. On the taxonomical structure of the genus Polygonatum Mill. Bot. Zhurn. (Moscow &amp; Leningrad) 60: 490–497. </reference>
    <reference>Bush, B. F. 1927. The species of Polygonatum. Amer. Midl. Naturalist 10: 385–400. </reference>
    <reference>Gates, R. R. 1917. A revision of the genus Polygonatum in North America. Bull. Torrey Bot. Club 44: 117–126. </reference>
    <reference>Ownbey, R. P. 1944. The liliaceous genus Polygonatum in North America. Ann. Missouri Bot. Gard. 31: 373–413. </reference>
    <reference>Tamura, M. N., A. E. Schwarzbach, S. Kruse, and R. Reski. 1997. Biosystematics studies on the genus Polygonatum (Convallariaceae) IV. Molecular phylogenetic analysis based on restriction site mapping of the chloroplast gene trnK. Feddes Repert. 108: 159–168. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves glabrous on both surfaces; axillary peduncle 2–10(–15)-flowered.</description>
      <determination>1 Polygonatum biflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves minutely hairy or pilose only on abaxial veins; axillary peduncle 1–3(–5)-flowered.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perianth yellowish green; filaments densely warty; e North America.</description>
      <determination>2 Polygonatum pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perianth whitish, tipped with green; filaments glabrous or sparsely downy; New England.</description>
      <determination>3 Polygonatum latifolium</determination>
    </key_statement>
  </key>
</bio:treatment>