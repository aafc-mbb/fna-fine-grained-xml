<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fritillaria</taxon_name>
    <taxon_name authority="R. M. MacFarlane" date="1978" rank="species">eastwoodiae</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>25: 95. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus fritillaria;species eastwoodiae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101619</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fritillaria</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">phaeanthera</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>1: 55. 1933,</place_in_publication>
      <other_info_on_pub>not Purdy 1932</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Fritillaria;species phaeanthera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb scales: large 2–5;</text>
      <biological_entity constraint="bulb" id="o27657" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>small 10–60.</text>
      <biological_entity constraint="bulb" id="o27658" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s1" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem 2–8 dm.</text>
      <biological_entity id="o27659" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in 1–2 whorls of 3–5 leaves per node proximally, alternate distally, 5–10 cm, usually shorter than inflorescence;</text>
      <biological_entity id="o27660" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character constraint="than inflorescence" constraintid="o27664" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o27661" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <biological_entity id="o27662" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o27663" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o27664" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <relation from="o27660" id="r3746" name="in" negation="false" src="d0_s3" to="o27661" />
      <relation from="o27661" id="r3747" name="part_of" negation="false" src="d0_s3" to="o27662" />
      <relation from="o27661" id="r3748" name="per" negation="false" src="d0_s3" to="o27663" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to narrowly lanceolate, ± glaucous;</text>
      <biological_entity id="o27665" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal leaves usually ± equaling proximalmost leaf.</text>
      <biological_entity constraint="distal" id="o27666" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximalmost" id="o27667" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="usually more or less" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers nodding;</text>
      <biological_entity id="o27668" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals pale greenish yellow to red, narrowly elliptic, 1–1.7 cm, apex usually flared to slightly recurved;</text>
      <biological_entity id="o27669" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale greenish" value_original="pale greenish" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s7" to="red" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27670" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="flared" value_original="flared" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectaries green, gold, or yellow, lanceolate, less than 1/3 tepal length;</text>
      <biological_entity id="o27671" name="nectary" name_original="nectaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gold" value_original="gold" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gold" value_original="gold" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="1/3" />
      </biological_entity>
      <biological_entity id="o27672" name="tepal" name_original="tepal" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>style obviously branched for less than 1/2 its length, branches barely recurved, longer than 1.5 mm.</text>
      <biological_entity id="o27673" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="obviously" name="length" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o27674" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="barely" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules angled.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24, 34, 36.</text>
      <biological_entity id="o27675" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27676" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry benches and slopes, sometimes on serpentine, in chaparral or beneath conifers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="benches" modifier="dry" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="chaparral" modifier="sometimes on serpentine in" />
        <character name="habitat" value="conifers" modifier="or beneath" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Butte County fritillary</other_name>
  <discussion>This species is highly variable and shows evidence of either being of hybrid origin between Fritillaria recurva and F. micrantha, or, if it is of separate origin, appearing to hybridize easily with those two species. In the northern part of its range, F. eastwoodiae intergrades with F. recurva, whereas in the southern part of its range it intergrades with F. micrantha. Occasional individuals throughout the range display traits of both F. recurva and F. micrantha.</discussion>
  <references>
    <reference>  McFarlane, R. M. 1978. On the taxonomic status of Fritillaria phaeanthera Eastw. (Liliaceae). Madroño 25: 93–100.</reference>
  </references>
  
</bio:treatment>