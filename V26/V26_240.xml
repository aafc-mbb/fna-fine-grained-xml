<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">uvularia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">perfoliata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 304. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus uvularia;species perfoliata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220014032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">uvularia</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Wilbur" date="unknown" rank="species">caroliniana</taxon_name>
    <taxon_hierarchy>genus uvularia;species caroliniana</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes short, ca. 1 cm, bearing numerous, clustered, fleshy roots and 1–2 slender, elongate, white stolons to 1.5 dm.</text>
      <biological_entity id="o12961" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character name="some_measurement" src="d0_s0" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="2" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o12962" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o12963" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
      </biological_entity>
      <relation from="o12961" id="r1807" name="bearing" negation="false" src="d0_s0" to="o12962" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–several, simple or 1-branched, rounded, 1.5–5 dm, glaucous, bearing (2–) 3–4 leaves below lowest branch.</text>
      <biological_entity id="o12964" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-branched" value_original="1-branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o12965" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s1" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
      <biological_entity id="o12966" name="branch" name_original="branch" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s1" value="lowest" value_original="lowest" />
      </biological_entity>
      <relation from="o12964" id="r1808" name="bearing" negation="false" src="d0_s1" to="o12965" />
      <relation from="o12964" id="r1809" name="below lowest" negation="false" src="d0_s1" to="o12966" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades perfoliate, ovate to ovate-oblong, 4–20 (–12.5) × 1.5–4 (–6) cm, glaucous and smooth abaxially, margins smooth, apex acute to short-acuminate.</text>
      <biological_entity id="o12967" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="perfoliate" value_original="perfoliate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="ovate-oblong" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="average_length" src="d0_s2" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12968" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12969" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 1 per stem;</text>
      <biological_entity id="o12970" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character constraint="per stem" constraintid="o12971" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12971" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>peduncles 1–2 cm, bearing 1 perfoliate bract;</text>
      <biological_entity id="o12972" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12973" name="bract" name_original="bract" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="perfoliate" value_original="perfoliate" />
      </biological_entity>
      <relation from="o12972" id="r1810" name="bearing" negation="false" src="d0_s4" to="o12973" />
    </statement>
    <statement id="d0_s5">
      <text>tepals straw-yellow, (15–) 20–35 × 3–5 mm, orange-papillose adaxially, apex acute;</text>
      <biological_entity id="o12974" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="straw-yellow" value_original="straw-yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s5" value="orange-papillose" value_original="orange-papillose" />
      </biological_entity>
      <biological_entity id="o12975" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 10–15 mm;</text>
      <biological_entity id="o12976" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 6–10 mm;</text>
      <biological_entity id="o12977" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>connectives 1 mm;</text>
      <biological_entity id="o12978" name="connectif" name_original="connectives" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary sessile, obovoid;</text>
      <biological_entity id="o12979" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovoid" value_original="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 8–10 mm;</text>
      <biological_entity id="o12980" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma lobes 3–5 mm.</text>
      <biological_entity constraint="stigma" id="o12981" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules obovoid-truncate, 3-lobed, 0.7–1.3 × 1–1.6 cm, 2 attenuate beaks per lobe.</text>
      <biological_entity id="o12982" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="obovoid-truncate" value_original="obovoid-truncate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s12" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s12" to="1.6" to_unit="cm" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12983" name="beak" name_original="beaks" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o12984" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <relation from="o12983" id="r1811" name="per" negation="false" src="d0_s12" to="o12984" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2.5–4.5 mm;</text>
      <biological_entity id="o12985" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>arils membranaceous.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 14.</text>
      <biological_entity id="o12986" name="aril" name_original="arils" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12987" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous woods and upland thickets, acid to neutral soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous woods" />
        <character name="habitat" value="upland thickets" />
        <character name="habitat" value="acid" />
        <character name="habitat" value="neutral soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ky., La., Maine, Md., Mass., Miss., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Perfoliate bellwort</other_name>
  <discussion>Detailed life-history studies of Uvularia perfoliata (D. F. Whigham 1974; D. K. Wijesinghe and D. F. Whigham 1997, 2001; H. Kudoh et al. 1999) have established the genetic basis of its clonal structure.</discussion>
  <discussion>Uvularia pudica Fernald is an illegitate name that pertains here.</discussion>
  
</bio:treatment>