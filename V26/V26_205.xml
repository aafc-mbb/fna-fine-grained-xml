<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="(Baker) Purdy" date="1901" rank="species">concolor</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>3, 2: 135. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species concolor</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101463</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calochortus</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="unknown" rank="species">luteus</taxon_name>
    <taxon_name authority="Baker" date="unknown" rank="variety">concolor</taxon_name>
    <place_of_publication>
      <publication_title>Garden (London 1871–1927)</publication_title>
      <place_in_publication>48: 440, plate 1043. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calochortus;species luteus;variety concolor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually bulbose;</text>
      <biological_entity id="o33341" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb coat, when present, membranous.</text>
      <biological_entity constraint="bulb" id="o33342" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when present" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually stout, sparingly branching, not flexuous or twisted, 3–6 dm.</text>
      <biological_entity id="o33343" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="course" src="d0_s2" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering, 1–2 dm;</text>
      <biological_entity id="o33344" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o33345" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, glaucous.</text>
      <biological_entity id="o33346" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o33347" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences subumbellate, 1–4-flowered;</text>
      <biological_entity id="o33348" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts opposite pedicel, 4–8 cm.</text>
      <biological_entity id="o33349" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33350" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o33351" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o33352" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals usually with dark red blotch near base, lanceolate-ovate, 2–3 cm;</text>
      <biological_entity id="o33353" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33354" name="blotch" name_original="blotch" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="dark red" value_original="dark red" />
      </biological_entity>
      <biological_entity id="o33355" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o33353" id="r4494" name="with" negation="false" src="d0_s9" to="o33354" />
      <relation from="o33354" id="r4495" name="near" negation="false" src="d0_s9" to="o33355" />
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, often tinged purple in drying, cuneate to obovate, 3–5 cm;</text>
      <biological_entity id="o33356" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character constraint="in drying" is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="tinged purple" value_original="tinged purple" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glands usually ± round, depressed, small, surrounded by conspicuously fringed membrane and a few long, yellow hairs, densely covered with slender, unbranched hairs;</text>
      <biological_entity id="o33357" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s11" value="depressed" value_original="depressed" />
        <character is_modifier="false" name="size" src="d0_s11" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o33358" name="membrane" name_original="membrane" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="conspicuously" name="shape" src="d0_s11" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o33359" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="conspicuously" name="shape" src="d0_s11" value="fringed" value_original="fringed" />
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o33360" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <relation from="o33357" id="r4496" name="surrounded by" negation="false" src="d0_s11" to="o33358" />
      <relation from="o33357" id="r4497" name="surrounded by" negation="false" src="d0_s11" to="o33359" />
      <relation from="o33357" id="r4498" modifier="densely" name="covered with" negation="false" src="d0_s11" to="o33360" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 9–10 mm;</text>
      <biological_entity id="o33361" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers yellowish, oblong, 8–10 mm, apex obtuse.</text>
      <biological_entity id="o33362" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33363" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, lanceoloid-linear, angled, 5–8 cm, apex acuminate.</text>
      <biological_entity id="o33364" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="lanceoloid-linear" value_original="lanceoloid-linear" />
        <character is_modifier="false" name="shape" src="d0_s14" value="angled" value_original="angled" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s14" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33365" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds flat.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = ca. 14.</text>
      <biological_entity id="o33366" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s15" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33367" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes in chaparral and pine forest, frequently on decomposed granite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" constraint="in chaparral and pine forest" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pine forest" />
        <character name="habitat" value="decomposed granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49.</number>
  <other_name type="common_name">Goldenbowl mariposa-lily</other_name>
  
</bio:treatment>