<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="Greene" date="1899" rank="species">montanum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">montanum</taxon_name>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species montanum;variety montanum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102298</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">alpestre</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species alpestre;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">heterocarpum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species heterocarpum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants pale green to olive when dry.</text>
      <biological_entity id="o24255" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="pale green" modifier="when dry" name="coloration" src="d0_s0" to="olive" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe: outer 36–76 mm, 14–46 mm longer than inner, connate basally 1–3.5 mm;</text>
      <biological_entity id="o24256" name="spathe" name_original="spathe" src="d0_s1" type="structure" />
      <biological_entity constraint="outer" id="o24257" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="36" from_unit="mm" name="some_measurement" src="d0_s1" to="76" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s1" to="46" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o24258" is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s1" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o24258" name="spathe" name_original="spathe" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>inner with hyaline margins ending 0.9–4.3 mm proximal to green apex.</text>
      <biological_entity id="o24259" name="spathe" name_original="spathe" src="d0_s2" type="structure" />
      <biological_entity id="o24261" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o24262" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
      <relation from="o24260" id="r3294" name="with" negation="false" src="d0_s2" to="o24261" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 96.</text>
      <biological_entity constraint="inner" id="o24260" name="spathe" name_original="spathe" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24263" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, stream banks, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Labr.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Colo., Conn., Idaho, Ind., Kans., Maine, Mass., Mich., Minn., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.Dak., Pa., R.I., S.Dak., Vt., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27b.</number>
  <discussion>Populations of var. montanum in Kansas and Nebraska have an unusually high proportion of branched-stem plants mixed with simple-stemmed ones—even within the same clump (Great Plains Flora Association 1986). Although branched stems are found occasionally elsewhere in the taxon’s range, the high incidence in this region is unusual and may be related to the distributional approach of S. angustifolium, with which it often has been confused.</discussion>
  <discussion>Canadian populations of var. montanum tend to have narrower bracts and other less pronounced interspecific differences than do populations elsewhere. These populations can easily be confused with S. mucronatum, which consistently has some purple in the spathes and usually has a shorter outer spathe, and with S. septentrionale, which lacks emarginate outer tepals and has a very long and narrow outer spathe.</discussion>
  
</bio:treatment>