<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erythronium</taxon_name>
    <taxon_name authority="Applegate" date="1935" rank="species">oregonum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 99. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus erythronium;species oregonum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101599</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erythronium</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">giganteum</taxon_name>
    <taxon_name authority="Applegate" date="unknown" rank="subspecies">leucandrum</taxon_name>
    <taxon_hierarchy>genus Erythronium;species giganteum;subspecies leucandrum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erythronium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">oregonum</taxon_name>
    <taxon_name authority="(Applegate) Applegate" date="unknown" rank="subspecies">leucandrum</taxon_name>
    <taxon_hierarchy>genus Erythronium;species oregonum;subspecies leucandrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs narrowly ovoid, 25–60 mm, sometimes producing sessile offsets.</text>
      <biological_entity id="o28268" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s0" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28269" name="offset" name_original="offsets" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o28268" id="r3821" modifier="sometimes" name="producing" negation="false" src="d0_s0" to="o28269" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 12–25 cm;</text>
      <biological_entity id="o28270" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade distinctly mottled with irregular streaks of brown or white, ovate to broadly lanceolate, margins wavy.</text>
      <biological_entity id="o28271" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="with streaks" constraintid="o28272" is_modifier="false" modifier="distinctly" name="coloration" src="d0_s2" value="mottled" value_original="mottled" />
      </biological_entity>
      <biological_entity id="o28272" name="streak" name_original="streaks" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o28273" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s2" to="broadly lanceolate" />
      </biological_entity>
      <relation from="o28272" id="r3822" name="part_of" negation="false" src="d0_s2" to="o28273" />
    </statement>
    <statement id="d0_s3">
      <text>Scape ± reddish, 15–40 cm.</text>
      <biological_entity id="o28274" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–3-flowered.</text>
      <biological_entity id="o28275" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals white to creamy white with yellow base at anthesis, sometimes pinkish in age, sometimes with red lines or bands, elliptic to narrowly ovate, 25–40 mm, inner with small auricles at base;</text>
      <biological_entity id="o28276" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o28277" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="with base" constraintid="o28278" from="white" name="coloration" src="d0_s5" to="creamy white" />
        <character constraint="in age" constraintid="o28279" is_modifier="false" modifier="at anthesis; sometimes" name="coloration" notes="" src="d0_s5" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s5" to="narrowly ovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28278" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o28279" name="age" name_original="age" src="d0_s5" type="structure" />
      <biological_entity id="o28280" name="line" name_original="lines" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o28281" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="inner" id="o28282" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
      <biological_entity id="o28283" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o28284" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o28277" id="r3823" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o28280" />
      <relation from="o28277" id="r3824" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o28281" />
      <relation from="o28282" id="r3825" name="with" negation="false" src="d0_s5" to="o28283" />
      <relation from="o28283" id="r3826" name="at" negation="false" src="d0_s5" to="o28284" />
    </statement>
    <statement id="d0_s6">
      <text>stamens 12–25 mm;</text>
      <biological_entity id="o28285" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28286" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments white, flattened, ± lanceolate, 2–3 mm wide;</text>
      <biological_entity id="o28287" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28288" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers cream to yellow;</text>
      <biological_entity id="o28289" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28290" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s8" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style white, 12–18 mm;</text>
      <biological_entity id="o28291" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28292" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma with recurved lobes 3–6 mm.</text>
      <biological_entity id="o28293" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28294" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o28295" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o28294" id="r3827" name="with" negation="false" src="d0_s10" to="o28295" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules oblong to narrowly obovoid, 3–5 cm. 2n = 24.</text>
      <biological_entity id="o28296" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="narrowly obovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s11" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28297" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous forests, rocky outcrops, oak woodlands, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open coniferous forests" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Oregon fawn-lily</other_name>
  <discussion>Forms from the southern part of the range with cream-white tepals and pale anthers have been described as subsp. leucandrum. This species is closely related to E. revolutum and occasionally hybridizes with it where their ranges meet. In addition, E. citrinum and E. hendersonii are reported to hybridize with E. oregonum in the southern part of its range.</discussion>
  
</bio:treatment>