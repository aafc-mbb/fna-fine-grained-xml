<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="treatment_page">160</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erythronium</taxon_name>
    <taxon_name authority="Applegate" date="1933" rank="species">helenae</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>1: 188. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus erythronium;species helenae</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101593</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs ovoid, 30–55 mm, sometimes producing sessile bulbels.</text>
      <biological_entity id="o27505" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s0" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27506" name="bulbel" name_original="bulbels" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o27505" id="r3729" modifier="sometimes" name="producing" negation="false" src="d0_s0" to="o27506" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 7–20 cm;</text>
      <biological_entity id="o27507" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade mottled with irregular streaks of brown or white, broadly lanceolate to ovate, margins ± wavy.</text>
      <biological_entity id="o27508" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="with streaks" constraintid="o27509" is_modifier="false" name="coloration" src="d0_s2" value="mottled" value_original="mottled" />
      </biological_entity>
      <biological_entity id="o27509" name="streak" name_original="streaks" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" value_original="irregular" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o27510" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character char_type="range_value" from="broadly lanceolate" is_modifier="true" name="shape" src="d0_s2" to="ovate" />
      </biological_entity>
      <relation from="o27509" id="r3730" name="part_of" negation="false" src="d0_s2" to="o27510" />
    </statement>
    <statement id="d0_s3">
      <text>Scape 12–30 cm.</text>
      <biological_entity id="o27511" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–3-flowered.</text>
      <biological_entity id="o27512" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers fragrant;</text>
      <biological_entity id="o27513" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="odor" src="d0_s5" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals ± white, bright-yellow at base, pinkish in age, lanceolate to ovate, 25–40 mm, inner with small auricles at base;</text>
      <biological_entity id="o27514" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character constraint="at base" constraintid="o27515" is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character constraint="in age" constraintid="o27516" is_modifier="false" name="coloration" notes="" src="d0_s6" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27515" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o27516" name="age" name_original="age" src="d0_s6" type="structure" />
      <biological_entity constraint="inner" id="o27517" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o27518" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o27519" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o27517" id="r3731" name="with" negation="false" src="d0_s6" to="o27518" />
      <relation from="o27518" id="r3732" name="at" negation="false" src="d0_s6" to="o27519" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 8–13 mm;</text>
      <biological_entity id="o27520" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments ± yellow, linear, ± slender, less than 0.8 mm wide;</text>
      <biological_entity id="o27521" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers yellow;</text>
      <biological_entity id="o27522" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style ± white, often bent to one side, 5–8 mm;</text>
      <biological_entity id="o27523" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character constraint="to side" constraintid="o27524" is_modifier="false" modifier="often" name="shape" src="d0_s10" value="bent" value_original="bent" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27524" name="side" name_original="side" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stigma unlobed or with lobes shorter than 1 mm.</text>
      <biological_entity id="o27525" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="with lobes" value_original="with lobes" />
      </biological_entity>
      <biological_entity id="o27526" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o27525" id="r3733" name="with" negation="false" src="d0_s11" to="o27526" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules obovoid, 2–4 cm. 2n = 24.</text>
      <biological_entity id="o27527" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27528" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry woods or scrub, on serpentines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry woods" constraint="on serpentines" />
        <character name="habitat" value="scrub" constraint="on serpentines" />
        <character name="habitat" value="serpentines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif. (vicinity of Mount St. Helena).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Calif. (vicinity of Mount St. Helena)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Mount St. Helena fawn-lily</other_name>
  
</bio:treatment>