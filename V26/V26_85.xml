<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">zigadenus</taxon_name>
    <taxon_name authority="(A. Gray) S. Watson" date="1871" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>343. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zigadenus;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102102</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amianthium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>4: 123. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amianthium;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Toxicoscordion</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_hierarchy>genus Toxicoscordion;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Toxicoscordion</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">texense</taxon_name>
    <taxon_hierarchy>genus Toxicoscordion;species texense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zigadenus</taxon_name>
    <taxon_name authority="(Rydberg) J. F. Macbride" date="unknown" rank="species">texensis</taxon_name>
    <taxon_hierarchy>genus Zigadenus;species texensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–7.5 dm, from bulbs;</text>
      <biological_entity id="o2628" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="7.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2629" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <relation from="o2628" id="r344" name="from" negation="false" src="d0_s0" to="o2629" />
    </statement>
    <statement id="d0_s1">
      <text>bulbs not clumped, tunicate, ovoid, 15–40 × 9–35 mm.</text>
      <biological_entity id="o2630" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s1" value="clumped" value_original="clumped" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s1" to="40" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s1" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades 15–45 cm × 3–15 mm.</text>
      <biological_entity id="o2631" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o2632" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences usually paniculate, 20–60-flowered, with 1–8 branches, narrow, terminal raceme pyramidal in early anthesis, 3–30 × 3–7 cm, proximal branches 1/10–1/2 length of entire inflorescence, ascending at 10°–60° angle.</text>
      <biological_entity id="o2633" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="20-60-flowered" value_original="20-60-flowered" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o2634" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2635" name="raceme" name_original="raceme" src="d0_s3" type="structure">
        <character constraint="in early anthesis" is_modifier="false" name="shape" src="d0_s3" value="pyramidal" value_original="pyramidal" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2636" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/10 length of entire inflorescence" name="length" src="d0_s3" to="1/2 length of entire inflorescence" />
        <character constraint="at angle" constraintid="o2637" is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o2637" name="angle" name_original="angle" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="10" value_original="10" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="60" value_original="60" />
      </biological_entity>
      <relation from="o2633" id="r345" name="with" negation="false" src="d0_s3" to="o2634" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: perianth hypogynous, campanulate, 10–15 mm diam.;</text>
      <biological_entity id="o2638" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2639" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tepals persistent in fruit, cream colored, ovate, 3–8 × 1–4 mm, outer unclawed or rarely clawed to 5 mm, apex usually obtuse;</text>
      <biological_entity id="o2640" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2641" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character constraint="in fruit" constraintid="o2642" is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="cream colored" value_original="cream colored" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2642" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o2643" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="unclawed" value_original="unclawed" />
        <character constraint="to 5 mm" is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o2644" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>gland 1, obovate, distal margins obscure, thin;</text>
      <biological_entity id="o2645" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2646" name="gland" name_original="gland" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2647" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments straight, usually equaling tepals, occasionally longer, thickened proximally;</text>
      <biological_entity id="o2648" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2649" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o2650" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="usually" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
        <character is_modifier="false" modifier="occasionally" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel usually ascending in fruit, occasionally perpendicular to stem, 10–35 mm, bracts green, somewhat falcate, 3–20 mm.</text>
      <biological_entity id="o2651" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2652" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o2653" is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="to stem" constraintid="o2654" is_modifier="false" modifier="occasionally" name="orientation" notes="" src="d0_s8" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2653" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o2654" name="stem" name_original="stem" src="d0_s8" type="structure" />
      <biological_entity id="o2655" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 8–16 × 3–8 mm. 2n = 32.</text>
      <biological_entity id="o2656" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2657" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tallgrass prairie, calcareous glades, rocky hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tallgrass prairie" />
        <character name="habitat" value="calcareous glades" />
        <character name="habitat" value="rocky hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Nuttall’s death camas</other_name>
  
</bio:treatment>