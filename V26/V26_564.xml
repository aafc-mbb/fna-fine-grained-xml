<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gerald B. Straley†,Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LEUCOJUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 289. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 140. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus LEUCOJUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leukos, white, and ion, violet, alluding to the color and scent of the flowers</other_info_on_name>
    <other_info_on_name type="fna_id">118398</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, from brown, globose to ovoid, tunicate bulb.</text>
      <biological_entity id="o31163" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o31164" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character char_type="range_value" from="globose" is_modifier="true" name="shape" src="d0_s0" to="ovoid" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tunicate" value_original="tunicate" />
      </biological_entity>
      <relation from="o31163" id="r4192" name="from" negation="false" src="d0_s0" to="o31164" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves several;</text>
      <biological_entity id="o31165" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear-ligulate, base sheathing.</text>
      <biological_entity id="o31166" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="linear-ligulate" value_original="linear-ligulate" />
      </biological_entity>
      <biological_entity id="o31167" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape stout, hollow [slender, solid].</text>
      <biological_entity id="o31168" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate, 2–5 (–7) -flowered, spathaceous;</text>
      <biological_entity id="o31169" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-5(-7)-flowered" value_original="2-5(-7)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="spathaceous" value_original="spathaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathe bracts 2, free or entirely adnate on 1 side, appearing monophyllous.</text>
      <biological_entity constraint="spathe" id="o31170" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="free" value_original="free" />
        <character constraint="on side" constraintid="o31171" is_modifier="false" modifier="entirely" name="fusion" src="d0_s5" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="monophyllous" value_original="monophyllous" />
      </biological_entity>
      <biological_entity id="o31171" name="side" name_original="side" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers nodding;</text>
      <biological_entity id="o31172" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth campanulate;</text>
      <biological_entity id="o31173" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals 6, distinct, oblanceolate to ovate, equal;</text>
      <biological_entity id="o31174" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="ovate" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 6, distinct;</text>
      <biological_entity id="o31175" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers basifixed, conic, longer than filaments, blunt apically, dehiscing by terminal pores;</text>
      <biological_entity id="o31176" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s10" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="conic" value_original="conic" />
        <character constraint="than filaments" constraintid="o31177" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
        <character constraint="by terminal pores" constraintid="o31178" is_modifier="false" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o31177" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity constraint="terminal" id="o31178" name="pore" name_original="pores" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary inferior, green, 3-locular, globose, septal nectaries present;</text>
      <biological_entity id="o31179" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity constraint="septal" id="o31180" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style filiform or clavate, exceeding anthers;</text>
      <biological_entity id="o31181" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o31182" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o31181" id="r4193" name="exceeding" negation="false" src="d0_s12" to="o31182" />
    </statement>
    <statement id="d0_s13">
      <text>stigma minutely capitate.</text>
      <biological_entity id="o31183" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsular, erect, pyriform to subglobose, dehiscence loculicidal;</text>
      <biological_entity id="o31184" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character char_type="range_value" from="pyriform" name="shape" src="d0_s14" to="subglobose" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pericarp somewhat fleshy.</text>
      <biological_entity id="o31185" name="pericarp" name_original="pericarp" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds numerous, black, appendages absent.</text>
      <biological_entity id="o31186" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>x = 7, 8, 9, 10, 11, 12.</text>
      <biological_entity id="o31187" name="appendage" name_original="appendages" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o31188" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
        <character name="quantity" src="d0_s17" value="11" value_original="11" />
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; c, sw Europe, nw Africa, and sw Asia (Armenia, Crimea); introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="introduced" />
        <character name="distribution" value="nw Africa" establishment_means="introduced" />
        <character name="distribution" value="and sw Asia (Armenia)" establishment_means="introduced" />
        <character name="distribution" value="and sw Asia (Crimea)" establishment_means="introduced" />
        <character name="distribution" value="elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <other_name type="common_name">Snowflake</other_name>
  <other_name type="common_name">nivéole</other_name>
  <discussion>Species ca. 10 (1 in the flora).</discussion>
  <discussion>Several species of Leucojum are cultivated for their flowers. Although other species such as L. autumnale Linnaeus (autumn snowflake) and L. vernum Linnaeus (spring snowflake) may persist in old gardens, only the commonly cultivated L. aestivum (summer snowflake) is known definitely to be naturalized in the flora. There are unconfirmed reports that L. vernum is naturalized in the panhandle area of Florida.</discussion>
  <discussion>Leucojum is sometimes confused with Galanthus, a spring-flowering, Eurasian relative, but it differs in that Leucojum has perianth segments that are all equal in size, and hollow stems that are usually taller and bear 2–5 flowers. In the southern states, the common name snowdrop, which usually refers to Galanthus species, is applied to Leucojum.</discussion>
  <discussion>Leaves and bulbs of Leucojum contain the alkaloids lycorine and galanthamine, and are poisonous.</discussion>
  <references>
    <reference>Crespo, M. B. M., D. Lledo, M. F. Fay, and M. W. Chase. 1996. Molecular phylogeny of Leucojum based on ITS sequences. [Abstract.] Amer. J. Bot. 83(6, suppl.): 149.  </reference>
    <reference>Stern, F. C. 1956. Snowdrops and Snowflakes—A Study of the Genera Galanthus and Leucojum. London.</reference>
  </references>
  
</bio:treatment>