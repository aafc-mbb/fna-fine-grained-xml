<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">502</other_info_on_meta>
    <other_info_on_meta type="treatment_page">501</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="subfamily">Cypripedioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cypripedium</taxon_name>
    <taxon_name authority="Swartz" date="1800" rank="species">guttatum</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Vetensk. Acad. Nya Handl.</publication_title>
      <place_in_publication>21: 251. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily cypripedioideae;genus cypripedium;species guttatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200028624</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 12–35 cm.</text>
      <biological_entity id="o21680" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2 (–3,very rarely), on middle half of stem, alternate to subopposite, widespreading;</text>
      <biological_entity id="o21681" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="2" value_original="2" />
        <character char_type="range_value" from="alternate" name="arrangement" notes="" src="d0_s1" to="subopposite" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
      </biological_entity>
      <biological_entity constraint="middle" id="o21682" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o21683" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o21681" id="r2958" name="on" negation="false" src="d0_s1" to="o21682" />
      <relation from="o21682" id="r2959" name="part_of" negation="false" src="d0_s1" to="o21683" />
    </statement>
    <statement id="d0_s2">
      <text>blade lanceovate to ovate-suborbiculate, 5–15 × 1.5–8 cm.</text>
      <biological_entity id="o21684" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s2" to="ovate-suborbiculate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers solitary, erect;</text>
      <biological_entity id="o21685" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals white with pink to reddish or magenta markings;</text>
      <biological_entity id="o21686" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character constraint="with markings" constraintid="o21687" is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21687" name="marking" name_original="markings" src="d0_s4" type="structure">
        <character char_type="range_value" from="pink" is_modifier="true" name="coloration" src="d0_s4" to="reddish or magenta" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal ovate to suborbiculate-elliptic, 12–28 × 6–19 mm;</text>
      <biological_entity constraint="dorsal" id="o21688" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="suborbiculate-elliptic" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="28" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals connate, synsepal 12–21 × 3–8 mm;</text>
      <biological_entity constraint="lateral" id="o21689" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o21690" name="synsepal" name_original="synsepal" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s6" to="21" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals spreading, same color as sepals, lanceolate-subpandurate (constricted near apex), flat, 10–20 × 4–9 mm, slightly shorter than to equaling lip, margins undulate-revolute to slightly spiraled;</text>
      <biological_entity id="o21691" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="lanceolate-subpandurate" value_original="lanceolate-subpandurate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="9" to_unit="mm" />
        <character constraint="than to equaling lip" constraintid="o21693" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o21692" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o21693" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="true" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o21694" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s7" value="undulate-revolute" value_original="undulate-revolute" />
        <character is_modifier="false" modifier="slightly" name="arrangement" src="d0_s7" value="spiraled" value_original="spiraled" />
      </biological_entity>
      <relation from="o21691" id="r2960" name="as" negation="false" src="d0_s7" to="o21692" />
    </statement>
    <statement id="d0_s8">
      <text>lip similarly colored, subglobose to obovoid, 15–30 mm;</text>
      <biological_entity id="o21695" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="similarly" name="coloration" src="d0_s8" value="colored" value_original="colored" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s8" to="obovoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>orifice basal, 10–24 mm;</text>
      <biological_entity id="o21696" name="orifice" name_original="orifice" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="basal" value_original="basal" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode oblong-quadrangular to broadly ellipsoid or ovoid.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 20, 20.</text>
      <biological_entity id="o21697" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong-quadrangular" name="shape" src="d0_s10" to="broadly ellipsoid or ovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21698" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry open deciduous and spruce forest, tundra, meadows, scree</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to dry open deciduous" />
        <character name="habitat" value="forest" modifier="and spruce" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="scree" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Spotted lady’s-slipper</other_name>
  <discussion>Cypripedium guttatum has been reported from British Columbia (D. S. Correll 1950), apparently based on a single collection of C. parviflorum (M. G. Henry 119 GH, PH). See the discussion under 4. C. yatabeanum.</discussion>
  
</bio:treatment>