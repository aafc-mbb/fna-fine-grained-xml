<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="treatment_page">399</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="genus">nemastylis</taxon_name>
    <taxon_name authority="Pickering ex R. C. Foster" date="1945" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>155: 36. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus nemastylis;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101792</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, 18–35 cm.</text>
      <biological_entity id="o7716" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bulbs ca. 10 mm diam.</text>
      <biological_entity id="o7717" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character name="diameter" src="d0_s1" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, rarely 1-branched.</text>
      <biological_entity id="o7718" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="1-branched" value_original="1-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually 3–4, basal leaves (0–) 1–2;</text>
      <biological_entity id="o7719" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7720" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s3" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, ± reaching base of rhipidia, proximalmost cauline leaf-blade linear, others reduced, bractlike.</text>
      <biological_entity id="o7721" name="blade" name_original="blade" src="d0_s4" type="structure" constraint="rhipidium" constraint_original="rhipidium; rhipidium">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o7722" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o7723" name="rhipidium" name_original="rhipidia" src="d0_s4" type="structure" />
      <biological_entity constraint="proximalmost cauline" id="o7724" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o7725" name="other" name_original="others" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <relation from="o7721" id="r1098" name="more or less reaching" negation="false" src="d0_s4" to="o7722" />
      <relation from="o7721" id="r1099" name="part_of" negation="false" src="d0_s4" to="o7723" />
    </statement>
    <statement id="d0_s5">
      <text>Rhipidia 1–2-flowered;</text>
      <biological_entity id="o7726" name="rhipidium" name_original="rhipidia" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer spathe 18–22 mm, inner 25–30 mm, apex dry.</text>
      <biological_entity constraint="outer" id="o7727" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o7728" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7729" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s6" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Tepals pale blue, lanceolate, ca. 20 mm;</text>
      <biological_entity id="o7730" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale blue" value_original="pale blue" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments connate into tube, 1–2 mm;</text>
      <biological_entity id="o7731" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character constraint="into tube" constraintid="o7732" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7732" name="tube" name_original="tube" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>anthers (4–) 6–8 mm;</text>
      <biological_entity id="o7733" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary oblong, 6–8 mm;</text>
      <biological_entity id="o7734" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style branching ca. 2 mm above apex of filament tube;</text>
      <biological_entity id="o7735" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="branching" value_original="branching" />
        <character constraint="above apex" constraintid="o7736" name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7736" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity constraint="filament" id="o7737" name="tube" name_original="tube" src="d0_s11" type="structure" />
      <relation from="o7736" id="r1100" name="part_of" negation="false" src="d0_s11" to="o7737" />
    </statement>
    <statement id="d0_s12">
      <text>branches ca. 2.5 mm.</text>
      <biological_entity id="o7738" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules oblong, truncate, 10–15 (–20) mm.</text>
      <biological_entity id="o7739" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds compressed with winged angles, ca. 2.5 × 1.5 mm.</text>
      <biological_entity id="o7740" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character constraint="with angles" constraintid="o7741" is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character name="length" notes="" src="d0_s14" unit="mm" value="2.5" value_original="2.5" />
        <character name="width" notes="" src="d0_s14" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o7741" name="angle" name_original="angles" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone barrens, bluffs, wet prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone barrens" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="wet prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Mo., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>The flowers of Nemastylis nuttallii open ca. 5 p.m., and close ca. 7 p.m. The name N. coelestina (W. Bartram) Nuttall has long been used for this species, but in fact that name properly applies to a different entity, Ixia coelestina W. Bartram.</discussion>
  
</bio:treatment>