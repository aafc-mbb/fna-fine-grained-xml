<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Trattinnick" date="unknown" rank="genus">HOSTA</taxon_name>
    <place_of_publication>
      <publication_title>Arch. Gewächsk.</publication_title>
      <place_in_publication>1: 55, plate 89. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus HOSTA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Nicolaus Thomas Host, 1761–1834, Austrian botanist and physician to Emperor Frances II</other_info_on_name>
    <other_info_on_name type="fna_id">115795</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Sprengel" date="unknown" rank="genus">Funkia</taxon_name>
    <place_of_publication>
      <publication_title>Niobe Salisbury</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Funkia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, forming dome-shaped clumps, from rhizomes;</text>
      <biological_entity id="o17" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="dome--shaped" value_original="dome--shaped" />
      </biological_entity>
      <biological_entity id="o19" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o17" id="r2" name="forming" negation="false" src="d0_s0" to="o18" />
      <relation from="o17" id="r3" name="from" negation="false" src="d0_s0" to="o19" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short, branching, sometimes stoloniferous, leaf-scars prominent;</text>
      <biological_entity id="o20" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
      <biological_entity id="o21" name="leaf-scar" name_original="leaf-scars" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots fleshy.</text>
      <biological_entity id="o22" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves numerous, basal, spiral, distinctly petiolate;</text>
      <biological_entity id="o23" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course" src="d0_s3" value="spiral" value_original="spiral" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole sulcate, terete, sometimes ridged;</text>
      <biological_entity id="o25" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade light to dark green, often variegated, cordate to orbiculate to lanceolate, smooth to puckered, margins entire, slightly undulate [flat or crisped];</text>
      <biological_entity id="o26" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s5" to="dark green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="variegated" value_original="variegated" />
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s5" to="puckered" />
      </biological_entity>
      <biological_entity id="o27" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>veins campylodromous, conspicuous, usually sunken adaxially, prominent abaxially.</text>
      <biological_entity id="o28" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="campylodromous" value_original="campylodromous" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="usually; adaxially" name="prominence" src="d0_s6" value="sunken" value_original="sunken" />
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Scape usually surpassing leaves.</text>
      <biological_entity id="o29" name="scape" name_original="scape" src="d0_s7" type="structure" />
      <biological_entity id="o30" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o29" id="r4" modifier="usually" name="surpassing" negation="false" src="d0_s7" to="o30" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences simple, terminal, racemose, usually subsecund, elongate, subtended proximally by 1 or more sterile bracts, each flower usually bracteate.</text>
      <biological_entity id="o31" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s8" value="subsecund" value_original="subsecund" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o32" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o33" name="flower" name_original="flower" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <relation from="o31" id="r5" modifier="proximally" name="by" negation="false" src="d0_s8" to="o32" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth tubular to campanulate or urceolate-cylindric [funnelform];</text>
      <biological_entity id="o34" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s9" to="campanulate or urceolate-cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals 6, similar, connate proximally into wide-throated tube, white, bluish purple, or purplish violet with darker markings or lines, lobes spreading, sometimes recurved, longer than perianth-tube;</text>
      <biological_entity id="o36" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o37" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character constraint="into tube" constraintid="o38" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="bluish purple" value_original="bluish purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish violet" value_original="purplish violet" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="bluish purple" value_original="bluish purple" />
        <character constraint="with lines" constraintid="o40" is_modifier="false" name="coloration" src="d0_s10" value="purplish violet" value_original="purplish violet" />
      </biological_entity>
      <biological_entity id="o38" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="wide-throated" value_original="wide-throated" />
      </biological_entity>
      <biological_entity id="o39" name="marking" name_original="markings" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o40" name="line" name_original="lines" src="d0_s10" type="structure" />
      <biological_entity id="o41" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character constraint="than perianth-tube" constraintid="o42" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o42" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, inserted at base of perianth-tube or ovary apex, exceeding tepals;</text>
      <biological_entity id="o43" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o44" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o45" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o46" name="perianth-tube" name_original="perianth-tube" src="d0_s11" type="structure" />
      <biological_entity id="o47" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
      <relation from="o44" id="r6" name="inserted at" negation="false" src="d0_s11" to="o45" />
      <relation from="o44" id="r7" name="part_of" negation="false" src="d0_s11" to="o46" />
      <relation from="o44" id="r8" name="exceeding" negation="false" src="d0_s11" to="o47" />
    </statement>
    <statement id="d0_s12">
      <text>filaments declinate;</text>
      <biological_entity id="o48" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o49" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="declinate" value_original="declinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers dorsifixed in connective pits, dehiscence introrse;</text>
      <biological_entity id="o50" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o51" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="in connective pits" constraintid="o52" is_modifier="false" name="fixation" src="d0_s13" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s13" value="introrse" value_original="introrse" />
      </biological_entity>
      <biological_entity constraint="connective" id="o52" name="pit" name_original="pits" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, sessile, 3-locular, oblong, septal nectaries present;</text>
      <biological_entity id="o53" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o54" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="septal" id="o55" name="nectary" name_original="nectaries" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style filiform, exceeding stamens;</text>
      <biological_entity id="o56" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o57" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o58" name="stamen" name_original="stamens" src="d0_s15" type="structure" />
      <relation from="o57" id="r9" name="exceeding" negation="false" src="d0_s15" to="o58" />
    </statement>
    <statement id="d0_s16">
      <text>stigma minute, capitate or 3-lobed;</text>
      <biological_entity id="o59" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o60" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel short.</text>
      <biological_entity id="o61" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o62" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsular, pendent at maturity, angled, elongate or triangular, dehiscence loculicidal.</text>
      <biological_entity id="o63" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="capsular" value_original="capsular" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s18" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="shape" src="d0_s18" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s18" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds numerous, black, flattened, winged.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 30.</text>
      <biological_entity id="o64" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s19" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o65" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; temperate e Asia, especially Japan; cultivated worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="temperate e Asia" establishment_means="introduced" />
        <character name="distribution" value="especially Japan" establishment_means="introduced" />
        <character name="distribution" value="cultivated worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43.</number>
  <other_name type="common_name">Plantain-lily</other_name>
  <other_name type="common_name">funkia</other_name>
  <discussion>Species ca. 40 (3 in the flora).</discussion>
  <discussion>Hosta species delimitation has been problematic due to a long history of cultivation, hybridization, and selection, particularly in Japan, from the eighth century onwards (W. G. Schmid 1991). Hosta nomenclature is further complicated because many names are based on types of garden origin or sports originating among wild populations. Earlier taxonomic treatments were largely based on materials cultivated regionally in Japan (N. Fujita 1976), North America (L. H. Bailey 1930), Korea (M. G. Chung 1990; M. G. Chung and J. W. Kim 1991), and Europe (N. Hylander 1954). Hosta can be considered to comprise as few as 23–26 species (F. Maekawa and K. Kaneko 1968; N. Fujita 1976), or 40 or more if a stricter species concept is applied (A. Huxley et al. 1992; F. Maekawa 1940; W. G. Schmid 1991).</discussion>
  <discussion>Well over 1000 cultivars have been recorded with the International Registration Authority. Primarily used in temperate shade gardens, these cultivars feature various combinations of leaf size, shape, color, variegation, and texture (P. Aden 1988; D. Grenfell 1996, 1998; N. Hylander 1954; K. Kubitzki 1998b; W. G. Schmid 1991). While Hosta is mainly of ornamental importance economically, the leaves of some species are cooked and eaten in Korea and Japan, thus depleting local populations.</discussion>
  <discussion>Funkia, a later generic name proposed by Sprengel for these plants, is an illegitimate later homonym of Funckia Wildenow, and the family name Funkiaceae based upon it is therefore invalid (B. Mathew 1988). However, “funkia,” from the vernacular Japanese fukurin fu, long ago passed into many European languages as another common name for Hosta.</discussion>
  <references>
    <reference>Chung, M. G. and S. B. Jones. 1989. Pollen morphology of Hosta Tratt. and related genera. Bull. Torrey Bot. Club 116: 31–44.  </reference>
    <reference>Currie, H. E. 1992. A Biosystematic Study of the Genus Hosta Tratt. (Liliaceae/Hostaceae) in Eastern Asia. Ph.D. dissertation. University of Georgia.  </reference>
    <reference>Hylander, N. 1960. The genus Hosta. J. Roy. Hort. Soc. 85: 356–369.  </reference>
    <reference>Kirkpatrick, N. S. 1993. A Chloroplast DNA Restriction Mapping Study of the Genus Hosta (Liliaceae). Ph.D. dissertation. Miami University.  </reference>
    <reference>Maekawa, F. 1940. The genus Hosta. J. Fac. Sci. Univ. Tokyo, Sect. 3, Bot. 5: 317–425.  </reference>
    <reference>Maekawa, F. and K. Kaneko. 1968. Evolution of karyotype in Hosta (Liliaceae). J. Jap. Bot. 43: 132–140.  </reference>
    <reference>Schmid, W. G. 1991. The Genus Hosta. Portland.  </reference>
    <reference>Stearn, W. T. 1931. The hostas and funkias: A revision of the plantain lilies. Gard. Chron., ser. 3, 90: 27, 47–49, 88–89, 110. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades light yellowish green; flowers fragrant, to 13 cm, perianth long-tubular; tepals white.</description>
      <determination>1 Hosta plantaginea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades green; flowers not fragrant, 4–5.5 cm, perianth tubular-campanulate or urceolate-cylindric; tepals bluish purple or purplish violet.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 40–50 cm; leaf blades lanceolate to ovate-lanceolate, 10–17 × 5–7.5 cm, with 5–6 lateral vein pairs; tepals purplish violet; anthers purple.</description>
      <determination>2 Hosta lancifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 80–95 cm; leaf blades broadly ovate to cordate, 20–30 × 15–20 cm, with 7–9 lateral vein pairs; tepals bluish purple; anthers spotted purple.</description>
      <determination>3 Hosta ventricosa</determination>
    </key_statement>
  </key>
</bio:treatment>