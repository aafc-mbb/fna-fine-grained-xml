<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Ownbey ex Traub" date="1972" rank="species">membranaceum</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>28: 63. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species membranaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101374</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–3, producing bulbels in basal cluster around roots, ovoid, 1–1.6 × 0.9–1.4 cm;</text>
      <biological_entity id="o24977" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s0" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24978" name="bulbel" name_original="bulbels" src="d0_s0" type="structure" />
      <biological_entity id="o24979" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="position" src="d0_s0" value="basal" value_original="basal" />
        <character is_modifier="true" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
      </biological_entity>
      <relation from="o24977" id="r3386" name="producing" negation="false" src="d0_s0" to="o24978" />
      <relation from="o24977" id="r3387" name="in" negation="false" src="d0_s0" to="o24979" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, brown, membranous, obscurely cellular-reticulate, cells quadrate, walls very sinuous, without fibers;</text>
      <biological_entity constraint="outer" id="o24980" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o24981" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" modifier="obscurely" name="architecture_or_coloration_or_relief" src="d0_s1" value="cellular-reticulate" value_original="cellular-reticulate" />
      </biological_entity>
      <biological_entity id="o24982" name="wall" name_original="walls" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="very" name="course" src="d0_s1" value="sinuous" value_original="sinuous" />
      </biological_entity>
      <biological_entity id="o24983" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o24980" id="r3388" name="enclosing" negation="false" src="d0_s1" to="o24981" />
      <relation from="o24982" id="r3389" name="without" negation="false" src="d0_s1" to="o24983" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white or pink, cells obscure, quadrate.</text>
      <biological_entity constraint="inner" id="o24984" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o24985" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, green at anthesis, 2–3, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o24986" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o24987" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o24988" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o24989" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o24987" id="r3390" name="extending" negation="false" src="d0_s3" to="o24988" />
      <relation from="o24987" id="r3391" name="extending" negation="false" src="d0_s3" to="o24989" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, 15–40 cm × 3–5 mm, margins entire.</text>
      <biological_entity id="o24990" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24991" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, solid, terete, 15–40 cm × 1–3 mm.</text>
      <biological_entity id="o24992" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, loose, 15–35-flowered, ± globose, bulbils unknown;</text>
      <biological_entity id="o24993" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="15-35-flowered" value_original="15-35-flowered" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o24994" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2, 4–6-veined, lanceolate, ± equal, apex acuminate, setaceous.</text>
      <biological_entity constraint="spathe" id="o24995" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="4-6-veined" value_original="4-6-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o24996" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="setaceous" value_original="setaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers stellate, 7–12 mm;</text>
      <biological_entity id="o24997" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals spreading, pink or less commonly white, ovate to elliptic, ± equal, becoming papery in fruit, not carinate, margins entire, apex acute to acuminate, not involute;</text>
      <biological_entity id="o24998" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="less commonly" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="elliptic" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o24999" is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s9" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity id="o24999" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o25000" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25001" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o25002" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o25003" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o25004" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crested;</text>
      <biological_entity id="o25005" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, prominent, central, triangular, short, margins minutely denticulate;</text>
      <biological_entity id="o25006" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o25007" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s14" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o25008" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o25009" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, scarcely thickened, unlobed;</text>
      <biological_entity id="o25010" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 10–20 mm.</text>
      <biological_entity id="o25011" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o25012" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells each with minute, central papilla.</text>
      <biological_entity constraint="central" id="o25014" name="papilla" name_original="papilla" src="d0_s19" type="structure">
        <character is_modifier="true" name="size" src="d0_s19" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o25013" id="r3392" name="with" negation="false" src="d0_s19" to="o25014" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o25013" name="cell" name_original="cells" src="d0_s19" type="structure" />
      <biological_entity constraint="2n" id="o25015" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded, shady slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded" />
        <character name="habitat" value="shady slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <discussion>Allium membranaceum is found in the foothills of the northern and central Sierra Nevada.</discussion>
  
</bio:treatment>