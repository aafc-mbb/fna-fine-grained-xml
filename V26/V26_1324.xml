<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John T. Atwood</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="treatment_page">644</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Maxillarieae</taxon_name>
    <taxon_name authority="Lindley" date="1843" rank="subtribe">Maxillariinae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="1794" rank="genus">MAXILLARIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Peruv. Prodr.,</publication_title>
      <place_in_publication>116, plate 25. 1794</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe maxillarieae;subtribe maxillariinae;genus maxillaria;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin maxilla, jawbone; apparently an allusion to the open-mouth appearance of the flower when viewed laterally</other_info_on_name>
    <other_info_on_name type="fna_id">119902</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Lindley" date="unknown" rank="genus">Camaridium</taxon_name>
    <taxon_hierarchy>genus Camaridium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Salisbury" date="unknown" rank="genus">Ornithidium</taxon_name>
    <taxon_hierarchy>genus Ornithidium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Hoehne" date="unknown" rank="genus">Pseudomaxillaria</taxon_name>
    <taxon_hierarchy>genus Pseudomaxillaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Schlechter" date="unknown" rank="genus">Sepalosaccus</taxon_name>
    <taxon_hierarchy>genus Sepalosaccus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, epiphytic, rarely terrestrial, cespitose to climbing.</text>
      <biological_entity id="o33782" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose to climbing" value_original="cespitose to climbing" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems reduced [elongate], usually terminated with pseudobulb.</text>
      <biological_entity id="o33783" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o33784" name="pseudobulb" name_original="pseudobulb" src="d0_s1" type="structure" />
      <relation from="o33783" id="r4552" modifier="usually" name="terminated with" negation="false" src="d0_s1" to="o33784" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1–many;</text>
      <biological_entity id="o33785" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade conduplicate, linear, lanceolate, or elliptic, margins entire.</text>
      <biological_entity id="o33786" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o33787" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences solitary flowers borne laterally within bract or leaf-axil of rhizome of mature, immature, or apparently abortive shoots;</text>
      <biological_entity id="o33788" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o33789" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o33790" name="bract" name_original="bract" src="d0_s4" type="structure" />
      <biological_entity id="o33791" name="leaf-axil" name_original="leaf-axil" src="d0_s4" type="structure" />
      <biological_entity id="o33792" name="rhizome" name_original="rhizome" src="d0_s4" type="structure" />
      <biological_entity id="o33793" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="immature" value_original="immature" />
        <character is_modifier="true" modifier="apparently" name="development" src="d0_s4" value="abortive" value_original="abortive" />
      </biological_entity>
      <relation from="o33789" id="r4553" name="borne" negation="false" src="d0_s4" to="o33790" />
      <relation from="o33789" id="r4554" name="borne" negation="false" src="d0_s4" to="o33791" />
      <relation from="o33789" id="r4555" name="part_of" negation="false" src="d0_s4" to="o33792" />
      <relation from="o33789" id="r4556" name="part_of" negation="false" src="d0_s4" to="o33793" />
    </statement>
    <statement id="d0_s5">
      <text>floral bracts triangular, usually acute.</text>
      <biological_entity constraint="floral" id="o33794" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals and petals nearly same;</text>
      <biological_entity id="o33795" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o33796" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
      <biological_entity id="o33797" name="petal" name_original="petals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>dorsal sepal erect, concave;</text>
      <biological_entity id="o33798" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="dorsal" id="o33799" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals adnate to column-foot;</text>
      <biological_entity id="o33800" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o33801" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="to column-foot" constraintid="o33802" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o33802" name="column-foot" name_original="column-foot" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>lip 3-lobed or simple;</text>
      <biological_entity id="o33803" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33804" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>column not winged, semiterete, with foot at base;</text>
      <biological_entity id="o33805" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33806" name="column" name_original="column" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s10" value="semiterete" value_original="semiterete" />
      </biological_entity>
      <biological_entity id="o33807" name="foot" name_original="foot" src="d0_s10" type="structure" />
      <biological_entity id="o33808" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o33806" id="r4557" name="with" negation="false" src="d0_s10" to="o33807" />
      <relation from="o33807" id="r4558" name="at" negation="false" src="d0_s10" to="o33808" />
    </statement>
    <statement id="d0_s11">
      <text>pollinia 4, waxy;</text>
      <biological_entity id="o33809" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33810" name="pollinium" name_original="pollinia" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="texture" src="d0_s11" value="ceraceous" value_original="waxy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmatic cavity round;</text>
      <biological_entity id="o33811" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="stigmatic" id="o33812" name="cavity" name_original="cavity" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rostellum not prominent.</text>
      <biological_entity id="o33813" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33814" name="rostellum" name_original="rostellum" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, ellipsoid to obovoid.</text>
      <biological_entity constraint="fruits" id="o33815" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="obovoid" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical regions, Western Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical regions" establishment_means="native" />
        <character name="distribution" value="Western Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>63.</number>
  <discussion>Species ca. 650 (2 in the flora).</discussion>
  <references>
    <reference>Atwood, J. T. 1993. A revision of the Maxillaria neglecta complex (Orchidaceae) in Mesoamerica. Lindleyana 8: 25–31.  </reference>
    <reference>Brieger, F. G. 1977. On the Maxillariinae (Orchidaceae) with sepaline spur. Bot. Jahrb. Syst. 97: 548–574.  </reference>
    <reference>Hammer, R. L. 1981. Finding new orchids: A contribution to the Orchidaceae of Florida. Fairchild Trop. Gard. Bull. 36(3): 16–18. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants conspicuously rhizomatous; pseudobulbs conspicuous; sepals less than 1 cm.</description>
      <determination>1 Maxillaria parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants cespitose; pseudobulbs absent or hidden from view; sepals more than 1 cm.</description>
      <determination>2 Maxillaria crassifolia</determination>
    </key_statement>
  </key>
</bio:treatment>