<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">609</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="treatment_page">610</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Kunth" date="1815" rank="tribe">Epidendreae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Laeliinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">epidendrum</taxon_name>
    <taxon_name authority="A. Richard" date="1853" rank="species">amphistomum</taxon_name>
    <place_of_publication>
      <publication_title>in R. de la Sagra, Hist. Fis. Cuba</publication_title>
      <place_in_publication>11: 240. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe epidendreae;subtribe laeliinae;genus epidendrum;species amphistomum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101578</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, to 110 cm.</text>
      <biological_entity id="o30971" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots basal, 1.5–4 mm diam.</text>
      <biological_entity id="o30972" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched, straight, compressed, to 60 cm.</text>
      <biological_entity id="o30973" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 5–13;</text>
      <biological_entity id="o30974" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 20–36 mm;</text>
      <biological_entity id="o30975" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="36" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, obtuse, middle ones somewhat larger than basal or apical, 4–18 × 1–4 cm, leathery.</text>
      <biological_entity id="o30976" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="middle" id="o30977" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character constraint="than basal or apical blade" constraintid="o30978" is_modifier="false" name="size" src="d0_s5" value="somewhat larger" value_original="somewhat larger" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o30978" name="blade" name_original="blade" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemes, subcapitate, densely flowered, 10–50 cm;</text>
    </statement>
    <statement id="d0_s7">
      <text>1st raceme terminal, producing additional racemes from nodes of long peduncle over years;</text>
      <biological_entity constraint="inflorescences" id="o30979" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30980" name="raceme" name_original="raceme" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o30981" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="additional" value_original="additional" />
      </biological_entity>
      <biological_entity id="o30982" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o30983" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o30984" name="year" name_original="years" src="d0_s7" type="structure" />
      <relation from="o30980" id="r4163" name="producing" negation="false" src="d0_s7" to="o30981" />
      <relation from="o30980" id="r4164" name="from" negation="false" src="d0_s7" to="o30982" />
      <relation from="o30982" id="r4165" name="part_of" negation="false" src="d0_s7" to="o30983" />
      <relation from="o30982" id="r4166" name="over" negation="false" src="d0_s7" to="o30984" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle elongate, laterally compressed, 9–48 cm.</text>
      <biological_entity id="o30985" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s8" to="48" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers to 25 per raceme, not resupinate, spirally oriented, yellow-green to redbrown;</text>
      <biological_entity id="o30986" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per raceme" constraintid="o30987" from="0" name="quantity" src="d0_s9" to="25" />
        <character is_modifier="false" modifier="not" name="orientation" notes="" src="d0_s9" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" modifier="spirally" name="orientation" src="d0_s9" value="oriented" value_original="oriented" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s9" to="redbrown" />
      </biological_entity>
      <biological_entity id="o30987" name="raceme" name_original="raceme" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals obovate-elliptic, 5.5–7.5 × 3–4 mm, fleshy, apex obtuse;</text>
      <biological_entity id="o30988" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30989" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals narrowly oblanceolate, thin, 5–7 × 0.5–1 mm, apex obtuse;</text>
      <biological_entity id="o30990" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30991" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip cordate, apex 2-lobed, 4–6 × 4.5–7 mm, fleshy, with prominent longitudinal ridge terminating in fleshy mucro;</text>
      <biological_entity id="o30992" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o30993" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30994" name="ridge" name_original="ridge" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o30995" name="mucro" name_original="mucro" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o30993" id="r4167" name="with" negation="false" src="d0_s12" to="o30994" />
      <relation from="o30994" id="r4168" name="terminating in" negation="false" src="d0_s12" to="o30995" />
    </statement>
    <statement id="d0_s13">
      <text>column 5 mm;</text>
      <biological_entity id="o30996" name="column" name_original="column" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anther ovoid, apex saddle-shaped;</text>
      <biological_entity id="o30997" name="anther" name_original="anther" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o30998" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="saddle--shaped" value_original="saddle--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 7–14 mm.</text>
      <biological_entity id="o30999" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules subglobose;</text>
      <biological_entity id="o31000" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 6.5 mm;</text>
      <biological_entity id="o31001" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="6.5" value_original="6.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>body 7.5 × 6 mm;</text>
      <biological_entity id="o31002" name="body" name_original="body" src="d0_s18" type="structure">
        <character name="length" src="d0_s18" unit="mm" value="7.5" value_original="7.5" />
        <character name="width" src="d0_s18" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>beak obsolete.</text>
      <biological_entity id="o31003" name="beak" name_original="beak" src="d0_s19" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s19" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Dec–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Widespread and common in forests and hammocks of the Big Cypress Swamp and Everglades, on deciduous trees and palms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="widespread and common in" constraint="of the big cypress" />
        <character name="habitat" value="hammocks" constraint="of the big cypress" />
        <character name="habitat" value="deciduous trees" modifier="and on" />
        <character name="habitat" value="palms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Epidendrum amphistomum grows in Martin, Lee, Collier, Miami-Dade, and Monroe counties, Florida.</discussion>
  <discussion>The species is pollinated by male night moths: Eucereon carolina, Lymire edwardsii, Cisseps fulvicollis, and Oxydia vesulia. Its reproduction is obligately allogamous. Flowers produce a fragrance reminiscent of overly ripe vegetables, heaviest between late afternoon and dawn (R. M. Adams and G. J. Goss 1976). </discussion>
  <discussion>This name has been generally regarded as synonymous with Epidendrum anceps Jacquin, which L. A. Garay and H. R. Sweet (1974) considered synonymous with Epidendrum secundum Jacquin (E. Hágsater 1993).</discussion>
  <discussion>Outside the flora area are several closely related species, all of them having been considered by recent authors under the name Epidendrum anceps.</discussion>
  <references>
    <reference>  Adams, R. M. and G. J. Goss. 1976. The reproductive biology of the epiphytic orchids of Florida 3. Epidendrum anceps Jacquin. Amer. Orchid Soc. Bull. 43: 488–492.  </reference>
    <reference>Hágsater, E. 1993. Epidendrum anceps or Epidendrum secundum? Orquidea (Mexico City) 13: 153–158.</reference>
  </references>
  
</bio:treatment>