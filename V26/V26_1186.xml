<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="mention_page">570</other_info_on_meta>
    <other_info_on_meta type="treatment_page">581</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Habenariinae</taxon_name>
    <taxon_name authority="Willdenow" date="1805" rank="genus">HABENARIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4(1): 5, 44. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe habenariinae;genus habenaria;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin habena, rein or strap</other_info_on_name>
    <other_info_on_name type="fna_id">114408</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, terrestrial or semiaquatic, often rather succulent.</text>
      <biological_entity id="o28178" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="semiaquatic" value_original="semiaquatic" />
        <character is_modifier="false" modifier="often rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots scattered along basal portion of stem, both slender and tuberous, fleshy;</text>
      <biological_entity id="o28179" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="along basal portion" constraintid="o28180" is_modifier="false" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="size" notes="" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28180" name="portion" name_original="portion" src="d0_s1" type="structure" />
      <biological_entity id="o28181" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o28180" id="r3810" name="part_of" negation="false" src="d0_s1" to="o28181" />
    </statement>
    <statement id="d0_s2">
      <text>tuberoids usually spheroid.</text>
      <biological_entity id="o28182" name="tuberoid" name_original="tuberoids" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems: leaves basal, abruptly reduced to bracts or scattered, gradually reduced toward inflorescence.</text>
      <biological_entity id="o28183" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o28184" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="to " constraintid="o28186" is_modifier="false" modifier="abruptly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o28185" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o28186" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
        <character is_modifier="true" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves several, alternate, ascending to spreading, conduplicate, bases sheathing stem.</text>
      <biological_entity id="o28187" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s4" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity id="o28188" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o28189" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, lax to rather dense spicate racemes;</text>
      <biological_entity id="o28190" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o28191" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="rather" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts large and foliaceous to reduced.</text>
      <biological_entity id="o28192" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers few-to-many, resupinate, pedicellate, sometimes showy;</text>
      <biological_entity id="o28193" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s7" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s7" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 2-lobed, lateral lobe on abaxial margin;</text>
      <biological_entity id="o28194" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o28195" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o28196" name="margin" name_original="margin" src="d0_s8" type="structure" />
      <relation from="o28195" id="r3811" name="on" negation="false" src="d0_s8" to="o28196" />
    </statement>
    <statement id="d0_s9">
      <text>lip 3-lobed, spurred at base;</text>
      <biological_entity id="o28197" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
        <character constraint="at base" constraintid="o28198" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o28198" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pollinaria 2;</text>
      <biological_entity id="o28199" name="pollinarium" name_original="pollinaria" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinia 2;</text>
      <biological_entity id="o28200" name="pollinium" name_original="pollinia" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidia free;</text>
      <biological_entity id="o28201" name="viscidium" name_original="viscidia" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma with 2 fleshy processes flanking or encircling mouth of spur.</text>
      <biological_entity id="o28202" name="stigma" name_original="stigma" src="d0_s13" type="structure" constraint="spur" constraint_original="spur; spur" />
      <biological_entity id="o28203" name="process" name_original="processes" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o28204" name="mouth" name_original="mouth" src="d0_s13" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s13" value="encircling" value_original="encircling" />
      </biological_entity>
      <biological_entity id="o28205" name="spur" name_original="spur" src="d0_s13" type="structure" />
      <relation from="o28202" id="r3812" name="with" negation="false" src="d0_s13" to="o28203" />
      <relation from="o28203" id="r3813" name="flanking" negation="false" src="d0_s13" to="o28204" />
      <relation from="o28202" id="r3814" name="part_of" negation="false" src="d0_s13" to="o28205" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, ellipsoid.</text>
      <biological_entity constraint="fruits" id="o28206" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pantropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pantropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Rein orchid</other_name>
  <discussion>Species ca. 600 (4 in the flora).</discussion>
  <discussion>A recently identified species, Habenaria macroceratitis, is described but not fully treated here.</discussion>
  <references>
    <reference>  Ames, O. 1905–1922. Orchidaceae: Illustrations and Studies of the Family Orchidaceae Issuing from the Ames Botanical Laboratory…. 7 vols. Boston and New York. Vol. 4.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral lobes of lip and petals greatly reduced; lip hastate-auriculate; petals auriculate on abaxial margin.</description>
      <determination>1 Habenaria odontopetala</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral lobes of lip and petals prominently filiform, nearly equal to or exceeding other lobes; lip conspicuously 3-lobed; petals conspicuously 2-lobed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spur more than 4 cm (often much more).</description>
      <determination>2 Habenaria quinqueseta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spur at most 2 cm (often much shorter).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves basal, spreading, broadly elliptic to ovate or obovate; spur slenderly clavate.</description>
      <determination>3 Habenaria distans</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves scattered along length of stem, ascending, linear-lanceolate to lanceolate, narrowly elliptic, or oblanceolate; spur slenderly cylindric to scarcely clavate.</description>
      <determination>4 Habenaria repens</determination>
    </key_statement>
  </key>
</bio:treatment>