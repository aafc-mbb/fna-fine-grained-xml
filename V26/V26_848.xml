<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Susan Verhoek,William J. Hess</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="treatment_page">413</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">AGAVACEAE</taxon_name>
    <taxon_hierarchy>family AGAVACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10019</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually perennial, occasionally epiphytic, sometimes monocarpic or polycarpic, monoecious, dioecious, or polygamodioecious, small to gigantic, sometimes arborescent, usually scapose.</text>
      <biological_entity id="o16446" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="gigantic" />
        <character is_modifier="false" modifier="sometimes; usually" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems subterranean or aboveground, sometimes branched.</text>
      <biological_entity id="o16447" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="location" src="d0_s1" value="aboveground" value_original="aboveground" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves simple, annual or long-lived, in terminal rosettes or occasionally cauline, sessile or occasionally pseudopetiolate;</text>
      <biological_entity id="o16448" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s2" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16449" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <biological_entity id="o16450" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="occasionally" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <relation from="o16448" id="r2245" name="in" negation="false" src="d0_s2" to="o16449" />
      <relation from="o16448" id="r2246" name="in" negation="false" src="d0_s2" to="o16450" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear, lanceolate, oblanceolate, ovate, or elliptic, fibrous, thin and flexible, thick and rigid or succulent, or fibrous, often glaucous, margins entire, serrulate, dentate, denticulate, corneous, or filiferous, apex rigid or flexible, sometimes pungent, often with short or long spine.</text>
      <biological_entity id="o16451" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o16452" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="corneous" value_original="corneous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
      </biological_entity>
      <biological_entity id="o16453" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="sometimes" name="odor_or_shape" src="d0_s3" value="pungent" value_original="pungent" />
      </biological_entity>
      <biological_entity id="o16454" name="spine" name_original="spine" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
      </biological_entity>
      <relation from="o16453" id="r2247" modifier="often" name="with" negation="false" src="d0_s3" to="o16454" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or axillary spikes, racemose or paniculate, sometimes umbellate, bracteate, often huge;</text>
      <biological_entity id="o16455" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s4" value="huge" value_original="huge" />
      </biological_entity>
      <biological_entity id="o16456" name="spike" name_original="spikes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts ascending or erect, occasionally reflexed, leaflike proximally, scalelike distally.</text>
      <biological_entity id="o16457" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 6-merous, bisexual or functionally unisexual;</text>
      <biological_entity id="o16458" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="6-merous" value_original="6-merous" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth of 2 similar petallike whorls, semisucculent;</text>
      <biological_entity id="o16459" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" notes="" src="d0_s7" value="semisucculent" value_original="semisucculent" />
      </biological_entity>
      <biological_entity id="o16460" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s7" value="petal-like" value_original="petallike" />
      </biological_entity>
      <relation from="o16459" id="r2248" name="consist_of" negation="false" src="d0_s7" to="o16460" />
    </statement>
    <statement id="d0_s8">
      <text>tepals distinct or connate into tube, apex glandular or glandular-pubescent;</text>
      <biological_entity id="o16461" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character constraint="into tube" constraintid="o16462" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o16462" name="tube" name_original="tube" src="d0_s8" type="structure" />
      <biological_entity id="o16463" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens included or exserted;</text>
      <biological_entity id="o16464" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments often broadened and succulent, glabrous, pubescent, or papillose;</text>
      <biological_entity id="o16465" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="width" src="d0_s10" value="broadened" value_original="broadened" />
        <character is_modifier="false" name="texture" src="d0_s10" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers versatile, dehiscence longitudinal;</text>
      <biological_entity id="o16466" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior or inferior, 3-locular or occasionally 1-locular, 3-angled, ovoid, or cylindrical, with axillary or rarely parietal placentation;</text>
      <biological_entity id="o16467" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="position" src="d0_s12" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="rarely" name="placentation" src="d0_s12" value="parietal" value_original="parietal" />
      </biological_entity>
      <biological_entity id="o16468" name="axillary" name_original="axillary" src="d0_s12" type="structure" />
      <relation from="o16467" id="r2249" name="with" negation="false" src="d0_s12" to="o16468" />
    </statement>
    <statement id="d0_s13">
      <text>style included or exserted;</text>
      <biological_entity id="o16469" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas 1 or 3, 3-lobed or capitate;</text>
      <biological_entity id="o16470" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="count" value_original="count" />
        <character is_modifier="false" name="shape" src="d0_s14" value="list" value_original="list" />
        <character name="quantity" src="d0_s14" unit="or punct -lobed or capitate" value="1" value_original="1" />
        <character name="quantity" src="d0_s14" unit="or punct -lobed or capitate" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pedicel usually distinct, articulate or not, rarely absent.</text>
      <biological_entity id="o16471" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="articulate" value_original="articulate" />
        <character name="architecture" src="d0_s15" value="not" value_original="not" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits occasionally baccate, usually capsular and sometimes winged or lobed, or indehiscent and dry or fleshy.</text>
      <biological_entity id="o16472" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s16" value="baccate" value_original="baccate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="texture" src="d0_s16" value="dry" value_original="dry" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 1–3 (–many) per locule, flattened, 3-angled, hemispheric, ovoid, obovoid, or globose.</text>
      <biological_entity id="o16473" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o16474" from="1" name="quantity" src="d0_s17" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s17" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o16474" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, primarily arid, semitropical, subtropical, and tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="primarily arid" establishment_means="native" />
        <character name="distribution" value="semitropical" establishment_means="native" />
        <character name="distribution" value="subtropical" establishment_means="native" />
        <character name="distribution" value="and tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>225.</number>
  <other_name type="common_name">Agave or Century Plant Family</other_name>
  <discussion>Genera 17 or 18, species ca. 550 (9 genera, 84 species in the flora; 2 genera, 6 species introduced).</discussion>
  <discussion>There is little agreement on the treatment of Agavaceae. The group containing Agave, Yucca, Furcraea, Hesperaloe, and Manfreda generally has been accepted as the core of Agavaceae, or as subfamilies Agavoideae and Yuccoideae, but treatment of Dracaena, Sansevieria, Cordyline, Nolina, and Dasylirion has been varied. A. L. Takhtajan (1987) and R. F. Thorne (1992b) placed these genera in Dracaenaceae but treated them at different levels. Takhtajan located them in the subfamily Dracaenoideae within sections Nolineae (Nolina and Dasylirion), Sansevierieae (Sansevieria), and Dracaeneae (Cordyline and Dracaena). Thorne, on the other hand, placed these same groupings at the subfamily level. R. M. T. Dahlgren et al. (1985) recognized them as separate families, Nolinaceae (Nolina and Dasylirion), Dracaenaceae (Sansevieria and Dracaena), and Asteliaceae (Cordyline), in addition to the Agavaceae (Yucca, Hesperaloe, Agave, Manfreda, and Furcraea).</discussion>
  <discussion>A. Cronquist (1981) based his broadly circumscribed Agavaceae on a common xerophytic habit. However, the karyotype of 5 long and 25 short chromosomes for the Agavoideae and Yuccoideae is distinct from the karyotypes of the other subfamilies that Cronquist included in the Agavaceae. Current research on the phylogenetics of moncotyledons, using DNA sequences of rbcL, support the separation of Dracaena, Nolina, and Dasylirion from Agavaceae (M. R. Duvall et al. 1993b). We believe that a broad interpretation of the Agavaceae unites groups that should be recognized as separate.</discussion>
  <discussion>Many genera in Agavaceae are economically important. All genera in the Agavoideae and Yuccoideae contain steroidal sapogenins; some have been used in folk medicine, and locally and commercially as soap (G. Blunden et al. 1978; S. E. Verhoek 1978; M. Wall et al. 1957). They provide fibers for cordage, baskets, and hats, as well as food and drink for many indigenous peoples of the southwestern United States (H. S. Gentry 1982). They are also used as commercial fiber and beverage crops in Latin America and the Old World (H. Brucher 1989). In the southern United States, some species in each genus are cultivated and represented in the flora, and at least one species of Yucca is now grown as far north as Canada. Collectors should record the uses of these plants in their notes along with the critical information on plant habit and morphology. Photographs are often important tools for the identification of these plants, and, with the advent of digital cameras, are now much easier to obtain and process.</discussion>
  <references>
    <reference>Bogler, D. J. and B. B. Simpson. 1995. A chloroplast DNA study of the Agavaceae. Syst. Bot. 20: 191–205. </reference>
    <reference>Bogler, D. J. and B. B. Simpson. 1996. Phylogeny of Agavaceae based on ITS rDNA sequence variation. Amer. J. Bot. 83: 1225–1235. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary inferior.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary superior.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems subterranean; leaf apex flexible.</description>
      <determination>9 Manfreda</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems aboveground; leaf apex rigid.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf apex abruptly narrowed, with long spine; flowers erect or recurved; style subulate, filaments filiform.</description>
      <determination>7 Agave</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf apex blunt; flowers drooping; style and filaments abruptly swollen in proximal half.</description>
      <determination>8 Furcraea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers functionally unisexual; fruits capsular; tepals no longer than 5 mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers bisexual; fruits capsular or baccate; tepals longer than 5 mm.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsules 3-locular, dehiscent; seeds globose; leaf blade not fibrous, margins entire or serrulate.</description>
      <determination>2 Nolina</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsules 1-locular, indehiscent; seeds 3-angled; leaf blade fibrous, margins with sharp, curved prickles.</description>
      <determination>3 Dasylirion</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruits baccate; seeds 1–3.</description>
      <determination>1 Sansevieria</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruits capsular, rarely baccate; seeds many.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Perianth narrowly tubular to broadly campanulate; filaments glabrous; capsules ovoid.</description>
      <determination>6 Hesperaloe</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Perianth campanulate or globose; filaments smooth or papillose; capsules almost never ovoid.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Fruits erect or pendent, usually indehiscent, occasionally dehiscent and septicidal; scape usually less than 2.5 cm diam.; bracts usually ascending.</description>
      <determination>4 Yucca</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Fruits erect, dehiscent and loculidical; scape usually more than 2.5 cm diam.; bracts usually reflexed.</description>
      <determination>5 Hesperoyucca</determination>
    </key_statement>
  </key>
</bio:treatment>