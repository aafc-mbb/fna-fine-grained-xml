<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">gladiolus</taxon_name>
    <taxon_name authority="Van Geel" date="unknown" rank="species">dalenii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">dalenii</taxon_name>
    <taxon_hierarchy>family iridaceae;genus gladiolus;species dalenii;subspecies dalenii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102249</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (50–) 70–120 (–150) cm.</text>
      <biological_entity id="o20272" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms tunicate, (15–) 20–30 mm diam.;</text>
      <biological_entity id="o20273" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s1" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tunic layered, brown, brittle, cartilaginous, outer layer becoming irregularly broken to fibrous, usually bearing numerous tiny cormlets basally.</text>
      <biological_entity id="o20274" name="tunic" name_original="tunic" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="layered" value_original="layered" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="brittle" value_original="brittle" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o20275" name="layer" name_original="layer" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="becoming irregularly" name="condition_or_fragility" src="d0_s2" value="broken" value_original="broken" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o20276" name="cormlet" name_original="cormlets" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="size" src="d0_s2" value="tiny" value_original="tiny" />
      </biological_entity>
      <relation from="o20275" id="r2777" modifier="usually; basally" name="bearing" negation="false" src="d0_s2" to="o20276" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple.</text>
      <biological_entity id="o20277" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves contemporary with flowering-stem [foliage leaves borne later on separate shoots], 4–6 (–7), at least proximalmost 2 ± basal, distalmost 1–2 cauline, ± sheathing, ± 1/2 length of spike;</text>
      <biological_entity id="o20278" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="with flowering-stem" constraintid="o20279" is_modifier="false" name="maturation" src="d0_s4" value="contemporary" value_original="contemporary" />
      </biological_entity>
      <biological_entity id="o20279" name="flowering-stem" name_original="flowering-stem" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o20280" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s4" value="proximalmost" value_original="proximalmost" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20281" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o20282" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" notes="" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character modifier="more or less" name="length" src="d0_s4" value="1/2 length of spike" value_original="1/2 length of spike" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20283" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly lanceolate to ± linear, (5–) 10–20 (–30) mm wide, midribs and margins notably thickened.</text>
      <biological_entity id="o20284" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="more or less linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20285" name="midrib" name_original="midribs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="notably" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o20286" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="notably" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes (2–) 3–7 (–14) -flowered, secund;</text>
      <biological_entity id="o20287" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="(2-)3-7(-14)-flowered" value_original="(2-)3-7(-14)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths subequal, (2.5–) 4–7 cm, inner slightly shorter than outer, apex sometimes dry, pale.</text>
      <biological_entity id="o20288" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s7" value="inner" value_original="inner" />
        <character constraint="than outer" constraintid="o20289" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o20289" name="outer" name_original="outer" src="d0_s7" type="structure" />
      <biological_entity id="o20290" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="condition_or_texture" src="d0_s7" value="dry" value_original="dry" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers unscented;</text>
      <biological_entity id="o20291" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="odor" src="d0_s8" value="unscented" value_original="unscented" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth-tube ± cylindrical, curving outward in distal 1/2, (25–) 35–45 mm;</text>
      <biological_entity id="o20292" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
        <character constraint="in distal 1/2" constraintid="o20293" is_modifier="false" name="course" src="d0_s9" value="curving" value_original="curving" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_distance" notes="" src="d0_s9" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="distance" notes="" src="d0_s9" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20293" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>tepals red to orange with yellow mark on outer 3 tepals (or yellow to greenish and often with red to brown streaks on inner tepals), unequal, inner 3 tepals broadly elliptic, dorsal tepal horizontal to downcurved, 35–50 mm, to 22–30 mm wide, inner lateral tepals usually curving outward distally, ± equaling dorsal, outer 3 tepals curving downward, 20–25 (–30) mm, outermost tepal somewhat longer and narrower than outer lateral tepals;</text>
      <biological_entity id="o20294" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="with mark" constraintid="o20295" from="red" name="coloration" src="d0_s10" to="orange" />
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o20295" name="mark" name_original="mark" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o20296" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="inner dorsal" id="o20297" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o20298" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o20299" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s10" to="downcurved" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s10" to="50" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="width" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner lateral" id="o20300" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s10" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity constraint="dorsal dorsal" id="o20301" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="outer dorsal" id="o20302" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character modifier="distally" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o20303" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="curving" value_original="curving" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="downward" value_original="downward" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o20304" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character constraint="than outer lateral tepals" constraintid="o20305" is_modifier="false" name="width" src="d0_s10" value="somewhat longer and narrower" value_original="somewhat longer and narrower" />
      </biological_entity>
      <biological_entity constraint="outer lateral" id="o20305" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
      <relation from="o20295" id="r2778" name="on" negation="false" src="d0_s10" to="o20296" />
    </statement>
    <statement id="d0_s11">
      <text>filaments ca. 25 mm;</text>
      <biological_entity id="o20306" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="25" value_original="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 12–16 mm;</text>
      <biological_entity id="o20307" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style branching near apex of anthers;</text>
      <biological_entity id="o20308" name="style" name_original="style" src="d0_s13" type="structure">
        <character constraint="near apex" constraintid="o20309" is_modifier="false" name="architecture" src="d0_s13" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o20309" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <biological_entity id="o20310" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o20309" id="r2779" name="part_of" negation="false" src="d0_s13" to="o20310" />
    </statement>
    <statement id="d0_s14">
      <text>branches (4–) 5–6 mm.</text>
      <biological_entity id="o20311" name="branch" name_original="branches" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ellipsoid to ovoid, (18–) 25–35 × 12–14 mm.</text>
      <biological_entity id="o20312" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="ovoid" />
        <character char_type="range_value" from="18" from_unit="mm" name="atypical_length" src="d0_s15" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s15" to="35" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s15" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds broadly winged, 8–12 × 5–9 mm;</text>
      <biological_entity id="o20313" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s16" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s16" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>seed-coats light-brown, glossy.</text>
      <biological_entity id="o20314" name="seed-coat" name_original="seed-coats" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="glossy" value_original="glossy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mainly May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mainly" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Introduced</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="introduced" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., La.; sub-Saharan Africa, Madagascar, Arabia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="sub-Saharan Africa" establishment_means="native" />
        <character name="distribution" value="Madagascar" establishment_means="native" />
        <character name="distribution" value="Arabia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  
</bio:treatment>