<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul Martin Brown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="treatment_page">530</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Schlechter" date="1920" rank="genus">SCHIEDEELLA</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>37: 379. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus schiedeella;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Christian Julius Wilhelm Schiede, 1798–1836, a German naturalist and plant collector in Mexico</other_info_on_name>
    <other_info_on_name type="fna_id">129453</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial.</text>
      <biological_entity id="o21719" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots tuberous, fleshy.</text>
      <biological_entity id="o21720" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, with slender sheaths.</text>
      <biological_entity id="o21721" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="slender" id="o21722" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <relation from="o21721" id="r2963" name="with" negation="false" src="d0_s2" to="o21722" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves senescing before or at anthesis, petiolate;</text>
      <biological_entity id="o21723" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly elliptic.</text>
      <biological_entity id="o21724" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences spikes, few–many-flowered, in spiral or sloping downward from center in all directions.</text>
      <biological_entity constraint="inflorescences" id="o21725" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="few-many-flowered" value_original="few-many-flowered" />
      </biological_entity>
      <biological_entity id="o21726" name="center" name_original="center" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_course" src="d0_s5" value="spiral" value_original="spiral" />
        <character is_modifier="true" name="shape" src="d0_s5" value="sloping" value_original="sloping" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o21727" name="direction" name_original="directions" src="d0_s5" type="structure" />
      <relation from="o21725" id="r2964" name="in" negation="false" src="d0_s5" to="o21726" />
      <relation from="o21726" id="r2965" name="in" negation="false" src="d0_s5" to="o21727" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers resupinate;</text>
      <biological_entity id="o21728" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals similar, connivent;</text>
      <biological_entity id="o21729" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>dorsal sepal adnate to back of column, concave;</text>
      <biological_entity constraint="dorsal" id="o21730" name="sepal" name_original="sepal" src="d0_s8" type="structure">
        <character constraint="to back" constraintid="o21731" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o21731" name="back" name_original="back" src="d0_s8" type="structure" />
      <biological_entity id="o21732" name="column" name_original="column" src="d0_s8" type="structure" />
      <relation from="o21731" id="r2966" name="part_of" negation="false" src="d0_s8" to="o21732" />
    </statement>
    <statement id="d0_s9">
      <text>lateral sepals adnate to column-foot;</text>
      <biological_entity constraint="lateral" id="o21733" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="to column-foot" constraintid="o21734" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o21734" name="column-foot" name_original="column-foot" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals linear;</text>
      <biological_entity id="o21735" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lip with distinct claw, central cinnabar-red disc, and 3 green stripes near apex;</text>
      <biological_entity id="o21736" name="lip" name_original="lip" src="d0_s11" type="structure" />
      <biological_entity id="o21737" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="central" id="o21738" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="cinnabar-red" value_original="cinnabar-red" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character constraint="near apex" constraintid="o21739" is_modifier="false" name="coloration" src="d0_s11" value="green stripes" value_original="green stripes" />
      </biological_entity>
      <biological_entity id="o21739" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <relation from="o21736" id="r2967" name="with" negation="false" src="d0_s11" to="o21737" />
    </statement>
    <statement id="d0_s12">
      <text>column slender, widened toward apex, with decurrent foot at base;</text>
      <biological_entity id="o21740" name="column" name_original="column" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character constraint="toward apex" constraintid="o21741" is_modifier="false" name="width" src="d0_s12" value="widened" value_original="widened" />
      </biological_entity>
      <biological_entity id="o21741" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o21742" name="foot" name_original="foot" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o21743" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o21740" id="r2968" name="with" negation="false" src="d0_s12" to="o21742" />
      <relation from="o21742" id="r2969" name="at" negation="false" src="d0_s12" to="o21743" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas 2, confluent;</text>
      <biological_entity id="o21744" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rostellum linear-triangular, apex acuminate;</text>
      <biological_entity id="o21745" name="rostellum" name_original="rostellum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-triangular" value_original="linear-triangular" />
      </biological_entity>
      <biological_entity id="o21746" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anther ovate-cordate;</text>
      <biological_entity id="o21747" name="anther" name_original="anther" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate-cordate" value_original="ovate-cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pollinia clavate, viscidium oblong, short;</text>
      <biological_entity id="o21748" name="pollinium" name_original="pollinia" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o21749" name="viscidium" name_original="viscidium" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary cyclindric.</text>
      <biological_entity id="o21750" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o21751" name="capsule" name_original="capsules" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Montane tropical regions, Western Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Montane tropical regions" establishment_means="native" />
        <character name="distribution" value="Western Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Species 9 (1 in the flora).</discussion>
  
</bio:treatment>