<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="mention_page">432</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="treatment_page">435</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">baileyi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>6: 114. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species baileyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102057</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="J. M. Webber" date="unknown" rank="species">baileyi</taxon_name>
    <taxon_name authority="(J. M. Webber) J. M. Webber" date="unknown" rank="variety">navajoa</taxon_name>
    <taxon_hierarchy>genus Yucca;species baileyi;variety navajoa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="McKelvey" date="unknown" rank="species">navajoa</taxon_name>
    <taxon_hierarchy>genus Yucca;species navajoa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">standleyi</taxon_name>
    <taxon_hierarchy>genus Yucca;species standleyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or cespitose, forming colonies 1.3–2 m diam., acaulescent, erect or semierect, up to 0.2 m;</text>
      <biological_entity id="o6324" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="1.3" from_unit="m" name="diameter" src="d0_s0" to="2" to_unit="m" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="semierect" value_original="semierect" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="0.2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes usually small, symmetrical.</text>
      <biological_entity id="o6325" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="small" value_original="small" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="symmetrical" value_original="symmetrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade yellowish green, planoconvex or plano-keeled, occasionally falcate, widest near middle, 25–45 (–50) × 0.6–0.9 cm, rigid, smooth adaxially and abaxially, margins entire, recurved, filiferous, whitish, apex spinose, spine acicular, to 3.2 mm.</text>
      <biological_entity id="o6326" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plano-keeled" value_original="plano-keeled" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character constraint="near middle" constraintid="o6327" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" notes="" src="d0_s2" to="45" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="adaxially" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6327" name="middle" name_original="middle" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" notes="" src="d0_s2" to="0.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6328" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o6329" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="spinose" value_original="spinose" />
      </biological_entity>
      <biological_entity id="o6330" name="spine" name_original="spine" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acicular" value_original="acicular" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemose, arising within or just beyond rosettes, 2.5–4.5 (–8.5) dm;</text>
      <biological_entity id="o6331" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character constraint="beyond rosettes" constraintid="o6332" is_modifier="false" name="orientation" src="d0_s3" value="arising" value_original="arising" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="8.5" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="4.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o6332" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracts erect, purplish;</text>
      <biological_entity id="o6333" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle scapelike, 0.1–0.2 m, less than 2.5 cm diam.</text>
      <biological_entity id="o6334" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s5" to="0.2" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pendent;</text>
      <biological_entity id="o6335" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth campanulate;</text>
      <biological_entity id="o6336" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals distinct, ovate to obovate or elliptic, 5–6.5 × 1.5–3.2 cm;</text>
      <biological_entity id="o6337" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="obovate or elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s8" to="3.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments to 2 cm, finely pubescent;</text>
      <biological_entity id="o6338" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil green, 2.5–3.2 × 0.8 cm;</text>
      <biological_entity id="o6339" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s10" to="3.2" to_unit="cm" />
        <character name="width" src="d0_s10" unit="cm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style white, 7 mm;</text>
      <biological_entity id="o6340" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas lobed.</text>
      <biological_entity id="o6341" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits erect, capsular, dehiscent, oblong-cylindric, not usually constricted, 5 × 2.5 cm, dehiscence septicidal.</text>
      <biological_entity id="o6342" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" modifier="not usually" name="size" src="d0_s13" value="constricted" value_original="constricted" />
        <character name="dehiscence" src="d0_s13" unit="cm" value="5" value_original="5" />
        <character name="dehiscence" src="d0_s13" unit="cm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds dull black, thin, 6–10 mm.</text>
      <biological_entity id="o6343" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s14" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountains, adjacent woodlands and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountains" />
        <character name="habitat" value="adjacent woodlands" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Alpine yucca</other_name>
  <discussion>Some populations of Yucca baileyi comprise compact colonies of semierect, branched plants instead of scattered, cespitose individuals. These populations were recognized as var. navajoa by J. M. Webber (1953), who noted that Y. baileyi possibly hybridizes with Y. glauca and Y. angustissima.</discussion>
  
</bio:treatment>