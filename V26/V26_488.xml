<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Brandegee" date="1906" rank="species">praecox</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>5: 228. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species praecox</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101392</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="Curran" date="unknown" rank="species">hyalinum</taxon_name>
    <taxon_name authority="(Brandegee) Jepson" date="unknown" rank="variety">praecox</taxon_name>
    <taxon_hierarchy>genus Allium;species hyalinum;variety praecox;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–3, increase bulbs absent or ± equaling parent bulbs, never appearing as basal cluster, not clustered on stout primary rhizome, ovoid to globose, 1–1.8 × 1–1.7 cm;</text>
      <biological_entity id="o31442" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="3" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character char_type="range_value" from="ovoid" modifier="never" name="shape" src="d0_s0" to="globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31443" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <biological_entity constraint="parent" id="o31444" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s0" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31445" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <biological_entity constraint="basal primary" id="o31446" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
        <character is_modifier="true" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o31442" id="r4242" name="increase" negation="false" src="d0_s0" to="o31443" />
      <relation from="o31442" id="r4243" modifier="never" name="appearing as" negation="false" src="d0_s0" to="o31445" />
      <relation from="o31442" id="r4244" modifier="never" name="appearing as" negation="false" src="d0_s0" to="o31446" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, gray-brown to brown, prominently cellular-reticulate, membranous, cells arranged in ± vertical rows, forming irregular herringbone pattern, transversely elongate, V-shaped, without fibers;</text>
      <biological_entity constraint="outer" id="o31447" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character constraint="in rows" constraintid="o31449" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o31448" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character char_type="range_value" from="gray-brown" is_modifier="true" name="coloration" src="d0_s1" to="brown" />
        <character is_modifier="true" modifier="prominently" name="architecture_or_coloration_or_relief" src="d0_s1" value="cellular-reticulate" value_original="cellular-reticulate" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o31449" name="row" name_original="rows" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="more or less" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o31450" name="pattern" name_original="pattern" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s1" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="arrangement" src="d0_s1" value="herringbone" value_original="herringbone" />
      </biological_entity>
      <biological_entity id="o31451" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o31447" id="r4245" name="enclosing" negation="false" src="d0_s1" to="o31448" />
      <relation from="o31447" id="r4246" name="forming" negation="false" src="d0_s1" to="o31450" />
      <relation from="o31447" id="r4247" name="without" negation="false" src="d0_s1" to="o31451" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white, cells obscure, ± transversely elongate, contorted.</text>
      <biological_entity constraint="inner" id="o31452" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o31453" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="more or less transversely" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, withering from tip by anthesis, 2–3, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o31454" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="from tip" constraintid="o31455" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o31455" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" modifier="by anthesis" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o31456" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o31457" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o31458" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o31456" id="r4248" name="extending" negation="false" src="d0_s3" to="o31457" />
      <relation from="o31456" id="r4249" name="extending" negation="false" src="d0_s3" to="o31458" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, broadly channeled, carinate, 20–75 cm × 5–10 mm, margins entire.</text>
      <biological_entity id="o31459" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="75" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31460" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, solid, terete, 20–60 cm × 2–4 mm.</text>
      <biological_entity id="o31461" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="60" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel shattering after seeds mature, each flower deciduous with its pedicel as a unit, erect, loose, 5–40-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o31462" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o31463" name="seed" name_original="seeds" src="d0_s6" type="structure" />
      <biological_entity id="o31464" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character constraint="with pedicel" constraintid="o31465" is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-40-flowered" value_original="5-40-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o31465" name="pedicel" name_original="pedicel" src="d0_s6" type="structure" />
      <biological_entity id="o31466" name="unit" name_original="unit" src="d0_s6" type="structure" />
      <biological_entity id="o31467" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
      <relation from="o31462" id="r4250" name="shattering after" negation="false" src="d0_s6" to="o31463" />
      <relation from="o31465" id="r4251" name="as" negation="false" src="d0_s6" to="o31466" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2, 5–7-veined, lanceolate to lanceovate, ± equal, apex acuminate.</text>
      <biological_entity constraint="spathe" id="o31468" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-7-veined" value_original="5-7-veined" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="lanceovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o31469" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers stellate, 6–13 mm;</text>
      <biological_entity id="o31470" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals spreading at anthesis, white to pale-pink with purple midveins, lanceovate to ovate, ± equal, becoming papery and connivent over capsule, margins entire, apex acute, obtuse, or emarginate;</text>
      <biological_entity id="o31471" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character constraint="at capsule, margins, apex" constraintid="o31473, o31474, o31475" is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o31472" name="midvein" name_original="midveins" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="anthesis" value_original="anthesis" />
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s9" to="pale-pink" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s9" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31473" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o31474" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o31475" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o31476" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers purple and yellow;</text>
      <biological_entity id="o31477" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple and yellow" value_original="purple and yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o31478" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crested;</text>
      <biological_entity id="o31479" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 3, central, 2-lobed, minute, margins entire;</text>
      <biological_entity id="o31480" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="size" src="d0_s14" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o31481" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o31482" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o31483" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, scarcely thickened, unlobed;</text>
      <biological_entity id="o31484" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 15–40 mm.</text>
      <biological_entity id="o31485" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s17" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat shining;</text>
      <biological_entity id="o31486" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells minutely roughened.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o31487" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31488" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clay soil on shaded, grassy slopes near coast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay soil" constraint="on shaded , grassy slopes" />
        <character name="habitat" value="shaded" />
        <character name="habitat" value="grassy slopes" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  
</bio:treatment>