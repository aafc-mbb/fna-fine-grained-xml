<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="treatment_page">159</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erythronium</taxon_name>
    <taxon_name authority="Shevock" date="1991" rank="species">pluriflorum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>37: 268, fig. 3. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus erythronium;species pluriflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101600</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs ± ovoid, 40–75 mm.</text>
      <biological_entity id="o34609" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s0" to="75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 7–30 cm;</text>
      <biological_entity id="o34610" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade green, oblanceolate to elliptic, margins ± wavy.</text>
      <biological_entity id="o34611" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="elliptic" />
      </biological_entity>
      <biological_entity id="o34612" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape 8–35 cm.</text>
      <biological_entity id="o34613" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–10-flowered.</text>
      <biological_entity id="o34614" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-10-flowered" value_original="1-10-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals yellow, bronze in age, lanceolate, 15–28 mm, not auriculate at base;</text>
      <biological_entity id="o34615" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o34616" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character constraint="in age" constraintid="o34617" is_modifier="false" name="coloration" src="d0_s5" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="28" to_unit="mm" />
        <character constraint="at base" constraintid="o34618" is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o34617" name="age" name_original="age" src="d0_s5" type="structure" />
      <biological_entity id="o34618" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stamens 8–12 mm;</text>
      <biological_entity id="o34619" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o34620" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments yellow, slender;</text>
      <biological_entity id="o34621" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o34622" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow;</text>
      <biological_entity id="o34623" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34624" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style yellow, 6–8 mm;</text>
      <biological_entity id="o34625" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34626" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma unlobed or with very short, rounded lobes shorter than 1 mm.</text>
      <biological_entity id="o34627" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34628" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="with very short , rounded lobes" />
      </biological_entity>
      <biological_entity id="o34629" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="very" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character modifier="shorter than" name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o34628" id="r4657" name="with" negation="false" src="d0_s10" to="o34629" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules obovoid, 2–4 cm.</text>
      <biological_entity id="o34630" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open montane coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open montane" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2300–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Golden fawn-lily</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erythronium pluriflorum is known only from Madera County.</discussion>
  
</bio:treatment>