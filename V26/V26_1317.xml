<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gustavo A. Romero-González</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="treatment_page">641</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Eulophiinae</taxon_name>
    <taxon_name authority="Reichenbach f." date="1878" rank="genus">PTEROGLOSSASPIS</taxon_name>
    <place_of_publication>
      <publication_title>Otia Bot. Hamburg.,</publication_title>
      <place_in_publication>67. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe eulophiinae;genus pteroglossaspis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pteron, wing, glossa, tongue, and aspis, shield</other_info_on_name>
    <other_info_on_name type="fna_id">127472</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small &amp; Nash" date="unknown" rank="genus">Triorchos</taxon_name>
    <taxon_hierarchy>genus Triorchos;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, terrestrial, cespitose.</text>
      <biological_entity id="o16195" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems cormlike pseudobulbs.</text>
      <biological_entity id="o16196" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16197" name="pseudobulb" name_original="pseudobulbs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="cormlike" value_original="cormlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves wilting at end of growing season, basal, nearly petiolate;</text>
      <biological_entity id="o16198" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at end" constraintid="o16199" is_modifier="false" name="life_cycle" src="d0_s2" value="wilting" value_original="wilting" />
      </biological_entity>
      <biological_entity id="o16199" name="end" name_original="end" src="d0_s2" type="structure" />
      <biological_entity id="o16200" name="season" name_original="season" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o16201" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <relation from="o16199" id="r2208" name="part_of" negation="false" src="d0_s2" to="o16200" />
    </statement>
    <statement id="d0_s3">
      <text>blade plicate, not articulate with leaf-sheaths.</text>
      <biological_entity id="o16202" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character constraint="with leaf-sheaths" constraintid="o16203" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o16203" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences lateral, from base of pseudobulb, racemes, erect.</text>
      <biological_entity id="o16204" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o16205" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o16206" name="pseudobulb" name_original="pseudobulb" src="d0_s4" type="structure" />
      <biological_entity id="o16207" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o16204" id="r2209" name="from" negation="false" src="d0_s4" to="o16205" />
      <relation from="o16205" id="r2210" name="part_of" negation="false" src="d0_s4" to="o16206" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers resupinate, inconspicuous;</text>
      <biological_entity id="o16208" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals and petals converging [usually spreading], distinct and free, nearly equal;</text>
      <biological_entity id="o16209" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="converging" value_original="converging" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o16210" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="converging" value_original="converging" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lip spreading, sessile at base of column, 3-lobed;</text>
      <biological_entity id="o16211" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="at base" constraintid="o16212" is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o16212" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o16213" name="column" name_original="column" src="d0_s7" type="structure" />
      <relation from="o16212" id="r2211" name="part_of" negation="false" src="d0_s7" to="o16213" />
    </statement>
    <statement id="d0_s8">
      <text>disc smooth, without spurs;</text>
      <biological_entity id="o16214" name="disc" name_original="disc" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o16215" name="spur" name_original="spurs" src="d0_s8" type="structure" />
      <relation from="o16214" id="r2212" name="without" negation="false" src="d0_s8" to="o16215" />
    </statement>
    <statement id="d0_s9">
      <text>column erect, wingless, stout, 3–4 mm, base 2-auriculate, apex blunt;</text>
      <biological_entity id="o16216" name="column" name_original="column" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="wingless" value_original="wingless" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16217" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="2-auriculate" value_original="2-auriculate" />
      </biological_entity>
      <biological_entity id="o16218" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anther terminal, incumbent, operculate, 1-locular;</text>
      <biological_entity id="o16219" name="anther" name_original="anther" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s10" value="incumbent" value_original="incumbent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="operculate" value_original="operculate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinia 2, yellow, spheric, sulcate, cartilaginous;</text>
      <biological_entity id="o16220" name="pollinium" name_original="pollinia" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s11" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stipe semilunar;</text>
      <biological_entity id="o16221" name="stipe" name_original="stipe" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="semilunar" value_original="semilunar" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>viscidium present.</text>
      <biological_entity id="o16222" name="viscidium" name_original="viscidium" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, erect when mature.</text>
      <biological_entity constraint="fruits" id="o16223" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="when mature" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Neotropical regions; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Neotropical regions" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60.</number>
  <discussion>Species 7–10 (1 in the flora).</discussion>
  <references>
    <reference>Rolfe, R. A. 1898. Pteroglossaspis. In: D. Oliver et al., eds. 1868–1937. Flora of Tropical Africa…. 10 vols. London. Vol. 7, pp. 99–100.   </reference>
    <reference>Romero-G., G. A. 1993. Notes on Pteroglossaspis (Orchidaceae), a new genus for the flora of Colombia. Orquidea (Mexico City) 13: 275–280. </reference>
    <reference>Wood, J. J. 1989. Pteroglossaspis. In: W. B. Turrill et al., eds. 1952+. Flora of Tropical East Africa. 152+ vols. London and Rotterdam. Orchidaceae, part 3, pp. 480–482.</reference>
  </references>
  
</bio:treatment>