<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="treatment_page">123</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="(Bentham) Alph. Wood" date="1868" rank="species">pulchellus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>20: 168. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species pulchellus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101491</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyclobothra</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">pulchella</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Hort. Soc. London, ser.</publication_title>
      <place_in_publication>2, 1: 412, plate 14, fig. 1. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cyclobothra;species pulchella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually branching distal to base, 1–3 dm, not glaucous.</text>
      <biological_entity id="o27007" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character constraint="to base" constraintid="o27008" is_modifier="false" name="position_or_shape" src="d0_s0" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o27008" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal persistent, 1–4 dm;</text>
      <biological_entity id="o27009" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o27010" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade green, lanceolate, tapering at base, flat, not glaucous.</text>
      <biological_entity id="o27011" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o27012" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="at base" constraintid="o27013" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o27013" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1–5-flowered.</text>
      <biological_entity id="o27014" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers nodding;</text>
      <biological_entity id="o27015" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth closed at apex, ± globular;</text>
      <biological_entity id="o27016" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="globular" value_original="globular" />
      </biological_entity>
      <biological_entity id="o27017" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o27016" id="r3676" name="closed at" negation="false" src="d0_s5" to="o27017" />
    </statement>
    <statement id="d0_s6">
      <text>sepals spreading, yellow-green, ovate to lanceolate, 2–3 cm, glabrous, apex acuminate;</text>
      <biological_entity id="o27018" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27019" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals pale-yellow, lanceolate, 3–5 cm, conspicuously fringed with short, thick hairs, adaxial surface glabrous or sparsely hairy, apex obtuse;</text>
      <biological_entity id="o27020" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character constraint="with hairs" constraintid="o27021" is_modifier="false" modifier="conspicuously" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o27021" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27022" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o27023" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>glands deeply depressed, bordered distally by long, slender hairs;</text>
      <biological_entity id="o27024" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="depressed" value_original="depressed" />
        <character constraint="by slender hairs" constraintid="o27025" is_modifier="false" name="architecture" src="d0_s8" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity constraint="slender" id="o27025" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 6–8 mm;</text>
      <biological_entity id="o27026" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers oblong, 3–5 mm, apex obtuse or acute.</text>
      <biological_entity id="o27027" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27028" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules nodding, winged, ellipsoid-oblong, 2–4 cm.</text>
      <biological_entity id="o27029" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid-oblong" value_original="ellipsoid-oblong" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark-brown, irregular.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20.</text>
      <biological_entity id="o27030" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s12" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27031" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open places in woods, chaparral, typically on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open places" constraint="in woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Mount Diablo fairy lantern</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Calochortus pulchellus is known from Mt. Diablo, Contra Costa County.</discussion>
  
</bio:treatment>