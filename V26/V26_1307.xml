<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">634</other_info_on_meta>
    <other_info_on_meta type="mention_page">636</other_info_on_meta>
    <other_info_on_meta type="mention_page">638</other_info_on_meta>
    <other_info_on_meta type="treatment_page">637</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="(Rafinesque) Rafinesque" date="1817" rank="species">maculata</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>2: 119. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species maculata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101533</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cladorhiza</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">maculata</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>1: 429. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cladorhiza;species maculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ± strongly thickened, base not bulbous.</text>
      <biological_entity id="o6611" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less strongly" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o6612" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: racemes dense to lax, 10–65 × 1.5–6.5 cm.</text>
      <biological_entity id="o6613" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o6614" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="65" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 6–41, conspicuous;</text>
      <biological_entity id="o6615" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="41" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth open;</text>
      <biological_entity id="o6616" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals directed forward to somewhat spreading, brown, tan, red, or yellow, often darker distally, lanceolate, 3-veined, 4.7–15 mm;</text>
      <biological_entity id="o6617" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="directed" value_original="directed" />
        <character is_modifier="false" modifier="forward to somewhat" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s4" value="darker" value_original="darker" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals frequently curved toward column, nearly adhering to it, tan to yellowish, often red distally, usually spotted with purple, lanceolate to oblanceolate, 4.5–11.5 mm;</text>
      <biological_entity id="o6618" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character constraint="toward column" constraintid="o6619" is_modifier="false" modifier="frequently" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="tan" modifier="nearly" name="coloration" notes="" src="d0_s5" to="yellowish" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="spotted with purple" value_original="spotted with purple" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="11.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6619" name="column" name_original="column" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>lip white, usually spotted with purple, obovate to elliptic, thin, lateral lobes 2, rounded, middle lobe oblong to broadly dilated toward apex, with 2 distinct basal lamellae, 4–9 × 1.5–6 mm;</text>
      <biological_entity id="o6620" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="spotted with purple" value_original="spotted with purple" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="elliptic" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6621" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="middle" id="o6622" name="lobe" name_original="lobe" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="toward apex" constraintid="o6623" from="oblong" name="shape" src="d0_s6" to="broadly dilated" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6623" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o6624" name="lamella" name_original="lamellae" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o6622" id="r945" name="with" negation="false" src="d0_s6" to="o6624" />
    </statement>
    <statement id="d0_s7">
      <text>column whitish yellow, spotted with purple, usually curved, 3.3–7.8 mm, basally with ridges or poorly developed auricles;</text>
      <biological_entity id="o6625" name="column" name_original="column" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish yellow" value_original="whitish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="spotted with purple" value_original="spotted with purple" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s7" to="7.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6626" name="ridge" name_original="ridges" src="d0_s7" type="structure" />
      <biological_entity id="o6627" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="poorly" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o6625" id="r946" modifier="basally" name="with" negation="false" src="d0_s7" to="o6626" />
      <relation from="o6625" id="r947" modifier="basally" name="with" negation="false" src="d0_s7" to="o6627" />
    </statement>
    <statement id="d0_s8">
      <text>ovary 5–14 mm;</text>
      <biological_entity id="o6628" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mentum obscure, adnate to edge of ovary.</text>
      <biological_entity id="o6629" name="mentum" name_original="mentum" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="obscure" value_original="obscure" />
        <character constraint="to edge" constraintid="o6630" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o6630" name="edge" name_original="edge" src="d0_s9" type="structure" />
      <biological_entity id="o6631" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <relation from="o6630" id="r948" name="part_of" negation="false" src="d0_s9" to="o6631" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules pendulous, ellipsoid, 9–24 × 5–9 mm. 2n = 42.</text>
      <biological_entity id="o6632" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="24" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6633" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Spotted coral-root</other_name>
  <other_name type="common_name">corallorhize maculée</other_name>
  <discussion>several color forms exist in corallorhiza maculata, often occurring in proximity and sometimes forming large clumps. white-flowered southern plants have occasionally been misidentified as corallorhiza trifida. dance-flies (empis).have been reported as pollinators (j. l. kipping 1971)</discussion>
  <discussion>Varieties 3 (2 in the flora).</discussion>
  <references>
    <reference>  Freudenstein, J. V. 1987. A preliminary study of Corallorhiza maculata (Orchidaceae) in eastern North America. Contr. Univ. Michigan Herb. 16: 145–153. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Middle lobe of lip expanded slightly or not at all distally, ratio of widths of dilated part to base of middle lobe less than 1.5; floral bracts averaging 0.5–1 mm.</description>
      <determination>6a Corallorhiza maculata var. maculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Middle lobe of lip distinctly expanded, ratio of widths of dilated part to base of middle lobe more than 1.5; floral bracts averaging 1–2.8 mm.</description>
      <determination>6b Corallorhiza maculata var. occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>