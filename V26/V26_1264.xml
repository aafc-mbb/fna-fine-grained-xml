<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James D. Ackerman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">618</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1837" rank="tribe">Dendrobieae</taxon_name>
    <taxon_name authority="Thouars" date="1822" rank="genus">BULBOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Orchid., table</publication_title>
      <place_in_publication>3. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe dendrobieae;genus bulbophyllum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek bolbos, bulb, and phyllon, leaf, referring to its leafy pseudobulb</other_info_on_name>
    <other_info_on_name type="fna_id">104827</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, epiphytic or lithophytic, sympodial.</text>
      <biological_entity id="o26022" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="lithophytic" value_original="lithophytic" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="sympodial" value_original="sympodial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots velamentous, glabrous.</text>
      <biological_entity id="o26023" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems rhizomatous, covered by scarious sheaths;</text>
      <biological_entity id="o26024" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity id="o26025" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o26024" id="r3543" name="covered by" negation="false" src="d0_s2" to="o26025" />
    </statement>
    <statement id="d0_s3">
      <text>pseudobulbs often angled, ovoid, stout.</text>
      <biological_entity id="o26026" name="pseudobulb" name_original="pseudobulbs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves conduplicate, articulate, thin to leathery.</text>
      <biological_entity id="o26027" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s4" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences lateral from base of pseudobulb, spikes [racemes], erect to pendent, pedunculate, rachis swollen, fleshy.</text>
      <biological_entity id="o26028" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="from base" constraintid="o26029" is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o26029" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o26030" name="pseudobulb" name_original="pseudobulb" src="d0_s5" type="structure" />
      <biological_entity id="o26031" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o26032" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o26029" id="r3544" name="part_of" negation="false" src="d0_s5" to="o26030" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers resupinate, sessile;</text>
      <biological_entity id="o26033" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>dorsal sepal distinct and free;</text>
      <biological_entity constraint="dorsal" id="o26034" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals adnate to column-foot forming mentum;</text>
      <biological_entity constraint="lateral" id="o26035" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="to column-foot" constraintid="o26036" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o26036" name="column-foot" name_original="column-foot" src="d0_s8" type="structure" />
      <biological_entity id="o26037" name="mentum" name_original="mentum" src="d0_s8" type="structure" />
      <relation from="o26036" id="r3545" name="forming" negation="false" src="d0_s8" to="o26037" />
    </statement>
    <statement id="d0_s9">
      <text>petals distinct and free, smaller than sepals;</text>
      <biological_entity id="o26038" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character constraint="than sepals" constraintid="o26039" is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o26039" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lip fleshy;</text>
      <biological_entity id="o26040" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>column erect with aristate terminal teeth or wings;</text>
      <biological_entity id="o26041" name="column" name_original="column" src="d0_s11" type="structure">
        <character constraint="with wings" constraintid="o26043" is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26042" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity id="o26043" name="wing" name_original="wings" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anther terminal, incumbent, operculate;</text>
      <biological_entity id="o26044" name="anther" name_original="anther" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s12" value="incumbent" value_original="incumbent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="operculate" value_original="operculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollinia 2 or 4, hard, waxy;</text>
      <biological_entity id="o26045" name="pollinium" name_original="pollinia" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s13" unit="or" value="4" value_original="4" />
        <character is_modifier="false" name="texture" src="d0_s13" value="hard" value_original="hard" />
        <character is_modifier="false" name="texture" src="d0_s13" value="ceraceous" value_original="waxy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma entire;</text>
      <biological_entity id="o26046" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary sessile [pedicellate].</text>
      <biological_entity id="o26047" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, beakless.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 19, 20.</text>
      <biological_entity constraint="fruits" id="o26048" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity constraint="x" id="o26049" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="19" value_original="19" />
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pantropical, mostly Asia and Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pantropical" establishment_means="native" />
        <character name="distribution" value="mostly Asia and Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <discussion>Species 1000–1200 (1 in the flora).</discussion>
  <references>
    <reference>Vermeulen, J. J. 1987. A taxonomic revision of the continental African Bulbophyllinae. Orchid Monogr. 2.  </reference>
    <reference>Vermeulen, J. J. 1993. A taxonomic revision of Bulbophyllum, sections Adelopetalum, Lepanthanthe, Macrouris, Pelma, Peltopus, and Uncifera (Orchidaceae). Orchid Monogr. 7.</reference>
  </references>
  
</bio:treatment>