<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="treatment_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">canadense</taxon_name>
    <taxon_name authority="(Bush) Ownbey" date="1951" rank="variety">hyacinthoides</taxon_name>
    <place_of_publication>
      <publication_title>Res. Stud. State Coll. Wash.</publication_title>
      <place_in_publication>18: 191. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species canadense;variety hyacinthoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102143</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="Bush" date="unknown" rank="species">hyacinthoides</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>17: 119. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Allium;species hyacinthoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs without basal bulbels, 1.5–2.5 × 2–3 cm;</text>
      <biological_entity id="o25406" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" notes="" src="d0_s0" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o25407" name="bulbel" name_original="bulbels" src="d0_s0" type="structure" />
      <relation from="o25406" id="r3442" name="without" negation="false" src="d0_s0" to="o25407" />
    </statement>
    <statement id="d0_s1">
      <text>inner coat cells obscure, regular or nearly so.</text>
      <biological_entity constraint="coat" id="o25408" name="cell" name_original="cells" src="d0_s1" type="structure" constraint_original="inner coat">
        <character is_modifier="false" name="prominence" src="d0_s1" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="regular" value_original="regular" />
        <character name="architecture" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–6;</text>
      <biological_entity id="o25409" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 2–7 mm wide, margins entire or denticulate.</text>
      <biological_entity id="o25410" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25411" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Scape solitary, 15–30 cm.</text>
      <biological_entity id="o25412" name="scape" name_original="scape" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Umbel 25–60-flowered, producing capsules and seeds, bulbils almost unknown;</text>
      <biological_entity id="o25413" name="umbel" name_original="umbel" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="25-60-flowered" value_original="25-60-flowered" />
      </biological_entity>
      <biological_entity id="o25414" name="capsule" name_original="capsules" src="d0_s5" type="structure" />
      <biological_entity id="o25415" name="seed" name_original="seeds" src="d0_s5" type="structure" />
      <biological_entity id="o25416" name="bulbil" name_original="bulbils" src="d0_s5" type="structure" />
      <relation from="o25413" id="r3443" name="producing capsules and seeds" negation="false" src="d0_s5" to="o25414" />
      <relation from="o25413" id="r3444" name="producing capsules and seeds" negation="false" src="d0_s5" to="o25415" />
    </statement>
    <statement id="d0_s6">
      <text>spathe bracts 3–4, lanceolate.</text>
      <biological_entity constraint="spathe" id="o25417" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers fragrant, 5–7 mm;</text>
      <biological_entity id="o25418" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="odor" src="d0_s7" value="fragrant" value_original="fragrant" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals erect, pink, thin;</text>
      <biological_entity id="o25419" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel not filiform, subequal, 15–20 mm. 2n = 14.</text>
      <biological_entity id="o25420" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25421" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On calcareous prairies or more infrequently in sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous prairies" modifier="on" />
        <character name="habitat" value="more infrequently" constraint="in sandy soils" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3c.</number>
  
</bio:treatment>