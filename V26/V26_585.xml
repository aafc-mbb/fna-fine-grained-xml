<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">301</other_info_on_meta>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="(Cory) Traub" date="1951" rank="species">jonesii</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>7: 42. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species jonesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102084</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cooperia</taxon_name>
    <taxon_name authority="Cory" date="unknown" rank="species">jonesii</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>18: 45. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cooperia;species jonesii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade dull green, to 4.5 mm wide.</text>
      <biological_entity id="o24188" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe 2.1–4 cm.</text>
      <biological_entity id="o24189" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect;</text>
      <biological_entity id="o24190" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth pale lemon yellow, salverform to funnelform, 2–12.5 cm;</text>
      <biological_entity id="o24191" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale lemon" value_original="pale lemon" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
        <character constraint="to funnelform , 2-12.5 cm" is_modifier="false" name="shape" src="d0_s3" value="salverform" value_original="salverform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube yellow with some green, 1.5–7.2 cm, diam. primarily uniform, ca. 2/3–3/4 perianth length, ca. 8–10 times filament length, longer than spathe;</text>
      <biological_entity id="o24192" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow with some green" value_original="yellow with some green" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="distance" src="d0_s4" to="7.2" to_unit="cm" />
        <character is_modifier="false" modifier="primarily" name="diam" src="d0_s4" value="uniform" value_original="uniform" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o24193" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character constraint="filament" constraintid="o24194" is_modifier="false" name="length" src="d0_s4" value="8-10 times filament length" value_original="8-10 times filament length" />
        <character constraint="than spathe" constraintid="o24195" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o24194" name="filament" name_original="filament" src="d0_s4" type="structure" />
      <biological_entity id="o24195" name="spathe" name_original="spathe" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>tepals often reflexed;</text>
      <biological_entity id="o24196" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens fasciculate, appearing equal;</text>
      <biological_entity id="o24197" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments subulate, 0.15–0.7 cm, apex acute;</text>
      <biological_entity id="o24198" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="some_measurement" src="d0_s7" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24199" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 4–7 mm;</text>
      <biological_entity id="o24200" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style longer than perianth-tube;</text>
      <biological_entity id="o24201" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o24202" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o24202" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma capitate, among to exserted beyond anthers;</text>
      <biological_entity id="o24203" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o24204" name="exserted" name_original="exserted" src="d0_s10" type="structure" />
      <biological_entity id="o24205" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o24203" id="r3288" name="among" negation="false" src="d0_s10" to="o24204" />
      <relation from="o24204" id="r3289" name="beyond" negation="false" src="d0_s10" to="o24205" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel 0–0.6 cm, shorter than spathe.</text>
      <biological_entity id="o24207" name="spathe" name_original="spathe" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 48, 72.</text>
      <biological_entity id="o24206" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s11" to="0.6" to_unit="cm" />
        <character constraint="than spathe" constraintid="o24207" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24208" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
        <character name="quantity" src="d0_s12" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–early fall (Jul–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Primarily low, sandy loam, open fields, swales, ditches, coastal bends</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loam" modifier="primarily low" />
        <character name="habitat" value="open fields" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="coastal bends" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Morphology, cytology, and distribution suggest that Zephyranthes jonesii, like Z. smallii, arose from hybridization between Z. pulchella (2n = 48) and Z. chlorosolen (2n = 48).</discussion>
  
</bio:treatment>