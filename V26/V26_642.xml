<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="genus">brodiaea</taxon_name>
    <taxon_name authority="Hoover" date="1937" rank="species">appendiculata</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>4: 130, fig. 1. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus brodiaea;species appendiculata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101434</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Scape 10–45 cm, stout.</text>
      <biological_entity id="o30761" name="scape" name_original="scape" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers 24–38 mm;</text>
      <biological_entity id="o30762" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s1" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>perianth violet purple, tube cylindrical, 8–12 mm, translucent, splitting in fruit, lobes ascending, recurved distally, 15–22 mm, usually less than twice length of tube;</text>
      <biological_entity id="o30763" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="violet purple" value_original="violet purple" />
      </biological_entity>
      <biological_entity id="o30764" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s2" value="translucent" value_original="translucent" />
        <character constraint="in fruit" constraintid="o30765" is_modifier="false" name="architecture_or_dehiscence" src="d0_s2" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o30765" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <biological_entity id="o30766" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="22" to_unit="mm" />
        <character constraint="tube" constraintid="o30767" is_modifier="false" name="length" src="d0_s2" value="0-2 times length of tube" />
      </biological_entity>
      <biological_entity id="o30767" name="tube" name_original="tube" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>filaments 4–7 mm, base not triangular, with 2 threadlike, forked appendages;</text>
      <biological_entity id="o30768" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30769" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o30770" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s3" value="thread-like" value_original="threadlike" />
        <character is_modifier="true" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
      <relation from="o30769" id="r4141" name="with" negation="false" src="d0_s3" to="o30770" />
    </statement>
    <statement id="d0_s4">
      <text>anthers obcordate, 3–6 mm, apex hooked;</text>
      <biological_entity id="o30771" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30772" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminodia erect, usually white, narrowly linear, 8–15 mm, margins 1/2 involute, wavy, apex rounded;</text>
      <biological_entity id="o30773" name="staminodium" name_original="staminodia" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30774" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o30775" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 5–6 mm;</text>
      <biological_entity id="o30776" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 9–12 mm;</text>
      <biological_entity id="o30777" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 4–10 cm. 2n = 12.</text>
      <biological_entity id="o30778" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30779" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands, gravelly clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="gravelly clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>This uncommon species is found at low altitudes in the Sierra foothills of central and northern California, usually in stony, red clay soils that become baked very hard during the flowering season. It is rare or extirpated from coast-range foothills.</discussion>
  
</bio:treatment>