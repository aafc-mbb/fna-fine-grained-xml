<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="treatment_page">387</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Californicae</taxon_name>
    <taxon_name authority="R. C. Foster" date="1938" rank="species">fernaldii</taxon_name>
    <place_of_publication>
      <publication_title>Iridis Sp. Nov.,</publication_title>
      <place_in_publication>1. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series californicae;species fernaldii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101700</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes many-branched, producing dense clumps, compact, slender, ca. 0.6 cm diam., base covered with remains of old leaves;</text>
      <biological_entity id="o33844" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="many-branched" value_original="many-branched" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="diameter" src="d0_s0" unit="cm" value="0.6" value_original="0.6" />
      </biological_entity>
      <biological_entity id="o33845" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o33846" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o33847" name="remains" name_original="remains" src="d0_s0" type="structure" />
      <biological_entity id="o33848" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <relation from="o33844" id="r4562" name="producing" negation="false" src="d0_s0" to="o33845" />
      <relation from="o33846" id="r4563" name="covered with" negation="false" src="d0_s0" to="o33847" />
      <relation from="o33846" id="r4564" name="covered with" negation="false" src="d0_s0" to="o33848" />
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous.</text>
      <biological_entity id="o33849" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, solid, 2–4 dm.</text>
      <biological_entity id="o33850" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal with blade gray-green, usually brilliantly colored basally, drying to unusual gray-green, veins fairly prominent, to 4 dm × 0.7–0.8 cm, often quite glaucous, margins not thickened;</text>
      <biological_entity id="o33851" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o33852" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually brilliantly; basally" name="coloration" notes="" src="d0_s3" value="colored" value_original="colored" />
        <character is_modifier="false" name="condition" src="d0_s3" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o33853" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o33854" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="fairly" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0" from_unit="dm" name="length" src="d0_s3" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="often quite" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o33855" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o33852" id="r4565" name="with" negation="false" src="d0_s3" to="o33853" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 2–several, spreading, sheathing stem for about 1/2 length, foliaceous, blade not inflated.</text>
      <biological_entity id="o33856" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o33857" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="false" name="quantity" src="d0_s4" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o33858" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o33859" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence units 2-flowered;</text>
      <biological_entity constraint="inflorescence" id="o33860" name="unit" name_original="units" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes opposite, connivent, often flushed red basally, broadly lanceolate, 5–9 cm × 6–11 mm, subequal.</text>
      <biological_entity id="o33861" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
        <character is_modifier="false" modifier="often; basally" name="coloration" src="d0_s6" value="flushed red" value_original="flushed red" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="11" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth pale creamy yellow with gold or gray veins;</text>
      <biological_entity id="o33862" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o33863" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale creamy" value_original="pale creamy" />
        <character constraint="with veins" constraintid="o33864" is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o33864" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="gold" value_original="gold" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube funnelform, 3–6.2 cm, spreading apically to form wide throat;</text>
      <biological_entity id="o33865" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33866" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="6.2" to_unit="cm" />
        <character is_modifier="false" modifier="apically" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o33867" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals horizontally spreading, claw with deep yellow median line, oblanceolate to spatulate, 4.5–7 × 1–2 cm, base gradually attenuate into broad claw;</text>
      <biological_entity id="o33868" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33869" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o33870" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" notes="" src="d0_s9" to="spatulate" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s9" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="median" id="o33871" name="line" name_original="line" src="d0_s9" type="structure">
        <character is_modifier="true" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o33872" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="into claw" constraintid="o33873" is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o33873" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o33870" id="r4566" name="with" negation="false" src="d0_s9" to="o33871" />
    </statement>
    <statement id="d0_s10">
      <text>petals narrowly oblanceolate, 4.3–6 × 0.6–1.4 cm, base gradually attenuate into narrow claw;</text>
      <biological_entity id="o33874" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33875" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4.3" from_unit="cm" name="length" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s10" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33876" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="into claw" constraintid="o33877" is_modifier="false" modifier="gradually" name="shape" src="d0_s10" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o33877" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary elliptical, nearly circular in cross-section, 1.5–2.3 cm;</text>
      <biological_entity id="o33878" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33879" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="elliptical" value_original="elliptical" />
        <character constraint="in cross-section" constraintid="o33880" is_modifier="false" modifier="nearly" name="arrangement_or_shape" src="d0_s11" value="circular" value_original="circular" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s11" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33880" name="cross-section" name_original="cross-section" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style 2.2–3 cm, crests divergent, linear to narrowly oblong, 1–1.7 cm;</text>
      <biological_entity id="o33881" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o33882" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.2" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33883" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="narrowly oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas triangular, margins entire;</text>
      <biological_entity id="o33884" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33885" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o33886" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 0.9–2.2 cm at anthesis.</text>
      <biological_entity id="o33887" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o33888" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0.9" from_unit="cm" name="some_measurement" src="d0_s14" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong, roundly trigonal, distinctly beaked, 2.5–3.5 cm.</text>
      <biological_entity id="o33889" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="roundly; distinctly" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s15" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, wrinkled.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o33890" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33891" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed evergreen forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed evergreen forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Fernald’s iris</other_name>
  <discussion>Iris fernaldii hybridizes with I. douglasiana, I. innominata, and I. macrosiphon.</discussion>
  
</bio:treatment>