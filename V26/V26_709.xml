<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Cavanilles" date="1793" rank="genus">milla</taxon_name>
    <taxon_name authority="Cavanilles" date="1793" rank="species">biflora</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>2: 76, plate 196. 1793</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus milla;species biflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220008607</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Askolame</taxon_name>
    <taxon_name authority="(Cavanilles) Rafinesque" date="unknown" rank="species">biflora</taxon_name>
    <taxon_hierarchy>genus Askolame;species biflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Corm ovoid, 1–2 cm;</text>
      <biological_entity id="o28084" name="corm" name_original="corm" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>coat brown or reddish-brown, minutely striate, splitting from base into narrow strips that shred and appear fibrous.</text>
      <biological_entity id="o28085" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="minutely" name="coloration_or_pubescence_or_relief" src="d0_s1" value="striate" value_original="striate" />
        <character constraint="from base" constraintid="o28086" is_modifier="false" name="architecture_or_dehiscence" src="d0_s1" value="splitting" value_original="splitting" />
        <character is_modifier="false" name="texture" notes="" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o28086" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o28087" name="strip" name_original="strips" src="d0_s1" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o28086" id="r3802" name="into" negation="false" src="d0_s1" to="o28087" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, 2–7 (–10), 1 mm wide, 1/2 as long as to equaling scape;</text>
      <biological_entity id="o28088" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="7" />
        <character name="width" src="d0_s2" unit="mm" value="1" value_original="1" />
        <character constraint="to scape" constraintid="o28089" name="quantity" src="d0_s2" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o28089" name="scape" name_original="scape" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade channeled.</text>
      <biological_entity id="o28090" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="channeled" value_original="channeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Scape 4–55 cm, scabrous on proximal veins.</text>
      <biological_entity id="o28091" name="scape" name_original="scape" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="55" to_unit="cm" />
        <character constraint="on proximal veins" constraintid="o28092" is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28092" name="vein" name_original="veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–9-flowered, if solitary, one or more undeveloped flower buds often present;</text>
      <biological_entity id="o28093" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-9-flowered" value_original="1-9-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="flower" id="o28094" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="undeveloped" value_original="undeveloped" />
        <character is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts 4 in 2 whorls, 5–12 mm, apex acute.</text>
      <biological_entity id="o28095" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="in whorls" constraintid="o28096" name="quantity" src="d0_s6" value="4" value_original="4" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28096" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28097" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4.5–18 cm (appearing 2.5–4 cm due to pseudopedicel);</text>
      <biological_entity id="o28098" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s7" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth-tube with lacuna between stipe angles to within 0.5–5 cm of base, then tube and stipe completely fused into pseudopedicel;</text>
      <biological_entity id="o28099" name="perianth-tube" name_original="perianth-tube" src="d0_s8" type="structure" />
      <biological_entity constraint="between angles" constraintid="o28101" id="o28100" name="lacuna" name_original="lacuna" src="d0_s8" type="structure" constraint_original="between  angles, " />
      <biological_entity constraint="stipe" id="o28101" name="angle" name_original="angles" src="d0_s8" type="structure" />
      <biological_entity id="o28102" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o28103" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character constraint="into pseudopedicel" constraintid="o28105" is_modifier="false" modifier="completely" name="fusion" src="d0_s8" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o28104" name="stipe" name_original="stipe" src="d0_s8" type="structure">
        <character constraint="into pseudopedicel" constraintid="o28105" is_modifier="false" modifier="completely" name="fusion" src="d0_s8" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o28105" name="pseudopedicel" name_original="pseudopedicel" src="d0_s8" type="structure" />
      <relation from="o28099" id="r3803" name="with" negation="false" src="d0_s8" to="o28100" />
      <relation from="o28100" id="r3804" modifier="to within 0.5-5 cm" name="part_of" negation="false" src="d0_s8" to="o28102" />
    </statement>
    <statement id="d0_s9">
      <text>perianth lobes spreading, white with green abaxial stripe, 3–5-veined, elliptic, becoming papery and persisting in fruit, eventually split by developing capsule, 1.5–2.5 cm;</text>
      <biological_entity constraint="perianth" id="o28106" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character constraint="with abaxial stripe" constraintid="o28107" is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character constraint="in fruit" constraintid="o28108" is_modifier="false" name="duration" src="d0_s9" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28107" name="stripe" name_original="stripe" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o28108" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o28109" name="split" name_original="split" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s9" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28110" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="true" name="development" src="d0_s9" value="developing" value_original="developing" />
      </biological_entity>
      <relation from="o28109" id="r3805" name="by" negation="false" src="d0_s9" to="o28110" />
    </statement>
    <statement id="d0_s10">
      <text>outer lobes 5–10 mm wide, apex subacute, inner lobes 8–12 mm wide, apex rounded;</text>
      <biological_entity constraint="outer" id="o28111" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28112" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o28113" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28114" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments triangular, 1 mm;</text>
      <biological_entity id="o28115" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers yellow, lanceolate, sagittate at base, 3–5 mm, suture margins minutely denticulate, ± crisped;</text>
      <biological_entity id="o28116" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character constraint="at base" constraintid="o28117" is_modifier="false" name="shape" src="d0_s12" value="sagittate" value_original="sagittate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28117" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity constraint="suture" id="o28118" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s12" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary proximally adnate to perianth-tube, ovoid to obovoid, 1 cm, stipe 3–16 cm;</text>
      <biological_entity id="o28119" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character constraint="to perianth-tube" constraintid="o28120" is_modifier="false" modifier="proximally" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s13" to="obovoid" />
        <character name="some_measurement" src="d0_s13" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o28120" name="perianth-tube" name_original="perianth-tube" src="d0_s13" type="structure" />
      <biological_entity id="o28121" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s13" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style exserted;</text>
      <biological_entity id="o28122" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate, minutely 3-lobed.</text>
      <biological_entity id="o28123" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s15" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ovoid, 1.5–2 cm;</text>
      <biological_entity id="o28124" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s16" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak 1.5–2.5 mm.</text>
      <biological_entity id="o28125" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="mid Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Most volcanic soils, dry hillsides, ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic soils" modifier="most" />
        <character name="habitat" value="dry hillsides" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico; Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Persistent reports of the occurrence of Milla biflora in trans-Pecos Texas are due to a collection made by Charles Wright “On the San Pedro, West Texas” in 1851–1852. Wright was not in west Texas during the blooming period of M. biflora in either year. This collection most likely came from along the San Pedro River below Benson, Cochise County, Arizona.</discussion>
  
</bio:treatment>