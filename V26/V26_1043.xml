<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Loyal A. Mehrhoff,Michael A. Homoya</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="(Lindley) Szlachetko" date="1995" rank="subfamily">Vanilloideae</taxon_name>
    <taxon_name authority="Blume" date="1837" rank="tribe">Vanilleae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="subtribe">Pogoniinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1808" rank="genus">ISOTRIA</taxon_name>
    <place_of_publication>
      <publication_title>Med. Repos., hexade</publication_title>
      <place_in_publication>2, 5: 357. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe pogoniinae;genus isotria;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek iso, equal, and tri, 3; probably referring to 3 sepals of equal size and shape</other_info_on_name>
    <other_info_on_name type="fna_id">116626</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial, winter dormant.</text>
      <biological_entity id="o33979" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="season" src="d0_s0" value="winter" value_original="winter" />
        <character is_modifier="false" name="life_cycle" src="d0_s0" value="dormant" value_original="dormant" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots long, slender, hairy with mycorrhizae.</text>
      <biological_entity id="o33980" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character constraint="with mycorrhizae" constraintid="o33981" is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o33981" name="mycorrhiza" name_original="mycorrhizae" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, hollow, fleshy, smooth, glaucous.</text>
      <biological_entity id="o33982" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (2–) 5 (–6), appearing whorled.</text>
      <biological_entity id="o33983" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="6" />
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal;</text>
      <biological_entity id="o33984" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts absent.</text>
      <biological_entity id="o33985" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–2, resupinate, erect, yellowish green to white, yellow, and purple, subsessile to pedicellate;</text>
      <biological_entity id="o33986" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s6" to="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s6" to="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals of same color, linear-oblanceolate to lanceolate, of equal size;</text>
      <biological_entity id="o33987" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblanceolate, elliptic-obovate, or elliptic-lanceolate, enclosing column;</text>
      <biological_entity id="o33988" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-obovate" value_original="elliptic-obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-obovate" value_original="elliptic-obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
      </biological_entity>
      <biological_entity id="o33989" name="column" name_original="column" src="d0_s8" type="structure" />
      <relation from="o33988" id="r4575" name="enclosing" negation="false" src="d0_s8" to="o33989" />
    </statement>
    <statement id="d0_s9">
      <text>lip apically 3-lobed, lateral lobes triangular, margins involute;</text>
      <biological_entity id="o33990" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o33991" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o33992" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>callus forming fleshy medial crest;</text>
      <biological_entity id="o33993" name="callus" name_original="callus" src="d0_s10" type="structure" />
      <biological_entity constraint="medial" id="o33994" name="crest" name_original="crest" src="d0_s10" type="structure">
        <character is_modifier="true" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o33993" id="r4576" name="forming" negation="false" src="d0_s10" to="o33994" />
    </statement>
    <statement id="d0_s11">
      <text>column white, apex denticulate;</text>
      <biological_entity id="o33995" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o33996" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther operculate;</text>
      <biological_entity id="o33997" name="anther" name_original="anther" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="operculate" value_original="operculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollinia 2, hinged, soft, mealy;</text>
      <biological_entity id="o33998" name="pollinium" name_original="pollinia" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="soft" value_original="soft" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="mealy" value_original="mealy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rostellum obscure;</text>
      <biological_entity id="o33999" name="rostellum" name_original="rostellum" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollen in tetrads, granular;</text>
      <biological_entity id="o34000" name="pollen" name_original="pollen" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence_or_relief_or_texture" notes="" src="d0_s15" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity id="o34001" name="tetrad" name_original="tetrads" src="d0_s15" type="structure" />
      <relation from="o34000" id="r4577" name="in" negation="false" src="d0_s15" to="o34001" />
    </statement>
    <statement id="d0_s16">
      <text>ovary green, slender.</text>
      <biological_entity id="o34002" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, commonly persistent through following growing season, erect, ellipsoid-cylindric, dehiscent in fall.</text>
      <biological_entity constraint="fruits" id="o34003" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character constraint="through growing" constraintid="o34004" is_modifier="false" modifier="commonly" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid-cylindric" value_original="ellipsoid-cylindric" />
        <character constraint="in fall" constraintid="o34005" is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o34004" name="growing" name_original="growing" src="d0_s17" type="structure" />
      <biological_entity id="o34005" name="fall" name_original="fall" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1.2 × 0.2 mm. x = 9.</text>
      <biological_entity id="o34006" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="length" src="d0_s18" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s18" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity constraint="x" id="o34007" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Whorled pogonia orchid</other_name>
  <discussion>Species 2 (2 in the flora).</discussion>
  <references>
    <reference>Ames, O. 1905–1922. Orchidaceae: Illustrations and Studies of the Family Orchidaceae Issuing from the Ames Botanical Laboratory…. 7 vols. Boston and New York. Vol. 7.  </reference>
    <reference>Homoya, M. A. 1977. The Distribution and Ecology of the Genus Isotria in Illinois. Master’s thesis. Southern Illinois University.  </reference>
    <reference>Mehrhoff, L. A. 1983. Pollination in the genus Isotria (Orchidaceae). Amer. J. Bot. 70: 1444–1453.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals green to light green, less than 30 mm; lip light yellowish green to pale greenish white, streaked with green; pedicel of mature capsule 5–17(–20) mm; stem and leaves pale grayish green.</description>
      <determination>1 Isotria medeoloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals purplish brown, greater than 30 mm; lip yellowish green to white, streaked with purple; pedicel of mature capsule 20–55 mm; stem purplish to brownish green; leaves green to dark green.</description>
      <determination>2 Isotria verticillata</determination>
    </key_statement>
  </key>
</bio:treatment>