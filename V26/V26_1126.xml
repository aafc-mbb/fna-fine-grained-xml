<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="mention_page">561</other_info_on_meta>
    <other_info_on_meta type="mention_page">570</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="treatment_page">551</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">PLATANTHERA</taxon_name>
    <place_of_publication>
      <publication_title>De Orchid. Eur.,</publication_title>
      <place_in_publication>20, 26, 35. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus platanthera;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek platys, broad, and anthera, anther</other_info_on_name>
    <other_info_on_name type="fna_id">125746</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, erect to somewhat decumbent, rather succulent.</text>
      <biological_entity id="o11575" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect to somewhat" value_original="erect to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fasciculate, both slender and tuberous, fleshy;</text>
    </statement>
    <statement id="d0_s2">
      <text>if tuberous, then lancefusiform.</text>
      <biological_entity id="o11576" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lancefusiform" value_original="lancefusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems leafy or leafless, terete;</text>
      <biological_entity id="o11577" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafless" value_original="leafless" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 1–several, strictly basal or gradually reduced to bracts toward inflorescence, conduplicate, ascending to spreading, bases sheathing stem.</text>
      <biological_entity id="o11578" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s4" to="several" />
        <character is_modifier="false" modifier="strictly" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="to bracts" constraintid="o11579" is_modifier="false" modifier="gradually; gradually" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="arrangement_or_vernation" notes="" src="d0_s4" value="conduplicate" value_original="conduplicate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
      </biological_entity>
      <biological_entity id="o11579" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity id="o11580" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <biological_entity id="o11581" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o11582" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o11579" id="r1628" name="toward" negation="false" src="d0_s4" to="o11580" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary, terminal, lax to dense spikes.</text>
      <biological_entity id="o11583" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o11584" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers few-to-many, usually resupinate (not resupinate in P. nivea), sometimes showy;</text>
      <biological_entity id="o11585" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s6" to="many" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals entire to fringed or emarginate;</text>
      <biological_entity id="o11586" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s7" to="fringed or emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip lobed, 3-partite, spurred at base, margins entire to fringed;</text>
      <biological_entity id="o11587" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-partite" value_original="3-partite" />
        <character constraint="at base" constraintid="o11588" is_modifier="false" name="architecture_or_shape" src="d0_s8" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o11588" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o11589" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s8" to="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pollinaria 2;</text>
      <biological_entity id="o11590" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollinia 2;</text>
      <biological_entity id="o11591" name="pollinium" name_original="pollinia" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>viscidia free;</text>
      <biological_entity id="o11592" name="viscidium" name_original="viscidia" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma entire.</text>
      <biological_entity id="o11593" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsules, ellipsoid to cylindric.</text>
      <biological_entity constraint="fruits" id="o11594" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s13" to="cylindric" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Primarily north temperate (a few tropical).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Primarily north temperate (a few tropical)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Bog orchid</other_name>
  <other_name type="common_name">fringed orchid</other_name>
  <other_name type="common_name">rein orchid</other_name>
  <discussion>Species ca. 200 (32 species in the flora).</discussion>
  <references>
    <reference>Ames, O. 1905–1922. Orchidaceae: Illustrations and Studies of the Family Orchidaceae Issuing from the Ames Botanical Laboratory…. 7 vols. Boston and New York. Vol. 4.  </reference>
    <reference>Folsom, J. P. 1984. Una reinterpretación del estatus y relaciones de las taxa del complejo de Platanthera ciliaris. (A reinterpretation of the status and relationships of taxa of the yellow-fringed orchid complex.) Orquidea (Mexico City) 9: 321–345.  </reference>
    <reference>Reddoch, A. H. and J. M. Reddoch. 1993. The species pair Platanthera orbiculata and P. macrophylla (Orchidaceae): Taxonomy, morphology, distributions and habitats. Lindleyana 8: 171–187.  </reference>
    <reference>Stoutamire, W. P. 1974. Relationships of purple-fringed orchids Platanthera psycodes and P. grandiflora. Brittonia 26: 42–58.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip lobed, fringed, eroded, lacerate, or emarginate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip entire, at most abruptly dilated toward base (in some robust Alaskan material of P. dilatata var. albiflora basal dilation of lip is elaborated forward, taking form of lateral lobules).</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip deeply and subequally 3-lobed (lateral lobes each in area equaling approximately 1/2 middle lobe, often much more).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip not deeply, subequally 3-lobed, merely auriculate, fringed, eroded, lacerate, or emarginate, or apically shallowly 3-lobed.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals linear-oblong to oblong-spatulate, usually entire; lip greenish white.</description>
      <determination>24 Platanthera lacera</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals oblong-obovate to flabellate, lacerate toward apex; lip pure white or pink-magenta to lavender.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lobes of lip dentate to dentate-lacerate, not fringed; rostellum lobes spreading.</description>
      <determination>19 Platanthera peramoena</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lobes of lip fringed; rostellum lobes spreading or parallel.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers pink-magenta to lavender, rarely white; pollinia erect; pollinaria nearly straight; column appearing truncate in lateral view.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers white; pollinia directed forward; pollinaria geniculate; column appearing hooded in lateral view.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Rostellum lobes spreading, angular in lateral view.</description>
      <determination>20 Platanthera grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Rostellum lobes parallel, rounded in lateral view.</description>
      <determination>21 Platanthera psycodes</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Rostellum lobes spreading, angular in lateral view.</description>
      <determination>22 Platanthera praeclara</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Rostellum lobes parallel, rounded in lateral view.</description>
      <determination>23 Platanthera leucophaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Lip basally auriculate, or apically emarginate or shallowly lobed.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Lip fringed or eroded.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Lip with basal pair of auricules flanking prominent tubercle on adaxial surface, apex obtuse to emarginate.</description>
      <determination>18 Platanthera flava</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Lip without basal pair of auricles or tubercle, apex shallowly 3-lobed.</description>
      <determination>31 Platanthera clavellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Lip not conspicuously fringed, merely eroded to lacerate.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Lip conspicuously fringed.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Flowers white; lip 10–15 mm.</description>
      <determination>26 Platanthera integrilabia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Flowers yellow to orange; lip less than 6 mm.</description>
      <determination>32 Platanthera integra</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rostellum lobes triangular, apices directed forward (column appearing acute in lateral view).</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rostellum lobes slender, directed downward toward apices (column appearing truncate in lateral view), straight to distally retrorse.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Flowers white.</description>
      <determination>25 Platanthera blephariglottis</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Flowers orange.</description>
      <determination>27 Platanthera ciliaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Spur 8–17 mm, its mouth nearly circular; rostellum lobes strongly retrorsely curved toward apices; viscidia presented downward and approximately parallel to lip.</description>
      <determination>29 Platanthera chapmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Spur 4–10 mm, its mouth triangular or keyhole-shaped; rostellum lobes scarcely to strongly curved; viscidia presented forward, nearly perpendicular to approximately parallel to lip.</description>
      <determination>28 Platanthera cristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaves 1–2; scape naked or with 1(–2, very rarely) bracts.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaves (1–)3–several; scape with gradually reduced bracts distially.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Spur saccate to stoutly cylindric; lip broadly elliptic-suborbiculate to obovate, concave.</description>
      <determination>17 Platanthera chorisiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Spur slender; lip linear to rhombic-lanceolate, ± flat.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf 1, or if 2 alternate and widely spaced on stem, erect-spreading.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves 2 in basal pair, wide-spreading, commonly lying on ground.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Lip linear to linear-rhombic-lanceolate (lanceolate in subsp. oligantha); spur tapering from broad base, ± as long as lip; widespread.</description>
      <determination>4 Platanthera obtusata</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Lip elliptic to elliptic-oblong or elliptic-lanceolate; spur cylindric to clavate, ± 2 times as long as lip; w Aleutian Islands.</description>
      <determination>5 Platanthera tipuloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Scape naked; lip directed forward, lanceolate; spur tapering to apex from broad base.</description>
      <determination>3 Platanthera hookeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Scape with 1 or more bracts; lip pendent, linear to linear-oblong; spur clavate.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Spur less than 28 mm; pollinaria less than 4.6 mm.</description>
      <determination>1 Platanthera orbiculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Spur equal to or greater than 28 mm; pollinaria equal to or greater than 4.6 mm.</description>
      <determination>2 Platanthera macrophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Lip uppermost (flowers not resupinate); flowers white.</description>
      <determination>30 Platanthera nivea</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Lip lowermost (flowers resupinate); flowers white, green, or yellowish.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Viscidia linear to linear-oblong; flowers white; lip usually with suborbiculate to orbiculate basal dilation.</description>
      <determination>6 Platanthera dilatata</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Viscidia oblong to orbiculate; flowers greenish, yellowish, or white; lip linear to linear-rhombic, elliptic, or lanceolate, base undilated or with obscure to orbiculate basal dilation.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Column comparatively large, occupying ca. 2/3 hood formed by dorsal sepal and petals; rostellum lobes widely spaced, diverging, and together with stigma and connective forming hemispheric chamber; lip linear to linear-oblong or linear-lanceolate, or narrowly elliptic, base undilated.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Column comparatively small, occupying less than 1/2 hood formed by dorsal sepal and petals; rostellum lobes parallel, diverging, or converging, but scarcely elevated, at most separated by narrow slit, not forming hemispheric chamber; lip linear to linear-lanceolate, rhombic-linear, rhombic-lanceolate, or elliptic, base undilated or with obscure to orbiculate basal dilation.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves reduced; blades rigidly erect, loosely sheathing stem, scarcely spreading beyond width of inflorescence, sometimes absent; dry to mesic forest.</description>
      <determination>14 Platanthera brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves well developed; blades prominent, spreading well beyond sheathing bases and width of inflorescence; wet-mesic to wet sites.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Spur 6–14 mm, 0.7–1.6 times length of lip; lip with prominent basal median ridge; leaves ascending or wide-spreading, scattered along stem.</description>
      <determination>13 Platanthera sparsiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Spur 12–17 mm, 1.3–2.6 times length of lip; lip without prominent median ridge, but often with series of obscure parallel ridges; leaves wide-spreading, usually limited to proximal portion of stem.</description>
      <determination>15 Platanthera zothecina</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Anther low, appearing to lie atop stigma; anther sacs widely diverging from apices scarcely separated by obscure connective; flowers autogamous; pollinia rotated forward, commonly free of anther sacs and/or fragmenting into loose masses that trail downward onto stigma; spur clavate, mostly slightly shorter than lip; lip rhombic-lanceolate, yellowish to yellowish green.</description>
      <determination>10 Platanthera aquilonis</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Anther high, rising above stigma; anther sacs more nearly parallel, converging, or diverging from apices separated by evident connective; flowers not autogamous (except evidently autogamous in P. hyperborea; see also note under P. huronensis); pollinia remaining within anther sacs; spur scrotiform or saccate to slenderly clavate or filiform, much shorter to much longer than lip; lip linear, elliptic, or broadly lanceolate, or with abrupt basal dilation, white to green or yellowish.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Spur 8–25 mm, much longer than lip, filiform; lip usually with small median basal thickening, linear-oblong to linear-elliptic, rarely rhombic-ovate.</description>
      <determination>16 Platanthera limosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Spur 2–12 mm, much shorter to slightly longer than lip, saccate or scrotiform to capitate, clavate, or slenderly cylindric; lip linear-rhombic, linear-lanceolate, or rhombic-lanceolate, or with pronounced orbiculate to suborbiculate basal dilation.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Pollinaria and rostellum lobes parallel to converging toward orbiculate viscidia; spur scrotiform or saccate to spatuloid-capitate or strongly clavate, much shorter than lip.</description>
      <determination>12 Platanthera stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Pollinaria and rostellum lobes diverging toward oblong or sometimes (in P. purpurascens) orbiculate viscidia; spur slenderly cylindric to strongly clavate, saccate, or scrotiform, slightly longer to much shorter than lip.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Lip oblong or rarely lanceolate, dull yellowish; rostellum lobes usually projecting forward, separated by narrow slit; leaves clustered toward base of stem.</description>
      <determination>13 Platanthera sparsiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Lip broadest at base, lanceolate, ovate, or linear, or with pronounced basal dilation, white, greenish or yellowish; rostellum lobes low, not projecting forward; column without evident slit; leaves scattered along stem.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Lip pure white, usually with pronounced suborbiculate to orbiculate basal dilation or rarely shallowly 3-lobed; spur clavate to slightly capitate, somewhat to very much shorter than lip.</description>
      <determination>6 Platanthera dilatata</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Lip greenish or yellowish, mostly lanceolate, ovate, or linear, sometimes obscurely or prominently dilated at base; spur slenderly cylindric to strongly clavate, saccate, or scrotiform, shorter than to slightly longer than lip.</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Spur saccate or scrotiform to clavate-inflated (widest at apex), ca. 1/2 length of lip; lip yellowish green to dull yellowish or intensely bluish green, sometimes marked with red, linear-lanceolate to prominently rounded-dilated at base; musty scented.</description>
      <determination>11 Platanthera purpurascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Spur slenderly cylindric to slenderly clavate or clavate, not inflated, merely obtuse to subacute, shorter to slighter longer than lip; lip whitish green to perhaps yellowish green in P. hyperborea; sweet scented.</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Lip linear-rhombic, with straight sides, not at all rounded-dilated at base; spur ca. 2/3 to slightly shorter than lip; Aleutian Islands, adjacent Alaska.</description>
      <determination>9 Platanthera convallariifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Lip lanceolate to ovate, usually obscurely to prominently rounded-dilated at base; spur slightly shorter to slightly longer than lip.</description>
      <next_statement_id>33</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Lip lanceolate, usually obscurely rounded-dilated at base, whitish green; spur slenderly clavate to slenderly cylindric; flowers not autogamous; pollinia retained within anther sacs (but see note); viscidia oblong; widespread.</description>
      <determination>7 Platanthera huronensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Lip ovate, rarely lanceolate, abruptly and broadly dilated at base, apparently yellowish green; spur markedly clavate; flowers autogamous; pollinia rotating forward and downward from anther sacs or fragmenting and pollen masses trailing onto stigma; viscidia linear to linear-oblong; Greenland.</description>
      <determination>8 Platanthera hyperborea</determination>
    </key_statement>
  </key>
</bio:treatment>