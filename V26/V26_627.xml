<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J. McNeill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Boissier" date="1844" rank="genus">CHIONODOXA</taxon_name>
    <place_of_publication>
      <publication_title>Diagn. Pl. Orient.</publication_title>
      <place_in_publication>1(5): 61. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus CHIONODOXA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chion, snow, and doxa, glory or repute</other_info_on_name>
    <other_info_on_name type="fna_id">106711</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, from tunicate bulbs.</text>
      <biological_entity id="o358" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o359" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tunicate" value_original="tunicate" />
      </biological_entity>
      <relation from="o358" id="r52" name="from" negation="false" src="d0_s0" to="o359" />
    </statement>
    <statement id="d0_s1">
      <text>Bulbs perennial, ovoid to globose, composed of free scales, progressively renewed annually;</text>
      <biological_entity id="o360" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s1" to="globose" />
        <character is_modifier="false" modifier="progressively; annually" name="duration" notes="" src="d0_s1" value="renewed" value_original="renewed" />
      </biological_entity>
      <biological_entity id="o361" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s1" value="free" value_original="free" />
      </biological_entity>
      <relation from="o360" id="r53" name="composed of" negation="false" src="d0_s1" to="o361" />
    </statement>
    <statement id="d0_s2">
      <text>tunics brown.</text>
      <biological_entity id="o362" name="tunic" name_original="tunics" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually 2, basal, erect or spreading.</text>
      <biological_entity id="o363" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="basal" id="o364" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemose, open, 1–few-flowered;</text>
      <biological_entity id="o365" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-few-flowered" value_original="1-few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts none or 1 subtending each flower.</text>
      <biological_entity id="o366" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="none or  subtending each flower" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth blue or blue and white;</text>
      <biological_entity id="o367" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o368" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="blue and white" value_original="blue and white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals shortly connate to form urceolate tube, lobes spreading to reflexed;</text>
      <biological_entity id="o369" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o370" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character constraint="to form, tube" constraintid="o371, o372" is_modifier="false" modifier="shortly" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o371" name="form" name_original="form" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
      </biological_entity>
      <biological_entity id="o372" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
      </biological_entity>
      <biological_entity id="o373" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 6;</text>
      <biological_entity id="o374" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o375" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments inserted at apex of perianth-tube, unequal, ± equaling anthers;</text>
      <biological_entity id="o376" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o377" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o378" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o379" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
      <biological_entity id="o380" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o377" id="r54" name="inserted at" negation="false" src="d0_s9" to="o378" />
      <relation from="o377" id="r55" name="part_of" negation="false" src="d0_s9" to="o379" />
    </statement>
    <statement id="d0_s10">
      <text>anthers dorsifixed, introrse;</text>
      <biological_entity id="o381" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o382" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s10" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 3-locular, septal nectaries present;</text>
      <biological_entity id="o383" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o384" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity constraint="septal" id="o385" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style simple.</text>
      <biological_entity id="o386" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o387" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, subglobose, 3-lobed, dehiscence loculicidal.</text>
      <biological_entity id="o388" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 3–18, globose to ellipsoid, not winged, elaiosomes present.</text>
      <biological_entity id="o389" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="18" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="ellipsoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o390" name="elaiosome" name_original="elaiosomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o391" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; e Mediterranean area, 1 or 2 species naturalized in temperate areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e Mediterranean area" establishment_means="introduced" />
        <character name="distribution" value="1 or 2 species naturalized in temperate areas" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>58.</number>
  <other_name type="common_name">Glory-of-the-snow</other_name>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>Chionodoxa, traditionally separated from Scilla because of its partially fused tepals, is very similar to species of Scilla subg. Scilla, especially S. bifolia Linnaeus, and hybrids between species of the two groups (×Chionoscilla allenii Nicholson) occur frequently. As a result, the species of Chionodoxa are sometimes included within Scilla subg. Scilla (F. Speta 1971, 1976).</discussion>
  <references>
    <reference>Meikle, R. D. 1970. Chionodoxa luciliae, a taxonomic note. J. Roy. Hort. Soc. 95: 21–24.  </reference>
    <reference>Rix, E. M. 1986. Chionodoxa. In: S. M. Walters et al., eds. 1984+. European Garden Flora…. 6+ vols. Cambridge etc. Vol. 1, pp. 214–215.  </reference>
    <reference>Speta, F. 1971. Beitrag zur Systematik von Scilla L. subgen. Scilla (inklusive Chionodoxa Boiss.). Oesterr. Bot. Z. 119: 6–18.  </reference>
    <reference>Speta, F. 1976. Über Chionodoxa Boiss., ihre Gliederung und Zugehörigkeit zu Scilla L. Naturk. Jahrb. Stadt Linz 21: 9–79.</reference>
  </references>
  
</bio:treatment>