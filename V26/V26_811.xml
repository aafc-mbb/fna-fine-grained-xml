<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter Goldblatt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Adanson" date="unknown" rank="genus">BELAMCANDA</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Pl.</publication_title>
      <place_in_publication>2: 60. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus BELAMCANDA</taxon_hierarchy>
    <other_info_on_name type="etymology">apparently based on a vernacular name in western India</other_info_on_name>
    <other_info_on_name type="fna_id">103713</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Kuntze" date="unknown" rank="genus">Gemmingia</taxon_name>
    <taxon_hierarchy>genus Gemmingia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Ker Gawler" date="unknown" rank="genus">Pardanthus</taxon_name>
    <taxon_hierarchy>genus Pardanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from creeping rhizomes.</text>
      <biological_entity id="o17109" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o17110" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o17109" id="r2348" name="from" negation="false" src="d0_s0" to="o17110" />
    </statement>
    <statement id="d0_s1">
      <text>Stems few to several-branched.</text>
      <biological_entity id="o17111" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="several-branched" value_original="several-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves several;</text>
      <biological_entity id="o17112" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade plane, ensiform.</text>
      <biological_entity id="o17113" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ensiform" value_original="ensiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences rhipidiate, several-flowered;</text>
      <biological_entity id="o17114" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="rhipidiate" value_original="rhipidiate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="several-flowered" value_original="several-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes green, membranous distally.</text>
      <biological_entity id="o17115" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="distally" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers fleeting, erect, unscented, actinomorphic;</text>
      <biological_entity id="o17116" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="fleeting" value_original="fleeting" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="odor" src="d0_s6" value="unscented" value_original="unscented" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals erect, basally connate into vestigial tube, light orange to reddish (rarely yellow), with scattered spots of darker pigment, obscurely clawed, ± equal, outer whorl slightly larger than inner;</text>
      <biological_entity id="o17117" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="into tube" constraintid="o17118" is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="light orange" name="coloration" notes="" src="d0_s7" to="reddish" />
        <character constraint="than inner whorl" constraintid="o17120" is_modifier="false" name="size" src="d0_s7" value="of darker pigment , obscurely clawed , more or less equal , outer whorl slightly larger" />
      </biological_entity>
      <biological_entity id="o17118" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <biological_entity id="o17119" name="spot" name_original="spots" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
        <character constraint="than inner whorl" constraintid="o17120" is_modifier="false" name="size" src="d0_s7" value="of darker pigment , obscurely clawed , more or less equal , outer whorl slightly larger" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17121" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="darker pigment" value_original="darker pigment" />
        <character is_modifier="true" modifier="obscurely" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17120" name="whorl" name_original="whorl" src="d0_s7" type="structure" />
      <relation from="o17117" id="r2349" name="with" negation="false" src="d0_s7" to="o17119" />
      <relation from="o17119" id="r2350" name="part_of" negation="false" src="d0_s7" to="o17121" />
    </statement>
    <statement id="d0_s8">
      <text>stamens diverging;</text>
      <biological_entity id="o17122" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments distinct;</text>
      <biological_entity id="o17123" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers diverging, not appressed to style-branches;</text>
      <biological_entity id="o17124" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="diverging" value_original="diverging" />
        <character constraint="to style-branches" constraintid="o17125" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o17125" name="style-branch" name_original="style-branches" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>style slender, short, not extending between stamens, branching distally into lobes;</text>
      <biological_entity id="o17126" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character constraint="into lobes" constraintid="o17128" is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o17127" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o17128" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o17126" id="r2351" name="extending between" negation="true" src="d0_s11" to="o17127" />
    </statement>
    <statement id="d0_s12">
      <text>lobes 3, flattened, stigmatic distally, stigmatic surfaces each subtended adaxially by paired flaps of tissue.</text>
      <biological_entity id="o17129" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="distally" name="structure_in_adjective_form" src="d0_s12" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o17130" name="surface" name_original="surfaces" src="d0_s12" type="structure" />
      <biological_entity id="o17131" name="flap" name_original="flaps" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="paired" value_original="paired" />
      </biological_entity>
      <biological_entity id="o17132" name="tissue" name_original="tissue" src="d0_s12" type="structure" />
      <relation from="o17130" id="r2352" modifier="adaxially" name="by" negation="false" src="d0_s12" to="o17131" />
      <relation from="o17131" id="r2353" name="part_of" negation="false" src="d0_s12" to="o17132" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid-truncate, ± woody, apex obtuse.</text>
      <biological_entity id="o17133" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="ovoid-truncate" value_original="ovoid-truncate" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s13" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o17134" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds several, globose;</text>
      <biological_entity id="o17135" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="several" value_original="several" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat blackish.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 16.</text>
      <biological_entity id="o17136" name="seed-coat" name_original="seed-coat" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="blackish" value_original="blackish" />
      </biological_entity>
      <biological_entity constraint="x" id="o17137" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia including Japan; introduced in tropical Asia, the Pacific Islands, and parts of South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia including Japan" establishment_means="introduced" />
        <character name="distribution" value="in tropical Asia" establishment_means="introduced" />
        <character name="distribution" value="the Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="and parts of South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="past_name">Belamkanda</other_name>
  <other_name type="common_name">Blackberry-lily</other_name>
  <discussion>Species 1 or 2 (1 in the flora).</discussion>
  <references>
    <reference>  Mathew, B. 1990. The Iris, rev. ed. Portland.</reference>
  </references>
  
</bio:treatment>