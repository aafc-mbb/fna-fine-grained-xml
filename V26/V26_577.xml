<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="D. Don" date="1836" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>in R. Sweet, Brit. Fl. Gard., ser.</publication_title>
      <place_in_publication>2, 4: plate 328. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species drummondii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102082</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cooperia</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="species">pedunculata</taxon_name>
    <taxon_hierarchy>genus Cooperia;species pedunculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade glaucous-green, to 8 mm wide.</text>
      <biological_entity id="o17291" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="glaucous-green" value_original="glaucous-green" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe (3–) 4–5 cm.</text>
      <biological_entity id="o17292" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect;</text>
      <biological_entity id="o17293" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth white, sometimes flushed pink abaxially, more so with age, broadly funnelform, 6–9 cm;</text>
      <biological_entity id="o17294" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="coloration" src="d0_s3" value="flushed pink" value_original="flushed pink" />
        <character char_type="range_value" from="6" from_unit="cm" modifier="with age; broadly" name="some_measurement" src="d0_s3" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube white to green, 3–4 (–4.7) cm, diam. primarily uniform, ca. 1/2 perianth length, more than 15 times filament length, equaling (3/4–11/4) spathe length;</text>
      <biological_entity id="o17295" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="green" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s4" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="distance" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="primarily" name="diam" src="d0_s4" value="uniform" value_original="uniform" />
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o17296" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character constraint="filament" constraintid="o17297" is_modifier="false" name="length" src="d0_s4" value="15+ times filament length" value_original="15+ times filament length" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character char_type="range_value" from="3/4" name="quantity" src="d0_s4" to="11/4" />
      </biological_entity>
      <biological_entity id="o17297" name="filament" name_original="filament" src="d0_s4" type="structure" />
      <biological_entity id="o17298" name="spathe" name_original="spathe" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>tepals rarely reflexed;</text>
      <biological_entity id="o17299" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens fasciculate, distinctly subequal;</text>
      <biological_entity id="o17300" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments subulate, 0.1–0.2 cm, apex acute;</text>
      <biological_entity id="o17301" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s7" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17302" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 5–8 mm;</text>
      <biological_entity id="o17303" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style notably shorter than perianth-tube;</text>
      <biological_entity id="o17304" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o17305" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="notably shorter" value_original="notably shorter" />
      </biological_entity>
      <biological_entity id="o17305" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma 3-fid, included in perianth-tube;</text>
      <biological_entity id="o17306" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
      </biological_entity>
      <biological_entity id="o17307" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure" />
      <relation from="o17306" id="r2378" name="included in" negation="false" src="d0_s10" to="o17307" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel (0.2–) 0.5–2 (–3.3) cm, shorter than spathe.</text>
      <biological_entity id="o17309" name="spathe" name_original="spathe" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 48, 72.</text>
      <biological_entity id="o17308" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
        <character constraint="than spathe" constraintid="o17309" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17310" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
        <character name="quantity" src="d0_s12" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring–mid summer (Mar–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="mid spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy/rocky, usually calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" modifier="sandy\/rocky usually calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., Tex.; ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Cebolleta</other_name>
  <discussion>Specimens of Zephyranthes drummondii with rare biflowered inflorescences were collected by B. C. Tharp in 1939 and 1946 in Austin, Texas. The species has naturalized in Florida.</discussion>
  
</bio:treatment>