<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1833" rank="species">luteus</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>19: plate 1567. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species luteus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101477</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mariposa</taxon_name>
    <taxon_name authority="(Douglas ex Lindley) Hoover" date="unknown" rank="species">lutea</taxon_name>
    <taxon_hierarchy>genus Mariposa;species lutea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems slender, 2–5 dm.</text>
      <biological_entity id="o21444" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal withering, 1–2 dm;</text>
      <biological_entity id="o21445" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o21446" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="withering" value_original="withering" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear.</text>
      <biological_entity id="o21447" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21448" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences subumbellate, 1–4-flowered;</text>
      <biological_entity id="o21449" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 1–8 cm.</text>
      <biological_entity id="o21450" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers erect;</text>
      <biological_entity id="o21451" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o21452" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals lanceolate-oblong, attenuate, 2–3 cm;</text>
      <biological_entity id="o21453" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals deep yellow, usually streaked-redbrown proximally, often with median redbrown blotch, cuneate to obovate, 2–4 cm, with a few slender hairs near gland;</text>
      <biological_entity id="o21454" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="usually; proximally" name="coloration" src="d0_s8" value="streaked-redbrown" value_original="streaked-redbrown" />
        <character char_type="range_value" from="cuneate" name="shape" notes="" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="median" id="o21455" name="blotch" name_original="blotch" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity constraint="slender" id="o21456" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o21457" name="gland" name_original="gland" src="d0_s8" type="structure" />
      <relation from="o21454" id="r2931" modifier="often" name="with" negation="false" src="d0_s8" to="o21455" />
      <relation from="o21454" id="r2932" name="with" negation="false" src="d0_s8" to="o21456" />
      <relation from="o21456" id="r2933" name="near" negation="false" src="d0_s8" to="o21457" />
    </statement>
    <statement id="d0_s9">
      <text>glands ± lunate to oblong, not depressed, covered with short, matted hairs;</text>
      <biological_entity id="o21458" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character char_type="range_value" from="less lunate" name="shape" src="d0_s9" to="oblong" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="depressed" value_original="depressed" />
      </biological_entity>
      <biological_entity id="o21459" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" name="growth_form" src="d0_s9" value="matted" value_original="matted" />
      </biological_entity>
      <relation from="o21458" id="r2934" name="covered with" negation="false" src="d0_s9" to="o21459" />
    </statement>
    <statement id="d0_s10">
      <text>filaments 7–9 mm;</text>
      <biological_entity id="o21460" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers linear-oblong, 4–6 mm, apex obtuse or acute.</text>
      <biological_entity id="o21461" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="linear-oblong" value_original="linear-oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21462" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules erect, lanceoloid-linear, angled, 3–6 cm.</text>
      <biological_entity id="o21463" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="lanceoloid-linear" value_original="lanceoloid-linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s12" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds light beige, flat.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14, 28.</text>
      <biological_entity id="o21464" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="light beige" value_original="light beige" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21465" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy soils in grasslands, open woodlands, mixed evergreen forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy soils" constraint="in grasslands , open woodlands ," />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="mixed evergreen forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <discussion>Coastal plants of Calochortus luteus are mostly triploid, while those of the interior are mostly diploid. Occasionally this species hybridizes with C. superbus.</discussion>
  
</bio:treatment>