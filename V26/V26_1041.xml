<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">511</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="(Lindley) Szlachetko" date="1995" rank="subfamily">Vanilloideae</taxon_name>
    <taxon_name authority="Blume" date="1837" rank="tribe">Vanilleae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="subtribe">Pogoniinae</taxon_name>
    <taxon_name authority="Richard ex Lindley" date="1840" rank="genus">cleistes</taxon_name>
    <taxon_name authority="(Fernald) Catling &amp; Gregg" date="1992" rank="species">bifaria</taxon_name>
    <place_of_publication>
      <publication_title>Lindleyana</publication_title>
      <place_in_publication>7: 65. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe pogoniinae;genus cleistes;species bifaria;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101527</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleistes</taxon_name>
    <taxon_name authority="(Linnaeus) Ames" date="unknown" rank="species">divaricata</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">bifaria</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>48: 186, plate 1048. 1946</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cleistes;species divaricata;variety bifaria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (12–) 15–46 (–64) cm (in flower).</text>
      <biological_entity id="o19454" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="46" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="64" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="46" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blades on flowering-stems, (2.5–) 45–145 × (4–) 6–25 mm.</text>
      <biological_entity id="o19455" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19456" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" notes="" src="d0_s1" to="45" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" notes="" src="d0_s1" to="145" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" notes="" src="d0_s1" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19457" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure" />
      <relation from="o19456" id="r2671" name="on" negation="false" src="d0_s1" to="o19457" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 24–55 × (2–) 3–5 mm;</text>
      <biological_entity id="o19458" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o19459" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals 21–36 × 6–10 (–12) mm;</text>
      <biological_entity id="o19460" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o19461" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s3" to="36" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lip 21–33 (–38) × 13–16 mm, central keel relatively thick, 2.4–2.6 mm wide, very slightly grooved, groove 0.3 mm deep and verrucose or braided with 5–7 discontinuous ridges;</text>
      <biological_entity id="o19462" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19463" name="lip" name_original="lip" src="d0_s4" type="structure">
        <character char_type="range_value" from="33" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="38" to_unit="mm" />
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s4" to="33" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o19464" name="keel" name_original="keel" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s4" to="2.6" to_unit="mm" />
        <character is_modifier="false" modifier="very slightly" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o19465" name="groove" name_original="groove" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="relief" src="d0_s4" value="verrucose" value_original="verrucose" />
        <character constraint="with ridges" constraintid="o19466" is_modifier="false" name="architecture" src="d0_s4" value="braided" value_original="braided" />
      </biological_entity>
      <biological_entity id="o19466" name="ridge" name_original="ridges" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="discontinuous" value_original="discontinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>column 13–19 mm.</text>
      <biological_entity id="o19467" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19468" name="column" name_original="column" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s5" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules (9–) 27–41 (–81) × (3.2–) 4–8 (–8.5) mm.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 18.</text>
      <biological_entity id="o19469" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s6" to="27" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="41" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="81" to_unit="mm" />
        <character char_type="range_value" from="27" from_unit="mm" name="length" src="d0_s6" to="41" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="atypical_width" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19470" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May (coastal plain) –Jul (mountains).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="coastal plain" to="May" from="Apr" />
        <character name="flowering time" char_type="range_value" modifier="mountains" to="Jul" from="" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Savannas, meadows, openings in oak or pine woodlands, mountain habitat often xeric, in acidic soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="savannas" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="openings" constraint="in oak" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine woodlands" />
        <character name="habitat" value="mountain habitat" />
        <character name="habitat" value="acidic soil" />
        <character name="habitat" value="xeric" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Ky., La., Miss., N.C., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Smaller spreading pogonia</other_name>
  <discussion>A station in Barbour County, West Virginia, is 240 km north of other stations and may be a result of introduction (K. B. Gregg 1989).</discussion>
  <discussion>Cleistes bifaria has been treated as a variety of the following species; it is readily separated by its relatively short column, which is correlated with the relatively small size of other floral parts, and differences in lip keel. It is, to a large extent, isolated from the following taxon by geographic distribution and flowering time where their distributions overlap (P. M. Catling and K. B. Gregg 1992).</discussion>
  <references>
    <reference>  Gregg, K. B. 1989. Reproductive biology of the orchid Cleistes divaricata (L.) Ames var. bifaria Fernald growing in a West Virginia meadow. Castanea 54: 57–78.</reference>
  </references>
  
</bio:treatment>