<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">634</other_info_on_meta>
    <other_info_on_meta type="treatment_page">636</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="Conrad" date="1829" rank="species">wisteriana</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>6: 145. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species wisteriana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101538</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Suksdorf" date="unknown" rank="species">hortensis</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species hortensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems red-purple to yellowish-brown to yellow, base not strongly thickened nor bulbous.</text>
      <biological_entity id="o757" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="red-purple" name="coloration" src="d0_s0" to="yellowish-brown" />
      </biological_entity>
      <biological_entity id="o758" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not strongly" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: racemes lax to dense, 10–55 × 1.3–3.5 cm.</text>
      <biological_entity id="o759" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o760" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="55" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s1" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 2–25, inconspicuous;</text>
      <biological_entity id="o761" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="25" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth open;</text>
      <biological_entity id="o762" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals and petals purplish brown, often greenish distally, or pure yellow, 3-veined;</text>
      <biological_entity id="o763" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o764" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals lanceolate, 4.5–10 mm;</text>
      <biological_entity id="o765" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals upcurved, connivent with dorsal sepal and petals, forming hood over column, somewhat falcate;</text>
      <biological_entity constraint="lateral" id="o766" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="upcurved" value_original="upcurved" />
        <character constraint="with petals" constraintid="o768" is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
        <character is_modifier="false" modifier="somewhat" name="shape" notes="" src="d0_s6" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o767" name="sepal" name_original="sepal" src="d0_s6" type="structure" />
      <biological_entity id="o768" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o769" name="hood" name_original="hood" src="d0_s6" type="structure" />
      <biological_entity id="o770" name="column" name_original="column" src="d0_s6" type="structure" />
      <relation from="o766" id="r103" name="forming" negation="false" src="d0_s6" to="o769" />
      <relation from="o766" id="r104" name="over" negation="false" src="d0_s6" to="o770" />
    </statement>
    <statement id="d0_s7">
      <text>petals often spotted with purple, broadly lanceolate, 4–7.2 mm;</text>
      <biological_entity id="o771" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="spotted with purple" value_original="spotted with purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip white, often spotted with purple, ovate to suborbiculate, unlobed, 4–7.5 × 2.5–7.5 mm, with 2 distinct basal lamellae, margins erose-denticulate to nearly entire;</text>
      <biological_entity id="o772" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="spotted with purple" value_original="spotted with purple" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s8" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o773" name="lamella" name_original="lamellae" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o774" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="erose-denticulate" name="shape" src="d0_s8" to="nearly entire" />
      </biological_entity>
      <relation from="o772" id="r105" name="with" negation="false" src="d0_s8" to="o773" />
    </statement>
    <statement id="d0_s9">
      <text>column curved toward lip, yellow to white, often with purplish apex, 2.4–5 mm, with basal auricles;</text>
      <biological_entity id="o775" name="column" name_original="column" src="d0_s9" type="structure">
        <character constraint="toward lip" constraintid="o776" is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s9" to="white" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o776" name="lip" name_original="lip" src="d0_s9" type="structure" />
      <biological_entity id="o777" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="basal" id="o778" name="auricle" name_original="auricles" src="d0_s9" type="structure" />
      <relation from="o775" id="r106" modifier="often" name="with" negation="false" src="d0_s9" to="o777" />
      <relation from="o775" id="r107" name="with" negation="false" src="d0_s9" to="o778" />
    </statement>
    <statement id="d0_s10">
      <text>ovary 3–8 mm;</text>
      <biological_entity id="o779" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mentum present.</text>
      <biological_entity id="o780" name="mentum" name_original="mentum" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ellipsoid-obovoid, 7–14 × 3.5–6.2 mm.</text>
      <biological_entity id="o781" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid-obovoid" value_original="ellipsoid-obovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s12" to="6.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Jul, according to latitude.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="according to latitude" to="Jul" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous and coniferous woods, often preferring richer habitats than other coral-roots</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="richer habitats" constraint="than other coral-roots" />
        <character name="habitat" value="other coral-roots" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Colo., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Kans., Ky., La., Md., Miss., Mo., Mont., Nebr., N.J., N.Mex., N.C., Ohio, Okla., Pa., S.C., S.Dak., Tenn., Tex., Utah, Va., Wash., W.Va., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Wister’s coral-root</other_name>
  <discussion>Western collections of Corallorhiza wisteriana have frequently been misidentified as C. maculata. Antho-cyanin-free forms from the central states have been misidentified as C. trifida.</discussion>
  
</bio:treatment>