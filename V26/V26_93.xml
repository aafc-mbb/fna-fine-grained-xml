<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">82</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="genus">AMIANTHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>4: 121. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus AMIANTHIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">merged Greek amiantos, unsoiled, and anthos, flower, alluding to the glandless tepals</other_info_on_name>
    <other_info_on_name type="fna_id">101358</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Chrosperma</taxon_name>
    <place_of_publication>
      <place_in_publication>1825</place_in_publication>
      <other_info_on_pub>name rejected</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Chrosperma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from bulbs;</text>
      <biological_entity id="o9938" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9939" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <relation from="o9938" id="r1416" name="from" negation="false" src="d0_s0" to="o9939" />
    </statement>
    <statement id="d0_s1">
      <text>bulbs tunicate, ovoid-oblong, ca. 5 × 2.5 cm;</text>
      <biological_entity id="o9940" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovoid-oblong" value_original="ovoid-oblong" />
        <character name="length" src="d0_s1" unit="cm" value="5" value_original="5" />
        <character name="width" src="d0_s1" unit="cm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots contractile.</text>
      <biological_entity id="o9941" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="contractile" value_original="contractile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, simple.</text>
      <biological_entity id="o9942" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal, reduced distally, spiral, arching downward, sheathing proximally;</text>
      <biological_entity id="o9943" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s4" value="spiral" value_original="spiral" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="downward" value_original="downward" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o9944" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-elongate, strap-shaped, glabrous, apex obtuse to abruptly acute.</text>
      <biological_entity id="o9945" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-elongate" value_original="linear-elongate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="strap--shaped" value_original="strap--shaped" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9946" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="abruptly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemose, dense, bracteate, glabrous.</text>
      <biological_entity id="o9947" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual or both bisexual and unisexual flowers borne on the same or on different plants, cupshaped to widely spreading, pedicellate;</text>
      <biological_entity id="o9948" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character name="reproduction" src="d0_s7" value="both" value_original="both" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o9949" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o9950" name="the same" name_original="the same" src="d0_s7" type="structure" />
      <biological_entity id="o9951" name="plant" name_original="plants" src="d0_s7" type="structure" />
      <relation from="o9949" id="r1417" name="borne on" negation="false" src="d0_s7" to="o9950" />
      <relation from="o9949" id="r1418" name="borne on" negation="false" src="d0_s7" to="o9951" />
    </statement>
    <statement id="d0_s8">
      <text>tepals persistent, 6, distinct, white, turning reddish green, claws absent, oblong-obovate, equal to subequal, apex acute;</text>
      <biological_entity id="o9952" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9953" name="claw" name_original="claws" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish green" value_original="reddish green" />
      </biological_entity>
      <biological_entity id="o9954" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="true" name="shape" src="d0_s8" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="true" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="true" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish green" value_original="reddish green" />
      </biological_entity>
      <relation from="o9952" id="r1419" name="turning" negation="false" src="d0_s8" to="o9953" />
      <relation from="o9952" id="r1420" name="turning" negation="false" src="d0_s8" to="o9954" />
    </statement>
    <statement id="d0_s9">
      <text>tepal glands absent;</text>
      <biological_entity constraint="tepal" id="o9955" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, perigynous, distinct;</text>
      <biological_entity id="o9956" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments subulate, apex acute;</text>
      <biological_entity id="o9957" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o9958" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers basifixed, 1-locular, cordate-ovate, extrorse, opening into peltate disc (apical/valvate dehiscence);</text>
      <biological_entity id="o9959" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s12" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cordate-ovate" value_original="cordate-ovate" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s12" value="extrorse" value_original="extrorse" />
      </biological_entity>
      <biological_entity id="o9960" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="peltate" value_original="peltate" />
      </biological_entity>
      <relation from="o9959" id="r1421" name="opening into" negation="false" src="d0_s12" to="o9960" />
    </statement>
    <statement id="d0_s13">
      <text>pollen-sacs apically confluent;</text>
      <biological_entity id="o9961" name="pollen-sac" name_original="pollen-sacs" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="apically" name="arrangement" src="d0_s13" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior to partly inferior, 3-locular, diverging;</text>
      <biological_entity id="o9962" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="superior" name="position" src="d0_s14" to="partly inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="diverging" value_original="diverging" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>septal nectaries absent;</text>
      <biological_entity constraint="septal" id="o9963" name="nectary" name_original="nectaries" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles persistent, 3, widely diverging, distinct;</text>
      <biological_entity id="o9964" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s16" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas terminal, minute.</text>
      <biological_entity id="o9965" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s17" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsular, deeply 3-lobed, thin-walled, dehiscence septicidal, then adaxially loculicidal.</text>
      <biological_entity id="o9966" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="capsular" value_original="capsular" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s18" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" modifier="adaxially" name="dehiscence" src="d0_s18" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1–2 per locule, brownish to purplish black, wingless, ellipsoid, lustrous;</text>
      <biological_entity id="o9967" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o9968" from="1" name="quantity" src="d0_s19" to="2" />
        <character char_type="range_value" from="brownish" name="coloration" notes="" src="d0_s19" to="purplish black" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="wingless" value_original="wingless" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="lustrous" value_original="lustrous" />
      </biological_entity>
      <biological_entity id="o9968" name="locule" name_original="locule" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>testa fleshy.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 8.</text>
      <biological_entity id="o9969" name="testa" name_original="testa" src="d0_s20" type="structure">
        <character is_modifier="false" name="texture" src="d0_s20" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="x" id="o9970" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Fly-poison</other_name>
  <other_name type="common_name">crow-poison</other_name>
  <other_name type="common_name">St. Elmo’s-feather</other_name>
  <other_name type="common_name">staggergrass</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Amianthium is frequently included in a broadly circumscribed Zigadenus (J. D. Ambrose 1975, 1980; S. M. Kupchan et al. 1961; S. J. Preece 1956; W. B. Zomlefer 1997b). The absence of tepal nectaries or glands is a major generic characteristic of this Appalachian–Ozark taxon (F. H. Utech 1986). Narrow-leaved specimens of Amianthium are often confused with Zigadenus densus.</discussion>
  <discussion>A self-incompatible, pollination-breeding system involving primarily beetles occurs in Amianthium (M. E. Palmer et al. 1989; A. M. Redmon et al. 1989; J. Travis 1984). An associated tepal color shift, as in Zigadenus densus, may represent possible pollinator signals (J. Travis 1984).</discussion>
  <discussion>Two unique, toxic alkaloids, jervine and amianthine, found in Amianthium roots and leaves (N. Neuss 1953; S. M. Kupchan et al. 1961; R. F. Raffauf 1970; I. W. Southon and J. Buckingham 1989; G. E. Burrows and R. J. Tyrl 2001), caused deaths in cattle and sheep. Native Cherokee used the plant as a dermatological cure for itch and as a crow poison (D. E. Moerman 1986). Root extracts mixed with molasses or honey have been used as a housefly insecticide (C. D. Marsh et al. 1918).</discussion>
  <references>
    <reference>Utech, F. H. 1986. Floral morphology and vascular anatomy of Amianthium muscaetoxicum (Walter) A. Gray (Liliaceae–Veratreae) with notes on distribution and taxonomy. Ann. Carnegie Mus. 55: 481–504. </reference>
  </references>
  
</bio:treatment>