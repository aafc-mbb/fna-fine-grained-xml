<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1836" rank="genus">olsynium</taxon_name>
    <taxon_name authority="(A. Dietrich) E. P. Bicknell" date="1900" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>27: 237. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus olsynium;species douglasii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101810</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="A. Dietrich" date="unknown" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 504. 1833,</place_in_publication>
      <other_info_on_pub>based on S. grandiflorum Douglas ex Lindley, Edwards’s Bot. Reg. 16: plate 1364. 1830, not Cavanilles 1790</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Sisyrinchium;species douglasii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 4.1 dm × 1–4.6 mm.</text>
      <biological_entity id="o22365" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="length" src="d0_s0" to="4.1" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1.5–4.5 mm wide distal to midlength.</text>
      <biological_entity id="o22366" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s1" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="distal" name="position" src="d0_s1" to="midlength" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spathes 4.3–7 mm wide, outer 2–6.2 cm;</text>
      <biological_entity id="o22367" name="spathe" name_original="spathes" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.3" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s2" value="outer" value_original="outer" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="6.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hyaline margins 0.2–0.5 (–1.6) mm wide, extending to just proximal to, or occasionally exceeding, green apex.</text>
      <biological_entity id="o22368" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="width" src="d0_s3" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22369" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="just" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="true" modifier="occasionally" name="position_relational" src="d0_s3" value="exceeding" value_original="exceeding" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <relation from="o22368" id="r3040" name="extending to" negation="false" src="d0_s3" to="o22369" />
    </statement>
    <statement id="d0_s4">
      <text>Tepals 12.5–33 mm;</text>
      <biological_entity id="o22370" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="12.5" from_unit="mm" name="some_measurement" src="d0_s4" to="33" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments 6–12.2 mm;</text>
      <biological_entity id="o22371" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="12.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 3–10.2 mm;</text>
      <biological_entity id="o22372" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="10.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary 2–5.6 mm;</text>
      <biological_entity id="o22373" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 2–3.8 cm.</text>
      <biological_entity id="o22374" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4.5–11.3 mm. 2n = 64.</text>
      <biological_entity id="o22375" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="11.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22376" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filament column abruptly and broadly flared at base.</description>
      <determination>1a Olsynium douglasii var. inflatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filament column slightly or not flared.</description>
      <determination>1b Olsynium douglasii var. douglasii</determination>
    </key_statement>
  </key>
</bio:treatment>