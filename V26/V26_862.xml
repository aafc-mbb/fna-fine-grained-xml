<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">nolina</taxon_name>
    <taxon_name authority="Gentry" date="1946" rank="species">interrata</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>8: 181, fig. 1, plate 19. 1946</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus nolina;species interrata</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101801</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent;</text>
      <biological_entity id="o27365" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes from branching, horizontal, subterranean caudices from lignotubers.</text>
      <biological_entity id="o27366" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity id="o27367" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
        <character is_modifier="true" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <biological_entity id="o27368" name="lignotuber" name_original="lignotubers" src="d0_s1" type="structure" />
      <relation from="o27366" id="r3714" name="from" negation="false" src="d0_s1" to="o27367" />
      <relation from="o27367" id="r3715" name="from" negation="false" src="d0_s1" to="o27368" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 30–150 cm × 12–35 mm, glaucous;</text>
      <biological_entity id="o27369" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s2" to="35" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bases 15–70 mm wide;</text>
      <biological_entity id="o27370" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins serrulate.</text>
      <biological_entity id="o27371" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape 0.7–9 dm, 5–18 mm diam.</text>
      <biological_entity id="o27372" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s5" to="9" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences compound paniculate, branched distally, 5–11 dm × 10–50 cm;</text>
      <biological_entity id="o27373" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="compound" value_original="compound" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="dm" name="length" src="d0_s6" to="11" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts persistent, 2–15 cm;</text>
      <biological_entity id="o27374" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bractlets laciniate.</text>
      <biological_entity id="o27375" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals 2–3.5 mm;</text>
      <biological_entity id="o27376" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27377" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>fertile stamens: anthers 1.1–1.7 mm;</text>
      <biological_entity id="o27378" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o27379" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel erect, proximal to joint 1–2 (–4.5) mm, distal to joint 1–2 mm.</text>
      <biological_entity id="o27380" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o27381" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character constraint="to joint" constraintid="o27382" is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character constraint="to joint" constraintid="o27383" is_modifier="false" name="position_or_shape" notes="" src="d0_s11" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o27382" name="joint" name_original="joint" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27383" name="joint" name_original="joint" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules thin-walled, 7–12 × 9–15 mm, notched basally and apically.</text>
      <biological_entity id="o27384" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="thin-walled" value_original="thin-walled" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="basally; apically" name="shape" src="d0_s12" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds bursting ovary walls, 4–6 × 3–4 mm.</text>
      <biological_entity id="o27385" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o27386" name="wall" name_original="walls" src="d0_s13" type="structure" />
      <relation from="o27385" id="r3716" name="bursting" negation="false" src="d0_s13" to="o27386" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides of chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" constraint="of chaparral" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Dehesa beargrass</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Nolina interrata is very rare and is listed by the U.S. Fish and Wildlife Service as rare and endangered. Its habitat is threatened by development.</discussion>
  
</bio:treatment>