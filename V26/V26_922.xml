<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">schottii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">schottii</taxon_name>
    <taxon_hierarchy>family agavaceae;genus agave;species schottii;variety schottii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102126</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 0.7–1.2 cm wide;</text>
      <biological_entity id="o17045" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s0" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade yellowish green with conspicous budprints on both surfaces, margins filiferous, apical spine grayish, weak and brittle, 0.8–1.2 cm.</text>
      <biological_entity id="o17046" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character constraint="with budprints" constraintid="o17047" is_modifier="false" name="coloration" src="d0_s1" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity id="o17047" name="budprint" name_original="budprints" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="conspicous" value_original="conspicous" />
      </biological_entity>
      <biological_entity id="o17048" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
      <biological_entity id="o17049" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="filiferous" value_original="filiferous" />
      </biological_entity>
      <biological_entity constraint="apical" id="o17050" name="spine" name_original="spine" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="weak" value_original="weak" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s1" to="1.2" to_unit="cm" />
      </biological_entity>
      <relation from="o17047" id="r2339" name="on" negation="false" src="d0_s1" to="o17048" />
    </statement>
    <statement id="d0_s2">
      <text>Scape 1.6–2.5 m.</text>
      <biological_entity id="o17051" name="scape" name_original="scape" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.6" from_unit="m" name="some_measurement" src="d0_s2" to="2.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences spicate;</text>
      <biological_entity id="o17052" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncle shorter than 2 mm.</text>
      <biological_entity id="o17053" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s4" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1 or 2–3 per cluster, 2.9–4.2 cm;</text>
      <biological_entity id="o17054" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" constraint="per cluster , 2.9-4.2 cm" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth-tube 8–11 (–14) × 5–8 mm, limb lobes light yellow;</text>
      <biological_entity id="o17055" name="perianth-tube" name_original="perianth-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o17056" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="light yellow" value_original="light yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments inserted 6–13 mm above perianth base;</text>
      <biological_entity id="o17057" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="inserted" value_original="inserted" />
        <character char_type="range_value" constraint="above perianth base" constraintid="o17058" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="perianth" id="o17058" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>ovary 0.8–1.5 cm. 2n = 60, 120.</text>
      <biological_entity id="o17059" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17060" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
        <character name="quantity" src="d0_s8" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky places, mostly in desert scrub, grasslands, juniper and oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky places" />
        <character name="habitat" value="desert scrub" modifier="mostly in" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <other_name type="common_name">Schott agave</other_name>
  <discussion>Agave schottii var. schottii hybridizes with A. deserti var. simplex, A. chrysantha, and possibly A. palmeri or A. parryi var. parryi.</discussion>
  
</bio:treatment>