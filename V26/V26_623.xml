<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey ex Durand" date="unknown" rank="genus">schoenolirion</taxon_name>
    <taxon_name authority="(Michaux) Alph. Wood" date="1870" rank="species">croceum</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Bot. Fl.,</publication_title>
      <place_in_publication>344. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus schoenolirion;species croceum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101885</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phalangium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">croceum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 196. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phalangium;species croceum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxytria</taxon_name>
    <taxon_name authority="(Michaux) Rafinesque" date="unknown" rank="species">crocea</taxon_name>
    <taxon_hierarchy>genus Oxytria;species crocea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with prominent bulbs at tops of vertical rootstocks;</text>
      <biological_entity id="o4455" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4456" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o4457" name="top" name_original="tops" src="d0_s0" type="structure" />
      <biological_entity id="o4458" name="rootstock" name_original="rootstocks" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o4455" id="r653" name="with" negation="false" src="d0_s0" to="o4456" />
      <relation from="o4456" id="r654" name="at" negation="false" src="d0_s0" to="o4457" />
      <relation from="o4457" id="r655" name="part_of" negation="false" src="d0_s0" to="o4458" />
    </statement>
    <statement id="d0_s1">
      <text>bulbs ovoid to elongate, to 17 mm diam.</text>
      <biological_entity id="o4459" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s1" to="elongate" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s1" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–7, 20–32 cm × 5–8 mm, not withering to persistent fibers;</text>
      <biological_entity id="o4460" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="32" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o4461" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade flattened or somewhat keeled, usually equaling or exceeding inflorescence, base fleshy.</text>
      <biological_entity id="o4462" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s3" value="exceeding inflorescence" value_original="exceeding inflorescence" />
      </biological_entity>
      <biological_entity id="o4463" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences seldom branched;</text>
      <biological_entity id="o4464" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts ovate-obtuse to lanceolate-acuminate.</text>
      <biological_entity id="o4465" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate-obtuse" name="shape" src="d0_s5" to="lanceolate-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals seldom strongly recurved, yellow, each with green or reddish abaxial stripe, 3–5-veined, ovate to ovate-oblong, 4.5–6 mm, apex obtuse;</text>
      <biological_entity id="o4466" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4467" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="seldom strongly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="3-5-veined" value_original="3-5-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="ovate-oblong" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4468" name="stripe" name_original="stripe" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o4469" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o4467" id="r656" name="with" negation="false" src="d0_s6" to="o4468" />
    </statement>
    <statement id="d0_s7">
      <text>ovary green or greenish yellow.</text>
      <biological_entity id="o4470" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 24, 30, 32.</text>
      <biological_entity id="o4471" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4472" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
        <character name="quantity" src="d0_s8" value="30" value_original="30" />
        <character name="quantity" src="d0_s8" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Mar–mid Apr in south and west; Apr–mid May in north; dormant by late Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in south and west" to="mid Apr" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, moist pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="moist pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., N.C., S.C., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Sunnybells</other_name>
  <other_name type="common_name">swamp candle</other_name>
  <discussion>Schoenolirion croceum is very localized on limestone outcrops in central Tennessee and northern Alabama, sandstone outcrops of the Alabama plateau region, granite outcrops of the Georgia and Carolina Piedmont, Selma chalk outcrops in western Alabama, and wet pinelands and boggy areas in more southern and western parts of its range. All of these habitats are very wet in spring and often very dry in summer.</discussion>
  
</bio:treatment>