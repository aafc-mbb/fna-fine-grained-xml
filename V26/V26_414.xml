<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="treatment_page">239</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="M. E. Jones" date="1930" rank="species">coryi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>17: 21. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species coryi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101345</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–5, without basal bulbels, ovoid, 1–1.8 × 0.7–1.5 cm;</text>
      <biological_entity id="o7259" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="5" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7260" name="bulbel" name_original="bulbels" src="d0_s0" type="structure" />
      <relation from="o7259" id="r1028" name="without" negation="false" src="d0_s0" to="o7260" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, brown, reticulate, cells fine-meshed, open, fibrous;</text>
      <biological_entity constraint="outer" id="o7261" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="fine-meshed" value_original="fine-meshed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="open" value_original="open" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o7262" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o7261" id="r1029" name="enclosing" negation="false" src="d0_s1" to="o7262" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats whitish or brownish, cells intricately contorted, walls not sinuous.</text>
      <biological_entity constraint="inner" id="o7263" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o7264" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="intricately" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
      </biological_entity>
      <biological_entity id="o7265" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s2" value="sinuous" value_original="sinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, green at anthesis, 3–5, sheathing;</text>
      <biological_entity id="o7266" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, channeled, 10–30 cm × 1–3 mm, margins entire.</text>
      <biological_entity id="o7267" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7268" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, ± terete, 10–30 cm × 1–3 mm.</text>
      <biological_entity id="o7269" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact or ± loose, usually 10–25-flowered, hemispheric-globose, bulbils unknown;</text>
      <biological_entity id="o7270" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="10-25-flowered" value_original="10-25-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric-globose" value_original="hemispheric-globose" />
      </biological_entity>
      <biological_entity id="o7271" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, 1-veined, ovate, ± equal, apex acuminate.</text>
      <biological_entity constraint="spathe" id="o7272" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o7273" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate to ± stellate, 6–9 mm;</text>
      <biological_entity id="o7274" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="more or less stellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals spreading, bright-yellow, sometimes tinged with red, fading with age and sometimes upon drying, ovate to lanceolate, ± equal, becoming papery and rigid in fruit, margins entire, apex obtuse or acute, midribs somewhat thickened;</text>
      <biological_entity id="o7275" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="tinged with red" value_original="tinged with red" />
        <character constraint="with age" constraintid="o7276" is_modifier="false" name="coloration" src="d0_s9" value="fading" value_original="fading" />
      </biological_entity>
      <biological_entity id="o7276" name="age" name_original="age" src="d0_s9" type="structure" />
      <biological_entity id="o7277" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="true" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="true" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character is_modifier="true" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7278" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="true" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character is_modifier="true" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7279" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="true" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="true" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character is_modifier="true" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7280" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o7275" id="r1030" modifier="sometimes" name="upon" negation="false" src="d0_s9" to="o7277" />
      <relation from="o7275" id="r1031" modifier="sometimes" name="upon" negation="false" src="d0_s9" to="o7278" />
      <relation from="o7275" id="r1032" modifier="sometimes" name="upon" negation="false" src="d0_s9" to="o7279" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o7281" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o7282" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o7283" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crestless or rarely crested;</text>
      <biological_entity id="o7284" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="crestless" value_original="crestless" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, central, low, sometimes distinct or connate in pairs across septa, rounded, margins entire;</text>
      <biological_entity id="o7285" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" modifier="sometimes" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character constraint="in pairs" constraintid="o7286" is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7286" name="pair" name_original="pairs" src="d0_s14" type="structure" />
      <biological_entity id="o7287" name="septum" name_original="septa" src="d0_s14" type="structure" />
      <biological_entity id="o7288" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o7286" id="r1033" name="across" negation="false" src="d0_s14" to="o7287" />
    </statement>
    <statement id="d0_s15">
      <text>style linear, ± equaling stamens;</text>
      <biological_entity id="o7289" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o7290" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, unlobed or obscurely lobed;</text>
      <biological_entity id="o7291" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 5–20 mm.</text>
      <biological_entity id="o7292" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat shining;</text>
      <biological_entity id="o7293" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o7294" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7295" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and plains, mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Allium coryi is known only from western Texas.</discussion>
  
</bio:treatment>