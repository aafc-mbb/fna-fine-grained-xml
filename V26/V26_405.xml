<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">geyeri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 227. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species geyeri</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101360</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 2–10+, not rhizomatous, ovoid or more elongate, 1–2.5 × 0.8–2 cm;</text>
      <biological_entity id="o34527" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, gray or brown, reticulate, cells rather coarse-meshed, open, fibrous;</text>
      <biological_entity constraint="outer" id="o34528" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s1" value="coarse-meshed" value_original="coarse-meshed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="open" value_original="open" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o34529" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o34528" id="r4645" name="enclosing" negation="false" src="d0_s1" to="o34529" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats whitish, cells vertically elongate and regular or obscure.</text>
      <biological_entity constraint="inner" id="o34530" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o34531" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="vertically" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="regular" value_original="regular" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, usually green at anthesis, usually 3–5, sheathing less than 1/4 scape;</text>
      <biological_entity id="o34532" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="3" modifier="usually" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1/4" />
      </biological_entity>
      <biological_entity id="o34533" name="scape" name_original="scape" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, ± straight, flat, channeled, (6–) 12–30 cm × 1–3 (–5) mm, margins entire or denticulate.</text>
      <biological_entity id="o34534" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_length" src="d0_s4" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34535" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, terete or somewhat 2-angled, 10–50 cm × 1–3 mm.</text>
      <biological_entity id="o34536" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="2-angled" value_original="2-angled" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="50" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact, 10–25-flowered, hemispheric to globose, not producing bulbils, or 0–5-flowered, largely replaced by ovoid, acuminate bulbils;</text>
      <biological_entity id="o34537" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-25-flowered" value_original="10-25-flowered" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="globose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="0-5-flowered" value_original="0-5-flowered" />
      </biological_entity>
      <biological_entity id="o34538" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
      <biological_entity id="o34539" name="bulbil" name_original="bulbils" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character is_modifier="true" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o34537" id="r4646" name="producing" negation="true" src="d0_s6" to="o34538" />
      <relation from="o34537" id="r4647" modifier="largely" name="replaced by" negation="false" src="d0_s6" to="o34539" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, mostly 1-veined, ovate to lanceolate, ± equal, apex acuminate, beakless.</text>
      <biological_entity constraint="spathe" id="o34540" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s7" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o34541" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers urceolate-campanulate, (4–) 6–8 (–10) mm;</text>
      <biological_entity id="o34542" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="urceolate-campanulate" value_original="urceolate-campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect or spreading, pink to white, ovate to lanceolate, ± equal, not withering in fruit and permanently investing fruit, or withering if fruit not produced, midribs papillose, becoming callous-keeled, margins often obscurely toothed, apex obtuse to acuminate;</text>
      <biological_entity id="o34543" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="white" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o34544" is_modifier="false" modifier="not" name="life_cycle" src="d0_s9" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="permanently" name="life_cycle" src="d0_s9" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o34544" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o34545" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o34546" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o34547" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s9" value="callous-keeled" value_original="callous-keeled" />
      </biological_entity>
      <biological_entity id="o34548" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often obscurely" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o34549" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
      <relation from="o34543" id="r4648" modifier="permanently" name="investing" negation="false" src="d0_s9" to="o34545" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o34550" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o34551" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o34552" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary when present, inconspicuously crested;</text>
      <biological_entity id="o34553" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="when present; inconspicuously" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, central, low, distinct or connate in pairs across septa, ± erect, rounded, margins entire, becoming variously developed or obsolete in fruit;</text>
      <biological_entity id="o34554" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character constraint="in pairs" constraintid="o34555" is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o34555" name="pair" name_original="pairs" src="d0_s14" type="structure" />
      <biological_entity id="o34556" name="septum" name_original="septa" src="d0_s14" type="structure" />
      <biological_entity id="o34557" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="becoming variously" name="development" src="d0_s14" value="developed" value_original="developed" />
        <character constraint="in fruit" constraintid="o34558" is_modifier="false" name="prominence" src="d0_s14" value="obsolete" value_original="obsolete" />
      </biological_entity>
      <biological_entity id="o34558" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
      <relation from="o34555" id="r4649" name="across" negation="false" src="d0_s14" to="o34556" />
    </statement>
    <statement id="d0_s15">
      <text>style linear, ± equaling stamens;</text>
      <biological_entity id="o34559" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o34560" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, unlobed or obscurely lobed;</text>
      <biological_entity id="o34561" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel becoming rigid and stiffly spreading in fruit, 8–13 mm.</text>
      <biological_entity id="o34562" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s17" value="rigid" value_original="rigid" />
        <character constraint="in fruit" constraintid="o34563" is_modifier="false" modifier="stiffly" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34563" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat shining;</text>
      <biological_entity id="o34564" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells each with minute, central papilla.</text>
      <biological_entity id="o34565" name="cell" name_original="cells" src="d0_s19" type="structure" />
      <biological_entity constraint="central" id="o34566" name="papilla" name_original="papilla" src="d0_s19" type="structure">
        <character is_modifier="true" name="size" src="d0_s19" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o34565" id="r4650" name="with" negation="false" src="d0_s19" to="o34566" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Umbel fully floriferous, not producing bulbils, ovaries mostly all producing seeds.</description>
      <determination>4a Allium geyeri var. geyeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Umbel not fully floriferous, producing 0–5, mostly sterile flowers, flowers mostly replaced by ovoid, acuminate bulbils.</description>
      <determination>4b Allium geyeri var. tenerum</determination>
    </key_statement>
  </key>
</bio:treatment>