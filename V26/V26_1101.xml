<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">539</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="treatment_page">541</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="Catling &amp; Cruise" date="1974" rank="species">casei</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>76: 527, figs. 1–4. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species casei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101947</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 7–44 cm.</text>
      <biological_entity id="o21790" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="44" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots few–several, descending, slender to somewhat tuberously thickened, mostly to 0.5 cm diam.</text>
      <biological_entity id="o21791" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender to somewhat" value_original="slender to somewhat" />
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="descending" value_original="descending" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender to somewhat" value_original="slender to somewhat" />
        <character is_modifier="false" modifier="somewhat; tuberously" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="mostly" name="diameter" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persisting through anthesis, basal and on proximal portion of stem, ascending to spreading, ovatelanceolate to linearlanceolate to oblanceolate, to 20 × 2 cm.</text>
      <biological_entity id="o21792" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="through anthesis basal leaves" constraintid="o21793" is_modifier="false" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21793" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="through" name="life_cycle" src="d0_s2" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21794" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o21795" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o21792" id="r2973" name="on" negation="false" src="d0_s2" to="o21794" />
      <relation from="o21794" id="r2974" name="part_of" negation="false" src="d0_s2" to="o21795" />
    </statement>
    <statement id="d0_s3">
      <text>Spikes loosely spiraled, usually 5 or more flowers per cycle of spiral;</text>
      <biological_entity id="o21796" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character constraint="per cycle" constraintid="o21797" modifier="usually" name="quantity" src="d0_s3" unit="or moreflowers" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21797" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>rachis moderately to densely pubescent, some trichomes capitate, glands obviously stalked.</text>
      <biological_entity id="o21798" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21799" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o21800" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers ivory to yellowish white or greenish cream, strongly nodding, curved downward throughout lengths, scarcely gaping, urceolate-tubular;</text>
      <biological_entity id="o21801" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="ivory" name="coloration" src="d0_s5" to="yellowish white or greenish cream" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character constraint="throughout lengths" is_modifier="false" name="orientation" src="d0_s5" value="downward" value_original="downward" />
        <character is_modifier="false" modifier="scarcely" name="architecture" src="d0_s5" value="gaping" value_original="gaping" />
        <character is_modifier="false" name="shape" src="d0_s5" value="urceolate-tubular" value_original="urceolate-tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals distinct to base, 5.2–8 mm;</text>
      <biological_entity id="o21802" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="to base" constraintid="o21803" is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21803" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals slightly spreading, apex straight;</text>
      <biological_entity constraint="lateral" id="o21804" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o21805" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals creamy white to greenish white, ovate-elliptic to obovate, 3.9–7.6 mm, apex obtuse;</text>
      <biological_entity id="o21806" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s8" to="greenish white" />
        <character char_type="range_value" from="ovate-elliptic" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s8" to="7.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21807" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip often darker centrally, 4.1–8 × 3.2–5.1 mm, glabrous;</text>
      <biological_entity id="o21808" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often; centrally" name="coloration" src="d0_s9" value="darker" value_original="darker" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s9" to="5.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>veins several, branches parallel;</text>
      <biological_entity id="o21809" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o21810" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>basal calli incurved, prominent, 0.7–1.1 mm;</text>
      <biological_entity constraint="basal" id="o21811" name="callus" name_original="calli" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidia linear to linearlanceolate;</text>
      <biological_entity id="o21812" name="viscidium" name_original="viscidia" src="d0_s12" type="structure">
        <character constraint="to linearlanceolate" constraintid="o21813" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o21813" name="linearlanceolate" name_original="linearlanceolate" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 4–7 mm.</text>
      <biological_entity id="o21814" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds partly or wholly polyembryonic.</text>
      <biological_entity id="o21815" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="wholly" name="architecture" src="d0_s14" value="polyembryonic" value_original="polyembryonic" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e, c North America along Canadian and Unites States border.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="c North America along Canadian and Unites States border" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip ovate to ovate-oblong, apex truncate, margin thin, delicately crisped and fringed.</description>
      <determination>15a Spiranthes casei var. casei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip ovate, apex acute, margin thick and inflexed.</description>
      <determination>15b Spiranthes casei var. novaescotiae</determination>
    </key_statement>
  </key>
</bio:treatment>