<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1812" rank="genus">hymenocallis</taxon_name>
    <taxon_name authority="Gerald L. Smith &amp; Darst" date="1994" rank="species">godfreyi</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>4: 396. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hymenocallis;species godfreyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101677</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb basally rhizomatous, ovoid, 3–4.5 × 2.5–3.5 cm;</text>
      <biological_entity id="o9177" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s0" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal plate 1.5–2 cm;</text>
      <biological_entity constraint="basal" id="o9178" name="plate" name_original="plate" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>neck 2–3 cm;</text>
      <biological_entity id="o9179" name="neck" name_original="neck" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tunic grayish brown.</text>
      <biological_entity id="o9180" name="tunic" name_original="tunic" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish brown" value_original="grayish brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, 3–6, nearly erect, (1.3–) 2–3.8 dm × 1–2.5 cm, coriaceous;</text>
      <biological_entity id="o9181" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.3" from_unit="dm" name="atypical_length" src="d0_s4" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s4" to="3.8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade yellowish green, liguliform, channeled proximally, apex obtuse to subacute.</text>
      <biological_entity id="o9182" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="liguliform" value_original="liguliform" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity id="o9183" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 2–3 dm, 2-edged, glaucous;</text>
      <biological_entity id="o9184" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s6" to="3" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-edged" value_original="2-edged" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scape bracts 2, enclosing buds, 4–5 × 1–1.5 cm;</text>
      <biological_entity constraint="scape" id="o9185" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9186" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <relation from="o9185" id="r1316" name="enclosing" negation="false" src="d0_s7" to="o9186" />
    </statement>
    <statement id="d0_s8">
      <text>subtending floral bracts 3.5–5 cm × 5–10 mm.</text>
      <biological_entity constraint="subtending floral" id="o9187" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 2, one opening slightly before the other, fragrant;</text>
      <biological_entity id="o9188" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" modifier="before the other" name="odor" src="d0_s9" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth-tube green, 4.5–7.5 (–8.5) cm;</text>
      <biological_entity id="o9189" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s10" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="distance" src="d0_s10" to="7.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals slightly ascending, white, distal green-striped on keel, 7–10.5 cm × 5–8 mm;</text>
      <biological_entity id="o9190" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9191" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character constraint="on keel" constraintid="o9192" is_modifier="false" name="coloration" src="d0_s11" value="green-striped" value_original="green-striped" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" notes="" src="d0_s11" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9192" name="keel" name_original="keel" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>corona white with small, yellowish green eye, broadly funnelform, shortly tubulose proximally, 3–4.5 × 5 cm, margins between free portions of filaments erect, with 2 prominent lacerate projections;</text>
      <biological_entity id="o9193" name="corona" name_original="corona" src="d0_s12" type="structure">
        <character constraint="with eye" constraintid="o9194" is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly; shortly; proximally" name="architecture_or_shape" notes="" src="d0_s12" value="tubulose" value_original="tubulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s12" to="4.5" to_unit="cm" />
        <character name="width" src="d0_s12" unit="cm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o9194" name="eye" name_original="eye" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity constraint="between portions" constraintid="o9196" id="o9195" name="margin" name_original="margins" src="d0_s12" type="structure" constraint_original="between  portions, ">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o9196" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o9197" name="filament" name_original="filaments" src="d0_s12" type="structure" />
      <biological_entity id="o9198" name="projection" name_original="projections" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="shape" src="d0_s12" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o9195" id="r1317" name="part_of" negation="false" src="d0_s12" to="o9197" />
      <relation from="o9195" id="r1318" name="with" negation="false" src="d0_s12" to="o9198" />
    </statement>
    <statement id="d0_s13">
      <text>free portions of filaments inserted on flat depression, slightly incurved, white, 1.5–3 cm;</text>
      <biological_entity id="o9199" name="portion" name_original="portions" src="d0_s13" type="structure" constraint="filament" constraint_original="filament; filament">
        <character is_modifier="true" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s13" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s13" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9200" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o9201" name="depression" name_original="depression" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o9199" id="r1319" name="part_of" negation="false" src="d0_s13" to="o9200" />
      <relation from="o9199" id="r1320" name="inserted on" negation="false" src="d0_s13" to="o9201" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.2–1.5 cm, pollen golden;</text>
      <biological_entity id="o9202" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s14" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9203" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="golden" value_original="golden" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary oblong to slightly pyriform, 1.5–2 cm × 10 mm, ovules 4–6 per locule;</text>
      <biological_entity id="o9204" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="slightly pyriform" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s15" to="2" to_unit="cm" />
        <character name="width" src="d0_s15" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o9205" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o9206" from="4" name="quantity" src="d0_s15" to="6" />
      </biological_entity>
      <biological_entity id="o9206" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style green in distal 1/2, fading to white proximally, 13–17 cm.</text>
      <biological_entity id="o9207" name="style" name_original="style" src="d0_s16" type="structure">
        <character constraint="in distal 1/2" constraintid="o9208" is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="fading" modifier="proximally" name="coloration" notes="" src="d0_s16" to="white" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s16" to="17" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9208" name="1/2" name_original="1/2" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules subglobose, ca. 3.5 × 2.5 cm.</text>
      <biological_entity id="o9209" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="subglobose" value_original="subglobose" />
        <character name="length" src="d0_s17" unit="cm" value="3.5" value_original="3.5" />
        <character name="width" src="d0_s17" unit="cm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds obovoid, 2–2.5 × 1.3–1.8 cm. 2n = 48.</text>
      <biological_entity id="o9210" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s18" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s18" to="1.8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9211" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–mid spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brackish marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brackish marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Godfrey’s spider-lily</other_name>
  <other_name type="common_name">St. Mark’s marsh spider-lily</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Hymenocallis godfreyi is endemic to the marsh area near Ft. San Marcos de Apalache along the St. Mark’s River. It is somewhat abundant locally, but because of its highly restricted range it should be recommended for conservation. G. L. Smith and M. Darst (1994) have proposed a relationship to H. rotata, from which it likely evolved. It is distinguished from that species by its short scape, short, erect coriaceous, yellowish green leaves, broadly funnelform staminal corona with prominent lacerate projections, and unique basal rhizome.</discussion>
  
</bio:treatment>