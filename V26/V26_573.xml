<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="mention_page">299</other_info_on_meta>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="(Linnaeus) Herbert" date="unknown" rank="species">atamasca</taxon_name>
    <place_of_publication>
      <publication_title>Appendix,</publication_title>
      <place_in_publication>36. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species atamasca</taxon_hierarchy>
    <other_info_on_name type="fna_id">220014476</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amaryllis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">atamasca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 292. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amaryllis;species atamasca;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atamosco</taxon_name>
    <taxon_name authority="(Linnaeus) Greene (as atamasco)" date="unknown" rank="species">atamasca</taxon_name>
    <taxon_hierarchy>genus Atamosco;species atamasca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade shiny green, to 8 mm wide.</text>
      <biological_entity id="o14218" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe (2–) 2.2–3.2 (–3.6) cm.</text>
      <biological_entity id="o14219" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3.6" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="some_measurement" src="d0_s1" to="3.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect to slightly inclined;</text>
      <biological_entity id="o14220" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="slightly inclined" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth mostly white, sometimes tinged or veined pink, more so with age, funnelform, (5.5–) 6.6–9 (–10) cm;</text>
      <biological_entity id="o14221" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="tinged" value_original="tinged" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="veined" value_original="veined" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="6.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="6.6" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14222" name="age" name_original="age" src="d0_s3" type="structure" />
      <relation from="o14221" id="r1974" name="with" negation="false" src="d0_s3" to="o14222" />
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube green, (0.8–) 1–2 (–2.1) cm, increasing in diam., less than 1/4 perianth length, ca. 1/2 (1/3–2/3) filament length, ca. 1/2 (1/3–3/4) spathe length;</text>
      <biological_entity id="o14223" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_distance" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s4" to="2.1" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14224" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character is_modifier="true" name="character" src="d0_s4" value="diam" value_original="diam" />
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s4" to="1/4" />
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s4" to="2/3" />
      </biological_entity>
      <biological_entity id="o14225" name="filament" name_original="filament" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="length" value_original="length" />
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o14226" name="spathe" name_original="spathe" src="d0_s4" type="structure" />
      <relation from="o14223" id="r1975" name="increasing in" negation="false" src="d0_s4" to="o14224" />
    </statement>
    <statement id="d0_s5">
      <text>tepals usually reflexed;</text>
      <biological_entity id="o14227" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens diverging, appearing equal;</text>
      <biological_entity id="o14228" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments filiform, (2.9–) 3.2–4.4 (–4.7) cm;</text>
      <biological_entity id="o14229" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="some_measurement" src="d0_s7" to="4.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3–6 (–8) mm;</text>
      <biological_entity id="o14230" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style longer than perianth-tube;</text>
      <biological_entity id="o14231" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o14232" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14232" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma 3-fid, exserted more than 2 mm beyond anthers;</text>
      <biological_entity id="o14233" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" constraint="beyond anthers" constraintid="o14234" from="2" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14234" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel (0–) 0.4–1.6 (–3.4) cm, usually shorter than spathe.</text>
      <biological_entity id="o14236" name="spathe" name_original="spathe" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o14235" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="3.4" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s11" to="1.6" to_unit="cm" />
        <character constraint="than spathe" constraintid="o14236" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14237" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid winter–spring (Jan–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="mid winter" />
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, mixed forests, moist clearings, meadows, moist to wet pastures, coastal plains, piedmonts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed forests" modifier="rich" />
        <character name="habitat" value="moist clearings" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="wet pastures" />
        <character name="habitat" value="coastal plains" />
        <character name="habitat" value="moist to wet pastures" />
        <character name="habitat" value="piedmonts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Md., Miss., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">atamasco</other_name>
  <other_name type="common_name">Atamasco-lily</other_name>
  <other_name type="common_name">Carolina-lily</other_name>
  <other_name type="common_name">Easter-lily</other_name>
  <other_name type="common_name">naked-lady</other_name>
  <other_name type="common_name">occidental swamp-lily</other_name>
  <other_name type="common_name">Virginia-lily</other_name>
  
</bio:treatment>