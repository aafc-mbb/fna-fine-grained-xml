<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Maratti" date="unknown" rank="genus">romulea</taxon_name>
    <taxon_name authority="(Linnaeus) Ecklon" date="1827" rank="species">rosea</taxon_name>
    <place_of_publication>
      <publication_title>Topogr. Verz. Pflanzensamml. Ecklon,</publication_title>
      <place_in_publication>19. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus romulea;species rosea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101876</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ixia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rosea</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>12, 2: 75. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ixia;species rosea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–12 (–30) cm.</text>
      <biological_entity id="o20048" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corm tunicate, 5–15 mm diam.;</text>
      <biological_entity id="o20049" name="corm" name_original="corm" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tunic hard, woody, splitting above and below into acuminate segments, those below bent backward.</text>
      <biological_entity id="o20050" name="tunic" name_original="tunic" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s2" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="below" name="shape" notes="" src="d0_s2" value="bent" value_original="bent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="backward" value_original="backward" />
      </biological_entity>
      <biological_entity id="o20051" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o20050" id="r2754" modifier="below" name="into" negation="false" src="d0_s2" to="o20051" />
    </statement>
    <statement id="d0_s3">
      <text>Stems usually branched below-ground level;</text>
      <biological_entity id="o20052" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="location" src="d0_s3" value="below-ground" value_original="below-ground" />
        <character is_modifier="false" name="position_relational" src="d0_s3" value="level" value_original="level" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches (peduncles) aerial, suberect, becoming falcate, ultimately erect.</text>
      <biological_entity id="o20053" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="location" src="d0_s4" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="ultimately" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves (2–) 6–9, much exceeding stem;</text>
      <biological_entity id="o20054" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o20055" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o20054" id="r2755" modifier="much" name="exceeding" negation="false" src="d0_s5" to="o20055" />
    </statement>
    <statement id="d0_s6">
      <text>blade 10–30 cm × ca. 1.5 mm.</text>
      <biological_entity id="o20056" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spathes green or flushed with purple;</text>
      <biological_entity id="o20057" name="spathe" name_original="spathes" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="flushed with purple" value_original="flushed with purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>outer 12–25 mm, margin narrow, membranous;</text>
      <biological_entity constraint="outer" id="o20058" name="spathe" name_original="spathe" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20059" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner margin brown-streaked, broad, membranous.</text>
      <biological_entity constraint="inner" id="o20060" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown-streaked" value_original="brown-streaked" />
        <character is_modifier="false" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Tepals pink to purple, usually pale-yellow in cup, outer pale abaxially, main veins dark green to purple, lanceolate, 13–22 × 3–4 mm;</text>
      <biological_entity id="o20061" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="purple" />
        <character constraint="in cup" constraintid="o20062" is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o20062" name="cup" name_original="cup" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o20063" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity constraint="main" id="o20064" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s10" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth-tube 2–3.5 mm;</text>
      <biological_entity id="o20065" name="perianth-tube" name_original="perianth-tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments ca. 5 mm;</text>
      <biological_entity id="o20066" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers ca. 4 mm;</text>
      <biological_entity id="o20067" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style branching opposite upper 1/3 anthers;</text>
      <biological_entity id="o20068" name="style" name_original="style" src="d0_s14" type="structure">
        <character constraint="opposite anthers" constraintid="o20069" is_modifier="false" name="architecture" src="d0_s14" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o20069" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="upper" value_original="upper" />
        <character is_modifier="true" name="quantity" src="d0_s14" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>branches ca. 2 mm, shorter than anther apices.</text>
      <biological_entity id="o20070" name="branch" name_original="branches" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="2" value_original="2" />
        <character constraint="than anther apices" constraintid="o20071" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="anther" id="o20071" name="apex" name_original="apices" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules 10–15 mm.</text>
      <biological_entity id="o20072" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ca. 2 mm diam.</text>
      <biological_entity id="o20073" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character name="diameter" src="d0_s17" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mainly Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mainly" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abandoned dwellings, along paths, in meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dwellings" modifier="abandoned" constraint="along paths" />
        <character name="habitat" value="paths" modifier="along" />
        <character name="habitat" value="meadows" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>M. P. de Vos (1972) recognized five varieties of Romulea rosea; the plants naturalized in North America correspond to var. australis (Ewart) M. P. de Vos, which has become a common weed of lawns, pathways, and roadsides in Australia, where it is a pest. To date, weediness does not seem to be the case in the parts of California where the species occurs.</discussion>
  
</bio:treatment>