<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="treatment_page">603</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="1794" rank="genus">bletia</taxon_name>
    <taxon_name authority="Graham" date="1836" rank="species">patula</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>21: 155. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus bletia;species patula;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101428</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 100 cm.</text>
      <biological_entity id="o8408" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots numerous, 1.5–3 mm diam.</text>
      <biological_entity id="o8409" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems pseudobulbs, ovoid to subglobose, 2–5 cm diam.</text>
      <biological_entity constraint="stems" id="o8410" name="pseudobulb" name_original="pseudobulbs" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s2" to="subglobose" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade narrowly elliptic to narrowly oblanceolate, 25–75 × 1.5–4 cm, apex acuminate.</text>
      <biological_entity id="o8411" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8412" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s3" to="75" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8413" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 39–118 cm;</text>
      <biological_entity id="o8414" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="39" from_unit="cm" name="some_measurement" src="d0_s4" to="118" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>floral bracts ovate-triangular to lanceolate, 1–3 mm, apex acute to acuminate.</text>
      <biological_entity constraint="floral" id="o8415" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate-triangular" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8416" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5–22, pink to rosy purple, rarely entirely white;</text>
      <biological_entity id="o8417" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="22" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s6" to="rosy purple" />
        <character is_modifier="false" modifier="rarely entirely" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>dorsal sepal elliptic-oblanceolate, 30–40 × 6–8 mm, apex acute to acuminate;</text>
      <biological_entity constraint="dorsal" id="o8418" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8419" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals similar but falcate;</text>
      <biological_entity constraint="lateral" id="o8420" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals erect, oblongelliptic, 30–35 × 5–6 mm, thinner than sepals, apex obtuse;</text>
      <biological_entity id="o8421" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
        <character constraint="than sepals" constraintid="o8422" is_modifier="false" name="width" src="d0_s9" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o8422" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o8423" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip adnate to base of column, clawed, 3-lobed, 30–35 × 27–31 mm, middle lobe subquadrangular, flabellate, emarginate, margins crispate-undulate, lateral lobes erect, flanking column, rounded to subquadrangular, broadest distally;</text>
      <biological_entity id="o8424" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character constraint="to base" constraintid="o8425" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s10" to="35" to_unit="mm" />
        <character char_type="range_value" from="27" from_unit="mm" name="width" src="d0_s10" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8425" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o8426" name="column" name_original="column" src="d0_s10" type="structure" />
      <biological_entity constraint="middle" id="o8427" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subquadrangular" value_original="subquadrangular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o8428" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="crispate-undulate" value_original="crispate-undulate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8429" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="subquadrangular" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s10" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o8430" name="column" name_original="column" src="d0_s10" type="structure" />
      <relation from="o8425" id="r1200" name="part_of" negation="false" src="d0_s10" to="o8426" />
      <relation from="o8429" id="r1201" name="flanking" negation="false" src="d0_s10" to="o8430" />
    </statement>
    <statement id="d0_s11">
      <text>disc lamellae 5, middle 3 white;</text>
      <biological_entity constraint="disc" id="o8431" name="lamella" name_original="lamellae" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s11" value="middle" value_original="middle" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>column clavate, 23–25 mm, column-foot absent;</text>
      <biological_entity id="o8432" name="column" name_original="column" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8433" name="column-foot" name_original="column-foot" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicellate ovary slender, 20–35 mm.</text>
      <biological_entity id="o8434" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules pendulous, 3–4.5 cm × 8–10 mm.</text>
      <biological_entity id="o8435" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="pendulous" value_original="pendulous" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s14" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Haitian pine-pink</other_name>
  <discussion>Bletia patula was originally collected from Miami-Dade County, Florida, in 1947. The locality has since become part of the greater Miami area (C. A. Luer 1972), and the persistence of the populations is doubtful.</discussion>
  
</bio:treatment>