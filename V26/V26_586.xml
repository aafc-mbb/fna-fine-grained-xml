<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="(W. Hayward) Moldenke" date="1951" rank="species">traubii</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>7: 79. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species traubii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cooperia</taxon_name>
    <taxon_name authority="W. Hayward" date="unknown" rank="species">traubii</taxon_name>
    <place_of_publication>
      <publication_title>Herbertia</publication_title>
      <place_in_publication>3: 63, 65 (fig. s.n.), 66 (figs. 1–3). 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cooperia;species traubii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade dull green, to 1 mm wide.</text>
      <biological_entity id="o24538" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe (2–) 3–4 cm.</text>
      <biological_entity id="o24539" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect;</text>
      <biological_entity id="o24540" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth white, sometimes tinged and veined pink, salverform, (11–) 12–15.4 cm;</text>
      <biological_entity id="o24541" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="tinged" value_original="tinged" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="veined" value_original="veined" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s3" value="salverform" value_original="salverform" />
        <character char_type="range_value" from="11" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s3" to="15.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube primarily white, 9–12.6 cm, diam. uniform, 3/4 or more perianth length, at least 15 times filament length, 3–5 times spathe length;</text>
      <biological_entity id="o24542" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="primarily" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="cm" name="distance" src="d0_s4" to="12.6" to_unit="cm" />
        <character is_modifier="false" name="diam" src="d0_s4" value="uniform" value_original="uniform" />
        <character name="quantity" src="d0_s4" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o24543" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character constraint="filament" constraintid="o24544" is_modifier="false" modifier="at-least" name="length" src="d0_s4" value="15 times filament length" value_original="15 times filament length" />
        <character constraint="spathe" constraintid="o24545" is_modifier="false" name="length" src="d0_s4" value="3-5 times spathe length" value_original="3-5 times spathe length" />
      </biological_entity>
      <biological_entity id="o24544" name="filament" name_original="filament" src="d0_s4" type="structure" />
      <biological_entity id="o24545" name="spathe" name_original="spathe" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>tepals often reflexed;</text>
      <biological_entity id="o24546" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens fasciculate, appearing equal;</text>
      <biological_entity id="o24547" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments subulate, 0.2–0.4 cm, apex blunt;</text>
      <biological_entity id="o24548" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s7" to="0.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24549" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 4–8 mm;</text>
      <biological_entity id="o24550" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style longer than perianth-tube;</text>
      <biological_entity id="o24551" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o24552" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o24552" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma capitate, exserted more than 2 mm beyond anthers;</text>
      <biological_entity id="o24553" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" constraint="beyond anthers" constraintid="o24554" from="2" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24554" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel absent.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o24555" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24556" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer–mid fall (Jul–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid fall" from="early summer" />
        <character name="flowering time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Primarily sandy loam, open fields, coastal plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loam" modifier="primarily" />
        <character name="habitat" value="open fields" />
        <character name="habitat" value="coastal plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  
</bio:treatment>