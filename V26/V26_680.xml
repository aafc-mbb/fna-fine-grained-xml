<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="S. Watson ex Bentham in G. Bentham and J. D. Hooker" date="1883" rank="genus">muilla</taxon_name>
    <taxon_name authority="Greene" date="1887" rank="species">transmontana</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 73. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus muilla;species transmontana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101782</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bloomeria</taxon_name>
    <taxon_name authority="(Greene) J. F. Macbride" date="unknown" rank="species">transmontana</taxon_name>
    <taxon_hierarchy>genus Bloomeria;species transmontana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1.5–5 dm;</text>
      <biological_entity id="o29615" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>corms 1–2.5 (–3) cm in diam.</text>
      <biological_entity id="o29616" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_diam" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="diam" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–5, 5–30 × 0.05–0.4 cm.</text>
      <biological_entity id="o29617" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" src="d0_s2" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape 8–50 cm.</text>
      <biological_entity id="o29618" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 12–30;</text>
      <biological_entity id="o29619" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s4" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tepals white, often tinged with lilac, mostly lanceolate;</text>
      <biological_entity id="o29620" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="tinged with lilac" value_original="tinged with lilac" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth-tube 1–1.5 mm, lobes 5–8 × 1.5–2 mm;</text>
      <biological_entity id="o29621" name="perianth-tube" name_original="perianth-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29622" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 4–6 mm;</text>
      <biological_entity id="o29623" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments erect, conspicuously dilated basally and connate to form nectariferous cup;</text>
      <biological_entity id="o29624" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="conspicuously; basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="nectariferous" id="o29625" name="cup" name_original="cup" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>anthers yellow, 1–1.5 mm;</text>
      <biological_entity id="o29626" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicel 1–2.5 cm.</text>
      <biological_entity id="o29627" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 8–10 mm.</text>
      <biological_entity id="o29628" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 4–5 mm. 2n = 20.</text>
      <biological_entity id="o29629" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29630" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring and early summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, coniferous woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="coniferous woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Muilla transmontana is found in coniferous forests in mountains of northern California and northwestern Nevada, and infrequently on sagebrush flats of lower elevations. The filaments are dilated and connate at the base to form a nectariferous cup similar to that seen in Bloomeria crocea.</discussion>
  
</bio:treatment>