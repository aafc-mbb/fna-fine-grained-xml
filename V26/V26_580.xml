<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="Hemsley" date="1880" rank="species">longifolia</taxon_name>
    <place_of_publication>
      <publication_title>Diagn. Pl. Nov. Mexic.,</publication_title>
      <place_in_publication>55. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species longifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102085</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade dull green, to 1 (–2) mm wide.</text>
      <biological_entity id="o28317" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="width" src="d0_s0" to="2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe 2.1–3.1 cm.</text>
      <biological_entity id="o28318" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.1" from_unit="cm" name="some_measurement" src="d0_s1" to="3.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect to slightly inclined;</text>
      <biological_entity id="o28319" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="slightly inclined" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth yellow, funnelform, 2.1–2.8 cm;</text>
      <biological_entity id="o28320" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube green, 0.2–0.6 cm, increasing in diam., no more than 1/4 perianth length, ca. 1/2 (1/4–3/4) filament length, no more than 1/4 spathe length;</text>
      <biological_entity id="o28321" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="distance" src="d0_s4" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="no" value_original="no" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" value="/4" value_original="/4" />
      </biological_entity>
      <biological_entity id="o28322" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o28323" name="filament" name_original="filament" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="length" value_original="length" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="no" value_original="no" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" value="/4" value_original="/4" />
      </biological_entity>
      <biological_entity id="o28324" name="spathe" name_original="spathe" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>tepals rarely reflexed;</text>
      <biological_entity id="o28325" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens diverging, in 2 distinctly unequal sets;</text>
      <biological_entity id="o28326" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o28327" name="set" name_original="sets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" modifier="distinctly" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o28326" id="r3830" name="in" negation="false" src="d0_s6" to="o28327" />
    </statement>
    <statement id="d0_s7">
      <text>filaments filiform, 0.7–1.1 cm;</text>
      <biological_entity id="o28328" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s7" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers in 2 nonoverlapping sets, 3–6 mm;</text>
      <biological_entity id="o28329" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28330" name="set" name_original="sets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="nonoverlapping" value_original="nonoverlapping" />
      </biological_entity>
      <relation from="o28329" id="r3831" name="in" negation="false" src="d0_s8" to="o28330" />
    </statement>
    <statement id="d0_s9">
      <text>style longer than perianth-tube;</text>
      <biological_entity id="o28331" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o28332" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o28332" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma 3-fid, usually among anthers;</text>
      <biological_entity id="o28333" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
      </biological_entity>
      <biological_entity id="o28334" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o28333" id="r3832" modifier="usually" name="among" negation="false" src="d0_s10" to="o28334" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel 0.7–1.5 cm, shorter than spathe.</text>
      <biological_entity id="o28336" name="spathe" name_original="spathe" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 48.</text>
      <biological_entity id="o28335" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
        <character constraint="than spathe" constraintid="o28336" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28337" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly, calcareous, alkaline soils in highlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="sandy" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="alkaline soils" constraint="in highlands" />
        <character name="habitat" value="highlands" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Cebolleta</other_name>
  
</bio:treatment>