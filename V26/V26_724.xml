<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="treatment_page">358</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1899" rank="species">rosulatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 228. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species rosulatum</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200028231</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">brownei</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species brownei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">exile</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species exile;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or short-lived perennial, cespitose, yellowish green to medium or dark olive green when dry, to 3.6 dm, not glaucous.</text>
      <biological_entity id="o33246" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="size" src="d0_s0" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark olive" value_original="dark olive" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="3.6" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched or rarely simple, with 1–2 (–3) nodes, 0.7–2 mm wide, glabrous, margins usually entire, similar in color and texture to stem body;</text>
      <biological_entity id="o33247" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" notes="" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33248" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="2" />
      </biological_entity>
      <biological_entity constraint="stem" id="o33250" name="body" name_original="body" src="d0_s1" type="structure" />
      <relation from="o33247" id="r4482" name="with" negation="false" src="d0_s1" to="o33248" />
      <relation from="o33249" id="r4483" name="to" negation="false" src="d0_s1" to="o33250" />
    </statement>
    <statement id="d0_s2">
      <text>first internode 2–10 cm, usually shorter than leaves;</text>
      <biological_entity id="o33249" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o33251" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
        <character constraint="than leaves" constraintid="o33252" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o33252" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>distalmost node with 1–2 branches.</text>
      <biological_entity constraint="distalmost" id="o33253" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o33254" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <relation from="o33253" id="r4484" name="with" negation="false" src="d0_s3" to="o33254" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o33255" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33256" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o33257" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o33257" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o33258" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green, obviously wider than supporting branch, glabrous, keels usually entire;</text>
      <biological_entity id="o33259" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than supporting branch , glabrous , keels" constraintid="o33260, o33261" is_modifier="false" name="width" src="d0_s6" value="obviously wider" value_original="obviously wider" />
      </biological_entity>
      <biological_entity id="o33260" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o33261" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer 13–32.2 mm, 2–8.2 mm longer than inner, tapering evenly towards apex or occasionally slightly constricted proximal to apex, margins basally connate 2.3–6.7 mm;</text>
      <biological_entity constraint="outer" id="o33262" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="32.2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="8.2" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o33263" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards " constraintid="o33265" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o33263" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o33264" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o33265" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="occasionally slightly" name="size" src="d0_s7" value="constricted" value_original="constricted" />
        <character is_modifier="true" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o33266" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s7" to="6.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved to straight, hyaline margins 0.2–0.3 mm wide, apex acute, occasionally erose, ending 0.3–4.6 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o33267" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o33268" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character char_type="range_value" from="evenly curved" name="course" src="d0_s8" to="straight" />
      </biological_entity>
      <biological_entity id="o33269" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33270" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o33271" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <relation from="o33267" id="r4485" name="with" negation="false" src="d0_s8" to="o33268" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth flaring distally, campanulate basally;</text>
      <biological_entity id="o33272" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33273" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="flaring" value_original="flaring" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals maroon or pink to lavender-rose with purple stripes, or yellow with rosy purple bases;</text>
      <biological_entity id="o33274" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33275" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="with purple stripes" from="pink" name="coloration" src="d0_s10" to="lavender-rose" />
        <character constraint="with bases" constraintid="o33276" is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o33276" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="rosy purple" value_original="rosy purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer tepals 5–11 mm, apex acute, rarely aristate;</text>
      <biological_entity id="o33277" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o33278" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33279" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments connate basally, occasionally to 1/2 their length, basally inflated and stipitate-glandular 0.5–0.8 mm;</text>
      <biological_entity id="o33280" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o33281" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" modifier="occasionally" name="length" src="d0_s12" to="1/2" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o33282" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33283" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
      <biological_entity id="o33284" name="foliage" name_original="foliage" src="d0_s13" type="structure" />
      <relation from="o33283" id="r4486" name="to" negation="false" src="d0_s13" to="o33284" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules tan with purplish sutures and sometimes apex, ± globose, 2.1–4.2 mm;</text>
      <biological_entity id="o33285" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character constraint="with sutures" constraintid="o33286" is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s14" value="globose" value_original="globose" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s14" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33286" name="suture" name_original="sutures" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o33287" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel spreading to recurved.</text>
      <biological_entity id="o33288" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s15" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds ± globose, sometimes with slight depression, 0.5–1 mm, rugulose.</text>
      <biological_entity id="o33290" name="depression" name_original="depression" src="d0_s16" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s16" value="slight" value_original="slight" />
      </biological_entity>
      <relation from="o33289" id="r4487" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o33290" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 32.</text>
      <biological_entity id="o33289" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33291" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, old fields, and other disturbed areas, stream banks, wet areas bordering woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="other disturbed areas" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="wet areas" />
        <character name="habitat" value="bordering woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–80 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="80" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Sisyrinchium rosulatum is apparently weedy throughout much of its range.</discussion>
  
</bio:treatment>