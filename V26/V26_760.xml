<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1899" rank="species">septentrionale</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 452. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species septentrionale</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101922</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, green to pale olive when dry, to 4.3 dm, not glaucous;</text>
      <biological_entity id="o9506" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="pale olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="4.3" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes scarcely discernable.</text>
      <biological_entity id="o9507" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely" name="prominence" src="d0_s1" value="discernable" value_original="discernable" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, 0.8–2 mm wide, glabrous, margins usually entire to denticulate apically, similar in color and texture to stem body.</text>
      <biological_entity id="o9508" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9509" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually entire" modifier="apically" name="shape" src="d0_s2" to="denticulate" />
      </biological_entity>
      <biological_entity constraint="stem" id="o9510" name="body" name_original="body" src="d0_s2" type="structure" />
      <relation from="o9509" id="r1355" name="to" negation="false" src="d0_s2" to="o9510" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o9511" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9512" name="base" name_original="bases" src="d0_s3" type="structure">
        <character constraint="in tufts" constraintid="o9513" is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o9513" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o9514" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes usually green, glabrous, keels entire to slightly denticulate;</text>
      <biological_entity id="o9515" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9516" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="slightly denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer 20–63 mm, 17–42 mm longer than inner, tapering evenly towards apex, basally connate 1.5–2.5 mm;</text>
      <biological_entity constraint="outer" id="o9517" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="63" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s6" to="42" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o9518" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o9519" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9518" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o9519" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner with keel evenly curved, hyaline margins 0.1–0.3 mm wide, apex acute to acuminate, ending 0.4–2.3 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o9520" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o9521" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o9522" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9523" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o9524" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o9520" id="r1356" name="with" negation="false" src="d0_s7" to="o9521" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals pale blue to light bluish violet, rarely white, bases yellow;</text>
      <biological_entity id="o9525" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9526" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s8" to="light bluish violet" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o9527" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer 8–9.1 mm, apex usually rounded, aristate;</text>
      <biological_entity id="o9528" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="outer" value_original="outer" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="9.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9529" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o9530" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9531" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o9532" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9533" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o9534" name="foliage" name_original="foliage" src="d0_s11" type="structure" />
      <relation from="o9533" id="r1357" name="to" negation="false" src="d0_s11" to="o9534" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules beige to light-brown, ± globose, 3–5 mm;</text>
      <biological_entity id="o9535" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="beige" name="coloration" src="d0_s12" to="light-brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicel spreading to erect.</text>
      <biological_entity id="o9536" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose to obconic, lacking obvious depression, 0.5–1.2 mm, rugulose.</text>
      <biological_entity id="o9537" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 32.</text>
      <biological_entity id="o9538" name="depression" name_original="depression" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9539" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic to dry meadows, stream banks, often in gravelly soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic to dry meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="gravelly soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Sask.; Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <discussion>Sisyrinchium septentrionale is widespread but apparently not common in western Canada. In central Canada it intergrades with S. mucronatum, to which it appears closely related (see discussion, p. 367). It is confused also with S. montanum but can be distinguished by its very slender, very long outer spathe and nongibbous inner spathe. Fresh material will show lighter blue flowers and outer tepals with rounded apices.</discussion>
  
</bio:treatment>