<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">628</other_info_on_meta>
    <other_info_on_meta type="treatment_page">632</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="(Nuttall) Torrey" date="1818" rank="genus">aplectrum</taxon_name>
    <taxon_name authority="(Muhlenburg ex Willdenow) Torrey" date="1826" rank="species">hyemale</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 198. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus aplectrum;species hyemale;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220000960</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cymbidium</taxon_name>
    <taxon_name authority="Muhlenburg ex Willdenow" date="unknown" rank="species">hyemale</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4(1): 107. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cymbidium;species hyemale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 18–50 cm.</text>
      <biological_entity id="o672" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent, petiolate;</text>
      <biological_entity id="o673" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade narrowly to broadly elliptic, prominently and irregularly plicate, 10–20 × 3–8 cm, abaxially usually maroon or greenish purple, adaxially dark green with whitish veins.</text>
      <biological_entity id="o674" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="prominently; irregularly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially usually" name="coloration" src="d0_s2" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish purple" value_original="greenish purple" />
        <character constraint="with veins" constraintid="o675" is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="dark green" value_original="dark green" />
      </biological_entity>
      <biological_entity id="o675" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: floral bracts inconspicuous, 3–7 × 1.5 mm, apex subulate to acuminate.</text>
      <biological_entity id="o676" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="floral" id="o677" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o678" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers prominent but inconspicuously colored;</text>
      <biological_entity id="o679" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="inconspicuously" name="coloration" src="d0_s4" value="colored" value_original="colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals spreading, greenish or yellowish suffused with magenta or purple-brown, oblong-oblanceolate to narrowly elliptic, 10–15 × 1.8–4 mm;</text>
      <biological_entity id="o680" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish suffused with magenta or yellowish suffused with purple-brown" />
        <character name="coloration" src="d0_s5" value="," value_original="," />
        <character char_type="range_value" from="oblong-oblanceolate" name="shape" src="d0_s5" to="narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals parallel to column, greenish or yellowish, suffused with magenta or purple-brown, oblong-oblanceolate, 9–13 × 1.8–3.5 mm;</text>
      <biological_entity id="o681" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character constraint="to column" constraintid="o682" is_modifier="false" name="arrangement" src="d0_s6" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="suffused with magenta or suffused with purple-brown" />
        <character name="coloration" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o682" name="column" name_original="column" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lip white with magenta spots, obovate in outline, 9–12 × 7–9 mm, apex 3-lobed, lateral lobes ascending and flanking column, middle lobe broadly expanded, crenate;</text>
      <biological_entity id="o683" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character constraint="with column" constraintid="o684" is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o684" name="column" name_original="column" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="broadly" name="size" src="d0_s7" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>column 7 × 2 mm.</text>
      <biological_entity id="o685" name="column" name_original="column" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="mm" value="7" value_original="7" />
        <character name="width" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules ellipsoid, strongly ribbed, 15–30 × 7–12 mm.</text>
      <biological_entity id="o686" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, deciduous, upland to swampy forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="upland to swampy forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., Md., Mass., Mich., Minn., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>