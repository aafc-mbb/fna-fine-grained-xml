<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lilium</taxon_name>
    <taxon_name authority="Kellogg" date="1859" rank="species">pardalinum</taxon_name>
    <taxon_name authority="(Eastwood) M. W. Skinner" date="2002" rank="subspecies">vollmeri</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 257. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lilium;species pardalinum;subspecies vollmeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102263</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">vollmeri</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>5: 121. 1948</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lilium;species vollmeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs branching less often and less regularly than subsp.</text>
    </statement>
    <statement id="d0_s1">
      <text>pardalinum, 1.4–3.6 × 4.6–12.8 cm, 0.2–0.5 times taller than long;</text>
      <biological_entity id="o27401" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="less often" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s1" to="3.6" to_unit="cm" />
        <character char_type="range_value" from="4.6" from_unit="cm" name="width" src="d0_s1" to="12.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales 1–2-segmented, longest 1–2.6 cm.</text>
      <biological_entity id="o27402" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="1-2-segmented" value_original="1-2-segmented" />
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems to 1.7 m, weakly clonal and not forming large colonies.</text>
      <biological_entity id="o27403" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s3" to="1.7" to_unit="m" />
        <character is_modifier="false" modifier="weakly" name="growth_form" src="d0_s3" value="clonal" value_original="clonal" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s3" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves often concentrated proximally, scattered, especially in small plants, or in 1–6 whorls or partial whorls, 3–15 leaves per whorl, often ascending, sometimes horizontal and drooping at the tips, 4.9–26.5 × 0.3–2.4 cm, 7.3–34 times longer than wide;</text>
      <biological_entity id="o27404" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; proximally" name="arrangement_or_density" src="d0_s4" value="concentrated" value_original="concentrated" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="partial" value_original="partial" />
      </biological_entity>
      <biological_entity id="o27405" name="plant" name_original="plants" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o27407" name="whorl" name_original="whorls" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o27408" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" notes="" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="horizontal" value_original="horizontal" />
        <character constraint="at tips" constraintid="o27410" is_modifier="false" name="orientation" src="d0_s4" value="drooping" value_original="drooping" />
        <character char_type="range_value" from="4.9" from_unit="cm" name="length" notes="" src="d0_s4" to="26.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" notes="" src="d0_s4" to="2.4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="7.3-34" value_original="7.3-34" />
      </biological_entity>
      <biological_entity id="o27409" name="whorl" name_original="whorl" src="d0_s4" type="structure" />
      <biological_entity id="o27410" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <relation from="o27404" id="r3718" modifier="especially" name="in" negation="false" src="d0_s4" to="o27405" />
      <relation from="o27404" id="r3719" name="in" negation="false" src="d0_s4" to="o27407" />
      <relation from="o27408" id="r3720" name="per" negation="false" src="d0_s4" to="o27409" />
    </statement>
    <statement id="d0_s5">
      <text>blade linear to narrowly elliptic, sometimes lance-linear, especially in distal leaves, or weakly oblanceolate, especially in proximal leaves, margins not undulate.</text>
      <biological_entity id="o27411" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s5" to="narrowly elliptic" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" modifier="weakly" name="shape" notes="" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27412" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o27413" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o27414" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
      <relation from="o27411" id="r3721" modifier="especially" name="in" negation="false" src="d0_s5" to="o27412" />
      <relation from="o27411" id="r3722" modifier="especially" name="in" negation="false" src="d0_s5" to="o27413" />
    </statement>
    <statement id="d0_s6">
      <text>Racemes 1–13-flowered.</text>
      <biological_entity id="o27415" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-13-flowered" value_original="1-13-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers not fragrant;</text>
      <biological_entity id="o27416" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s7" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals and petals reflexed 1/4–1/3 along length from base, sometimes uniformly orange, usually yellow-orange or orange proximally, darker red-orange to red or crimson on distal 2/5–3/5;</text>
      <biological_entity id="o27417" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="sometimes uniformly" name="coloration" notes="" src="d0_s8" value="orange" value_original="orange" />
        <character char_type="range_value" constraint="on distal 2/5-3/5" constraintid="o27420" from="darker red-orange" name="coloration" src="d0_s8" to="red or crimson" />
      </biological_entity>
      <biological_entity id="o27418" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="from base" constraintid="o27419" from="1/4" name="length" src="d0_s8" to="1/3" />
        <character is_modifier="false" modifier="sometimes uniformly" name="coloration" notes="" src="d0_s8" value="orange" value_original="orange" />
        <character char_type="range_value" constraint="on distal 2/5-3/5" constraintid="o27420" from="darker red-orange" name="coloration" src="d0_s8" to="red or crimson" />
      </biological_entity>
      <biological_entity id="o27419" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o27420" name="2/5-3/5" name_original="2/5-3/5" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals (4.9–) 5.3–8.3 × 1–2.2 cm;</text>
      <biological_entity id="o27421" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.9" from_unit="cm" name="atypical_length" src="d0_s9" to="5.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.3" from_unit="cm" name="length" src="d0_s9" to="8.3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s9" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4.8–8 × 1–2.1 cm;</text>
      <biological_entity id="o27422" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.8" from_unit="cm" name="length" src="d0_s10" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s10" to="2.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens moderately exserted;</text>
      <biological_entity id="o27423" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments widely spreading, diverging 12°–22° from axis;</text>
      <biological_entity id="o27424" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character constraint="from axis" constraintid="o27425" is_modifier="false" modifier="12°-22°" name="orientation" src="d0_s12" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o27425" name="axis" name_original="axis" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers magenta or purple, 0.6–1.8 cm;</text>
      <biological_entity id="o27426" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s13" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen dark orange, sometimes rust-orange;</text>
      <biological_entity id="o27427" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark orange" value_original="dark orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="rust-orange" value_original="rust-orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistil 3.5–5.3 cm;</text>
      <biological_entity id="o27428" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s15" to="5.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary 1.4–2.2 cm;</text>
      <biological_entity id="o27429" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s16" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 9–32 cm.</text>
      <biological_entity id="o27430" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s17" to="32" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules 2.5–4.8 × 1.2–2 cm, 1.5–3.2 times longer than wide.</text>
      <biological_entity id="o27431" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s18" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s18" to="2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s18" value="1.5-3.2" value_original="1.5-3.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 132–207.2n = 24.</text>
      <biological_entity id="o27432" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="132" name="quantity" src="d0_s19" to="207" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27433" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–mid Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-mid Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs with California pitcher plant (Darlingtonia californica Torrey), hillside springs, streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" constraint="with california pitcher plant" />
        <character name="habitat" value="california pitcher plant" />
        <character name="habitat" value="hillside springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14d.</number>
  <other_name type="common_name">Vollmer’s lily</other_name>
  <discussion>Subspecies vollmeri is narrowly endemic to the Siskiyou Mountains serpentine in extreme northwestern California and adjacent Oregon. A collection from near Wimer in Jackson County, Oregon, evidently represents the northernmost extent of this taxon. To the south it intergrades with subsp. pardalinum, but it can usually be told by its somewhat smaller and often redder flowers, and its narrowly elliptic or linear leaves that are often concentrated proximally on the stem. Northern populations in Curry County, Oregon, and those in the shade are rather similar to subsp. pardalinum, though the plants are usually less clonal. In the eastern part of its range, for example near Grayback Mountain in Josephine County, Oregon, and near Sanger Peak in Del Norte County, California, it intergrades extensively with subsp. wigginsii, producing swarms of individuals that vary in leaf arrangement and shape, and flower and anther coloration.</discussion>
  
</bio:treatment>