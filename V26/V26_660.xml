<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="treatment_page">327</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="genus">brodiaea</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">stellaris</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 381. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus brodiaea;species stellaris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101446</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hookera</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">stellaris</taxon_name>
    <taxon_hierarchy>genus Hookera;species stellaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Scape 2–6 cm, slender.</text>
      <biological_entity id="o15316" name="scape" name_original="scape" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers 14–24 mm;</text>
      <biological_entity id="o15317" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s1" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>perianth bluish purple, tube campanulate, 7–10 mm, transparent, not splitting in fruit, lobes ascending, recurved distally, 7–15 mm;</text>
      <biological_entity id="o15318" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish purple" value_original="bluish purple" />
      </biological_entity>
      <biological_entity id="o15319" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="transparent" value_original="transparent" />
        <character constraint="in fruit" constraintid="o15320" is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s2" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o15320" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <biological_entity id="o15321" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>filaments 1–3 mm, base not triangular, apex forked with 2 conspicuous, broad, white, abaxial appendages appearing as wings behind anthers;</text>
      <biological_entity id="o15322" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15323" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o15324" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character constraint="with abaxial appendages" constraintid="o15325" is_modifier="false" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15325" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o15326" name="wing" name_original="wings" src="d0_s3" type="structure" />
      <biological_entity id="o15327" name="anther" name_original="anthers" src="d0_s3" type="structure" />
      <relation from="o15325" id="r2083" name="appearing as" negation="false" src="d0_s3" to="o15326" />
      <relation from="o15325" id="r2084" name="appearing as" negation="false" src="d0_s3" to="o15327" />
    </statement>
    <statement id="d0_s4">
      <text>anthers linear, 4–6 mm, apex notched;</text>
      <biological_entity id="o15328" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15329" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminodia erect, held close to stamens, white, broad, 4–8 mm, wide, margins 1/4 involute, apex widely notched;</text>
      <biological_entity id="o15330" name="staminodium" name_original="staminodia" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s5" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o15331" name="close-to-stamen" name_original="close-to-stamens" src="d0_s5" type="structure" />
      <biological_entity id="o15332" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o15333" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s5" value="notched" value_original="notched" />
      </biological_entity>
      <relation from="o15330" id="r2085" name="held" negation="false" src="d0_s5" to="o15331" />
    </statement>
    <statement id="d0_s6">
      <text>ovary 6–9 mm;</text>
      <biological_entity id="o15334" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 4–5 mm;</text>
      <biological_entity id="o15335" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 1–5 cm. 2n = 12.</text>
      <biological_entity id="o15336" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15337" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="May-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in coastal forests, on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in coastal forests" />
        <character name="habitat" value="coastal forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Star brodiaea</other_name>
  <other_name type="common_name">star-flower cluster-lily</other_name>
  <discussion>Brodiaea stellaris is a serpentine endemic found in mixed evergreen and redwood forests in the North Coast Ranges of California. The white, glossy, forked appendages on the filaments of this species are very different from those found in B. appendiculata and B. californica and perhaps are not homologous. Also, the capsule is unique in the genus in that it does not split at maturity and is transparent. All other members of the genus have mature capsules that either split and are transparent, or do not split and are opaque.</discussion>
  
</bio:treatment>