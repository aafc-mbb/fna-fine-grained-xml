<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter C. Holmes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="treatment_page">282</other_info_on_meta>
    <other_info_on_meta type="illustrator">Kimberly J. Martin</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="1821" rank="genus">HIPPEASTRUM</taxon_name>
    <place_of_publication>
      <publication_title>Appendix,</publication_title>
      <place_in_publication>31. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus HIPPEASTRUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hippeus, rider, and astron, star, the allusion obscure</other_info_on_name>
    <other_info_on_name type="fna_id">115497</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, glabrous, from globose bulbs.</text>
      <biological_entity id="o22818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22819" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="globose" value_original="globose" />
      </biological_entity>
      <relation from="o22818" id="r3098" name="from" negation="false" src="d0_s0" to="o22819" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves few, basal;</text>
      <biological_entity id="o22820" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22821" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade liguliform, fleshy, parallel-veined, margins entire, apex tapering.</text>
      <biological_entity id="o22822" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="liguliform" value_original="liguliform" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="parallel-veined" value_original="parallel-veined" />
      </biological_entity>
      <biological_entity id="o22823" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22824" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape hollow.</text>
      <biological_entity id="o22825" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate, bracteate;</text>
      <biological_entity id="o22826" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, scarious.</text>
      <biological_entity id="o22827" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers spreading to slightly drooping, syntepalous;</text>
      <biological_entity id="o22828" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s6" to="slightly drooping" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="syntepalous" value_original="syntepalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth connate proximally, funnelform to campanulate, with minute corona reduced to small crown inserted on throat of tube;</text>
      <biological_entity id="o22829" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o22830" name="corona" name_original="corona" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" value_original="minute" />
        <character char_type="range_value" from="reduced" name="size" src="d0_s7" to="small" />
      </biological_entity>
      <biological_entity id="o22831" name="crown" name_original="crown" src="d0_s7" type="structure" />
      <biological_entity id="o22832" name="throat" name_original="throat" src="d0_s7" type="structure" />
      <biological_entity id="o22833" name="tube" name_original="tube" src="d0_s7" type="structure" />
      <relation from="o22829" id="r3099" name="with" negation="false" src="d0_s7" to="o22830" />
      <relation from="o22831" id="r3100" name="inserted on" negation="false" src="d0_s7" to="o22832" />
      <relation from="o22831" id="r3101" name="part_of" negation="false" src="d0_s7" to="o22833" />
    </statement>
    <statement id="d0_s8">
      <text>tepals 6 in 2 whorls of 3, outer slightly shorter than inner;</text>
      <biological_entity id="o22834" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character constraint="in whorls" constraintid="o22835" name="quantity" src="d0_s8" value="6" value_original="6" />
        <character constraint="than inner tepals" constraintid="o22837" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o22835" name="whorl" name_original="whorls" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character constraint="than inner tepals" constraintid="o22837" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity constraint="outer" id="o22836" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o22837" name="tepal" name_original="tepals" src="d0_s8" type="structure" />
      <relation from="o22835" id="r3102" name="part_of" negation="false" src="d0_s8" to="o22836" />
    </statement>
    <statement id="d0_s9">
      <text>stamens inserted on perianth-tube, declinate, subequal;</text>
      <biological_entity id="o22838" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="declinate" value_original="declinate" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o22839" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
      <relation from="o22838" id="r3103" name="inserted on" negation="false" src="d0_s9" to="o22839" />
    </statement>
    <statement id="d0_s10">
      <text>filaments slender;</text>
      <biological_entity id="o22840" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary inferior, ellipsoid;</text>
      <biological_entity id="o22841" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style slender, ca. equaling tepals;</text>
      <biological_entity id="o22842" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o22843" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma capitate or slightly 3-lobed.</text>
      <biological_entity id="o22844" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsular, dehiscence loculicidal.</text>
      <biological_entity id="o22845" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds few-to-many.</text>
      <biological_entity id="o22846" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s15" to="many" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c America, West Indies, South America, w Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="w Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49.</number>
  <other_name type="common_name">Barbados-lily</other_name>
  <other_name type="common_name">naked lady</other_name>
  <other_name type="common_name">red spider-lily</other_name>
  <discussion>Species ca. 75 (1 in the flora).</discussion>
  
</bio:treatment>