<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="J. D. Freeman" date="1975" rank="species">foetidissimum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>27: 31, fig. 7. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species foetidissimum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101993</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes horizontal, brownish, thick, short, praemorse, not brittle.</text>
      <biological_entity id="o6019" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s0" value="praemorse" value_original="praemorse" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s0" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–2, green to maroon, round in cross-section, 0.8–2.8 dm, papillose basally.</text>
      <biological_entity id="o6020" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="maroon" />
        <character constraint="in cross-section" constraintid="o6021" is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="2.8" to_unit="dm" />
        <character is_modifier="false" modifier="basally" name="relief" src="d0_s1" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6021" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts often carried quite horizontally, well above ground, sessile;</text>
      <biological_entity id="o6022" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6023" name="ground" name_original="ground" src="d0_s2" type="structure" />
      <relation from="o6022" id="r859" name="carried" negation="false" src="d0_s2" to="o6023" />
    </statement>
    <statement id="d0_s3">
      <text>blade light green or bronze-green, strongly mottled in dark green with central light green stripe, mottling becoming obscure with age but less so than in most species, elliptic-ovate, rarely ± orbicular, 6.7–12 × 3.8–6 cm, not glossy, base evenly tapered to broad attachment, apex obtuse-acute.</text>
      <biological_entity id="o6024" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="bronze-green" value_original="bronze-green" />
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s3" value="mottled" value_original="mottled" />
        <character constraint="with central stripe" constraintid="o6025" is_modifier="false" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o6026" is_modifier="false" modifier="becoming" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="less" name="shape" notes="" src="d0_s3" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character is_modifier="false" modifier="rarely more or less" name="shape" src="d0_s3" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="6.7" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s3" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="central" id="o6025" name="stripe" name_original="stripe" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="light green" value_original="light green" />
      </biological_entity>
      <biological_entity id="o6026" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o6027" name="species" name_original="species" src="d0_s3" type="taxon_name" />
      <biological_entity id="o6028" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o6029" name="attachment" name_original="attachment" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o6030" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse-acute" value_original="obtuse-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flower borne directly on bracts, odor of putrid meat, especially when in strong sunlight;</text>
      <biological_entity id="o6031" name="flower" name_original="flower" src="d0_s4" type="structure" />
      <biological_entity id="o6032" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity id="o6033" name="meat" name_original="meat" src="d0_s4" type="structure">
        <character is_modifier="true" name="odor" src="d0_s4" value="putrid" value_original="putrid" />
      </biological_entity>
      <relation from="o6031" id="r860" name="borne" negation="false" src="d0_s4" to="o6032" />
    </statement>
    <statement id="d0_s5">
      <text>sepals displayed above bracts, carried almost horizontally, green or green streaked with dark maroon, lanceolate, 16–40 × 4–6 mm, thick-textured, margins entire, apex acute;</text>
      <biological_entity id="o6034" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6035" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o6036" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="green streaked with dark maroon" />
        <character is_modifier="true" modifier="almost horizontally" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="16" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="true" name="texture" src="d0_s5" value="thick-textured" value_original="thick-textured" />
      </biological_entity>
      <biological_entity id="o6037" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="green streaked with dark maroon" />
        <character is_modifier="true" modifier="almost horizontally" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="16" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="true" name="texture" src="d0_s5" value="thick-textured" value_original="thick-textured" />
      </biological_entity>
      <relation from="o6034" id="r861" name="displayed above" negation="false" src="d0_s5" to="o6035" />
      <relation from="o6034" id="r862" name="carried" negation="false" src="d0_s5" to="o6036" />
      <relation from="o6034" id="r863" name="carried" negation="false" src="d0_s5" to="o6037" />
    </statement>
    <statement id="d0_s6">
      <text>petals long-lasting, erect, very gradually incurved from base to apex, ± connivent, ± concealing stamens and ovary, pinkish purple, light to reddish purple, brownish purple, rarely yellow, fading to brownish tones with age, not spirally twisted, not inrolling with age, veins not engraved, narrowly elliptic to linearlanceolate, 2–5 × 0.3–0.5 cm, thick-textured, not glossy, margins entire, flat, acute at apex;</text>
      <biological_entity id="o6038" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="long-lasting" value_original="long-lasting" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character constraint="from base" constraintid="o6039" is_modifier="false" modifier="very gradually" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o6039" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o6040" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o6041" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pinkish purple" value_original="pinkish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light to reddish purple" value_original="light to reddish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character constraint="with age" constraintid="o6043" is_modifier="false" name="coloration" src="d0_s6" value="fading to brownish tones" value_original="fading to brownish tones" />
        <character is_modifier="false" modifier="not spirally" name="architecture" notes="" src="d0_s6" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o6042" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pinkish purple" value_original="pinkish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light to reddish purple" value_original="light to reddish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character constraint="with age" constraintid="o6043" is_modifier="false" name="coloration" src="d0_s6" value="fading to brownish tones" value_original="fading to brownish tones" />
        <character is_modifier="false" modifier="not spirally" name="architecture" notes="" src="d0_s6" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o6043" name="age" name_original="age" src="d0_s6" type="structure" />
      <biological_entity id="o6044" name="age" name_original="age" src="d0_s6" type="structure" />
      <biological_entity id="o6045" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s6" value="engraved" value_original="engraved" />
        <character constraint="to margins" constraintid="o6046" is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s6" value="flat" value_original="flat" />
        <character constraint="at apex" constraintid="o6047" is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6046" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s6" to="0.5" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s6" value="thick-textured" value_original="thick-textured" />
        <character is_modifier="true" modifier="not" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6047" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o6039" id="r864" name="to" negation="false" src="d0_s6" to="o6040" />
      <relation from="o6041" id="r865" name="with" negation="true" src="d0_s6" to="o6044" />
      <relation from="o6042" id="r866" name="with" negation="false" src="d0_s6" to="o6044" />
    </statement>
    <statement id="d0_s7">
      <text>stamens relatively prominent, erect, 9–25 mm;</text>
      <biological_entity id="o6048" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="relatively" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments dark maroon, 3–6 mm, dilated basally;</text>
      <biological_entity id="o6049" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark maroon" value_original="dark maroon" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers straight, dark maroon-black, 8–15 mm, dehiscence introrse;</text>
      <biological_entity id="o6050" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark maroon-black" value_original="dark maroon-black" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>connectives straight, extended 1–1.5 mm beyond anther sacs;</text>
      <biological_entity id="o6051" name="connectif" name_original="connectives" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s10" value="extended" value_original="extended" />
        <character char_type="range_value" constraint="beyond anther sacs" constraintid="o6052" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="anther" id="o6052" name="sac" name_original="sacs" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary red-purple, ovoid, hexagonal in cross-section, 5–12 mm, broadly attached;</text>
      <biological_entity id="o6053" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character constraint="in cross-section" constraintid="o6054" is_modifier="false" name="shape" src="d0_s11" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="fixation" src="d0_s11" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o6054" name="cross-section" name_original="cross-section" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stigmas erect, divergent-recurved, distinct, dark purple, subulate, nearly as long as ovary, fleshy.</text>
      <biological_entity id="o6055" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="divergent-recurved" value_original="divergent-recurved" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="texture" notes="" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o6056" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <relation from="o6055" id="r867" modifier="nearly" name="as long as" negation="false" src="d0_s12" to="o6056" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits purplish brown, ovoid, 6-angled at least apically, fleshy.</text>
      <biological_entity id="o6057" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="at-least apically; apically" name="shape" src="d0_s13" value="6-angled" value_original="6-angled" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early spring (early Mar [rarely Feb]--early Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="late winter" />
        <character name="flowering time" char_type="atypical_range" modifier="rarely" to="early Mar" from="early Mar" />
        <character name="flowering time" char_type="range_value" to="Feb" from="Feb" constraint=" -early Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>River bluffs, ravines, floodplains, low ground, rich woods, road shoulders, silts, sandy-alluvium, loess soils, drier upland oak and pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="river bluffs" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="low ground" />
        <character name="habitat" value="rich woods" />
        <character name="habitat" value="road shoulders" />
        <character name="habitat" value="silts" />
        <character name="habitat" value="sandy-alluvium" />
        <character name="habitat" value="loess soils" />
        <character name="habitat" value="drier upland oak" />
        <character name="habitat" value="pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Stinking trillium</other_name>
  <other_name type="common_name">fetid trillium</other_name>
  <discussion>Trillium foetidissimum seems tolerant of a wide range of soil moistures and types, from low, swampy woods to high, dry bluffs and ravine slopes. This is the only Trillium known to occur within its Louisiana range (J. D. Freeman 1975). Freeman considered it to be closely related to T. sessile.</discussion>
  
</bio:treatment>