<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="treatment_page">638</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="Bongard" date="1832" rank="species">mertensiana</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Acad. Imp. Sci. St. Pétersbourg, Sér.</publication_title>
      <place_in_publication>6, Sci. Math. 2: 165. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species mertensiana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101534</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="(Rafinesque) Rafinesque" date="unknown" rank="species">maculata</taxon_name>
    <taxon_name authority="(Bongard) Calder &amp; Roy L. Taylor" date="unknown" rank="subspecies">mertensiana</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species maculata;subspecies mertensiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="L. O. Williams" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species purpurea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ± strongly thickened, base not bulbous.</text>
      <biological_entity id="o34915" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less strongly" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o34916" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: racemes dense, 35–65 × 1.5–4 cm.</text>
      <biological_entity id="o34917" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o34918" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character char_type="range_value" from="35" from_unit="cm" name="length" src="d0_s1" to="65" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 8–35, showy;</text>
      <biological_entity id="o34919" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="35" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth open;</text>
      <biological_entity id="o34920" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reddish purple, sometimes yellowish near base, or completely yellow, lanceolate, 3-veined, 6–12 mm;</text>
      <biological_entity id="o34921" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish purple" value_original="reddish purple" />
        <character constraint="near base" constraintid="o34922" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="completely" name="coloration" notes="" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34922" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal arching over column, nearly adhering to it;</text>
      <biological_entity constraint="dorsal" id="o34923" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character constraint="over column" constraintid="o34924" is_modifier="false" name="orientation" src="d0_s5" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o34924" name="column" name_original="column" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals strongly spreading;</text>
      <biological_entity constraint="lateral" id="o34925" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals arching over column, connivent with dorsal sepal, often yellowish basally, and streaked with purple, or completely yellow suffused with purple toward apex;</text>
      <biological_entity id="o34926" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character constraint="over column" constraintid="o34927" is_modifier="false" name="orientation" src="d0_s7" value="arching" value_original="arching" />
        <character constraint="with dorsal sepal" constraintid="o34928" is_modifier="false" name="arrangement" notes="" src="d0_s7" value="connivent" value_original="connivent" />
        <character is_modifier="false" modifier="often; basally" name="coloration" notes="" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="coloration" value_original="coloration" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="list" value_original="list" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="streaked with purple" value_original="streaked with purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="punct" value_original="punct" />
        <character is_modifier="false" modifier="completely" name="coloration" src="d0_s7" value="yellow suffused" value_original="yellow suffused" />
        <character constraint="toward apex" constraintid="o34929" is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o34927" name="column" name_original="column" src="d0_s7" type="structure" />
      <biological_entity constraint="dorsal" id="o34928" name="sepal" name_original="sepal" src="d0_s7" type="structure" />
      <biological_entity id="o34929" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lip red-purple, white, or white with purple streaks or spots, narrowly obovate, 4.8–9.5 × 2.5–5 mm, thin, usually with small (0.7 mm) tooth on each side, margins undulate-denticulate;</text>
      <biological_entity id="o34930" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character constraint="with spots" constraintid="o34932" is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="length" src="d0_s8" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o34931" name="streak" name_original="streaks" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o34932" name="spot" name_original="spots" src="d0_s8" type="structure" />
      <biological_entity id="o34933" name="tooth" name_original="tooth" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o34934" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o34935" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="undulate-denticulate" value_original="undulate-denticulate" />
      </biological_entity>
      <relation from="o34930" id="r4688" modifier="usually" name="with" negation="false" src="d0_s8" to="o34933" />
      <relation from="o34933" id="r4689" name="on" negation="false" src="d0_s8" to="o34934" />
    </statement>
    <statement id="d0_s9">
      <text>column curved somewhat toward lip, yellow, often flushed with purple or white basally, and streaked or spotted with purple, 5–8.2 mm;</text>
      <biological_entity id="o34936" name="column" name_original="column" src="d0_s9" type="structure">
        <character constraint="toward lip" constraintid="o34937" is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="flushed with purple or flushed with white" />
        <character name="coloration" src="d0_s9" value="basally" value_original="basally" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="spotted with purple" value_original="spotted with purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34937" name="lip" name_original="lip" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>ovary 5.9–10 mm;</text>
      <biological_entity id="o34938" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.9" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mentum prominent, protruding backward along ovary but free from it.</text>
      <biological_entity id="o34939" name="mentum" name_original="mentum" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="protruding" value_original="protruding" />
        <character constraint="along ovary" constraintid="o34940" is_modifier="false" name="orientation" src="d0_s11" value="backward" value_original="backward" />
      </biological_entity>
      <biological_entity id="o34940" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character constraint="from it" is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ellipsoid, 10–25 × 6–9 mm. 2n = 40.</text>
      <biological_entity id="o34941" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34942" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry coniferous and mixed woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to dry coniferous" />
        <character name="habitat" value="mixed woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Merten’s coral-root</other_name>
  <other_name type="common_name">western coral-root</other_name>
  <discussion>In the Pacific Northwest Corallorhiza mertensiana is largely sympatric with C. maculata and occasionally intergrades with it. It frequently forms large clumps. </discussion>
  
</bio:treatment>