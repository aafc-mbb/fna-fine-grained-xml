<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mark W. Chase,James D. Ackerman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">651</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Maxillarieae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Oncidiinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">TOLUMNIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Tellur.</publication_title>
      <place_in_publication>2: 101. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe maxillarieae;subtribe oncidiinae;genus tolumnia;</taxon_hierarchy>
    <other_info_on_name type="etymology">Probably for Tolumnius, a Rutulian mentioned by Virgil</other_info_on_name>
    <other_info_on_name type="fna_id">133096</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Xaritonia</taxon_name>
    <taxon_hierarchy>genus Xaritonia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, epiphytic or rarely terrestrial, ± climbing, fan-shaped.</text>
      <biological_entity id="o32273" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="shape" src="d0_s0" value="fan--shaped" value_original="fan--shaped" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes often elongate, covered with bracts;</text>
      <biological_entity id="o32274" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o32275" name="bract" name_original="bracts" src="d0_s1" type="structure" />
      <relation from="o32274" id="r4340" name="covered with" negation="false" src="d0_s1" to="o32275" />
    </statement>
    <statement id="d0_s2">
      <text>pseudobulbs reduced to absent.</text>
      <biological_entity id="o32276" name="pseudobulb" name_original="pseudobulbs" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal 3–7, apical not more than 1;</text>
      <biological_entity id="o32277" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o32278" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity constraint="apical" id="o32279" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade recurved, lanceolate, laterally flattened–3-gonal, margins often serrulate.</text>
      <biological_entity id="o32280" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o32281" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s4" value="flattened-3-gonal" value_original="flattened-3-gonal" />
      </biological_entity>
      <biological_entity id="o32282" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes or weakly paniculate;</text>
      <biological_entity constraint="inflorescences" id="o32283" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts narrowly triangular.</text>
      <biological_entity id="o32284" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals and petals spatulate-clawed;</text>
      <biological_entity id="o32285" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o32286" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="spatulate-clawed" value_original="spatulate-clawed" />
      </biological_entity>
      <biological_entity id="o32287" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="spatulate-clawed" value_original="spatulate-clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals sometimes partially connate;</text>
      <biological_entity id="o32288" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o32289" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes partially" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip 3-lobed, pandurate, without nectar cavity;</text>
      <biological_entity id="o32290" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32291" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="pandurate" value_original="pandurate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o32292" name="cavity" name_original="cavity" src="d0_s9" type="structure" />
      <relation from="o32291" id="r4341" name="without" negation="false" src="d0_s9" to="o32292" />
    </statement>
    <statement id="d0_s10">
      <text>callus prominent tuberculate-lobulate;</text>
      <biological_entity id="o32293" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32294" name="callus" name_original="callus" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tuberculate-lobulate" value_original="tuberculate-lobulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>column with pair of outstretched apical wings, without hood;</text>
      <biological_entity id="o32295" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32296" name="column" name_original="column" src="d0_s11" type="structure" />
      <biological_entity id="o32297" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <biological_entity constraint="apical" id="o32298" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="outstretched" value_original="outstretched" />
      </biological_entity>
      <biological_entity id="o32299" name="hood" name_original="hood" src="d0_s11" type="structure" />
      <relation from="o32296" id="r4342" name="with" negation="false" src="d0_s11" to="o32297" />
      <relation from="o32297" id="r4343" name="part_of" negation="false" src="d0_s11" to="o32298" />
      <relation from="o32296" id="r4344" name="without" negation="false" src="d0_s11" to="o32299" />
    </statement>
    <statement id="d0_s12">
      <text>stigmatic cavity round;</text>
      <biological_entity id="o32300" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="stigmatic" id="o32301" name="cavity" name_original="cavity" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rostellum not prominent.</text>
      <biological_entity id="o32302" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o32303" name="rostellum" name_original="rostellum" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, ellipsoid.</text>
      <biological_entity constraint="fruits" id="o32304" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Restricted to s Fla., West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Restricted to s Fla" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>69.</number>
  <discussion>Species 30 (1 in the flora).</discussion>
  <discussion>Tolumnia does not belong to the main body of Oncidium (its traditional placement) even though many of the species have floral morphologies nearly identical to that of Oncidium. It shares, with other twig epiphytes, a fan-shaped habit and seeds with prominent extensions of the testa.</discussion>
  <references>
    <reference>Braem, G. J. 1986. Tolumnia—Der neue, aber doch alte, Name für die “Variegaten Oncidien.” Orchidee (Hamburg) 37: 55–59.  </reference>
    <reference>Williams, N. H., M. W. Chase, T. Fulcher, and W. M. Whitten. 2001. Molecular systematics of the Oncidiinae based on evidence from four DNA sequence regions: Expanded circumscriptions of Cyrtochilum, Erycina, Otoglossum, and Trichocentrum and a new genus (Orchidaceae). Lindleyana 16: 113–139.</reference>
    <reference>Chase, M. W. and J. S. Pippen. 1988. Seed morphology in the Oncidiinae and related subtribes (Orchidaceae). Syst. Bot. 13: 313–323.</reference>
  </references>
  
</bio:treatment>