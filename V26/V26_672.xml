<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J. Chris Pires</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Hoover" date="1941" rank="genus">TRITELEIOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>25: 98. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus TRITELEIOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">genus Triteleia and Greek opsis, like</other_info_on_name>
    <other_info_on_name type="fna_id">133819</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, from fibrous-coated corms.</text>
      <biological_entity id="o23702" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o23703" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character is_modifier="true" name="coating" src="d0_s0" value="fibrous-coated" value_original="fibrous-coated" />
      </biological_entity>
      <relation from="o23702" id="r3217" name="from" negation="false" src="d0_s0" to="o23703" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, crowded at base;</text>
      <biological_entity id="o23704" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character constraint="at base" constraintid="o23705" is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o23705" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade planate, keeled, linear, base expanded, margins entire.</text>
      <biological_entity id="o23706" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="planate" value_original="planate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23707" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o23708" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape solitary (rarely 2), cylindrical, stout, 4.5–6 dm × 7–15 mm, pithy, glaucous distally.</text>
      <biological_entity id="o23709" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="4.5" from_unit="dm" name="length" src="d0_s3" to="6" to_unit="dm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="pithy" value_original="pithy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate, open, many-flowered, bracteate;</text>
      <biological_entity id="o23710" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucral-bracts 7–10, scarious, apex acuminate.</text>
      <biological_entity id="o23711" name="involucral-bract" name_original="involucral-bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o23712" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth 6-tepaled, funnelform, distinctly connate proximally into tube;</text>
      <biological_entity id="o23713" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o23714" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="6-tepaled" value_original="6-tepaled" />
        <character constraint="into tube" constraintid="o23715" is_modifier="false" modifier="distinctly" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o23715" name="tube" name_original="tube" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>perianth appendages arising at intersection of perianth-tube and lobes;</text>
      <biological_entity id="o23716" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="perianth" id="o23717" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="at intersection" constraintid="o23718" is_modifier="false" name="orientation" src="d0_s7" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o23718" name="intersection" name_original="intersection" src="d0_s7" type="structure" />
      <biological_entity id="o23719" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure" />
      <biological_entity id="o23720" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <relation from="o23718" id="r3218" name="part_of" negation="false" src="d0_s7" to="o23719" />
      <relation from="o23718" id="r3219" name="part_of" negation="false" src="d0_s7" to="o23720" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 6, epitepalous;</text>
      <biological_entity id="o23721" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23722" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s8" value="epitepalous" value_original="epitepalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments inserted in distal part of perianth-tube;</text>
      <biological_entity id="o23723" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23724" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o23725" name="part" name_original="part" src="d0_s9" type="structure" />
      <biological_entity id="o23726" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
      <relation from="o23724" id="r3220" name="inserted in" negation="false" src="d0_s9" to="o23725" />
      <relation from="o23724" id="r3221" name="part_of" negation="false" src="d0_s9" to="o23726" />
    </statement>
    <statement id="d0_s10">
      <text>anthers basifixed, distant from style;</text>
      <biological_entity id="o23727" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23728" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s10" value="basifixed" value_original="basifixed" />
        <character constraint="from style" constraintid="o23729" is_modifier="false" name="arrangement" src="d0_s10" value="distant" value_original="distant" />
      </biological_entity>
      <biological_entity id="o23729" name="style" name_original="style" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o23730" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23731" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, stipitate, 3-locular, ovules several;</text>
      <biological_entity id="o23732" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23733" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity id="o23734" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style erect, slender;</text>
      <biological_entity id="o23735" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23736" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma not evidently lobed;</text>
      <biological_entity id="o23737" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o23738" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not evidently" name="shape" src="d0_s14" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pedicel articulate, 1.5–5 cm.</text>
      <biological_entity id="o23739" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o23740" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="articulate" value_original="articulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s15" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular, rounded at apex, equaling withered perianth, dehiscence loculicidal.</text>
      <biological_entity id="o23741" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character constraint="at apex" constraintid="o23742" is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o23742" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o23743" name="perianth" name_original="perianth" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="life_cycle" src="d0_s16" value="withered" value_original="withered" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds black, elongate, thin, flat, surface minutely and irregularly roughened, coat with crust.</text>
      <biological_entity id="o23744" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s17" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o23745" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="irregularly" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o23747" name="crust" name_original="crust" src="d0_s17" type="structure" />
      <relation from="o23746" id="r3222" name="with" negation="false" src="d0_s17" to="o23747" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 33.</text>
      <biological_entity id="o23746" name="coat" name_original="coat" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o23748" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="33" value_original="33" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw Ariz., n Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="sw Ariz." establishment_means="native" />
        <character name="distribution" value="n Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  <other_name type="common_name">Baja-lily</other_name>
  <discussion>Species 1.</discussion>
  <discussion>G. Keator (1989) considered Triteleiopsis to be part of a group of Mexican genera (Milla, Bessera, Behria Greene), while H. E. Moore Jr. (1953) and R. F. Hoover (1941) considered it to be allied with Androstephium, Bloomeria, Brodiaea, Dichelostemma, Muilla, and Triteleia. Molecular and morphological evidence indicates that it is related to Dichelostemma and Brodiaea, but not to Triteleia (J. C. Pires 2000).</discussion>
  <references>
    <reference>Hoover, R. F. 1941. A systematic study of Triteleia. Amer. Midl. Naturalist 25: 73–100.  </reference>
    <reference>Lenz, L. W. 1975. A biosystematic study of Triteleia (Liliaceae): 1. Revision of the species of section Calliprora. Aliso 8: 221–258.</reference>
  </references>
  
</bio:treatment>