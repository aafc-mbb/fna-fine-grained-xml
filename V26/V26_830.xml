<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="treatment_page">404</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="N. E. Brown" date="1932" rank="genus">chasmanthe</taxon_name>
    <taxon_name authority="(Salisbury) N. E. Brown" date="1932" rank="species">floribunda</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Roy. Soc. South Africa</publication_title>
      <place_in_publication>20: 274. 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus chasmanthe;species floribunda</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101522</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antholyza</taxon_name>
    <taxon_name authority="Salisbury" date="unknown" rank="species">floribunda</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Hort. Soc. London</publication_title>
      <place_in_publication>1: 324. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Antholyza;species floribunda;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antholyza</taxon_name>
    <taxon_name authority="Redouté" date="unknown" rank="species">prealta</taxon_name>
    <taxon_hierarchy>genus Antholyza;species prealta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–120 cm;</text>
      <biological_entity id="o15610" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>corm depressed-globose, 40–70 mm diam.</text>
      <biological_entity id="o15611" name="corm" name_original="corm" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="40" from_unit="mm" name="diameter" src="d0_s1" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–2-branched.</text>
      <biological_entity id="o15612" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-2-branched" value_original="1-2-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 8–10, mostly basal, basal longer than cauline;</text>
      <biological_entity id="o15613" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity id="o15614" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15615" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="than cauline leaves" constraintid="o15616" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o15616" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate, ± reaching base of spike, 18–35 mm wide.</text>
      <biological_entity id="o15617" name="blade" name_original="blade" src="d0_s4" type="structure" constraint="spike" constraint_original="spike; spike">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="18" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15618" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o15619" name="spike" name_original="spike" src="d0_s4" type="structure" />
      <relation from="o15617" id="r2121" modifier="more or less" name="reaching" negation="false" src="d0_s4" to="o15618" />
      <relation from="o15617" id="r2122" name="part_of" negation="false" src="d0_s4" to="o15619" />
    </statement>
    <statement id="d0_s5">
      <text>Spikes 25–40-flowered;</text>
      <biological_entity id="o15620" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="25-40-flowered" value_original="25-40-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer spathe 13–15 mm, apex obtuse to truncate;</text>
      <biological_entity constraint="outer" id="o15621" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15622" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner ± equaling outer, apex bifurcate;</text>
      <biological_entity constraint="inner" id="o15623" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o15624" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o15625" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flowers distichous.</text>
      <biological_entity id="o15626" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Tepals: perianth-tube 35–45 mm, 9–12 mm proximally, often twisted, ca. 40 mm distally, base pouched;</text>
      <biological_entity id="o15627" name="tepal" name_original="tepals" src="d0_s9" type="structure" />
      <biological_entity id="o15628" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="distance" src="d0_s9" to="45" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" modifier="proximally" name="distance" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="twisted" value_original="twisted" />
        <character modifier="distally" name="distance" src="d0_s9" unit="mm" value="40" value_original="40" />
      </biological_entity>
      <biological_entity id="o15629" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="pouched" value_original="pouched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dorsal tepal horizontal, 28–33 × 7–9 mm, much exceeding others;</text>
      <biological_entity id="o15630" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
      <biological_entity constraint="dorsal" id="o15631" name="tepal" name_original="tepal" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character char_type="range_value" from="28" from_unit="mm" name="length" src="d0_s10" to="33" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15632" name="other" name_original="others" src="d0_s10" type="structure" />
      <relation from="o15631" id="r2123" modifier="much" name="exceeding" negation="false" src="d0_s10" to="o15632" />
    </statement>
    <statement id="d0_s11">
      <text>lateral tepals patent or recurved, 12–15 × 4–7 mm;</text>
      <biological_entity id="o15633" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
      <biological_entity constraint="lateral" id="o15634" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower median tepal slightly smaller than laterals;</text>
      <biological_entity id="o15635" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <biological_entity constraint="lower median" id="o15636" name="tepal" name_original="tepal" src="d0_s12" type="structure">
        <character constraint="than laterals" constraintid="o15637" is_modifier="false" name="size" src="d0_s12" value="slightly smaller" value_original="slightly smaller" />
      </biological_entity>
      <biological_entity id="o15637" name="lateral" name_original="laterals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 50–55 mm;</text>
      <biological_entity id="o15638" name="tepal" name_original="tepals" src="d0_s13" type="structure" />
      <biological_entity id="o15639" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s13" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 7–8 mm;</text>
      <biological_entity id="o15640" name="tepal" name_original="tepals" src="d0_s14" type="structure" />
      <biological_entity id="o15641" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 6–9 mm, style branching shortly below and opposite to anthers (or exceeding them);</text>
      <biological_entity id="o15642" name="tepal" name_original="tepals" src="d0_s15" type="structure" />
      <biological_entity id="o15643" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15644" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="shortly below; below" name="architecture" src="d0_s15" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o15645" name="opposite-to-anther" name_original="opposite-to-anthers" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>branches 7–10 mm.</text>
      <biological_entity id="o15646" name="tepal" name_original="tepals" src="d0_s16" type="structure" />
      <biological_entity id="o15647" name="branch" name_original="branches" src="d0_s16" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s16" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 10–15 mm.</text>
      <biological_entity id="o15648" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s17" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 5–7 mm diam.</text>
      <biological_entity id="o15649" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s18" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Road verges, grassy slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="road verges" />
        <character name="habitat" value="grassy slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Chasmanthe floribunda has escaped from gardens and has become naturalized locally. It is native to the winter-rainfall region of South Africa. The species has been confused in North America with C. aethiopica (Linnaeus) N. E. Brown, which has similar flowers but is a smaller plant with unbranched stems, normally flowering in the early winter months.</discussion>
  
</bio:treatment>