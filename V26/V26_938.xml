<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="treatment_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 323. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species americana</taxon_hierarchy>
    <other_info_on_name type="fna_id">200028030</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent or short-stemmed, commonly suckering, trunks less than 2 m;</text>
      <biological_entity id="o21549" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-stemmed" value_original="short-stemmed" />
        <character is_modifier="false" modifier="commonly" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21550" name="trunk" name_original="trunks" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes not cespitose, 10–20 × 20–37 dm.</text>
      <biological_entity id="o21551" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="10" from_unit="dm" name="length" src="d0_s1" to="20" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="width" src="d0_s1" to="37" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, spreading to ascending, occasionally reflexed, 80–200 × 15–25 cm;</text>
      <biological_entity id="o21552" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="80" from_unit="cm" name="length" src="d0_s2" to="200" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light green to green or glaucous-gray, sometimes variegated or cross-zoned, narrowly to broadly lanceolate, smooth, rigid;</text>
      <biological_entity id="o21553" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s3" to="green or glaucous-gray" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="variegated" value_original="variegated" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="cross-zoned" value_original="cross-zoned" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins nearly straight or undulate to crenate, armed, teeth single, 5–10 mm, 1–4 cm apart;</text>
      <biological_entity id="o21554" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character name="course" src="d0_s4" value="undulate to crenate" value_original="undulate to crenate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o21555" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apical spine dark-brown to grayish, conical or subulate, 2–6 cm.</text>
      <biological_entity constraint="apical" id="o21556" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s5" to="grayish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="conical" value_original="conical" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 5–9 m.</text>
      <biological_entity id="o21557" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="m" name="some_measurement" src="d0_s6" to="9" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, not bulbiferous;</text>
      <biological_entity id="o21558" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, triangular, 5–15 cm;</text>
      <biological_entity id="o21559" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 15–35, horizontal to slightly ascending, comprising distal 1/3–1/2 of inflorescence, longer than 10 cm.</text>
      <biological_entity constraint="lateral" id="o21560" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="35" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s9" to="slightly ascending" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o21561" name="inflorescence" name_original="inflorescence" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <relation from="o21560" id="r2944" name="comprising" negation="false" src="d0_s9" to="o21561" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers erect, 7–10.5 cm;</text>
      <biological_entity id="o21562" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s10" to="10.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth yellow, tube funnelform to cylindric, 8–20 × 12–20 mm, limb lobes erect, subequal, 20–35 mm;</text>
      <biological_entity id="o21563" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21564" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o21565" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="length" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" is_modifier="true" name="width" src="d0_s11" to="20" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o21564" id="r2945" name="to" negation="false" src="d0_s11" to="o21565" />
    </statement>
    <statement id="d0_s12">
      <text>stamens long-exserted;</text>
      <biological_entity id="o21566" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted above mid perianth-tube, erect, yellow, 6–9 cm;</text>
      <biological_entity id="o21567" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s13" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="mid" id="o21568" name="perianth-tube" name_original="perianth-tube" src="d0_s13" type="structure" />
      <relation from="o21567" id="r2946" name="inserted above" negation="false" src="d0_s13" to="o21568" />
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, 25–35 mm;</text>
      <biological_entity id="o21569" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s14" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 3–4.5 cm, neck constricted, 3–6 (–8) mm.</text>
      <biological_entity id="o21570" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s15" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21571" name="neck" name_original="neck" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules short-pedicellate, oblong, 3.5–8 cm, apex beaked.</text>
      <biological_entity id="o21572" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="short-pedicellate" value_original="short-pedicellate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s16" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21573" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 6–8 mm.</text>
      <biological_entity id="o21574" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Various chromosome numbers have been reported for Agave americana under a variety of names, typically without regard to the plant’s origin or its precise taxonomic disposition. Nonetheless, the species is most certainly a polyploid complex based on x = 30, with reports of n = 30 and 2n = 60, 120, and 180 documented by S. D. McKelvey and K. Sax (1933), H. Matsuura and T. Sutô (1935), E. B. Granick (1944), A. K. Sharma and U. C. Bhattacharyya (1962), M. S. Cave (1964), S. Banerjee and A. K. Sharma (1987), Huang S. F. et al. (1989) and B. Vijayavalli and P. M. Mathew (1990). Various dysploids have also been reported (A. F. Dyer et al. 1970; J. L. Strother and G. L. Nesom 1997). See H. S. Gentry (1982) for details.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 80–135 cm, 4–6 times longer than wide; capsules 3.5–4 cm.</description>
      <determination>12a Agave americana subsp. protamericana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 100–200 cm, 6–10 times longer than wide; capsules 4–8 cm.</description>
      <determination>12b Agave americana subsp. americana</determination>
    </key_statement>
  </key>
</bio:treatment>