<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>R. David Whetstone</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">STEMONACEAE</taxon_name>
    <taxon_hierarchy>family STEMONACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10849</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [herbaceous vines, or low shrubs], caulescent [stemless], from rhizomes [tuberous roots].</text>
      <biological_entity id="o18109" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18110" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o18109" id="r2478" name="from" negation="false" src="d0_s0" to="o18110" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves simple, stipules absent.</text>
      <biological_entity id="o18111" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o18112" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves sheathing.</text>
      <biological_entity constraint="basal" id="o18113" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves present [absent], alternate [whorled], petiolate, venation ± palmate [arcuate-pinnate with strongly developed midvein], transverse parallel veinlets connecting primary-veins.</text>
      <biological_entity constraint="cauline" id="o18114" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity id="o18115" name="veinlet" name_original="veinlets" src="d0_s3" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s3" value="transverse" value_original="transverse" />
        <character is_modifier="true" name="arrangement" src="d0_s3" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o18116" name="primary-vein" name_original="primary-veins" src="d0_s3" type="structure" />
      <relation from="o18115" id="r2479" name="connecting" negation="false" src="d0_s3" to="o18116" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, cymose [racemose or unifloral].</text>
      <biological_entity id="o18117" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, radially [bilaterally] symmetric;</text>
      <biological_entity id="o18118" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth hypogynous [to epigynous];</text>
      <biological_entity id="o18119" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals 4 [–5], distinct [connate];</text>
      <biological_entity id="o18120" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="5" />
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 4 [–5], opposite tepals;</text>
      <biological_entity id="o18121" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o18122" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1-locular;</text>
    </statement>
    <statement id="d0_s10">
      <text>placentation apical [basal];</text>
      <biological_entity id="o18123" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s10" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 1 [absent];</text>
      <biological_entity id="o18124" name="style" name_original="style" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas [1] –3.</text>
      <biological_entity id="o18125" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="atypical_quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, 2 [–3] -valved.</text>
      <biological_entity id="o18126" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2[-3]-valved" value_original="2[-3]-valved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose [to ellipsoid].</text>
      <biological_entity id="o18127" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, e and se Asia, n Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="e and se Asia" establishment_means="native" />
        <character name="distribution" value="n Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>226.</number>
  <other_name type="common_name">Stemona Family</other_name>
  <discussion>Genera 4, species 30–35 (1 genus, 1 species in the flora).</discussion>
  <references>
    <reference>Rogers, G. K. 1982. The Stemonaceae in the southeastern United States. J. Arnold Arbor. 63: 327–336.  </reference>
    <reference>Whetstone, R. D. 1984. Notes on Croomia pauciflora (Stemonaceae). Rhodora 86: 131–137.</reference>
  </references>
  
</bio:treatment>