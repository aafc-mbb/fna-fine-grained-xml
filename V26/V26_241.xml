<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MEDEOLA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 339. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 158. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus MEDEOLA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Medea, mythical sorceress</other_info_on_name>
    <other_info_on_name type="fna_id">119943</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from tapered, white, tuberlike, horizontal rhizomes.</text>
      <biological_entity id="o7402" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7403" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="tapered" value_original="tapered" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s0" value="tuberlike" value_original="tuberlike" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o7402" id="r1047" name="from" negation="false" src="d0_s0" to="o7403" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple, slender.</text>
      <biological_entity id="o7404" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in 2 whorls;</text>
      <biological_entity id="o7405" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7406" name="whorl" name_original="whorls" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7405" id="r1048" name="in" negation="false" src="d0_s2" to="o7406" />
    </statement>
    <statement id="d0_s3">
      <text>proximal blades oblong-oblanceolate, base attenuate, margins entire, apex acuminate;</text>
      <biological_entity constraint="proximal" id="o7407" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
      </biological_entity>
      <biological_entity id="o7408" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o7409" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7410" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal blades ovate, rounded at base, apex acuminate.</text>
      <biological_entity constraint="distal" id="o7411" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character constraint="at base" constraintid="o7412" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7412" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o7413" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, umbellate, (2–) 3–9-flowered, sessile.</text>
      <biological_entity id="o7414" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(2-)3-9-flowered" value_original="(2-)3-9-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals caducous, 6, similar, recurved, distinct;</text>
      <biological_entity id="o7415" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7416" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 6;</text>
      <biological_entity id="o7417" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7418" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers versatile, oblong, extrorse;</text>
      <biological_entity id="o7419" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7420" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s8" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s8" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary superior, 3-locular;</text>
      <biological_entity id="o7421" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7422" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3, recurved, distinct to base, often purple;</text>
      <biological_entity id="o7423" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7424" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character constraint="to base" constraintid="o7425" is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="often" name="coloration_or_density" notes="" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o7425" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel declined or spreading in flower, erect in fruit.</text>
      <biological_entity id="o7426" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7427" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="declined" value_original="declined" />
        <character constraint="in flower" constraintid="o7428" is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character constraint="in fruit" constraintid="o7429" is_modifier="false" name="orientation" notes="" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o7428" name="flower" name_original="flower" src="d0_s11" type="structure" />
      <biological_entity id="o7429" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits baccate, dark purple to black, globose.</text>
      <biological_entity id="o7430" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="baccate" value_original="baccate" />
        <character char_type="range_value" from="dark purple" name="coloration" src="d0_s12" to="black" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds few, shiny brown, subglobose.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 7.</text>
      <biological_entity id="o7431" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="few" value_original="few" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
      </biological_entity>
      <biological_entity constraint="x" id="o7432" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Indian cucumber-root</other_name>
  <other_name type="common_name">concombre sauvage</other_name>
  <other_name type="common_name">jarnotte</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Medeola was removed from its long association with Trillium and Paris (tribe Parideae) when A. L. Takhtajan (1987, 1997) created the monotypic Medeolaceae, placing it next to a strictly defined Liliaceae. M. N. Tamura (1998c) included Medeola and Clintonia in the tribe Medeoloideae within a narrowly defined Liliaceae. The association of Medeola and Clintonia is supported by molecular analysis (K. Hayashi et al. 1998, 2001; T. B. Patterson and T. J. Givnish 1998) as well as morphology (R. Y. Berg 1962; M. Takahashi 1984; F. H. Utech 1978e), embryology (R. Y. Berg 1962b), and cytology (R. M. Stewart and R. Bamford 1942; M. N. Tamura 1995).</discussion>
  <references>
    <reference>Bell, A. D. 1974. Rhizome organization in relation to vegetative spread in Medeola virginiana. J. Arnold Arbor. 55: 458–468.  </reference>
    <reference>Berg, R. Y. 1962. Morphology and taxonomic position of Medeola, Liliaceae. Skr. Norske Vidensk.-Akad. Oslo, Mat.-Naturvidensk. Kl., n. s. 3: 1–56.  </reference>
    <reference>Berg, R. Y. 1962b. Contribution to the comparative embryology of the Liliaceae: Scoliopus, Trillium, Paris and Medeola. Skr. Norske Vidensk.-Akad. Oslo, Mat.-Naturvidensk. Kl., n. s. 4: 1–64.  </reference>
    <reference>Utech, F. H. 1978e. Floral vascular anatomy of Medeola virginiana L. (Liliaceae-Parideae = Trilliaceae) and tribal note. Ann. Carnegie Mus. 47: 13–28.</reference>
  </references>
  
</bio:treatment>