<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Eric Hágsater</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">613</other_info_on_meta>
    <other_info_on_meta type="treatment_page">609</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Kunth" date="1815" rank="tribe">Epidendreae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Laeliinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">EPIDENDRUM</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1246. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe epidendreae;subtribe laeliinae;genus epidendrum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek epi -, on, and dendron, tree, alluding to the epiphytic habit</other_info_on_name>
    <other_info_on_name type="fna_id">111824</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Amphiglottis Salisbury" date="unknown" rank="genus">Amphiglottis</taxon_name>
    <taxon_hierarchy>genus Amphiglottis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Larnandra Rafinesque" date="unknown" rank="genus">Larnandra</taxon_name>
    <taxon_hierarchy>genus Larnandra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nyctosma Rafinesque" date="unknown" rank="genus">Nyctosma</taxon_name>
    <taxon_hierarchy>genus Nyctosma;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Spathiger Small" date="unknown" rank="genus">Spathiger</taxon_name>
    <taxon_hierarchy>genus Spathiger;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Tritelandra Rafinesque" date="unknown" rank="genus">Tritelandra</taxon_name>
    <taxon_hierarchy>genus Tritelandra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, epiphytic, rarely lithophytic, cespitose.</text>
      <biological_entity id="o10277" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" modifier="rarely" name="habitat" src="d0_s0" value="lithophytic" value_original="lithophytic" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fleshy, glabrous.</text>
      <biological_entity id="o10278" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, repent, or hanging, canelike, simple or branching.</text>
      <biological_entity id="o10279" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="repent" value_original="repent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="hanging" value_original="hanging" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cane-like" value_original="canelike" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
      <biological_entity id="o10280" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole tubular, sheathing, articulate;</text>
      <biological_entity id="o10281" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="articulate" value_original="articulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to elliptic or lanceolate, fleshy-leathery.</text>
      <biological_entity id="o10282" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic or lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy-leathery" value_original="fleshy-leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal [lateral or basal], racemose to nearly corymbose or distichous, producing flowers only once, or during several years from same or new racemes from old axis;</text>
      <biological_entity id="o10283" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="racemose" name="arrangement" src="d0_s6" to="nearly corymbose or distichous" />
      </biological_entity>
      <biological_entity id="o10284" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10285" name="year" name_original="years" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o10286" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o10287" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="old" value_original="old" />
      </biological_entity>
      <relation from="o10283" id="r1466" modifier="only once; once" name="producing" negation="false" src="d0_s6" to="o10284" />
      <relation from="o10283" id="r1467" name="during" negation="false" src="d0_s6" to="o10285" />
      <relation from="o10285" id="r1468" name="from" negation="false" src="d0_s6" to="o10286" />
      <relation from="o10286" id="r1469" name="from" negation="false" src="d0_s6" to="o10287" />
    </statement>
    <statement id="d0_s7">
      <text>flowers pedicellate or not.</text>
      <biological_entity id="o10288" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
        <character name="architecture" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers resupinate or not, if not then distichous or spirally arranged, simultaneous or successive;</text>
      <biological_entity id="o10289" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
        <character name="orientation" src="d0_s8" value="not" value_original="not" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s8" value="distichous" value_original="distichous" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="development" src="d0_s8" value="simultaneous" value_original="simultaneous" />
        <character is_modifier="false" name="development" src="d0_s8" value="successive" value_original="successive" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals nearly equal;</text>
      <biological_entity id="o10290" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip adnate to column throughout, forming nectary tube, tube penetrating ovary, occasionally producing nectar;</text>
      <biological_entity id="o10291" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character constraint="to column" constraintid="o10292" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o10292" name="column" name_original="column" src="d0_s10" type="structure" />
      <biological_entity constraint="nectary" id="o10293" name="tube" name_original="tube" src="d0_s10" type="structure" />
      <biological_entity id="o10294" name="tube" name_original="tube" src="d0_s10" type="structure" />
      <biological_entity id="o10295" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <biological_entity id="o10296" name="nectar" name_original="nectar" src="d0_s10" type="structure" />
      <relation from="o10291" id="r1470" name="forming" negation="false" src="d0_s10" to="o10293" />
      <relation from="o10294" id="r1471" name="penetrating" negation="false" src="d0_s10" to="o10295" />
      <relation from="o10294" id="r1472" modifier="occasionally" name="producing" negation="false" src="d0_s10" to="o10296" />
    </statement>
    <statement id="d0_s11">
      <text>column straight or slightly arched;</text>
      <biological_entity id="o10297" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation_or_shape" src="d0_s11" value="arched" value_original="arched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>clinandrium hood partly covering anther;</text>
      <biological_entity constraint="clinandrium" id="o10298" name="hood" name_original="hood" src="d0_s12" type="structure" />
      <biological_entity id="o10299" name="anther" name_original="anther" src="d0_s12" type="structure" />
      <relation from="o10298" id="r1473" modifier="partly" name="covering" negation="false" src="d0_s12" to="o10299" />
    </statement>
    <statement id="d0_s13">
      <text>anther 4-celled;</text>
      <biological_entity id="o10300" name="anther" name_original="anther" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="4-celled" value_original="4-celled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollinia 4, obovoid, laterally compressed, nearly equal;</text>
      <biological_entity id="o10301" name="pollinium" name_original="pollinia" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s14" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>rostellum slit, producing semiliquid viscidium attached to caudicles of pollinarium.</text>
      <biological_entity constraint="rostellum" id="o10302" name="slit" name_original="slit" src="d0_s15" type="structure">
        <character constraint="to caudicles" constraintid="o10304" is_modifier="false" name="fixation" src="d0_s15" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o10303" name="viscidium" name_original="viscidium" src="d0_s15" type="structure">
        <character is_modifier="true" name="texture" src="d0_s15" value="semiliquid" value_original="semiliquid" />
      </biological_entity>
      <biological_entity id="o10304" name="caudicle" name_original="caudicles" src="d0_s15" type="structure" />
      <biological_entity id="o10305" name="pollinarium" name_original="pollinarium" src="d0_s15" type="structure" />
      <relation from="o10302" id="r1474" name="producing" negation="false" src="d0_s15" to="o10303" />
      <relation from="o10304" id="r1475" name="part_of" negation="false" src="d0_s15" to="o10305" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, ellipsoid to subglobose, sometimes with pedicel or beak, 3-ribbed, 1-locular.</text>
      <biological_entity constraint="fruits" id="o10306" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s16" to="subglobose" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s16" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <biological_entity id="o10307" name="pedicel" name_original="pedicel" src="d0_s16" type="structure" />
      <biological_entity id="o10308" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o10306" id="r1476" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o10307" />
      <relation from="o10306" id="r1477" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o10308" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical regions, Western Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical regions" establishment_means="native" />
        <character name="distribution" value="Western Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <discussion>Species over 1000 (7 in the flora).</discussion>
  <discussion>Outside the flora area Epidendrum is highly varied (E. Hágsater 1984), sometimes producing pseudobulbs or thickened stems, semiterete to membranaceous leaf blades, and apical, lateral, or basal, racemose to nearly corymbose to paniculate inflorescences. The lip of the flower is adnate to the column, forming a nectary tube, but it rarely produces nectar. The lip is exceptionally distinct and free. The rostellum is always slit, producing a transparent or whitish semiliquid viscidium. The pollinarium usually has 4 pollinia, sometimes with 2 very reduced, rarely with only 2.</discussion>
  <discussion>The Florida species were segregated into 2 genera, Amphiglottis and Spathiger (J. K. Small 1933). Later authors have recognized only Epidendrum.</discussion>
  <references>
    <reference>  Ames, O., F. T. Hubbard, and C. Schweinfurth. 1936. The Genus Epidendrum in the United States and Middle America. Cambridge, Mass.  </reference>
    <reference>Hágsater, E. 1984. Towards an understanding of the genus Epidendrum. In: K. W. Tan, ed. 1984. Proceedings of the Eleventh World Orchid Conference: March 1984, Miami, Florida, U.S.A. Miami. Pp. 195–201. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants creeping or hanging, ± branching; inflorescences distichous.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants cespitose, aerial stems erect, unbranched; inflorescences nearly racemose or nearly corymbose.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants hanging, main stem greater than 40 cm; inflorescences distichous-imbricate; sepals greater than 12 mm.</description>
      <determination>1 Epidendrum acunae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants creeping, main stem less than 20 cm; inflorescences distichous-elongate or distichous-imbricate; sepals less than 8 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences distichous-elongate, equal to or greater than 5 cm; flowers mostly more than 5, sepals 5 mm.</description>
      <determination>6 Epidendrum rigidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences distichous-imbricate, less than 3 cm; flowers usually 3, sepals 3 mm.</description>
      <determination>7 Epidendrum strobiliferum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences 3–50 cm (excluding flowers), racemose or compound racemose; sepals 5–11 mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences less than 3 cm (excluding flowers), nearly corymbose; sepals equal to or greater than 12 mm.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences racemose; flowers spread out along apical 1/2 of inflorescence; sepals 6–11 mm.</description>
      <determination>4 Epidendrum magnoliae</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences racemose, compound racemose with time; flowers in compact short racemes at end or from nodes of long peduncle; sepals 5.5–7.5 mm.</description>
      <determination>2 Epidendrum amphistomum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals 60 mm; flowers yellowish and white, flowering successively, 1–2 at a time.</description>
      <determination>5 Epidendrum nocturnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals 12–15 mm; flowers green, flowering simultaneously, 4–14.</description>
      <determination>3 Epidendrum floridense</determination>
    </key_statement>
  </key>
</bio:treatment>