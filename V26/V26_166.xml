<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="Ownbey &amp; M. Peck" date="1954" rank="species">indecorus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>7: 191. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species indecorus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101472</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems stout, not branching, 16–22 cm, not bearing bulblets.</text>
      <biological_entity id="o28564" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28565" name="bulblet" name_original="bulblets" src="d0_s0" type="structure" />
      <relation from="o28564" id="r3858" name="bearing" negation="true" src="d0_s0" to="o28565" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 2–25 dm × 8–12 mm;</text>
      <biological_entity id="o28566" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o28567" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s1" to="25" to_unit="dm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s1" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline usually present.</text>
      <biological_entity id="o28568" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o28569" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2–6-flowered.</text>
      <biological_entity id="o28570" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers ± erect;</text>
      <biological_entity id="o28571" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o28572" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals broadly lanceolate, somewhat shorter than petals;</text>
      <biological_entity id="o28573" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character constraint="than petals" constraintid="o28574" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="somewhat shorter" value_original="somewhat shorter" />
      </biological_entity>
      <biological_entity id="o28574" name="petal" name_original="petals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>petals bright lavender, broadly obovate, adaxial surface glabrous or with short, purple hairs distal to gland, apex margins erose;</text>
      <biological_entity id="o28575" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bright lavender" value_original="bright lavender" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28576" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="with short , purple hairs" />
      </biological_entity>
      <biological_entity id="o28577" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
        <character constraint="to gland" constraintid="o28578" is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o28578" name="gland" name_original="gland" src="d0_s7" type="structure" />
      <biological_entity constraint="apex" id="o28579" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o28576" id="r3859" name="with" negation="false" src="d0_s7" to="o28577" />
    </statement>
    <statement id="d0_s8">
      <text>glands slightly depressed, surrounded proximally by minutely denticulate membrane;</text>
      <biological_entity id="o28580" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="depressed" value_original="depressed" />
        <character constraint="by membrane" constraintid="o28581" is_modifier="false" name="position_relational" src="d0_s8" value="surrounded" value_original="surrounded" />
      </biological_entity>
      <biological_entity id="o28581" name="membrane" name_original="membrane" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="minutely" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments longer than anthers;</text>
      <biological_entity id="o28582" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="than anthers" constraintid="o28583" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o28583" name="anther" name_original="anthers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers oblong, apex acute to obtuse.</text>
      <biological_entity id="o28584" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o28585" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules nodding, narrowly 3-winged, ovoid, 1.5–2 cm.</text>
      <biological_entity id="o28586" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="nodding" value_original="nodding" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s11" value="3-winged" value_original="3-winged" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds unknown.</text>
      <biological_entity id="o28587" name="seed" name_original="seeds" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Calochortus indecorus was collected from the western slope of Sexton Mountain, northeastern Josephine County. Endemic to that area, the taxon is now presumed extinct.</discussion>
  
</bio:treatment>