<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1818" rank="genus">clintonia</taxon_name>
    <taxon_name authority="(Michaux) Morong" date="1894" rank="species">umbellulata</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>5: 114. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus clintonia;species umbellulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101530</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Convallaria</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">umbellulata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 202. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Convallaria;species umbellulata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clintonia</taxon_name>
    <taxon_name authority="Harned" date="unknown" rank="species">allegheniensis</taxon_name>
    <taxon_hierarchy>genus Clintonia;species allegheniensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xeniatrum</taxon_name>
    <taxon_name authority="(Michaux) Small" date="unknown" rank="species">umbellulatum</taxon_name>
    <taxon_hierarchy>genus Xeniatrum;species umbellulatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2.7–6 dm;</text>
      <biological_entity id="o5910" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.7" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short, thick.</text>
      <biological_entity id="o5911" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves 3–4;</text>
      <biological_entity constraint="cauline" id="o5912" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade dark green, oblong to obovate-elliptic, 18–30 × 4.5–8 cm.</text>
      <biological_entity id="o5913" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="obovate-elliptic" />
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences in terminal clusters, 10–25 (–30) -flowered;</text>
      <biological_entity id="o5914" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="10-25(-30)-flowered" value_original="10-25(-30)-flowered" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5915" name="cluster" name_original="clusters" src="d0_s4" type="structure" />
      <relation from="o5914" id="r853" name="in" negation="false" src="d0_s4" to="o5915" />
    </statement>
    <statement id="d0_s5">
      <text>bract 1, foliaceous.</text>
      <biological_entity id="o5916" name="bract" name_original="bract" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals white to greenish white, often spotted purplish brown or green distally, ovoid-obovate, 5.5–8 × 2.7–4 mm;</text>
      <biological_entity id="o5917" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5918" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="greenish white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="spotted purplish" value_original="spotted purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-obovate" value_original="ovoid-obovate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 5.5–7 mm;</text>
      <biological_entity id="o5919" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5920" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers short-oblong, 3.5–5.5 mm.</text>
      <biological_entity id="o5921" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5922" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="short-oblong" value_original="short-oblong" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Berries black, globose to ellipsoid, 2–4-seeded, 6–8 mm.</text>
      <biological_entity id="o5923" name="berry" name_original="berries" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="ellipsoid" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="2-4-seeded" value_original="2-4-seeded" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 3–4 mm. 2n = 28.</text>
      <biological_entity id="o5924" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5925" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early May–late Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jun" from="early May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich cove hardwood forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich cove hardwood forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., Ky., Md., N.Y., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">White clintonia</other_name>
  <other_name type="common_name">speckled wood-lily</other_name>
  <discussion>Clintonia umbellulata and C. borealis are allopatric in the southern Appalachians (P. P. Gunther 1972). A putative hybrid between the two, C. allegheniensis Harned (J. E. Harned 1931) is not recognized here and is regarded as a mere fruit-color variant of C. umbellulata (F. H. Utech 1973). Isozyme evidence supports this conclusion (M. T. Blain 1997).</discussion>
  
</bio:treatment>