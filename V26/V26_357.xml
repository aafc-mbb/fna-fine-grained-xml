<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="unknown" rank="genus">maianthemum</taxon_name>
    <taxon_name authority="(Alph. Wood) A. Nelson &amp; J. F. Macbride" date="1916" rank="species">dilatatum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>61: 30. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus maianthemum;species dilatatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101758</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maianthemum</taxon_name>
    <taxon_name authority="(Linnaeus) F. W. Schmidt" date="unknown" rank="species">bifolium</taxon_name>
    <taxon_name authority="Alph. Wood" date="unknown" rank="variety">dilatatum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>20: 174. 1868 (as Majanthemum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Maianthemum;species bifolium;variety dilatatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maianthemum</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Nakai" date="unknown" rank="species">bifolium</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Jepson" date="unknown" rank="variety">kamtschaticum</taxon_name>
    <taxon_hierarchy>genus Maianthemum;species bifolium;variety kamtschaticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maianthemum</taxon_name>
    <taxon_name authority="(Alph. Wood) Greene" date="unknown" rank="species">kamtschaticum</taxon_name>
    <taxon_hierarchy>genus Maianthemum;species kamtschaticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Unifolium</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Gorman" date="unknown" rank="species">dilatatum</taxon_name>
    <taxon_hierarchy>genus Unifolium;species dilatatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Unifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">kamtschaticum</taxon_name>
    <taxon_hierarchy>genus Unifolium;species kamtschaticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, 20–45 cm.</text>
      <biological_entity id="o32553" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes sympodial, proliferatively branching, units 8–20 cm × 1–1.5 mm, roots restricted to nodes.</text>
      <biological_entity id="o32554" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sympodial" value_original="sympodial" />
        <character is_modifier="false" modifier="proliferatively" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o32555" name="unit" name_original="units" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32556" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o32557" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o32556" id="r4371" name="restricted to" negation="false" src="d0_s1" to="o32557" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, 1.5–3.5 dm × 2–4 mm.</text>
      <biological_entity id="o32558" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="length" src="d0_s2" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves solitary on sterile shoots, 2–3 on fertile shoots, petiolate;</text>
      <biological_entity id="o32559" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="on shoots" constraintid="o32560" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o32560" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
        <character char_type="range_value" constraint="on shoots" constraintid="o32561" from="2" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o32561" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade cordate, 6–10 × 5–8 cm;</text>
      <biological_entity id="o32562" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base lobed, with deep sinus;</text>
      <biological_entity id="o32563" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o32564" name="sinus" name_original="sinus" src="d0_s5" type="structure">
        <character is_modifier="true" name="depth" src="d0_s5" value="deep" value_original="deep" />
      </biological_entity>
      <relation from="o32563" id="r4372" name="with" negation="false" src="d0_s5" to="o32564" />
    </statement>
    <statement id="d0_s6">
      <text>apex sharply acute;</text>
      <biological_entity id="o32565" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal leaves short-petiolate, blade triangular to cordate, petiole 4–7 cm;</text>
      <biological_entity constraint="proximal" id="o32566" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
      <biological_entity id="o32567" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s7" to="cordate" />
      </biological_entity>
      <biological_entity id="o32568" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal leaves petiolate, blade deeply cordate, petiole 7–10 cm.</text>
      <biological_entity constraint="distal" id="o32569" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o32570" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o32571" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences racemose, complex, 15–40-flowered.</text>
      <biological_entity id="o32572" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="complex" value_original="complex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="15-40-flowered" value_original="15-40-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers (1–) 3 (–4) per node, 2-merous;</text>
      <biological_entity id="o32573" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s10" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="4" />
        <character constraint="per node" constraintid="o32574" name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="2-merous" value_original="2-merous" />
      </biological_entity>
      <biological_entity id="o32574" name="node" name_original="node" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>tepals conspicuous, 2–3.2 × 1.5 mm;</text>
      <biological_entity id="o32575" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="conspicuous" value_original="conspicuous" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3.2" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 1.5 mm;</text>
      <biological_entity id="o32576" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.2–0.4 mm;</text>
      <biological_entity id="o32577" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary globose, 0.8–1 mm wide;</text>
      <biological_entity id="o32578" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 0.4–0.5 mm;</text>
      <biological_entity id="o32579" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma distinctly 2-lobed;</text>
      <biological_entity id="o32580" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 3–5 × 0.2–0.4 mm.</text>
      <biological_entity id="o32581" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s17" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries green mottled with red when young, maturing to deep translucent red, globose, 4–6 mm diam.</text>
      <biological_entity id="o32582" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s18" value="green mottled with red" value_original="green mottled with red" />
        <character is_modifier="false" name="life_cycle" src="d0_s18" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="depth" src="d0_s18" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="translucent red" value_original="translucent red" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s18" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1–2, globose, 2–3 mm. 2n = 36.</text>
      <biological_entity id="o32583" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s19" to="2" />
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s19" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32584" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abundant in coniferous and deciduous forests, especially in forest margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" modifier="abundant in" />
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="forest margins" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska, Calif., Idaho, Oreg., Wash.; Asia (Kamtchatka peninsula in e Russia to Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Asia (Kamtchatka peninsula in e Russia to Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">May-lily</other_name>
  <other_name type="common_name">false lily-of-the-valley</other_name>
  <discussion>Variation in the gross morphology, karyology, and ecology of the North American populations has been documented (S. Kawano et al. 1971) and compared with that of disjunct populations in Japan (S. Kawano et al. 1968b).</discussion>
  
</bio:treatment>