<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">361</other_info_on_meta>
    <other_info_on_meta type="treatment_page">360</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1901" rank="species">pruinosum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 578. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species pruinosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101918</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">brayi</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species brayi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">bushii</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species bushii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">helleri</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species helleri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">varians</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species varians;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, ashy to bronze olive when dry, to 2.5 dm, not glaucous.</text>
      <biological_entity id="o12811" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="ashy" modifier="when dry" name="coloration" src="d0_s0" to="bronze olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, with 1–2 nodes, 1–2.8 mm wide, scabrous at least apically, margins entire to denticulate apically, similar in color and texture to stem body;</text>
      <biological_entity id="o12812" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s1" to="2.8" to_unit="mm" />
        <character is_modifier="false" modifier="at-least apically; apically" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12813" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="2" />
      </biological_entity>
      <biological_entity constraint="stem" id="o12815" name="body" name_original="body" src="d0_s1" type="structure" />
      <relation from="o12812" id="r1784" name="with" negation="false" src="d0_s1" to="o12813" />
      <relation from="o12814" id="r1785" name="to" negation="false" src="d0_s1" to="o12815" />
    </statement>
    <statement id="d0_s2">
      <text>first internode 5.3–12.5 cm, equaling or shorter than leaves;</text>
      <biological_entity id="o12814" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="entire" modifier="apically" name="shape" src="d0_s1" to="denticulate" />
      </biological_entity>
      <biological_entity id="o12816" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="5.3" from_unit="cm" name="some_measurement" src="d0_s2" to="12.5" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
        <character constraint="than leaves" constraintid="o12817" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12817" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>distalmost node with 2–3 branches.</text>
      <biological_entity constraint="distalmost" id="o12818" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o12819" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o12818" id="r1786" name="with" negation="false" src="d0_s3" to="o12819" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades usually scabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o12820" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12821" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o12822" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o12822" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o12823" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green, obviously wider than supporting branch, scabrous, keels denticulate or entire;</text>
      <biological_entity id="o12824" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than supporting branch , scabrous , keels" constraintid="o12825, o12826" is_modifier="false" name="width" src="d0_s6" value="obviously wider" value_original="obviously wider" />
      </biological_entity>
      <biological_entity id="o12825" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12826" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer 18.6–26.5 mm, 2.5–5.5 mm longer than inner, tapering evenly towards apex, margins basally connate 2.5–3.9 mm;</text>
      <biological_entity constraint="outer" id="o12827" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="18.6" from_unit="mm" name="some_measurement" src="d0_s7" to="26.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o12828" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o12829" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12828" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o12829" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o12830" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved or straight, hyaline margins 0.2–0.4 mm wide, apex acuminate to acute, ending 0–1.5 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o12831" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o12832" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o12833" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12834" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="acute" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o12835" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <relation from="o12831" id="r1787" name="with" negation="false" src="d0_s8" to="o12832" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals bluish violet to purplish blue, bases yellow;</text>
      <biological_entity id="o12836" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12837" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="bluish violet" name="coloration" src="d0_s9" to="purplish blue" />
      </biological_entity>
      <biological_entity id="o12838" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer tepals 8.3–10.5 mm, apex rounded to truncate or emarginate, aristate;</text>
      <biological_entity id="o12839" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o12840" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="8.3" from_unit="mm" name="some_measurement" src="d0_s10" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12841" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded to truncate" value_original="rounded to truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ± entirely, slightly stipitate-glandular basally;</text>
      <biological_entity id="o12842" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12843" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="slightly; basally" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o12844" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12845" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o12846" name="foliage" name_original="foliage" src="d0_s12" type="structure" />
      <relation from="o12845" id="r1788" name="to" negation="false" src="d0_s12" to="o12846" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules tan to light-brown, ± globose, 3–5 mm;</text>
      <biological_entity id="o12847" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="light-brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel ascending to spreading.</text>
      <biological_entity id="o12848" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s14" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds globose to obconic, lacking obvious depression, rugulose.</text>
      <biological_entity id="o12849" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 32.</text>
      <biological_entity id="o12850" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s15" value="obvious" value_original="obvious" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12851" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Kans., Nebr., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Sisyrinchium pruinosum can be expected also in Louisiana, Missouri, and Mississippi.</discussion>
  
</bio:treatment>