<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">587</other_info_on_meta>
    <other_info_on_meta type="treatment_page">590</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Neottieae</taxon_name>
    <taxon_name authority="Bentham" date="" rank="subtribe">Limodorinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">listera</taxon_name>
    <taxon_name authority="Morong" date="1893" rank="species">borealis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>20: 31. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe neottieae;subtribe limodorinae;genus listera;species borealis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101752</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neottia</taxon_name>
    <taxon_name authority="(Morong) Szlachetko" date="unknown" rank="species">borealis</taxon_name>
    <taxon_hierarchy>genus Neottia;species borealis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophrys</taxon_name>
    <taxon_name authority="(Morong) Rydberg" date="unknown" rank="species">borealis</taxon_name>
    <taxon_hierarchy>genus Ophrys;species borealis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–26 cm.</text>
      <biological_entity id="o3186" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="26" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green, slender to stout, slightly 4-angled, succulent, glabrous.</text>
      <biological_entity id="o3187" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="stout" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade green to dark bluish green, elliptic, ovate-elliptic, or lanceolate, 1.3–6 × 0.7–3 cm, apex obtuse to rounded.</text>
      <biological_entity id="o3188" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3189" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="dark bluish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3190" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 5–20-flowered, lax, 20–90 mm;</text>
      <biological_entity id="o3191" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="5-20-flowered" value_original="5-20-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="90" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral bracts lanceolate, ovate, or oblong, 1–3 × 0.5–1 mm, apex obtuse;</text>
      <biological_entity constraint="floral" id="o3192" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3193" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle and rachis glandular-pubescent.</text>
      <biological_entity id="o3194" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o3195" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pale green, yellowish green, or bluish green with veins darker green;</text>
      <biological_entity id="o3196" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character constraint="with veins" constraintid="o3197" is_modifier="false" name="coloration" src="d0_s6" value="bluish green" value_original="bluish green" />
      </biological_entity>
      <biological_entity id="o3197" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="darker green" value_original="darker green" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel filiform, 3.5–7 mm, glandular-pubescent;</text>
      <biological_entity id="o3198" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals and petals strongly reflexed away from lip and column;</text>
      <biological_entity id="o3199" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="away-from column" constraintid="o3202" is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o3200" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character constraint="away-from column" constraintid="o3202" is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o3201" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o3202" name="column" name_original="column" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal elliptic-lanceolate to linear-elliptic, 4–6 × 1.5–2.2 mm, apex obtuse to rounded;</text>
      <biological_entity constraint="dorsal" id="o3203" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s9" to="linear-elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3204" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals linear-elliptic to oblong-lanceolate, falcate, 4.5–7 × 1.4–2.3 mm, apex obtuse to rounded;</text>
      <biological_entity constraint="lateral" id="o3205" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear-elliptic" name="shape" src="d0_s10" to="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3206" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals linear to linear-oblong, falcate, 4.5–5 × 0.7–1.5 mm, apex obtuse;</text>
      <biological_entity id="o3207" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3208" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip oblong, slightly narrowed in center, base with broadly rounded and bluntly angled divergent auricles, apex slightly dilated, cleft into 2 oblong or semiorbiculate lobes, with apicule in sinus;</text>
      <biological_entity id="o3209" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character constraint="in center" constraintid="o3210" is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o3210" name="center" name_original="center" src="d0_s12" type="structure" />
      <biological_entity id="o3211" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o3212" name="auricle" name_original="auricles" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="bluntly" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o3213" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="dilated" value_original="dilated" />
        <character constraint="into lobes" constraintid="o3214" is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o3214" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s12" value="semiorbiculate" value_original="semiorbiculate" />
      </biological_entity>
      <biological_entity id="o3215" name="apicule" name_original="apicule" src="d0_s12" type="structure" />
      <biological_entity id="o3216" name="sinus" name_original="sinus" src="d0_s12" type="structure" />
      <relation from="o3211" id="r442" name="with" negation="false" src="d0_s12" to="o3212" />
      <relation from="o3213" id="r443" name="with" negation="false" src="d0_s12" to="o3215" />
      <relation from="o3215" id="r444" name="in" negation="false" src="d0_s12" to="o3216" />
    </statement>
    <statement id="d0_s13">
      <text>disc 3-veined, lateral-veins branched and purplish, base darker green, with ridge in center, thickened along center, 7–12 × 4.2–6.5 mm, margins ciliate;</text>
      <biological_entity id="o3217" name="disc" name_original="disc" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o3218" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="branched" value_original="branched" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o3219" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="darker green" value_original="darker green" />
        <character constraint="along center" constraintid="o3222" is_modifier="false" name="size_or_width" notes="" src="d0_s13" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="width" notes="" src="d0_s13" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3220" name="ridge" name_original="ridge" src="d0_s13" type="structure" />
      <biological_entity id="o3221" name="center" name_original="center" src="d0_s13" type="structure" />
      <biological_entity id="o3222" name="center" name_original="center" src="d0_s13" type="structure" />
      <biological_entity id="o3223" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o3219" id="r445" name="with" negation="false" src="d0_s13" to="o3220" />
      <relation from="o3220" id="r446" name="in" negation="false" src="d0_s13" to="o3221" />
    </statement>
    <statement id="d0_s14">
      <text>column arcuate, stout, 3–4 × 1 mm.</text>
      <biological_entity id="o3224" name="column" name_original="column" src="d0_s14" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s14" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ellipsoid, 8 × 5 mm. 2n = 56.</text>
      <biological_entity id="o3225" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s15" unit="mm" value="8" value_original="8" />
        <character name="width" src="d0_s15" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3226" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In moist, rich humus of mossy coniferous or mixed hardwood forests, swamps, often along cold streams fed by melting snow, prefers high acidic soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" modifier="in" constraint="of mossy coniferous or mixed hardwood forests" />
        <character name="habitat" value="rich humus" constraint="of mossy coniferous or mixed hardwood forests" />
        <character name="habitat" value="mossy coniferous" />
        <character name="habitat" value="mixed hardwood forests" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="cold streams" />
        <character name="habitat" value="melting snow" />
        <character name="habitat" value="high acidic soils" modifier="prefers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Colo., Idaho, Mont., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Northern twayblade</other_name>
  <discussion>In Japan Listera borealis is replaced by L. yatabei Makino, which is nearly identical except for short basal auricles. Listera borealis and L. auriculata are very similar in overall appearance; the ovaries and pedicels in L. borealis are glandular-pubescent, and in L. auriculata they are glabrous.</discussion>
  <discussion>Leaves occur three in a whorl in Listera borealis forma trifolia Lepage.</discussion>
  
</bio:treatment>