<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">canadense</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1195. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species canadense</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101341</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–4+, without rhizome, with or without basal bulbels, often clustered, ovoid, 1–2.5 × 0.6–3 cm;</text>
      <biological_entity id="o14829" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="4" upper_restricted="false" />
        <character is_modifier="false" modifier="often" name="arrangement_or_growth_form" notes="" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s0" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" notes="" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14830" name="bulbel" name_original="bulbels" src="d0_s0" type="structure" />
      <relation from="o14829" id="r2026" name="without rhizome , with or without" negation="false" src="d0_s0" to="o14830" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, brownish or grayish, reticulate, cells fine-meshed, open, fibrous;</text>
      <biological_entity constraint="outer" id="o14831" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="fine-meshed" value_original="fine-meshed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="open" value_original="open" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o14832" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o14831" id="r2027" name="enclosing" negation="false" src="d0_s1" to="o14832" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats whitish, cells vertically elongate, sometimes contorted, walls straight or ± sinuous.</text>
      <biological_entity constraint="inner" id="o14833" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o14834" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="vertically" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
      </biological_entity>
      <biological_entity id="o14835" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s2" value="sinuous" value_original="sinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, green at anthesis, 2–6, basally sheathing, sheaths extending less than 1/4 scape;</text>
      <biological_entity id="o14836" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o14837" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o14838" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s3" to="1/4" />
      </biological_entity>
      <relation from="o14837" id="r2028" name="extending" negation="false" src="d0_s3" to="o14838" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, channeled, not carinate, 20–50 cm × 1–7 mm, margins entire or denticulate, apex acute to obtuse.</text>
      <biological_entity id="o14839" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14840" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o14841" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, usually solitary, erect, terete, 10–60 cm × 1–5 mm.</text>
      <biological_entity id="o14842" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="60" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, loose, 0–60-flowered, hemispheric to globose, bulbils unknown or flowering pedicels replaced at least in part by bulbils;</text>
      <biological_entity id="o14843" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="0-60-flowered" value_original="0-60-flowered" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="globose" />
      </biological_entity>
      <biological_entity id="o14844" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
      <biological_entity id="o14845" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o14846" name="part" name_original="part" src="d0_s6" type="structure" />
      <biological_entity id="o14847" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
      <relation from="o14845" id="r2029" name="replaced" negation="false" src="d0_s6" to="o14846" />
      <relation from="o14845" id="r2030" name="by" negation="false" src="d0_s6" to="o14847" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 3–4, 3–7-veined, ovate to lanceolate, ± equal, apex acuminate, beakless.</text>
      <biological_entity constraint="spathe" id="o14848" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-7-veined" value_original="3-7-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o14849" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers urceolate-campanulate, 4–8 mm;</text>
      <biological_entity id="o14850" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="urceolate-campanulate" value_original="urceolate-campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect or spreading, white to pink or lavender, lanceolate to elliptic, ± equal, withering in fruit and exposing capsule, midribs somewhat thickened, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o14851" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink or lavender" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="elliptic" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit; in fruit and exposing capsule" constraintid="o14853, o14854" is_modifier="false" name="life_cycle" src="d0_s9" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o14852" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o14853" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o14854" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
      <biological_entity id="o14855" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o14856" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14857" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o14858" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o14859" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o14860" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary, when present, crestless;</text>
      <biological_entity id="o14861" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="when present" name="architecture" src="d0_s13" value="crestless" value_original="crestless" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style linear, ± equaling stamens;</text>
      <biological_entity id="o14862" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o14863" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate, unlobed or obscurely 3-lobed;</text>
      <biological_entity id="o14864" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s15" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pedicel 8–70 mm.</text>
      <biological_entity id="o14865" name="pedicel" name_original="pedicel" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s16" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seed-coat shining;</text>
      <biological_entity id="o14866" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>cells each with minute, central papilla.</text>
      <biological_entity id="o14867" name="cell" name_original="cells" src="d0_s18" type="structure" />
      <biological_entity constraint="central" id="o14868" name="papilla" name_original="papilla" src="d0_s18" type="structure">
        <character is_modifier="true" name="size" src="d0_s18" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o14867" id="r2031" name="with" negation="false" src="d0_s18" to="o14868" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Varieties 6 (6 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering pedicels mostly or entirely replaced by bulbils, rarely producing capsules or seeds.</description>
      <determination>3a Allium canadense var. canadense</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering pedicels all floriferous, bulbils almost unknown, producing capsules and seeds.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals usually white.</description>
      <determination>3f Allium canadense var. fraseri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals pinkish to lilac.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pedicels filiform; scape 10–30(–50) cm.</description>
      <determination>3b Allium canadense var. mobilense</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pedicels stouter, not filiform; scape 10–60 cm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Umbels 5–25-flowered; pedicels 3–4 times flowers; tepals thick.</description>
      <determination>3e Allium canadense var. ecristatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Umbels usually more than 25-flowered; pedicels 2–5 times flowers; tepals thin.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers fragrant; scape 15–30 cm; nc Texas to s Oklahoma.</description>
      <determination>3c Allium canadense var. hyacinthoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers odorless; scape 20–60 cm; n Oklahoma to n Arkansas, northward.</description>
      <determination>3d Allium canadense var. lavendulare</determination>
    </key_statement>
  </key>
</bio:treatment>