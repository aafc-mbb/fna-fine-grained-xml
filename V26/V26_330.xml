<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="treatment_page">195</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lilium</taxon_name>
    <taxon_name authority="Farwell" date="1915" rank="species">michiganense</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>42: 353. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lilium;species michiganense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101738</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">canadense</taxon_name>
    <taxon_name authority="(Farwell) B. Boivin &amp; Cody" date="unknown" rank="subspecies">michiganense</taxon_name>
    <taxon_hierarchy>genus Lilium;species canadense;subspecies michiganense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">canadense</taxon_name>
    <taxon_name authority="(Farwell) B. Boivin" date="unknown" rank="variety">umbelliferum</taxon_name>
    <taxon_hierarchy>genus Lilium;species canadense;variety umbelliferum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">michiganense</taxon_name>
    <taxon_name authority="Farwell" date="unknown" rank="variety">umbelliferum</taxon_name>
    <taxon_hierarchy>genus Lilium;species michiganense;variety umbelliferum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">michiganense</taxon_name>
    <taxon_name authority="Farwell" date="unknown" rank="variety">uniflorum</taxon_name>
    <taxon_hierarchy>genus Lilium;species michiganense;variety uniflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs usually yellowish, rhizomatous, unbranched, 1.6–5.8 × 4.9–14.1 cm, 0.3–0.5 times taller than long, 2 years’ growth evident as annual bulbs, scaleless sections between these 2.6–6.2 cm;</text>
      <biological_entity id="o30252" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s0" to="5.8" to_unit="cm" />
        <character char_type="range_value" from="4.9" from_unit="cm" name="width" src="d0_s0" to="14.1" to_unit="cm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="0.3-0.5 times taller than long" />
        <character name="quantity" src="d0_s0" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o30253" name="growth" name_original="growth" src="d0_s0" type="structure">
        <character constraint="as bulbs" constraintid="o30254" is_modifier="false" name="prominence" src="d0_s0" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o30254" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o30255" name="section" name_original="sections" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="scaleless" value_original="scaleless" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales unsegmented, longest 1–3 cm;</text>
      <biological_entity id="o30256" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stem roots present or absent.</text>
      <biological_entity constraint="stem" id="o30257" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems to 1.9 m.</text>
      <biological_entity id="o30258" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s3" to="1.9" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Buds rounded in cross-section.</text>
      <biological_entity id="o30259" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o30260" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o30260" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves in 4–12 whorls or partial whorls, 3–13 leaves per whorl, ± horizontal or ascending in sun, drooping at tips, 4.6–15.3 × 0.6–2.3 cm, 3.5–13.7 times longer than wide;</text>
      <biological_entity id="o30261" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o30263" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="partial" value_original="partial" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o30264" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character constraint="in sun" constraintid="o30266" is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character constraint="at tips" constraintid="o30267" is_modifier="false" name="orientation" notes="" src="d0_s5" value="drooping" value_original="drooping" />
        <character char_type="range_value" from="4.6" from_unit="cm" name="length" notes="" src="d0_s5" to="15.3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" notes="" src="d0_s5" to="2.3" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="3.5-13.7" value_original="3.5-13.7" />
      </biological_entity>
      <biological_entity id="o30265" name="whorl" name_original="whorl" src="d0_s5" type="structure" />
      <biological_entity id="o30266" name="sun" name_original="sun" src="d0_s5" type="structure" />
      <biological_entity id="o30267" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <relation from="o30261" id="r4078" name="in" negation="false" src="d0_s5" to="o30263" />
      <relation from="o30264" id="r4079" name="per" negation="false" src="d0_s5" to="o30265" />
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly elliptic, occasionally linear or slightly lanceolate, margins not undulate, apex acute, acuminate in distal leaves;</text>
      <biological_entity id="o30268" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o30269" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o30270" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character constraint="in distal leaves" constraintid="o30271" is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30271" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>principal and some secondary-veins impressed adaxially, veins and margins noticeably roughened abaxially with tiny ± deltoid epidermal spicules, especially on proximal leaves.</text>
      <biological_entity id="o30272" name="secondary-vein" name_original="secondary-veins" src="d0_s7" type="structure" constraint="principal">
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s7" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o30273" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character constraint="with epidermal, spicules" constraintid="o30275, o30276" is_modifier="false" modifier="noticeably" name="relief_or_texture" src="d0_s7" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o30274" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character constraint="with epidermal, spicules" constraintid="o30275, o30276" is_modifier="false" modifier="noticeably" name="relief_or_texture" src="d0_s7" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o30275" name="epidermal" name_original="epidermal" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="tiny" value_original="tiny" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity id="o30276" name="spicule" name_original="spicules" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="tiny" value_original="tiny" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30277" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o30273" id="r4080" modifier="especially" name="on" negation="false" src="d0_s7" to="o30277" />
      <relation from="o30274" id="r4081" modifier="especially" name="on" negation="false" src="d0_s7" to="o30277" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences racemose, 1–11-flowered.</text>
      <biological_entity id="o30278" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-11-flowered" value_original="1-11-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers ± pendent, not fragrant;</text>
      <biological_entity id="o30279" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s9" value="pendent" value_original="pendent" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s9" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth Turk’s-cap-shaped;</text>
      <biological_entity id="o30280" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="s-cap--shaped" value_original="s-cap--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals and petals reflexed 1/4–2/5 along length from base, yellow-orange or sometimes orange-yellow or orange proximally, red-orange distally, with maroon, often large spots, red-orange or occasionally red or orange-red abaxially, not distinctly clawed;</text>
      <biological_entity id="o30281" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character name="coloration" src="d0_s11" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s11" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="not distinctly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o30282" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="from base" constraintid="o30283" from="1/4" name="length" src="d0_s11" to="2/5" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s11" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="not distinctly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o30283" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o30284" name="spot" name_original="spots" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character is_modifier="true" modifier="often" name="size" src="d0_s11" value="large" value_original="large" />
      </biological_entity>
      <relation from="o30281" id="r4082" name="with" negation="false" src="d0_s11" to="o30284" />
      <relation from="o30282" id="r4083" name="with" negation="false" src="d0_s11" to="o30284" />
    </statement>
    <statement id="d0_s12">
      <text>sepals not ridged abaxially, 5.5–9.3 × 1.2–2 cm;</text>
      <biological_entity id="o30285" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; abaxially" name="shape" src="d0_s12" value="ridged" value_original="ridged" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="length" src="d0_s12" to="9.3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 5.3–9.1 × 1.5–2.2 cm;</text>
      <biological_entity id="o30286" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="5.3" from_unit="cm" name="length" src="d0_s13" to="9.1" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s13" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens moderately exserted;</text>
      <biological_entity id="o30287" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="moderately" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments parallel at first, then ± widely spreading, diverging 13°–23° from axis, pale yellow-green;</text>
      <biological_entity id="o30288" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character is_modifier="false" modifier="more or less widely" name="orientation" src="d0_s15" value="spreading" value_original="spreading" />
        <character constraint="from axis" constraintid="o30289" is_modifier="false" modifier="13°-23°" name="orientation" src="d0_s15" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="pale yellow-green" value_original="pale yellow-green" />
      </biological_entity>
      <biological_entity id="o30289" name="axis" name_original="axis" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers magenta or occasionally pink-magenta, 0.6–1.3 cm;</text>
      <biological_entity id="o30290" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="magenta" value_original="magenta" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s16" value="pink-magenta" value_original="pink-magenta" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s16" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pollen orange-rust, sometimes orange, rust, or rust-brown;</text>
      <biological_entity id="o30291" name="pollen" name_original="pollen" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="orange-rust" value_original="orange-rust" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s17" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="rust" value_original="rust" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="rust-brown" value_original="rust-brown" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="rust" value_original="rust" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="rust-brown" value_original="rust-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pistil 3.4–6.5 cm;</text>
      <biological_entity id="o30292" name="pistil" name_original="pistil" src="d0_s18" type="structure">
        <character char_type="range_value" from="3.4" from_unit="cm" name="some_measurement" src="d0_s18" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary 1.5–2.9 cm;</text>
      <biological_entity id="o30293" name="ovary" name_original="ovary" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s19" to="2.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>style red entirely or only distally;</text>
      <biological_entity id="o30294" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="entirely; entirely; only distally; distally" name="coloration" src="d0_s20" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>pedicel 11–22 cm.</text>
      <biological_entity id="o30295" name="pedicel" name_original="pedicel" src="d0_s21" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s21" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 2.8–5 × 1.5–2.6 cm, 1.4–2.8 times longer than wide.</text>
      <biological_entity id="o30296" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" src="d0_s22" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s22" to="2.6" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s22" value="1.4-2.8" value_original="1.4-2.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds not counted.</text>
    </statement>
    <statement id="d0_s24">
      <text>2n = 24.</text>
      <biological_entity id="o30297" name="seed" name_original="seeds" src="d0_s23" type="structure" />
      <biological_entity constraint="2n" id="o30298" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (mid Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="mid Jun-Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tallgrass prairies, streamsides, swamps and bottoms, moist woodland edges, lakeshores, ditches along roads and railways, often calciphilic</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tallgrass prairies" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bottoms" />
        <character name="habitat" value="moist woodland edges" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="roads" modifier="ditches along" />
        <character name="habitat" value="railways" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ark., Ill., Ind., Iowa, Kans., Ky., Mich., Minn., Mo., Nebr., N.Y., Ohio, Okla., S.Dak., Tenn., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Michigan lily</other_name>
  <discussion>B. Boivin and W. J. Cody (1956) proposed uniting Lilium michiganense and L. superbum as subspecies of L. canadense on the basis of overall similarity, though it is now well accepted that L. superbum does not belong there. There can be little doubt as to the close relationship between L. michiganense and L. canadense, however, and vegetatively the two are often indistinguishable. Hybrid intermediates occur across a wide band where the distributions meet in central Ohio and northwestern New York (R. M. Adams and W. J. Dress 1982). It would not be unreasonable to include L. grayi and treat them as subspecies, but floral differences among the three are comparable to those between other species in the genus.</discussion>
  <discussion>Farwell’s proposed varieties uniflorum and umbelliferum were described from young plants with single flowers and umbellate inflorescences respectively, but young plants with these characteristics are found throughout the range of this species.</discussion>
  <discussion>Plants examined from east-central Tennessee (e.g., Wayne and Coffee counties) that were previously referred to Lilium michiganense are L. superbum in some cases, in others L. canadense perhaps introgressed with L. michiganense.</discussion>
  <discussion>The Michigan lily often co-occurs in tallgrass prairies with Lilium philadelphicum; here as everywhere it usually blooms much later. However, it flowers earlier than L. canadense where their ranges are contiguous in Ohio (E. L. Braun 1967).</discussion>
  <discussion>Lilium michiganense is pollinated primarily by swallowtail butterflies; in the southern part of its range these include the pipevine [Battus philenor (Linnaeus), family Papilionidae]. Great spangled fritillaries [Speyeria cybele (Fabricius), family Nymphalidae] also visit this species and carry its pollen, though it is unlikely that this brushfooted butterfly is a major pollinator.</discussion>
  
</bio:treatment>