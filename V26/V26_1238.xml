<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">604</other_info_on_meta>
    <other_info_on_meta type="mention_page">605</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="treatment_page">606</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1825" rank="genus">hexalectris</taxon_name>
    <taxon_name authority="Correll" date="1941" rank="species">revoluta</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mus. Leafl.</publication_title>
      <place_in_publication>10: 19, fig. 2. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus hexalectris;species revoluta;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101665</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems tan, pale-pink, to pale brown-purple, 30–50 cm;</text>
      <biological_entity id="o19334" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="tan" value_original="tan" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s0" to="pale brown-purple" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s0" to="pale brown-purple" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheathing bracts 3–5.</text>
      <biological_entity id="o19335" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: floral bracts lanceolate to ovate, 5–15 × 3–4 mm.</text>
      <biological_entity id="o19336" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="floral" id="o19337" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 5–15 (–20), pedicellate, chasmogamous;</text>
      <biological_entity id="o19338" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="20" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals and petals strongly recurved, tan to pinkish brown;</text>
      <biological_entity id="o19339" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="pinkish brown" />
      </biological_entity>
      <biological_entity id="o19340" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="pinkish brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal oblongelliptic to lanceolate, 15–25 × 3–8 mm;</text>
      <biological_entity constraint="dorsal" id="o19341" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals obliquely elliptic to elliptic-lanceolate, 15–22 × 3–8 mm;</text>
      <biological_entity constraint="lateral" id="o19342" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="obliquely elliptic" name="shape" src="d0_s6" to="elliptic-lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals narrowly elliptic to obovate or oblanceolate, slightly falcate, 15–19 × 4.5–7.5 mm;</text>
      <biological_entity id="o19343" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s7" to="obovate or oblanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="19" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip broadly elliptic, deeply 3-lobed, 13–18 × 9–13 mm, fissure between lobes more than 3 mm deep, middle lobe white with purple, pale-yellow near base, obovate-cuneate, apex truncate to retuse, lateral lobes incurved, oblong, obtuse, 1/2–2/3 length of middle lobe;</text>
      <biological_entity id="o19344" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s8" to="18" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="between lobes" constraintid="o19346" id="o19345" name="fissure" name_original="fissure" src="d0_s8" type="structure" constraint_original="between  lobes, ">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" upper_restricted="false" />
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o19346" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity constraint="middle" id="o19347" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white with purple" value_original="white with purple" />
        <character constraint="near base" constraintid="o19348" is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="obovate-cuneate" value_original="obovate-cuneate" />
      </biological_entity>
      <biological_entity id="o19348" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o19349" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s8" to="retuse" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19350" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="1/2 length of middle lobe" name="length" src="d0_s8" to="2/3 length of middle lobe" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lamellae 3, 5, or 7, white to yellow, obscure;</text>
      <biological_entity id="o19351" name="lamella" name_original="lamellae" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character name="quantity" src="d0_s9" value="7" value_original="7" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="yellow" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>column purple abaxially, white-purple adaxially, 9–15 mm;</text>
      <biological_entity id="o19352" name="column" name_original="column" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="white-purple" value_original="white-purple" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anther yellow.</text>
      <biological_entity id="o19353" name="anther" name_original="anther" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 20 × 5 mm.</text>
      <biological_entity id="o19354" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="20" value_original="20" />
        <character name="width" src="d0_s12" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak-juniper-pinyon pine woodlands in leaf litter and humus, occasionally in rocky, open terrain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-juniper-pinyon pine woodlands" constraint="in leaf litter and humus" />
        <character name="habitat" value="leaf litter" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="open terrain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Curly coral-root</other_name>
  <other_name type="common_name">Correll’s cock’s-comb</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In Arizona Hexalectris revoluta grows in Cochise and Pima counties; in Texas, in the Guadalupe and Chisos mountains.</discussion>
  <discussion>Hexalectris revoluta has a less densely flowered raceme than H. nitida, and more revolute sepals and petals, and more deeply lobed labellum than H. spicata, both species with which it is occasionally confused. The plants are occasionally associated with Acacia, Juglans, and Prosopis in Arizona (R. A. Coleman). </discussion>
  
</bio:treatment>