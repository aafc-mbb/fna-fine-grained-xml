<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">fimbriatum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 232. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species fimbriatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101359</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–3, not clustered on stout, primary rhizome, ovoid to ± globose, 1–1.7 × 0.8–1.7 cm;</text>
      <biological_entity id="o29337" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="3" />
        <character constraint="on primary rhizome" constraintid="o29338" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s0" to="more or less globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s0" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o29338" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, reddish-brown, membranous, lacking cellular reticulation or cells arranged in only 2–3 rows proximal to roots, ± quadrate, without fibers;</text>
      <biological_entity constraint="outer" id="o29339" name="coat" name_original="coats" src="d0_s1" type="structure" />
      <biological_entity id="o29340" name="reticulation" name_original="reticulation" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="cellular" value_original="cellular" />
      </biological_entity>
      <biological_entity id="o29341" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character constraint="in rows" constraintid="o29342" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o29342" name="row" name_original="rows" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" modifier="only" name="quantity" src="d0_s1" to="3" />
        <character constraint="to roots" constraintid="o29343" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o29343" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o29344" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o29339" id="r3959" name="enclosing" negation="false" src="d0_s1" to="o29340" />
      <relation from="o29341" id="r3960" name="without" negation="false" src="d0_s1" to="o29344" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats pale-brown to white, cells obscure, quadrate.</text>
      <biological_entity constraint="inner" id="o29345" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s2" to="white" />
      </biological_entity>
      <biological_entity id="o29346" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, withering from tip by anthesis, 1, basally sheathing, sheath not extending much above soil surface;</text>
      <biological_entity id="o29347" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="from tip" constraintid="o29348" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o29348" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character modifier="by anthesis" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29349" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
      <biological_entity id="o29350" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o29351" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o29349" id="r3961" name="extending" negation="false" src="d0_s3" to="o29350" />
      <relation from="o29349" id="r3962" name="extending" negation="false" src="d0_s3" to="o29351" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, terete, 12–50 cm × 1–4 mm.</text>
      <biological_entity id="o29352" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, solid, terete, 10–35 cm × 0.5–3.5 mm.</text>
      <biological_entity id="o29353" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact, 6–75-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o29354" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="6-75-flowered" value_original="6-75-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o29355" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, 4–7-veined, lanceolate to ovate, ± equal, apex attenuate to setaceous.</text>
      <biological_entity constraint="spathe" id="o29356" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="4-7-veined" value_original="4-7-veined" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o29357" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s7" to="setaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers urceolate to campanulate, 6–12 mm;</text>
      <biological_entity id="o29358" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="urceolate" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, dark reddish purple to pale lavender, or white, lanceolate to ovate, ± equal, becoming ± rigid to papery in fruit, margins entire or denticulate with few minute teeth near tip, apex acute to acuminate, recurved-spreading or not at tip;</text>
      <biological_entity id="o29359" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark reddish" value_original="dark reddish" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s9" to="pale lavender or white" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s9" to="pale lavender or white" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character char_type="range_value" constraint="in fruit" constraintid="o29360" from="less rigid" name="texture" src="d0_s9" to="papery" />
      </biological_entity>
      <biological_entity id="o29360" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o29361" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character constraint="with teeth" constraintid="o29362" is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o29362" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s9" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o29363" name="tip" name_original="tip" src="d0_s9" type="structure" />
      <biological_entity id="o29364" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved-spreading" value_original="recurved-spreading" />
        <character name="orientation" src="d0_s9" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o29365" name="tip" name_original="tip" src="d0_s9" type="structure" />
      <relation from="o29362" id="r3963" name="near" negation="false" src="d0_s9" to="o29363" />
      <relation from="o29364" id="r3964" name="at" negation="false" src="d0_s9" to="o29365" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o29366" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o29367" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o29368" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary usually crested (rarely crestless);</text>
      <biological_entity id="o29369" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, prominent, ± triangular, margins denticulate to laciniate;</text>
      <biological_entity id="o29370" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o29371" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s14" to="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o29372" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o29373" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, 3-lobed, lobes slender, recurved;</text>
      <biological_entity id="o29374" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o29375" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 6–20 mm.</text>
      <biological_entity id="o29376" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o29377" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells minutely roughened.</text>
      <biological_entity id="o29378" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals dark reddish purple, apex usually spreading or recurved at tip.</description>
      <determination>45a Allium fimbriatum var. fimbriatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals white to pale lavender with darker midvein, apex not spreading or recurved at tip.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 10–25 cm × 1–2 mm; Mojave Desert, se California.</description>
      <determination>45b Allium fimbriatum var. mohavense</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 10–35 cm × 1.5–3.5 mm; known only from serpentine soils, n Coast Ranges, California.</description>
      <determination>45c Allium fimbriatum var. purdyi</determination>
    </key_statement>
  </key>
</bio:treatment>