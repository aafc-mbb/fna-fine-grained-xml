<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="mention_page">455</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Engelmann" date="1875" rank="species">deserti</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>3: 310. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species deserti</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101305</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent or short-stemmed, sparsely or prolifically suckering, trunks 0.2–0.4 m;</text>
      <biological_entity id="o10496" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-stemmed" value_original="short-stemmed" />
        <character is_modifier="false" modifier="sparsely; prolifically" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10497" name="trunk" name_original="trunks" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.4" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes solitary or numerous, 3–7 × 4–8 dm, compact to rather open.</text>
      <biological_entity id="o10498" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s1" to="7" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="width" src="d0_s1" to="8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="compact to rather" value_original="compact to rather" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s1" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending, widest at base, (20–) 25–70 × 4.5–10 cm;</text>
      <biological_entity id="o10499" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o10500" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_length" notes="" src="d0_s2" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" notes="" src="d0_s2" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10500" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" notes="" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade grayish, gray or blue-glaucous, yellowish green, or greenish, occasionally cross-zoned, linearlanceolate to lanceolate, rigid, adaxially concave, abaxially convex;</text>
      <biological_entity id="o10501" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="blue-glaucous" value_original="blue-glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s3" value="cross-zoned" value_original="cross-zoned" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins straight or undulate to crenate, armed, teeth single, weakly attached, well defined, of 2 types, 2–3 mm or (5–) 6–8 mm, (1–) 1.5–3 cm apart;</text>
      <biological_entity id="o10502" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character name="course" src="d0_s4" value="undulate to crenate" value_original="undulate to crenate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o10503" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" modifier="weakly" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s4" value="defined" value_original="defined" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="3" to_unit="mm" />
        <character name="some_measurement" src="d0_s4" value="(5-)6-8 mm" value_original="(5-)6-8 mm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o10504" name="type" name_original="types" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o10503" id="r1501" name="consist_of" negation="false" src="d0_s4" to="o10504" />
    </statement>
    <statement id="d0_s5">
      <text>apical spine light or dark-brown to gray, subulate to acicular, 2–4 cm.</text>
      <biological_entity constraint="apical" id="o10505" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s5" to="gray" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="acicular" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 2–6 m.</text>
      <biological_entity id="o10506" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s6" to="6" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, not bulbiferous, open;</text>
      <biological_entity id="o10507" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="bulbiferous" value_original="bulbiferous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, triangular, 8–15 cm;</text>
      <biological_entity id="o10508" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 6–15, slightly ascending, comprising distal 1/5–1/4 of inflorescence, longer than 10 cm.</text>
      <biological_entity constraint="lateral" id="o10509" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="15" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o10510" name="inflorescence" name_original="inflorescence" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s9" to="1/4" />
      </biological_entity>
      <relation from="o10509" id="r1502" name="comprising" negation="false" src="d0_s9" to="o10510" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers 12–48 per cluster, erect, 3–6 cm;</text>
      <biological_entity id="o10511" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per cluster" from="12" name="quantity" src="d0_s10" to="48" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth yellow, tube shallow, campanulate or funnelform, 3–10 × 9–15 mm, limb lobes wilting soon after anthesis, spreading, equal, 13–20 mm;</text>
      <biological_entity id="o10512" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o10513" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character is_modifier="false" name="depth" src="d0_s11" value="shallow" value_original="shallow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character name="shape" src="d0_s11" value="funnel" value_original="funnelform" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o10514" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="life_cycle" src="d0_s11" value="wilting" value_original="wilting" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens long-exserted;</text>
      <biological_entity id="o10515" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted unequally above middle or at rim of perianth-tube, erect, yellow, 2.5–3.5 (–4.2) cm;</text>
      <biological_entity id="o10516" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="above " constraintid="o10518" is_modifier="false" name="position" src="d0_s13" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s13" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10517" name="middle" name_original="middle" src="d0_s13" type="structure" />
      <biological_entity id="o10518" name="rim" name_original="rim" src="d0_s13" type="structure" />
      <biological_entity id="o10519" name="perianth-tube" name_original="perianth-tube" src="d0_s13" type="structure" />
      <relation from="o10518" id="r1503" name="part_of" negation="false" src="d0_s13" to="o10519" />
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, 13–21 mm;</text>
      <biological_entity id="o10520" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s14" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1.6–4 cm, neck slightly constricted, 4–6 mm.</text>
      <biological_entity id="o10521" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" src="d0_s15" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10522" name="neck" name_original="neck" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules short-pedicellate, ovoid to oblong or obovoid, 3.5–5.5 cm, apex beaked.</text>
      <biological_entity id="o10523" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="short-pedicellate" value_original="short-pedicellate" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="oblong or obovoid" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s16" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10524" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 5–6 mm.</text>
      <biological_entity id="o10525" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion>Varieties 3 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth tube campanulate, 3–5 mm; filaments inserted at top of tube; plants sparsely to prolifically suckering; rosettes numerous.</description>
      <determination>22a Agave deserti var. deserti</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth tube funnelform, (3–)5–10 mm; filaments inserted above middle but well within tube; plants rarely with 1–3 suckers; rosettes usually solitary.</description>
      <determination>22b Agave deserti var. simplex</determination>
    </key_statement>
  </key>
</bio:treatment>