<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">narcissus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pseudonarcissus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 289. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus narcissus;species pseudonarcissus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101787</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs ovoid, 3–5 × 2–3 cm, tunic pale-brown.</text>
      <biological_entity id="o936" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o937" name="tunic" name_original="tunic" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3–4;</text>
      <biological_entity id="o938" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade flat, 20–45 cm × 5–12 (–15) mm, glaucous.</text>
      <biological_entity id="o939" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1-flowered, 25–50 cm;</text>
      <biological_entity id="o940" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s3" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>spathe pale-brown, 2–3 cm, papery.</text>
      <biological_entity id="o941" name="spathe" name_original="spathe" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-brown" value_original="pale-brown" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers fragrant;</text>
      <biological_entity id="o942" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="odor" src="d0_s5" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth white, 5–7 cm wide;</text>
      <biological_entity id="o943" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube 1.5–2 cm, tapering abruptly to base;</text>
      <biological_entity id="o944" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="distance" src="d0_s7" to="2" to_unit="cm" />
        <character constraint="to base" constraintid="o945" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o945" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>distinct portions of tepals erect to spreading, yellow, often twisted, oblanceolate, 2.5–3.5 × 1–1.5 cm, apex acute;</text>
      <biological_entity id="o946" name="portion" name_original="portions" src="d0_s8" type="structure" constraint="tepal" constraint_original="tepal; tepal">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o947" name="tepal" name_original="tepals" src="d0_s8" type="structure" />
      <biological_entity id="o948" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o946" id="r129" name="part_of" negation="false" src="d0_s8" to="o947" />
    </statement>
    <statement id="d0_s9">
      <text>corona yellow, tubular, 30–35 × 15–25 mm, apex flared and ruffled;</text>
      <biological_entity id="o949" name="corona" name_original="corona" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o950" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="flared" value_original="flared" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens uniseriate, exserted to ca. midlength of corona;</text>
      <biological_entity id="o951" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="uniseriate" value_original="uniseriate" />
        <character constraint="midlength of corona" constraintid="o952" is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o952" name="corona" name_original="corona" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>style exserted 2–5 mm beyond anthers;</text>
      <biological_entity id="o953" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" constraint="beyond anthers" constraintid="o954" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o954" name="anther" name_original="anthers" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel 5–10 mm. 2n = 14.</text>
      <biological_entity id="o955" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o956" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Nfld. and Labr. (Nfld.), Ont.; Ala., Ark., Conn., Ga., Ill., Ind., Ky., La., Md., Mass., Mich., Miss., Mo., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.C., Tenn., Tex., Va., Wash.; w Europe; expected naturalized elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="w Europe" establishment_means="native" />
        <character name="distribution" value="expected naturalized elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">pseudo narcissus</other_name>
  <other_name type="common_name">Daffodil</other_name>
  <other_name type="common_name">trumpet narcissus</other_name>
  <discussion>Narcissus pseudonarcissus is the most variable species in the genus and includes many elements that sometimes have been recognized as separate species (e.g., H. W. Pugsley 1933). An old cultivated variety, “Telemonius Plenus,” with highly doubled flowers, commonly persists, although it does not reseed. Natural hybrids between N. pseudonarcissus and N. poeticus (N. ×incomparabilis Miller) have 1-flowered inflorescences and yellow flowers with the corona about half as long as the distinct portions of the tepals. They are known to persist in Kentucky, Louisiana, Mississippi, New Jersey, New York, North Carolina, Oregon, Pennsylvania, Texas, and Virginia. Natural hybrids between N. pseudonarcissus and N. jonquilla (N. ×odorus Linnaeus) have 1–4-flowered inflorescences and bright yellow flowers with the corona one-half to three-fourths as long as the distinct portions of the tepals. They are known to persist in Louisiana, South Carolina, Texas, and Virginia.</discussion>
  
</bio:treatment>