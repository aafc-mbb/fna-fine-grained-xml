<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Engelmann" date="1875" rank="species">parryi</taxon_name>
    <taxon_name authority="(Engelmann ex Trelease) Kearney &amp; Peebles" date="1939" rank="variety">couesii</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>29: 474. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species parryi;subspecies parryi;variety couesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102122</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agave</taxon_name>
    <taxon_name authority="Engelmann ex Trelease" date="unknown" rank="species">couesii</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>22: 94, plates 94–97. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Agave;species couesii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rosettes 3.5–5.5 × 4–6.5 dm.</text>
      <biological_entity id="o18305" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="3.5" from_unit="dm" name="length" src="d0_s0" to="5.5" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="width" src="d0_s0" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 25–42 (–47) cm × 6.5–11 cm;</text>
      <biological_entity id="o18306" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="42" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="47" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s1" to="42" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_unit="cm" name="width" src="d0_s1" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade ovate.</text>
      <biological_entity id="o18307" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 4.3–5.5 (–6) cm;</text>
      <biological_entity id="o18308" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="4.3" from_unit="cm" name="some_measurement" src="d0_s3" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube 6–9 mm, limb lobes 13–21 mm;</text>
      <biological_entity id="o18309" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o18310" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s4" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments 3.3–4.4 mm;</text>
      <biological_entity id="o18311" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s5" to="4.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 2–3.4 cm. 2n = 120.</text>
      <biological_entity id="o18312" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3.4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18313" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly places in grasslands, chaparral, pinyon-juniper, and oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly places" constraint="in grasslands , chaparral , pinyon-juniper , and oak woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19b.3.</number>
  <other_name type="common_name">Coues agave</other_name>
  <discussion>Variety couesii hybridizes with A. chrysantha in Arizona.</discussion>
  
</bio:treatment>