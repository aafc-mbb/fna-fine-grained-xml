<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="Alph. Wood" date="1868" rank="species">weedii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>20: 169. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species weedii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101505</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually bulbose;</text>
      <biological_entity id="o27529" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb coat fibrous-reticulate.</text>
      <biological_entity constraint="bulb" id="o27530" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s1" value="fibrous-reticulate" value_original="fibrous-reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems slender, usually branching, 3–9 dm.</text>
      <biological_entity id="o27531" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="9" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering, 2–4 dm;</text>
      <biological_entity id="o27532" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o27533" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to lanceolate.</text>
      <biological_entity id="o27534" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27535" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–6-flowered;</text>
      <biological_entity id="o27536" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts resembling distal cauline leaves.</text>
      <biological_entity id="o27537" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <biological_entity constraint="distal cauline" id="o27538" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o27537" id="r3734" name="resembling" negation="false" src="d0_s6" to="o27538" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o27539" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth open, broadly campanulate;</text>
      <biological_entity id="o27540" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to lanceolate, attenuate, 2–3 cm;</text>
      <biological_entity id="o27541" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals orange-yellow, flecked, broadly cuneate or obovate, ca. 2–3 cm, adaxially bearded with long yellow hairs, margins often redbrown, dentate or fringed;</text>
      <biological_entity id="o27542" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="flecked" value_original="flecked" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character constraint="with hairs" constraintid="o27543" is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o27543" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o27544" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glands round, slightly depressed, ± glabrous, surrounded by ring of long, dense, obscuring yellow hairs;</text>
      <biological_entity id="o27545" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="depressed" value_original="depressed" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27546" name="ring" name_original="ring" src="d0_s11" type="structure" />
      <biological_entity id="o27547" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="true" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o27545" id="r3735" name="surrounded by" negation="false" src="d0_s11" to="o27546" />
      <relation from="o27545" id="r3736" name="surrounded by" negation="false" src="d0_s11" to="o27547" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 8–12 mm;</text>
      <biological_entity id="o27548" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers lanceolate-oblong, apex acute.</text>
      <biological_entity id="o27549" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate-oblong" value_original="lanceolate-oblong" />
      </biological_entity>
      <biological_entity id="o27550" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, linear, angled, 4–5 cm, apex acute.</text>
      <biological_entity id="o27551" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s14" value="angled" value_original="angled" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s14" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27552" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds light beige.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 18.</text>
      <biological_entity id="o27553" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light beige" value_original="light beige" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27554" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>55.</number>
  <discussion>Varieties 4 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals deep yellow.</description>
      <determination>55b Calochortus weedii var. weedii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals purplish or cream to red-brown.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals purplish, apex rounded; anther apex rounded.</description>
      <determination>55a Calochortus weedii var. intermedius</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals usually cream or red-brown, never purplish, apex squarish; anther apex abruptly apiculate.</description>
      <determination>55c Calochortus weedii var. vestus</determination>
    </key_statement>
  </key>
</bio:treatment>