<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gerald B. Straley†,Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">MUSCARI</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 2. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus MUSCARI</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek moschos, musk, alluding to the scent of the flowers</other_info_on_name>
    <other_info_on_name type="fna_id">121348</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, scapose, from brown, tunicate, ovoid bulbs, with or without offsets (bulblets).</text>
      <biological_entity id="o19756" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19757" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tunicate" value_original="tunicate" />
        <character is_modifier="true" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o19758" name="offset" name_original="offsets" src="d0_s0" type="structure" />
      <relation from="o19756" id="r2710" name="from" negation="false" src="d0_s0" to="o19757" />
      <relation from="o19756" id="r2711" name="with or without" negation="false" src="d0_s0" to="o19758" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves (1–) 2–7, basal;</text>
      <biological_entity id="o19759" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="7" />
      </biological_entity>
      <biological_entity constraint="basal" id="o19760" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade linear, sometimes sulcate, glabrous, rather fleshy.</text>
      <biological_entity id="o19761" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape terete.</text>
      <biological_entity id="o19762" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminally racemose, many-flowered, dense, bracteate, usually elongating in fruit;</text>
      <biological_entity id="o19763" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="terminally" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o19764" is_modifier="false" modifier="usually" name="length" src="d0_s4" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o19764" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>distal flowers smaller, sterile, differing in color, forming a tuft (coma);</text>
      <biological_entity constraint="distal" id="o19765" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o19766" name="tuft" name_original="tuft" src="d0_s5" type="structure" />
      <relation from="o19765" id="r2712" name="forming a" negation="false" src="d0_s5" to="o19766" />
    </statement>
    <statement id="d0_s6">
      <text>bracts minute.</text>
      <biological_entity id="o19767" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers fragrant;</text>
      <biological_entity id="o19768" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="odor" src="d0_s7" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth tubular to urceolate, usually constricted basally;</text>
      <biological_entity id="o19769" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s8" to="urceolate" />
        <character is_modifier="false" modifier="usually; basally" name="size" src="d0_s8" value="constricted" value_original="constricted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals 6, connate most of their length, distal portions distinct, reflexed, short, toothlike;</text>
      <biological_entity id="o19770" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="length" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19771" name="portion" name_original="portions" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s9" value="toothlike" value_original="toothlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, epitepalous, in 2 rows, included;</text>
      <biological_entity id="o19772" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="epitepalous" value_original="epitepalous" />
        <character is_modifier="false" name="position" notes="" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o19773" name="row" name_original="rows" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <relation from="o19772" id="r2713" name="in" negation="false" src="d0_s10" to="o19773" />
    </statement>
    <statement id="d0_s11">
      <text>anthers dark blue, dorsifixed, globose;</text>
      <biological_entity id="o19774" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" name="fixation" src="d0_s11" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, green, 3-locular, inner sepal nectaries present;</text>
      <biological_entity id="o19775" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o19776" name="nectary" name_original="nectaries" src="d0_s12" type="structure" constraint_original="inner sepal">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1;</text>
      <biological_entity id="o19777" name="style" name_original="style" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma 3-lobed.</text>
      <biological_entity id="o19778" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsular, obtusely 3-angled, papery, dehiscence loculicidal.</text>
      <biological_entity id="o19779" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s15" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="texture" src="d0_s15" value="papery" value_original="papery" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 6, black, globose, wrinkled to reticulate.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o19780" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character char_type="range_value" from="wrinkled" name="relief" src="d0_s16" to="reticulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o19781" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; temperate Europe, n Africa, sw Asia; expected introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="temperate Europe" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="expected  elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>60.</number>
  <other_name type="common_name">Grape-hyacinth</other_name>
  <discussion>Species ca. 30 (3 in the flora).</discussion>
  <discussion>Various species and cultivated forms of Muscari are commonly grown for their early spring flowers. They may reseed in the flora area, but they are mostly transported in soil containing the bulblets.</discussion>
  <discussion>Muscari armeniacum Baker has been attributed to the flora, but no definite records of naturalized plants have been found. Herbarium specimens of that species are difficult to distinguish from those of M. neglectum, but live specimens of M. armeniacum have much paler blue flowers (A. Huxley et al. 1992).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals of fertile flowers pale to olive brown, sterile flowers bright violet, shorter than the 6–25 mm ascending pedicels.</description>
      <determination>1 Muscari comosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals of fertile and sterile flowers blue, longer than the 1–4(–5) mm declined, nodding, or spreading pedicels.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Racemes 12–20-flowered; leaf blades 3–8 mm wide; perianth tubes of fertile flowers globose, sky blue.</description>
      <determination>2 Muscari botryoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Racemes 20–40-flowered; leaf blades 2–4(–5) mm; perianth tubes of fertile flowers obovoid to oblong-urceolate or cylindric, blackish blue.</description>
      <determination>3 Muscari neglectum</determination>
    </key_statement>
  </key>
</bio:treatment>