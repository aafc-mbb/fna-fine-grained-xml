<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">narcissus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">poeticus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 289. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus narcissus;species poeticus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220008994</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs ovoid, 3–4 × 1.5–2 cm, tunic brown.</text>
      <biological_entity id="o11456" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11457" name="tunic" name_original="tunic" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 4;</text>
      <biological_entity id="o11458" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade flat, 20–40 cm × 6–10 mm, green to glaucous.</text>
      <biological_entity id="o11459" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1-flowered, 30–45 cm;</text>
      <biological_entity id="o11460" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s3" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>spathe pale-brown, 4–5 cm, papery.</text>
      <biological_entity id="o11461" name="spathe" name_original="spathe" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-brown" value_original="pale-brown" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers fragrant;</text>
      <biological_entity id="o11462" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="odor" src="d0_s5" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth 5–7 cm wide;</text>
      <biological_entity id="o11463" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube 2–3 cm, gradually tapering to base;</text>
      <biological_entity id="o11464" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s7" to="3" to_unit="cm" />
        <character constraint="to base" constraintid="o11465" is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o11465" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>tepals overlapping, distinct portions spreading to reflexed, white, ovate-orbicular, 1.5–2.5 × 1.5–2 cm, apex mucronate;</text>
      <biological_entity id="o11466" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o11467" name="portion" name_original="portions" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate-orbicular" value_original="ovate-orbicular" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11468" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corona yellow with red, crenulate margin, cupshaped, 3–5 × 10–15 mm;</text>
      <biological_entity id="o11469" name="corona" name_original="corona" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow with red" value_original="yellow with red" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>3 short stamens included in perianth-tube, 3 longer stamens and style exserted into mouth of corona;</text>
      <biological_entity id="o11470" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o11471" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o11472" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure" />
      <biological_entity constraint="longer" id="o11473" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity constraint="longer" id="o11474" name="style" name_original="style" src="d0_s10" type="structure" />
      <biological_entity id="o11475" name="mouth" name_original="mouth" src="d0_s10" type="structure" />
      <biological_entity id="o11476" name="corona" name_original="corona" src="d0_s10" type="structure" />
      <relation from="o11471" id="r1611" name="included in" negation="false" src="d0_s10" to="o11472" />
      <relation from="o11471" id="r1612" name="included in" negation="false" src="d0_s10" to="o11473" />
      <relation from="o11471" id="r1613" name="included in" negation="false" src="d0_s10" to="o11474" />
      <relation from="o11471" id="r1614" name="exserted into" negation="false" src="d0_s10" to="o11475" />
      <relation from="o11471" id="r1615" name="part_of" negation="false" src="d0_s10" to="o11476" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel 2–3 cm. 2n = 14, 21.</text>
      <biological_entity id="o11477" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11478" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="14" value_original="14" />
        <character name="quantity" src="d0_s11" value="21" value_original="21" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., Nfld. and Labr. (Nfld.), Ont., Que.; Ala., Ark., Conn., Ga., Ill., Ky., La., Maine, Md., Mich., Miss., Mo., N.J., N.Y., N.C., Ohio, Oreg., Pa., S.C., Tenn., Utah, Vt., Va., Wash., Wis.; c, s Europe; expected naturalized elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="expected naturalized elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Poet’s narcissus</other_name>
  <other_name type="common_name">pheasant’s-eye narcissus</other_name>
  <discussion>Natural hybrids between Narcissus poeticus and N. tazetta have been given the name N. ×medioluteus Miller (N. biflorus Curtis). These are more or less intermediate between the parents, and the inflorescence is usually 2-flowered. The tepals are white to pale yellow, and the short corona is dark yellow. These hybrids may persist in the flora area, as known from Alabama, Arkansas, Louisiana, Maryland, North Carolina, South Carolina, and Virginia.</discussion>
  
</bio:treatment>