<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J. Chris Pires</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Kellogg" date="1863" rank="genus">BLOOMERIA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 11. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus BLOOMERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for H. G. Bloomer, 1821–1874, early San Francisco botanist and one-time botanical curator at the California Academy of Sciences</other_info_on_name>
    <other_info_on_name type="fna_id">104115</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, from fibrous-coated corms.</text>
      <biological_entity id="o3894" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3895" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character is_modifier="true" name="coating" src="d0_s0" value="fibrous-coated" value_original="fibrous-coated" />
      </biological_entity>
      <relation from="o3894" id="r571" name="from" negation="false" src="d0_s0" to="o3895" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1–8, basal;</text>
      <biological_entity id="o3896" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="8" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3897" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade linearlanceolate, keeled, margins entire.</text>
      <biological_entity id="o3898" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o3899" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape slender, cylindrical, rigid.</text>
      <biological_entity id="o3900" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate, open, 10–35-flowered, bracteate;</text>
      <biological_entity id="o3901" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="10-35-flowered" value_original="10-35-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 2–4, scarious, membranous, not enclosing flower buds.</text>
      <biological_entity id="o3902" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="flower" id="o3903" name="bud" name_original="buds" src="d0_s5" type="structure" />
      <relation from="o3902" id="r572" name="enclosing" negation="true" src="d0_s5" to="o3903" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals 6, persistent, widely spreading, distinct or barely connate at base, golden yellow, striped brownish or green, nearly equal, oblong-linear, subrotate at anthesis;</text>
      <biological_entity id="o3904" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3905" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o3906" is_modifier="false" modifier="barely" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="striped brownish" value_original="striped brownish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="oblong-linear" value_original="oblong-linear" />
        <character constraint="at anthesis" is_modifier="false" name="shape" src="d0_s6" value="subrotate" value_original="subrotate" />
      </biological_entity>
      <biological_entity id="o3906" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 6, epitepalous, slightly shorter than and inserted at base of tepals;</text>
      <biological_entity id="o3907" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3908" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s7" value="epitepalous" value_original="epitepalous" />
        <character constraint="than and inserted" constraintid="o3909" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o3909" name="inserted" name_original="inserted" src="d0_s7" type="structure" />
      <biological_entity id="o3910" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o3911" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <relation from="o3909" id="r573" name="at" negation="false" src="d0_s7" to="o3910" />
      <relation from="o3910" id="r574" name="part_of" negation="false" src="d0_s7" to="o3911" />
    </statement>
    <statement id="d0_s8">
      <text>filaments filiform distally, dilated basally, ca. 6 mm, dilated bases sometimes connate into nectariferous cup, cup sometimes having basal filament appendages arising from apex;</text>
      <biological_entity id="o3912" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3913" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o3914" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
        <character constraint="into nectariferous cup" constraintid="o3915" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="nectariferous" id="o3915" name="cup" name_original="cup" src="d0_s8" type="structure" />
      <biological_entity id="o3916" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character constraint="from apex" constraintid="o3918" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="filament" id="o3917" name="appendage" name_original="appendages" src="d0_s8" type="structure" constraint_original="basal filament" />
      <biological_entity id="o3918" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o3916" id="r575" name="having" negation="false" src="d0_s8" to="o3917" />
    </statement>
    <statement id="d0_s9">
      <text>anthers subbasifixed, versatile;</text>
      <biological_entity id="o3919" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3920" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="subbasifixed" value_original="subbasifixed" />
        <character is_modifier="false" name="fixation" src="d0_s9" value="versatile" value_original="versatile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o3921" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3922" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, sessile, 3-locular, ovules anatropous, several per locule;</text>
      <biological_entity id="o3923" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3924" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity id="o3925" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="anatropous" value_original="anatropous" />
        <character constraint="per locule" constraintid="o3926" is_modifier="false" name="quantity" src="d0_s11" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o3926" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style persistent, splitting with capsule, filiform or clavate, 5 mm;</text>
      <biological_entity id="o3927" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3928" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character constraint="with capsule" constraintid="o3929" is_modifier="false" name="architecture_or_dehiscence" src="d0_s12" value="splitting" value_original="splitting" />
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o3929" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigma 3-lobed;</text>
      <biological_entity id="o3930" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3931" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel long, erect, raylike, base and apex articulate.</text>
      <biological_entity id="o3932" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3933" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s14" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="raylike" value_original="raylike" />
      </biological_entity>
      <biological_entity id="o3934" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o3935" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="articulate" value_original="articulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits sessile, capsular, 3-angled, subglobose, 5–6 mm, dehiscence loculicidal.</text>
      <biological_entity id="o3936" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds black, angular, subovoid, wrinkled, coat with crust.</text>
      <biological_entity id="o3937" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subovoid" value_original="subovoid" />
        <character is_modifier="false" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity id="o3939" name="crust" name_original="crust" src="d0_s16" type="structure" />
      <relation from="o3938" id="r576" name="with" negation="false" src="d0_s16" to="o3939" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9 (except for B. clevelandii x = 14).</text>
      <biological_entity id="o3938" name="coat" name_original="coat" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o3940" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c and s Calif., Mexico (n Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c and s Calif" establishment_means="native" />
        <character name="distribution" value="Mexico (n Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>68.</number>
  <other_name type="common_name">Golden star</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>J. W. Ingram (1953) considered Bloomeria to be closely related to Brodiaea, Muilla, and Allium. R. F. Hoover (1941, 1955), on the other hand, considered it to be closely related to Triteleia, on the basis of corm morphology, keeled leaves, umbel structure, versatile anthers distant from the style, filament appendages, a stigma with three small lobes, similar seeds, and the same base chromosome number. However, Bloomeria differs generally from Triteleia in that it has distinct tepals and a sessile ovary, although the tepals of some B. crocea are slightly connate at the base, and T. ixioides has a very short perianth tube. Hoover kept the two genera separate on the basis of the stipitate ovary and geographic distribution. Triteleia rarely occurs south of the Tehachapi and the Santa Lucia Mountains in California, where Bloomeria occurs.</discussion>
  <discussion>Among the most important diagnostic characters within Bloomeria are features of the androecium, particularly the orientation of the stamens relative to the style, and the appendages at the dilated bases of the filaments. These characters are easily seen in the field with a hand lens. When collecting flowering specimens, one should make a point of preparing a few dissected flowers in a manner that displays these critical characters.</discussion>
  <references>
    <reference>Hoover, R. F. 1955. Further observations on Brodiaea and some related genera. Pl. Life 11: 13–22.</reference>
    <reference>Ingram, J. W. 1953. A monograph of the genera Bloomeria and Muilla (Liliaceae). Madroño 12: 19–27. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filaments leaning away from style, leaving dilated bases separated and therefore not forming nectariferous cup; style shorter than or equal to ovary; leaves 2–8, 1–3 mm wide.</description>
      <determination>3 Bloomeria clevelandii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filaments parallel to style, dilated bases connate into nectariferous cup; style longer than ovary; leaves 1–2, 3–15 mm wide.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 15–70 cm; tepals abruptly spreading at base; leaves usually 1.</description>
      <determination>1 Bloomeria crocea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scape 5–10 cm; tepals ascending at base, then gradually spreading; leaves usually 1–2.</description>
      <determination>2 Bloomeria humilis</determination>
    </key_statement>
  </key>
</bio:treatment>