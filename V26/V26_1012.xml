<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Blume" date="unknown" rank="family">burmanniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">burmannia</taxon_name>
    <taxon_name authority="Martius" date="1823" rank="species">flava</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Gen. Sp. Pl.</publication_title>
      <place_in_publication>1: 11, plate 5, fig. 3. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family burmanniaceae;genus burmannia;species flava</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101451</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 3.5–15 cm.</text>
      <biological_entity id="o30299" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal leaves 2–7, linear, 3–11 × 0.8–1 mm;</text>
      <biological_entity id="o30300" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o30301" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="7" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves subulate to linear, 2–12 × 0.3–1 mm.</text>
      <biological_entity id="o30302" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o30303" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1–2-flowered;</text>
      <biological_entity id="o30304" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral bracts lanceolate to ovate, 2–5 mm.</text>
      <biological_entity constraint="floral" id="o30305" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 3-winged, 5–10 (–14) mm, wings yellow, to 1.8 mm wide;</text>
      <biological_entity id="o30306" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-winged" value_original="3-winged" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30307" name="wing" name_original="wings" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth yellow or greenish;</text>
      <biological_entity id="o30308" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube 6-lobed;</text>
      <biological_entity id="o30309" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="6-lobed" value_original="6-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>outer lobes erect, triangular, 1.3–1.8 mm;</text>
      <biological_entity constraint="outer" id="o30310" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner lobes linear or elliptic, 1–1.5 mm.</text>
      <biological_entity constraint="inner" id="o30311" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 3–6 mm.</text>
      <biological_entity id="o30312" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall (Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies (Cuba); Central America; South America (to Argentina, Bolivia, Paraguay, Brazil).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (to Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="South America (Paraguay)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>