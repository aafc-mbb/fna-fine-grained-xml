<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">XEROPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 210. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus XEROPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek xeros, dry, and phyllon, leaf, alluding to the sclerified foliage</other_info_on_name>
    <other_info_on_name type="fna_id">135103</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from stout, tuberous, woody rhizomes ending in weakly developed, tunicate bulbs;</text>
      <biological_entity id="o8253" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o8254" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o8255" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="weakly" name="development" src="d0_s0" value="developed" value_original="developed" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tunicate" value_original="tunicate" />
      </biological_entity>
      <relation from="o8253" id="r1181" name="from" negation="false" src="d0_s0" to="o8254" />
      <relation from="o8254" id="r1182" name="ending in" negation="false" src="d0_s0" to="o8255" />
    </statement>
    <statement id="d0_s1">
      <text>offshoots not flowering for several years, then dying after fruiting;</text>
      <biological_entity id="o8256" name="offshoot" name_original="offshoots" src="d0_s1" type="structure">
        <character constraint="for years" constraintid="o8257" is_modifier="false" modifier="not" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
        <character constraint="after fruiting" constraintid="o8258" is_modifier="false" name="condition" notes="" src="d0_s1" value="dying" value_original="dying" />
      </biological_entity>
      <biological_entity id="o8257" name="year" name_original="years" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o8258" name="fruiting" name_original="fruiting" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>roots cordlike, thick.</text>
      <biological_entity id="o8259" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordlike" value_original="cordlike" />
        <character is_modifier="false" name="width" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, simple, robust.</text>
      <biological_entity id="o8260" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="robust" value_original="robust" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves persistent, mostly basal, spiral, arching, dilated proximally, reduced distally;</text>
      <biological_entity id="o8261" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o8262" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s4" value="spiral" value_original="spiral" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s4" value="dilated" value_original="dilated" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade filiform-linear, keeled, rigid, margins serrulate, apex wiry pointed, densely tufted.</text>
      <biological_entity id="o8263" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="filiform-linear" value_original="filiform-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="texture" src="d0_s5" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o8264" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o8265" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s5" value="tufted" value_original="tufted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences at first corymbose, becoming elongate, terminal-racemose, densely flowered, bracteate.</text>
      <biological_entity id="o8266" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="corymbose" value_original="corymbose" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="terminal-racemose" value_original="terminal-racemose" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect or spreading, semirotate;</text>
      <biological_entity id="o8267" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s7" value="semirotate" value_original="semirotate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth hypogynous;</text>
      <biological_entity id="o8268" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals persistent, 6, spreading, distinct, creamy white, 5–7-veined, oblong or obovate, equal to subequal, claws absent, tepal nectaries absent;</text>
      <biological_entity id="o8269" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o8270" name="claw" name_original="claws" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="tepal" id="o8271" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, weakly epitepalous, divergent, distinct, 3–4 mm;</text>
      <biological_entity id="o8272" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" modifier="weakly" name="position" src="d0_s10" value="epitepalous" value_original="epitepalous" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments distinct, subulate, dilated proximally;</text>
      <biological_entity id="o8273" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subulate" value_original="subulate" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s11" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers dorsifixed, versatile, 2-locular (not confluent), extrorse;</text>
      <biological_entity id="o8274" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s12" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="fixation" src="d0_s12" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s12" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynoecium weakly syncarpous;</text>
      <biological_entity id="o8275" name="gynoecium" name_original="gynoecium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s13" value="syncarpous" value_original="syncarpous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, 3-locular;</text>
      <biological_entity id="o8276" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>septal nectaries absent;</text>
      <biological_entity constraint="septal" id="o8277" name="nectary" name_original="nectaries" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles persistent, 3, recurved, distinct;</text>
      <biological_entity id="o8278" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas papillate along adaxial apex;</text>
      <biological_entity id="o8279" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character constraint="along adaxial apex" constraintid="o8280" is_modifier="false" name="relief" src="d0_s17" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8280" name="apex" name_original="apex" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>pedicel spreading or erect, 3–5 cm.</text>
      <biological_entity id="o8281" name="pedicel" name_original="pedicel" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s18" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsular, 3-lobed, globular to ovoid, dehiscence loculicidal.</text>
      <biological_entity id="o8282" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="globular" name="shape" src="d0_s19" to="ovoid" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 2–4 per locule, greenish brown, wingless, oblong-fusiform, 3-angled, 3–4 mm, glossy.</text>
      <biological_entity id="o8284" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>x = 15.</text>
      <biological_entity id="o8283" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o8284" from="2" name="quantity" src="d0_s20" to="4" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s20" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="wingless" value_original="wingless" />
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong-fusiform" value_original="oblong-fusiform" />
        <character is_modifier="false" name="shape" src="d0_s20" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s20" to="4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s20" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="x" id="o8285" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e, w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Xerophyllum, an endemic genus with one species in eastern and one in western North America, is distinct from other melanthoid genera with which it has long been associated (J. D. Ambrose 1975, 1980; P. Goldblatt 1995; C. Sterling 1980; M. Takahashi and S. Kawano 1989; F. H. Utech 1978c) and has recently been segregated in the tribe Xerophylleae S. Watson within the Melanthiaceae sensu stricto (M. N. Tamura 1998; W. B. Zomlefer 1997b), or in the monotypic family Xerophyllaceae (A. L. Takhtajan 1994, 1997).</discussion>
  <discussion>Xerophyllum tenax and X. asphodeloides are both cultivated for their showy, white racemes. Any given offshoot plant may not flower for several years, but ultimately an unbranched, strongly bracteate, flowering stalk is produced which, along with the surrounding leaves, dies after fruiting.</discussion>
  <references>
    <reference>Utech, F. H. 1978c. Comparison of the vascular floral anatomy of Xerophyllum asphodeloides (L.) Nutt. and X. tenax (Pursh) Nutt. (Liliaceae–Melanthioideae). Ann. Carnegie Mus. 47: 147–167.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems (0.8–)1.2–1.8(–2) m; leaf blade 2–8(–10) dm × 2–4(–6) mm; racemes 5–7 dm; w Cordilleras.</description>
      <determination>1 Xerophyllum tenax</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems (0.5–)0.8–1(–1.5) m; leaf blade 3–5 dm × 1–2.5 mm; racemes 1.5–3 dm; e mountains and coastal pine barrens</description>
      <determination>2 Xerophyllum asphodeloides</determination>
    </key_statement>
  </key>
</bio:treatment>