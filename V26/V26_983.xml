<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">smilacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">smilax</taxon_name>
    <taxon_name authority="(A. de Candolle) A. Gray in S. Watson" date="1880" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Bot. California</publication_title>
      <place_in_publication>2: 186. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family smilacaceae;genus smilax;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101928</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilax</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rotundifolia</taxon_name>
    <taxon_name authority="A. de Candolle" date="unknown" rank="variety">californica</taxon_name>
    <place_of_publication>
      <publication_title>in A. L. P. P. de Candolle and C. de Candolle, Monogr. Phan.</publication_title>
      <place_in_publication>1: 75. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Smilax;species rotundifolia;variety californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or vines;</text>
      <biological_entity id="o34943" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short, knotty.</text>
      <biological_entity id="o34945" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems perennial, climbing or not, to 12 m, woody, glabrous;</text>
      <biological_entity id="o34946" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="climbing" value_original="climbing" />
        <character name="growth_form" src="d0_s2" value="not" value_original="not" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="12" to_unit="m" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>prickles sometimes absent distally, bristlelike, 3–11 mm, flexible.</text>
      <biological_entity id="o34947" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bristlelike" value_original="bristlelike" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="11" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves evergreen, ± evenly dispersed;</text>
      <biological_entity id="o34948" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="more or less evenly" name="density" src="d0_s4" value="dispersed" value_original="dispersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–2 cm;</text>
      <biological_entity id="o34949" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade dull green, drying to dull, ashy green, ovate to broadly ovate, conspicuously veined, 4–11 × 3–8 cm, not glaucous, glabrous, base cordate to subcordate;</text>
      <biological_entity id="o34950" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="condition" src="d0_s6" value="drying" value_original="drying" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="ashy green" value_original="ashy green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="broadly ovate" />
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s6" value="veined" value_original="veined" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="11" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34951" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s6" to="subcordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins entire, thin, flat, not banded, never lobed;</text>
      <biological_entity id="o34952" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="banded" value_original="banded" />
        <character is_modifier="false" modifier="never" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute, often apiculate.</text>
      <biological_entity id="o34953" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Umbels axillary to distal leaves, (2–) 8–13 (–19) -flowered;</text>
      <biological_entity id="o34954" name="umbel" name_original="umbels" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="(2-)8-13(-19)-flowered" value_original="(2-)8-13(-19)-flowered" />
      </biological_entity>
      <biological_entity id="o34955" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>peduncle 2–5 cm, longer than petiole of subtending leaf.</text>
      <biological_entity id="o34956" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
        <character constraint="than petiole" constraintid="o34957" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o34957" name="petiole" name_original="petiole" src="d0_s10" type="structure" />
      <biological_entity constraint="subtending" id="o34958" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
      <relation from="o34957" id="r4690" name="part_of" negation="false" src="d0_s10" to="o34958" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: perianth green;</text>
      <biological_entity id="o34959" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34960" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tepals 3–6 mm;</text>
      <biological_entity id="o34961" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34962" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovule 1 per locule;</text>
      <biological_entity id="o34963" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34964" name="ovule" name_original="ovule" src="d0_s13" type="structure">
        <character constraint="per locule" constraintid="o34965" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o34965" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pedicel thin, 1–1.5 cm.</text>
      <biological_entity id="o34966" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o34967" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Berries black, ovoid, 7–9 mm.</text>
      <biological_entity id="o34968" name="berry" name_original="berries" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Yellow pine and mixed evergreen forests, often in thickets along rivers, streams, and springs, partial–full sun</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="mixed evergreen forests" />
        <character name="habitat" value="thickets" constraint="along rivers , streams , and springs ," />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="full sun" modifier="partial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>250–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="250" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Smilax californica is apparently closely related to the more eastern S. tamnoides. It lacks the minute serrulations characteristic of the latter’s leaves.</discussion>
  
</bio:treatment>