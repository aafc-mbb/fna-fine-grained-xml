<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="genus">odontostomum</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="species">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 150. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus odontostomum;species hartwegii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220009387</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 5.5 dm.</text>
      <biological_entity id="o1218" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="5.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bulbs subglobose, slightly broader than tall, 1–3 cm wide.</text>
      <biological_entity id="o1219" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="width" src="d0_s1" value="slightly broader than tall" value_original="slightly broader than tall" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, tapering gradually to attenuate apex;</text>
      <biological_entity id="o1220" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character constraint="to apex" constraintid="o1221" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o1221" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves smaller than basal, gradually reduced to bracts subtending panicle branches;</text>
      <biological_entity constraint="cauline" id="o1222" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="than basal" constraintid="o1223" is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character constraint="to bracts" constraintid="o1224" is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o1223" name="basal" name_original="basal" src="d0_s3" type="structure" />
      <biological_entity id="o1224" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity constraint="panicle" id="o1225" name="branch" name_original="branches" src="d0_s3" type="structure" constraint_original="subtending panicle" />
    </statement>
    <statement id="d0_s4">
      <text>blade (6.5–) 10–25 × (0.2–) 0.5–0.8 cm.</text>
      <biological_entity id="o1226" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="6.5" from_unit="cm" name="atypical_length" src="d0_s4" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s4" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 0–4-branched, each branch (1–) 5–40-flowered;</text>
      <biological_entity id="o1227" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="0-4-branched" value_original="0-4-branched" />
      </biological_entity>
      <biological_entity id="o1228" name="branch" name_original="branch" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)5-40-flowered" value_original="(1-)5-40-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts subulate, ± equaling pedicels.</text>
      <biological_entity id="o1229" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o1230" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth creamy white to yellowish;</text>
      <biological_entity id="o1231" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1232" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s7" to="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth-tube 4–6 mm, limb lobes ca. as long;</text>
      <biological_entity id="o1233" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1234" name="perianth-tube" name_original="perianth-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o1235" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>outer 3 tepals 5–7-veined, ± lanceolate, apex acute;</text>
      <biological_entity id="o1236" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="outer" value_original="outer" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1237" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1238" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner 3 tepals 3–5-veined, ± oblanceolate, apex obtuse;</text>
      <biological_entity id="o1239" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1240" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o1241" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–2 mm;</text>
      <biological_entity id="o1242" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1243" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes 0.5–1 mm;</text>
      <biological_entity id="o1244" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1245" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicels 3–5 mm.</text>
      <biological_entity id="o1246" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1247" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ca. 4 mm wide.</text>
      <biological_entity id="o1248" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character name="width" src="d0_s14" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (mid Apr–mid Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="mid Apr-mid Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassland, savanna with blue oak (Quercus douglasii Hooker &amp; Arnott) or digger pine (Pinus sabiniana Douglas ex D. Don), usually in well-drained, rocky clay soils (often serpentine), but sometimes on vernal pool margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassland" />
        <character name="habitat" value="savanna" constraint="with blue oak ( quercus douglasii hooker &amp; arnott ) or digger pine ( pinus sabiniana douglas ex d" />
        <character name="habitat" value="blue oak" />
        <character name="habitat" value="quercus douglasii hooker" />
        <character name="habitat" value="arnott" />
        <character name="habitat" value="digger pine" />
        <character name="habitat" value="pinus" />
        <character name="habitat" value="sabiniana douglas" />
        <character name="habitat" value="don)" />
        <character name="habitat" value="well-drained" modifier="usually in" />
        <character name="habitat" value="rocky clay soils" />
        <character name="habitat" value="vernal pool margins" modifier="but sometimes on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Odontostomum hartwegii ranges from Shasta County south to Mariposa County along the Sierra foothills, and locally in the Coast Ranges in Tehama and Napa counties. It is more frequent in the northern portion of this range, from Butte to Shasta counties.</discussion>
  
</bio:treatment>