<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">600</other_info_on_meta>
    <other_info_on_meta type="treatment_page">601</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">calopogon</taxon_name>
    <taxon_name authority="(Linnaeus) Britton" date="1888" rank="species">tuberosus</taxon_name>
    <taxon_name authority="(Small) Magrath" date="1989" rank="variety">simpsonii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>13: 371. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus calopogon;species tuberosus;variety simpsonii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102205</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">simpsonii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>322, 1329. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Limodorum;species simpsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calopogon</taxon_name>
    <taxon_name authority="(Salisbury) R. Brown" date="unknown" rank="species">pulchellus</taxon_name>
    <taxon_name authority="(Small) Ames" date="unknown" rank="variety">simpsonii</taxon_name>
    <taxon_hierarchy>genus Calopogon;species pulchellus;variety simpsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–100 cm or more.</text>
      <biological_entity id="o10717" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose, 8–22 mm.</text>
      <biological_entity id="o10718" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="globose" value_original="globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s1" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade strongly curled transversely, linear, 25–50 cm × 3.5–15 mm.</text>
      <biological_entity id="o10719" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10720" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly; transversely" name="shape" src="d0_s2" value="curled" value_original="curled" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences green or seldom slightly purple at base, becoming entirely green after flowering, 20–100 cm or more;</text>
      <biological_entity id="o10721" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character constraint="at base" constraintid="o10722" is_modifier="false" modifier="seldom slightly" name="coloration" src="d0_s3" value="purple" value_original="purple" />
        <character constraint="after flowering" is_modifier="false" modifier="becoming entirely" name="coloration" notes="" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s3" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10722" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>floral bracts ovate to ovatelanceolate, subulate, 3–12 (–30) mm.</text>
      <biological_entity constraint="floral" id="o10723" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1–20 (–25);</text>
      <biological_entity id="o10724" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="25" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>dorsal sepal 18–25 × 5–10 mm;</text>
      <biological_entity constraint="dorsal" id="o10725" name="sepal" name_original="sepal" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals sometimes reflexed distally, often narrowed, 15–20 × 7.5–13 mm, apex acuminate;</text>
      <biological_entity constraint="lateral" id="o10726" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="width" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10727" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 18–25 × 5–10 mm;</text>
      <biological_entity id="o10728" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip 12–23 mm, dilated end of middle lobe paler color than rest of flower, narrowly anvil-shaped to triangular, 5.5–16 mm wide;</text>
      <biological_entity id="o10729" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10730" name="end" name_original="end" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
        <character constraint="of flower" constraintid="o10732" is_modifier="false" name="coloration" src="d0_s9" value="paler color" value_original="paler color" />
        <character char_type="range_value" from="narrowly anvil-shaped" name="shape" notes="" src="d0_s9" to="triangular" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o10731" name="lobe" name_original="lobe" src="d0_s9" type="structure" />
      <biological_entity id="o10732" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <relation from="o10730" id="r1519" name="part_of" negation="false" src="d0_s9" to="o10731" />
    </statement>
    <statement id="d0_s10">
      <text>column 15–25 × 1.5–2 mm, distal end 4.5–8 mm wide;</text>
      <biological_entity id="o10733" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10734" name="end" name_original="end" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rostellum absent or present.</text>
      <biological_entity id="o10735" name="rostellum" name_original="rostellum" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid to ellipsoid, 15–25 × 5–8 mm. 2n = 40.</text>
      <biological_entity id="o10736" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="ellipsoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10737" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Nov–Jul, most commonly mid–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Nov" />
        <character name="flowering time" char_type="range_value" modifier="commonly" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet, alkaline tropical and subtropical conifer savannas, meadows, and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline tropical" modifier="seasonally wet" />
        <character name="habitat" value="subtropical conifer savannas" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Bahamas, Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4b.</number>
  <discussion>Calopogon tuberosus var. simpsonii can be distinguished from the typical variety of the species based on its strongly transversely curled leaves, the frequently narrowed and pale apex of the middle lip lobe, and its habitat in wet, marly soils. Based on morphology and allozymes (D. W. Trapnell 1995) and on nuclear and chloroplast DNA (D. H. Goldman 2000), C. tuberosus var. simpsonii was determined to be a distinct taxon.</discussion>
  
</bio:treatment>