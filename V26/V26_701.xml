<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">343</other_info_on_meta>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="genus">triteleia</taxon_name>
    <taxon_name authority="(W. T. Aiton) Greene" date="1886" rank="species">ixioides</taxon_name>
    <taxon_name authority="L. W. Lenz" date="1975" rank="subspecies">unifolia</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>8: 243, figs. 15, 25. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus triteleia;species ixioides;subspecies unifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102321</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves usually only 1, relatively short, 10–20 cm.</text>
      <biological_entity id="o8755" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" value="1" value_original="1" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scape 10–30 cm, smooth or slighty scabrous.</text>
      <biological_entity id="o8756" name="scape" name_original="scape" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s1" value="slighty" value_original="slighty" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: perianth dull yellow, sometimes with darker midvein color extending into abaxial portion of perianth, tube 5–7 mm, ca. 1/2 length of lobes, lobes spreading, 9–12 mm;</text>
      <biological_entity id="o8757" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o8758" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s2" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o8759" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="darker" value_original="darker" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="extending" value_original="extending" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8760" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o8761" name="perianth" name_original="perianth" src="d0_s2" type="structure" />
      <biological_entity id="o8762" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
        <character name="length" src="d0_s2" value="1/2 length of lobes" value_original="1/2 length of lobes" />
      </biological_entity>
      <biological_entity id="o8763" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <relation from="o8758" id="r1255" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o8759" />
      <relation from="o8759" id="r1256" name="into" negation="false" src="d0_s2" to="o8760" />
      <relation from="o8760" id="r1257" name="part_of" negation="false" src="d0_s2" to="o8761" />
    </statement>
    <statement id="d0_s3">
      <text>short filaments 2–4 mm, long filaments 3–5 mm, apical appendages short, straight or incurved;</text>
      <biological_entity id="o8764" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o8765" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8766" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8767" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>anthers cream or yellow;</text>
      <biological_entity id="o8768" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o8769" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 1–4 cm. 2n = 14, 16.</text>
      <biological_entity id="o8770" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o8771" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8772" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="14" value_original="14" />
        <character name="quantity" src="d0_s5" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests, in clay and granite soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" constraint="in clay and granite soils" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="granite soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8e.</number>
  
</bio:treatment>