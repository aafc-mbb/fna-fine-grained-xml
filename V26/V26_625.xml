<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">313</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey ex Durand" date="unknown" rank="genus">schoenolirion</taxon_name>
    <taxon_name authority="(Rafinesque) R. R. Gates" date="1918" rank="species">albiflorum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>44: 167. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus schoenolirion;species albiflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101884</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblostima</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">albiflora</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Tellur.</publication_title>
      <place_in_publication>2: 26. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amblostima;species albiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxytria</taxon_name>
    <taxon_name authority="(Rafinesque) Pollard" date="unknown" rank="species">albiflora</taxon_name>
    <taxon_hierarchy>genus Oxytria;species albiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with coarsely fibrous leaves arising directly from top of vertical rootstock;</text>
      <biological_entity id="o12900" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12901" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="coarsely" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character constraint="from top" constraintid="o12902" is_modifier="false" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o12902" name="top" name_original="top" src="d0_s0" type="structure" />
      <biological_entity id="o12903" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o12900" id="r1799" name="with" negation="false" src="d0_s0" to="o12901" />
      <relation from="o12902" id="r1800" name="top of" negation="false" src="d0_s0" to="o12903" />
    </statement>
    <statement id="d0_s1">
      <text>bulbs absent.</text>
      <biological_entity id="o12904" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 4–7, 26–45 (–72) cm × 2–4 mm, fibers withering to usually persistent;</text>
      <biological_entity id="o12905" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="72" to_unit="cm" />
        <character char_type="range_value" from="26" from_unit="cm" name="length" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12906" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade flattened or ± terete with prominent adaxial groove, usually shorter than scape and inflorescence, base not fleshy.</text>
      <biological_entity id="o12907" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character constraint="with adaxial groove" constraintid="o12908" is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character constraint="than scape and inflorescence" constraintid="o12909, o12910" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s3" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12908" name="groove" name_original="groove" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o12909" name="scape" name_original="scape" src="d0_s3" type="structure" />
      <biological_entity id="o12910" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <biological_entity id="o12911" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–3 (–6) -branched (occasionally simple or with secondary branches), flowers separated up to 6 cm or distally clustered;</text>
      <biological_entity id="o12912" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3(-6)-branched" value_original="1-3(-6)-branched" />
      </biological_entity>
      <biological_entity id="o12913" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_growth_form" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate-acuminate to ovate-acuminate, basal usually much longer than distal.</text>
      <biological_entity id="o12914" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate-acuminate" name="shape" src="d0_s5" to="ovate-acuminate" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character constraint="than distal inflorescence" constraintid="o12915" is_modifier="false" name="length_or_size" src="d0_s5" value="usually much longer" value_original="usually much longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12915" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals often strongly recurved, white or greenish white, each with greenish abaxial stripe, 5–7-veined, ovate, 3–4.5 mm, apex obtuse;</text>
      <biological_entity id="o12916" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12917" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often strongly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12918" name="stripe" name_original="stripe" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o12919" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o12917" id="r1801" name="with" negation="false" src="d0_s6" to="o12918" />
    </statement>
    <statement id="d0_s7">
      <text>ovary and style green.</text>
      <biological_entity id="o12920" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 48?, 49.</text>
      <biological_entity id="o12921" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12922" name="style" name_original="style" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12923" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="48" value_original="48" />
        <character name="quantity" src="d0_s8" value="49" value_original="49" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May in south; late May–early Jun in north.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in south" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy pinelands, cypress bogs, wet savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy pinelands" />
        <character name="habitat" value="cypress bogs" />
        <character name="habitat" value="wet savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>