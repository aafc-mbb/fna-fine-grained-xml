<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1812" rank="genus">hymenocallis</taxon_name>
    <taxon_name authority="Traub" date="1962" rank="species">pygmaea</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>18: 70. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hymenocallis;species pygmaea</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101684</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb rhizomatous, narrowly ovoid, 1.3–3 × 1–2 cm;</text>
      <biological_entity id="o11595" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s0" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal plate ca. 1 cm;</text>
      <biological_entity constraint="basal" id="o11596" name="plate" name_original="plate" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>neck 2–5 cm;</text>
      <biological_entity id="o11597" name="neck" name_original="neck" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tunic gray.</text>
      <biological_entity id="o11598" name="tunic" name_original="tunic" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, 2–5, suberect, 1.5–4 dm × 0.5–1.3 (–2) cm, coriaceous;</text>
      <biological_entity id="o11599" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="suberect" value_original="suberect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="length" src="d0_s4" to="4" to_unit="dm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1.3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade shiny green, narrowly liguliform, apex subacute.</text>
      <biological_entity id="o11600" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="liguliform" value_original="liguliform" />
      </biological_entity>
      <biological_entity id="o11601" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 1.2–3 (–4.2) dm, 2-edged, glaucous;</text>
      <biological_entity id="o11602" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s6" to="4.2" to_unit="dm" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="some_measurement" src="d0_s6" to="3" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-edged" value_original="2-edged" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scape bracts 2, enclosing buds, 2–3 × 0.5–1 cm;</text>
      <biological_entity constraint="scape" id="o11603" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11604" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <relation from="o11603" id="r1629" name="enclosing" negation="false" src="d0_s7" to="o11604" />
    </statement>
    <statement id="d0_s8">
      <text>subtending floral bracts 1.5–2.5 cm × 3–5 mm.</text>
      <biological_entity constraint="subtending floral" id="o11605" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 1–2, opening sequentially, with lemony fragrance;</text>
      <biological_entity id="o11606" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth-tube green, 4–6 cm;</text>
      <biological_entity id="o11607" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s10" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals slightly ascending, white, tinged green on keel, 5–7 cm × 3–6 mm;</text>
      <biological_entity id="o11608" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character constraint="on keel" constraintid="o11609" is_modifier="false" name="coloration" src="d0_s11" value="tinged green" value_original="tinged green" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s11" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11609" name="keel" name_original="keel" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>corona white with small green eye, funnelform at full anthesis, then gradually spreading, shortly tubulose proximally, 2–3 × ca. 3 cm, margins between free portions of filaments irregularly dentate, projections small;</text>
      <biological_entity id="o11610" name="corona" name_original="corona" src="d0_s12" type="structure">
        <character constraint="with eye" constraintid="o11611" is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11611" name="eye" name_original="eye" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="small green" value_original="small green" />
      </biological_entity>
      <biological_entity constraint="between portions" constraintid="o11613" id="o11612" name="margin" name_original="margins" src="d0_s12" type="structure" constraint_original="between  portions, ">
        <character is_modifier="true" name="architecture" src="d0_s12" value="full" value_original="full" />
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="gradually" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="true" modifier="shortly" name="architecture_or_shape" src="d0_s12" value="tubulose" value_original="tubulose" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="length" src="d0_s12" to="3" to_unit="cm" />
        <character is_modifier="true" name="width" src="d0_s12" unit="cm" value="3" value_original="3" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s12" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o11613" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o11614" name="filament" name_original="filaments" src="d0_s12" type="structure" />
      <biological_entity id="o11615" name="projection" name_original="projections" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="small" value_original="small" />
      </biological_entity>
      <relation from="o11610" id="r1630" name="at" negation="false" src="d0_s12" to="o11612" />
      <relation from="o11612" id="r1631" name="part_of" negation="false" src="d0_s12" to="o11614" />
    </statement>
    <statement id="d0_s13">
      <text>free portions of filaments inserted on flat sinal base, suberect, white, 1.8–2.8 cm;</text>
      <biological_entity id="o11616" name="portion" name_original="portions" src="d0_s13" type="structure" constraint="filament" constraint_original="filament; filament">
        <character is_modifier="true" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s13" to="2.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11617" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o11618" name="sinal" name_original="sinal" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o11619" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o11616" id="r1632" name="part_of" negation="false" src="d0_s13" to="o11617" />
      <relation from="o11616" id="r1633" name="inserted on" negation="false" src="d0_s13" to="o11618" />
      <relation from="o11616" id="r1634" name="inserted on" negation="false" src="d0_s13" to="o11619" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 1–1.5 cm, pollen golden;</text>
      <biological_entity id="o11620" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11621" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="golden" value_original="golden" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary ovoid, ca. 1 cm × 5 mm, ovules 2 per locule;</text>
      <biological_entity id="o11622" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character name="length" src="d0_s15" unit="cm" value="1" value_original="1" />
        <character name="width" src="d0_s15" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o11623" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character constraint="per locule" constraintid="o11624" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11624" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style green in distal 1/3, fading to white proximally, 10–13 cm.</text>
      <biological_entity id="o11625" name="style" name_original="style" src="d0_s16" type="structure">
        <character constraint="in distal 1/3" constraintid="o11626" is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="fading" modifier="proximally" name="coloration" notes="" src="d0_s16" to="white" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s16" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11626" name="1/3" name_original="1/3" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules subglobose, ca. 2 × 2 cm.</text>
      <biological_entity id="o11627" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="subglobose" value_original="subglobose" />
        <character name="length" src="d0_s17" unit="cm" value="2" value_original="2" />
        <character name="width" src="d0_s17" unit="cm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds obovoid, ca. 1.5 × 1 cm. 2n = 40, 41.</text>
      <biological_entity id="o11628" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character name="length" src="d0_s18" unit="cm" value="1.5" value_original="1.5" />
        <character name="width" src="d0_s18" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11629" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
        <character name="quantity" src="d0_s18" value="41" value_original="41" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs and stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Dwarf spider-lily</other_name>
  <discussion>Hymenocallis pygmaea was described from bulbs collected by Mary G. Henry along a small tributary of the Waccamaw River in South Carolina. Populations that we have studied and vouchered from the Waccamaw River drainage are characterized by extremely narrow, coriaceous, nearly erect, shiny leaves and small flowers. This species could be considered a cytological and/or ecological variant of H. crassifolia, but because of its smaller stature and features it is maintained here as a separate species. Additional studies on the dwarf spider-lilies of the Atlantic Coastal Plain should be undertaken to clarify their taxonomic status.</discussion>
  
</bio:treatment>