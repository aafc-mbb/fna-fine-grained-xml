<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">427</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Torrey in W. H. Emory" date="1859" rank="species">baccata</taxon_name>
    <taxon_name authority="L. D. Benson &amp; R. A. Darrow" date="1943" rank="variety">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>30: 234. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species baccata;variety brevifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102332</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="Schott ex Trelease" date="unknown" rank="species">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>13: 100, plates 57–59. 1902,</place_in_publication>
      <other_info_on_pub>not Engelmann 1871</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Yucca;species brevifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="McKelvey" date="unknown" rank="species">arizonica</taxon_name>
    <taxon_hierarchy>genus Yucca;species arizonica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="McKelvey" date="unknown" rank="species">confinis</taxon_name>
    <taxon_hierarchy>genus Yucca;species confinis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="McKelvey" date="unknown" rank="species">thornberi</taxon_name>
    <taxon_hierarchy>genus Yucca;species thornberi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="J. F. Macbride" date="unknown" rank="species">treleasei</taxon_name>
    <place_of_publication>
      <place_in_publication>1918</place_in_publication>
      <other_info_on_pub>not Sprenger 1906</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Yucca;species treleasei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants caulescent.</text>
      <biological_entity id="o33892" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–24, aerial, often branched, some reaching 2 m.</text>
      <biological_entity id="o33893" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="24" />
        <character is_modifier="false" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf margins filiferous.</text>
      <biological_entity constraint="leaf" id="o33894" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="filiferous" value_original="filiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences arising almost completely within to mostly extending beyond rosettes;</text>
      <biological_entity id="o33895" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character constraint="within" is_modifier="false" modifier="almost completely" name="orientation" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o33896" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <relation from="o33895" id="r4567" modifier="mostly" name="extending beyond" negation="false" src="d0_s3" to="o33896" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle to 0.3 m.</text>
      <biological_entity id="o33897" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s4" to="0.3" to_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hills, mesas, flats in Sonoran Desert, desert grasslands, oak woodlands, 500–2000 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hills" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="flats" constraint="in sonoran desert" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="2000 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <other_name type="common_name">Thornber yucca</other_name>
  
</bio:treatment>