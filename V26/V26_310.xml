<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lilium</taxon_name>
    <taxon_name authority="Kellogg" date="1859" rank="species">washingtonianum</taxon_name>
    <taxon_name authority="(Stearn) M. W. Skinner" date="2002" rank="subspecies">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 258. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus lilium;species washingtonianum;subspecies purpurascens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102265</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Purdy" date="unknown" rank="species">washingtonianum</taxon_name>
    <taxon_name authority="Stearn" date="unknown" rank="variety">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Chron., ser.</publication_title>
      <place_in_publication>3, 124: 13. 1948</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lilium;species washingtonianum;variety purpurascens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpureum</taxon_name>
    <taxon_hierarchy>genus Lilium;species purpureum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">washingtonianum</taxon_name>
    <taxon_name authority="(Purdy) Purdy" date="unknown" rank="variety">purpureum</taxon_name>
    <place_of_publication>
      <place_in_publication>1919</place_in_publication>
      <other_info_on_pub>not W. Bull ex Baker 1874</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Lilium;species washingtonianum;variety purpureum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs variable, subrhizomatous to ± ovoid, 3–10 × 4.4–11.7 cm, 0.3–0.9 (–1.4) times taller than long;</text>
      <biological_entity id="o25942" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="variability" src="d0_s0" value="variable" value_original="variable" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="subrhizomatous" value_original="subrhizomatous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="4.4" from_unit="cm" name="width" src="d0_s0" to="11.7" to_unit="cm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="0.3-0.9(-1.4) times taller than long" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales variable, usually notched with 2 (–3) poorly defined segments, some clearly 2-segmented or unsegmented, longest 3.3–11.9 cm.</text>
      <biological_entity id="o25943" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="variable" value_original="variable" />
        <character constraint="with segments" constraintid="o25944" is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="notched" value_original="notched" />
        <character is_modifier="false" modifier="clearly" name="shape" notes="" src="d0_s1" value="2-segmented" value_original="2-segmented" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="3.3" from_unit="cm" name="some_measurement" src="d0_s1" to="11.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25944" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="true" modifier="poorly" name="prominence" src="d0_s1" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems to 1.7 m.</text>
      <biological_entity id="o25945" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="1.7" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in 1–8 whorls or partial whorls, 3–15 leaves per whorl, usually ascending and occasionally nearly clasping stem, rarely horizontal and drooping at tips, 3.7–12.3 × 0.9–4.7 cm, 2–6.5 times longer than wide;</text>
      <biological_entity id="o25946" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25948" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="partial" value_original="partial" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o25949" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" notes="" src="d0_s3" value="horizontal" value_original="horizontal" />
        <character constraint="at tips" constraintid="o25952" is_modifier="false" name="orientation" src="d0_s3" value="drooping" value_original="drooping" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="length" notes="" src="d0_s3" to="12.3" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" notes="" src="d0_s3" to="4.7" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-6.5" value_original="2-6.5" />
      </biological_entity>
      <biological_entity id="o25950" name="whorl" name_original="whorl" src="d0_s3" type="structure" />
      <biological_entity id="o25951" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="occasionally nearly" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o25952" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <relation from="o25946" id="r3526" name="in" negation="false" src="d0_s3" to="o25948" />
      <relation from="o25949" id="r3527" name="per" negation="false" src="d0_s3" to="o25950" />
    </statement>
    <statement id="d0_s4">
      <text>margins undulate.</text>
      <biological_entity id="o25953" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals and petals recurved 2/3 along length from base, mostly or entirely white but aging deep pink or lavender, sometimes with short yellowish stripe extending from basal median nectaries;</text>
      <biological_entity id="o25954" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25955" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="mostly; entirely" name="coloration" notes="" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o25956" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character constraint="from base" constraintid="o25957" name="length" src="d0_s5" value="2/3" value_original="2/3" />
        <character is_modifier="false" modifier="mostly; entirely" name="coloration" notes="" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o25957" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o25958" name="extending" name_original="extending" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellowish stripe" value_original="yellowish stripe" />
      </biological_entity>
      <biological_entity constraint="basal median" id="o25959" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <relation from="o25955" id="r3528" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o25958" />
      <relation from="o25956" id="r3529" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o25958" />
      <relation from="o25958" id="r3530" name="from" negation="false" src="d0_s5" to="o25959" />
    </statement>
    <statement id="d0_s6">
      <text>sepals usually if obscurely purplish adaxially, (6.1–) 6.7–9.5 × 0.9–1.6 cm;</text>
      <biological_entity id="o25960" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25961" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="if" is_modifier="false" modifier="obscurely; adaxially" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="6.1" from_unit="cm" name="atypical_length" src="d0_s6" to="6.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6.7" from_unit="cm" name="length" src="d0_s6" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s6" to="1.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals (6.1–) 6.6–9.5 × 1.1–1.9 cm;</text>
      <biological_entity id="o25962" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o25963" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="6.1" from_unit="cm" name="atypical_length" src="d0_s7" to="6.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6.6" from_unit="cm" name="length" src="d0_s7" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s7" to="1.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers cream, sometimes spotted minutely with magenta on abaxial surface, becoming yellow;</text>
      <biological_entity id="o25964" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25965" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character constraint="on abaxial surface" constraintid="o25966" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
        <character is_modifier="false" modifier="becoming" name="coloration" notes="" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25966" name="surface" name_original="surface" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pollen pale-yellow to occasionally yellow.</text>
      <biological_entity id="o25967" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25968" name="pollen" name_original="pollen" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s9" to="occasionally yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules usually with 6 longitudinal ridges, 2.8–5.8 × 1.6–2.9 cm, 1.3–2.3 times longer than wide.</text>
      <biological_entity id="o25969" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" notes="" src="d0_s10" to="5.8" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" notes="" src="d0_s10" to="2.9" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s10" value="1.3-2.3" value_original="1.3-2.3" />
      </biological_entity>
      <biological_entity id="o25970" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o25969" id="r3531" name="with" negation="false" src="d0_s10" to="o25970" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds 144–231.2n = 24.</text>
      <biological_entity id="o25971" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="144" name="quantity" src="d0_s11" to="231" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25972" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (mid Jun–mid Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="mid Jun-mid Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forest openings, chaparral, burned clearcuts, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="clearcuts" modifier="burned" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <other_name type="common_name">Cascade lily</other_name>
  <discussion>Subspecies purpurascens replaces the Sierran subsp. washingtonianum near Mount Shasta in Siskiyou County, California, and extends west through the Klamath Mountains and north through the Cascade ranges to Mount Hood in Clackamas County, Oregon. In addition to the characters mentioned in the key, subsp. purpurascens is also distinguished from subsp. washingtonianum by more compact bulbs with longer scales.</discussion>
  
</bio:treatment>