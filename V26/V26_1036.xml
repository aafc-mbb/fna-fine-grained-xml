<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">508</other_info_on_meta>
    <other_info_on_meta type="treatment_page">509</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="(Lindley) Szlachetko" date="1995" rank="subfamily">Vanilloideae</taxon_name>
    <taxon_name authority="Blume" date="1837" rank="tribe">Vanilleae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">Vanillinae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">vanilla</taxon_name>
    <taxon_name authority="Reichenbach f." date="1865" rank="species">barbellata</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>48: 274. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe vanillinae;genus vanilla;species barbellata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242102044</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vanilla</taxon_name>
    <taxon_name authority="Northrop" date="unknown" rank="species">articulata</taxon_name>
    <taxon_hierarchy>genus Vanilla;species articulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots usually 1–2 per node, gray, 1–3 mm diam. when aerial, thicker and villous when in contact with substrate, glabrous.</text>
      <biological_entity id="o18766" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" constraint="per node" constraintid="o18767" from="1" name="quantity" src="d0_s0" to="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="gray" value_original="gray" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="when aerial" name="diameter" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s0" value="thicker" value_original="thicker" />
        <character is_modifier="false" modifier="when in-contact-with substrate" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18767" name="node" name_original="node" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems occasionally branched, 3–9 mm diam., smooth.</text>
      <biological_entity id="o18768" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s1" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves early deciduous;</text>
      <biological_entity id="o18769" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broad basally, otherwise narrowly lanceolate, relatively thin, to 4 × 0.8 cm.</text>
      <biological_entity id="o18770" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="otherwise narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary on short lateral branches, several to many-flowered racemes, 1.5–6 cm excluding peduncle;</text>
      <biological_entity id="o18771" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="on lateral branches" constraintid="o18772" is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s4" value="several" value_original="several" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18772" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o18773" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
      <biological_entity id="o18774" name="peduncle" name_original="peduncle" src="d0_s4" type="structure" />
      <relation from="o18771" id="r2588" name="excluding" negation="false" src="d0_s4" to="o18774" />
    </statement>
    <statement id="d0_s5">
      <text>floral bracts broadly ovate, 4–12 mm, fleshy.</text>
      <biological_entity constraint="floral" id="o18775" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals and petals green, somewhat spreading, distinct and free;</text>
      <biological_entity id="o18776" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18777" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o18778" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals oblong-oblanceolate, 3–4 × 0.9–1.2 cm;</text>
      <biological_entity id="o18779" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18780" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblong-oblanceolate, slightly falcate, dorsally keeled, 3–4 × 1–1.3 cm, apex acute to obtuse;</text>
      <biological_entity id="o18781" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18782" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18783" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip greenish abaxially, deep red adaxially, shading to white margin, with broad, yellow midrib, overall triangular-obovate, medially thickened, apex 3-lobed, lateral lobes arching over column, orbiculate, margins involute, sinuses 4–5 mm deep, middle lobe reflexed, fleshy;</text>
      <biological_entity id="o18784" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18785" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="shading" name="coloration" src="d0_s9" to="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="triangular-obovate" value_original="triangular-obovate" />
        <character is_modifier="false" modifier="medially" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o18786" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <biological_entity id="o18787" name="midrib" name_original="midrib" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18788" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18789" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character constraint="over column" constraintid="o18790" is_modifier="false" name="orientation" src="d0_s9" value="arching" value_original="arching" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
      <biological_entity id="o18790" name="column" name_original="column" src="d0_s9" type="structure" />
      <biological_entity id="o18791" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o18792" name="sinuse" name_original="sinuses" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="middle" id="o18793" name="lobe" name_original="lobe" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="texture" src="d0_s9" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o18785" id="r2589" name="with" negation="false" src="d0_s9" to="o18787" />
    </statement>
    <statement id="d0_s10">
      <text>disc with tuft of rigid, retrorse bristles;</text>
      <biological_entity id="o18794" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18795" name="disc" name_original="disc" src="d0_s10" type="structure" />
      <biological_entity id="o18796" name="tuft" name_original="tuft" src="d0_s10" type="structure" />
      <biological_entity id="o18797" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character is_modifier="true" name="texture" src="d0_s10" value="rigid" value_original="rigid" />
        <character is_modifier="true" name="orientation" src="d0_s10" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <relation from="o18795" id="r2590" name="with" negation="false" src="d0_s10" to="o18796" />
      <relation from="o18796" id="r2591" name="part_of" negation="false" src="d0_s10" to="o18797" />
    </statement>
    <statement id="d0_s11">
      <text>claw and basal margins adnate to proximal 1/2 of column;</text>
      <biological_entity id="o18798" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18799" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18800" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character constraint="of column" constraintid="o18801" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o18801" name="column" name_original="column" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>column straight, semiterete, 2.3–3.3 cm;</text>
      <biological_entity id="o18802" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18803" name="column" name_original="column" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s12" value="semiterete" value_original="semiterete" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="some_measurement" src="d0_s12" to="3.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicellate ovary 3–4.5 cm.</text>
      <biological_entity id="o18804" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18805" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s13" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries pendent, fusiform-cylindric, slightly curved, 7–9 cm × 9–13 mm.</text>
      <biological_entity id="o18806" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform-cylindric" value_original="fusiform-cylindric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s14" to="9" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mangroves, coastal hammocks, bay tree islands, rocky pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mangroves" />
        <character name="habitat" value="coastal hammocks" />
        <character name="habitat" value="bay tree islands" />
        <character name="habitat" value="rocky pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Worm vine</other_name>
  <discussion>Vanilla barbellata is known in the flora area from several keys in Everglades National Park, Miami, Miami-Dade and Monroe counties, Florida (P. M. Brown 2002).</discussion>
  
</bio:treatment>