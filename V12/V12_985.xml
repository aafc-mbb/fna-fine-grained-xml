<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">MANIHOT</taxon_name>
    <taxon_name authority="Hooker" date="1843" rank="species">grahamii</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Pl.</publication_title>
      <place_in_publication>6: plate 530. 1843</place_in_publication>
      <other_info_on_pub>(as grahami)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus manihot;species grahamii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250016243</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Hardy tapioca</other_name>
  <other_name type="common_name">Graham's manihot or cassava</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 2–6 [–7] m.</text>
      <biological_entity id="o11692" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots not thickened.</text>
      <biological_entity id="o11694" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, angled when young;</text>
      <biological_entity id="o11695" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when young" name="shape" src="d0_s2" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes not swollen;</text>
      <biological_entity id="o11696" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaf and stipule-scars not elevated.</text>
      <biological_entity id="o11697" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="elevated" value_original="elevated" />
      </biological_entity>
      <biological_entity id="o11698" name="stipule-scar" name_original="stipule-scars" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="elevated" value_original="elevated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous;</text>
      <biological_entity id="o11699" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules linear, remotely serrate;</text>
      <biological_entity id="o11700" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole 5–33 cm;</text>
      <biological_entity id="o11701" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="33" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade basally attached, 5–13-lobed, median and adjacent lobes with pair of weakly defined rounded secondary lobes distal to middle, lateral lobes without secondary lobes, median lobe 5–24 cm, margins neither thickened nor revolute, entire, apex acuminate, surface glabrous, abaxial smooth.</text>
      <biological_entity id="o11702" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="fixation" src="d0_s8" value="attached" value_original="attached" />
        <character is_modifier="false" name="shape" src="d0_s8" value="5-13-lobed" value_original="5-13-lobed" />
      </biological_entity>
      <biological_entity constraint="median" id="o11703" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o11704" name="pair" name_original="pair" src="d0_s8" type="structure">
        <character char_type="range_value" from="distal" name="position" src="d0_s8" to="middle" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o11705" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="weakly" name="prominence" src="d0_s8" value="defined" value_original="defined" />
        <character is_modifier="true" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11706" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity constraint="secondary" id="o11707" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity constraint="median" id="o11708" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="24" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11709" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11710" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o11711" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11712" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o11703" id="r963" name="with" negation="false" src="d0_s8" to="o11704" />
      <relation from="o11704" id="r964" name="part_of" negation="false" src="d0_s8" to="o11705" />
      <relation from="o11706" id="r965" name="without" negation="false" src="d0_s8" to="o11707" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences axillary, panicles, to 30 cm.</text>
      <biological_entity id="o11713" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o11714" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels: staminate 4–10 mm;</text>
      <biological_entity id="o11715" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate 10–40 mm in fruit, straight.</text>
      <biological_entity id="o11716" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o11717" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character is_modifier="false" name="course" notes="" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o11717" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: calyx campanulate, 10–15 mm, lobes erect or spreading;</text>
      <biological_entity id="o11718" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o11719" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11720" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10.</text>
      <biological_entity id="o11721" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o11722" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 1.8 cm, smooth, not winged.</text>
      <biological_entity id="o11723" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="cm" value="1.8" value_original="1.8" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds oblong, 10–12 mm.</text>
      <biological_entity id="o11724" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Manihot grahamii is native to northern Argentina, southeastern Brazil, Paraguay, and Uruguay, and is sometimes cultivated for its distinctive, attractive foliage. The flowers are relatively inconspicuous, but are much-visited by bees. This is the most cold-tolerant Manihot species; above-ground stems survive light frosts and if severe cold kills the aerial shoot system outright, new stems can regenerate from underground parts. It survives well and self-sows in garden settings as far north as tidewater Virginia; northern limits for the persistence of plants escaping from cultivation have yet to be established. In addition to characteristics noted in the key, herbarium specimens frequently exhibit contracted petiole bases.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug; fruiting Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, spreading from cultivation.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
        <character name="habitat" value="cultivation" modifier="spreading from" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Fla., Ga., La., Miss., Tex.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>