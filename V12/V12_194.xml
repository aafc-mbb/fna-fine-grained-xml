<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="Greene" date="1894" rank="species">jepsonii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. San Francisco,</publication_title>
      <place_in_publication>78. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species jepsonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101457</other_info_on_name>
  </taxon_identification>
  <number>47.</number>
  <other_name type="common_name">Jepson’s ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–1.5 m.</text>
      <biological_entity id="o8038" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, not rooting at nodes;</text>
      <biological_entity id="o8039" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character constraint="at nodes" constraintid="o8040" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o8040" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets reddish to grayish brown, rigid, puberulent, glabrescent.</text>
      <biological_entity id="o8041" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s2" to="grayish brown" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled, deflexed;</text>
      <biological_entity id="o8042" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="deflexed" value_original="deflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–2 mm;</text>
      <biological_entity id="o8043" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ± cupped, slightly folded lengthwise adaxially, elliptic to ± oblong, 10–20 × 5–13 mm, base rounded, margins thick or slightly revolute, spinose-dentate, teeth 7–11, apex rounded or sharply acute, abaxial surface pale yellowish green, glabrous, adaxial surface pale green, glabrous.</text>
      <biological_entity id="o8044" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly; lengthwise adaxially; adaxially" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="more or less oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8045" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8046" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinose-dentate" value_original="spinose-dentate" />
      </biological_entity>
      <biological_entity id="o8047" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="11" />
      </biological_entity>
      <biological_entity id="o8048" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8049" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale yellowish" value_original="pale yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8050" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary or terminal, 1–2 cm.</text>
      <biological_entity id="o8051" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals and petals (5–) 6 (–8), usually blue to lavender or white, rarely pink;</text>
      <biological_entity id="o8052" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o8053" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually blue" name="coloration" src="d0_s7" to="lavender or white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o8054" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s7" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="8" />
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character char_type="range_value" from="usually blue" name="coloration" src="d0_s7" to="lavender or white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary blue;</text>
      <biological_entity id="o8055" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8056" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens (5–) 6 (–8).</text>
      <biological_entity id="o8057" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8058" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s9" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 5–7 mm wide, lobed;</text>
      <biological_entity id="o8059" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves rugose, horns subapical, prominent, thick, erect, rugose, intermediate ridges present.</text>
      <biological_entity id="o8060" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o8061" name="horn" name_original="horns" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="width" src="d0_s11" value="thick" value_original="thick" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o8062" name="ridge" name_original="ridges" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="intermediate" value_original="intermediate" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus jepsonii, composed of two, allopatric varieties, is the only species with mostly six (rarely eight) sepals and petals, and cymules reduced to solitary flowers (M. A. Nobs 1963). T. M. Hardig et al. (2000) provided evidence showing that the two varieties may not form a monophyletic group. H. McMinn (1942) and Nobs (1963) reported putative hybrids with C. cuneatus and C. prostratus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and petals usually pale blue to lavender, rarely pink or white; capsules globose.</description>
      <determination>47a. Ceanothus jepsonii var. jepsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and petals white; capsules ± oblong.</description>
      <determination>47b. Ceanothus jepsonii var. albiflorus</determination>
    </key_statement>
  </key>
</bio:treatment>