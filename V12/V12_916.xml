<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">hirta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 434. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species hirta</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242413881</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Tropical wood-sorrel</other_name>
  <discussion>Varieties 5–7 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, Calif.; Africa (South Africa); introduced also in Europe, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Africa (South Africa)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Oxalis hirta recognized by its rhizomatous habit, sessile to subsessile leaves with unlobed leaflets, and large, solitary, axillary flowers.</text>
      <biological_entity id="o2276" name="whole-organism" name_original="" src="d0_s0" type="structure" />
      <biological_entity id="o2277" name="habit" name_original="habit" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity id="o2278" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="subsessile" value_original="subsessile" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity id="o2279" name="leaflet" name_original="leaflets" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="subsessile" value_original="subsessile" />
        <character is_modifier="true" name="shape" src="d0_s0" value="unlobed" value_original="unlobed" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o2280" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="subsessile" value_original="subsessile" />
        <character is_modifier="true" name="shape" src="d0_s0" value="unlobed" value_original="unlobed" />
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o2276" id="r202" name="recognized by its" negation="false" src="d0_s0" to="o2277" />
      <relation from="o2276" id="r203" name="recognized by its" negation="false" src="d0_s0" to="o2278" />
      <relation from="o2276" id="r204" name="recognized by its" negation="false" src="d0_s0" to="o2279" />
      <relation from="o2276" id="r205" name="recognized by its" negation="false" src="d0_s0" to="o2280" />
    </statement>
    <statement id="d0_s1">
      <text>Plants apparently do not fruit in California.</text>
      <biological_entity id="o2282" name="fruit" name_original="fruit" src="d0_s1" type="structure" />
      <biological_entity id="o2283" name="california" name_original="california" src="d0_s1" type="structure" />
      <relation from="o2281" id="r206" name="do" negation="false" src="d0_s1" to="o2282" />
      <relation from="o2281" id="r207" name="in" negation="false" src="d0_s1" to="o2283" />
    </statement>
    <statement id="d0_s2">
      <text>T. M.</text>
    </statement>
    <statement id="d0_s3">
      <text>Salter (1944) referred to O. hirta as a polymorphous group-species and recognized seven varieties in South Africa, primarily based on variation in habit and corolla color (white to pink, purplish, or yellowish) and shape (funnelform to cylindric).</text>
      <biological_entity id="o2281" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="as group-species" constraintid="o2284" name="atypical_quantity" src="d0_s3" value="1944" value_original="1944" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2284" name="group-species" name_original="group-species" src="d0_s3" type="taxon_name" />
      <biological_entity id="o2285" name="variety" name_original="varieties" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o2286" name="africa" name_original="africa" src="d0_s3" type="structure">
        <character is_modifier="true" name="geographical_terms" src="d0_s3" value="south" value_original="south" />
      </biological_entity>
      <biological_entity id="o2287" name="variation" name_original="variation" src="d0_s3" type="structure" />
      <biological_entity id="o2288" name="corolla" name_original="corolla" src="d0_s3" type="structure" />
      <relation from="o2284" id="r208" name="recognized" negation="false" src="d0_s3" to="o2285" />
      <relation from="o2286" id="r209" modifier="primarily" name="based on" negation="false" src="d0_s3" to="o2287" />
    </statement>
  </description>
  
</bio:treatment>