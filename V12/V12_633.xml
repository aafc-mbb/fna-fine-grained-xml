<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="illustration_page">322</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Graham) Baillon" date="1858" rank="section">Poinsettia</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">radians</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>8. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section poinsettia;species radians</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101661</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poinsettia</taxon_name>
    <taxon_name authority="(Bentham) Klotzsch &amp; Garcke" date="unknown" rank="species">radians</taxon_name>
    <taxon_hierarchy>genus poinsettia;species radians</taxon_hierarchy>
  </taxon_identification>
  <number>139.</number>
  <other_name type="common_name">Sun spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with moniliform tuberous rootstock.</text>
      <biological_entity id="o4695" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4696" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="moniliform" value_original="moniliform" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o4695" id="r423" name="with" negation="false" src="d0_s0" to="o4696" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 5–20 (–30) cm, usually glabrous, occasionally puberulent;</text>
      <biological_entity id="o4697" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches ± straight.</text>
      <biological_entity id="o4698" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
      <biological_entity id="o4699" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–2 mm, glabrous or strigose;</text>
      <biological_entity id="o4700" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-lanceolate to ovate or broadly elliptic, 25–50 × 3–20 mm, unlobed, base rounded (tapered to petiole), margins with few glandular teeth, strigillose, flat to revolute, apex acute, abaxial surface coarsely strigose, adaxial surface strigose-hirsute;</text>
      <biological_entity id="o4701" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="ovate or broadly elliptic" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o4702" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4703" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="strigillose" value_original="strigillose" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="revolute" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o4704" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o4705" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4706" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <relation from="o4703" id="r424" name="with" negation="false" src="d0_s5" to="o4704" />
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate, midvein prominent.</text>
      <biological_entity constraint="adaxial" id="o4707" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose-hirsute" value_original="strigose-hirsute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o4708" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathial arrangement: terminal pleiochasial branches usually 3, occasionally reduced to congested cyme, 1–2-branched (often highly condensed);</text>
      <biological_entity id="o4709" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4710" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pleiochasial" value_original="pleiochasial" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="1-2-branched" value_original="1-2-branched" />
      </biological_entity>
      <biological_entity id="o4711" name="cyme" name_original="cyme" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pleiochasial bracts 6–8 (–10), as tight involucrate whorl, wholly white to pale-pink or red, usually narrower than distal leaves;</text>
      <biological_entity id="o4712" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o4713" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="10" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="8" />
        <character char_type="range_value" from="wholly white" name="coloration" notes="" src="d0_s8" to="pale-pink or red" />
        <character constraint="than distal leaves" constraintid="o4715" is_modifier="false" name="width" src="d0_s8" value="usually narrower" value_original="usually narrower" />
      </biological_entity>
      <biological_entity id="o4714" name="whorl" name_original="whorl" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s8" value="tight" value_original="tight" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="involucrate" value_original="involucrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4715" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o4713" id="r425" name="as" negation="false" src="d0_s8" to="o4714" />
    </statement>
    <statement id="d0_s9">
      <text>dichasial bracts linear and highly reduced.</text>
      <biological_entity id="o4716" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o4717" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="highly" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cyathia: peduncle 2–5.5 mm.</text>
      <biological_entity id="o4718" name="cyathium" name_original="cyathia" src="d0_s10" type="structure" />
      <biological_entity id="o4719" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucre broadly globose-cupulate, 1.7–2.1 × 2.2–2.5 mm, glabrous or puberulent;</text>
      <biological_entity id="o4720" name="involucre" name_original="involucre" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="globose-cupulate" value_original="globose-cupulate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s11" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>involucral lobes divided into triangular segments;</text>
      <biological_entity id="o4721" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s12" value="involucral" value_original="involucral" />
        <character constraint="into segments" constraintid="o4722" is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o4722" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>glands 1–4 (–5), white, sessile and broadly attached, 1.1 × 1.4 mm, opening oblong, glabrous;</text>
      <biological_entity id="o4723" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="broadly" name="fixation" src="d0_s13" value="attached" value_original="attached" />
        <character name="length" src="d0_s13" unit="mm" value="1.1" value_original="1.1" />
        <character name="width" src="d0_s13" unit="mm" value="1.4" value_original="1.4" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>appendages absent.</text>
      <biological_entity id="o4724" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers 20–25.</text>
      <biological_entity id="o4725" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s15" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary glabrous or puberulent, styles 3–4 mm, 2-fid 1/2 to nearly entire length.</text>
      <biological_entity id="o4726" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4727" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4728" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="2-fid" value_original="2-fid" />
        <character name="quantity" src="d0_s16" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s16" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules depressed-globose, 3.8–5 × 4–5 mm, 3-lobed, glabrous or puberulent;</text>
      <biological_entity id="o4729" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s17" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella 3.6–4.5 mm.</text>
      <biological_entity id="o4730" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s18" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds white, mottled brown to gray, ellipsoid, rounded in cross-section, 4–4.6 × 2.4–3.2 mm, smoothly and broadly pitted or grooved;</text>
      <biological_entity id="o4731" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" value_original="white" />
        <character char_type="range_value" from="mottled brown" name="coloration" src="d0_s19" to="gray" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character constraint="in cross-section" constraintid="o4732" is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s19" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" notes="" src="d0_s19" to="3.2" to_unit="mm" />
        <character is_modifier="false" modifier="smoothly; broadly" name="relief" src="d0_s19" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="smoothly" name="architecture" src="d0_s19" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o4732" name="cross-section" name_original="cross-section" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>caruncle 0.1 mm.</text>
      <biological_entity id="o4733" name="caruncle" name_original="caruncle" src="d0_s20" type="structure">
        <character name="some_measurement" src="d0_s20" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia radians is widely distributed but scattered from the Sonoran and Chihuahuan deserts south to Oaxaca in Mexico. The species is distinct among species in sect. Poinsettia in the flora area in its precocious habit, often flowering before the leaves emerge.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinyon-juniper woodlands, oak savannas, desert grasslands and scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="oak savannas" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>