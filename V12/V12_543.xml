<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="Reveal" date="2002" rank="species">rhizomata</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>27: 763, figs. 1, 2. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species rhizomata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101827</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuttallia</taxon_name>
    <taxon_name authority="(Reveal) W. A. Weber &amp; R. C. Wittmann" date="unknown" rank="species">rhizomata</taxon_name>
    <taxon_hierarchy>genus nuttallia;species rhizomata</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Roan Cliffs blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, bushlike, with rhizomes.</text>
      <biological_entity id="o29766" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushlike" value_original="bushlike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29767" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o29766" id="r2443" name="with" negation="false" src="d0_s0" to="o29767" />
    </statement>
    <statement id="d0_s1">
      <text>Stems multiple, erect or decumbent, zigzag;</text>
      <biological_entity id="o29768" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches along entire stem, distal longest, antrorse, upcurved;</text>
      <biological_entity id="o29769" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o29770" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o29769" id="r2444" name="along" negation="false" src="d0_s2" to="o29770" />
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="distal" id="o29771" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="upcurved" value_original="upcurved" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 8.1–29 × 4.1–12.7 mm, widest intersinus distance 4.1–7.4 mm;</text>
      <biological_entity id="o29772" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o29773" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="8.1" from_unit="mm" name="length" src="d0_s4" to="29" to_unit="mm" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="width" src="d0_s4" to="12.7" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="some_measurement" src="d0_s4" to="7.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate, margins entire;</text>
      <biological_entity id="o29774" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o29775" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o29776" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal elliptic, base not clasping, margins entire or dentate, teeth 0–8, perpendicular to leaf axis, 0.6–2.7 mm;</text>
      <biological_entity id="o29777" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o29778" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o29779" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o29780" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o29781" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="8" />
        <character constraint="to leaf axis" constraintid="o29782" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o29782" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with simple grappling-hook and needlelike trichomes, adaxial surface with needlelike trichomes.</text>
      <biological_entity id="o29783" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o29784" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o29785" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o29786" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29787" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o29788" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o29784" id="r2445" name="with" negation="false" src="d0_s7" to="o29785" />
      <relation from="o29784" id="r2446" name="with" negation="false" src="d0_s7" to="o29786" />
      <relation from="o29787" id="r2447" name="with" negation="false" src="d0_s7" to="o29788" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o29789" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o29790" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 12.5–17.2 × 6.1–11.2 mm, apex rounded to obtuse, glabrous abaxially;</text>
      <biological_entity id="o29791" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29792" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="12.5" from_unit="mm" name="length" src="d0_s9" to="17.2" to_unit="mm" />
        <character char_type="range_value" from="6.1" from_unit="mm" name="width" src="d0_s9" to="11.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29793" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s9" to="obtuse" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, strongly clawed, 7–9.5 × 2.7–6 mm, with anthers, second whorl with anthers;</text>
      <biological_entity id="o29794" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29795" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o29796" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o29797" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29798" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o29799" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o29800" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o29797" id="r2448" name="with" negation="false" src="d0_s10" to="o29798" />
      <relation from="o29799" id="r2449" name="with" negation="false" src="d0_s10" to="o29800" />
    </statement>
    <statement id="d0_s11">
      <text>anthers straight after dehiscence, epidermis smooth;</text>
      <biological_entity id="o29801" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o29802" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o29803" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 7–10 mm.</text>
      <biological_entity id="o29804" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o29805" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped, 7–10 × 6.5–8 mm, base rounded, not longitudinally ridged.</text>
      <biological_entity id="o29806" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="width" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29807" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls straight, papillae 9–13 per cell.</text>
      <biological_entity id="o29808" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o29809" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o29810" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o29811" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o29812" from="9" name="quantity" src="d0_s14" to="13" />
      </biological_entity>
      <biological_entity id="o29812" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Mentzelia rhizomata is known only from Garfield County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sparsely vegetated, steep talus slopes with loose gravelly soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep talus" modifier="sparsely vegetated" />
        <character name="habitat" value="loose gravelly soils" modifier="slopes with" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>