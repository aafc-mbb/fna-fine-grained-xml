<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">130</other_info_on_meta>
    <other_info_on_meta type="illustration_page">131</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">PAXISTIMA</taxon_name>
    <taxon_name authority="A. Gray" date="1873" rank="species">canbyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 623. 1873</place_in_publication>
      <other_info_on_pub>(as Pachystima)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus paxistima;species canbyi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101486</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Canby’s mountain-lover</other_name>
  <other_name type="common_name">rat-stripper</other_name>
  <other_name type="common_name">cliff green</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 1–4 dm.</text>
      <biological_entity id="o19506" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems diffuse, creeping.</text>
      <biological_entity id="o19507" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole absent;</text>
      <biological_entity id="o19508" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19509" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually linear to narrowly elliptic, rarely oblanceolate, 5–20 × 2–4 mm, base obtuse, margins serrulate, apex obtuse.</text>
      <biological_entity id="o19510" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19511" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="linear to narrowly" value_original="linear to narrowly" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19512" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o19513" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o19514" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–5-flowered.</text>
      <biological_entity id="o19515" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals obtuse-deltate, 0.8–1 mm;</text>
      <biological_entity id="o19516" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19517" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse-deltate" value_original="obtuse-deltate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals obovate, 1.5 mm;</text>
      <biological_entity id="o19518" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19519" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments shorter than anthers.</text>
      <biological_entity id="o19520" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19521" name="all" name_original="filaments" src="d0_s7" type="structure">
        <character constraint="than anthers" constraintid="o19522" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19522" name="anther" name_original="anthers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules ellipsoid, 3–4 × 1.5–2 mm.</text>
      <biological_entity id="o19523" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds black, ellipsoid;</text>
      <biological_entity id="o19524" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>aril yellow or white.</text>
      <biological_entity id="o19525" name="aril" name_original="aril" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <discussion>There are only about 50 to 60 extant populations of Paxistima canbyi.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring; fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs, shaded banks, dry gravelly soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="shaded banks" />
        <character name="habitat" value="dry gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., Md., N.C., Ohio, Pa., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>