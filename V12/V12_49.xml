<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jess A. Peirson, Paul E. Berry, Victor W. Steinmann</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Graham) Baillon" date="1858" rank="section">Poinsettia</taxon_name>
    <place_of_publication>
      <publication_title>Étude Euphorb.,</publication_title>
      <place_in_publication>284. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section poinsettia</taxon_hierarchy>
    <other_info_on_name type="fna_id">318100</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Graham" date=" 1836" rank="genus">Poinsettia</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>20: 412. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus poinsettia</taxon_hierarchy>
  </taxon_identification>
  <number>24f.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial [rarely shrubs or small trees], with taproot or tuberous rootstock.</text>
      <biological_entity id="o19007" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19008" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <biological_entity id="o19009" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o19007" id="r1555" name="with" negation="false" src="d0_s0" to="o19008" />
      <relation from="o19007" id="r1556" name="with" negation="false" src="d0_s0" to="o19009" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, branched, terete, glabrous or hairy.</text>
      <biological_entity id="o19010" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite or alternate;</text>
      <biological_entity id="o19011" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules usually present, occasionally absent, at base of petiole;</text>
      <biological_entity id="o19012" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19013" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o19014" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <relation from="o19012" id="r1557" name="at" negation="false" src="d0_s3" to="o19013" />
      <relation from="o19013" id="r1558" name="part_of" negation="false" src="d0_s3" to="o19014" />
    </statement>
    <statement id="d0_s4">
      <text>petiole present, glabrous or hairy;</text>
      <biological_entity id="o19015" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade monomorphic (occasionally polymorphic in E. cyathophora and E. heterophylla), base symmetric, margins entire or toothed, flat to revolute, surfaces glabrous or variously hairy;</text>
      <biological_entity id="o19016" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity id="o19017" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o19018" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="toothed flat" name="shape" src="d0_s5" to="revolute" />
        <character char_type="range_value" from="toothed flat" name="shape" src="d0_s5" to="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate, midvein often prominent.</text>
      <biological_entity id="o19019" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o19020" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathial arrangement: terminal monochasia, dichasia, or condensed pleiochasia with 1–3 primary branches;</text>
      <biological_entity id="o19021" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o19022" name="monochasium" name_original="monochasia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o19023" name="dichasium" name_original="dichasia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o19024" name="pleiochasium" name_original="pleiochasia" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="condensed" value_original="condensed" />
      </biological_entity>
      <biological_entity constraint="primary" id="o19025" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o19022" id="r1559" name="with" negation="false" src="d0_s7" to="o19025" />
      <relation from="o19023" id="r1560" name="with" negation="false" src="d0_s7" to="o19025" />
      <relation from="o19024" id="r1561" name="with" negation="false" src="d0_s7" to="o19025" />
    </statement>
    <statement id="d0_s8">
      <text>individual pleiochasial branches unbranched or few-branched at 1 or more successive nodes;</text>
      <biological_entity id="o19026" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o19027" name="individual" name_original="individual" src="d0_s8" type="structure" />
      <biological_entity id="o19028" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pleiochasial" value_original="pleiochasial" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character constraint="at nodes" constraintid="o19029" is_modifier="false" name="architecture" src="d0_s8" value="few-branched" value_original="few-branched" />
      </biological_entity>
      <biological_entity id="o19029" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts subtending pleiochasia (pleiochasial bracts) opposite or whorled, usually wholly green or with paler green, white, pink, or red at base, sometimes wholly white, pink, or red, similar in shape and size to distal leaves or distinctly different, those on branches and subtending cyathia (dichasial and subcyathial bracts) opposite, distinct;</text>
      <biological_entity id="o19030" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o19031" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="whorled" value_original="whorled" />
        <character constraint="with paler green" is_modifier="false" modifier="usually wholly" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="with paler green" value_original="with paler green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character constraint="at base" constraintid="o19033" is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" modifier="sometimes wholly" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o19032" name="pleiochasium" name_original="pleiochasia" src="d0_s9" type="structure" />
      <biological_entity id="o19033" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o19034" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o19035" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o19036" name="cyathium" name_original="cyathia" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o19037" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o19038" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o19039" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o19040" name="cyathium" name_original="cyathia" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o19041" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o19031" id="r1562" name="subtending" negation="false" src="d0_s9" to="o19032" />
      <relation from="o19031" id="r1563" name="to" negation="false" src="d0_s9" to="o19034" />
      <relation from="o19031" id="r1564" name="to" negation="false" src="d0_s9" to="o19035" />
      <relation from="o19031" id="r1565" name="to" negation="false" src="d0_s9" to="o19036" />
      <relation from="o19035" id="r1566" name="to" negation="false" src="d0_s9" to="o19038" />
      <relation from="o19036" id="r1567" name="to" negation="false" src="d0_s9" to="o19038" />
      <relation from="o19035" id="r1568" name="to" negation="false" src="d0_s9" to="o19039" />
      <relation from="o19035" id="r1569" name="to" negation="false" src="d0_s9" to="o19040" />
      <relation from="o19036" id="r1570" name="to" negation="false" src="d0_s9" to="o19039" />
      <relation from="o19036" id="r1571" name="to" negation="false" src="d0_s9" to="o19040" />
    </statement>
    <statement id="d0_s10">
      <text>additional cymose branches occasionally present in axils of distal leaves, but alternately arranged and without whorled bracts.</text>
      <biological_entity id="o19042" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o19043" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="additional" value_original="additional" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="cymose" value_original="cymose" />
        <character constraint="in axils" constraintid="o19044" is_modifier="false" modifier="occasionally" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="alternately" name="arrangement" notes="" src="d0_s10" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o19044" name="axil" name_original="axils" src="d0_s10" type="structure" />
      <biological_entity constraint="distal" id="o19045" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity id="o19046" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="whorled" value_original="whorled" />
      </biological_entity>
      <relation from="o19044" id="r1572" name="part_of" negation="false" src="d0_s10" to="o19045" />
      <relation from="o19043" id="r1573" name="without" negation="false" src="d0_s10" to="o19046" />
    </statement>
    <statement id="d0_s11">
      <text>Involucre ± actinomorphic, not spurred;</text>
      <biological_entity id="o19047" name="involucre" name_original="involucre" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s11" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>glands 1–3 (sometimes 4–5 in E. eriantha, E. exstipulata, and E. radians), sessile or stipitate, shallowly cupped to deeply concave;</text>
      <biological_entity id="o19048" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="shallowly; deeply" name="shape" src="d0_s12" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>appendages absent or petaloid (E. bifurcata, E. eriantha, and E. exstipulata).</text>
      <biological_entity id="o19049" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Staminate flowers 3–25.</text>
      <biological_entity id="o19050" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: ovary glabrous or hairy;</text>
      <biological_entity id="o19051" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19052" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles distinct, occasionally appearing connate at base, unbranched or 2-fid.</text>
      <biological_entity id="o19053" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19054" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s16" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s16" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o19055" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o19054" id="r1574" modifier="occasionally" name="appearing" negation="false" src="d0_s16" to="o19055" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds: caruncle present or absent.</text>
      <biological_entity id="o19056" name="seed" name_original="seeds" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>x = 7.</text>
      <biological_entity id="o19057" name="caruncle" name_original="caruncle" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o19058" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 30 (10 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Indian Ocean Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Poinsettia belongs to Euphorbia subg. Chamaesyce (Gray) Reichenbach. An expanded sect. Poinsettia is recognized here to include three species that have previously often been included in sect. Alectoroctonum (Schlechtendal) Baillon (E. bifurcata, E. eriantha, and E. exstipulata). These three species differ from the so-called core Poinsettia by the presence of involucral gland appendages, but they possess the shallowly to deeply concave involucral glands and toothed leaves that are generally diagnostic for the broader section. Molecular phylogenetic analyses clearly unite these three species with the other members of sect. Poinsettia and not with sect. Alectoroctonum (Y. Yang et al. 2012).</discussion>
  <references>
    <reference>Dressler, R. L. 1961. A synopsis of Poinsettia (Euphorbiaceae). Ann. Missouri. Bot. Gard. 48: 329–341.</reference>
    <reference>Mayfield, M. H. 1997. A Systematic Treatment of Euphorbia Subg. Poinsettia (Euphorbiaceae). Ph.D. dissertation. University of Texas.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades linear to linear-elliptic, margins entire or with 2–4 inconspicuous teeth near apex; involucral glands densely canescent, appendages divided into subulate segments, incurved and covering glands, densely canescent; styles unbranched.</description>
      <determination>135. Euphorbia eriantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades linear, lanceolate, ovate, oblong, elliptic, or pandurate, margins conspicuously toothed (sometimes subentire in E. cyathophora, entire or few toothed in E. pinetorum); involucral glands glabrous, appendages absent or entire, undulate, slightly lobed, or divided into triangular segments, not incurved and covering glands, glabrous; styles 2-fid.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branches often arcuate; involucral gland appendages usually present, rarely absent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades usually ovate, rarely oblong or elliptic, margins finely serrulate; petioles 15–49 mm; involucral glands 1(–3); ovaries glabrous; caruncles absent or rudimentary.</description>
      <determination>130. Euphorbia bifurcata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades linear to narrowly elliptic or ovate, margins coarsely serrate; petioles 1–3 mm; involucral glands 4 (–5); ovaries puberulent on keels; caruncles 0.1 × 0.2 mm.</description>
      <determination>136. Euphorbia exstipulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branches ± straight (except occasionally proximal branches arcuate in E. davidii and E. dentata); involucral gland appendages absent.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves usually opposite, occasionally alternate distally, blade margins coarsely crenate-dentate or doubly crenate; seeds with caruncles; pleiochasial bracts wholly green or with paler green, white, or mauve near base; annual herbs.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ovaries densely pilose; capsules pilose (often sparsely); involucral gland taller than wide, stipitate.</description>
      <determination>131. Euphorbia cuphosperma</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ovaries and capsules glabrous or sparsely strigose; involucral glands shorter than wide, sessile.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Hairs of abaxial leaf blade surface stiff, strongly tapered; capsules 4–4.8 mm wide; seeds angular in cross section, unevenly tuberculate.</description>
      <determination>133. Euphorbia davidii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Hairs of abaxial leaf blade surface weak, filiform; capsules 3.5–4 mm wide; seeds rounded in cross section, evenly tuberculate.</description>
      <determination>134. Euphorbia dentata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves usually alternate, occasionally opposite proximally, blade margins entire, subentire, or glandular-serrulate; seeds usually without caruncles, occasionally caruncles rudimentary; pleiochasial bracts green (purpurescent in E. pinetorum), often paler green, white, pink, or red at base, occasionally wholly white, pink, or red; annual or perennial herbs.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems 5–20(–30) cm, from moniliform tuberous rootstocks; seeds 4–4.6 mm; perennial herbs.</description>
      <determination>139. Euphorbia radians</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems 20–100 cm, with taproots or woody rootstocks; seeds 2.1–3.1 mm; annual or perennial herbs.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Pleiochasial bracts wholly green or paler green at base; involucral glands stipitate, opening round (occasionally flattened from pressing), with annular rim.</description>
      <determination>137. Euphorbia heterophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Pleiochasial bracts green or purpurescent, often white, pink, or red at base, occasionally wholly white, pink, or red; involucral glands sessile or substipitate, opening oblong (flattened without pressing), without annular rim.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Annual herbs with spreading taproots; leaf blades 4–40 mm wide, linear, lanceolate, elliptic, or broadly pandurate; bracts usually green with white, pink, or red at base, occasionally distal bracts wholly white, pink, or red, rarely all bracts wholly green; capsules green; involucral gland 1; widespread, including s Florida.</description>
      <determination>132. Euphorbia cyathophora</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Perennial herbs with thickened, woody taproots; leaf blades 2.5–5 mm wide, lanceolate to linear; bracts wholly purpurescent green or pink at base; capsules purpurescent; involucral glands 3(–5); Miami-Dade and Monroe counties, s Florida.</description>
      <determination>138. Euphorbia pinetorum</determination>
    </key_statement>
  </key>
</bio:treatment>