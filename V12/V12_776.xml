<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">glandulosus</taxon_name>
    <taxon_name authority="Croizat" date="1945" rank="variety">pubentissimus</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>26: 188. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species glandulosus;variety pubentissimus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101977</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Croton</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">glandulosus</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="variety">hirsutus</taxon_name>
    <taxon_hierarchy>genus croton;species glandulosus;variety hirsutus</taxon_hierarchy>
  </taxon_identification>
  <number>12d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2 dm.</text>
      <biological_entity id="o30312" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems densely stellate-hairy, hairs spreading, radii unequal, central radius prominent.</text>
      <biological_entity id="o30313" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o30314" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s1" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="position" src="d0_s1" value="central" value_original="central" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole apical glands sessile, wavy-wrinkled when dry, 05–0.8 mm diam.;</text>
      <biological_entity id="o30315" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o30316" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity constraint="apical" id="o30317" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="when dry" name="relief" src="d0_s2" value="wavy-wrinkled" value_original="wavy-wrinkled" />
        <character char_type="range_value" from="05" from_unit="mm" name="diameter" src="d0_s2" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 2–7 × 0.7–3 cm, length mostly more than 2 times width, membranous, marginal teeth pointed, both surfaces densely stellate-villous;</text>
      <biological_entity id="o30318" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o30319" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s3" value="2+" value_original="2+" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o30320" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity id="o30321" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="stellate-villous" value_original="stellate-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base obscurely 3-veined.</text>
      <biological_entity id="o30322" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30323" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety pubentissimus grows primarily in the Texas Gulf Coast counties of Aransas, Brazoria, Cameron, Kenedy, Kleberg, Nueces, and San Patricio, but a few collections are known from inland Colorado and Victoria counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Beaches, sand dunes, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beaches" />
        <character name="habitat" value="sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>