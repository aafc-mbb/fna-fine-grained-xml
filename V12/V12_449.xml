<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">CORNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CORNUS</taxon_name>
    <taxon_name authority="(Rafinesque) Rafinesque" date="1838" rank="subgenus">Cynoxylon</taxon_name>
    <place_of_publication>
      <publication_title>Alsogr. Amer.,</publication_title>
      <place_in_publication>59. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cornaceae;genus cornus;subgenus cynoxylon</taxon_hierarchy>
    <other_info_on_name type="fna_id">318095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cornus</taxon_name>
    <taxon_name authority="Rafinesque" date="1828" rank="section">Cynoxylon</taxon_name>
    <place_of_publication>
      <publication_title>Med. Fl.</publication_title>
      <place_in_publication>1: 132. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cornus;section cynoxylon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Spach" date="unknown" rank="genus">Benthamidia</taxon_name>
    <taxon_hierarchy>genus benthamidia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Rafinesque) Small" date="unknown" rank="genus">Cynoxylon</taxon_name>
    <taxon_hierarchy>genus cynoxylon</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees;</text>
      <biological_entity id="o20427" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o20429" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Branches and leaves opposite.</text>
      <biological_entity id="o20430" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o20431" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences capitula;</text>
      <biological_entity constraint="inflorescences" id="o20432" name="capitulum" name_original="capitula" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracts 4–6, well developed, petaloid, subtending inflorescence and surrounding or enclosing it over winter.</text>
      <biological_entity id="o20433" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s4" value="developed" value_original="developed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="position_relational" src="d0_s4" value="surrounding" value_original="surrounding" />
        <character constraint="over winter" is_modifier="false" name="position_relational" src="d0_s4" value="enclosing" value_original="enclosing" />
      </biological_entity>
      <biological_entity id="o20434" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <relation from="o20433" id="r1670" name="subtending" negation="false" src="d0_s4" to="o20434" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels absent.</text>
      <biological_entity id="o20435" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Drupes distinct;</text>
      <biological_entity id="o20436" name="drupe" name_original="drupes" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stone apex dimpled to rounded.</text>
      <biological_entity constraint="stone" id="o20437" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="dimpled" value_original="dimpled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Drupes angular in cross section, closely appressed to each other; petaloid bracts 4–6 per inflorescence; w North America.</description>
      <determination>2. Cornus nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Drupes round in cross section, spreading from each other; petaloid bracts 4 per inflorescence; e North America.</description>
      <determination>3. Cornus florida</determination>
    </key_statement>
  </key>
</bio:treatment>