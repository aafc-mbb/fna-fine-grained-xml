<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Geoffrey A. Levin</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">160</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ACALYPHA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1003. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 436. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus acalypha</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek akalephe, stinging nettle, from a-, without, kalos, good, and haphe, touch, alluding to some species resembling Urtica (though not stinging)</other_info_on_name>
    <other_info_on_name type="fna_id">100064</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Threeseeded mercury</other_name>
  <other_name type="common_name">copperleaf</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herb or shrubs [trees], annual or perennial, unarmed, monoecious or dioecious;</text>
      <biological_entity id="o5178" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s0" value="herb" value_original="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs unbranched [stellate], sometimes glandular, or absent;</text>
      <biological_entity id="o5179" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex absent.</text>
      <biological_entity id="o5180" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent or drought-deciduous, alternate, simple;</text>
      <biological_entity id="o5181" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="drought-deciduous" value_original="drought-deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, persistent or deciduous;</text>
      <biological_entity id="o5182" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present, glands absent or present at apex, adaxial, inconspicuous [conspicuous];</text>
      <biological_entity id="o5183" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o5184" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character constraint="at apex" constraintid="o5185" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o5185" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o5186" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed, margins deeply serrate or crenate to subentire, laminar glands absent;</text>
      <biological_entity id="o5187" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o5188" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s6" to="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation palmate at base, pinnate distally [pinnate].</text>
      <biological_entity constraint="laminar" id="o5189" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character constraint="at base" constraintid="o5190" is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity id="o5190" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences unisexual or bisexual (pistillate flowers proximal, staminate distal [staminate proximal, pistillate distal]), axillary or terminal, spikelike [paniclelike] thyrses;</text>
      <biological_entity id="o5191" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5192" name="thyrse" name_original="thyrses" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>allomorphic pistillate flowers sometimes present;</text>
      <biological_entity id="o5193" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts subtending pistillate flowers enlarging in fruit [remaining minute];</text>
      <biological_entity id="o5194" name="bract" name_original="bracts" src="d0_s10" type="structure" />
      <biological_entity constraint="subtending" id="o5195" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character constraint="in fruit" constraintid="o5196" is_modifier="false" name="size" src="d0_s10" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o5196" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>glands subtending bracts 0.</text>
      <biological_entity id="o5197" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o5198" name="bract" name_original="bracts" src="d0_s11" type="structure" />
      <relation from="o5197" id="r462" name="subtending" negation="false" src="d0_s11" to="o5198" />
    </statement>
    <statement id="d0_s12">
      <text>Pedicels: staminate present, pistillate absent [present], allomorphic present or absent.</text>
      <biological_entity id="o5199" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers: sepals 4, not petaloid, 1–2 [–3] mm, valvate, distinct [connate];</text>
      <biological_entity id="o5200" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5201" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s13" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 0;</text>
      <biological_entity id="o5202" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5203" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectary absent;</text>
      <biological_entity id="o5204" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5205" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 4–8, distinct;</text>
      <biological_entity id="o5206" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5207" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s16" to="8" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers elongated and twisted at maturity;</text>
      <biological_entity id="o5208" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5209" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="length" src="d0_s17" value="elongated" value_original="elongated" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pistillode absent.</text>
      <biological_entity id="o5210" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5211" name="pistillode" name_original="pistillode" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Pistillate flowers: sepals 3 [or 5], distinct [connate];</text>
      <biological_entity id="o5212" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5213" name="sepal" name_original="sepals" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>petals 0;</text>
      <biological_entity id="o5214" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5215" name="petal" name_original="petals" src="d0_s20" type="structure">
        <character name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>nectary absent;</text>
      <biological_entity id="o5216" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5217" name="nectary" name_original="nectary" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>pistil (1–) 3-carpellate;</text>
      <biological_entity id="o5218" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5219" name="pistil" name_original="pistil" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="(1-)3-carpellate" value_original="(1-)3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>styles (1–) 3, distinct or connate basally, usually multifid or laciniate, rarely 2-fid or unbranched, branches threadlike.</text>
      <biological_entity id="o5220" name="flower" name_original="flowers" src="d0_s23" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s23" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5221" name="style" name_original="styles" src="d0_s23" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s23" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s23" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s23" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s23" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s23" value="multifid" value_original="multifid" />
        <character is_modifier="false" name="shape" src="d0_s23" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s23" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o5222" name="branch" name_original="branches" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="thread-like" value_original="threadlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Fruits capsules, allomorphic fruits achenes or schizocarps.</text>
      <biological_entity constraint="fruits" id="o5223" name="capsule" name_original="capsules" src="d0_s24" type="structure" />
      <biological_entity id="o5224" name="fruit" name_original="fruits" src="d0_s24" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s24" value="allomorphic" value_original="allomorphic" />
      </biological_entity>
      <biological_entity id="o5225" name="achene" name_original="achenes" src="d0_s24" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s24" value="allomorphic" value_original="allomorphic" />
      </biological_entity>
      <biological_entity id="o5226" name="schizocarp" name_original="schizocarps" src="d0_s24" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s24" value="allomorphic" value_original="allomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Seeds ellipsoid to subglobose;</text>
      <biological_entity id="o5227" name="seed" name_original="seeds" src="d0_s25" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s25" to="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>caruncle present, sometimes rudimentary.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 10.</text>
      <biological_entity id="o5228" name="caruncle" name_original="caruncle" src="d0_s26" type="structure">
        <character is_modifier="false" name="presence" src="d0_s26" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s26" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity constraint="x" id="o5229" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 450 (18 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Asia, Africa, Indian Ocean Islands, Pacific Islands, Australia; introduced in Europe; primarily tropical and subtropical regions, reaching temperate regions in eastern North America and eastern Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
        <character name="distribution" value="primarily tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="reaching temperate regions in eastern North America and eastern Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Some species of Acalypha are cultivated as ornamentals, notably A. herzogiana Pax &amp; K. Hoffmann, A. hispida Burman f., and A. wilkesiana; the last has become naturalized in Florida. Acalypha herzogiana has escaped locally on Dauphin Island, Alabama (H. Horne, pers. comm.). It may escape locally elsewhere but might not become naturalized in the flora area, because the cultivated form is sterile, although pieces of the plant root readily if they are spread. It is an herbaceous perennial readily recognized by its erect, feathery, red spikes of sterile pistillate flowers (V. W. Steinmann and G. A. Levin 2011).</discussion>
  <discussion>Acalypha mexicana Müller Arg. [A. indica Linnaeus var. mexicana (Müller Arg.) Pax &amp; K. Hoffmann], native from central Mexico to Guatemala, was collected twice early in the twentieth century in southeastern Arizona but has not been collected there since and presumably did not become established. It will key here to A. australis; A. mexicana differs in having pistillate bracts that are 10 mm and eglandular (versus 10–15 mm and glandular) and allomorphic flowers that are common, long-pedicelled, and 1-carpellate (versus rare, sessile, and 2-carpellate).</discussion>
  <discussion>Some Acalypha species, including about half of those in the flora area, produce allomorphic pistillate flowers (A. Radcliffe-Smith 1973). These flowers may be mixed with normal pistillate or staminate flowers or be terminal on the inflorescence. Their pistils generally have fewer carpels than normal pistillate flowers of the same species, bear sub-basal rather than terminal styles, and develop into nutlets or schizocarps, frequently bearing bristles or variously ornamented outgrowths that presumably facilitate dispersal. Unlike normal pistillate flowers, allomorphic flowers frequently lack bracts and are borne on elongate pedicels. Characters of these flowers generally are species-specific and useful for identification (Radcliffe-Smith).</discussion>
  <discussion>The bracts subtending normal pistillate flowers of most Acalypha species, including all in the flora area, enlarge as the fruits develop. Measurements for these bracts, referred to here as pistillate bracts, and the pistillate portion of the inflorescences given in the key and descriptions are post-anthesis, after the bracts have completed most or all of their growth.</discussion>
  <discussion>Seed descriptions pertain to those from normal pistillate flowers.</discussion>
  <discussion>The sequence of species below starts with shrubs (species 1 and 2), followed by subshrubs and perennial herbs (species 3 to 6), and concludes with annual herbs (species 7 to 18). Within each growth form, similar species are grouped together.</discussion>
  <references>
    <reference>Levin, G. A. 1995. Systematics of the Acalypha californica complex (Euphorbiaceae). Madroño 41: 254–265.</reference>
    <reference>Levin, G. A. 1999. Evolution in the Acalypha gracilens/monococca complex (Euphorbiaceae): Morphological analysis. Syst. Bot. 23: 269–287.</reference>
    <reference>Levin, G. A. 1999b. Notes on Acalypha (Euphorbiaceae) in North America. Rhodora 101: 217–233.</reference>
    <reference>Miller, L. W. 1964. A Taxonomic Study of the Species of Acalypha in the United States. Ph.D. dissertation. Purdue University.</reference>
    <reference>Radcliffe-Smith, A. 1973. Allomorphic female flowers in the genus Acalypha (Euphorbiaceae). Kew Bull. 28: 525–529.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 1–5 cm; stems stipitate-glandular; plants 5–10 dm; Arizona, California.</description>
      <determination>1. Acalypha californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 9–20 cm; stems not glandular; plants 20–50 dm; Florida.</description>
      <determination>2. Acalypha wilkesiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs or subshrubs.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistillate bract lobes linear or proximally deltate with linear tips.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pistillate bracts densely crowded (inflorescence axis not or sparingly visible between bracts), abaxial surfaces long-hirsute (nonglandular hairs to 2 mm), lobes proximally deltate with linear tips, smooth.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems, petioles, and peduncles stipitate-glandular; pistillate inflorescences terminal; styles unbranched or rarely 2-fid.</description>
      <determination>7. Acalypha alopecuroidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems, petioles, and peduncles not stipitate-glandular; pistillate (and bisexual) inflorescences axillary; styles multifid or laciniate.</description>
      <determination>8. Acalypha arvensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pistillate bracts loosely arranged (inflorescence axis visible between bracts), abaxial surfaces glabrous or pubescent (nonglandular hairs to 0.3 mm, glandular hairs may be longer), lobes linear, muricate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf bases cordate; pistillate bracts pubescent and stipitate-glandular; capsules spiny; seeds tuberculate.</description>
      <determination>9. Acalypha ostryifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf bases obtuse to rounded or truncate; pistillate bracts glabrous; capsules smooth; seeds minutely pitted.</description>
      <determination>10. Acalypha setosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistillate bract lobes deltate (without linear tips), triangular, attenuate, narrowly oblong, lanceolate, spatulate, or rounded.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Inflorescences (all or some) terminal.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems erect; leaf blades 2–6 cm.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Inflorescences usually bisexual (rarely staminate portion replaced by allomorphic pistillate flowers), all terminal; petioles 0.2–1 cm; allomorphic pistillate flowers rare, pedicels 3–5 mm, without bracts.</description>
      <determination>3. Acalypha phleoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Inflorescences unisexual, pistillate terminal (sometimes on short lateral branches, appearing axillary), staminate axillary; petioles 1–4 cm; allomorphic pistillate flowers common, pedicels rudimentary, bracts like those of normal pistillate flowers.</description>
      <determination>11. Acalypha neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems prostrate to ascending; leaf blades 0.3–2.5 cm.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petioles 0.1–0.5 cm, less than 1/3 leaf blade length; inflorescences all bisexual and terminal; Florida.</description>
      <determination>4. Acalypha chamaedrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petioles 0.4–2.5 cm, more than 2/3 leaf blade length; inflorescences unisexual or bisexual, terminal and axillary; Texas.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade margins shallowly crenate; pistillate bract lobes 1/4 bract length.</description>
      <determination>5. Acalypha monostachya</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade margins deeply crenate; pistillate bract lobes 1/2 bract length.</description>
      <determination>6. Acalypha radians</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Inflorescences all axillary.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistillate bracts 4–5 mm; styles unbranched.</description>
      <determination>12. Acalypha poiretii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistillate bracts 6–15(–20) mm; styles multifid or laciniate.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pistillate bract lobes rounded, 1/20 bract length.</description>
      <determination>13. Acalypha australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pistillate bract lobes deltate, triangular, lanceolate or narrowly oblong, 1/10–3/4 bract length.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades linear, linear-lanceolate, or oblong-lanceolate; pistillate bracts sessile-glandular, lobes deltate, 1/10–1/4 bract length.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pistils 3-carpellate.</description>
      <determination>17. Acalypha gracilens</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pistils 1-carpellate.</description>
      <determination>18. Acalypha monococca</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades broadly lanceolate to ovate or rhombic; pistillate bracts not sessile-glandular (sometimes stipitate-glandular), lobes triangular to lanceolate or narrowly oblong, 1/4–3/4 bract length.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pistillate bract abaxial surfaces hirsute (sometimes also stipitate-glandular), lobes (9–)10–14(–16), 1/4–1/2 bract length; stems usually hirsute.</description>
      <determination>16. Acalypha virginica</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pistillate bract abaxial surfaces sparsely pubescent (usually also stipitate-glandular), lobes (5–)7–9(–11), 1/3–3/4 bract length; stems usually pubescent or glabrate, rarely hirsute.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Pistils (normal flowers) 2-carpellate; seeds 2.4–3.2 mm.</description>
      <determination>14. Acalypha deamii</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Pistils (normal flowers) 3-carpellate; seeds (1.2–)1.5–1.7(–2) mm.</description>
      <determination>15. Acalypha rhomboidea</determination>
    </key_statement>
  </key>
</bio:treatment>