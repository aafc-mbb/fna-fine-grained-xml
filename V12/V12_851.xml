<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">520</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="J. Darlington" date="1934" rank="species">longiloba</taxon_name>
    <taxon_name authority="J. J. Schenk &amp; L. Hufford" date="2010" rank="variety">yavapaiensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>57: 258, figs. 4C, 5B. 2010</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species longiloba;variety yavapaiensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101848</other_info_on_name>
  </taxon_identification>
  <number>43c.</number>
  <other_name type="common_name">Yavapai blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blades 39–72.4 × 4.3–19.1 mm, widest intersinus distance 2.3–7.1 mm;</text>
      <biological_entity id="o4019" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o4020" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="39" from_unit="mm" name="length" src="d0_s0" to="72.4" to_unit="mm" />
        <character char_type="range_value" from="4.3" from_unit="mm" name="width" src="d0_s0" to="19.1" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s0" value="widest" value_original="widest" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s0" to="7.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal with margins pinnate, lobes 14–24, slightly antrorse, 1–6.5 × 0.8–3.2 mm;</text>
      <biological_entity id="o4021" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o4022" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4023" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o4024" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s1" to="24" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s1" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s1" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s1" to="3.2" to_unit="mm" />
      </biological_entity>
      <relation from="o4022" id="r373" name="with" negation="false" src="d0_s1" to="o4023" />
    </statement>
    <statement id="d0_s2">
      <text>distal with margins pinnate, lobes 8–20, slightly antrorse, 0.8–5.8 × 0.7–2.8 mm.</text>
      <biological_entity id="o4025" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o4026" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4027" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o4028" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="20" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s2" to="5.8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s2" to="2.8" to_unit="mm" />
      </biological_entity>
      <relation from="o4026" id="r374" name="with" negation="false" src="d0_s2" to="o4027" />
    </statement>
    <statement id="d0_s3">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o4029" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o4030" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals 11.6–18.4 × 2.9–6.8 mm;</text>
      <biological_entity id="o4031" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4032" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="11.6" from_unit="mm" name="length" src="d0_s4" to="18.4" to_unit="mm" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="width" src="d0_s4" to="6.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>5 outermost stamens with anthers.</text>
      <biological_entity id="o4033" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o4034" name="stamen" name_original="stamens" src="d0_s5" type="structure" />
      <biological_entity id="o4035" name="anther" name_original="anthers" src="d0_s5" type="structure" />
      <relation from="o4034" id="r375" name="with" negation="false" src="d0_s5" to="o4035" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds 3–3.4 mm;</text>
      <biological_entity id="o4036" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>seed-coat anticlinal cell-walls sinuous, papillae 10–21 per cell.</text>
      <biological_entity id="o4037" name="seed-coat" name_original="seed-coat" src="d0_s7" type="structure" />
      <biological_entity id="o4038" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s7" value="sinuous" value_original="sinuous" />
      </biological_entity>
      <biological_entity id="o4040" name="cell" name_original="cell" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 18.</text>
      <biological_entity id="o4039" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o4040" from="10" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4041" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety yavapaiensis is found in all counties in Arizona.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy washes, roadsides in grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy washes" />
        <character name="habitat" value="grasslands" modifier="roadsides in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>