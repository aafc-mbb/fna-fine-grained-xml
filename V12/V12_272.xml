<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Molina" date="1782" rank="genus">MAYTENUS</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="species">phyllanthoides</taxon_name>
    <taxon_name authority="Loesener" date="1910" rank="variety">ovalifolia</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>8: 291. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus maytenus;species phyllanthoides;variety ovalifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101482</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maytenus</taxon_name>
    <taxon_name authority="Lundell" date="unknown" rank="species">texana</taxon_name>
    <taxon_hierarchy>genus maytenus;species texana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tricerma</taxon_name>
    <taxon_name authority="(Lundell) Lundell" date="unknown" rank="species">texanum</taxon_name>
    <taxon_hierarchy>genus tricerma;species texanum</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 0.2–2 m.</text>
      <biological_entity id="o11035" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to prostrate.</text>
      <biological_entity id="o11036" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="growth_form_or_orientation" src="d0_s1" to="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–3 mm;</text>
      <biological_entity id="o11037" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11038" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblongelliptic to elliptic-obovate, 1.5–4 (–6) × 1–2 (–3.5) cm, base rounded, margins entire or remotely serrulate, apex obtuse to rounded, sometimes mucronate.</text>
      <biological_entity id="o11039" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11040" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s3" to="elliptic-obovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11041" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11042" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o11043" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, var. ovalifolia occurs along the Gulf coast of Texas from Cameron to Nueces counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–summer; fruiting summer–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="early spring" />
        <character name="fruiting time" char_type="range_value" to="winter" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal prairies, marshes, clay or sand-clay mounds, often saline sites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal prairies" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="sand-clay mounds" />
        <character name="habitat" value="sites" modifier="often saline" />
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="1" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>