<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Michael J. Huft</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HURA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1008. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 439. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus hura</taxon_hierarchy>
    <other_info_on_name type="etymology">Native American word for poisonous sap, alluding to caustic latex</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">115891</other_info_on_name>
  </taxon_identification>
  <number>20.</number>
  <other_name type="common_name">Sandbox tree</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, monoecious;</text>
      <biological_entity id="o14227" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk with broad-based, conic thorns;</text>
      <biological_entity id="o14228" name="trunk" name_original="trunk" src="d0_s1" type="structure" />
      <biological_entity id="o14229" name="thorn" name_original="thorns" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="broad-based" value_original="broad-based" />
        <character is_modifier="true" name="shape" src="d0_s1" value="conic" value_original="conic" />
      </biological_entity>
      <relation from="o14228" id="r1191" name="with" negation="false" src="d0_s1" to="o14229" />
    </statement>
    <statement id="d0_s2">
      <text>hairs unbranched;</text>
      <biological_entity id="o14230" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>latex white or colorless.</text>
      <biological_entity id="o14231" name="latex" name_original="latex" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="colorless" value_original="colorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, alternate, simple;</text>
      <biological_entity id="o14232" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules present, caducous;</text>
      <biological_entity id="o14233" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s5" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole present, glands present at apex, lateral, conspicuous;</text>
      <biological_entity id="o14234" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14235" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character constraint="at apex" constraintid="o14236" is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" notes="" src="d0_s6" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o14236" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade unlobed, margins serrate or crenate-serrulate, laminar glands absent;</text>
      <biological_entity id="o14237" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o14238" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="crenate-serrulate" value_original="crenate-serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation pinnate.</text>
      <biological_entity constraint="laminar" id="o14239" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences usually unisexual, rarely bisexual (pistillate flowers proximal, staminate distal);</text>
    </statement>
    <statement id="d0_s10">
      <text>staminate terminal, spikelike thyrses, cymules densely crowded in conelike structure;</text>
      <biological_entity id="o14240" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14241" name="thyrse" name_original="thyrses" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="true" name="shape" src="d0_s10" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o14243" name="structure" name_original="structure" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="conelike" value_original="conelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate axillary, solitary flowers;</text>
      <biological_entity id="o14242" name="cymule" name_original="cymules" src="d0_s10" type="structure">
        <character constraint="in structure" constraintid="o14243" is_modifier="false" modifier="densely" name="arrangement" src="d0_s10" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bisexual as in staminate with solitary pistillate flower at base;</text>
      <biological_entity id="o14244" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character constraint="as" is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o14245" name="flower" name_original="flower" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14246" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o14245" id="r1192" name="at" negation="false" src="d0_s12" to="o14246" />
    </statement>
    <statement id="d0_s13">
      <text>glands subtending each bract 0.</text>
      <biological_entity id="o14247" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o14248" name="bract" name_original="bract" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels present.</text>
      <biological_entity id="o14249" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers: sepals 5, imbricate, connate most of length;</text>
      <biological_entity id="o14250" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14251" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="length" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 0;</text>
      <biological_entity id="o14252" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14253" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>nectary absent;</text>
      <biological_entity id="o14254" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14255" name="nectary" name_original="nectary" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stamens 10–80, connate entire length forming thick column;</text>
      <biological_entity id="o14256" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14257" name="stamen" name_original="stamens" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s18" to="80" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="connate" value_original="connate" />
        <character is_modifier="false" name="length" src="d0_s18" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14258" name="column" name_original="column" src="d0_s18" type="structure">
        <character is_modifier="true" name="width" src="d0_s18" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o14257" id="r1193" name="forming" negation="false" src="d0_s18" to="o14258" />
    </statement>
    <statement id="d0_s19">
      <text>pistillode absent.</text>
      <biological_entity id="o14259" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14260" name="pistillode" name_original="pistillode" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Pistillate flowers: sepals 5, connate entire length;</text>
      <biological_entity id="o14261" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14262" name="sepal" name_original="sepals" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character is_modifier="false" name="length" src="d0_s20" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>petals 0;</text>
      <biological_entity id="o14263" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14264" name="petal" name_original="petals" src="d0_s21" type="structure">
        <character name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>nectary absent;</text>
      <biological_entity id="o14265" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14266" name="nectary" name_original="nectary" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>pistil 5–20-carpellate;</text>
      <biological_entity id="o14267" name="flower" name_original="flowers" src="d0_s23" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s23" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14268" name="pistil" name_original="pistil" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s23" value="5-20-carpellate" value_original="5-20-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>style 1, unbranched, terminating in lobed stigmatic disc.</text>
      <biological_entity id="o14269" name="flower" name_original="flowers" src="d0_s24" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s24" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14270" name="style" name_original="style" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o14271" name="stigmatic" name_original="stigmatic" src="d0_s24" type="structure">
        <character is_modifier="true" name="shape" src="d0_s24" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o14272" name="disc" name_original="disc" src="d0_s24" type="structure">
        <character is_modifier="true" name="shape" src="d0_s24" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o14270" id="r1194" name="terminating in" negation="false" src="d0_s24" to="o14271" />
      <relation from="o14270" id="r1195" name="terminating in" negation="false" src="d0_s24" to="o14272" />
    </statement>
    <statement id="d0_s25">
      <text>Fruits capsules, woody.</text>
      <biological_entity constraint="fruits" id="o14273" name="capsule" name_original="capsules" src="d0_s25" type="structure">
        <character is_modifier="false" name="texture" src="d0_s25" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>Seeds lenticular;</text>
      <biological_entity id="o14274" name="seed" name_original="seeds" src="d0_s26" type="structure">
        <character is_modifier="false" name="shape" src="d0_s26" value="lenticular" value_original="lenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>caruncle absent.</text>
    </statement>
    <statement id="d0_s28">
      <text>x = 11.</text>
      <biological_entity id="o14275" name="caruncle" name_original="caruncle" src="d0_s27" type="structure">
        <character is_modifier="false" name="presence" src="d0_s27" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o14276" name="chromosome" name_original="" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, Fla.; Mexico, West Indies, Central America, South America; introduced also in tropical areas worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in tropical areas worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hura polyandra Baillon is distributed from Mexico to Nicaragua.</discussion>
  
</bio:treatment>