<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
    <other_info_on_meta type="illustration_page">66</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Grisebach" date="1866" rank="genus">REYNOSIA</taxon_name>
    <taxon_name authority="Urban" date="1899" rank="species">septentrionalis</taxon_name>
    <place_of_publication>
      <publication_title>Symb. Antill.</publication_title>
      <place_in_publication>1: 356. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus reynosia;species septentrionalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101377</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Darling-plum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, 1.5–7 m, glabrous;</text>
      <biological_entity id="o17051" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks to 2 dm diam.</text>
      <biological_entity id="o17053" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="diameter" src="d0_s1" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1.5–3 mm;</text>
      <biological_entity id="o17054" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o17055" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic-oblong to oval or obovate, 2–4 cm, coriaceous, base cuneate to truncate, margins entire, revolute, apex usually truncate-emarginate, sometimes rounded.</text>
      <biological_entity id="o17056" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17057" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic-oblong" name="shape" src="d0_s3" to="oval or obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o17058" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o17059" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o17060" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="truncate-emarginate" value_original="truncate-emarginate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–4-flowered.</text>
      <biological_entity id="o17061" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 4–9 mm.</text>
      <biological_entity id="o17062" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Drupes dark purple to black, globose to ovoid or ellipsoid.</text>
      <biological_entity id="o17063" name="drupe" name_original="drupes" src="d0_s6" type="structure">
        <character char_type="range_value" from="dark purple" name="coloration" src="d0_s6" to="black" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="ovoid or ellipsoid" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reynosia septentrionalis is found in the flora area in Miami-Dade and Monroe counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering sporadically year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="sporadically" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal hammocks, open woods, thickets, mangrove margins, dunes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal hammocks" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="margins" modifier="mangrove" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>