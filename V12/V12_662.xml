<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="N. D. Atwood &amp; S. L. Welsh" date="2005" rank="species">todiltoensis</taxon_name>
    <place_of_publication>
      <publication_title>W. N. Amer. Naturalist</publication_title>
      <place_in_publication>65: 365, fig. 1. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species todiltoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101815</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Jemez Mountains blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants biennial or perennial, bushlike, with ground-level caudices.</text>
      <biological_entity id="o10813" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushlike" value_original="bushlike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10814" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s0" value="ground-level" value_original="ground-level" />
      </biological_entity>
      <relation from="o10813" id="r902" name="with" negation="false" src="d0_s0" to="o10814" />
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary or multiple, erect, straight;</text>
      <biological_entity id="o10815" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distal, distal longest, antrorse, straight;</text>
      <biological_entity id="o10816" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="distal" id="o10817" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 41–121 × 1.5–40 mm, widest intersinus distance 1.1–3.3 mm;</text>
      <biological_entity id="o10818" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10819" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="41" from_unit="mm" name="length" src="d0_s4" to="121" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s4" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate, elliptic, or linear, margins entire or serrate to pinnatisect, teeth or lobes 0–26, slightly antrorse, (0.9–) 2.4–18.6 mm;</text>
      <biological_entity id="o10820" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o10821" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o10822" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="to lobes" constraintid="o10824" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="slightly" name="orientation" notes="" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="2.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s5" to="18.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10823" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity id="o10824" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal oblanceolate, elliptic, lanceolate, or linear, base not clasping, margins entire, serrate, or pinnatisect, teeth or lobes 0–22, slightly antrorse, 0.8–18 mm;</text>
      <biological_entity id="o10825" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o10826" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o10827" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o10828" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity id="o10829" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10830" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="22" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with needlelike and occasionally simple grappling-hook and/or complex grappling-hook trichomes, adaxial surface with needlelike trichomes.</text>
      <biological_entity id="o10831" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o10832" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o10833" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o10834" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity constraint="grappling-hook" id="o10835" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o10836" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o10837" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o10832" id="r903" name="with" negation="false" src="d0_s7" to="o10833" />
      <relation from="o10832" id="r904" name="with" negation="false" src="d0_s7" to="o10834" />
      <relation from="o10836" id="r905" name="with" negation="false" src="d0_s7" to="o10837" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire or pinnate.</text>
      <biological_entity id="o10838" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o10839" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals light to golden yellow, (10.4–) 11.7–24.6 × 1.8–5.1 mm, apex acute to rounded, glabrous abaxially;</text>
      <biological_entity id="o10840" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10841" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s9" to="golden yellow" />
        <character char_type="range_value" from="10.4" from_unit="mm" name="atypical_length" src="d0_s9" to="11.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11.7" from_unit="mm" name="length" src="d0_s9" to="24.6" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s9" to="5.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10842" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens light to golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 10–21 × 1.4–4 mm, without anthers, second whorl with anthers;</text>
      <biological_entity id="o10843" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10844" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s10" to="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o10845" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o10846" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="21" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10847" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o10848" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o10849" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o10846" id="r906" name="without" negation="false" src="d0_s10" to="o10847" />
      <relation from="o10848" id="r907" name="with" negation="false" src="d0_s10" to="o10849" />
    </statement>
    <statement id="d0_s11">
      <text>anthers twisted or occasionally straight after dehiscence, epidermis papillate;</text>
      <biological_entity id="o10850" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10851" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="occasionally" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o10852" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.5–12.7 mm.</text>
      <biological_entity id="o10853" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10854" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s12" to="12.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped to cylindric, 6.7–20.2 × 4.5–8.5 mm, base tapering or rounded, not longitudinally ridged.</text>
      <biological_entity id="o10855" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="cupshaped" name="shape" src="d0_s13" to="cylindric" />
        <character char_type="range_value" from="6.7" from_unit="mm" name="length" src="d0_s13" to="20.2" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s13" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10856" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls sinuous, papillae 6–12 per cell.</text>
      <biological_entity id="o10857" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o10858" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o10859" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="sinuous" value_original="sinuous" />
      </biological_entity>
      <biological_entity id="o10861" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 20.</text>
      <biological_entity id="o10860" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o10861" from="6" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10862" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia todiltoensis occurs in northcentral New Mexico.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hillside slopes, hilltops, hard gypsum-rich clayey soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hillside slopes" />
        <character name="habitat" value="hilltops" />
        <character name="habitat" value="hard gypsum-rich clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>