<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="illustration_page">322</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Graham) Baillon" date="1858" rank="section">Poinsettia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">heterophylla</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 453. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section poinsettia;species heterophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200012563</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="Ortega" date="unknown" rank="species">geniculata</taxon_name>
    <taxon_hierarchy>genus euphorbia;species geniculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poinsettia</taxon_name>
    <taxon_name authority="(Ortega) Klotzsch &amp; Garcke" date="unknown" rank="species">geniculata</taxon_name>
    <taxon_hierarchy>genus poinsettia;species geniculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="(Linnaeus) Klotzsch &amp; Garcke" date="unknown" rank="species">heterophylla</taxon_name>
    <taxon_hierarchy>genus p.;species heterophylla</taxon_hierarchy>
  </taxon_identification>
  <number>137.</number>
  <other_name type="common_name">Mexican fireplant</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o33542" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o33543" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o33542" id="r2757" name="with" negation="false" src="d0_s0" to="o33543" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect-ascending, 20–100 cm, sparsely pilose to villous;</text>
      <biological_entity id="o33544" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-ascending" value_original="erect-ascending" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="sparsely pilose" name="pubescence" src="d0_s1" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches ± straight.</text>
      <biological_entity id="o33545" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually alternate, occasionally opposite proximally;</text>
      <biological_entity id="o33546" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="occasionally; proximally" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 10–50 mm, pilose;</text>
      <biological_entity id="o33547" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly lanceolate to elliptic or broadly obovate (then usually pandurate and 4-lobed), often polymorphic on single plants, 30–200 × 20–140 mm, base acute, margins sparsely glandular-serrulate, hirtellous, flat, apex acute to obtuse, abaxial surface sparsely appressed-pilose, adaxial surface sparsely pilosulous to glabrate;</text>
      <biological_entity id="o33548" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="elliptic or broadly obovate" />
        <character constraint="on plants" constraintid="o33549" is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="polymorphic" value_original="polymorphic" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" notes="" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" notes="" src="d0_s5" to="140" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33549" name="plant" name_original="plants" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o33550" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o33551" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="glandular-serrulate" value_original="glandular-serrulate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o33552" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33553" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="appressed-pilose" value_original="appressed-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate, midvein prominent.</text>
      <biological_entity constraint="adaxial" id="o33554" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely pilosulous" name="pubescence" src="d0_s5" to="glabrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o33555" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathial arrangement: terminal dichasial branches usually 2, occasionally reduced to congested cyme, 1–2-branched (often congested and difficult to discern);</text>
      <biological_entity id="o33556" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o33557" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="1-2-branched" value_original="1-2-branched" />
      </biological_entity>
      <biological_entity id="o33558" name="cyme" name_original="cyme" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pleiochasial bracts 2–4, often whorled, wholly green or paler green at base, similar in shape and size to distal leaves;</text>
      <biological_entity id="o33559" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o33560" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s8" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="wholly" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character constraint="at base" constraintid="o33561" is_modifier="false" name="coloration" src="d0_s8" value="paler green" value_original="paler green" />
      </biological_entity>
      <biological_entity id="o33561" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o33562" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>dichasial bracts highly reduced, rarely absent in highly congested clusters.</text>
      <biological_entity id="o33563" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o33564" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" modifier="highly" name="size" src="d0_s9" value="reduced" value_original="reduced" />
        <character constraint="in highly congested clusters" is_modifier="false" modifier="rarely" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cyathia: peduncle 0.9–1.5 mm.</text>
      <biological_entity id="o33565" name="cyathium" name_original="cyathia" src="d0_s10" type="structure" />
      <biological_entity id="o33566" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucre usually campanulate, occasionally nearly hemispheric, 1.5–1.9 × 1.2–1.8 mm, glabrous;</text>
      <biological_entity id="o33567" name="involucre" name_original="involucre" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="occasionally nearly" name="shape" src="d0_s11" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>involucral lobes divided into several linear, smooth lobes;</text>
      <biological_entity id="o33568" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s12" value="involucral" value_original="involucral" />
        <character constraint="into lobes" constraintid="o33569" is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o33569" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="several" value_original="several" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gland 1, yellow-green, stipitate, clavate, 1–1.4 × 1–1.2 mm, opening circular (occasionally flattened from pressing), with annular rim, glabrous;</text>
      <biological_entity id="o33570" name="gland" name_original="gland" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="circular" value_original="circular" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33571" name="rim" name_original="rim" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="annular" value_original="annular" />
      </biological_entity>
      <relation from="o33570" id="r2758" name="with" negation="false" src="d0_s13" to="o33571" />
    </statement>
    <statement id="d0_s14">
      <text>appendages absent.</text>
      <biological_entity id="o33572" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers 8–15.</text>
      <biological_entity id="o33573" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s15" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary glabrous or puberulent;</text>
      <biological_entity id="o33574" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o33575" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 0.8–1.3 mm, 2-fid 1/2 to nearly entire length.</text>
      <biological_entity id="o33576" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o33577" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules broadly ovoid, 2.8–3.8 × 4–5.3 mm, 3-lobed, usually glabrous, rarely sparsely puberulent;</text>
      <biological_entity id="o33578" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s18" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s18" to="5.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>columella 2.1–2.8 mm.</text>
      <biological_entity id="o33579" name="columella" name_original="columella" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s19" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds brown-gray to ashy gray, broadly deltoid, 2.4–2.8 × 1.9–2.4 mm, angular in cross-section, dorsal face strongly acute-carinate, tuberculate, with broad rounded tubercles in 2 rows;</text>
      <biological_entity id="o33580" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="brown-gray" name="coloration" src="d0_s20" to="ashy gray" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s20" value="deltoid" value_original="deltoid" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s20" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="width" src="d0_s20" to="2.4" to_unit="mm" />
        <character constraint="in cross-section" constraintid="o33581" is_modifier="false" name="arrangement_or_shape" src="d0_s20" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity id="o33581" name="cross-section" name_original="cross-section" src="d0_s20" type="structure" />
      <biological_entity constraint="dorsal" id="o33582" name="face" name_original="face" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s20" value="acute-carinate" value_original="acute-carinate" />
        <character is_modifier="false" name="relief" src="d0_s20" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o33583" name="tubercle" name_original="tubercles" src="d0_s20" type="structure">
        <character is_modifier="true" name="width" src="d0_s20" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o33584" name="row" name_original="rows" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <relation from="o33582" id="r2759" name="with" negation="false" src="d0_s20" to="o33583" />
      <relation from="o33583" id="r2760" name="in" negation="false" src="d0_s20" to="o33584" />
    </statement>
    <statement id="d0_s21">
      <text>caruncle 0.1 mm. 2n = 28.</text>
      <biological_entity id="o33585" name="caruncle" name_original="caruncle" src="d0_s21" type="structure">
        <character name="some_measurement" src="d0_s21" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33586" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia heterophylla occurs from the southern United States, where it is likely naturalized, south through Mexico and Central America to South America. Because of its weediness, the precise native range in tropical and subtropical parts of the New World is not well understood. It has become widely established also in warm areas of the Old World. Leaf shape in this species is highly polymorphic within both populations and individuals. Euphorbia heterophylla can appear superficially similar to E. cyathophora but differs in its stipitate, circular involucral glands and its floral bracts that are at most very pale at the base (never colored as is typical in E. cyathophora).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting nearly year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="nearly" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="nearly" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, Ala., Ariz., Calif., Fla., Ga., La., Miss., N.Mex., Tex.; Mexico; Central America; South America; introduced also in Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>