<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="illustration_page">370</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Linum</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">lewisii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 210. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linum;species lewisii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">perenne</taxon_name>
    <taxon_name authority="(Pursh) Hultén" date="unknown" rank="subspecies">lewisii</taxon_name>
    <taxon_hierarchy>genus linum;species perenne;subspecies lewisii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">perenne</taxon_name>
    <taxon_name authority="(Pursh) Eaton &amp; Wright" date="unknown" rank="variety">lewisii</taxon_name>
    <taxon_hierarchy>genus l.;species perenne;variety lewisii</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Lewis's or wild blue flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 5–80 cm, glabrous or glabrate throughout, ± glaucous.</text>
      <biological_entity id="o26511" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading or ascending, branched from near base and in inflorescence.</text>
      <biological_entity id="o26512" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading or ascending" />
        <character constraint="from near base and in inflorescence" constraintid="o26513" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o26513" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear to linear-lanceolate or linear-oblanceolate, 5–30 × 0.5–3 (–4.5) mm.</text>
      <biological_entity id="o26514" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26515" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="linear-lanceolate or linear-oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open panicles or racemes.</text>
      <biological_entity id="o26516" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o26517" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <biological_entity id="o26518" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <relation from="o26516" id="r2155" name="open" negation="false" src="d0_s3" to="o26517" />
      <relation from="o26516" id="r2156" name="open" negation="false" src="d0_s3" to="o26518" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 5–20 mm.</text>
      <biological_entity id="o26519" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers homostylous;</text>
      <biological_entity id="o26520" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="homostylous" value_original="homostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals elliptic or elliptic-ovate, 3.5–6 mm, margins glabrous, apex acute;</text>
      <biological_entity id="o26521" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26522" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26523" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals usually blue, sometimes white, base whitish or yellowish, cuneate-obovate, 6–23 mm;</text>
      <biological_entity id="o26524" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26525" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate-obovate" value_original="cuneate-obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 3–10 mm;</text>
      <biological_entity id="o26526" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1–2.2 mm;</text>
      <biological_entity id="o26527" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodia present;</text>
      <biological_entity id="o26528" name="staminodium" name_original="staminodia" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles distinct, 2–12 mm;</text>
      <biological_entity id="o26529" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas thickened ellipsoid-capitate.</text>
      <biological_entity id="o26530" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="ellipsoid-capitate" value_original="ellipsoid-capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid globose, 4–8 × 5–6 mm, apex acute, segments ± persistent on plant, margins arachnoid-ciliate.</text>
      <biological_entity id="o26531" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26532" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o26533" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character constraint="on plant" constraintid="o26534" is_modifier="false" modifier="more or less" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o26534" name="plant" name_original="plant" src="d0_s13" type="structure" />
      <biological_entity id="o26535" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="arachnoid-ciliate" value_original="arachnoid-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2.5–5 × 1.5–3 mm. 2n = 18.</text>
      <biological_entity id="o26536" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26537" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Ariz., Ark., Calif., Colo., Idaho, Kans., La., Minn., Mo., Mont., N.Dak., N.Mex., Nebr., Nev., Okla., Oreg., S.Dak., Tex., Utah, W.Va., Wash., Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Linum lewisii grows in many habitats in western North America from northern Mexico to Alaska east to the Great Plains in the United States and to the west side of Hudson and James bays in Canada; it appears to be less common in the Great Basin. A component of wildflower seed mixes, the species may be expanding its range. Some authors have considered it conspecific with L. perenne, and many collections in herbaria are identified as L. perenne without an indication of variety; they are most likely L. lewisii var. lewisii (D. J. Ockendon 1971; C. M. Rogers 1984). Because of the prevalence of L. bienne, L. perenne, and L. usitatissimum in bird seed and wildflower mixes, it may be that these three non-natives are becoming more common than in the past. Capitate stigmas distinguish L. lewisii from L. bienne and L. usitatissimum, which have linear or clavate stigmas. Distinguishing L. lewisii from L. perenne is more difficult: the size of flower parts in the homostyled L. lewisii varies along elevational and latitudinal gradients, with smaller flowers and flower parts in higher elevations and higher latitudes; except in var. lepagei, the styles are always longer than the stamens. In the heterostyled L. perenne, populations usually include plants in which flowers have stamens much longer than the very short styles (short-styled form) and plants in which flowers have stamens much shorter than the very long styles, up to twice as long as the stamens (long-styled form).</discussion>
  <discussion>C. A. Kearns and D. W. Inouye (1994) reported that Linum lewisii is facultatively autogamous but tends not to set seed in the absence of pollinators; small bees and flies are the most common pollinators. A. Cronquist et al. (1997b) reported unusual populations of L. lewisii on sandy soil in Nye County, Nevada, in the 40-Mile-Canyon drainage, that had persistent, ascending, pale blue petals with darker veins.</discussion>
  <references>
    <reference>Becker, J. D. T. 2010. Taxonomy of the Linum lewisii Complex in Canada Based on Macromorphology, Micromorphology, and Phytogeography. Honors thesis. University of Manitoba.</reference>
    <reference>Kearns, C. A. and D. W. Inouye. 1994. Fly pollination in Linum lewisii (Linaceae). Amer. J. Bot. 81: 1091–1095.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals mostly white; Hudson and James Bay regions.</description>
      <determination>3c. Linum lewisii var. lepagei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually blue; w North America.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals (8–)12–23 mm; styles 6–12 mm.</description>
      <determination>3a. Linum lewisii var. lewisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals 6–13 mm; styles 2–6 mm.</description>
      <determination>3b. Linum lewisii var. alpicola</determination>
    </key_statement>
  </key>
</bio:treatment>