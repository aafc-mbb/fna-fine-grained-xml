<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">ZIZIPHUS</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">jujuba</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Ziziphus no. 1. 1768</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ziziphus;species jujuba</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200013464</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhamnus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">zizyphus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 194. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus rhamnus;species zizyphus</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Common jujube</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, 2–12 m;</text>
      <biological_entity id="o20949" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>secondary branches reddish, glabrescent, not thorn-tipped, axillary thorns absent;</text>
      <biological_entity constraint="secondary" id="o20951" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o20952" name="thorn" name_original="thorns" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipular spines usually present, straight or curving, 15–40 mm, solitary or paired, sometimes absent.</text>
      <biological_entity constraint="stipular" id="o20953" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s2" value="curving" value_original="curving" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, alternate;</text>
      <biological_entity id="o20954" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green abaxially, darker green and glossy adaxially, ovate to ovatelanceolate or elliptic-oblong, 3–6 cm, coriaceous, base oblique, margins crenate-serrate, apex usually obtuse to rounded, rarely acute, surfaces glabrous;</text>
      <biological_entity id="o20955" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="darker green" value_original="darker green" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="ovatelanceolate or elliptic-oblong" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o20956" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s4" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o20957" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o20958" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>3-veined from base.</text>
      <biological_entity id="o20959" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="from base" constraintid="o20960" is_modifier="false" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o20960" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymes, 2–8-flowered, or rarely flowers solitary.</text>
      <biological_entity constraint="inflorescences" id="o20961" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
      <biological_entity id="o20962" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium and sepals yellow-green, petals pale-yellow.</text>
      <biological_entity id="o20963" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20964" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o20965" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o20966" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Drupes ripening through yellow-green to dark red or reddish purple, ellipsoid to narrowly ovoid, 15–20 (–30) mm.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 24, 36, 48, 60, 72, 96.</text>
      <biological_entity id="o20967" name="drupe" name_original="drupes" src="d0_s8" type="structure">
        <character constraint="through yellow-green to dark red or reddish purple , ellipsoid to narrowly ovoid , 15-20(-30) mm" is_modifier="false" name="life_cycle" src="d0_s8" value="ripening" value_original="ripening" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20968" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="24" value_original="24" />
        <character name="quantity" src="d0_s9" value="36" value_original="36" />
        <character name="quantity" src="d0_s9" value="48" value_original="48" />
        <character name="quantity" src="d0_s9" value="60" value_original="60" />
        <character name="quantity" src="d0_s9" value="72" value_original="72" />
        <character name="quantity" src="d0_s9" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The fruits of Ziziphus jujuba have a datelike taste and are eaten fresh, dried, candied, or preserved. Hundreds of cultivars have been developed in China. The spineless var. inermis (Bunge) Rehder sometimes is identified as a cultivar.</discussion>
  <discussion>The species was first introduced to North America in 1837 and has spread widely. It is not always clear whether it is naturalized or persisting from earlier plantings.</discussion>
  <discussion>The name Ziziphus zizyphus (Linnaeus) H. Karsten, sometimes used for this species, is a tautonym and therefore illegitimate.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old home and ranch sites, fencerows, fields, pastures, roadsides, weedy riparian woods, alluvial slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old home" />
        <character name="habitat" value="ranch sites" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="weedy riparian woods" />
        <character name="habitat" value="alluvial slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ariz., Calif., Fla., Ga., La., Tex., Utah; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>