<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">PHYLLANTHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHYLLANTHUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">niruri</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 981. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phyllanthaceae;genus phyllanthus;species niruri</taxon_hierarchy>
    <other_info_on_name type="fna_id">220010364</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phyllanthus</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">lathyroides</taxon_name>
    <taxon_hierarchy>genus phyllanthus;species lathyroides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">niruri</taxon_name>
    <taxon_name authority="(Kunth) G. L. Webster" date="unknown" rank="subspecies">lathyroides</taxon_name>
    <taxon_hierarchy>genus p.;species niruri;subspecies lathyroides</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Gale of the wind</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, monoecious, 1–5 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>branching phyllanthoid.</text>
      <biological_entity id="o866" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems: main-stems terete, not winged, glabrous;</text>
      <biological_entity id="o867" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o868" name="main-stem" name_original="main-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ultimate branchlets subterete, not winged, glabrous.</text>
      <biological_entity id="o869" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity constraint="ultimate" id="o870" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves on main-stems spiral, scalelike;</text>
      <biological_entity id="o871" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o872" name="main-stem" name_original="main-stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course" src="d0_s4" value="spiral" value_original="spiral" />
      </biological_entity>
      <relation from="o871" id="r73" name="on" negation="false" src="d0_s4" to="o872" />
    </statement>
    <statement id="d0_s5">
      <text>stipules not auriculate, brown.</text>
      <biological_entity id="o873" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves on ultimate branchlets distichous, well developed;</text>
      <biological_entity id="o874" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="development" notes="" src="d0_s6" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o875" name="branchlet" name_original="branchlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="distichous" value_original="distichous" />
      </biological_entity>
      <relation from="o874" id="r74" name="on" negation="false" src="d0_s6" to="o875" />
    </statement>
    <statement id="d0_s7">
      <text>stipules not auriculate, brown;</text>
      <biological_entity id="o876" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade elliptic, 11–20 × 4.5–9 mm, base obtuse to rounded, apex obtuse, both surfaces glabrous.</text>
      <biological_entity id="o877" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o878" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o879" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o880" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences cymules or flowers solitary, unisexual, proximal with 3–7 staminate flowers, distal with 1 pistillate flower.</text>
      <biological_entity id="o881" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character constraint="with flowers" constraintid="o884" is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character constraint="with flower" constraintid="o885" is_modifier="false" name="position_or_shape" notes="" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o882" name="cymule" name_original="cymules" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character constraint="with flowers" constraintid="o884" is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character constraint="with flower" constraintid="o885" is_modifier="false" name="position_or_shape" notes="" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o883" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character constraint="with flowers" constraintid="o884" is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character constraint="with flower" constraintid="o885" is_modifier="false" name="position_or_shape" notes="" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o884" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="7" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o885" name="flower" name_original="flower" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels: staminate 1.2–1.8 mm, pistillate spreading in fruit, 4–7 mm.</text>
      <biological_entity id="o886" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character constraint="in fruit" constraintid="o887" is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o887" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals 5 (–6), pale green, flat, 1.5–3 mm;</text>
      <biological_entity id="o888" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o889" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary extrastaminal, 5 (–6) glands;</text>
      <biological_entity id="o890" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o891" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="extrastaminal" value_original="extrastaminal" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o892" name="gland" name_original="glands" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 3, filaments connate 1/2 length.</text>
      <biological_entity id="o893" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o894" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o895" name="all" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character name="length" src="d0_s13" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: sepals 5, green, flat, 3–3.5 mm, pinnately veined;</text>
      <biological_entity id="o896" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o897" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s14" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectary annular, unlobed.</text>
      <biological_entity id="o898" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o899" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules 3.5 mm diam., smooth.</text>
      <biological_entity id="o900" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character name="diameter" src="d0_s16" unit="mm" value="3.5" value_original="3.5" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds uniformly brown, 1.5–1.8 mm, verrucose.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 26 (Costa Rica).</text>
      <biological_entity id="o901" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s17" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o902" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phyllanthus niruri is found in the flora area only in DeWitt, Fayette, and Lavaca counties (and historically from Gonzales County, where it appears to be extirpated; L. E. Brown and S. J. Marcus 1998); it is widespread in the American tropics. Like P. urinaria, it is widely used in folk medicine and is the subject of intense pharmacological research. Plants from outside the West Indies and Caribbean northern South America often have been segregated as subsp. lathyroides; the differences are trivial and recent authors (G. L. Webster 2001; V. W. Steinmann 2007) did not subdivide the species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>River and stream banks, sand.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="river" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="sand" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–120 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="120" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>