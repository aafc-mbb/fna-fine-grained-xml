<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Grisebach" date="1866" rank="genus">REYNOSIA</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Cub.,</publication_title>
      <place_in_publication>33. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus reynosia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Alvaro Reynoso, 1829–1888, Cuban chemist and agriculturalist, who revolutionized the sugar industry</other_info_on_name>
    <other_info_on_name type="fna_id">128196</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Red ironwood</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, unarmed;</text>
      <biological_entity id="o4461" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bud-scales present.</text>
      <biological_entity id="o4463" name="bud-scale" name_original="bud-scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite;</text>
      <biological_entity id="o4464" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade not gland-dotted;</text>
    </statement>
    <statement id="d0_s4">
      <text>pinnately veined, secondary-veins straight nearly to margins, higher order veins forming adaxially raised reticulum enclosing isodiametric areoles.</text>
      <biological_entity id="o4465" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o4466" name="secondary-vein" name_original="secondary-veins" src="d0_s4" type="structure">
        <character constraint="to margins" constraintid="o4467" is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="position" notes="" src="d0_s4" value="higher" value_original="higher" />
      </biological_entity>
      <biological_entity id="o4467" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o4468" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o4469" name="reticulum" name_original="reticulum" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="adaxially" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o4470" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <relation from="o4468" id="r400" name="forming" negation="false" src="d0_s4" to="o4469" />
      <relation from="o4468" id="r401" name="enclosing" negation="false" src="d0_s4" to="o4470" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, fascicles or flowers solitary;</text>
      <biological_entity id="o4471" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o4472" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pedicels not fleshy in fruit.</text>
      <biological_entity id="o4473" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o4474" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4474" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o4475" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o4476" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium cupulate, 2–4 mm wide;</text>
      <biological_entity id="o4477" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals [4–] 5, spreading, yellow-green, triangular-ovate, small-keeled adaxially, not crested;</text>
      <biological_entity id="o4478" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular-ovate" value_original="triangular-ovate" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s10" value="small-keeled" value_original="small-keeled" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0 [4–5];</text>
      <biological_entity id="o4479" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary fleshy, filling hypanthium, margin entire;</text>
      <biological_entity id="o4480" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4481" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure" />
      <biological_entity id="o4482" name="margin" name_original="margin" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o4480" id="r402" name="filling" negation="false" src="d0_s12" to="o4481" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5;</text>
      <biological_entity id="o4483" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, 2-locular;</text>
      <biological_entity id="o4484" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1.</text>
      <biological_entity id="o4485" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits drupes, 10–20 mm;</text>
      <biological_entity constraint="fruits" id="o4486" name="drupe" name_original="drupes" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stone 1, indehiscent.</text>
      <biological_entity id="o4487" name="stone" name_original="stone" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 15 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., West Indies, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Schirarend, C. and P. Hoffmann. 1993. Untersuchungen zur Blutenmorphologie der Gattung Reynosia Griseb. (Rhamnaceae). Flora, Morphol. Geobot. Ecophysiol. 188: 275–286.</reference>
  </references>
  
</bio:treatment>