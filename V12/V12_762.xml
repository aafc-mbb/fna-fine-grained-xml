<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann ex Chapman" date="1883" rank="species">deltoidea</taxon_name>
    <taxon_name authority="(Small) Oudejans" date="1993" rank="subspecies">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>World Cat. Euphorb. Cum. Suppl. I,</publication_title>
      <place_in_publication>11. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species deltoidea;subspecies pinetorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">117267</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="Small" date="1905" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. New York Bot. Gard.</publication_title>
      <place_in_publication>3: 429. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus chamaesyce;species pinetorum</taxon_hierarchy>
  </taxon_identification>
  <number>37c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, not mat-forming, usually red, villous-hirsute, hairs straight and spreading, 0.6–0.7 mm.</text>
      <biological_entity id="o13671" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13672" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s0" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stipules villous-hirsute with straight, spreading hairs;</text>
      <biological_entity id="o13673" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character constraint="with hairs" constraintid="o13674" is_modifier="false" name="pubescence" src="d0_s1" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13674" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole villous-hirsute with straight, spreading hairs;</text>
      <biological_entity id="o13675" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character constraint="with hairs" constraintid="o13676" is_modifier="false" name="pubescence" src="d0_s2" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13676" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 2–4.5 × 2–4.5 mm, as long as wide, surfaces silver-green, villous-hirsute with straight, spreading hairs.</text>
      <biological_entity id="o13677" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o13678" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="silver-green" value_original="silver-green" />
        <character constraint="with hairs" constraintid="o13679" is_modifier="false" name="pubescence" src="d0_s3" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13679" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucre villous-hirsute with straight, spreading hairs.</text>
      <biological_entity id="o13680" name="involucre" name_original="involucre" src="d0_s4" type="structure">
        <character constraint="with hairs" constraintid="o13681" is_modifier="false" name="pubescence" src="d0_s4" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13681" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers: ovary villous-hirsute with straight, spreading hairs.</text>
      <biological_entity id="o13682" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13683" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character constraint="with hairs" constraintid="o13684" is_modifier="false" name="pubescence" src="d0_s5" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13684" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules villous-hirsute with straight, spreading hairs.</text>
      <biological_entity id="o13685" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character constraint="with hairs" constraintid="o13686" is_modifier="false" name="pubescence" src="d0_s6" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
      <biological_entity id="o13686" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies pinetorum is the southernmost of the three mainland subspecies.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine rocklands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>