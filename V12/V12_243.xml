<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Trelease" date="1888" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 1: 109. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species parryi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101411</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Parry’s ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, 2–6 m.</text>
      <biological_entity id="o4088" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, not rooting at nodes;</text>
      <biological_entity id="o4089" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o4090" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o4090" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets grayish green to brown, not thorn-tipped, usually round, sometimes ± angled, in cross-section, flexible, lanate to woolly, glabrescent.</text>
      <biological_entity id="o4091" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s2" to="brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="thorn-tipped" value_original="thorn-tipped" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="round" value_original="round" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s2" value="pliable" value_original="flexible" />
        <character char_type="range_value" from="lanate" name="pubescence" src="d0_s2" to="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o4092" name="cross-section" name_original="cross-section" src="d0_s2" type="structure" />
      <relation from="o4091" id="r383" name="in" negation="false" src="d0_s2" to="o4092" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–8 mm;</text>
      <biological_entity id="o4093" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4094" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade flat to slightly cupped, oblong or ± elliptic, 12–50 × 6–20 mm, base obtuse to rounded, margins entire or obscurely glandular-denticulate, narrowly revolute, glands 21–36, apex obtuse, abaxial surface green, cobwebby, soon glabrescent, adaxial surface dark green, shiny, villosulous, glabrescent;</text>
      <biological_entity id="o4095" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4096" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4097" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o4098" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s4" value="glandular-denticulate" value_original="glandular-denticulate" />
        <character is_modifier="false" modifier="narrowly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o4099" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="21" name="quantity" src="d0_s4" to="36" />
      </biological_entity>
      <biological_entity id="o4100" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4101" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="cobwebby" value_original="cobwebby" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4102" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villosulous" value_original="villosulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>usually 3-veined from base, rarely pinnately veined.</text>
      <biological_entity id="o4103" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="from base" constraintid="o4104" is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="rarely pinnately" name="architecture" notes="" src="d0_s5" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o4104" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary or terminal, paniclelike, 5–15 cm.</text>
      <biological_entity id="o4105" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="paniclelike" value_original="paniclelike" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals, petals, and nectary deep blue.</text>
      <biological_entity id="o4106" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4107" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o4108" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o4109" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 2.5–4 mm wide, lobed;</text>
      <biological_entity id="o4110" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves smooth, not or weakly crested.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o4111" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not; weakly" name="shape" src="d0_s9" value="crested" value_original="crested" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4112" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus parryi occurs in the outer coast ranges of Oregon (Benton and Lane counties) and from Humboldt County south to Napa County in California; it is reported to hybridize with C. foliosus, C. incanus, and C. thyrsiflorus (H. McMinn 1944). The deep blue sepals and petals, cobwebby indumentum on young leaves, and narrowly revolute leaf margins are diagnostic.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils, open sites, flats, mixed evergreen and redwood forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="mixed evergreen" />
        <character name="habitat" value="redwood forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>