<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Meisner" date="1837" rank="genus">ADOLPHIA</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Vasc. Gen.</publication_title>
      <place_in_publication>1: 70; 2: 50. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus adolphia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Adolphe Brongniart, 1801–1876, French botanist and student of Rhamnaceae</other_info_on_name>
    <other_info_on_name type="fna_id">100622</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Prickbush</other_name>
  <other_name type="common_name">spinebrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, armed with thorns, secondary branches and branchlets green;</text>
      <biological_entity id="o30398" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="with thorns" constraintid="o30399" is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o30399" name="thorn" name_original="thorns" src="d0_s0" type="structure" />
      <biological_entity constraint="secondary" id="o30400" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <biological_entity constraint="secondary" id="o30401" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bud-scales absent.</text>
      <biological_entity id="o30402" name="bud-scale" name_original="bud-scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves early deciduous, usually absent by flowering, opposite or subopposite;</text>
      <biological_entity id="o30403" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="by flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade not gland-dotted;</text>
    </statement>
    <statement id="d0_s4">
      <text>pinnately veined (obscurely, appearing 1-veined or vaguely 3-veined from base).</text>
      <biological_entity id="o30404" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, cymes or flowers solitary;</text>
      <biological_entity id="o30405" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o30406" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o30407" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels not fleshy in fruit.</text>
      <biological_entity id="o30408" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o30410" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30409" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o30410" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30410" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o30411" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o30412" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium broadly obconic to hemispheric-campanulate, 1.5–3 mm wide;</text>
      <biological_entity id="o30413" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="broadly obconic" name="shape" src="d0_s9" to="hemispheric-campanulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals (4–) 5, spreading, whitish to greenish white, ovate-triangular or triangular-deltate, keeled adaxially;</text>
      <biological_entity id="o30414" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s10" to="greenish white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular-deltate" value_original="triangular-deltate" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals (4–) 5, white, sometimes yellow-tipped, hooded, spatulate, clawed;</text>
      <biological_entity id="o30415" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="yellow-tipped" value_original="yellow-tipped" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary fleshy, 5-angled, lining hypanthium;</text>
      <biological_entity id="o30416" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="shape" src="d0_s12" value="5-angled" value_original="5-angled" />
      </biological_entity>
      <biological_entity id="o30417" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure" />
      <relation from="o30416" id="r2491" name="lining" negation="false" src="d0_s12" to="o30417" />
    </statement>
    <statement id="d0_s13">
      <text>stamens (4–) 5;</text>
      <biological_entity id="o30418" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s13" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, 3-locular;</text>
      <biological_entity id="o30419" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1.</text>
      <biological_entity id="o30420" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o30421" name="capsule" name_original="capsules" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Mantese, A. and D. Medan. 1993. Anatomía y arquitectura foliares de Colletia y Adolphia (Rhamnaceae). Darwiniana 32: 91–97.</reference>
    <reference>Tortosa, R. D. 1993. Revisión del género Adolphia (Rhamnaceae: Colletieae). Darwiniana 32: 185–189.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Secondary branches and branchlets glabrous or glabrate, branchlets 1–1.5 mm diam.; leaf blades elliptic-oblong to obovate, 1- or vaguely 3-veined from base; hypanthia broadly obconic, 2.5–3 mm wide, sides nearly straight; sepals triangular-deltate, length equaling width, margins straight; petals 2–2.5 mm, deeply hooded.</description>
      <determination>1. Adolphia californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Secondary branches and branchlets minutely and persistently short-hispid, slowly glabrescent, branchlets usually 0.5–1 mm diam.; leaf blades narrowly oblanceolate to linear, 1-veined from base; hypanthia hemispheric-campanulate, 1.5 mm wide, sides convex; sepals ovate-triangular, length nearly 2 times width, margins curved; petals 1.2–1.5 mm, shallowly hooded.</description>
      <determination>2. Adolphia infesta</determination>
    </key_statement>
  </key>
</bio:treatment>