<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Persoon" date="1806" rank="subgenus">Esula</taxon_name>
    <taxon_name authority="Linnaeus" date="1762" rank="species">terracina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 1: 654. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;subgenus esula;species terracina</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101645</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tithymalus</taxon_name>
    <taxon_name authority="(Linnaeus) Klotzsch &amp; Garcke" date="unknown" rank="species">terracinus</taxon_name>
    <taxon_hierarchy>genus tithymalus;species terracinus</taxon_hierarchy>
  </taxon_identification>
  <number>120.</number>
  <other_name type="common_name">Terracina or carnation spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial or biennial, with taproot.</text>
      <biological_entity id="o102" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o103" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o102" id="r9" name="with" negation="false" src="d0_s0" to="o103" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, unbranched or branched, 10–100 cm, glabrous.</text>
      <biological_entity id="o104" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole absent;</text>
      <biological_entity id="o105" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o106" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear, linear-lanceolate, oblongelliptic, or obovate, 4–50 × 2–10 mm, base obtuse or truncate, margins finely serrulate, apex acute, obtuse, or truncate, sometimes mucronulate, surfaces glabrous;</text>
      <biological_entity id="o107" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o108" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o109" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o110" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o111" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o112" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation inconspicuous, only midvein prominent.</text>
      <biological_entity id="o113" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o114" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cyathial arrangement: terminal pleiochasial branches 2–5, each 1–5 times 2-branched;</text>
      <biological_entity id="o115" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o116" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-5 times 2-branched" value_original="1-5 times 2-branched " />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pleiochasial bracts lanceolate, elliptic or ovate, similar in size to distal leaves;</text>
      <biological_entity id="o117" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o118" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pleiochasial" value_original="pleiochasial" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o119" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="size" value_original="size" />
      </biological_entity>
      <relation from="o118" id="r10" name="in" negation="false" src="d0_s6" to="o119" />
    </statement>
    <statement id="d0_s7">
      <text>dichasial bracts distinct, ovate to subreniform, base cuneate to cordate, margins finely serrulate, apex acute, obtuse, or rounded, sometimes mucronulate or cuspidate;</text>
      <biological_entity id="o120" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o121" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="subreniform" />
      </biological_entity>
      <biological_entity id="o122" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="cordate" />
      </biological_entity>
      <biological_entity id="o123" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o124" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary cymose branches 0–7.</text>
      <biological_entity id="o125" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o126" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cyathia: peduncle 1–3 mm.</text>
      <biological_entity id="o127" name="cyathium" name_original="cyathia" src="d0_s9" type="structure" />
      <biological_entity id="o128" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucre cupulate to slightly turbinate, 1.1–2 × 1.3–1.5 mm, glabrous or puberulent;</text>
      <biological_entity id="o129" name="involucre" name_original="involucre" src="d0_s10" type="structure">
        <character char_type="range_value" from="cupulate" name="shape" src="d0_s10" to="slightly turbinate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glands 4, elliptic to trapezoidal, 0.6–0.8 × 1–2 mm;</text>
      <biological_entity id="o130" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="trapezoidal" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s11" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>horns slightly convergent to divergent, 1–2 mm.</text>
      <biological_entity id="o131" name="horn" name_original="horns" src="d0_s12" type="structure">
        <character char_type="range_value" from="slightly convergent" name="arrangement" src="d0_s12" to="divergent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers 15–20.</text>
      <biological_entity id="o132" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o133" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o134" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 1–1.8 mm, 2-fid.</text>
      <biological_entity id="o135" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o136" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules depressed-globose, 2.5–3 × 3–4.5 mm, deeply 3-lobed;</text>
      <biological_entity id="o137" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s16" to="3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>cocci rounded to subangular, smooth, glabrous;</text>
      <biological_entity id="o138" name="cocci" name_original="cocci" src="d0_s17" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s17" to="subangular" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella 1.9–2.3 mm.</text>
      <biological_entity id="o139" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s18" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds pale gray, subovoid, 1.6–2.4 × 1.3–1.8 mm, smooth;</text>
      <biological_entity id="o140" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="pale gray" value_original="pale gray" />
        <character is_modifier="false" name="shape" src="d0_s19" value="subovoid" value_original="subovoid" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s19" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s19" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>caruncle boatshaped, 0.4–0.6 × 0.4–0.6 mm.</text>
      <biological_entity id="o141" name="caruncle" name_original="caruncle" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="boat-shaped" value_original="boat-shaped" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s20" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s20" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia terracina is native to the Mediterranean region of Europe. This species is invasive and spreading rapidly, displacing native coastal scrub in southern California, and has been listed as a noxious weed by that state.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges of cultivated fields and woodlands, roadsides, waste areas, pastures, coastal bluffs, dunes, riparian areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of cultivated fields and woodlands , roadsides , waste areas , pastures , coastal bluffs , dunes ," />
        <character name="habitat" value="cultivated fields" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="areas" modifier="riparian" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe; introduced also in Mexico, s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>