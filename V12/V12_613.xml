<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ACALYPHA</taxon_name>
    <taxon_name authority="Müller Arg. in A. P. de Candolle and A. L. P. P. de Candolle" date="1866" rank="species">wilkesiana</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>15(2): 817. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus acalypha;species wilkesiana</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242300270</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acalypha</taxon_name>
    <taxon_name authority="Roxburgh" date="unknown" rank="species">amentacea</taxon_name>
    <taxon_name authority="(Müller Arg.) Fosberg" date="unknown" rank="subspecies">wilkesiana</taxon_name>
    <taxon_hierarchy>genus acalypha;species amentacea;subspecies wilkesiana</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Painted copperleaf</other_name>
  <other_name type="common_name">beefsteak plant</other_name>
  <other_name type="common_name">match-me-if-you-can</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 20–50 dm, monoecious.</text>
      <biological_entity id="o19146" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely to densely pubescent, not glandular.</text>
      <biological_entity id="o19147" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent;</text>
      <biological_entity id="o19148" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–6 cm;</text>
      <biological_entity id="o19149" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to broadly ovate or suborbiculate, 9–20 × 4–15 cm, base obtuse to rounded or subcordate, margins serrate-crenate, apex acuminate.</text>
      <biological_entity id="o19150" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly ovate or suborbiculate" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19151" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded or subcordate" />
      </biological_entity>
      <biological_entity id="o19152" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="serrate-crenate" value_original="serrate-crenate" />
      </biological_entity>
      <biological_entity id="o19153" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences usually unisexual, rarely bisexual, axillary;</text>
      <biological_entity id="o19154" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminate peduncle 0.1–1.5 cm, fertile portion 10–20 cm;</text>
      <biological_entity id="o19155" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19156" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate peduncle 1–2 cm, fertile portion 4–15 × 0.5–0.8 cm;</text>
      <biological_entity id="o19157" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual similar to staminate, with 1–2 pistillate bracts near base;</text>
      <biological_entity id="o19158" name="portion" name_original="portion" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o19159" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19160" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o19159" id="r1579" name="near" negation="false" src="d0_s8" to="o19160" />
    </statement>
    <statement id="d0_s9">
      <text>allomorphic pistillate flowers absent.</text>
      <biological_entity id="o19161" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate bracts loosely arranged (inflorescence axis visible between bracts), 2–4 × 3–5 mm, abaxial surface sparsely to moderately pubescent;</text>
      <biological_entity id="o19162" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s10" value="arranged" value_original="arranged" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19163" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lobes 7–9, ovate to lanceolate, 1/4 bract length, except terminal lobe to 1/2 bract length.</text>
      <biological_entity id="o19164" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="9" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="lanceolate" />
        <character name="quantity" src="d0_s11" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o19165" name="bract" name_original="bract" src="d0_s11" type="structure" />
      <biological_entity constraint="terminal" id="o19166" name="lobe" name_original="lobe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="1/2" />
      </biological_entity>
      <biological_entity id="o19167" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="length" value_original="length" />
      </biological_entity>
      <relation from="o19165" id="r1580" name="except" negation="false" src="d0_s11" to="o19166" />
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: pistil 3-carpellate;</text>
      <biological_entity id="o19168" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19169" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles multifid or laciniate.</text>
      <biological_entity id="o19170" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19171" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="multifid" value_original="multifid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules unknown.</text>
      <biological_entity id="o19172" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds unknown.</text>
      <biological_entity id="o19173" name="seed" name_original="seeds" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Acalypha wilkesiana is not known in the wild, but presumably originated in the southwestern Pacific Islands (Bismarck Archipelago east to Fiji). The species is commonly cultivated as an ornamental for its leaves that may be various shades of green, purple, red, orange, and yellow (sometimes variegated), and sometimes contorted into unusual shapes. Despite low seed set, it occasionally becomes naturalized in tropical and subtropical areas. Naturalized plants often lack the distinctive leaf coloration found in cultivated plants. Although sometimes treated as A. amentacea subsp. wilkesiana, DNA sequence data show that A. wilkesiana and A. amentacea are distinct species (V. G. Sagun et al. 2010).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old home sites, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old home sites" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Pacific Islands; introduced also in Mexico, West Indies, Central America, n South America, se Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="n South America" establishment_means="introduced" />
        <character name="distribution" value="se Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>