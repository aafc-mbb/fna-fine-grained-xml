<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">358</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MALPIGHIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MALPIGHIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 425. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 194. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malpighiaceae;genus malpighia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Marcello Malpighi, 1628–1694, Italian anatomist</other_info_on_name>
    <other_info_on_name type="fna_id">119565</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees.</text>
      <biological_entity id="o26129" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually bearing (0–) 2–4 [–10] glands impressed in abaxial surface of blade;</text>
      <biological_entity id="o26131" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="in abaxial surface" constraintid="o26133" is_modifier="false" name="prominence" src="d0_s1" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o26132" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="atypical_quantity" src="d0_s1" to="2" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26133" name="surface" name_original="surface" src="d0_s1" type="structure" />
      <biological_entity id="o26134" name="blade" name_original="blade" src="d0_s1" type="structure" />
      <relation from="o26131" id="r2119" name="bearing" negation="false" src="d0_s1" to="o26132" />
      <relation from="o26133" id="r2120" name="part_of" negation="false" src="d0_s1" to="o26134" />
    </statement>
    <statement id="d0_s2">
      <text>stipules interpetiolar, mostly distinct.</text>
      <biological_entity id="o26135" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="interpetiolar" value_original="interpetiolar" />
        <character is_modifier="false" modifier="mostly" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary, dense corymbs or umbels.</text>
      <biological_entity id="o26136" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o26137" name="corymb" name_original="corymbs" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o26138" name="umbel" name_original="umbels" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels raised on peduncles.</text>
      <biological_entity id="o26139" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o26140" name="peduncle" name_original="peduncles" src="d0_s4" type="structure" />
      <relation from="o26139" id="r2121" name="raised on" negation="false" src="d0_s4" to="o26140" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers all chasmogamous, 6+ mm diam., showy with visible petals, stamens, and styles;</text>
      <biological_entity id="o26141" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s5" upper_restricted="false" />
        <character constraint="with styles" constraintid="o26144" is_modifier="false" name="prominence" src="d0_s5" value="showy" value_original="showy" />
      </biological_entity>
      <biological_entity id="o26142" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o26143" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o26144" name="style" name_original="styles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>calyx glands 6 (–10) (3 sepals each bearing 2 large glands, others very rarely bearing 1–4 smaller glands);</text>
      <biological_entity constraint="calyx" id="o26145" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas bilaterally symmetric, petals pink, lavender, or white, glabrous [glabrate];</text>
      <biological_entity id="o26146" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o26147" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 10, all fertile;</text>
      <biological_entity id="o26148" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="10" value_original="10" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers subequal or 2 opposite posterior-lateral petals larger;</text>
      <biological_entity id="o26149" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="posterior-lateral" id="o26150" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="size" src="d0_s9" value="larger" value_original="larger" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil 3-carpellate, carpels completely [rarely proximally] connate in ovary;</text>
      <biological_entity id="o26151" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
      <biological_entity id="o26152" name="carpel" name_original="carpels" src="d0_s10" type="structure">
        <character constraint="in ovary" constraintid="o26153" is_modifier="false" modifier="completely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o26153" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>styles 3, cylindric, stout;</text>
      <biological_entity id="o26154" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas on internal angle or subterminal, large.</text>
      <biological_entity id="o26155" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" name="size" src="d0_s12" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="internal" id="o26156" name="angle" name_original="angle" src="d0_s12" type="structure" />
      <relation from="o26155" id="r2122" name="on" negation="false" src="d0_s12" to="o26156" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits drupes [berries or very rarely breaking into separate pyrenes], red [sometimes orange];</text>
      <biological_entity constraint="fruits" id="o26157" name="drupe" name_original="drupes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pyrenes 3, connate in center or distinct at maturity but then usually retained in common exocarp, walls hard, bearing rudimentary dorsal and lateral wings and sometimes rudimentary intermediate winglets or dissected outgrowths.</text>
      <biological_entity id="o26158" name="pyrene" name_original="pyrenes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character constraint="in " constraintid="o26160" is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o26159" name="center" name_original="center" src="d0_s14" type="structure" />
      <biological_entity id="o26160" name="exocarp" name_original="exocarp" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="common" value_original="common" />
        <character is_modifier="true" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o26161" name="center" name_original="center" src="d0_s14" type="structure" />
      <biological_entity id="o26162" name="exocarp" name_original="exocarp" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="common" value_original="common" />
        <character is_modifier="true" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o26163" name="center" name_original="center" src="d0_s14" type="structure" />
      <biological_entity id="o26164" name="exocarp" name_original="exocarp" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="common" value_original="common" />
        <character is_modifier="true" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="dorsal and lateral" id="o26166" name="wing" name_original="wings" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o26167" name="outgrowth" name_original="outgrowths" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="dissected" value_original="dissected" />
      </biological_entity>
      <relation from="o26160" id="r2123" modifier="at maturity" name="in" negation="false" src="d0_s14" to="o26161" />
      <relation from="o26160" id="r2124" modifier="at maturity" name="in" negation="false" src="d0_s14" to="o26162" />
      <relation from="o26162" id="r2125" name="in" negation="false" src="d0_s14" to="o26163" />
      <relation from="o26162" id="r2126" name="in" negation="false" src="d0_s14" to="o26164" />
      <relation from="o26165" id="r2127" name="bearing" negation="false" src="d0_s14" to="o26166" />
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o26165" name="wall" name_original="walls" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s14" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="size" src="d0_s14" value="intermediate" value_original="intermediate" />
      </biological_entity>
      <biological_entity constraint="x" id="o26168" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 50 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malpighia coccigera Linnaeus, dwarf- or Singapore-holly, native to the West Indies, is grown as an ornamental. Malpighia emarginata, acerola or Barbados cherry, native to Mexico and Central America, is widely cultivated for its fruits, which are rich in vitamin C.</discussion>
  
</bio:treatment>