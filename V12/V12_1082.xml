<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1819" rank="genus">ARCEUTHOBIUM</taxon_name>
    <taxon_name authority="Engelmann" date="1850" rank="species">campylopodum</taxon_name>
    <taxon_name authority="(Engelmann) Nickrent" date="2012" rank="subspecies">occidentale</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-51: 10. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species campylopodum;subspecies occidentale</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101926</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arceuthobium</taxon_name>
    <taxon_name authority="Engelmann in J. T. Rothrock" date="1879" rank="species">occidentale</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Geogr. Surv., Wheeler,</publication_title>
      <place_in_publication>254, 375. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus arceuthobium;species occidentale</taxon_hierarchy>
  </taxon_identification>
  <number>7k.</number>
  <other_name type="common_name">Digger pine dwarf mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually forming localized infections only.</text>
      <biological_entity id="o12674" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12675" name="infection" name_original="infections" src="d0_s0" type="structure" />
      <relation from="o12674" id="r1068" modifier="only" name="forming localized" negation="false" src="d0_s0" to="o12675" />
    </statement>
    <statement id="d0_s1">
      <text>Stems yellow or orange, 8 (–17) cm;</text>
    </statement>
    <statement id="d0_s2">
      <text>third internode 7–12.7 (–18) × 1.5–1.8 (–3.5) mm, dominant shoot 1.5–5 mm diam. at base.</text>
      <biological_entity id="o12676" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="17" to_unit="cm" />
        <character name="some_measurement" src="d0_s1" unit="cm" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o12677" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="12.7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="12.7" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12678" name="shoot" name_original="shoot" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="dominant" value_original="dominant" />
        <character char_type="range_value" constraint="at base" constraintid="o12679" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12679" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers 3 mm diam.;</text>
      <biological_entity id="o12680" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character name="diameter" src="d0_s3" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 3–4.</text>
      <biological_entity id="o12681" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 4.5 × 3 mm.</text>
      <biological_entity id="o12682" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="mm" value="4.5" value_original="4.5" />
        <character name="width" src="d0_s5" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meiosis occurs in August, with fruits maturing 13 months after pollination.</discussion>
  <discussion>Pinus sabiniana is the principal host of subsp. occidentale; secondary hosts include Pinus attenuata, P. coulteri, P. jeffreyi, and P. ponderosa, as well as some exotic species of pines. Subspecies occidentale occurs in the foothills surrounding the Central Valley and in the Coast Ranges.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov(–Dec); fruiting (Sep–)Oct–Jan(–Feb).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Sep" />
        <character name="fruiting time" char_type="range_value" to="Jan" from="Oct" />
        <character name="fruiting time" char_type="atypical_range" to="Feb" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous and mixed forests, especially with digger pine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="digger" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>