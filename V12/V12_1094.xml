<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">RHAMNUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cathartica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 193. 1753</place_in_publication>
      <other_info_on_pub>(as catharticus)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus rhamnus;species cathartica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200013368</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">European or common buckthorn</other_name>
  <other_name type="common_name">nerprun cathartique</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, (1–) 2–8 m, armed with thorns.</text>
      <biological_entity id="o16068" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="8" to_unit="m" />
        <character constraint="with thorns" constraintid="o16070" is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="8" to_unit="m" />
        <character constraint="with thorns" constraintid="o16070" is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o16070" name="thorn" name_original="thorns" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Branchlets dark to reddish gray or purple, glabrous.</text>
      <biological_entity id="o16071" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character char_type="range_value" from="dark" name="coloration" src="d0_s1" to="reddish gray or purple" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous (often present well after frost), usually opposite to subopposite, rarely alternate, sometimes fascicled on short-shoots;</text>
      <biological_entity id="o16072" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="opposite" modifier="usually" name="arrangement" src="d0_s2" to="subopposite" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character constraint="on short-shoots" constraintid="o16073" is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o16073" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–27 mm;</text>
      <biological_entity id="o16074" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dull green abaxially, glossy darker green adaxially, usually ovate to elliptic-ovate, sometimes broadly elliptic or nearly orbiculate, (2–) 4–7 cm, usually 1–2 times longer than wide, herbaceous, base rounded to rounded-truncate or slightly subcordate, margins crenate-serrate, apex acute to rounded, often abruptly short-acuminate, both surfaces glabrous;</text>
      <biological_entity id="o16075" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="darker green" value_original="darker green" />
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s4" to="elliptic-ovate" />
        <character is_modifier="false" modifier="sometimes broadly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="1-2" value_original="1-2" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o16076" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="rounded-truncate or slightly subcordate" />
      </biological_entity>
      <biological_entity id="o16077" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o16078" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="often abruptly" name="shape" src="d0_s4" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity id="o16079" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>secondary-veins 2–4 pairs, all diverging at nearly same angle or proximal diverging more obtusely.</text>
      <biological_entity id="o16080" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
        <character constraint="at angle" constraintid="o16081" is_modifier="false" name="orientation" src="d0_s5" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="obtusely" name="orientation" src="d0_s5" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o16081" name="angle" name_original="angle" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences fascicles or flowers solitary.</text>
      <biological_entity id="o16082" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o16083" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 2–4 mm.</text>
      <biological_entity id="o16084" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sepals 4.</text>
      <biological_entity id="o16085" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Petals 4.</text>
      <biological_entity id="o16086" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Drupes black, globose to depressed-globose, 5–6 (–8) mm;</text>
      <biological_entity id="o16087" name="drupe" name_original="drupes" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s10" to="depressed-globose" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stones 3–4.2n = 24.</text>
      <biological_entity id="o16088" name="stone" name_original="stones" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16089" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rhamnus cathartica was introduced to North America as an ornamental shrub in the middle 1800s and was originally used for hedges, farm shelterbelts, and wildlife habitat; it is an aggressive invader of woods and prairies and is able to completely displace native vegetation. W. H. Brewer collected R. cathartica (UC 18526) in the 1800s from an unknown location in California. No specimens have been collected in the state since that time and it apparently is not naturalized there.</discussion>
  <discussion>Rhamnus cathartica is a primary host for the soybean aphid, Aphis glycines, native to eastern Asia. It uses the buckthorn as a winter host and spreads to soybean in the spring. The insect was first discovered in North America in 2000 in Wisconsin and subsequently has spread to at least 20 states in the United States and three provinces in Canada. The orange-colored wood of R. cathartica is sometimes used by woodcarvers.</discussion>
  <references>
    <reference>Kurylo, J. S. 2007. Rhamnus cathartica: Native and naturalized distribution and habitat preferences. J. Torrey Bot. Soc. 134: 420–430.</reference>
  </references>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vacant lots, fields, forest edges, fencerows, roadsides, stream channels, riverbanks, ravines, floodplains, swampy habitats, deciduous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="stream channels" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="swampy habitats" />
        <character name="habitat" value="deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1000(–2000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Ariz., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio., Pa., R.I., S.Dak., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; c, sw Asia (China, Russia in w Siberia); nw Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="sw Asia (China)" establishment_means="native" />
        <character name="distribution" value="sw Asia (Russia in w Siberia)" establishment_means="native" />
        <character name="distribution" value="nw Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>