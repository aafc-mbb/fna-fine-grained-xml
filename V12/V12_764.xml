<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="illustration_page">110</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Meisner" date="1837" rank="genus">ADOLPHIA</taxon_name>
    <taxon_name authority="(Kunth) Meisner" date="1837" rank="species">infesta</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Vasc. Gen.</publication_title>
      <place_in_publication>2: 50. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus adolphia;species infesta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220000238</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="unknown" rank="species">infestus</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Gen. Sp.</publication_title>
      <place_in_publication>7(fol.): 47</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus ceanothus;species infestus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">7(qto.):</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">61</taxon_name>
    <taxon_hierarchy>genus 7(qto.):;species 61</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">plate</taxon_name>
    <taxon_name authority="1824 (as infesta)" date="unknown" rank="species">614.</taxon_name>
    <taxon_hierarchy>genus plate;species 614.</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Colubrina</taxon_name>
    <taxon_name authority="(Kunth) Schlechtendal" date="unknown" rank="species">infesta</taxon_name>
    <taxon_hierarchy>genus colubrina;species infesta</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Junco</other_name>
  <other_name type="common_name">Texas adolphia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.3–1 (–2) m, secondary branches and branchlets minutely and persistently short-hispid, slowly glabrescent, branchlets usually 0.5–1 mm diam.</text>
      <biological_entity id="o16584" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o16585" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s0" value="short-hispid" value_original="short-hispid" />
        <character is_modifier="false" modifier="slowly" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o16586" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s0" value="short-hispid" value_original="short-hispid" />
        <character is_modifier="false" modifier="slowly" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o16587" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade narrowly oblanceolate to linear, 3–10 mm, margins entire [rarely dentate], surfaces glabrous;</text>
      <biological_entity id="o16588" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16589" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s1" to="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16590" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16591" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>1-veined from base.</text>
      <biological_entity id="o16592" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o16593" is_modifier="false" name="architecture" src="d0_s2" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o16593" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 1–2.5 mm.</text>
      <biological_entity id="o16594" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium hemispheric-campanulate, 1.5 mm wide, sides convex;</text>
      <biological_entity id="o16595" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16596" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric-campanulate" value_original="hemispheric-campanulate" />
        <character name="width" src="d0_s4" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o16597" name="side" name_original="sides" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals ovate-triangular, length nearly 2 times width, margins curved;</text>
      <biological_entity id="o16598" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16599" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" modifier="nearly" name="l_w_ratio" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16600" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 1.2–1.5 mm, shallowly hooded.</text>
      <biological_entity id="o16601" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o16602" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s6" value="hooded" value_original="hooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open slopes and washes, rocky outcrops, limestone and igneous substrates, roadsides, chaparral, oak-juniper and mixed conifer-oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="igneous substrates" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="mixed conifer-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; c, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>