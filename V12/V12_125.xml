<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">62</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Cavanilles" date="1799" rank="genus">CONDALIA</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">spathulata</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 32. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus condalia;species spathulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101366</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Squaw bush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–1 (–2) m;</text>
      <biological_entity id="o1106" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>primary branches usually not thorn-tipped, secondary branches thorn-tipped, with short-shoots and often some thorn-tipped tertiary branches, glabrous;</text>
      <biological_entity constraint="primary" id="o1107" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o1108" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
        <character is_modifier="false" modifier="often" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1109" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="tertiary" id="o1110" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <relation from="o1108" id="r91" name="with" negation="false" src="d0_s1" to="o1109" />
    </statement>
    <statement id="d0_s2">
      <text>internodes 1–3 (–5) mm.</text>
      <biological_entity id="o1111" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–2 mm;</text>
      <biological_entity id="o1112" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1113" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spatulate, 4–12 (–14) × 1.6–3 mm, subcoriaceous, margins entire, revolute, apex rounded to emarginate or acute, surfaces glabrous;</text>
      <biological_entity id="o1114" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1115" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o1116" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o1117" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="emarginate or acute" />
      </biological_entity>
      <biological_entity id="o1118" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation raised, conspicuous abaxially, abaxial intervein surfaces with rounded transverse ridges, surface appearing as if molded in wax.</text>
      <biological_entity id="o1119" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="raised" value_original="raised" />
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity constraint="intervein" id="o1120" name="surface" name_original="surfaces" src="d0_s5" type="structure" constraint_original="abaxial intervein" />
      <biological_entity id="o1121" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s5" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o1122" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o1123" name="wax" name_original="wax" src="d0_s5" type="substance" />
      <relation from="o1120" id="r92" name="with" negation="false" src="d0_s5" to="o1121" />
      <relation from="o1122" id="r93" name="appearing" negation="false" src="d0_s5" to="o1123" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences on short-shoots, 1–5-flowered.</text>
      <biological_entity id="o1124" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
      <biological_entity id="o1125" name="short-shoot" name_original="short-shoots" src="d0_s6" type="structure" />
      <relation from="o1124" id="r94" name="on" negation="false" src="d0_s6" to="o1125" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels (1.5–) 2–3 mm.</text>
      <biological_entity id="o1126" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: petals 0.</text>
      <biological_entity id="o1127" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1128" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Drupes globose, 3.5–4.5 mm;</text>
      <biological_entity id="o1129" name="drupe" name_original="drupes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stones 1-seeded.</text>
      <biological_entity id="o1130" name="stone" name_original="stones" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-seeded" value_original="1-seeded" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Condalia spathulata occurs in central and southern Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, low hills, sandstone bluffs, gravelly slopes, disturbed sites, shrublands, shortgrass grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="low hills" />
        <character name="habitat" value="bluffs" modifier="sandstone" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="shortgrass grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>