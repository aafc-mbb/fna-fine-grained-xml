<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">CORNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CORNUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 117. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 54. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cornaceae;genus cornus</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin cornu, horn, alluding to the hard wood</other_info_on_name>
    <other_info_on_name type="fna_id">108042</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Dogwood</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, shrubs, or trees, clonal from rhizomes, rooting from decumbent branches, or aclonal;</text>
      <biological_entity id="o19259" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character constraint="from rhizomes" constraintid="o19262" is_modifier="false" name="growth_form" notes="" src="d0_s0" value="clonal" value_original="clonal" />
        <character constraint="from branches" constraintid="o19263" is_modifier="false" name="architecture" notes="" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o19262" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o19263" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs 1-celled, arms either short and ornamented with micropapillae and calcium carbonate crystals, or long, erect, curling, and twisted.</text>
      <biological_entity id="o19264" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-celled" value_original="1-celled" />
      </biological_entity>
      <biological_entity id="o19265" name="arm" name_original="arms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="either" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character constraint="with calcium carbonate crystals" constraintid="o19266" is_modifier="false" name="relief" src="d0_s1" value="ornamented" value_original="ornamented" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="curling" value_original="curling" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="calcium carbonate" id="o19266" name="crystal" name_original="crystals" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lanceolate to broadly ovate;</text>
      <biological_entity id="o19267" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19268" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="broadly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>abaxial surface often papillate.</text>
      <biological_entity id="o19269" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o19270" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="relief" src="d0_s3" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: bracts adnate to inflorescence branches, distal portion either minute and caducous or expanding into showy, nonchlorophyllous involucres.</text>
      <biological_entity id="o19271" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o19272" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character constraint="to inflorescence branches" constraintid="o19273" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="inflorescence" id="o19273" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o19274" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="either" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="false" name="duration" src="d0_s4" value="caducous" value_original="caducous" />
        <character constraint="into involucres" constraintid="o19275" is_modifier="false" name="size" src="d0_s4" value="expanding" value_original="expanding" />
      </biological_entity>
      <biological_entity id="o19275" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels present or absent.</text>
      <biological_entity id="o19276" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium turbinate or urceolate;</text>
      <biological_entity id="o19277" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19278" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals spreading or recurved, usually cream, rarely purple;</text>
      <biological_entity id="o19279" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19280" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens exserted;</text>
      <biological_entity id="o19281" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19282" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers dorsifixed, versatile.</text>
      <biological_entity id="o19283" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19284" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="fixation" src="d0_s9" value="versatile" value_original="versatile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Drupes globose, subglobose, or ellipsoid, slightly fleshy.</text>
    </statement>
    <statement id="d0_s11">
      <text>x = 11.</text>
      <biological_entity id="o19285" name="drupe" name_original="drupes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="x" id="o19286" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 60 (20 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, n, w South America, Eurasia, Africa; predominately northern boreal and temperate regions, also high elevations in subtropical and tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="w South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="predominately northern boreal and temperate regions" establishment_means="native" />
        <character name="distribution" value="also high elevations in subtropical and tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cornus as treated here is a monophyletic genus (Z. E. Murrell 1993; Xiang Q. Y. et al. 2006) that has at various times been more narrowly circumscribed by other authors who have chosen to recognize morphological variation in this diverse group as worthy of generic segregation [for example, Arctocrania (Endlicher) Nakai, Benthamia Lindley (not A. Richard), Benthamidia Spach, Chamaepericlymenum Hill, Cynoxylon (Rafinesque) Small, Eukrania Rafinesque, Macrocarpium (Spach) Nakai, Swida Opiz, and Thelycrania (Dumortier) Fourreau]. Cornus is retained here as a coherent group, maintaining subgenera as more appropriate biological units for recognition of this variation.</discussion>
  <discussion>Some North American members of Cornus are susceptible to fungal pathogens that may cause severe species decline, such as Dogwood Anthracnose (Discula destructiva) in association with C. florida and C. nuttallii, or the less virulent but still destructive Cryptodiaporthe Canker (Cryptodiaporthe corni), which is restricted to C. alternifolia.</discussion>
  <references>
    <reference>Eyde, R. H. 1988. Comprehending Cornus: Puzzles and progress in the systematics of dogwoods. Bot. Rev. (Lancaster) 54: 233–351.</reference>
    <reference>Ferguson, I. K. 1966. Notes on the nomenclature of Cornus. J. Arnold Arbor. 47: 100–105.</reference>
    <reference>Ferguson, I. K. 1966b. The Cornaceae of the southeastern United States. J. Arnold Arbor. 47: 106–116.</reference>
    <reference>Murrell, Z. E. 1992. Systematics of the Genus Cornus (Cornaceae). Ph.D. dissertation. Duke University.</reference>
    <reference>Murrell, Z. E. 1993. Phylogenetic relationships in Cornus (Cornaceae). Syst. Bot. 18: 469–495.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts petaloid, subtending inflorescences.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennial herbs; inflorescences congested cymes; pedicels present.</description>
      <determination>1c. Cornus subg. Arctocrania</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Trees; inflorescences capitula; pedicels absent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Drupes within inflorescence fused into a syncarp.</description>
      <determination>1a. Cornus subg. Syncarpea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Drupes distinct.</description>
      <determination>1b. Cornus subg. Cynoxylon</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts not petaloid, subtending inflorescences or not.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences umbels, bracts well developed, subtending inflorescence and enclosing it over winter.</description>
      <determination>1d. Cornus subg. Cornus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences cymes, bracts minute, subtending primary and secondary inflorescence branches.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Branches and leaves alternate; stone apex with cavity.</description>
      <determination>1e. Cornus subg. Mesomora</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Branches and leaves usually opposite, rarely whorled, subopposite, or alternate at some nodes; stone apex rounded, pointed, or with slight dimple.</description>
      <determination>1f. Cornus subg. Thelycrania</determination>
    </key_statement>
  </key>
</bio:treatment>