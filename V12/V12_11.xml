<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="illustration_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">PHYLLANTHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHYLLANTHUS</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">caroliniensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">caroliniensis</taxon_name>
    <taxon_hierarchy>family phyllanthaceae;genus phyllanthus;species caroliniensis;subspecies caroliniensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101449</other_info_on_name>
  </taxon_identification>
  <number>4a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually with 5+ lateral branches, these often branched, glabrous.</text>
      <biological_entity id="o21618" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" notes="" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21619" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s0" upper_restricted="false" />
      </biological_entity>
      <relation from="o21618" id="r1774" name="with" negation="false" src="d0_s0" to="o21619" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades: both surfaces glabrous;</text>
      <biological_entity id="o21620" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure" />
      <biological_entity id="o21621" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vein reticulum clearly visible abaxially.</text>
      <biological_entity id="o21622" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure" />
      <biological_entity constraint="vein" id="o21623" name="reticulum" name_original="reticulum" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="clearly; abaxially" name="prominence" src="d0_s2" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cymules with 1 staminate and (1–) 2–3 (–5) pistillate flowers.</text>
      <biological_entity id="o21624" name="cymule" name_original="cymules" src="d0_s3" type="structure" />
      <biological_entity id="o21625" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s3" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o21624" id="r1775" name="with" negation="false" src="d0_s3" to="o21625" />
    </statement>
    <statement id="d0_s4">
      <text>Pistillate flowers: sepals linear-lanceolate or narrowly spatulate, (0.7–) 0.8–1 (–1.4) × 0.2–0.3 mm, apex acute;</text>
      <biological_entity id="o21626" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21627" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_length" src="d0_s4" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s4" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21628" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nectary cupular, unlobed, enclosing ovary 1/3–1/2 length at anthesis.</text>
      <biological_entity id="o21629" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21630" name="nectary" name_original="nectary" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cupular" value_original="cupular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" constraint="at anthesis" from="1/3" name="length" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o21631" name="ovary" name_original="ovary" src="d0_s5" type="structure" />
      <relation from="o21630" id="r1776" name="enclosing" negation="false" src="d0_s5" to="o21631" />
    </statement>
  </description>
  <discussion>Subspecies caroliniensis is found almost throughout the range of Phyllanthus caroliniensis. In Florida, it reaches its native southern limit in Hillsborough County [it was collected on Key West once in the late nineteenth century (Curtis 185, GH), where it presumably was an introduced waif]. It has also been found as a garden weed in San Diego, California (Rebman 7115, SD), and may become established in that state.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, moist areas such as stream banks, lake and pond margins, forest openings, depressions in grasslands, disturbed sites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="moist areas" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="depressions" constraint="in grasslands , disturbed sites" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., D.C., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Miss., Mo., N.J., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; Mexico; West Indies; Central America; South America; introduced in se Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in se Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>