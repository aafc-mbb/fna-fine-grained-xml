<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Zuccarini" date="1844" rank="genus">EUCNIDE</taxon_name>
    <taxon_name authority="Zuccarini" date="1844" rank="species">bartonioides</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">bartonioides</taxon_name>
    <taxon_hierarchy>family loasaceae;genus eucnide;species bartonioides;variety bartonioides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101796</other_info_on_name>
  </taxon_identification>
  <number>1a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, annual or perennial, moundlike to spindle-shaped (wider than tall).</text>
      <biological_entity id="o12920" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="moundlike" name="shape" src="d0_s0" to="spindle-shaped" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="moundlike" name="shape" src="d0_s0" to="spindle-shaped" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: petals connate 0.5–3 mm, [20–] 30–55 [–58] mm;</text>
      <biological_entity id="o12922" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o12923" name="petal" name_original="petals" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="58" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s1" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stamens (55–) 70–150+, filaments 30–60 mm. 2n = 42.</text>
      <biological_entity id="o12924" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o12925" name="stamen" name_original="stamens" src="d0_s2" type="structure">
        <character char_type="range_value" from="55" name="atypical_quantity" src="d0_s2" to="70" to_inclusive="false" />
        <character char_type="range_value" from="70" name="quantity" src="d0_s2" to="150" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12926" name="all" name_original="filaments" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s2" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12927" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety bartonioides is known in the flora area from Brewster, Edwards, Jeff Davis, Presidio, Terrell, Uvalde, Val Verde, and Webb counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and cliffs, arroyos, limestone and volcanic substrates.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="volcanic substrates" modifier="limestone and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Nuevo León, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>