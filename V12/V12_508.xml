<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Elliott" date="1824" rank="species">cordifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sketch. Bot. S. Carolina</publication_title>
      <place_in_publication>2: 656. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species cordifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101563</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Elliott) Small" date="unknown" rank="species">cordifolia</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species cordifolia</taxon_hierarchy>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Heartleaf sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o28431" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o28432" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o28431" id="r2338" name="with" negation="false" src="d0_s0" to="o28432" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, occasionally mat-forming, 10–43 cm, glabrous.</text>
      <biological_entity id="o28433" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="occasionally" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="43" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o28434" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules usually distinct, occasionally connate at base, filiform, 1–1.2 (–2.8) mm, usually glabrous, rarely pilose;</text>
      <biological_entity id="o28435" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o28436" is_modifier="false" modifier="occasionally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28436" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.4–1 mm, usually glabrous;</text>
      <biological_entity id="o28437" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to oblong, 4.4–12 × 2.6–7.6 mm, base asymmetric, cordate to rounded, margins entire, apex rounded to mucronulate, surfaces glabrous;</text>
      <biological_entity id="o28438" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="4.4" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s5" to="7.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28439" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o28440" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28441" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="mucronulate" />
      </biological_entity>
      <biological_entity id="o28442" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>only midvein conspicuous.</text>
      <biological_entity id="o28443" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal nodes;</text>
      <biological_entity id="o28444" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o28445" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28445" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.9–3 mm.</text>
      <biological_entity id="o28446" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1–1.3 × 1–1.3 mm, glabrous;</text>
      <biological_entity id="o28447" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellowish to pink, elliptic, 0.3–0.5 × 0.5–1 mm;</text>
      <biological_entity id="o28448" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s10" to="pink" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s10" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages whitish to pink, sometimes drying red, elliptic to ovoid, 1.1–1.5 × 1.2–1.9 mm, distal margin entire, retuse, or erose.</text>
      <biological_entity id="o28449" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" modifier="sometimes" name="condition" src="d0_s11" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="ovoid" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28450" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 5–40.</text>
      <biological_entity id="o28451" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o28452" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28453" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.5–0.8 mm, 2-fid nearly entire length.</text>
      <biological_entity id="o28454" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28455" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid, 2–3 mm diam., glabrous;</text>
      <biological_entity id="o28456" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.2–2.7 mm.</text>
      <biological_entity id="o28457" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds gray or tan with dark-brown mottling, ovoid, bluntly 3–4-angled in cross-section, 1.8–2.1 × 1.2–1.4 mm, smooth to rugose.</text>
      <biological_entity id="o28458" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="tan with dark-brown mottling" value_original="tan with dark-brown mottling" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character constraint="in cross-section" constraintid="o28459" is_modifier="false" modifier="bluntly" name="shape" src="d0_s17" value="3-4-angled" value_original="3-4-angled" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" notes="" src="d0_s17" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" notes="" src="d0_s17" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s17" to="rugose" />
      </biological_entity>
      <biological_entity id="o28459" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Euphorbia cordifolia is easily identified by its cordate to rounded leaf base and distinctive filiform stipules.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Xeric oak-pine scrub, pine-barrens, sand barrens, sandy stream banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="xeric oak-pine scrub" />
        <character name="habitat" value="pine-barrens" />
        <character name="habitat" value="sand barrens" />
        <character name="habitat" value="sandy stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., Okla., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>