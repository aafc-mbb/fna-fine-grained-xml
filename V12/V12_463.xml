<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Thunberg" date="1781" rank="genus">HOVENIA</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Gen. Pl.</publication_title>
      <place_in_publication>1: 7. 1781</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus hovenia</taxon_hierarchy>
    <other_info_on_name type="etymology">For David Hoven, 1724–1787, Dutch senator and botanical patron</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">115811</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Raisin tree</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [shrubs], unarmed;</text>
      <biological_entity id="o21148" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bud-scales present.</text>
      <biological_entity id="o21149" name="bud-scale" name_original="bud-scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, alternate;</text>
      <biological_entity id="o21150" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade not gland-dotted;</text>
    </statement>
    <statement id="d0_s4">
      <text>pinnately veined, basal pair of secondary-veins more prominent.</text>
      <biological_entity id="o21151" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character constraint="of secondary-veins" constraintid="o21152" is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o21152" name="secondary-vein" name_original="secondary-veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal or axillary, compound dichasia (appearing repeatedly dichotomously branched);</text>
      <biological_entity id="o21153" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o21154" name="dichasium" name_original="dichasia" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels becoming fleshy [not fleshy] in fruit.</text>
      <biological_entity id="o21155" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o21157" is_modifier="false" modifier="becoming" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21156" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o21157" is_modifier="false" modifier="becoming" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21157" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o21158" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o21159" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium hemispheric, 2–3 mm wide;</text>
      <biological_entity id="o21160" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, spreading, white, ovate-triangular to triangular, keeled adaxially;</text>
      <biological_entity id="o21161" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="ovate-triangular" name="shape" src="d0_s10" to="triangular" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, white or yellow-green, inrolled full length, elliptic to ovate, short-clawed;</text>
      <biological_entity id="o21162" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="length" src="d0_s11" value="full" value_original="full" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="short-clawed" value_original="short-clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary fleshy, filling hypanthium;</text>
      <biological_entity id="o21163" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21164" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure" />
      <relation from="o21163" id="r1737" name="filling" negation="false" src="d0_s12" to="o21164" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5;</text>
      <biological_entity id="o21165" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 1/2-inferior, 3-locular;</text>
      <biological_entity id="o21166" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2–3, connate basally.</text>
      <biological_entity id="o21167" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="3" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, appearing drupaceous, tardily dehiscent, exocarp sloughing off from leathery mesocarp prior to dehiscence.</text>
      <biological_entity constraint="fruits" id="o21168" name="capsule" name_original="capsules" src="d0_s16" type="structure" />
      <biological_entity id="o21169" name="exocarp" name_original="exocarp" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="true" modifier="tardily" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o21170" name="mesocarp" name_original="mesocarp" src="d0_s16" type="structure">
        <character is_modifier="true" name="texture" src="d0_s16" value="leathery" value_original="leathery" />
      </biological_entity>
      <relation from="o21168" id="r1738" name="appearing" negation="false" src="d0_s16" to="o21169" />
      <relation from="o21168" id="r1739" name="from" negation="false" src="d0_s16" to="o21170" />
    </statement>
  </description>
  <discussion>Species 7 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Asia; introduced also in South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Koller, G. L. and J. H. Alexander. 1979. The raisin tree—its use, hardiness and size. Arnoldia (Jamaica Plain) 39: 6–15.</reference>
  </references>
  
</bio:treatment>