<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Mark H. Mayfield</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">182</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Houstoun ex Miller" date="1754" rank="genus">BERNARDIA</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 1. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus bernardia</taxon_hierarchy>
    <other_info_on_name type="etymology">Probably for Bernard de Jussieu, 1699–1777, French botanist</other_info_on_name>
    <other_info_on_name type="fna_id">103847</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Myrtlecroton</other_name>
  <other_name type="common_name">oreja de raton</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [herbs or subshrubs], dioecious [monoecious];</text>
      <biological_entity id="o17552" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs stellate [unbranched or absent];</text>
      <biological_entity id="o17553" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex absent.</text>
      <biological_entity id="o17554" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, alternate, simple;</text>
      <biological_entity id="o17555" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, persistent or caducous;</text>
      <biological_entity id="o17556" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present [absent], glands absent;</text>
      <biological_entity id="o17557" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17558" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed, margins coarsely crenate to crenate-serrate [serrate or entire], laminar glands usually abaxial, proximal, crateriform, occasionally absent on some leaves [absent];</text>
      <biological_entity id="o17559" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o17560" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="coarsely crenate" name="shape" src="d0_s6" to="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o17562" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>venation pinnate (with strong secondary-veins ascending from base).</text>
      <biological_entity constraint="laminar" id="o17561" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s6" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s6" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crateriform" value_original="crateriform" />
        <character constraint="on leaves" constraintid="o17562" is_modifier="false" modifier="occasionally" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences unisexual, axillary, often on short, lateral shoots;</text>
      <biological_entity id="o17563" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o17564" name="shoot" name_original="shoots" src="d0_s8" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <relation from="o17563" id="r1447" modifier="often" name="on" negation="false" src="d0_s8" to="o17564" />
    </statement>
    <statement id="d0_s9">
      <text>staminate spicate thyrses, pistillate flowers solitary [terminal spikes];</text>
      <biological_entity id="o17565" name="thyrse" name_original="thyrses" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="spicate" value_original="spicate" />
      </biological_entity>
      <biological_entity id="o17566" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands subtending bracts 0.</text>
      <biological_entity id="o17567" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17568" name="bract" name_original="bracts" src="d0_s10" type="structure" />
      <relation from="o17567" id="r1448" name="subtending" negation="false" src="d0_s10" to="o17568" />
    </statement>
    <statement id="d0_s11">
      <text>Pedicels: staminate present, pistillate absent [present].</text>
      <biological_entity id="o17569" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: sepals 3 (–4), valvate, distinct;</text>
      <biological_entity id="o17570" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17571" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="4" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s12" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 0;</text>
      <biological_entity id="o17572" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17573" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectary intrastaminal, 1 to several glands;</text>
      <biological_entity id="o17574" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17575" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="intrastaminal" value_original="intrastaminal" />
        <character constraint="to glands" constraintid="o17576" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17576" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 3–15 (–20) [–50], ± straight in bud, distinct;</text>
      <biological_entity id="o17577" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17578" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="20" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="15" />
        <character constraint="in bud" constraintid="o17579" is_modifier="false" modifier="more or less" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o17579" name="bud" name_original="bud" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>pistillode absent.</text>
      <biological_entity id="o17580" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17581" name="pistillode" name_original="pistillode" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pistillate flowers: sepals 3–5, distinct;</text>
      <biological_entity id="o17582" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17583" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 0;</text>
      <biological_entity id="o17584" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17585" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>nectary absent [present];</text>
      <biological_entity id="o17586" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17587" name="nectary" name_original="nectary" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>pistil 2–3-carpellate;</text>
      <biological_entity id="o17588" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17589" name="pistil" name_original="pistil" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="2-3-carpellate" value_original="2-3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>styles 2–3, distinct, 2-fid, branches flattened, adaxial surface stigmatic.</text>
      <biological_entity id="o17590" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17591" name="style" name_original="styles" src="d0_s21" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s21" to="3" />
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s21" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o17592" name="branch" name_original="branches" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17593" name="surface" name_original="surface" src="d0_s21" type="structure">
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s21" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o17594" name="capsule" name_original="capsules" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>Seeds subglobose;</text>
      <biological_entity id="o17595" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>caruncle absent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 13.</text>
      <biological_entity id="o17596" name="caruncle" name_original="caruncle" src="d0_s24" type="structure">
        <character is_modifier="false" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o17597" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 70 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw, sc United States, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The species that occur in the flora area are distinct from most species of the genus in being shrubs with relatively small leaves and stellate vestiture; most Bernardia species are perennial herbs or subshrubs. The rounded shrubs native to the flora area grow well in cultivation and would make attractive native borders within their range.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces green, sparsely spreading stellate-pubescent, veins not prominent abaxially; pistils 2-carpellate.</description>
      <determination>3. Bernardia obovata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces grayish white, densely appressed and/or spreading stellate-pubescent, veins prominent abaxially; pistils 3-carpellate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stipules persistent, yellowish brown to black, bases thickened, with dark resinous exudate; stamens (3–)5–7.</description>
      <determination>1. Bernardia incana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stipules caducous, green to straw colored, bases not thickened, without dark resinous exudate; stamens (10–)12–15(–20).</description>
      <determination>2. Bernardia myricifolia</determination>
    </key_statement>
  </key>
</bio:treatment>