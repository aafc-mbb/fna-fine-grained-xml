<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann in A. Gray" date="1856" rank="species">humistrata</taxon_name>
    <place_of_publication>
      <publication_title>Manual ed.</publication_title>
      <place_in_publication>2, 386. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species humistrata</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101580</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Small" date="unknown" rank="species">humistrata</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species humistrata</taxon_hierarchy>
  </taxon_identification>
  <number>47.</number>
  <other_name type="common_name">Spreading sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o16233" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o16234" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o16233" id="r1352" name="with" negation="false" src="d0_s0" to="o16234" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to ascending, usually mat-forming and rooting at nodes, 5–45 cm, sparsely to moderately villous to pilose (densely on young growth).</text>
      <biological_entity id="o16235" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character constraint="at nodes" constraintid="o16236" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="45" to_unit="cm" />
        <character char_type="range_value" from="villous" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s1" to="pilose" />
      </biological_entity>
      <biological_entity id="o16236" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o16237" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules distinct, linear-subulate, often irregularly 2-lobed or 3-lobed, 1–1.3 mm, sparsely villous to pilose;</text>
      <biological_entity id="o16238" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" modifier="often irregularly" name="shape" src="d0_s3" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s3" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–1.5 mm, sparsely to moderately villous to pilose;</text>
      <biological_entity id="o16239" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="villous" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s4" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong-ovate to ovate-elliptic or oblongelliptic, 4–18 × 2.5–8 mm, base strongly asymmetric, one side angled and other rounded to auriculate, margin on longer side serrulate, on shorter side subentire, apex rounded or broadly acute, abaxial surface pale grayish green, sparsely lanulose, adaxial surface usually with irregular reddish streak along midvein, usually glabrate, rarely sparsely lanulose;</text>
      <biological_entity id="o16240" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="ovate-elliptic or oblongelliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="18" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16241" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o16242" name="side" name_original="side" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="angled" value_original="angled" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="auriculate" />
      </biological_entity>
      <biological_entity id="o16243" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity constraint="longer" id="o16244" name="side" name_original="side" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o16245" name="side" name_original="side" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o16246" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16247" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale grayish" value_original="pale grayish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="lanulose" value_original="lanulose" />
      </biological_entity>
      <biological_entity id="o16249" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <relation from="o16243" id="r1353" name="on" negation="false" src="d0_s5" to="o16244" />
      <relation from="o16243" id="r1354" name="on" negation="false" src="d0_s5" to="o16245" />
    </statement>
    <statement id="d0_s6">
      <text>palmately veined at base, pinnate distally.</text>
      <biological_entity constraint="adaxial" id="o16248" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character constraint="with" is_modifier="false" name="architecture_or_course" src="d0_s5" value="irregular" value_original="irregular" />
        <character constraint="along midvein" constraintid="o16249" is_modifier="false" name="coloration" src="d0_s5" value="reddish streak" value_original="reddish streak" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s5" value="lanulose" value_original="lanulose" />
        <character constraint="at base" constraintid="o16250" is_modifier="false" modifier="palmately" name="architecture" src="d0_s6" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o16250" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal nodes;</text>
      <biological_entity id="o16251" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o16252" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16252" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.1–0.6 (–2) mm.</text>
      <biological_entity id="o16253" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic, 0.8–1 × 0.6–0.8 mm, sparsely villous to pilose;</text>
      <biological_entity id="o16254" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s9" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s9" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, green to yellow-green (turning pink with age), usually ± unequal, narrowly oblong, 0.1–0.2 × 0.2–0.5 mm;</text>
      <biological_entity id="o16255" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellow-green" />
        <character is_modifier="false" modifier="usually more or less" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s10" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white to reddish tinged, lunate, ± irregular and variable in shape, 0.1–0.3 × 0.2–1.5 mm, distal margin crenulate.</text>
      <biological_entity id="o16256" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="reddish tinged" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lunate" value_original="lunate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_course" src="d0_s11" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="variable" value_original="variable" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s11" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16257" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 2–5.</text>
      <biological_entity id="o16258" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary short-sericeous;</text>
      <biological_entity id="o16259" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16260" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="short-sericeous" value_original="short-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.5–0.8 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o16261" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16262" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid, well exserted from involucre at maturity, 1.3–1.5 × 1.2–1.6 mm, sparsely to moderately short-sericeous;</text>
      <biological_entity id="o16263" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s15" value="short-sericeous" value_original="short-sericeous" />
      </biological_entity>
      <biological_entity id="o16264" name="involucre" name_original="involucre" src="d0_s15" type="structure" />
      <relation from="o16263" id="r1355" modifier="well" name="exserted from" negation="false" src="d0_s15" to="o16264" />
    </statement>
    <statement id="d0_s16">
      <text>columella 0.9–1.2 mm.</text>
      <biological_entity id="o16265" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds white to light-brown, oblong-ovoid, bluntly angular in cross-section, 0.8–1.2 × 0.5–0.9 mm, smooth or papillate.</text>
      <biological_entity id="o16266" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s17" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character constraint="in cross-section" constraintid="o16267" is_modifier="false" modifier="bluntly" name="arrangement_or_shape" src="d0_s17" value="angular" value_original="angular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" notes="" src="d0_s17" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s17" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o16267" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Euphorbia humistrata is distributed throughout the Mississippi River valley and along other major river systems in the central and eastern United States. There are scattered reports of this species as a waif or as introduced farther north and/or east (for example, Minnesota, New York, North Carolina, Ontario, South Dakota, and Wisconsin), but the authors have not been able to verify these occurrences. Euphorbia humistrata is similar to E. maculata and is often confused with that species in herbaria. It can be distinguished from E. maculata by its tendency to root at the stem nodes, its longer styles, and its seeds that lack low transverse ridges and that are more bluntly angled. When growing side-by-side, E. humistrata has an overall less congested appearance and its cyathia are not as numerous or crowded as those of E. maculata.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream and river banks, gravel bars, floodplains, pond edges, disturbed fields, railroads, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream" />
        <character name="habitat" value="river banks" />
        <character name="habitat" value="gravel bars" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="pond edges" />
        <character name="habitat" value="disturbed fields" />
        <character name="habitat" value="railroads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Kans., Ky., La., Miss., Mo., Ohio, Okla., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>