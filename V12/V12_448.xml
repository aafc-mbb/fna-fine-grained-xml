<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="illustration_page">535</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">TRACHYPHYTUM</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Torrey &amp; A. Gray" date="1840" rank="species">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 535. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section trachyphytum;species micrantha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101880</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bartonia</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1839" rank="species">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>343, plate 85. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bartonia;species micrantha</taxon_hierarchy>
  </taxon_identification>
  <number>75.</number>
  <other_name type="common_name">San Luis or chaparral blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants wandlike or candelabra-form, 10–80 cm.</text>
      <biological_entity id="o4576" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="wand-like" value_original="wandlike" />
        <character name="shape" src="d0_s0" value="candelabra-form" value_original="candelabra-form" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves persisting;</text>
      <biological_entity constraint="basal" id="o4577" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present or absent;</text>
      <biological_entity id="o4578" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to linear, margins irregularly deeply lobed to dentate.</text>
      <biological_entity id="o4579" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o4580" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="irregularly deeply lobed" name="shape" src="d0_s3" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: petiole present or absent (proximal leaves), absent (distal leaves);</text>
      <biological_entity constraint="cauline" id="o4581" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4582" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to linear (proximal leaves), orbiculate to lanceolate (distal leaves), to 18 cm, margins irregularly deeply lobed to dentate proximally, dentate or entire distally.</text>
      <biological_entity constraint="cauline" id="o4583" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4584" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear orbiculate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear orbiculate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4585" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly deeply" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="lobed to dentate" value_original="lobed to dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Bracts green, orbiculate to ovate, 3.4–6.6 × 2.5–5.9 mm, width 3/4 to ± equal length, often concealing capsule, margins sinuate or entire.</text>
      <biological_entity id="o4586" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="width" src="d0_s6" to="6.6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="5.9" to_unit="mm" />
        <character name="quantity" src="d0_s6" value="3/4" value_original="3/4" />
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o4587" name="capsule" name_original="capsule" src="d0_s6" type="structure" />
      <biological_entity id="o4588" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o4586" id="r415" name="often concealing" negation="false" src="d0_s6" to="o4587" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–3 mm;</text>
      <biological_entity id="o4589" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4590" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, 2-5 mm, apex acute;</text>
      <biological_entity id="o4591" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4592" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4593" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 10–20, 1.5–4 mm, filaments heteromorphic, 5 outermost elliptic, distally 2-lobed, inner filiform, unlobed;</text>
      <biological_entity id="o4594" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4595" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="20" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4596" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="heteromorphic" value_original="heteromorphic" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o4597" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4598" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 2–3 (–5) mm.</text>
      <biological_entity id="o4599" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4600" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules cylindric, 6–13 × 1.5–2.5 mm, axillary curved to 20° at maturity, usually inconspicuously, occasionally prominently, longitudinally ribbed.</text>
      <biological_entity id="o4601" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character constraint="at maturity" is_modifier="false" modifier="0-20°" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually inconspicuously; inconspicuously; occasionally prominently; prominently; longitudinally" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 4–10, in 1 row distal to mid fruit, dark-brown or tan, dark-mottled, triangular prisms, surface ±smooth to minutely tessellate under 10× magnification;</text>
      <biological_entity id="o4602" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-mottled" value_original="dark-mottled" />
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o4603" name="row" name_original="row" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4604" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o4605" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="under 10×magnification" from="less smooth" name="relief" src="d0_s12" to="minutely tessellate" />
      </biological_entity>
      <relation from="o4602" id="r416" name="in" negation="false" src="d0_s12" to="o4603" />
    </statement>
    <statement id="d0_s13">
      <text>recurved flap over hilum absent;</text>
      <biological_entity id="o4606" name="flap" name_original="flap" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o4607" name="hilum" name_original="hilum" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o4606" id="r417" name="over" negation="false" src="d0_s13" to="o4607" />
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cell outer periclinal wall flat.</text>
      <biological_entity constraint="seed-coat" id="o4608" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity constraint="outer" id="o4609" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="periclinal" value_original="periclinal" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4610" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia micrantha is easily distinguished from other species in sect. Trachyphytum by the presence of two lateral lobes on the filaments of the five outermost stamens. This characteristic is distinct from the filament lobes of some species in sect. Bicuspidaria, which occur on all or most stamens. Phylogenetic studies have found that M. micrantha is not closely related to species in sect. Bicuspidaria (L. Hufford et al. 2003; J. M. Brokaw and Hufford 2010).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, often recently-burned or disturbed chaparral or oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="recently-burned" modifier="often" />
        <character name="habitat" value="disturbed chaparral" />
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>