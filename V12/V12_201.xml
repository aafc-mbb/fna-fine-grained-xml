<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Hill" date="1756" rank="genus">RADIOLA</taxon_name>
    <place_of_publication>
      <publication_title>Brit. Herb.,</publication_title>
      <place_in_publication>227, plate 33 [upper center]. 1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus radiola</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin radiolus, little ray, alluding to rayed capsules</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">127902</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Allseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, glabrous.</text>
      <biological_entity id="o15874" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched throughout.</text>
      <biological_entity id="o15875" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite;</text>
      <biological_entity id="o15876" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands absent;</text>
      <biological_entity constraint="stipular" id="o15877" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to lanceolate, margins entire.</text>
      <biological_entity id="o15878" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o15879" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences dichasia.</text>
      <biological_entity constraint="inflorescences" id="o15880" name="dichasium" name_original="dichasia" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels not articulated.</text>
      <biological_entity id="o15881" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="articulated" value_original="articulated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals persistent, 4, connate at base, equal in size, margins entire, not glandular;</text>
      <biological_entity id="o15882" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15883" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character constraint="at base" constraintid="o15884" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o15884" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o15885" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 4, distinct, attached at rim of filament cup, white, appendages absent;</text>
      <biological_entity id="o15886" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15887" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character constraint="at rim" constraintid="o15888" is_modifier="false" name="fixation" src="d0_s8" value="attached" value_original="attached" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o15888" name="rim" name_original="rim" src="d0_s8" type="structure" />
      <biological_entity constraint="filament" id="o15889" name="cup" name_original="cup" src="d0_s8" type="structure" />
      <biological_entity id="o15890" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o15888" id="r1334" name="part_of" negation="false" src="d0_s8" to="o15889" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4;</text>
      <biological_entity id="o15891" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15892" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 0;</text>
      <biological_entity id="o15893" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15894" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistil 4-carpellate, ovary 4-locular;</text>
      <biological_entity id="o15895" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15896" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="4-carpellate" value_original="4-carpellate" />
      </biological_entity>
      <biological_entity id="o15897" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="4-locular" value_original="4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 4, distinct;</text>
      <biological_entity id="o15898" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15899" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas capitate, wider than styles.</text>
      <biological_entity id="o15900" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15901" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
        <character constraint="than styles" constraintid="o15902" is_modifier="false" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o15902" name="style" name_original="styles" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, dehiscing into 4 segments, each partially divided by an incomplete septum.</text>
      <biological_entity constraint="fruits" id="o15903" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character constraint="into segments" constraintid="o15904" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
        <character constraint="by septum" constraintid="o15905" is_modifier="false" modifier="partially" name="shape" notes="" src="d0_s14" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o15904" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o15905" name="septum" name_original="septum" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="incomplete" value_original="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 8, obovoid to ellipsoid, sometimes flattened on one side.</text>
      <biological_entity id="o15907" name="side" name_original="side" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9</text>
      <biological_entity id="o15906" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s15" to="ellipsoid" />
        <character constraint="on side" constraintid="o15907" is_modifier="false" modifier="sometimes" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="x" id="o15908" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Europe, sw Asia, Africa, Atlantic Islands (Macaronesia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>