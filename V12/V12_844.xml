<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="J. T. Howell" date="1939" rank="species">sonomensis</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 162. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species sonomensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101448</other_info_on_name>
  </taxon_identification>
  <number>40.</number>
  <other_name type="common_name">Sonoma ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–1 m, often moundlike.</text>
      <biological_entity id="o13207" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, not rooting at nodes;</text>
      <biological_entity id="o13208" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character constraint="at nodes" constraintid="o13209" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o13209" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets gray to grayish brown, rigid, strigillose, glabrescent.</text>
      <biological_entity id="o13210" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s2" to="grayish brown" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled;</text>
      <biological_entity id="o13211" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–1 mm;</text>
      <biological_entity id="o13212" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade cupped, widely obovate to suborbiculate, 5–12 × 2–10 mm, base cuneate, margins not revolute, wavy, spinose-dentate, teeth 2–4, apex widely notched;</text>
      <biological_entity id="o13213" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="widely obovate" name="shape" src="d0_s5" to="suborbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13214" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13215" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinose-dentate" value_original="spinose-dentate" />
      </biological_entity>
      <biological_entity id="o13216" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o13217" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s5" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial surface pale green or grayish green and glaucous, strigillose on veins, adaxial surface shiny green, glabrous.</text>
      <biological_entity constraint="abaxial" id="o13218" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character constraint="on veins" constraintid="o13219" is_modifier="false" name="pubescence" src="d0_s6" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o13219" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o13220" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s6" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary or terminal, 0.8–1.5 cm.</text>
      <biological_entity id="o13221" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s7" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals, petals, and nectary blue to lavender.</text>
      <biological_entity id="o13222" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13223" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o13224" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity id="o13225" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="lavender" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4–5 mm wide, usually not, sometimes weakly lobed;</text>
      <biological_entity id="o13226" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually not; not; sometimes weakly" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves smooth, horns subapical, minute to ± prominent, erect, intermediate ridges absent.</text>
      <biological_entity id="o13227" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o13228" name="horn" name_original="horns" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o13229" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="intermediate" value_original="intermediate" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13230" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus sonomensis is distinctive in having spinose-dentate, few-toothed leaves, and slender fruit horns two to three millimeters; it occurs at a few scattered localities in the mountains of Napa and Sonoma counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to rocky soils derived mostly from volcanic substrates, slopes, ridges, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="volcanic substrates" />
        <character name="habitat" value="sandy to rocky soils derived from volcanic substrates" modifier="mostly" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>