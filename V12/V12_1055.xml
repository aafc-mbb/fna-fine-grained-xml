<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="W. Knight" date="1968" rank="species">roderickii</taxon_name>
    <place_of_publication>
      <publication_title>Four Seasons</publication_title>
      <place_in_publication>2(4): 23. 1968</place_in_publication>
      <other_info_on_pub>(as rodericki)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species roderickii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101442</other_info_on_name>
  </taxon_identification>
  <number>37.</number>
  <other_name type="common_name">Pine Hill ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.1–0.5 m, moundlike.</text>
      <biological_entity id="o28699" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.5" to_unit="m" />
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate or spreading, arcuate, often rooting at distal nodes;</text>
      <biological_entity id="o28700" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course_or_shape" src="d0_s1" value="arcuate" value_original="arcuate" />
        <character constraint="at distal nodes" constraintid="o28701" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28701" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets brown to grayish brown, rigid, puberulent, glabrescent.</text>
      <biological_entity id="o28702" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="grayish brown" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves both fascicled and not on same plant, axillary short-shoots erect;</text>
      <biological_entity id="o28703" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="not on plant" constraintid="o28704" is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o28704" name="plant" name_original="plant" src="d0_s3" type="structure" />
      <biological_entity constraint="axillary" id="o28705" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–2 mm;</text>
      <biological_entity id="o28706" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade folded lengthwise abaxially, elliptic to oblanceolate, 4–12 × 2–6 mm, base obtuse to cuneate, margins not revolute, entire or denticulate near apex, teeth 3–5, apex obtuse, abaxial surface pale green, glabrate or sparsely strigillose between the veins, adaxial surface green, glabrate.</text>
      <biological_entity id="o28707" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="lengthwise abaxially; abaxially" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28708" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o28709" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="near apex" constraintid="o28710" is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o28710" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o28711" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o28712" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28713" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character constraint="between veins" constraintid="o28714" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o28714" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o28715" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, 0.6–1.4 cm.</text>
      <biological_entity id="o28716" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s6" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals and petals white to pale blue;</text>
      <biological_entity id="o28717" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28718" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pale blue" />
      </biological_entity>
      <biological_entity id="o28719" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pale blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary blue.</text>
      <biological_entity id="o28720" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28721" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4–5 mm wide, usually not, sometimes weakly lobed;</text>
      <biological_entity id="o28722" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually not; not; sometimes weakly" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves smooth or slightly rugulose, sometimes ridged, horns absent or weakly developed bulges, intermediate ridges absent.</text>
      <biological_entity id="o28723" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s10" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity id="o28724" name="horn" name_original="horns" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="weakly developed bulges , intermediate ridges" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus roderickii is restricted to a few localities in the foothills of the Sierra Nevada (El Dorado County). A close relationship to C. cuneatus var. cuneatus is supported by molecular data (T. M. Hardig et al. 2000b). The ability to root at remote, distal nodes was shown to enhance density and recovery, long after episodic establishment from seeds following fires (R. S. Boyd 2007).</discussion>
  <discussion>Ceanothus roderickii is in the Center for Plant Conservation's National Collection of Endangered Plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils derived from gabbro, chaparral, pine woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="gabbro" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="woodlands" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>