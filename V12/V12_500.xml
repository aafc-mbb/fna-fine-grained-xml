<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Trelease" date="1888" rank="species">parvifolius</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 1: 110. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species parvifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101394</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">integerrimus</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1876" rank="variety">parvifolius</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>1: 102. 1876</place_in_publication>
      <other_info_on_pub>[illegitimate name] based on C. integerrimus var. parviflorus S. Watson, Proc. Amer. Acad. Arts 10: 334. 1875</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus ceanothus;species integerrimus;variety parvifolius</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Little-leaf ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, deciduous, 1–2.5 m.</text>
      <biological_entity id="o29666" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect or ascending, not rooting at nodes;</text>
      <biological_entity id="o29667" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o29668" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o29668" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets green, not thorn-tipped, round in cross-section, flexible, tomentulose, glabrescent.</text>
      <biological_entity id="o29669" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="thorn-tipped" value_original="thorn-tipped" />
        <character constraint="in cross-section" constraintid="o29670" is_modifier="false" name="shape" src="d0_s2" value="round" value_original="round" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s2" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o29670" name="cross-section" name_original="cross-section" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled;</text>
      <biological_entity id="o29671" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1.5–5 mm;</text>
      <biological_entity id="o29672" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat, oblongelliptic to elliptic, 6–25 × 3–13 mm, base cuneate, margins usually entire, sometimes denticulate distally, teeth 3–5, apex acute to obtuse, abaxial surface pale green, usually glabrous, veins sometimes strigillose, adaxial surface green, ± shiny, glabrous;</text>
      <biological_entity id="o29673" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29674" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o29675" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes; distally" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o29676" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o29677" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29678" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29679" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pinnately veined or ± 3-veined from base.</text>
      <biological_entity constraint="adaxial" id="o29680" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s6" value="veined" value_original="veined" />
        <character constraint="from base" constraintid="o29681" is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o29681" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, racemelike, 3–8 cm.</text>
      <biological_entity id="o29682" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s7" value="racemelike" value_original="racemelike" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals and petals pale to deep blue;</text>
      <biological_entity id="o29683" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o29684" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="deep blue" />
      </biological_entity>
      <biological_entity id="o29685" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="deep blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary blue.</text>
      <biological_entity id="o29686" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29687" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 4–5 mm wide, usually not lobed, sometimes weakly lobed;</text>
      <biological_entity id="o29688" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves smooth, weakly viscid, usually not crested, sometimes weakly crested.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o29689" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="coating" src="d0_s11" value="viscid" value_original="viscid" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s11" value="crested" value_original="crested" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s11" value="crested" value_original="crested" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29690" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus parvifolius is restricted to the western slope of the Sierra Nevada from Plumas County south to Tulare County. Putative hybrids with C. cordulatus have been reported (H. McMinn 1944).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites and flats, conifer forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>