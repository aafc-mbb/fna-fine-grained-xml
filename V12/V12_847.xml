<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">32</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">ZYGOPHYLLACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ZYGOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 385. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 182. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zygophyllaceae;genus zygophyllum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek zygon, yoke, and phyllon, leaf, alluding to conjugate leaflets as in Z. fabago</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">135408</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Bean-caper</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs [shrubs], perennial.</text>
      <biological_entity id="o11916" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect, highly branched, terete, less than 1 m, fleshy [becoming woody basally], glabrous [hairy].</text>
      <biological_entity id="o11918" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite, 2-foliolate [even-pinnately compound or simple];</text>
      <biological_entity id="o11919" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-foliolate" value_original="2-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules deciduous [persistent], ± herbaceous [membranaceous], triangular, apex acute;</text>
      <biological_entity id="o11920" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o11921" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiolules absent;</text>
      <biological_entity id="o11922" name="petiolule" name_original="petiolules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 2 [–10], opposite, distinct, obovate, flat [terete], equal, base oblique, apex rounded [acute or obtuse], fleshy, surfaces glabrous [hairy].</text>
      <biological_entity id="o11923" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="10" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11924" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o11925" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o11926" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels in leaf-axils, reflexed downward [erect].</text>
      <biological_entity id="o11927" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o11928" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
      <relation from="o11927" id="r983" name="in" negation="false" src="d0_s6" to="o11928" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1–2, slightly irregular by twisting of petals [regular];</text>
      <biological_entity id="o11929" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_course" src="d0_s7" value="irregular" value_original="irregular" />
        <character constraint="of petals" constraintid="o11930" is_modifier="false" name="architecture" src="d0_s7" value="twisting" value_original="twisting" />
      </biological_entity>
      <biological_entity id="o11930" name="petal" name_original="petals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>sepals deciduous or persistent, [4–] 5, distinct, green, often unequal, margins often membranous, apex rounded [obtuse to acute], glabrous;</text>
      <biological_entity id="o11931" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o11932" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o11933" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals deciduous, [0 or 4–] 5, imbricate, ± erect, slightly twisted [not twisted to twisted], white or yellow [orange], base red-orange [same as rest of petal], obovate, base clawed, apex rounded;</text>
      <biological_entity id="o11934" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s9" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o11935" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o11936" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o11937" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectary annular;</text>
      <biological_entity id="o11938" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens [8–] 10, ± equal;</text>
      <biological_entity id="o11939" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s11" to="10" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments free, subulate, each with basal scale;</text>
      <biological_entity id="o11940" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11941" name="scale" name_original="scale" src="d0_s12" type="structure" />
      <relation from="o11940" id="r984" name="with" negation="false" src="d0_s12" to="o11941" />
    </statement>
    <statement id="d0_s13">
      <text>anthers ovate;</text>
      <biological_entity id="o11942" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary sessile, [3–] 5-lobed, [3–] 5-locular, glabrous;</text>
      <biological_entity id="o11943" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s14" value="[3-]5-lobed" value_original="[3-]5-lobed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="[3-]5-locular" value_original="[3-]5-locular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovules [2–] 10 per locule;</text>
      <biological_entity id="o11944" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s15" to="10" to_inclusive="false" />
        <character constraint="per locule" constraintid="o11945" name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o11945" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style persistent, not forming beak on fruit;</text>
      <biological_entity id="o11946" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11947" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <biological_entity id="o11948" name="fruit" name_original="fruit" src="d0_s16" type="structure" />
      <relation from="o11946" id="r985" name="forming" negation="true" src="d0_s16" to="o11947" />
      <relation from="o11946" id="r986" name="on" negation="true" src="d0_s16" to="o11948" />
    </statement>
    <statement id="d0_s17">
      <text>stigma minute.</text>
      <biological_entity id="o11949" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsules, oblong-cylindric [to globose], [4–] 5-angled [or winged], septicidally dehiscent [indehiscent].</text>
      <biological_entity constraint="fruits" id="o11950" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s18" value="[4-]5-angled" value_original="[4-]5-angled" />
        <character is_modifier="false" modifier="septicidally" name="dehiscence" src="d0_s18" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds [1–] 10 per locule, gray-brown, obovoid [ovoid].</text>
      <biological_entity id="o11951" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s19" to="10" to_inclusive="false" />
        <character constraint="per locule" constraintid="o11952" name="quantity" src="d0_s19" value="10" value_original="10" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s19" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity id="o11952" name="locule" name_original="locule" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Species ca. 70 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Eurasia, n, s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="n" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The common name of Zygophyllum results from the pickled flower buds of several species being used as substitutes for capers (Capparis spinosa Linnaeus, Capparaceae) in flavoring foods.</discussion>
  
</bio:treatment>