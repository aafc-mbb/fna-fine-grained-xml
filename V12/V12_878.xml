<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="mention_page">439</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">PHORADENDRON</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall ex Engelmann" date="1850" rank="species">villosum</taxon_name>
    <taxon_name authority="(Trelease) Wiens" date="1964" rank="subspecies">coryae</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>16: 45. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus phoradendron;species villosum;subspecies coryae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101691</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phoradendron</taxon_name>
    <taxon_name authority="Trelease" date="1916" rank="species">coryae</taxon_name>
    <place_of_publication>
      <publication_title>Phoradendron,</publication_title>
      <place_in_publication>43, plate 44. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus phoradendron;species coryae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">villosum</taxon_name>
    <taxon_name authority="(Trelease) B. L. Turner" date="unknown" rank="variety">coryae</taxon_name>
    <taxon_hierarchy>genus p.;species villosum;variety coryae</taxon_hierarchy>
  </taxon_identification>
  <number>7b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Hairs on young stems, leaf-blades, and inflorescences of two types, some in dense clusters, relatively longer, others uniformly distributed, relatively shorter.</text>
      <biological_entity id="o9385" name="hair" name_original="hairs" src="d0_s0" type="structure" />
      <biological_entity id="o9386" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o9387" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure" />
      <biological_entity id="o9388" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" notes="" src="d0_s0" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9389" name="type" name_original="types" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9390" name="other" name_original="others" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
        <character is_modifier="true" modifier="relatively" name="length_or_size" src="d0_s0" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="uniformly" name="arrangement" src="d0_s0" value="distributed" value_original="distributed" />
      </biological_entity>
      <relation from="o9385" id="r774" name="on" negation="false" src="d0_s0" to="o9386" />
      <relation from="o9388" id="r775" name="consist_of" negation="false" src="d0_s0" to="o9389" />
      <relation from="o9388" id="r776" name="in" negation="false" src="d0_s0" to="o9390" />
    </statement>
    <statement id="d0_s1">
      <text>Staminate inflorescences: internodes each (14–) 25 (–44) -flowered.</text>
      <biological_entity id="o9391" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9392" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="(14-)25(-44)-flowered" value_original="(14-)25(-44)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pistillate inflorescences: internodes each (6–) 8 (–12) -flowered.</text>
      <biological_entity id="o9393" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9394" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="(6-)8(-12)-flowered" value_original="(6-)8(-12)-flowered" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, subsp. coryae is found in Arizona south of the Mogollon Rim, through central New Mexico to the Chisos and Davis mountains of western Texas. Its principal hosts are Quercus species.</discussion>
  <discussion>The molecular study by V. E. T. M. Ashworth (2000b) showed that Phoradendron villosum is paraphyletic because P. scaberrimum Trelease forms a clade with subsp. coryae. All three of these taxa parasitize Quercus and a shared indel supported their phylogenetic relationship.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>