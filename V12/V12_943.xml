<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">541</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">TRACHYPHYTUM</taxon_name>
    <taxon_name authority="H. J. Thompson &amp; J. E. Roberts" date="1971" rank="species">ravenii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>21: 285. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section trachyphytum;species ravenii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101888</other_info_on_name>
  </taxon_identification>
  <number>83.</number>
  <other_name type="common_name">Raven’s blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants candelabra-form, (5–) 20–45 cm.</text>
      <biological_entity id="o30092" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves persisting;</text>
      <biological_entity constraint="basal" id="o30093" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present or absent;</text>
      <biological_entity id="o30094" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear-lanceolate, margins deeply to shallowly lobed.</text>
      <biological_entity id="o30095" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o30096" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply to shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: petiole absent;</text>
      <biological_entity constraint="cauline" id="o30097" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30098" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovatelanceolate to lanceolate, to 18 cm, margins deeply lobed to dentate.</text>
      <biological_entity constraint="cauline" id="o30099" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o30100" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30101" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="deeply lobed" name="shape" src="d0_s5" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Bracts green with prominent white base usually conspicuously extending outwards from midvein, ovate, 5.6–8.4 × 1.8–3.9 mm, width 1/5–2/3 length, not concealing capsule, margins 3–5-lobed.</text>
      <biological_entity id="o30102" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="with base" constraintid="o30103" is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="from midvein" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5.6" from_unit="mm" name="length" src="d0_s6" to="8.4" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="3.9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1/(1/5-2/3)" value_original="1/(1/5-2/3)" />
      </biological_entity>
      <biological_entity id="o30103" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o30104" name="capsule" name_original="capsule" src="d0_s6" type="structure" />
      <biological_entity id="o30105" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <relation from="o30102" id="r2465" name="concealing" negation="true" src="d0_s6" to="o30104" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 2–6 mm;</text>
      <biological_entity id="o30106" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o30107" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals orange proximally, yellow distally, 5–11 (–13) mm, apex retuse;</text>
      <biological_entity id="o30108" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o30109" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30110" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20+, 3–7 mm, filaments monomorphic, filiform, unlobed;</text>
      <biological_entity id="o30111" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o30112" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30113" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3.5–8 mm.</text>
      <biological_entity id="o30114" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o30115" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules clavate, 8–23 × 2–3 mm, axillary curved to 45° at maturity, usually inconspicuously longitudinally ribbed.</text>
      <biological_entity id="o30116" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="23" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character constraint="at maturity" is_modifier="false" modifier="0-45°" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually inconspicuously longitudinally" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 15–30, in 2+ rows distal to mid fruit, tan, dark-mottled, usually irregularly polygonal, occasionally triangular prisms proximal to mid fruit, surface tuberculate under 10× magnification;</text>
      <biological_entity id="o30117" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="30" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-mottled" value_original="dark-mottled" />
        <character is_modifier="false" modifier="usually irregularly" name="shape" src="d0_s12" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o30118" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o30119" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o30120" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o30121" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character constraint="under 10×magnification" is_modifier="false" name="relief" src="d0_s12" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <relation from="o30117" id="r2466" name="in" negation="false" src="d0_s12" to="o30118" />
    </statement>
    <statement id="d0_s13">
      <text>recurved flap over hilum absent;</text>
      <biological_entity id="o30122" name="flap" name_original="flap" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o30123" name="hilum" name_original="hilum" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o30122" id="r2467" name="over" negation="false" src="d0_s13" to="o30123" />
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cell outer periclinal wall domed, domes on seed edges more than or equal to 1/2 as tall as wide at maturity.</text>
      <biological_entity constraint="seed-coat" id="o30124" name="cell" name_original="cell" src="d0_s14" type="structure" />
      <biological_entity constraint="outer" id="o30125" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="periclinal" value_original="periclinal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="domed" value_original="domed" />
      </biological_entity>
      <biological_entity constraint="seed" id="o30127" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more than" name="variability" src="d0_s14" value="equal" value_original="equal" />
      </biological_entity>
      <relation from="o30126" id="r2468" name="on" negation="false" src="d0_s14" to="o30127" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 36.</text>
      <biological_entity id="o30126" name="dome" name_original="domes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" modifier="more than" name="quantity" src="d0_s14" to="1/2" />
        <character is_modifier="false" modifier="more than" name="width" src="d0_s14" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="more than" name="life_cycle" src="d0_s14" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30128" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia ravenii is narrowly distributed, with most populations limited to desert foothills on the northern edge of the San Gabriel Mountains in northeastern Los Angeles County. Populations of M. ravenii with relatively large flowers with yellow petals are often found growing under desert shrubs in mixed populations with M. veatchiana, which has relatively small flowers with orange petals in this area. A few tetraploid populations from western Riverside County have also been called M. ravenii (J. E. Zavortink 1966); further work is needed to confirm that these represent the same species. See 72. M. gracilenta for discussion of similarities it shares with M. ravenii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy desert foothills, roadsides, desert scrub, Joshua-tree woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy desert foothills" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>