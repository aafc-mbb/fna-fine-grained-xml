<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>J. Arturo De-Nova</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">160</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">ADELIA</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1285, 1298. 1759</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus adelia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a-, not, and delos, evident, alluding to small, obscure flowers</other_info_on_name>
    <other_info_on_name type="fna_id">100503</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Wild lime</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [trees], unarmed or branchlets sometimes stiff and thorn-tipped, dioecious [monoecious], hairs unbranched;</text>
      <biological_entity id="o23705" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o23706" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="thorn-tipped" value_original="thorn-tipped" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
      </biological_entity>
      <biological_entity id="o23707" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>latex absent.</text>
      <biological_entity id="o23708" name="latex" name_original="latex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, fascicled on short-shoots [alternate], simple;</text>
      <biological_entity id="o23709" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="on short-shoots" constraintid="o23710" is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o23710" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipules present, deciduous;</text>
      <biological_entity id="o23711" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present, glands absent;</text>
      <biological_entity id="o23712" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23713" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade unlobed, margins entire [crenate], laminar glands absent;</text>
      <biological_entity id="o23714" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o23715" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation palmate at base and pinnate distally [pinnate].</text>
      <biological_entity constraint="laminar" id="o23716" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character constraint="at base" constraintid="o23717" is_modifier="false" name="architecture" src="d0_s6" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity id="o23717" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences unisexual [bisexual], axillary, fascicles [racemes] or flowers solitary;</text>
      <biological_entity id="o23718" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o23719" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts subtending pistillate flowers minute, not enlarging in fruit;</text>
      <biological_entity id="o23720" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity constraint="subtending" id="o23721" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="size" src="d0_s8" value="minute" value_original="minute" />
        <character constraint="in fruit" constraintid="o23722" is_modifier="false" modifier="not" name="size" src="d0_s8" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o23722" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>glands subtending each bract 0.</text>
      <biological_entity id="o23723" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o23724" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels present.</text>
      <biological_entity id="o23725" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals [4–] 5, not petaloid, 2 [–5] mm, valvate, distinct;</text>
      <biological_entity id="o23726" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23727" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o23728" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23729" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary extrastaminal, annular [5 glands], adnate to calyx;</text>
      <biological_entity id="o23730" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23731" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="extrastaminal" value_original="extrastaminal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="annular" value_original="annular" />
        <character constraint="to calyx" constraintid="o23732" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o23732" name="calyx" name_original="calyx" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens [6–-] 14–17 [–30], connate basally [distinct];</text>
      <biological_entity id="o23733" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23734" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s14" to="14" to_inclusive="false" />
        <character char_type="range_value" from="17" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="30" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s14" to="17" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistillode present [absent].</text>
      <biological_entity id="o23735" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23736" name="pistillode" name_original="pistillode" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: sepals 5 (–6) [–7], distinct;</text>
      <biological_entity id="o23737" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23738" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="6" />
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 0;</text>
      <biological_entity id="o23739" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23740" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>nectary annular;</text>
      <biological_entity id="o23741" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23742" name="nectary" name_original="nectary" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pistil (2–) 3 (–4) –carpellate;</text>
      <biological_entity id="o23743" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23744" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="(2-)3(-4)-carpellate" value_original="(2-)3(-4)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles (2–) 3 (–4), distinct [connate basally], deeply multifid.</text>
      <biological_entity id="o23745" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23746" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s20" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="4" />
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s20" value="multifid" value_original="multifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o23747" name="capsule" name_original="capsules" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds subglobose;</text>
      <biological_entity id="o23748" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>caruncle absent.</text>
      <biological_entity id="o23749" name="caruncle" name_original="caruncle" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 9 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., Mexico, West Indies, Central America, South America; tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phylogenetic analyses of morphological and molecular data support Adelia as a monophyletic group sister to the Caribbean genera Lasiocroton Grisebach and Leucocroton Grisebach (De-Nova and V. Sosa 2007). Three principal lineages were recognized in Adelia: the first includes only A. cinerea (Wiggins &amp; Rollins) A. Cervantes, V. W. Steinmann &amp; Flores Olvera, a species endemic to Mexico; the second includes four Mexican species (including A. vaseyi); and the third comprises the remaining four species from the West Indies, Central America, and South America.</discussion>
  <references>
    <reference>De-Nova, J. A. and V. Sosa. 2007. Phylogenetic relationships and generic delimitation in Adelia (Euphorbiaceae s.s.) inferred from nuclear, chloroplast, and morphological data. Taxon 56: 1027–-1036.</reference>
    <reference>De-Nova, J. A., V. Sosa, and V. W. Steinmann. 2007. A synopsis of Adelia (Euphorbiaceae s.s.). Syst. Bot. 32: 583–595.</reference>
  </references>
  
</bio:treatment>