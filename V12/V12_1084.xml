<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
    <other_info_on_meta type="illustration_page">131</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Jacquin" date="1760" rank="genus">SCHAEFFERIA</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">cuneifolia</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 35. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus schaefferia;species cuneifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101487</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schaefferia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cuneifolia</taxon_name>
    <taxon_name authority="Lundell" date="unknown" rank="variety">pedicellata</taxon_name>
    <taxon_hierarchy>genus schaefferia;species cuneifolia;variety pedicellata</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Desert yaupon</other_name>
  <other_name type="common_name">capul</other_name>
  <other_name type="common_name">panalero</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 1–2 m.</text>
      <biological_entity id="o18913" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs ± spinescent;</text>
      <biological_entity id="o18914" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o18915" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="spinescent" value_original="spinescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>short-shoots present.</text>
      <biological_entity id="o18916" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18917" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate or fascicled;</text>
      <biological_entity id="o18918" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o18919" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade cuneate-obovate to oblanceolate, 5–23 × 2–15 mm, base cuneate, apex broadly obtuse to shallowly emarginate.</text>
      <biological_entity id="o18920" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate-obovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="23" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18921" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o18922" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly obtuse" name="shape" src="d0_s5" to="shallowly emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels (fruiting) 0–2 mm.</text>
      <biological_entity id="o18923" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 0.5 mm;</text>
      <biological_entity id="o18924" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18925" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblong, 3 mm.</text>
      <biological_entity id="o18926" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18927" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Drupes orange to bright red, 3–5 mm.</text>
      <biological_entity id="o18928" name="drupe" name_original="drupes" src="d0_s9" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s9" to="bright red" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds yellow.</text>
      <biological_entity id="o18929" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Schaefferia cuneifolia occurs in southern and western Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides, breaks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="breaks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Baja California, Baja California Sur, Coahuila, Nuevo León, San Luis Potosí, Sonora, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>