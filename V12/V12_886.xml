<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">ELATINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ELATINE</taxon_name>
    <taxon_name authority="(Nuttall) Fischer &amp; C. A. Meyer" date="1835" rank="species">minima</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>10: 73. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family elatinaceae;genus elatine;species minima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101683</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crypta</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="species">minima</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 117, plate 6, fig. 1. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus crypta;species minima</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Small waterwort</other_name>
  <other_name type="common_name">élatine naine</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, submersed or emersed on wet substrates, 0.2–3 (–10) cm.</text>
      <biological_entity id="o17064" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character constraint="on substrates" constraintid="o17065" is_modifier="false" name="location" src="d0_s0" value="emersed" value_original="emersed" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="3" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o17065" name="substrate" name_original="substrates" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="wet" value_original="wet" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, usually unbranched, sometimes 1–2 branched.</text>
      <biological_entity id="o17066" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1" modifier="sometimes" name="quantity" src="d0_s1" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green;</text>
      <biological_entity id="o17067" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate, 0.2 mm, margins entire, apex rounded;</text>
      <biological_entity id="o17068" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o17069" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17070" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–1 mm;</text>
      <biological_entity id="o17071" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic to oblanceolate or spatulate, 0.3–5 (–11) × 0.7–1.8 (–4) mm, base rounded to truncate, apex obtuse-acute.</text>
      <biological_entity id="o17072" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="oblanceolate or spatulate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17073" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="truncate" />
      </biological_entity>
      <biological_entity id="o17074" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse-acute" value_original="obtuse-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0–0.2 mm, erect.</text>
      <biological_entity id="o17075" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers cleistogamous when submersed;</text>
      <biological_entity id="o17076" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="when submersed" name="reproduction" src="d0_s7" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 2, equal, ovate to oblong, 0.5 × 0.3 mm;</text>
      <biological_entity id="o17077" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong" />
        <character name="length" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
        <character name="width" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 2, reddish, long-elliptic, elliptic, or ovate, 1–1.5 × 0.5 mm;</text>
      <biological_entity id="o17078" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="long-elliptic" value_original="long-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.5" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 2;</text>
      <biological_entity id="o17079" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 2.</text>
      <biological_entity id="o17080" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules oblong-ovoid, 2-locular, 0.9–1.5 mm diam.</text>
      <biological_entity id="o17081" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="2-locular" value_original="2-locular" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="diameter" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 4–9 per locule, thickly cylindric to barrel-shaped, straight, 0.4–0.5 (–0.7) × 0.2 (–0.3) mm;</text>
      <biological_entity id="o17082" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o17083" from="4" name="quantity" src="d0_s13" to="9" />
        <character char_type="range_value" from="thickly cylindric" name="shape" notes="" src="d0_s13" to="barrel-shaped" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s13" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="0.3" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o17083" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pits elliptic, length 1.2–1.6 times width, in 8–9 rows, (11–) 15–20 per row.</text>
      <biological_entity id="o17084" name="pit" name_original="pits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="1.2-1.6" value_original="1.2-1.6" />
      </biological_entity>
      <biological_entity id="o17085" name="row" name_original="rows" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s14" to="9" />
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s14" to="15" to_inclusive="false" />
        <character char_type="range_value" constraint="per row" constraintid="o17086" from="15" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
      <biological_entity id="o17086" name="row" name_original="row" src="d0_s14" type="structure" />
      <relation from="o17084" id="r1416" name="in" negation="false" src="d0_s14" to="o17085" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shores (sometimes intertidal) or in water to 2 m deep, sandy substrates mixed with organic debris, abandoned sunfish nests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" />
        <character name="habitat" value="water" modifier="sometimes intertidal" />
        <character name="habitat" value="sandy substrates" modifier="deep" />
        <character name="habitat" value="organic debris" />
        <character name="habitat" value="sunfish nests" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask.; Conn., Del., Ill., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Pa., R.I., S.C., Tenn., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>