<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Geoffrey A. Levin</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">PHYLLANTHACEAE</taxon_name>
    <taxon_hierarchy>family phyllanthaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101710</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Leafflower Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, shrubs, or trees, perennial or annual, deciduous or evergreen, monoecious or dioecious.</text>
      <biological_entity id="o14111" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate [rarely opposite], simple (pinnately compound in Bischofia);</text>
      <biological_entity id="o14114" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules present [rarely absent];</text>
      <biological_entity id="o14115" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole usually present, sometimes absent;</text>
      <biological_entity id="o14116" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire or crenate-serrate;</text>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o14117" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences unisexual or bisexual, axillary [rarely supra-axillary or terminal], racemelike or paniclelike [spikelike] thyrses, cymes, fascicles, or glomes, or flowers solitary.</text>
      <biological_entity id="o14118" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
        <character is_modifier="false" name="shape" src="d0_s6" value="paniclelike" value_original="paniclelike" />
      </biological_entity>
      <biological_entity id="o14119" name="thyrse" name_original="thyrses" src="d0_s6" type="structure" />
      <biological_entity id="o14120" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o14121" name="glome" name_original="glomes" src="d0_s6" type="structure" />
      <biological_entity id="o14122" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual;</text>
      <biological_entity id="o14123" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth hypogynous;</text>
      <biological_entity id="o14124" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium absent;</text>
      <biological_entity id="o14125" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4–6, distinct or connate basally to most of length;</text>
      <biological_entity id="o14126" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="false" name="length" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="length" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0 or [4–] 5 [–6], distinct;</text>
      <biological_entity id="o14127" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary present or absent;</text>
      <biological_entity id="o14128" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2–5 [–50], distinct or connate, free;</text>
      <biological_entity id="o14129" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="50" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers dehiscing by longitudinal slits;</text>
      <biological_entity id="o14130" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="by slits" constraintid="o14131" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o14131" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistil 1, [2–] 3–10 [–15] -carpellate, ovary superior, [2–] 3–10 [–15] -locular, placentation axile;</text>
      <biological_entity id="o14132" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="[2-]3-10[-15]-carpellate" value_original="[2-]3-10[-15]-carpellate" />
      </biological_entity>
      <biological_entity id="o14133" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="[2-]3-10[-15]-locular" value_original="[2-]3-10[-15]-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 2 per locule, anatropous or hemitropous;</text>
      <biological_entity id="o14134" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character constraint="per locule" constraintid="o14135" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s16" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="hemitropous" value_original="hemitropous" />
      </biological_entity>
      <biological_entity id="o14135" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles [2–] 3–10 [–15], distinct or connate, unbranched or 2-fid;</text>
      <biological_entity id="o14136" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s17" to="3" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="15" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas [2–] 3–10 [–15] (as many as style divisions).</text>
      <biological_entity id="o14137" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s18" to="3" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="15" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s18" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits usually capsules, dehiscence septicidal, (usually schizocarpic with cocci separating from persistent columella, coccus usually dehiscent loculicidally), sometimes berries or drupes.</text>
      <biological_entity id="o14138" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s19" value="septicidal" value_original="septicidal" />
      </biological_entity>
      <biological_entity id="o14139" name="capsule" name_original="capsules" src="d0_s19" type="structure" />
      <biological_entity id="o14140" name="berry" name_original="berries" src="d0_s19" type="structure" />
      <biological_entity id="o14141" name="drupe" name_original="drupes" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1–2 per locule.</text>
      <biological_entity id="o14142" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o14143" from="1" name="quantity" src="d0_s20" to="2" />
      </biological_entity>
      <biological_entity id="o14143" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Genera ca. 60, species ca. 2000 (7 genera, 23 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Indian Ocean Islands, Pacific Islands, Australia; introduced in Bermuda, Atlantic Islands (Macaronesia); primarily tropical and warm temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="in Bermuda" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="introduced" />
        <character name="distribution" value="primarily tropical and warm temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The genera that make up Phyllanthaceae traditionally have been treated as Euphorbiaceae subfam. Phyllanthoideae Beilschmied. Molecular data (Angiosperm Phylogeny Group 2003; K. Wurdack et al. 2004; C. C. Davis et al. 2005; Wurdack and Davis 2009; Z. Xi et al. 2012) strongly support its monophyly and show that it is sister to Picrodendraceae (formerly treated as Euphorbiaceae subfam. Oldfieldioideae Eg. Köhler &amp; G. L. Webster), both of which are more distant from Euphorbiaceae in the narrow sense. Drypetes, the other genus in the flora area often included in Phyllanthoideae, belongs in Putranjivaceae (for example, see Wurdack and Davis).</discussion>
  <discussion>Breynia, Glochidion, and most Phyllanthus species exhibit phyllanthoid branching (G. L. Webster 1956–1958), in which the main stems are orthotropic, indeterminate, persistent, and (beyond the first few seedling nodes) bear only scalelike leaves, while the ultimate branches are plagiotropic, of limited growth, deciduous (in woody species), and usually bear well-developed leaves. Most species with phyllanthoid branching produce flowers only on the ultimate branchlets. Because these branchlets often fall as a unit and resemble pinnate leaves, the flowers superficially appear to be borne on the leaves. In some species of Phyllanthus, the ultimate branches are essentially leafless and flattened into strikingly leaflike cladodes; their homology with branches is clearly demonstrated by the cymules of flowers along the margins.</discussion>
  <discussion>Phylogenetic studies using DNA sequence data show that Breynia, Glochidion, and Sauropus Blume (also with phyllanthoid branching; not present in the flora area) are each monophyletic and that all three are derived from within Phyllanthus as usually treated, making that genus paraphyletic (K. Wurdack et al. 2004; H. Kathriarachchi et al. 2005, 2006; P. Hoffmann et al. 2006). The number of sequenced species currently is too few, and the taxonomic and nomenclatural problems too numerous, to produce a robust phylogenetic classification of this alliance of about 1200 species (Kathriarachchi et al. 2006). Thus in this treatment, Breynia, Glochidion, and Phyllanthus are maintained in their traditional senses. Reverchonia, which lacks phyllanthoid branching, is also derived from within Phyllanthus (Kathriarachchi et al. 2006) and here its single species is treated as P. warnockii.</discussion>
  <discussion>Andrachne telephioides Linnaeus, native from southern Europe and northern Africa east to India, was collected in 1880 on a ballast dump in New Jersey. In the key below, this prostrate to procumbent perennial herb would key with the Phyllanthus species that lack phyllanthoid branching. Andrachne telephioides differs from Phyllanthus by having petals (minute in the pistillate flowers), pistillodes in the staminate flowers, and styles 2-fid to the base (versus 2-fid only distally).</discussion>
  <references>
    <reference>Hoffmann, P., H. Kathriarachchi, and K. Wurdack. 2006. A phylogenetic classification of Phyllanthaceae (Malpighiales; Euphorbiaceae sensu lato). Kew Bull. 61: 37–53.</reference>
    <reference>Kathriarachchi, H. et al. 2005. Molecular phylogenetics of Phyllanthaceae inferred from five genes (plastid atpB, matK, 3'ndhF, rbcL, and nuclear PHYC). Molec. Phylogen. Evol. 36: 112–134.</reference>
    <reference>Levin, G. A. 1986b. Systematic foliar morphology of Phyllanthoideae (Euphorbiaceae). III. Cladistic analysis. Syst. Bot. 11: 515–530.</reference>
    <reference>Wurdack, K. et al. 2004. Molecular phylogenetic analysis of Phyllanthaceae (Phyllanthoideae pro parte, Euphorbiaceae sensu lato) using plastid rbcL sequences. Amer. J. Bot. 91: 1882–1900.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves pinnately compound, blade margins crenate-serrate; inflorescences racemose to paniculate thyrses.</description>
      <determination>1 Bischofia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves simple, blade margins entire; inflorescences glomes, fascicles, or cymules, or flowers solitary.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branching phyllanthoid; leaves on main stems scalelike, those on ultimate branchlets usually well developed, rarely scalelike.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistils 5–10-carpellate; styles unbranched; sepals distinct; anther connectives extending beyond anthers as deltoid appendage.</description>
      <determination>7 Glochidion</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistils 3(–4)-carpellate; styles 2-fid; sepals connate basally or most of length; anther connectives not extending beyond anthers.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Staminate sepals connate basally; nectary present; seed coat dry.</description>
      <determination>5 Phyllanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Staminate sepals connate most of length; nectary absent; seed coat fleshy.</description>
      <determination>6 Breynia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branching not phyllanthoid; leaves all well developed.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Herbs.</description>
      <determination>5 Phyllanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Shrubs or trees.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Shrubs 0.5–2 dm; leaf blades 1.5–3.5(–5) × 0.7–1.5 mm; sepals connate basally; pistillodes absent in staminate flowers.</description>
      <determination>5 Phyllanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Shrubs or trees (2–)4–60(–80) dm; leaf blades 4–50(–80) × 3–30(–45) mm; sepals distinct; pistillodes present in staminate flowers.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves deciduous; fruits berries; petals absent; staminate nectary 5 glands.</description>
      <determination>4 Flueggea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves persistent; fruits capsules; petals present; staminate nectary annular.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Staminate pedicels present; staminate flowers 1–2(–4) per fascicle; pistillate petals 0.4–0.8 mm; shrubs (2–)4–10 dm.</description>
      <determination>2 Phyllanthopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Staminate pedicels rudimentary; staminate flowers 6–25 per glome; pistillate petals 1–2 mm; shrubs or trees (5–)20–40(–80) dm.</description>
      <determination>3 Heterosavia</determination>
    </key_statement>
  </key>
</bio:treatment>