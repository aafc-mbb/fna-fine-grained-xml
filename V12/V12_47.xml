<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="J. T. Howell" date="1939" rank="species">confusus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 160. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species confusus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101450</other_info_on_name>
  </taxon_identification>
  <number>42.</number>
  <other_name type="common_name">Rincon Ridge ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.1–0.6 m, matlike to moundlike.</text>
      <biological_entity id="o4611" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, spreading, or weakly ascending, often rooting at proximal nodes;</text>
      <biological_entity id="o4612" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at proximal nodes" constraintid="o4613" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4613" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets brown to reddish-brown, ± rigid, glabrous or sparsely puberulent.</text>
      <biological_entity id="o4614" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="reddish-brown" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled, not crowded, shorter than internodes;</text>
      <biological_entity id="o4615" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character constraint="than internodes" constraintid="o4616" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4616" name="internode" name_original="internodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–2 mm;</text>
      <biological_entity id="o4617" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat to ± cupped, elliptic to ± oblong or obovate, 10–20 × 5–14 mm, base obtuse to cuneate, margins thick or slightly revolute, slightly wavy, sharply dentate to spinose-dentate, teeth 3–9, apex acute or retuse, with an apical tooth, abaxial surface grayish green, strigillose on veins, adaxial surface green, dull, glabrous.</text>
      <biological_entity id="o4618" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="elliptic" modifier="more or less" name="shape" src="d0_s5" to="more or less oblong or obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4619" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o4620" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="dentate to spinose-dentate" value_original="dentate to spinose-dentate" />
      </biological_entity>
      <biological_entity id="o4621" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o4622" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4623" name="tooth" name_original="tooth" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o4624" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
        <character constraint="on veins" constraintid="o4625" is_modifier="false" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o4625" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o4626" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4622" id="r418" name="with" negation="false" src="d0_s5" to="o4623" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, 1.5–3 cm.</text>
      <biological_entity id="o4627" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals, petals, and nectary blue, lavender, or purple.</text>
      <biological_entity id="o4628" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4629" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o4630" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o4631" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 4–6 mm wide, lobed;</text>
      <biological_entity id="o4632" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves smooth, crested, horns subapical, prominent, erect, intermediate ridges weakly developed.</text>
      <biological_entity id="o4633" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s9" value="crested" value_original="crested" />
      </biological_entity>
      <biological_entity id="o4634" name="horn" name_original="horns" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o4635" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="intermediate" value_original="intermediate" />
        <character is_modifier="false" modifier="weakly" name="development" src="d0_s9" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4636" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus confusus is weakly defined and perhaps best treated as a part of C. divergens (L. Abrams and R. S. Ferris 1923–1960, vol. 3). At least some populations in the Hood Mountains (Napa and Sonoma counties) include plants with the habit and leaf morphology of both species, while other, more uniform populations appear intermediate; it remains to be determined whether this pattern is a product of primary or secondary intergradation.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils apparently derived from serpentine or volcanic substrates, chaparral, oak and pine woodlands, conifer forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="serpentine" />
        <character name="habitat" value="volcanic substrates" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine woodlands" />
        <character name="habitat" value="forests" modifier="conifer" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>