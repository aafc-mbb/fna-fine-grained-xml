<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="N. H. Holmgren &amp; P. K. Holmgren" date="2002" rank="species">tiehmii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>27: 748, fig. 1. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species tiehmii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101819</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Jerry’s blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, bushlike, with subterranean caudices.</text>
      <biological_entity id="o1765" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushlike" value_original="bushlike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1766" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <relation from="o1765" id="r149" name="with" negation="false" src="d0_s0" to="o1766" />
    </statement>
    <statement id="d0_s1">
      <text>Stems multiple, erect, straight;</text>
      <biological_entity id="o1767" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches along entire stem, all ± equal, antrorse, upcurved;</text>
      <biological_entity id="o1769" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1768" id="r150" name="along" negation="false" src="d0_s2" to="o1769" />
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity id="o1768" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" notes="" src="d0_s2" value="equal" value_original="equal" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="upcurved" value_original="upcurved" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 19–47.5 × 5.8–9.1 mm, widest intersinus distance 4.6–7.1 mm;</text>
      <biological_entity id="o1770" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1771" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s4" to="47.5" to_unit="mm" />
        <character char_type="range_value" from="5.8" from_unit="mm" name="width" src="d0_s4" to="9.1" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" src="d0_s4" to="7.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate to elliptic, margins dentate, teeth 6–20, perpendicular to leaf axis, 0.5–1.9 mm;</text>
      <biological_entity id="o1772" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o1773" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity id="o1774" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o1775" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="20" />
        <character constraint="to leaf axis" constraintid="o1776" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o1776" name="axis" name_original="axis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal lanceolate, deltate, or cordate, base clasping, margins entire or dentate, teeth 0–6, perpendicular to leaf axis, 0.4–1.4 mm;</text>
      <biological_entity id="o1777" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o1778" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o1779" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o1780" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o1781" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="6" />
        <character constraint="to leaf axis" constraintid="o1782" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o1782" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with complex grappling-hook trichomes, adaxial surface with complex grappling-hook and needlelike trichomes, both surfaces green, moderately hairy.</text>
      <biological_entity id="o1783" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o1784" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity constraint="grappling-hook" id="o1785" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1786" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o1787" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o1788" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity id="o1789" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o1784" id="r151" name="with" negation="false" src="d0_s7" to="o1785" />
      <relation from="o1786" id="r152" name="with" negation="false" src="d0_s7" to="o1787" />
      <relation from="o1786" id="r153" name="with" negation="false" src="d0_s7" to="o1788" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o1790" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o1791" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 7.7–10.8 × 1.8–4.8 mm, apex rounded, glabrous abaxially;</text>
      <biological_entity id="o1792" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1793" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="7.7" from_unit="mm" name="length" src="d0_s9" to="10.8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s9" to="4.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1794" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 7–7.3 × 1.2–2 mm, with anthers, second whorl with anthers;</text>
      <biological_entity id="o1795" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1796" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o1797" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o1798" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="7.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1799" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o1800" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o1801" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o1798" id="r154" name="with" negation="false" src="d0_s10" to="o1799" />
      <relation from="o1800" id="r155" name="with" negation="false" src="d0_s10" to="o1801" />
    </statement>
    <statement id="d0_s11">
      <text>anthers twisted after dehiscence, epidermis smooth;</text>
      <biological_entity id="o1802" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1803" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o1804" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.8–8 mm.</text>
      <biological_entity id="o1805" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1806" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped, 4–6.6 × 4–7.4 mm, base rounded, not longitudinally ridged.</text>
      <biological_entity id="o1807" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6.6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="7.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1808" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls straight, papillae 15–46 per cell.</text>
      <biological_entity id="o1809" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o1810" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o1811" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o1812" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o1813" from="15" name="quantity" src="d0_s14" to="46" />
      </biological_entity>
      <biological_entity id="o1813" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Mentzelia tiehmii occurs in Lincoln and Nye counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>White, sparsely vegetated clay knolls rich in gypsum.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="white" />
        <character name="habitat" value="vegetated clay" modifier="sparsely" />
        <character name="habitat" value="gypsum" modifier="knolls rich in" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–1600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>