<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="A. Richard" date="1841" rank="species">intermedia</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Phys. Cuba, Pl. Vasc.,</publication_title>
      <place_in_publication>315. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species intermedia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101509</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ionoxalis</taxon_name>
    <taxon_name authority="A. St.-Hilaire" date="unknown" rank="species">intermedia</taxon_name>
    <taxon_hierarchy>genus ionoxalis;species intermedia</taxon_hierarchy>
  </taxon_identification>
  <number>31.</number>
  <other_name type="common_name">West Indian wood-sorrel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, acaulous, rhizomes absent, stolons often present, numerous, slender, with bulblets at tips, bulbs usually clustered, sometimes solitary;</text>
      <biological_entity id="o33327" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulous" value_original="acaulous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o33328" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o33329" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o33330" name="bulblet" name_original="bulblets" src="d0_s0" type="structure" />
      <biological_entity id="o33331" name="tip" name_original="tips" src="d0_s0" type="structure" />
      <biological_entity id="o33332" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o33329" id="r2734" name="with" negation="false" src="d0_s0" to="o33330" />
      <relation from="o33330" id="r2735" name="at" negation="false" src="d0_s0" to="o33331" />
    </statement>
    <statement id="d0_s1">
      <text>bulb scales (3–) 5–7-nerved.</text>
      <biological_entity constraint="bulb" id="o33333" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="(3-)5-7-nerved" value_original="(3-)5-7-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, rarely absent at flowering;</text>
      <biological_entity id="o33334" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character constraint="at flowering" is_modifier="false" modifier="rarely" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–22 cm;</text>
      <biological_entity id="o33335" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3, green, obtriangular to broadly obtriangular, 20–50 mm, lobed 1/5–1/3 length, lobes apically truncate, surfaces glabrous, oxalate deposits absent.</text>
      <biological_entity id="o33336" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="obtriangular" name="shape" src="d0_s4" to="broadly obtriangular" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="1/5" name="length" src="d0_s4" to="1/3" />
      </biological_entity>
      <biological_entity id="o33337" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o33338" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o33339" name="deposit" name_original="deposits" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences umbelliform cymes, 3–12 (–18) -flowered;</text>
      <biological_entity id="o33340" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="3-12(-18)-flowered" value_original="3-12(-18)-flowered" />
      </biological_entity>
      <biological_entity id="o33341" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="umbelliform" value_original="umbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scapes 7–30 cm, glabrous or sparsely hairy.</text>
      <biological_entity id="o33342" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers semihomostylous;</text>
      <biological_entity id="o33343" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>sepal apices with 2 orange tubercles;</text>
      <biological_entity constraint="sepal" id="o33344" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o33345" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="orange" value_original="orange" />
      </biological_entity>
      <relation from="o33344" id="r2736" name="with" negation="false" src="d0_s8" to="o33345" />
    </statement>
    <statement id="d0_s9">
      <text>petals usually lavender to purple, less commonly pink or white, 8–12 mm.</text>
      <biological_entity id="o33346" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually lavender" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="less commonly" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ellipsoid, 3–8 mm, glabrous.</text>
      <biological_entity id="o33347" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oxalis intermedia is recognized by a combination of its large, obtriangular leaflets; numerous, small flowers; and usually clustered bulbs. It was collected in California in 1934 and Massachusetts in 1940 but does not appear to have become naturalized in either state. Plants in the flora area are usually without fertile fruit.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gardens, fields, orchards, roadsides, moist waste areas, fencerows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gardens" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste areas" modifier="moist" />
        <character name="habitat" value="fencerows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., La., Tex.; West Indies; introduced also in Mexico (Chiapas, San Luis Potosí, Veracruz).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value=" also in Mexico (Chiapas)" establishment_means="introduced" />
        <character name="distribution" value=" also in Mexico (San Luis Potosí)" establishment_means="introduced" />
        <character name="distribution" value=" also in Mexico (Veracruz)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>