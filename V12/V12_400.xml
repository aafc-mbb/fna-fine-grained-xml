<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Persoon" date="1806" rank="species">serpillifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">serpillifolia</taxon_name>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species serpillifolia;subspecies serpillifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242427508</other_info_on_name>
  </taxon_identification>
  <number>79a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous.</text>
      <biological_entity id="o17256" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole and blade glabrous.</text>
      <biological_entity id="o17257" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17258" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17259" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucre glabrous.</text>
      <biological_entity id="o17260" name="involucre" name_original="involucre" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pistillate flowers: ovary glabrous.</text>
      <biological_entity id="o17261" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17262" name="ovary" name_original="ovary" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsules glabrous.</text>
      <biological_entity id="o17263" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies serpillifolia is likely native to the central-western United States and Canada, and it may be disjunct in northern Argentina. It appears to be adventive in eastern Canada and possibly other northern and eastern parts of its range. A report of the species from Cameron Parish, Louisiana, was based on a misidentified specimen of E. glyptosperma.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round in response to sufficient moisture (but mostly summer–fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in response to sufficient moisture (but mostly" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" modifier="in response to sufficient moisture (but mostly" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine forests, cottonwood-willow riparian forests, temperate deciduous forests, chaparral, grasslands, Joshua tree woodlands, desert scrub, juniper-sagebrush scrub, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="cottonwood-willow riparian forests" />
        <character name="habitat" value="deciduous forests" modifier="temperate" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="joshua tree woodlands" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="juniper-sagebrush scrub" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pine forests, cottonwood-willow riparian forests, temperate deciduous forests, chaparral, grasslands, Joshua tree woodlands, desert scrub, juniper-sagebrush scrub, disturbed areas; Alta., B.C., Man., N.B., Ont., Que., Sask.; Ariz., Calif., Colo., Fla., Ga., Idaho, Ill., Iowa, Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.Mex., N.Y., N.Dak., Okla., Oreg., Pa., S.Dak., Tex., Utah, Wash., Wis., Wyo.; Mexico; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pine forests" establishment_means="native" />
        <character name="distribution" value="cottonwood-willow riparian forests" establishment_means="native" />
        <character name="distribution" value="temperate deciduous forests" establishment_means="native" />
        <character name="distribution" value="chaparral" establishment_means="native" />
        <character name="distribution" value="grasslands" establishment_means="native" />
        <character name="distribution" value="Joshua tree woodlands" establishment_means="native" />
        <character name="distribution" value="desert scrub" establishment_means="native" />
        <character name="distribution" value="juniper-sagebrush scrub" establishment_means="native" />
        <character name="distribution" value="disturbed areas" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>