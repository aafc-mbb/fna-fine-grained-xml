<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Millspaugh" date="1890" rank="species">laredana</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 88. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species laredana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101584</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Millspaugh) Small" date="unknown" rank="species">laredana</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species laredana</taxon_hierarchy>
  </taxon_identification>
  <number>53.</number>
  <other_name type="common_name">Laredo sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o21413" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o21414" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o21413" id="r1753" name="with" negation="false" src="d0_s0" to="o21414" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, ± mat-forming, 10–20 cm, densely ashy pilose-tomentose.</text>
      <biological_entity id="o21415" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="coloration_or_taste" src="d0_s1" value="ashy" value_original="ashy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose-tomentose" value_original="pilose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o21416" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules distinct, filiform, 0.5–1 mm, pilose-tomentose;</text>
      <biological_entity id="o21417" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose-tomentose" value_original="pilose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–1 mm, pilose-tomentose;</text>
      <biological_entity id="o21418" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-tomentose" value_original="pilose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to elliptic-oblong, 3–6 × 3–5 mm, base markedly asymmetric, rounded to slightly auriculate, margins usually entire, rarely largest leaves sparsely serrulate, apex acute to obtuse, surfaces moderately to densely strigose;</text>
      <biological_entity id="o21419" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21420" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="markedly" name="shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="slightly auriculate" />
      </biological_entity>
      <biological_entity id="o21421" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21422" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="rarely" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o21423" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>3-veined from base.</text>
      <biological_entity id="o21424" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character constraint="from base" constraintid="o21425" is_modifier="false" name="architecture" src="d0_s6" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o21425" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary or in small, cymose clusters at distal nodes or on congested, axillary branches;</text>
      <biological_entity id="o21426" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes or on axillary branches" constraintid="o21427" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="in small , cymose clusters" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o21427" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.5–1.5 mm.</text>
      <biological_entity id="o21428" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic, 0.6–1 × 0.5–1 mm, densely strigose;</text>
      <biological_entity id="o21429" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s9" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellowish to reddish, oval to oblong, 0.1 × 0.2–0.3 mm;</text>
      <biological_entity id="o21430" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s10" to="reddish" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s10" to="oblong" />
        <character name="length" src="d0_s10" unit="mm" value="0.1" value_original="0.1" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white to pink, rudimentary or minute, (0–) 0.1–0.2 × (0–) 0.1–0.3 mm, distal margin crenulate.</text>
      <biological_entity id="o21431" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="size" src="d0_s11" value="minute" value_original="minute" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s11" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s11" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_width" src="d0_s11" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21432" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 3–5.</text>
      <biological_entity id="o21433" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary densely white villous;</text>
      <biological_entity id="o21434" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21435" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.1–0.2 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o21436" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21437" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules broadly ovoid, 1.3–1.5 × 1.4–1.5 mm, villous on keels, often glabrous or less hairy between keels;</text>
      <biological_entity id="o21438" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
        <character constraint="on keels" constraintid="o21439" is_modifier="false" name="pubescence" src="d0_s15" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="often" name="pubescence" notes="" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character constraint="between keels" constraintid="o21440" is_modifier="false" modifier="less; less" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21439" name="keel" name_original="keels" src="d0_s15" type="structure" />
      <biological_entity id="o21440" name="keel" name_original="keels" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>columella 1.1–1.3 mm.</text>
      <biological_entity id="o21441" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds white, barely concealing brown undercoat, 4-angled, sharply angled in cross-section, abaxial faces plane to convex, adaxial faces concave, 1.1–1.2 × 0.5–0.7 mm, with several rounded, irregular, transverse ridges.</text>
      <biological_entity id="o21442" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s17" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character constraint="in cross-section" constraintid="o21443" is_modifier="false" modifier="sharply" name="shape" src="d0_s17" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o21443" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
      <biological_entity constraint="abaxial" id="o21444" name="face" name_original="faces" src="d0_s17" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s17" to="convex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21445" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s17" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s17" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21446" name="ridge" name_original="ridges" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="several" value_original="several" />
        <character is_modifier="true" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s17" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o21445" id="r1754" name="with" negation="false" src="d0_s17" to="o21446" />
    </statement>
  </description>
  <discussion>Euphorbia laredana is similar to E. prostrata but differs from that species in its more densely tomentose indumentum, leaves with usually entire rather than serrulate margins, and slightly longer seeds with rounded rather than sharp ridges. The species occurs primarily in southern Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting almost year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="al" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="al" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy, loamy, or gravelly sites, old dunes, pastures.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly sites" modifier="open" />
        <character name="habitat" value="old dunes" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="loamy" />
        <character name="habitat" value="gravelly sites" />
        <character name="habitat" value="pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>