<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1819" rank="genus">ARCEUTHOBIUM</taxon_name>
    <taxon_name authority="Engelmann" date="1850" rank="species">campylopodum</taxon_name>
    <taxon_name authority="(Hawksworth &amp; Wiens) Nickrent" date="2012" rank="subspecies">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-51: 10. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species campylopodum;subspecies californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101417</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arceuthobium</taxon_name>
    <taxon_name authority="Hawksworth &amp; Wiens" date="1970" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>22: 266. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus arceuthobium;species californicum</taxon_hierarchy>
  </taxon_identification>
  <number>7e.</number>
  <other_name type="common_name">Sugar pine dwarf mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming witches brooms.</text>
      <biological_entity id="o29209" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29210" name="witch" name_original="witches" src="d0_s0" type="structure" />
      <relation from="o29209" id="r2407" name="forming" negation="false" src="d0_s0" to="o29210" />
    </statement>
    <statement id="d0_s1">
      <text>Stems bright-yellow or green, 6–8 (–14) cm;</text>
    </statement>
    <statement id="d0_s2">
      <text>third internode 6–10.5 (–16) × 1–1.5 (–2) mm, dominant shoot 1.5–4 mm diam. at base.</text>
      <biological_entity id="o29211" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="14" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29212" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="10.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="16" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29213" name="shoot" name_original="shoot" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="dominant" value_original="dominant" />
        <character char_type="range_value" constraint="at base" constraintid="o29214" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29214" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers 3.3 mm diam.;</text>
      <biological_entity id="o29215" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character name="diameter" src="d0_s3" unit="mm" value="3.3" value_original="3.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 3–4.</text>
      <biological_entity id="o29216" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 4 × 2.5 mm.</text>
      <biological_entity id="o29217" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="mm" value="4" value_original="4" />
        <character name="width" src="d0_s5" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meiosis occurs in July, with fruits maturing 13–14 months after pollination.</discussion>
  <discussion>As the common name implies, subsp. californicum is parasitic primarily on Pinus lambertiana, secondarily on P. monticola. It is found from the Peninsular Ranges of San Diego County through the Sierra Nevada to the Cascade Range of Siskiyou County, as well as some locations in the Klamath Mountains. In some locations it is sympatric with subsp. campylopodum, and rarely both taxa can be found on the same host. It induces large witches' brooms on sugar pine and is considered a serious pathogen of that species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests with sugar pine or western white pine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="coniferous" constraint="with sugar pine or western white pine" />
        <character name="habitat" value="sugar pine" modifier="with" />
        <character name="habitat" value="western white pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>