<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Petra Hoffmann, Geoffrey A. Levin</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">PHYLLANTHACEAE</taxon_name>
    <taxon_name authority="(Urban) Petra Hoffmann" date="2008" rank="genus">HETEROSAVIA</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>60: 152. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phyllanthaceae;genus heterosavia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hetero-, other or different from, and genus Savia</other_info_on_name>
    <other_info_on_name type="fna_id">318088</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Willdenow" date="unknown" rank="genus">Savia</taxon_name>
    <taxon_name authority="Urban" date="1902" rank="section">Heterosavia</taxon_name>
    <place_of_publication>
      <publication_title>Symb. Antill.</publication_title>
      <place_in_publication>3: 284. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus savia;section heterosavia</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Maidenbush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, dioecious, hairy, glabrescent, hairs simple;</text>
      <biological_entity id="o17350" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branching not phyllanthoid.</text>
      <biological_entity id="o17352" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, alternate, simple, all well developed;</text>
      <biological_entity id="o17353" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules persistent;</text>
      <biological_entity id="o17354" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire.</text>
      <biological_entity constraint="blade" id="o17355" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences unisexual, staminate glomes, pistillate flowers solitary.</text>
      <biological_entity id="o17356" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o17357" name="glome" name_original="glomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17358" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate rudimentary, pistillate present.</text>
      <biological_entity id="o17359" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: sepals 5, distinct;</text>
      <biological_entity id="o17360" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17361" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5;</text>
      <biological_entity id="o17362" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17363" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary extrastaminal, annular, crenate [entire];</text>
      <biological_entity id="o17364" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17365" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="extrastaminal" value_original="extrastaminal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="annular" value_original="annular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 5;</text>
      <biological_entity id="o17366" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17367" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments distinct or connate basally [to 1/2 length];</text>
      <biological_entity id="o17368" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17369" name="all" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>connectives not extending beyond anthers;</text>
      <biological_entity id="o17370" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17371" name="connective" name_original="connectives" src="d0_s12" type="structure" />
      <biological_entity id="o17372" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o17371" id="r1435" name="extending beyond" negation="false" src="d0_s12" to="o17372" />
    </statement>
    <statement id="d0_s13">
      <text>pistillode 3-divided to base or nearly so.</text>
      <biological_entity id="o17373" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17374" name="pistillode" name_original="pistillode" src="d0_s13" type="structure">
        <character constraint="to base" constraintid="o17375" is_modifier="false" name="shape" src="d0_s13" value="3-divided" value_original="3-divided" />
      </biological_entity>
      <biological_entity id="o17375" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: sepals persistent, 5, distinct;</text>
      <biological_entity id="o17376" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17377" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals 5;</text>
      <biological_entity id="o17378" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17379" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectary annular, crenate [entire];</text>
      <biological_entity id="o17380" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17381" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="annular" value_original="annular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pistil 3 (–4) -carpellate;</text>
      <biological_entity id="o17382" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17383" name="pistil" name_original="pistil" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="3(-4)-carpellate" value_original="3(-4)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles (3–) 4, distinct, 2-fid 1/2 length.</text>
      <biological_entity id="o17384" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17385" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s18" to="4" to_inclusive="false" />
        <character name="quantity" src="d0_s18" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s18" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s18" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o17386" name="capsule" name_original="capsules" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds 2 per locule, rounded-trigonous;</text>
      <biological_entity id="o17387" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character constraint="per locule" constraintid="o17388" name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s20" value="rounded-trigonous" value_original="rounded-trigonous" />
      </biological_entity>
      <biological_entity id="o17388" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>seed-coat dry, smooth;</text>
      <biological_entity id="o17389" name="seed-coat" name_original="seed-coat" src="d0_s21" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s21" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s21" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>caruncle absent.</text>
      <biological_entity id="o17390" name="caruncle" name_original="caruncle" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., West Indies (Greater Antilles).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Greater Antilles)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Molecular phylogenetic analyses have shown that Savia as traditionally recognized is polyphyletic and should be treated as three genera, each assigned to a different tribe (H. Kathriarachchi et al. 2005; P. Hoffmann et al. 2006; Hoffmann 2008). Heterosavia, which includes the species formerly included in the Caribbean Savia sect. Heterosavia, belongs to Phyllantheae Dumortier and is sister to Flueggea.</discussion>
  <references>
    <reference>Hoffmann, P. 2008. Revision of Heterosavia, stat. nov., with notes on Gonatogyne and Savia (Phyllanthaceae). Brittonia 60: 136–166.</reference>
  </references>
  
</bio:treatment>