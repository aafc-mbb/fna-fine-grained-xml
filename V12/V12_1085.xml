<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jinshuang Ma</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Sargent" date="1891" rank="genus">GYMINDA</taxon_name>
    <place_of_publication>
      <publication_title>Gard. &amp; Forest</publication_title>
      <place_in_publication>4: 4. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus gyminda</taxon_hierarchy>
    <other_info_on_name type="etymology">Anagram of Myginda, to which these species had been referred</other_info_on_name>
    <other_info_on_name type="fna_id">114221</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, dioecious.</text>
      <biological_entity id="o31177" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets 4-angled.</text>
      <biological_entity id="o31179" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite;</text>
      <biological_entity id="o31180" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present;</text>
      <biological_entity id="o31181" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o31182" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins entire or toothed;</text>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o31183" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, cymes.</text>
      <biological_entity id="o31184" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o31185" name="cyme" name_original="cymes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual, radially symmetric;</text>
      <biological_entity id="o31186" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o31187" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o31188" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium absent;</text>
      <biological_entity id="o31189" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (3–) 4, distinct;</text>
      <biological_entity id="o31190" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="4" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (3–) 4, white;</text>
      <biological_entity id="o31191" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="4" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary intrastaminal, 4-lobed, fleshy.</text>
      <biological_entity id="o31192" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="intrastaminal" value_original="intrastaminal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-lobed" value_original="4-lobed" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Staminate flowers: stamens 4, free from and inserted between lobes of nectary;</text>
      <biological_entity id="o31193" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o31194" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character constraint="between lobes" constraintid="o31195" is_modifier="false" name="position" src="d0_s14" value="free-inserted" value_original="free-inserted" />
      </biological_entity>
      <biological_entity id="o31195" name="lobe" name_original="lobes" src="d0_s14" type="structure" />
      <biological_entity id="o31196" name="nectary" name_original="nectary" src="d0_s14" type="structure" />
      <relation from="o31195" id="r2546" name="part_of" negation="false" src="d0_s14" to="o31196" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes 0;</text>
      <biological_entity id="o31197" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o31198" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistillode present.</text>
      <biological_entity id="o31199" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o31200" name="pistillode" name_original="pistillode" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pistillate flowers: staminodes 0;</text>
      <biological_entity id="o31201" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31202" name="staminode" name_original="staminodes" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pistil 2-carpellate;</text>
      <biological_entity id="o31203" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31204" name="pistil" name_original="pistil" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary superior, adnate to nectary at base, 2-locular, placentation axile;</text>
      <biological_entity id="o31205" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31206" name="ovary" name_original="ovary" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="superior" value_original="superior" />
        <character constraint="to nectary" constraintid="o31207" is_modifier="false" name="fusion" src="d0_s19" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" notes="" src="d0_s19" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s19" value="axile" value_original="axile" />
      </biological_entity>
      <biological_entity id="o31207" name="nectary" name_original="nectary" src="d0_s19" type="structure" />
      <biological_entity id="o31208" name="base" name_original="base" src="d0_s19" type="structure" />
      <relation from="o31207" id="r2547" name="at" negation="false" src="d0_s19" to="o31208" />
    </statement>
    <statement id="d0_s20">
      <text>style absent;</text>
      <biological_entity id="o31209" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31210" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigmas 2;</text>
      <biological_entity id="o31211" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31212" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovule 1 per locule.</text>
      <biological_entity id="o31213" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31214" name="ovule" name_original="ovule" src="d0_s22" type="structure">
        <character constraint="per locule" constraintid="o31215" name="quantity" src="d0_s22" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31215" name="locule" name_original="locule" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>Fruits drupes, bluish black, 2-locular, ellipsoid to ovoid, apex not beaked.</text>
      <biological_entity constraint="fruits" id="o31216" name="drupe" name_original="drupes" src="d0_s23" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s23" value="bluish black" value_original="bluish black" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s23" value="2-locular" value_original="2-locular" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s23" to="ovoid" />
      </biological_entity>
      <biological_entity id="o31217" name="apex" name_original="apex" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s23" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds 1 per locule, sometimes 1 per fruit by abortion, ovoid, not winged;</text>
      <biological_entity id="o31218" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character constraint="per locule" constraintid="o31219" name="quantity" src="d0_s24" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o31219" name="locule" name_original="locule" src="d0_s24" type="structure">
        <character constraint="per fruit" constraintid="o31220" modifier="sometimes" name="quantity" src="d0_s24" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31220" name="fruit" name_original="fruit" src="d0_s24" type="structure" />
      <biological_entity id="o31221" name="abortion" name_original="abortion" src="d0_s24" type="structure" />
      <relation from="o31220" id="r2548" name="by" negation="false" src="d0_s24" to="o31221" />
    </statement>
    <statement id="d0_s25">
      <text>aril absent.</text>
      <biological_entity id="o31222" name="aril" name_original="aril" src="d0_s25" type="structure">
        <character is_modifier="false" name="presence" src="d0_s25" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., s Mexico, West Indies, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gyminda orbicularis Borhidi &amp; O. Muñiz is endemic to Cuba.</discussion>
  
</bio:treatment>