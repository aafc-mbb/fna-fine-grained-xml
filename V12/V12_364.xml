<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">hirta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">hirta</taxon_name>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species hirta;variety hirta</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101499</other_info_on_name>
  </taxon_identification>
  <number>18a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, caulescent, rhizomes absent, stolons present, bulbs absent.</text>
      <biological_entity id="o2019" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o2020" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2021" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2022" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial stems (1–) 2–5 from base, erect to ascending, 10–30 cm, herbaceous, villous.</text>
      <biological_entity id="o2023" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="2" to_inclusive="false" />
        <character char_type="range_value" constraint="from base" constraintid="o2024" from="2" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o2024" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
      <biological_entity id="o2025" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules rudimentary;</text>
      <biological_entity id="o2026" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–0.2 cm;</text>
      <biological_entity id="o2027" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="0.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3, green, linear to narrowly oblanceolate or oblong-cuneate, (3–) 5–15 mm, not lobed, abaxial surface villous, adaxial surface glabrous, oxalate deposits absent.</text>
      <biological_entity id="o2028" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly oblanceolate or oblong-cuneate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2029" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2030" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o2031" name="deposit" name_original="deposits" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered, axillary, produced evenly along stems;</text>
      <biological_entity id="o2032" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2033" name="stem" name_original="stems" src="d0_s6" type="structure" />
      <relation from="o2032" id="r174" name="produced" negation="false" src="d0_s6" to="o2033" />
    </statement>
    <statement id="d0_s7">
      <text>peduncles 1.5–2.5 (–7) cm.</text>
      <biological_entity id="o2034" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers heterostylous;</text>
      <biological_entity id="o2035" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="heterostylous" value_original="heterostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepal apices without tubercles;</text>
      <biological_entity constraint="sepal" id="o2036" name="apex" name_original="apices" src="d0_s9" type="structure" />
      <biological_entity id="o2037" name="tubercle" name_original="tubercles" src="d0_s9" type="structure" />
      <relation from="o2036" id="r175" name="without" negation="false" src="d0_s9" to="o2037" />
    </statement>
    <statement id="d0_s10">
      <text>petals rosy purple to pink or white, 15–20 (–25) mm.</text>
      <biological_entity id="o2038" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="rosy purple" name="coloration" src="d0_s10" to="pink or white" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules not seen.</text>
      <biological_entity id="o2039" name="capsule" name_original="capsules" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, especially near gardens, cemeteries, sidewalks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="gardens" modifier="especially near" />
        <character name="habitat" value="cemeteries" />
        <character name="habitat" value="sidewalks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Africa (South Africa); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Africa (South Africa)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>