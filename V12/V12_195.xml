<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">400</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="(A. Gray) Small in N. L. Britton et al." date="1907" rank="genus">HESPEROLINON</taxon_name>
    <taxon_name authority="(Curran) Small in N. L. Britton et al." date="1907" rank="species">drymarioides</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>25: 84. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus hesperolinon;species drymarioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101743</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linum</taxon_name>
    <taxon_name authority="Curran" date="1885" rank="species">drymarioides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 152. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus linum;species drymarioides</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Drymaria-like western flax</other_name>
  <other_name type="common_name">drymary dwarf flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, (5–) 15–20 (–30) cm, puberulent, sometimes appearing hoary, stems less hairy distally;</text>
      <biological_entity id="o24242" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes; less; distally" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24243" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="hoary" value_original="hoary" />
      </biological_entity>
      <relation from="o24242" id="r1983" modifier="sometimes" name="appearing" negation="false" src="d0_s0" to="o24243" />
    </statement>
    <statement id="d0_s1">
      <text>branches from distal nodes, alternate, virgate, 1 or 2 prostrate.</text>
      <biological_entity id="o24244" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="virgate" value_original="virgate" />
        <character name="quantity" src="d0_s1" unit="or prostrate" value="1" value_original="1" />
        <character name="quantity" src="d0_s1" unit="or prostrate" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24245" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o24244" id="r1984" name="from" negation="false" src="d0_s1" to="o24245" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, whorled (usually in 4s), distal sometimes ± opposite;</text>
      <biological_entity id="o24246" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24247" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes more or less" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands absent;</text>
      <biological_entity constraint="stipular" id="o24248" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate or orbiculate, 4–8 × 3–6 mm, base keeled, clasping, margins minutely stipitate-glandular.</text>
      <biological_entity id="o24249" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24250" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o24251" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: cymes dichasial and open proximally, monochasial and dense near tips, few-branched, internodes long proximally, short near apices, flowers congested;</text>
      <biological_entity id="o24252" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o24253" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="monochasial" value_original="monochasial" />
        <character constraint="near tips" constraintid="o24254" is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="few-branched" value_original="few-branched" />
      </biological_entity>
      <biological_entity id="o24254" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity id="o24255" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character constraint="near apices" constraintid="o24256" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o24256" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o24257" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bract margins stipitate-glandular.</text>
      <biological_entity id="o24258" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="bract" id="o24259" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–2 (–10) mm, scarcely longer in fruit, little spreading, not bent at apex.</text>
      <biological_entity id="o24260" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character constraint="in fruit" constraintid="o24261" is_modifier="false" modifier="scarcely" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="at apex" constraintid="o24262" is_modifier="false" modifier="not" name="shape" src="d0_s7" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o24261" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o24262" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals erect, tips spreading (even in bud), ovate to broadly lanceolate, 2.5–4 mm, usually equal, marginal glands inconspicuous, surfaces glabrous or with a few stiff, white hairs;</text>
      <biological_entity id="o24263" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24264" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o24265" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="broadly lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o24266" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o24267" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with a few stiff , white hairs" />
      </biological_entity>
      <biological_entity id="o24268" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <relation from="o24267" id="r1985" name="with" negation="false" src="d0_s8" to="o24268" />
    </statement>
    <statement id="d0_s9">
      <text>petals widely spreading, white or lavendar to pink, sometimes streaked with deeper pink or rose-purple, obovate, 3–5 (–6) mm, apex notched;</text>
      <biological_entity id="o24269" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24270" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character name="coloration" src="d0_s9" value="lavendar" value_original="lavendar" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="streaked with deeper pink or streaked with rose-purple" />
        <character name="coloration" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24271" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cup white, rim lobed between filaments and petal attachment;</text>
      <biological_entity id="o24272" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24273" name="cup" name_original="cup" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o24274" name="rim" name_original="rim" src="d0_s10" type="structure">
        <character constraint="between filaments, attachment" constraintid="o24275, o24276" is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o24275" name="all" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o24276" name="attachment" name_original="attachment" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o24277" name="all" name_original="filaments" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens exserted;</text>
      <biological_entity id="o24278" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24279" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 2.5–3 (–3.5) mm;</text>
      <biological_entity id="o24280" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o24281" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers usually white, sometimes purple, white-margined, dehisced anthers (0.8–) 1–1.5 (–2) mm;</text>
      <biological_entity id="o24282" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24283" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="white-margined" value_original="white-margined" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24284" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o24283" id="r1986" name="dehisced" negation="false" src="d0_s13" to="o24284" />
    </statement>
    <statement id="d0_s14">
      <text>ovary chambers 6;</text>
      <biological_entity id="o24285" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="ovary" id="o24286" name="chamber" name_original="chambers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, white, (2–) 2.5–3 (–3.5) mm, exserted.</text>
      <biological_entity id="o24287" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o24288" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hesperolinon drymarioides is known from only 20 populations scattered along the eastern slopes of the Inner North Coast Ranges of Colusa, Lake, and Napa counties. It is the most distinctive species in the genus. The persistent, ovate to orbiculate leaves arranged in well-spaced whorls, separated by long internodes, to the top of the main axis, with margins outlined by rows of tiny, stipitate glands, set it apart from all other species. The species is also unusual in often producing one or two long, decumbent, cyme-producing branches at the base of the main stem (H. K. Sharsmith 1961).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry chaparral ridges or woodlands, serpentine soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry chaparral ridges" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="serpentine soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>