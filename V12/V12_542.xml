<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="illustration_page">161</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ACALYPHA</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Voy. Sulphur,</publication_title>
      <place_in_publication>51. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus acalypha;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101510</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acalypha</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">pringlei</taxon_name>
    <taxon_hierarchy>genus acalypha;species pringlei</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">California copperleaf</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–10 dm, monoecious.</text>
      <biological_entity id="o11329" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, hirsute and stipitate-glandular, becoming glabrate.</text>
      <biological_entity id="o11330" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent or drought-deciduous;</text>
      <biological_entity id="o11331" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="drought-deciduous" value_original="drought-deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.5–2.5 cm;</text>
      <biological_entity id="o11332" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to cordate, 1–5 × 0.5–4 cm, base truncate to rounded or cordate, margins serrate-crenate, apex acute or obtuse.</text>
      <biological_entity id="o11333" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="cordate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11334" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="rounded or cordate" />
      </biological_entity>
      <biological_entity id="o11335" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="serrate-crenate" value_original="serrate-crenate" />
      </biological_entity>
      <biological_entity id="o11336" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences unisexual and bisexual, axillary and terminal;</text>
      <biological_entity id="o11337" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminate peduncle 0.3–2.5 cm, fertile portion 1–4 cm;</text>
      <biological_entity id="o11338" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11339" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate peduncle 0.4–3 cm, fertile portion 1–3 × 0.8–1.2 cm;</text>
      <biological_entity id="o11340" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual similar to staminate, with 1–3 pistillate bracts near base;</text>
      <biological_entity id="o11341" name="portion" name_original="portion" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s7" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o11342" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11343" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o11342" id="r937" name="near" negation="false" src="d0_s8" to="o11343" />
    </statement>
    <statement id="d0_s9">
      <text>allomorphic pistillate flowers absent.</text>
      <biological_entity id="o11344" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate bracts loosely arranged (inflorescence axis visible between bracts), 3–6 × 5.5–11 mm, abaxial surface pubescent, sessile and stipitate-glandular;</text>
      <biological_entity id="o11345" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s10" value="arranged" value_original="arranged" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11346" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lobes (8–) 10–18, rounded, 1/5 bract length.</text>
      <biological_entity id="o11347" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s11" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="18" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s11" value="1/5" value_original="1/5" />
      </biological_entity>
      <biological_entity id="o11348" name="bract" name_original="bract" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: pistil 3-carpellate;</text>
      <biological_entity id="o11349" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11350" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles multifid or laciniate.</text>
      <biological_entity id="o11351" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11352" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="multifid" value_original="multifid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules smooth, pubescent and stipitate-glandular.</text>
      <biological_entity id="o11353" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5–2 mm, minutely pitted.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20.</text>
      <biological_entity id="o11354" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11355" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants in Arizona and Sonora have been segregated as Acalypha pringlei based on having long nonglandular hairs mixed with shorter hairs on the stem (versus hairs all of one length). This trait appears throughout the range of A. californica and cannot be used to distinguish two species (G. A. Levin 1995).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round, especially spring and fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" modifier="especially" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="especially" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arid rocky slopes, desert washes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arid rocky slopes" />
        <character name="habitat" value="desert washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Baja California Sur, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>