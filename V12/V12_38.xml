<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="McMinn" date="1933" rank="species">ferrisiae</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>2: 89. 1933</place_in_publication>
      <other_info_on_pub>(as ferrisae)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species ferrisiae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101439</other_info_on_name>
  </taxon_identification>
  <number>34.</number>
  <other_name type="common_name">Coyote ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–2 m.</text>
      <biological_entity id="o18017" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, not rooting at nodes;</text>
      <biological_entity id="o18018" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at nodes" constraintid="o18019" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o18019" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets grayish brown, glaucous, rigid, puberulent.</text>
      <biological_entity id="o18020" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish brown" value_original="grayish brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled;</text>
      <biological_entity id="o18021" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–3 mm;</text>
      <biological_entity id="o18022" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat or ± cupped, widely elliptic to widely obovate, 11–30 × 7–18 mm, base obtuse to rounded, margins not revolute, usually denticulate, rarely entire, teeth 6–13, apex rounded, abaxial surface pale green, sparsely strigillose between veins, adaxial surface dark green, glabrate.</text>
      <biological_entity id="o18023" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character name="prominence_or_shape" src="d0_s5" value="more or less" value_original="more or less" />
        <character char_type="range_value" from="widely elliptic" name="shape" src="d0_s5" to="widely obovate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18024" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o18025" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18026" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o18027" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18028" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character constraint="between veins" constraintid="o18029" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o18029" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o18030" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, 1.2–1.5 (–2) cm.</text>
      <biological_entity id="o18031" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals and petals white;</text>
      <biological_entity id="o18032" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18033" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o18034" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary dark blue to purple.</text>
      <biological_entity id="o18035" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18036" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s8" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 7–9 mm wide, weakly lobed;</text>
      <biological_entity id="o18037" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves ± smooth, horns subapical, prominent, erect, intermediate ridges absent.</text>
      <biological_entity id="o18038" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18039" name="horn" name_original="horns" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o18040" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="intermediate" value_original="intermediate" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18041" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus ferrisiae, federally listed as endangered, occurs at a few localities in the foothills of the Mount Hamilton Range northeast of Morgan Hill, Santa Clara County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine soils and outcrops, chaparral, pine and oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soils" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>