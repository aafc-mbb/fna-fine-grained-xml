<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann in W. H. Emory" date="1859" rank="species">florida</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 189. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species florida</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101571</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Engelmann) Millspaugh" date="unknown" rank="species">florida</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species florida</taxon_hierarchy>
  </taxon_identification>
  <number>39.</number>
  <other_name type="common_name">Chiricahua Mountain sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with slender taproot.</text>
      <biological_entity id="o27991" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity constraint="slender" id="o27992" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o27991" id="r2308" name="with" negation="false" src="d0_s0" to="o27992" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 15–60 cm, usually glabrous, rarely puberulent.</text>
      <biological_entity id="o27993" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o27994" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules distinct, divided into 3–4 subulate-filiform divisions, 0.4–1.6 mm, usually glabrous, rarely puberulent;</text>
      <biological_entity id="o27995" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character constraint="into divisions" constraintid="o27996" is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o27996" name="division" name_original="divisions" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="true" name="shape" src="d0_s3" value="subulate-filiform" value_original="subulate-filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–2.5 mm, glabrous;</text>
      <biological_entity id="o27997" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually linear, rarely to narrowly elliptic, 10–40 (–60) × 0.5–2.5 mm, base symmetric, attenuate, margins serrulate, often revolute, apex acute, surfaces usually glabrous, rarely puberulent;</text>
      <biological_entity id="o27998" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely to narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27999" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o28000" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o28001" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>obscurely pinnately veined.</text>
      <biological_entity id="o28002" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="obscurely pinnately" name="architecture" src="d0_s6" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at nodes or in small, cymose clusters at branch tips;</text>
      <biological_entity id="o28003" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at nodes or in tips" constraintid="o28004" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o28004" name="tip" name_original="tips" src="d0_s7" type="structure" constraint="branch" constraint_original="at branch">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 1.2–8.1 mm.</text>
      <biological_entity id="o28005" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="8.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic, 1.7–2.4 × 1.5–2.1 mm, glabrous;</text>
      <biological_entity id="o28006" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s9" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, greenish yellow to slightly pink, circular to oblong, 0.4–0.5 × 0.4–0.6 mm;</text>
      <biological_entity id="o28007" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="greenish yellow" name="coloration" src="d0_s10" to="slightly pink" />
        <character char_type="range_value" from="circular" name="shape" src="d0_s10" to="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s10" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white to pink, obovoid, circular, flabellate, or oblong, 0.8–2.9 × 1–2.8 mm, distal margin entire.</text>
      <biological_entity id="o28008" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="circular" value_original="circular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s11" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28009" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 25–35.</text>
      <biological_entity id="o28010" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o28011" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28012" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.8–1.4 mm, 2-fid entire length.</text>
      <biological_entity id="o28013" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28014" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="length" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblate, 2.2–2.5 × 2.7–3.1 mm, glabrous;</text>
      <biological_entity id="o28015" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s15" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.8–2.1 mm.</text>
      <biological_entity id="o28016" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds light gray to light-brown, ovoid, slightly 4-angled in cross-section, 1.6–2 × 1.3–1.7 mm, with 2 or 3 well-developed transverse ridges.</text>
      <biological_entity id="o28017" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="light gray" name="coloration" src="d0_s17" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character constraint="in cross-section" constraintid="o28018" is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" notes="" src="d0_s17" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" notes="" src="d0_s17" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28018" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
      <biological_entity id="o28019" name="ridge" name_original="ridges" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s17" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o28017" id="r2309" name="with" negation="false" src="d0_s17" to="o28019" />
    </statement>
  </description>
  <discussion>Euphorbia florida is known in the flora area from Coconino County south to the Mexican border</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats, gravelly washes, rocky hillsides, talus slopes, desert scrub, desert grasslands, mesquite woodlands, rarely oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="gravelly washes" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="woodlands" modifier="mesquite" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>