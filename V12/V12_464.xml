<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">discoidalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>401. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species discoidalis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101535</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tithymalopsis</taxon_name>
    <taxon_name authority="(Chapman) Small" date="unknown" rank="species">discoidalis</taxon_name>
    <taxon_hierarchy>genus tithymalopsis;species discoidalis</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Summer spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with spreading rootstock.</text>
      <biological_entity id="o685" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o686" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o685" id="r66" name="with" negation="false" src="d0_s0" to="o686" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, unbranched, solitary or few, previous years dead stems not persistent, 45–70 cm, usually densely puberulent to sericeous, rarely glabrous.</text>
      <biological_entity id="o687" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="growth_order" src="d0_s1" value="previous" value_original="previous" />
      </biological_entity>
      <biological_entity id="o688" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="dead" value_original="dead" />
        <character is_modifier="false" modifier="not" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="usually densely puberulent" name="pubescence" src="d0_s1" to="sericeous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o689" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules to 0.1 mm;</text>
      <biological_entity id="o690" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0–) 1–2 mm (or absent), densely puberulent;</text>
      <biological_entity id="o691" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually linear, rarely ovate, 25–55 × 1.5–4 mm, base cuneate, margins entire, revolute, apex rounded, abaxial surface glabrous or puberulent to sericeous, adaxial surface glabrous;</text>
      <biological_entity id="o692" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o693" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o694" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o695" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o696" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s5" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation often obscure on smaller leaves, midvein conspicuous.</text>
      <biological_entity constraint="adaxial" id="o697" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="on smaller leaves" constraintid="o698" is_modifier="false" modifier="often" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o698" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o699" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in terminal pleiochasia;</text>
      <biological_entity id="o700" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o701" name="pleiochasium" name_original="pleiochasia" src="d0_s7" type="structure" />
      <relation from="o700" id="r67" name="in" negation="false" src="d0_s7" to="o701" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 5–15 mm, filiform, glabrous or very sparsely puberulent to sericeous.</text>
      <biological_entity id="o702" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s8" value="very sparsely puberulent to sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1.2–1.4 × 1.2–2 mm, sparsely to densely puberulent;</text>
      <biological_entity id="o703" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s9" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 5, green, reniform, 0.2–0.3 × 0.5–0.6 mm;</text>
      <biological_entity id="o704" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s10" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white, orbiculate to oblong, (0.5–) 1–1.7 × 1–1.5 mm, entire.</text>
      <biological_entity id="o705" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s11" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_length" src="d0_s11" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 20–25.</text>
      <biological_entity id="o706" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous or sparsely strigose;</text>
      <biological_entity id="o707" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o708" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.5–1.1 mm, 2-fid at apex to 1/2 length.</text>
      <biological_entity id="o709" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o710" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
        <character constraint="at apex" constraintid="o711" is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o711" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" name="length" src="d0_s14" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules globose, 1.8–3 × 2.5–4.8 mm, glabrous or sparsely strigose;</text>
      <biological_entity id="o712" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s15" to="4.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 2.3–2.5 mm.</text>
      <biological_entity id="o713" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds light gray, ovoid, 2 × 1.2–1.3 mm, smooth or with few, very shallow depressions;</text>
      <biological_entity id="o714" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="light gray" value_original="light gray" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character name="length" src="d0_s17" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s17" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="with few , very shallow depressions" />
      </biological_entity>
      <biological_entity id="o715" name="depression" name_original="depressions" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="few" value_original="few" />
        <character is_modifier="true" modifier="very" name="depth" src="d0_s17" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o714" id="r68" name="with" negation="false" src="d0_s17" to="o715" />
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o716" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>M. J. Huft (1979) remarked that Euphorbia discoidalis is uncommon west of Alabama and referred many narrow-leaved specimens from Louisiana and Texas to E. corollata. K. R. Park (1998) included them in an expanded E. discoidalis, and that is followed here. The western populations can be distinguished from E. corollata by their shorter involucral gland appendages and revolute leaf margins. Further study of these western populations is warranted.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand hills, pine savannas, woodland borders, open fields with sandy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand hills" />
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="borders" modifier="woodland" />
        <character name="habitat" value="open fields" constraint="with sandy soils" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–150 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>