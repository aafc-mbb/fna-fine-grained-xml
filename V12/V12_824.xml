<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
    <other_info_on_meta type="illustration_page">121</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CELASTRUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">scandens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 196. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus celastrus;species scandens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220002499</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">American bittersweet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines to 15 m.</text>
      <biological_entity id="o30788" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade oblong, 5–10 × 2–4 cm, aestivation involute.</text>
      <biological_entity id="o30789" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o30790" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="cm" name="aestivation" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="aestivation" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s1" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences terminal, panicles, 3–8 cm.</text>
      <biological_entity id="o30791" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o30792" name="panicle" name_original="panicles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers: pollen yellow.</text>
      <biological_entity id="o30793" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o30794" name="pollen" name_original="pollen" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsules orange, subglobose, 4–8 × 5–9 mm, glabrous.</text>
      <biological_entity id="o30795" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seeds dark-brown, 6 mm. 2n = 46.</text>
      <biological_entity id="o30796" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="6" value_original="6" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30797" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, woodland margins, roadsides, usually rich soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="woodland margins" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="rich soils" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Que., Sask.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>