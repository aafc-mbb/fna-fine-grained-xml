<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PARNASSIA</taxon_name>
    <taxon_name authority="Piper" date="1899" rank="species">cirrata</taxon_name>
    <taxon_name authority="(Rydberg) P. K. Holmgren &amp; N. H. Holmgren in A. Cronquist et al." date="1997" rank="variety">intermedia</taxon_name>
    <place_of_publication>
      <publication_title>Intermount. Fl.</publication_title>
      <place_in_publication>3(A): 61. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus parnassia;species cirrata;variety intermedia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101470</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parnassia</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1905" rank="species">intermedia</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>22: 78. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus parnassia;species intermedia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Koenig" date="unknown" rank="species">fimbriata</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="variety">hoodiana</taxon_name>
    <taxon_hierarchy>genus p.;species fimbriata;variety hoodiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fimbriata</taxon_name>
    <taxon_name authority="(Rydberg) C. L. Hitchcock" date="unknown" rank="variety">intermedia</taxon_name>
    <taxon_hierarchy>genus p.;species fimbriata;variety intermedia</taxon_hierarchy>
  </taxon_identification>
  <number>5b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blade (of larger leaves) (10–) 13–50 mm wide.</text>
      <biological_entity id="o6996" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o6997" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s0" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s0" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: sepals (2–) 2.5–3.5 mm wide;</text>
      <biological_entity id="o6998" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o6999" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>larger petals (4–) 5–10 mm wide, longer fimbriae 1–3 (–3.5) mm.</text>
      <biological_entity id="o7000" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="larger" id="o7001" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o7002" name="fimbria" name_original="fimbriae" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In California, var. intermedia is known only from Shasta, Siskiyou, and Trinity counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, fens, wet meadows, streamsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>