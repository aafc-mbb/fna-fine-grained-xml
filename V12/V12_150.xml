<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="illustration_page">150</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="Savigny in J. Lamarck et al." date="1798" rank="species">articulata</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>4: 686. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species articulata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250031733</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxalis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">articulata</taxon_name>
    <taxon_name authority="(A. St.-Hilaire) Lourteig" date="unknown" rank="subspecies">rubra</taxon_name>
    <taxon_hierarchy>genus oxalis;species articulata;subspecies rubra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="A. St.-Hilaire" date="unknown" rank="species">rubra</taxon_name>
    <taxon_hierarchy>genus o.;species rubra</taxon_hierarchy>
  </taxon_identification>
  <number>36.</number>
  <other_name type="common_name">Windowbox wood-sorrel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, acaulous, rhizomes present, thick, woody, irregularly nodulate-segmented, often covered with persistent petiole bases, stolons absent, bulbs absent.</text>
      <biological_entity id="o23680" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulous" value_original="acaulous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o23681" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s0" value="nodulate-segmented" value_original="nodulate-segmented" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o23682" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o23683" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23684" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o23681" id="r1952" modifier="often" name="covered with" negation="false" src="d0_s0" to="o23682" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o23685" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 11–30 cm;</text>
      <biological_entity id="o23686" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3, green to purplish abaxially, green adaxially, rounded-obcordate, 18–20 mm, margins densely loosely ciliate, lobed 1/5–1/3 length, lobes apically convex, surfaces evenly strigose-villous to strigose-hirsute, oxalate deposits in dots concentrated mostly toward margins or over whole surface.</text>
      <biological_entity id="o23687" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="purplish abaxially" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded-obcordate" value_original="rounded-obcordate" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23688" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely loosely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="1/5" name="length" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o23689" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o23690" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="evenly strigose-villous" name="pubescence" src="d0_s3" to="strigose-hirsute" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o23691" name="deposit" name_original="deposits" src="d0_s3" type="structure">
        <character constraint="toward " constraintid="o23693, o23694" is_modifier="false" name="arrangement_or_density" src="d0_s3" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity id="o23692" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o23693" name="whole" name_original="whole" src="d0_s3" type="structure" />
      <biological_entity id="o23694" name="surface" name_original="surface" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences usually umbelliform cymes, less commonly in irregular cymes, 3–12-flowered;</text>
      <biological_entity id="o23695" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="3-12-flowered" value_original="3-12-flowered" />
      </biological_entity>
      <biological_entity id="o23696" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="umbelliform" value_original="umbelliform" />
      </biological_entity>
      <biological_entity id="o23697" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
      </biological_entity>
      <relation from="o23695" id="r1953" modifier="less commonly; commonly" name="in" negation="false" src="d0_s4" to="o23697" />
    </statement>
    <statement id="d0_s5">
      <text>scapes 12–28 cm, sparsely strigose.</text>
      <biological_entity id="o23698" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s5" to="28" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers heterostylous;</text>
      <biological_entity id="o23699" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="heterostylous" value_original="heterostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepal apices with 2 orange tubercles;</text>
      <biological_entity constraint="sepal" id="o23700" name="apex" name_original="apices" src="d0_s7" type="structure" />
      <biological_entity id="o23701" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="orange" value_original="orange" />
      </biological_entity>
      <relation from="o23700" id="r1954" name="with" negation="false" src="d0_s7" to="o23701" />
    </statement>
    <statement id="d0_s8">
      <text>petals usually purplish rose to red, rarely white, 10–14 mm.</text>
      <biological_entity id="o23702" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually purplish rose" name="coloration" src="d0_s8" to="red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules ovoid, 4–8 mm, sparsely strigose.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 42.</text>
      <biological_entity id="o23703" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23704" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oxalis articulata in the United States commonly has been identified as O. rubra. Oxalis rubra was treated at subspecific rank by A. Lourteig (1982), but subsp. articulata and subsp. rubra have essentially the same native range and occur in similar habitats. Lourteig identified both subspecies in the United States, noting in her key that vestiture is reduced and the sepals are broader in subsp. rubra. Evidence is weak for recognizing more than a single entity. In the Flora of Panama (Lourteig 1980), she recognized only O. articulata, noting that it is naturalized in other parts of America and in the Old World.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places, especially near gardens, lawns, fields, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed places" />
        <character name="habitat" value="near gardens" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–250 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="250" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Calif., Fla., Ga., La., Miss., N.C., Okla., Oreg., S.C., Tex., Va.; South America (Argentina, Brazil, Uruguay); introduced also in Europe, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Uruguay)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>