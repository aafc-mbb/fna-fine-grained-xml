<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">RHAMNUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">alaternus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 193. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus rhamnus;species alaternus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101341</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Italian or evergreen buckthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–6 (–10) m, unarmed.</text>
      <biological_entity id="o10927" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets purplish brown, puberulent.</text>
      <biological_entity id="o10928" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, alternate;</text>
      <biological_entity id="o10929" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 4–10 mm;</text>
      <biological_entity id="o10930" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dull to glossy yellowish green on both surfaces, elliptic to elliptic-obovate, ovate, or ovatelanceolate, 2–4 (–6) cm, distinctly coriaceous, base acute to obtuse, margins sharply serrate to spinulose-serrate, apex acute to subspinulose, both surfaces glabrous except abaxial vein-axils tomentose;</text>
      <biological_entity id="o10931" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s4" to="glossy" />
        <character constraint="on surfaces" constraintid="o10932" is_modifier="false" name="coloration" src="d0_s4" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s4" to="elliptic-obovate ovate or ovatelanceolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="elliptic-obovate ovate or ovatelanceolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="elliptic-obovate ovate or ovatelanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="distinctly" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o10932" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o10933" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o10934" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to spinulose-serrate" value_original="serrate to spinulose-serrate" />
      </biological_entity>
      <biological_entity id="o10935" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subspinulose" value_original="subspinulose" />
      </biological_entity>
      <biological_entity id="o10936" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character constraint="except abaxial vein-axils" constraintid="o10937" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10937" name="vein-axil" name_original="vein-axils" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>secondary-veins (3–) 4–5 pairs, basal pair diverging much more acutely than distal pairs.</text>
      <biological_entity id="o10938" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="much; acutely" name="orientation" src="d0_s5" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10939" name="pair" name_original="pairs" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymes or flowers solitary.</text>
      <biological_entity id="o10940" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o10941" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o10942" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 2–6 mm.</text>
      <biological_entity id="o10943" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sepals 5.</text>
      <biological_entity id="o10944" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Petals 0.</text>
      <biological_entity id="o10945" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Drupes dark red, becoming black, globose, 5–7 mm;</text>
      <biological_entity id="o10946" name="drupe" name_original="drupes" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark red" value_original="dark red" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stones 3.</text>
      <biological_entity id="o10947" name="stone" name_original="stones" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodland edges, fencerows, low areas, stream banks, canyons, arroyos.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodland edges" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="low areas" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="arroyos" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe; introduced also in Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>