<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">cordata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 176. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species cordata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417368</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Heart-leaf noseburn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, 15–20 dm.</text>
      <biological_entity id="o6644" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually decumbent or twining, rarely erect, gray-green to light green, apex flexuous.</text>
      <biological_entity id="o6645" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="twining" value_original="twining" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s1" to="light green" />
      </biological_entity>
      <biological_entity id="o6646" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 15–85 mm;</text>
      <biological_entity id="o6647" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6648" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="85" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to broadly cordate, 4.5–10 (–13) × 3.5–10 cm, base cordate, margins serrate, apex acuminate.</text>
      <biological_entity id="o6649" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6650" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly cordate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6651" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o6652" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6653" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal (often appearing leaf-opposed), glands absent, staminate flowers 20–60 per raceme;</text>
      <biological_entity id="o6654" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o6655" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6656" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per raceme" constraintid="o6657" from="20" name="quantity" src="d0_s4" to="60" />
      </biological_entity>
      <biological_entity id="o6657" name="raceme" name_original="raceme" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>staminate bracts 1.5–2 mm.</text>
      <biological_entity id="o6658" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate 1.5–2.2 mm, persistent base 0.7–1 mm;</text>
      <biological_entity id="o6659" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6660" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 2.5–3 mm in fruit.</text>
      <biological_entity id="o6661" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o6662" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6662" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3, green, 0.7–1 mm;</text>
      <biological_entity id="o6663" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6664" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3, filaments 0.2–0.5 mm.</text>
      <biological_entity id="o6665" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6666" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o6667" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals elliptic to ovate, 1.5–2 mm;</text>
      <biological_entity id="o6668" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6669" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles connate 1/4–1/3 length;</text>
      <biological_entity id="o6670" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6671" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s11" to="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas papillate.</text>
      <biological_entity id="o6672" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6673" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 11–13 mm wide.</text>
      <biological_entity id="o6674" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds dark-brown, 4.3–5.3 mm.</text>
      <biological_entity id="o6675" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="4.3" from_unit="mm" name="some_measurement" src="d0_s14" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Both the morphology and ecology of Tragia cordata make it unique among American members of Tragia. The relatively large, heart-shaped leaves separate it from the other Tragia in the flora area; it is the only twining species of Tragia found in the deciduous forest of the Midwest.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall; fruiting summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich deciduous forests, riverbanks, rocky thickets.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="rich deciduous" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="rocky thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Ky., La., Miss., Mo., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>