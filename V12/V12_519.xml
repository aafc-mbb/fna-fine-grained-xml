<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="(K. H. Thorne &amp; F. J. Smith) J. J. Schenk &amp; L. Hufford" date="2009" rank="species">librina</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>19: 119. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species librina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101822</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mentzelia</taxon_name>
    <taxon_name authority="(Osterhout) J. Darlington" date="unknown" rank="species">multicaulis</taxon_name>
    <taxon_name authority="K. H. Thorne &amp; F. J. Smith" date="1986" rank="variety">librina</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>46: 556, fig. 1. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mentzelia;species multicaulis;variety librina</taxon_hierarchy>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Book Cliffs stickleaf</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, bushlike, with subterranean caudices or rhizomes.</text>
      <biological_entity id="o26538" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushlike" value_original="bushlike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26539" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <biological_entity id="o26540" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o26538" id="r2157" name="with" negation="false" src="d0_s0" to="o26539" />
      <relation from="o26538" id="r2158" name="with" negation="false" src="d0_s0" to="o26540" />
    </statement>
    <statement id="d0_s1">
      <text>Stems multiple, erect, zigzag;</text>
      <biological_entity id="o26541" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distal or along entire stem, distal longest or all ± equal, antrorse, straight to upcurved;</text>
      <biological_entity id="o26542" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="along entire stem" value_original="along entire stem" />
      </biological_entity>
      <biological_entity id="o26543" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o26542" id="r2159" name="along" negation="false" src="d0_s2" to="o26543" />
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="distal" id="o26544" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character name="length" src="d0_s2" value="all" value_original="all" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="upcurved" value_original="upcurved" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 14–26.3 × 5.1–10.8 mm, widest intersinus distance 2.3–5.2 mm;</text>
      <biological_entity id="o26545" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26546" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s4" to="26.3" to_unit="mm" />
        <character char_type="range_value" from="5.1" from_unit="mm" name="width" src="d0_s4" to="10.8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s4" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate to elliptic, margins usually 3-fid, rarely entire or pinnate, teeth or lobes usually 2, rarely 0 or 4, perpendicular to leaf axis, 0.8–2.7 mm;</text>
      <biological_entity id="o26547" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o26548" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity id="o26549" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o26550" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character constraint="to leaf axis" constraintid="o26552" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26551" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character modifier="rarely" name="presence" src="d0_s5" unit="or" value="absent" value_original="absent" />
        <character modifier="rarely" name="quantity" src="d0_s5" unit="or" value="4" value_original="4" />
        <character constraint="to leaf axis" constraintid="o26552" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o26552" name="axis" name_original="axis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal elliptic, base not clasping, margins 3-fid to occasionally pinnate, teeth or lobes usually 2, occasionally 4, perpendicular to leaf axis, 1.5–3.5 mm;</text>
      <biological_entity id="o26553" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o26554" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o26555" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o26556" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="3-fid" name="shape" src="d0_s6" to="occasionally pinnate" />
      </biological_entity>
      <biological_entity id="o26557" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character constraint="to leaf axis" constraintid="o26559" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26558" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character modifier="occasionally" name="quantity" src="d0_s6" value="4" value_original="4" />
        <character constraint="to leaf axis" constraintid="o26559" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o26559" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with simple grappling-hook and complex grappling-hook trichomes, adaxial surface with simple grappling-hook and needlelike trichomes.</text>
      <biological_entity id="o26560" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o26561" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o26562" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="grappling-hook" id="o26563" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26564" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o26565" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o26566" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o26561" id="r2160" name="with" negation="false" src="d0_s7" to="o26562" />
      <relation from="o26561" id="r2161" name="with" negation="false" src="d0_s7" to="o26563" />
      <relation from="o26564" id="r2162" name="with" negation="false" src="d0_s7" to="o26565" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins usually entire, rarely pinnate.</text>
      <biological_entity id="o26567" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o26568" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s8" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 8.3–13.6 × 2.6–4.8 (–6.2) mm, apex acute, glabrous abaxially;</text>
      <biological_entity id="o26569" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26570" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="8.3" from_unit="mm" name="length" src="d0_s9" to="13.6" to_unit="mm" />
        <character char_type="range_value" from="4.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s9" to="4.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26571" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments broadly spatulate, strongly clawed, 6–8 (–9.1) × 2.3–4.4 mm, with anthers, second whorl with anthers;</text>
      <biological_entity id="o26572" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26573" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o26574" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o26575" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="9.1" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s10" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26576" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o26577" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o26578" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o26575" id="r2163" name="with" negation="false" src="d0_s10" to="o26576" />
      <relation from="o26577" id="r2164" name="with" negation="false" src="d0_s10" to="o26578" />
    </statement>
    <statement id="d0_s11">
      <text>anthers straight after dehiscence, epidermis smooth;</text>
      <biological_entity id="o26579" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26580" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o26581" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5–7.9 mm.</text>
      <biological_entity id="o26582" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26583" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped, 4–7.3 × 3.3–5.8 mm, base rounded, not longitudinally ridged.</text>
      <biological_entity id="o26584" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="7.3" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="width" src="d0_s13" to="5.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26585" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls straight, papillae 4–5 per cell.</text>
      <biological_entity id="o26586" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o26587" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o26588" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o26589" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o26590" from="4" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <biological_entity id="o26590" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Mentzelia librina in narrowly distributed along the border between Carbon and Emery counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Barren gray soils, steep shale slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren gray soils" />
        <character name="habitat" value="steep shale slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>