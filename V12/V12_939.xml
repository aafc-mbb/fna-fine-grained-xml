<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
    <other_info_on_meta type="illustration_page">75</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Richard ex Brongniart" date="1826" rank="genus">COLUBRINA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="1850" rank="species">texensis</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 169. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus colubrina;species texensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101382</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhamnus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1838" rank="species">texensis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 263. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus rhamnus;species texensis</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Texan hog-plum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, erect, 1–2 (–2.8) m.</text>
      <biological_entity id="o763" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2.8" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="2.8" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems zigzag, white tomentose-sericeous, becoming glabrate.</text>
      <biological_entity id="o765" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose-sericeous" value_original="tomentose-sericeous" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, sometimes fascicled on short-shoots;</text>
      <biological_entity id="o766" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="on short-shoots" constraintid="o767" is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o767" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–4 mm;</text>
      <biological_entity id="o768" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to elliptic, oblong-obovate, or obovate, 1–3 (–4) cm, subcoriaceous, base rounded to subcordate, margins shallowly serrate, teeth 10–20 per side, apex rounded, often apiculate, abaxial surface loosely sericeous, adaxial glabrate;</text>
      <biological_entity id="o769" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic oblong-obovate or obovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic oblong-obovate or obovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic oblong-obovate or obovate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o770" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o771" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o772" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o773" from="10" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
      <biological_entity id="o773" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o774" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o775" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pinnately veined, secondary-veins (2–) 3–4 pairs, arcuate, basal pair prominent.</text>
      <biological_entity constraint="adaxial" id="o776" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o777" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="course_or_shape" src="d0_s5" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences fascicles, 2–4 (–7) -flowered, or flowers solitary;</text>
      <biological_entity id="o778" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-4(-7)-flowered" value_original="2-4(-7)-flowered" />
      </biological_entity>
      <biological_entity id="o779" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles absent;</text>
    </statement>
    <statement id="d0_s8">
      <text>fruiting pedicels 5–13 mm.</text>
      <biological_entity id="o780" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o781" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o780" id="r69" name="fruiting" negation="false" src="d0_s8" to="o781" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules 6–9 mm.</text>
      <biological_entity id="o782" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Colubrina texensis is widespread in central and southern Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fencerows, roadsides, disturbed sites, clay banks, shell ridges, loose sand, sandy loam, rocky limestone slopes and crevices, gravel hills, stream banks, alluvial terraces, gravelly flood plains, shrub-grasslands, mesquite shrublands, oak-mesquite, oak-juniper, and mesquite-hackberry woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="clay banks" />
        <character name="habitat" value="shell ridges" />
        <character name="habitat" value="loose sand" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="rocky limestone slopes" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="gravel hills" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="alluvial terraces" />
        <character name="habitat" value="gravelly flood plains" />
        <character name="habitat" value="shrub-grasslands" />
        <character name="habitat" value="mesquite shrublands" />
        <character name="habitat" value="oak-mesquite" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="mesquite-hackberry woodlands" modifier="and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>