<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <taxon_name authority="Pax &amp; K. Hoffmann in H. G. A. Engler" date="1919" rank="species">glanduligera</taxon_name>
    <place_of_publication>
      <publication_title>Pflanzenr.</publication_title>
      <place_in_publication>68[IV,147]: 55. 1919</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species glanduligera</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101924</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Brush or sticky noseburn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or vines, 3–10 dm.</text>
      <biological_entity id="o21288" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems trailing or twining, dark green, apex flexuous.</text>
      <biological_entity id="o21290" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="twining" value_original="twining" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark green" value_original="dark green" />
      </biological_entity>
      <biological_entity id="o21291" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 6–22 mm;</text>
      <biological_entity id="o21292" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21293" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly ovate to lanceolate, 2.5–4 × 1.5–2 cm, base shallowly cordate to truncate, margins serrate to crenate, apex acute to acuminate.</text>
      <biological_entity id="o21294" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21295" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21296" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate to truncate" value_original="cordate to truncate" />
      </biological_entity>
      <biological_entity id="o21297" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s3" to="crenate" />
      </biological_entity>
      <biological_entity id="o21298" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal (often appearing leaf-opposed), glands stipitate, prominent throughout, staminate flowers 10–30 per raceme;</text>
      <biological_entity id="o21299" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21300" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="throughout" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o21301" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per raceme" constraintid="o21302" from="10" name="quantity" src="d0_s4" to="30" />
      </biological_entity>
      <biological_entity id="o21302" name="raceme" name_original="raceme" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>staminate bracts 0.5–1.5 mm.</text>
      <biological_entity id="o21303" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate 1–2 mm, persistent base 0.3–0.7 mm;</text>
      <biological_entity id="o21304" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21305" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 3–7 mm in fruit.</text>
      <biological_entity id="o21306" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o21307" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21307" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3, green, 0.7–1.2 mm;</text>
      <biological_entity id="o21308" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21309" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3, filaments 0.2–0.4 mm.</text>
      <biological_entity id="o21310" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21311" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21312" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals lanceolate, 0.7–1.5 mm;</text>
      <biological_entity id="o21313" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21314" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles connate 1/3 length;</text>
      <biological_entity id="o21315" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21316" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas smooth to undulate.</text>
      <biological_entity id="o21317" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21318" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s12" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–5 mm wide.</text>
      <biological_entity id="o21319" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds dark-brown to black, 1.9–2.2 mm.</text>
      <biological_entity id="o21320" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s14" to="black" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Southern Texas is the northernmost distribution of Tragia glanduligera. In Mexico, it is found in tropical deciduous forests in Campeche, Nuevo León, Tabasco, Veracruz, and Yucatan. This species and T. jonesii are the only species in the flora area with stipitate glands on the inflorescence. Tragia glanduligera differs from T. jonesii by its leaf blade margins with 10–15 smaller teeth per side, shorter staminate pedicels, and truncate to weakly cordate leaf blade bases.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring; fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy limestone soils, abandoned home sites and mesquite scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="sandy limestone soils" />
        <character name="habitat" value="abandoned home sites" />
        <character name="habitat" value="mesquite scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–80 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="80" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; s, e Mexico; Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="s" establishment_means="native" />
        <character name="distribution" value="e Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>