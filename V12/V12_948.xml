<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">269</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann" date="1845" rank="species">geyeri</taxon_name>
    <taxon_name authority="Warnock &amp; M. C. Johnston" date="1969" rank="variety">wheeleriana</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>14: 128. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species geyeri;variety wheeleriana</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101575</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Engelmann &amp; A. Gray) Small" date="unknown" rank="species">geyeri</taxon_name>
    <taxon_name authority="(Warnock &amp; M. C. Johnston) Mayfield" date="unknown" rank="variety">wheeleriana</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species geyeri;variety wheeleriana</taxon_hierarchy>
  </taxon_identification>
  <number>41b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Involucral gland appendages absent or rudimentary.</text>
      <biological_entity constraint="gland" id="o3844" name="appendage" name_original="appendages" src="d0_s0" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s0" value="involucral" value_original="involucral" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s0" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Staminate flowers 10–20.</text>
      <biological_entity id="o3845" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s1" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Capsules 1.5–2 × 2–3 mm.</text>
      <biological_entity id="o3846" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seeds 1.6–1.7 × 0.9–1.2 mm. 2n = 12.</text>
      <biological_entity id="o3847" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s3" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s3" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3848" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety wheeleriana is known in the flora area from south-central New Mexico and western trans-Pecos Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting midsummer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="midsummer" />
        <character name="fruiting time" char_type="range_value" to="early fall" from="midsummer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand barrens, riverbanks, disturbed sandy or gravelly areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand barrens" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="sandy" modifier="disturbed" />
        <character name="habitat" value="gravelly areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>