<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">461</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Dumortier" date="unknown" rank="family">NYSSACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">NYSSA</taxon_name>
    <taxon_name authority="Small" date="1927" rank="species">ursina</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>27: 92. 1927</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyssaceae;genus nyssa;species ursina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101789</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nyssa</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">biflora</taxon_name>
    <taxon_name authority="(Small) D. B. Ward" date="unknown" rank="variety">ursina</taxon_name>
    <taxon_hierarchy>genus nyssa;species biflora;variety ursina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">N.</taxon_name>
    <taxon_name authority="Marshall" date="unknown" rank="species">sylvatica</taxon_name>
    <taxon_name authority="(Small) J. Wen &amp; Stuessy" date="unknown" rank="variety">ursina</taxon_name>
    <taxon_hierarchy>genus n.;species sylvatica;variety ursina</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Bear  or Apalachicola tupelo</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 2–5 m, crown typically intricately branched;</text>
      <biological_entity id="o12381" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o12383" name="crown" name_original="crown" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="typically intricately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark irregularly fissured;</text>
      <biological_entity id="o12384" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="relief" src="d0_s1" value="fissured" value_original="fissured" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs usually glabrous, rarely puberulent.</text>
      <biological_entity id="o12385" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole (4–) 5–9 mm;</text>
      <biological_entity id="o12386" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12387" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic to oblanceolate, rarely to ovate, 3–7 × 1–2 cm, coriaceous, base cuneate to rounded, margins entire, apex obtuse, abaxial surface glabrous or puberulent (primarily along veins), adaxial surface glabrous.</text>
      <biological_entity id="o12388" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12389" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o12390" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" is_modifier="true" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o12391" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" is_modifier="true" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o12392" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" is_modifier="true" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12393" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12394" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12389" id="r1031" modifier="rarely" name="to" negation="false" src="d0_s4" to="o12390" />
      <relation from="o12389" id="r1032" modifier="rarely" name="to" negation="false" src="d0_s4" to="o12391" />
      <relation from="o12389" id="r1033" modifier="rarely" name="to" negation="false" src="d0_s4" to="o12392" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: peduncle 3.2–5.5 cm, sparsely hairy or glabrous;</text>
      <biological_entity id="o12395" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o12396" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.2" from_unit="cm" name="some_measurement" src="d0_s5" to="5.5" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminate (1–) 2–5-flowered, pistillate and bisexual 1–2-flowered.</text>
      <biological_entity id="o12397" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="(1-)2-5-flowered" value_original="(1-)2-5-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate pedicels present.</text>
      <biological_entity id="o12398" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: ovary glabrous.</text>
      <biological_entity id="o12399" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12400" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Drupes usually black, rarely blue, glaucous, globose, 7–11 mm, smooth;</text>
      <biological_entity id="o12401" name="drupe" name_original="drupes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stone 6–8 mm, with several low, rounded longitudinal ridges.</text>
      <biological_entity id="o12402" name="stone" name_original="stone" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12403" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="several" value_original="several" />
        <character is_modifier="true" name="position" src="d0_s10" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o12402" id="r1034" name="with" negation="false" src="d0_s10" to="o12403" />
    </statement>
  </description>
  <discussion>Nyssa ursina is limited to six counties in the panhandle region of Florida. It occurs together with N. biflora throughout its limited range, which supports recognizing it as a distinct species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open savannas, depressions in flatwoods.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open savannas" />
        <character name="habitat" value="depressions" constraint="in flatwoods" />
        <character name="habitat" value="flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–70 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="70" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>