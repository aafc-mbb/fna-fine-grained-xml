<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1838" rank="species">oliganthus</taxon_name>
    <taxon_name authority="(Parry) Trelease ex Jepson" date="1925" rank="variety">orcuttii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>621. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species oliganthus;variety orcuttii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101405</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="Parry" date="1889" rank="species">orcuttii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Davenport Acad. Nat. Sci.</publication_title>
      <place_in_publication>5: 194. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus ceanothus;species orcuttii</taxon_hierarchy>
  </taxon_identification>
  <number>16b.</number>
  <other_name type="common_name">Orcutt’s ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branchlets ± flexible, puberulent to villosulous.</text>
      <biological_entity id="o10171" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o10172" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s0" value="pliable" value_original="flexible" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s0" to="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate, elliptic, or elliptic-oblong, adaxial surface sparsely villosulous.</text>
      <biological_entity id="o10173" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic-oblong" value_original="elliptic-oblong" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10174" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals, petals, and nectary blue;</text>
      <biological_entity id="o10175" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o10176" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o10177" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o10178" name="nectary" name_original="nectary" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nectary and ovary pilosulous.</text>
      <biological_entity id="o10179" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o10180" name="nectary" name_original="nectary" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
      <biological_entity id="o10181" name="ovary" name_original="ovary" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsules glabrescent;</text>
      <biological_entity id="o10182" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>valves rugose, weakly crested.</text>
      <biological_entity id="o10183" name="valve" name_original="valves" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety orcuttii occurs in the Peninsular Ranges of southern California and northern Baja California, Mexico. The pilosulous nectary and ovary are unique in Ceanothus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open slopes and ridges, chaparral, pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="forests" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>