<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="species">misera</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Voy. Sulphur,</publication_title>
      <place_in_publication>51. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species misera;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101542</other_info_on_name>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Cliff spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, soft wooded, with woody rootstock.</text>
      <biological_entity id="o32533" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="wooded" value_original="wooded" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o32534" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o32533" id="r2661" name="with" negation="false" src="d0_s0" to="o32534" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, often gnarled and scraggly, branched, with conspicuous knobby short-shoots, 70–150 cm, puberulent-tomentose, bark grayish red to light gray.</text>
      <biological_entity id="o32535" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s1" value="gnarled" value_original="gnarled" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="scraggly" value_original="scraggly" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="150" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent-tomentose" value_original="puberulent-tomentose" />
      </biological_entity>
      <biological_entity id="o32536" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="shape" src="d0_s1" value="knobby" value_original="knobby" />
      </biological_entity>
      <biological_entity id="o32537" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="grayish red" name="coloration" src="d0_s1" to="light gray" />
      </biological_entity>
      <relation from="o32535" id="r2662" name="with" negation="false" src="d0_s1" to="o32536" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, well spaced on long-shoots or fasciculate on short-shoots;</text>
      <biological_entity id="o32538" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character constraint="on " constraintid="o32540" is_modifier="false" modifier="well" name="arrangement" src="d0_s2" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o32539" name="long-shoot" name_original="long-shoots" src="d0_s2" type="structure" />
      <biological_entity id="o32540" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s2" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules 0.6–1.1 mm;</text>
      <biological_entity id="o32541" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s3" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 4–12 (–19) mm, slender, puberulent to shortly pilose;</text>
      <biological_entity id="o32542" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s4" to="shortly pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong, ovate, orbiculate, elliptic, or obovate, 6–24 × 5–21 mm, base rounded to cuneate, margins entire, apex acute to obtuse, surfaces puberulent-tomentose;</text>
      <biological_entity id="o32543" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="24" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32544" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o32545" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o32546" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation conspicuous.</text>
      <biological_entity id="o32547" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent-tomentose" value_original="puberulent-tomentose" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia usually solitary on short-shoots, peduncle 1.8–10.5 mm, puberulent-tomentose.</text>
      <biological_entity id="o32548" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="on short-shoots" constraintid="o32549" is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o32549" name="short-shoot" name_original="short-shoots" src="d0_s7" type="structure" />
      <biological_entity id="o32550" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s7" to="10.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent-tomentose" value_original="puberulent-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucre campanulate, 1.4–3.8 × 2.1–4.4 mm, puberulent-tomentose;</text>
      <biological_entity id="o32551" name="involucre" name_original="involucre" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s8" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="width" src="d0_s8" to="4.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent-tomentose" value_original="puberulent-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glands 5, yellow to reddish, oblong to reniform, 0.7–1.3 × 1.1–2.6 mm;</text>
      <biological_entity id="o32552" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="reddish" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="reniform" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s9" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>appendages green-yellow to yellowish or whitish, oblong to transversely oblong, 0.6–1.9 × 1.3–3.8 mm, crenulate to erose.</text>
      <biological_entity id="o32553" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character char_type="range_value" from="green-yellow" name="coloration" src="d0_s10" to="yellowish or whitish" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="transversely oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s10" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers 40–50.</text>
      <biological_entity id="o32554" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s11" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: ovary glabrous or puberulent;</text>
      <biological_entity id="o32555" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o32556" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 1.6–2.7 mm, 2-fid at apex.</text>
      <biological_entity id="o32557" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o32558" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s13" to="2.7" to_unit="mm" />
        <character constraint="at apex" constraintid="o32559" is_modifier="false" name="shape" src="d0_s13" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o32559" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules oblate, 4.6–5.1 × 6.1–6.7 mm, usually glabrous or glabrescent, occasionally puberulent;</text>
      <biological_entity id="o32560" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="length" src="d0_s14" to="5.1" to_unit="mm" />
        <character char_type="range_value" from="6.1" from_unit="mm" name="width" src="d0_s14" to="6.7" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>columella 2.8–3.6 mm.</text>
      <biological_entity id="o32561" name="columella" name_original="columella" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s15" to="3.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds grayish, subglobose to ovoid, rounded in cross-section, 2.7–3.3 × 2.5–2.8 mm, foveolate;</text>
      <biological_entity id="o32562" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s16" to="ovoid" />
        <character constraint="in cross-section" constraintid="o32563" is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" notes="" src="d0_s16" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" notes="" src="d0_s16" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="foveolate" value_original="foveolate" />
      </biological_entity>
      <biological_entity id="o32563" name="cross-section" name_original="cross-section" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>caruncle absent.</text>
      <biological_entity id="o32564" name="caruncle" name_original="caruncle" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia misera is relatively infrequent within the flora area, known primarily from coastal southern California and the Channel Islands (although a relictual inland population occurs in the Little San Bernardino Mountains). The species has been considered worthy of conservation, but appears to be under little threat, especially in Mexico where it is frequent and often locally abundant.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round (but most prolific after winter rains).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="but most prolific" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" modifier="rains" to="winter" from="winter" />
        <character name="fruiting time" char_type="range_value" modifier="but most prolific" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="rains" to="winter" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils, sometimes in crevices of vertical cliff faces, coastal scrub, maritime desert scrub, arid desert scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="crevices" modifier="sometimes" constraint="of vertical cliff faces , coastal scrub , maritime desert scrub ," />
        <character name="habitat" value="coastal scrub" modifier="faces" />
        <character name="habitat" value="maritime desert scrub" />
        <character name="habitat" value="desert scrub" modifier="arid" />
        <character name="habitat" value="cliff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California, Baja California Sur, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>