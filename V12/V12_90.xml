<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">114</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PARNASSIA</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="species">grandifolia</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>1: 320. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus parnassia;species grandifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416941</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with caudices.</text>
      <biological_entity id="o22664" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22665" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o22664" id="r1863" name="with" negation="false" src="d0_s0" to="o22665" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 12–70 cm.</text>
      <biological_entity id="o22666" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal in rosettes;</text>
      <biological_entity id="o22667" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o22668" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22669" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <relation from="o22668" id="r1864" name="in" negation="false" src="d0_s2" to="o22669" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 3–15 cm;</text>
      <biological_entity id="o22670" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22671" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (of larger leaves) oblong-ovate to suborbiculate, 25–80 × 20–70 mm, longer than to ca. as long as wide, base rounded to subcordate, apex obtuse;</text>
      <biological_entity id="o22672" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22673" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="70" to_unit="mm" />
        <character constraint="as-long-as base, apex" constraintid="o22674, o22675" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o22674" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o22675" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline on proximal half of stem or absent.</text>
      <biological_entity id="o22676" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o22677" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22678" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o22679" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o22677" id="r1865" name="on" negation="false" src="d0_s5" to="o22678" />
      <relation from="o22678" id="r1866" name="part_of" negation="false" src="d0_s5" to="o22679" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals reflexed in fruit, oblong to ovate, 2.5–5 mm, margins hyaline, 0.2–0.5 mm wide, entire, apex obtuse;</text>
      <biological_entity id="o22680" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22681" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o22682" is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22682" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o22683" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22684" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 7–11-veined, oblong to ovate, 15–22 × 6–10 mm, length 3–4 times sepals, base cuneate to rounded, margins entire or undulate;</text>
      <biological_entity id="o22685" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22686" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="7-11-veined" value_original="7-11-veined" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="22" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o22687" is_modifier="false" name="length" src="d0_s7" value="3-4 times sepals" value_original="3-4 times sepals" />
      </biological_entity>
      <biological_entity id="o22687" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o22688" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
      <biological_entity id="o22689" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 7–9 mm;</text>
      <biological_entity id="o22690" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22691" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 2–3 mm;</text>
      <biological_entity id="o22692" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22693" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 3-fid almost to base, gland-tipped, 10–16 mm, longer than stamens, apical glands elliptic to subglobose, 0.4–0.6 mm;</text>
      <biological_entity id="o22694" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22695" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character constraint="to base" constraintid="o22696" is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character constraint="than stamens" constraintid="o22697" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o22696" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o22697" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity constraint="apical" id="o22698" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="subglobose" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary green, sometimes whitish at base.</text>
      <biological_entity id="o22699" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22700" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character constraint="at base" constraintid="o22701" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o22701" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 10–15 mm. 2n = 32.</text>
      <biological_entity id="o22702" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22703" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Parnassia grandifolia is uncommon throughout most of its range; it is listed as endangered in Florida and Kentucky, threatened in North Carolina, and of special concern in Tennessee.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, calcareous rocks, shores, meadows, fens.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="calcareous rocks" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ky., La., Miss., Mo., N.C., Okla., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>