<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Boissier in A. P. de Candolle and A. L. P. P. de Candolle" date="1862" rank="section">Nummulariopsis</taxon_name>
    <taxon_name authority="Torrey ex Chapman" date="1860" rank="species">inundata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">inundata</taxon_name>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section nummulariopsis;species inundata;variety inundata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101652</other_info_on_name>
  </taxon_identification>
  <number>127a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades usually linear to narrowly elliptic or narrowly lanceolate, rarely oblanceolate, (30–) 40–60 (–115) × (3–) 4–14 (–15) mm, length usually 5–10 (–25) times width, apex obtuse to short-acute.</text>
      <biological_entity id="o10994" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s0" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s0" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s0" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="115" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s0" to="60" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s0" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s0" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="l_w_ratio" src="d0_s0" value="5-10(-25)" value_original="5-10(-25)" />
      </biological_entity>
      <biological_entity id="o10995" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s0" to="short-acute" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety inundata  is found in a variety of habitats from northeastern Florida and southern Georgia west to southeastern Mississippi (Jackson County).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy meadows, prairies, seepage-bogs, pine savannas, xeric oak woods, pine-oak scrub, often in seasonally inundated soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy meadows" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="seepage-bogs" />
        <character name="habitat" value="savannas" modifier="pine" />
        <character name="habitat" value="xeric oak woods" />
        <character name="habitat" value="pine-oak scrub" />
        <character name="habitat" value="inundated soils" modifier="seasonally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>