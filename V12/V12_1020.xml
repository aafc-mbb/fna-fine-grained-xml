<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="L. C. Wheeler" date="1941" rank="species">theriaca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">theriaca</taxon_name>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species theriaca;variety theriaca</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101616</other_info_on_name>
  </taxon_identification>
  <number>84a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–30 cm.</text>
      <biological_entity id="o16268" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 2–5.5 × 1–3.5 mm.</text>
      <biological_entity id="o16269" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cyathia solitary at distal nodes of stem;</text>
      <biological_entity id="o16270" name="cyathium" name_original="cyathia" src="d0_s2" type="structure">
        <character constraint="at distal nodes" constraintid="o16271" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16271" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o16272" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o16271" id="r1356" name="part_of" negation="false" src="d0_s2" to="o16272" />
    </statement>
    <statement id="d0_s3">
      <text>peduncle 0.3–0.7 mm.</text>
      <biological_entity id="o16273" name="peduncle" name_original="peduncle" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucre usually turbinate-campanulate, occasionally suburceolate, 1.1–1.5 × 0.9–1.3 mm;</text>
      <biological_entity id="o16274" name="involucre" name_original="involucre" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="suburceolate" value_original="suburceolate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s4" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>glands sessile, subcircular to slightly elliptic, 0.2–0.5 × 0.3–0.7 mm;</text>
      <biological_entity id="o16275" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="subcircular" name="shape" src="d0_s5" to="slightly elliptic" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s5" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>appendages absent.</text>
      <biological_entity id="o16276" name="appendage" name_original="appendages" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers 15–36.</text>
      <biological_entity id="o16277" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 1.1–1.6 × 1.5–1.8 mm.</text>
      <biological_entity id="o16278" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s8" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds with (2–) 3 (–4) prominent transverse ridges.</text>
      <biological_entity id="o16279" name="seed" name_original="seeds" src="d0_s9" type="structure" />
      <biological_entity id="o16280" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s9" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s9" to="4" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o16279" id="r1357" name="with" negation="false" src="d0_s9" to="o16280" />
    </statement>
  </description>
  <discussion>Variety theriaca occurs in Brewster and Presidio counties in trans-Pecos Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, mostly in calcareous soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="calcareous soils" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>