<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Tieghem" date="unknown" rank="family">SIMMONDSIACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1844" rank="genus">SIMMONDSIA</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>3: 400, plate 16. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family simmondsiaceae;genus simmondsia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Thomas Williams Simmonds, d. 1804, English naturalist</other_info_on_name>
    <other_info_on_name type="fna_id">130381</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Jojoba</other_name>
  <other_name type="common_name">goatnut</other_name>
  <other_name type="common_name">pignut</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, indumentum of slender, mostly appressed, simple hairs.</text>
      <biological_entity id="o26245" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o26246" name="indumentum" name_original="indumentum" src="d0_s0" type="structure" />
      <biological_entity id="o26247" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="true" modifier="mostly" name="fixation_or_orientation" src="d0_s0" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o26246" id="r2130" name="part_of" negation="false" src="d0_s0" to="o26247" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves jointed near base;</text>
      <biological_entity id="o26248" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="near base" constraintid="o26249" is_modifier="false" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
      </biological_entity>
      <biological_entity id="o26249" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade secondary-veins steeply ascending from near base, usually basally distinct, distally obscure.</text>
      <biological_entity constraint="blade" id="o26250" name="secondary-vein" name_original="secondary-veins" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o26251" is_modifier="false" modifier="steeply" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually basally" name="fusion" notes="" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="distally" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o26251" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: staminate terminal, often pseudoaxillary (terminal but overtopped by lateral branch and appearing axillary), pedunculate, 10–20-flowered;</text>
      <biological_entity id="o26252" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s3" value="pseudoaxillary" value_original="pseudoaxillary" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="10-20-flowered" value_original="10-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate axillary, pedunculate, 1 (–3) -flowered;</text>
      <biological_entity id="o26253" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1(-3)-flowered" value_original="1(-3)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts present.</text>
      <biological_entity id="o26254" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o26255" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels present or absent.</text>
      <biological_entity id="o26256" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: sepals oblanceolate.</text>
      <biological_entity id="o26257" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26258" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pistillate flowers: sepals persistent, spreading, triangular-ovate, enlarged in fruit.</text>
      <biological_entity id="o26259" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26260" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-ovate" value_original="triangular-ovate" />
        <character constraint="in fruit" constraintid="o26261" is_modifier="false" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o26261" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules nutlike, ovoid or ellipsoid, usually ± obtusely 3-angled;</text>
      <biological_entity id="o26262" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="usually more or less obtusely" name="shape" src="d0_s9" value="3-angled" value_original="3-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pericarp tough-leathery.</text>
      <biological_entity id="o26263" name="pericarp" name_original="pericarp" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="tough-leathery" value_original="tough-leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds ovoid.</text>
    </statement>
    <statement id="d0_s12">
      <text>x = 13.</text>
      <biological_entity id="o26264" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o26265" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico; desert and semidesert areas; introduced in other similar habitats in North America, South America, Asia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
        <character name="distribution" value="desert and semidesert areas" establishment_means="native" />
        <character name="distribution" value="in other similar habitats in North America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>