<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jinshuang Ma</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HIPPOCRATEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1191. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 498. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus hippocratea</taxon_hierarchy>
    <other_info_on_name type="etymology">For Hippocrates, ca. 460–370 BC, Greek physician</other_info_on_name>
    <other_info_on_name type="fna_id">115505</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, clambering.</text>
      <biological_entity id="o2711" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets terete to 4-angled.</text>
      <biological_entity id="o2712" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s1" to="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite;</text>
      <biological_entity id="o2713" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present;</text>
      <biological_entity id="o2714" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o2715" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins crenate-serrate;</text>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o2716" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, cymes.</text>
      <biological_entity id="o2717" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2718" name="cyme" name_original="cymes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o2719" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o2720" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o2721" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium absent;</text>
      <biological_entity id="o2722" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, distinct;</text>
      <biological_entity id="o2723" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, pale-yellow to white;</text>
      <biological_entity id="o2724" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s12" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary extrastaminal, annular, fleshy;</text>
      <biological_entity id="o2725" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="extrastaminal" value_original="extrastaminal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="annular" value_original="annular" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 3, adnate to nectary margin;</text>
      <biological_entity id="o2726" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character constraint="to nectary margin" constraintid="o2727" is_modifier="false" name="fusion" src="d0_s14" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="nectary" id="o2727" name="margin" name_original="margin" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes 0;</text>
      <biological_entity id="o2728" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o2729" name="pistil" name_original="pistil" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary superior, immersed in and adnate to nectary, 3-locular, placentation axile;</text>
      <biological_entity id="o2730" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="superior" value_original="superior" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="immersed" value_original="immersed" />
        <character constraint="to nectary" constraintid="o2731" is_modifier="false" name="fusion" src="d0_s17" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" notes="" src="d0_s17" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="axile" value_original="axile" />
      </biological_entity>
      <biological_entity id="o2731" name="nectary" name_original="nectary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1;</text>
      <biological_entity id="o2732" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 3;</text>
      <biological_entity id="o2733" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 6–8 per locule.</text>
      <biological_entity id="o2734" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o2735" from="6" name="quantity" src="d0_s20" to="8" />
      </biological_entity>
      <biological_entity id="o2735" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, 3-locular, deeply 3-parted longitudinally, segments obovate-elliptic or narrowly elliptic, apex not beaked.</text>
      <biological_entity constraint="fruits" id="o2736" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s21" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="deeply; longitudinally" name="shape" src="d0_s21" value="3-parted" value_original="3-parted" />
      </biological_entity>
      <biological_entity id="o2737" name="segment" name_original="segments" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s21" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o2738" name="apex" name_original="apex" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s21" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 5–6 per locule, ellipsoid, winged in proximal 1/2;</text>
      <biological_entity id="o2739" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o2740" from="5" name="quantity" src="d0_s22" to="6" />
        <character is_modifier="false" name="shape" notes="" src="d0_s22" value="ellipsoid" value_original="ellipsoid" />
        <character constraint="in proximal 1/2" constraintid="o2741" is_modifier="false" name="architecture" src="d0_s22" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o2740" name="locule" name_original="locule" src="d0_s22" type="structure" />
      <biological_entity constraint="proximal" id="o2741" name="1/2" name_original="1/2" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>aril absent.</text>
      <biological_entity id="o2742" name="aril" name_original="aril" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Smith, A. C. 1940. The American species of Hippocrateaceae. Brittonia 3: 356–367.</reference>
  </references>
  
</bio:treatment>