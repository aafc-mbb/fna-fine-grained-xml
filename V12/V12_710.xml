<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Nancy R. Morin</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_hierarchy>family linaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101529</other_info_on_name>
  </taxon_identification>
  <number>103.</number>
  <other_name type="common_name">Flax Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs [shrubs, trees, vines], annual, biennial, or perennial.</text>
      <biological_entity id="o19652" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, opposite, or whorled, simple;</text>
      <biological_entity id="o19654" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules absent or present as small, dark, spheric glands;</text>
      <biological_entity id="o19655" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character constraint="as glands" constraintid="o19656" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19656" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s2" value="spheric" value_original="spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole usually absent, rarely present;</text>
      <biological_entity id="o19657" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire, serrate, or denticulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o19658" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes, panicles, or cymes (rarely thyrses or corymbs in Linum) [spikes].</text>
      <biological_entity id="o19659" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o19660" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o19661" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o19662" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o19663" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o19664" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o19665" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium absent;</text>
      <biological_entity id="o19666" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4–5, connate basally [distinct];</text>
      <biological_entity id="o19667" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4–5, distinct or coherent basally, imbricate or convolute, bases sometimes with appendages;</text>
      <biological_entity id="o19668" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s11" value="coherent" value_original="coherent" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity id="o19669" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o19670" name="appendage" name_original="appendages" src="d0_s11" type="structure" />
      <relation from="o19669" id="r1626" name="with" negation="false" src="d0_s11" to="o19670" />
    </statement>
    <statement id="d0_s12">
      <text>nectary extrastaminal;</text>
      <biological_entity id="o19671" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="extrastaminal" value_original="extrastaminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 4–5 [10], connate basally, filament tube and petal bases adherent or adnate [free];</text>
      <biological_entity id="o19672" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="atypical_quantity" src="d0_s13" value="10" value_original="10" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o19673" name="filament" name_original="filament" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o19674" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="petal" id="o19675" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers dehiscing by longitudinal slits;</text>
      <biological_entity id="o19676" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="by slits" constraintid="o19677" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o19677" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistil 1, 2–5-carpellate, ovary superior, 4–5-locular, placentation axile or apical-axile;</text>
      <biological_entity id="o19678" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-5-carpellate" value_original="2-5-carpellate" />
      </biological_entity>
      <biological_entity id="o19679" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="4-5-locular" value_original="4-5-locular" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="placentation" value_original="placentation" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="axile" value_original="axile" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="apical-axile" value_original="apical-axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 2 per locule, anatropous;</text>
      <biological_entity id="o19680" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character constraint="per locule" constraintid="o19681" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s16" value="anatropous" value_original="anatropous" />
      </biological_entity>
      <biological_entity id="o19681" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 2–5, distinct or partly connate;</text>
      <biological_entity id="o19682" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="partly" name="fusion" src="d0_s17" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 2–5.</text>
      <biological_entity id="o19683" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s18" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsules, dehiscence septicidal, or indehiscent or schizocarps breaking into 4 nutlets (Sclerolinon).</text>
      <biological_entity constraint="fruits" id="o19684" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o19685" name="schizocarp" name_original="schizocarps" src="d0_s19" type="structure" />
      <biological_entity id="o19686" name="nutlet" name_original="nutlets" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="4" value_original="4" />
      </biological_entity>
      <relation from="o19685" id="r1627" name="breaking into" negation="false" src="d0_s19" to="o19686" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds 2 per locule, seed-coat often mucilaginous.</text>
      <biological_entity id="o19687" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character constraint="per locule" constraintid="o19688" name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o19688" name="locule" name_original="locule" src="d0_s20" type="structure" />
      <biological_entity id="o19689" name="seed-coat" name_original="seed-coat" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="often" name="coating" src="d0_s20" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 10–14, species ca. 260 (4 genera, 52 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Bermuda, Central America, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Two subfamilies are generally recognized in Linaceae, the mostly herbaceous, temperate Linoideae Arnott (8 genera, ca. 240 species), in which all the genera in the flora area are placed, and the woody, mostly tropical Hugonoideae Reveal. Based on molecular phylogenetic analysis, J. R. McDill et al. (2009) concluded that Linaceae is a monophyletic group, as is Linoideae.</discussion>
  <discussion>According to J. R. McDill (2009), Cliococca Babington, Hesperolinon, and Sclerolinon are nested within Linum sect. Linopsis, and collectively these are sister to Radiola; Hesperolinon and Sclerolinon are most closely related to Mexican and Central American species of Linum. McDill et al. (2009) noted that the relationships within this clade are not well-enough resolved or supported to warrant nomenclatural changes; McDill (2009) came to the same conclusion based on a much wider sample of species. The current generic circumscriptions are maintained here.</discussion>
  <references>
    <reference>McDill, J. R. 2009. Molecular Phylogenetic Studies in the Linaceae and Linum, with Implications for Their Systematics and Historical Biogeography. Ph.D. dissertation. University of Texas.</reference>
    <reference>McDill, J. R. et al. 2009. The phylogeny of Linum and Linaceae subfamily Linoideae, with implications for their systematics, biogeography, and evolution of heterostyly. Syst. Bot. 34: 386–405.</reference>
    <reference>McDill, J. R. and B. B. Simpson. 2011. Molecular phylogenetics of Linaceae with complete generic sampling and data from two plastid genes. Bot. J. Linn Soc. 165: 64–83.</reference>
    <reference>Rogers, C. M. 1975. Relationships of Hesperolinon and Linum (Linaceae). Madroño 23: 153–159.</reference>
    <reference>Rogers, C. M. 1984. Linaceae. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Ser. 2, part 12, pp. 1–54.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 4; petals 4.</description>
      <determination>1 Radiola</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 5; petals 5.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 5; fruits capsules, dehiscing into 5 or 10 segments.</description>
      <determination>2 Linum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 2–3; fruits capsules dehiscing into 4 or 6 segments, schizocarps breaking into 4 nutlets, or indehiscent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: basal and proximal usually whorled, distal alternate or opposite; fruits capsules, dehiscing into 4 or 6 segments; styles 2–3, stigmas ± equal in width to styles; stipular glands present (exudate often red) or absent.</description>
      <determination>3 Hesperolinon</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: proximal opposite, distal sometimes alternate; fruits schizocarps, breaking into 4 nutlets, or indehiscent; styles 2, stigmas wider than styles; stipular glands absent.</description>
      <determination>4 Sclerolinon</determination>
    </key_statement>
  </key>
</bio:treatment>