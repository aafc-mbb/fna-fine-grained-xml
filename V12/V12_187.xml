<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_hierarchy>family oxalidaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242417482</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Wood-sorrel Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [subshrubs, shrubs, vines or trees], annual or perennial.</text>
      <biological_entity id="o8104" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate or whorled, usually palmately or pinnately compound, sometimes 1-foliolate;</text>
      <biological_entity id="o8105" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="usually palmately; palmately; pinnately" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="1-foliolate" value_original="1-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules usually present, rarely apparently absent;</text>
      <biological_entity id="o8106" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely apparently" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o8107" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire;</text>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate or subpalmate.</text>
      <biological_entity constraint="blade" id="o8108" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subpalmate" value_original="subpalmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, cymes or racemes, or flowers solitary.</text>
      <biological_entity id="o8109" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o8110" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
      <biological_entity id="o8111" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o8112" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, perianth and androecium hypogynous;</text>
      <biological_entity id="o8113" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o8114" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o8115" name="androecium" name_original="androecium" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium absent;</text>
      <biological_entity id="o8116" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 5, distinct or slightly connate basally;</text>
      <biological_entity id="o8117" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="slightly; basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, distinct or slightly connate basally;</text>
      <biological_entity id="o8118" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="slightly; basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary present;</text>
      <biological_entity id="o8119" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10 in 2 whorls, connate basally (monadelphous), free;</text>
      <biological_entity id="o8120" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="in whorls" constraintid="o8121" name="quantity" src="d0_s12" value="10" value_original="10" />
        <character is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o8121" name="whorl" name_original="whorls" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers dehiscing by longitudinal slits;</text>
      <biological_entity id="o8122" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="by slits" constraintid="o8123" is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o8123" name="slit" name_original="slits" src="d0_s13" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistil 1, 5-carpellate, ovary superior, 5-locular;</text>
      <biological_entity id="o8124" name="pistil" name_original="pistil" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>placentation axile;</text>
      <biological_entity id="o8125" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules (1–) 3–8 (–10) per locule, anatropous;</text>
      <biological_entity id="o8126" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s16" to="3" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="10" />
        <character char_type="range_value" constraint="per locule" constraintid="o8127" from="3" name="quantity" src="d0_s16" to="8" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s16" value="anatropous" value_original="anatropous" />
      </biological_entity>
      <biological_entity id="o8127" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 5, distinct;</text>
      <biological_entity id="o8128" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 5.</text>
      <biological_entity id="o8129" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsules [berries], dehiscence loculicidal, often elastically.</text>
      <biological_entity constraint="fruits" id="o8130" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1–10 per locule.</text>
      <biological_entity id="o8131" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o8132" from="1" name="quantity" src="d0_s20" to="10" />
      </biological_entity>
      <biological_entity id="o8132" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Genera 5, species ca. 800 (1 genus, 36 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands, Australia; introduced in Bermuda.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="in Bermuda" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oxalidaceae occurs mostly in the tropics and subtropics of both hemispheres but extends into temperate regions. Species of Averrhoa Linnaeus and Sarcotheca Blume are trees or shrubs, those of Dapania Korthals lianas; all have fleshy, baccate fruits. Species of Biophytum de Candolle and Oxalis are herbs, subshrubs, shrubs, or rarely vines with capsular fruits; Biophytum has even-pinnate leaves fascicled at the stem tips. Seeds of Oxalidaceae develop an elastic, translucent, arilliform epidermis that turns inside out, explosively ejecting them from the capsule (K. R. Robertson 1975).</discussion>
  <discussion>Oxalis tuberosa Molina (oca or New Zealand yam), of Andean South America, is cultivated for its edible tubers. Averrhoa is widely cultivated in the tropics for its fruits, which are eaten fresh, used in drinks, or made into jelly and jam. Both species of Averrhoa [A. bilimbi Linnaeus (bilimbi or cucumber tree), A. carambola Linnaeus (starfruit or carambola)] are known only in cultivation or as escapes from cultivation.</discussion>
  
</bio:treatment>