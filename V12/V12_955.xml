<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Houstoun ex Miller" date="1754" rank="genus">BERNARDIA</taxon_name>
    <taxon_name authority="C. V. Morton" date="1939" rank="species">incana</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>29: 376. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus bernardia;species incana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101930</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs to 1.5 m.</text>
      <biological_entity id="o23408" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules persistent, yellowish-brown to black, base thickened, with dark resinous exudate;</text>
      <biological_entity id="o23409" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o23410" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="yellowish-brown" name="coloration" src="d0_s1" to="black" />
      </biological_entity>
      <biological_entity id="o23411" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o23412" name="exudate" name_original="exudate" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
        <character is_modifier="true" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o23411" id="r1925" name="with" negation="false" src="d0_s1" to="o23412" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 1.5–2.5 (–3.4) mm;</text>
      <biological_entity id="o23413" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23414" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually broadly elliptic to suborbiculate, rarely cuneate, 0.8–2.5 × 0.3–1.1 cm, margins revolute, crenate, laminar glands (0–) 2–4 (–6), abaxial surface grayish white, densely appressed stellate-pubescent, adaxial surface green, glabrate;</text>
      <biological_entity id="o23415" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23416" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually broadly elliptic" name="shape" src="d0_s3" to="suborbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23417" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="laminar" id="o23418" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s3" to="2" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23419" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish white" value_original="grayish white" />
        <character is_modifier="false" modifier="densely" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23420" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins prominent abaxially.</text>
      <biological_entity id="o23421" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23422" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: staminate thyrses 5–20 mm.</text>
      <biological_entity id="o23423" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o23424" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: stamens (3–) 5–7, nectary glands peltiform.</text>
      <biological_entity id="o23425" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23426" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity constraint="nectary" id="o23427" name="gland" name_original="glands" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers: pistil 3-carpellate;</text>
      <biological_entity id="o23428" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23429" name="pistil" name_original="pistil" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles 3, lobulate adaxially.</text>
      <biological_entity id="o23430" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23431" name="style" name_original="styles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s8" value="lobulate" value_original="lobulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 6 mm, 3-lobed.</text>
      <biological_entity id="o23432" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="6" value_original="6" />
        <character is_modifier="false" name="shape" src="d0_s9" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As indicated by V. W. Steinmann and R. S. Felger (1997), Bernardia incana is readily distinguished from B. myricifolia, with which it has often been merged (for example, G. L. Webster 1993b), by its dark persistent stipules that are thickened by a dried, resinous exudate. Within the flora area, B. incana occupies a mostly montane Sonoran Desert range in southern Arizona and California (extending just into the southern Mojave Desert), with outlying populations in the Grand Canyon. No specimens have been located to document a reported distribution in Nevada, but it is present to within a few miles of the border along the Grand Wash Cliffs in Arizona.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer; fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Protected slopes in desert canyon washes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="protected slopes" constraint="in desert" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="washes" modifier="canyon" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>