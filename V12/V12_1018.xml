<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">KRAMERIACEAE</taxon_name>
    <taxon_name authority="Loefling" date="1758" rank="genus">KRAMERIA</taxon_name>
    <taxon_name authority="S. Watson" date="1886" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>21: 417. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family krameriaceae;genus krameria;species bicolor</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101320</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Krameria</taxon_name>
    <taxon_name authority="Rose &amp; J. H. Painter" date="unknown" rank="species">grayi</taxon_name>
    <taxon_hierarchy>genus krameria;species grayi</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Gray’s or white ratany</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, mound-forming, 0.2–1.5 m.</text>
      <biological_entity id="o24714" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mound-forming" value_original="mound-forming" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, long-shoots only, young branches green, becoming blue-green with age, canescent, tips thorny.</text>
      <biological_entity id="o24715" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o24716" name="long-shoot" name_original="long-shoots" src="d0_s1" type="structure" />
      <biological_entity id="o24717" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character constraint="with age" constraintid="o24718" is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="blue-green" value_original="blue-green" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o24718" name="age" name_original="age" src="d0_s1" type="structure" />
      <biological_entity id="o24719" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="thorny" value_original="thorny" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear or linear-lanceolate, 4–20 × 1–5 mm, ape× mucronate, surfaces canescent, lacking glandular-hairs.</text>
      <biological_entity id="o24720" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24721" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24722" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o24723" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary, solitary flowers.</text>
      <biological_entity id="o24724" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o24725" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals reflexed, purple or dark maroon, lanceolate, 7–13 mm;</text>
      <biological_entity id="o24726" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o24727" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark maroon" value_original="dark maroon" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>secretory petals dark purple, redbrown, pink, or yellow, 1.5–4.5 mm, with oil-filled blisters covering outer surfaces;</text>
      <biological_entity id="o24728" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="secretory" id="o24729" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24730" name="blister" name_original="blisters" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o24731" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <relation from="o24729" id="r2008" name="with" negation="false" src="d0_s5" to="o24730" />
      <relation from="o24730" id="r2009" name="covering" negation="false" src="d0_s5" to="o24731" />
    </statement>
    <statement id="d0_s6">
      <text>petaloid petals 3–6 mm, distinct, green basally, pink or purple distally, narrowly oblanceolate;</text>
      <biological_entity id="o24732" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o24733" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens didynamous;</text>
      <biological_entity id="o24734" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24735" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="didynamous" value_original="didynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary tomentose;</text>
      <biological_entity id="o24736" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24737" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style red or pink.</text>
      <biological_entity id="o24738" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24739" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules cordate to circular in outline, often with conspicuous longitudinal ridge, 5.5–10 mm diam., canescent, sericeous, or tomentose, spines slender, 1.5–5.5 mm, each bearing unicellular hairs basally and amber-colored recurved barbs to 1 mm near tip.</text>
      <biological_entity id="o24740" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o24741" from="cordate" name="shape" src="d0_s10" to="circular" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="diameter" notes="" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o24741" name="outline" name_original="outline" src="d0_s10" type="structure" />
      <biological_entity id="o24742" name="ridge" name_original="ridge" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o24743" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24744" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o24746" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <relation from="o24740" id="r2010" modifier="often" name="with" negation="false" src="d0_s10" to="o24742" />
      <relation from="o24743" id="r2011" modifier="basally" name="bearing" negation="false" src="d0_s10" to="o24744" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 12.</text>
      <biological_entity id="o24745" name="barb" name_original="barbs" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="amber-colored" value_original="amber-colored" />
        <character is_modifier="true" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="near tip" constraintid="o24746" from="0" from_unit="mm" name="location" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24747" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The name Krameria grayi has generally been used for this species in the United States (for example, B. L. Turner et al. 2003) but must be replaced by the older correct name (B. B. Simpson 2013). The species was originally described in 1852 as K. canescens by A. Gray, but this name is a later homonym of K. canescens Willdenow e\× Schultes. Rose and Painter, realizing that the name was illegitimate, renamed the species K. grayi in 1906.</discussion>
  <discussion>Krameria bicolor was reported from New Mexico by W. C. Martin and C. R. Hutchins (1980), but no New Mexico specimens of this species were seen by the author.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deserts on limestone, volcanic, or igneous-derived soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deserts" constraint="on limestone" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="igneous-derived soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Tex., Utah; Mexico (Baja California, Baja California Sur, Chihuahua, Coahuila, Durango, Hidalgo, Jalisco, Michoacán, Nayarit, Sinaloa, Sonora, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Hidalgo)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Michoacán)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>