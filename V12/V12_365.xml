<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">FRANGULA</taxon_name>
    <taxon_name authority="(Eschscholtz) A. Gray" date="1849" rank="species">californica</taxon_name>
    <taxon_name authority="(Howell ex Greene) Kartesz &amp; Gandhi" date="1994" rank="subspecies">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>76: 449. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus frangula;species californica;subspecies occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101499</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhamnus</taxon_name>
    <taxon_name authority="Howell ex Greene" date="1889" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 15. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus rhamnus;species occidentalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Eschscholtz" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Howell ex Greene) C. B. Wolf" date="unknown" rank="subspecies">occidentalis</taxon_name>
    <taxon_hierarchy>genus r.;species californica;subspecies occidentalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Howell ex Greene) Jepson" date="unknown" rank="variety">occidentalis</taxon_name>
    <taxon_hierarchy>genus r.;species californica;variety occidentalis</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades yellowish green on both surfaces, ovate to elliptic, 2–8 cm, margins entire or serrulate, flat, apex acute to obtuse, both surfaces glabrous or abaxial slightly puberulent;</text>
      <biological_entity id="o8325" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character constraint="on surfaces" constraintid="o8326" is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s0" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s0" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8326" name="surface" name_original="surfaces" src="d0_s0" type="structure" />
      <biological_entity id="o8327" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s0" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o8328" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s0" to="obtuse" />
      </biological_entity>
      <biological_entity id="o8329" name="surface" name_original="surfaces" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8330" name="surface" name_original="surfaces" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>veins not prominent abaxially.</text>
      <biological_entity id="o8331" name="vein" name_original="veins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not; abaxially" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Drupe stones 3.</text>
      <biological_entity constraint="drupe" id="o8332" name="stone" name_original="stones" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, pine woodlands, serpentine substrates, creek bottoms.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pine woodlands" />
        <character name="habitat" value="serpentine substrates" />
        <character name="habitat" value="bottoms" modifier="creek" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>