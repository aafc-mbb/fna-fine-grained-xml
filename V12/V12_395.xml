<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>407. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101922</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Elliott’s or pondshore croton</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 3–8 dm, monoecious.</text>
      <biological_entity id="o4336" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually single from base, then well branched from first reproductive node, appressed stellate-hairy.</text>
      <biological_entity id="o4337" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from base" constraintid="o4338" is_modifier="false" modifier="usually" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character constraint="from reproductive node" constraintid="o4339" is_modifier="false" modifier="well" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o4338" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity constraint="reproductive" id="o4339" name="node" name_original="node" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sometimes clustered near inflorescences;</text>
      <biological_entity id="o4340" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near inflorescences" constraintid="o4341" is_modifier="false" modifier="sometimes" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o4341" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o4342" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–2 cm, glands absent at apex;</text>
      <biological_entity id="o4343" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4344" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o4345" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4345" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to oblong, 2–5.5 × 0.2–0.8 cm, base rounded, margins entire, apex subacute, abaxial surface pale green, not appearing brown-dotted, no stellate hairs with brown centers, densely long stellate-hairy, adaxial surface darker green, less densely short stellate-hairy.</text>
      <biological_entity id="o4346" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s5" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4347" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4348" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4349" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4350" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="densely" name="length_or_size" notes="" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o4351" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="brown-dotted" value_original="brown-dotted" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="no" value_original="no" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s5" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o4352" name="center" name_original="centers" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4353" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker green" value_original="darker green" />
        <character is_modifier="false" modifier="less densely" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o4350" id="r389" name="appearing" negation="true" src="d0_s5" to="o4351" />
      <relation from="o4350" id="r390" name="with" negation="true" src="d0_s5" to="o4352" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences bisexual, congested racemes, 0.8–1.5 cm, staminate flowers 5–15, pistillate flowers 3–6.</text>
      <biological_entity id="o4354" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o4355" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4356" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <biological_entity id="o4357" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: staminate 0.4–0.9 mm, pistillate 0–0.8 mm.</text>
      <biological_entity id="o4358" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 5, 0.8–1 mm, abaxial surface stellate-hairy;</text>
      <biological_entity id="o4359" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4360" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4361" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, linear-oblong to lanceolate, 0.8–1 mm, abaxial surface glabrous;</text>
      <biological_entity id="o4362" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4363" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4364" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 7–10.</text>
      <biological_entity id="o4365" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4366" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 6–7, equal, 5–6 mm, margins entire, apex incurved, abaxial surface stellate-hairy;</text>
      <biological_entity id="o4367" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4368" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="7" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4369" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4370" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4371" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o4372" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4373" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular;</text>
      <biological_entity id="o4374" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4375" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, 2–3 mm, 2 times 2-fid, terminal segments 12.</text>
      <biological_entity id="o4376" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4377" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character constraint="segment" constraintid="o4378" is_modifier="false" name="size_or_quantity" src="d0_s14" value="2 times 2-fid terminal segments" />
        <character constraint="segment" constraintid="o4379" is_modifier="false" name="size_or_quantity" src="d0_s14" value="2 times 2-fid terminal segments" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4378" name="segment" name_original="segments" src="d0_s14" type="structure" />
      <biological_entity constraint="terminal" id="o4379" name="segment" name_original="segments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules 4–5 mm diam., smooth;</text>
      <biological_entity id="o4380" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella slightly 3-winged distally.</text>
      <biological_entity id="o4381" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="architecture" src="d0_s16" value="3-winged" value_original="3-winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 4–4.5 × 3–4 mm, shiny.</text>
      <biological_entity id="o4382" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s17" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Croton elliottii is most closely related to C. capitatus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Depression ponds, depression meadows, clay-based Carolina bays, usually on exposed pond edges or bottoms.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="depression ponds" />
        <character name="habitat" value="depression meadows" />
        <character name="habitat" value="carolina bays" modifier="clay-based" />
        <character name="habitat" value="exposed pond edges" modifier="usually on" />
        <character name="habitat" value="bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>