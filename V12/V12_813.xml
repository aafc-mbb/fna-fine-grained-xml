<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Hoover &amp; Roof" date="1966" rank="species">hearstiorum</taxon_name>
    <place_of_publication>
      <publication_title>Four Seasons</publication_title>
      <place_in_publication>2(1): 4. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species hearstiorum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101419</other_info_on_name>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Hearst ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, 0.1–0.3 m, matlike or moundlike.</text>
      <biological_entity id="o7530" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.3" to_unit="m" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading or prostrate, not rooting at nodes, some flowering branches ascending;</text>
      <biological_entity id="o7531" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="at nodes" constraintid="o7532" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o7532" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o7533" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets green to reddish-brown, not thorn-tipped, round or slightly angled in cross-section, flexible, densely puberulent.</text>
      <biological_entity id="o7534" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="reddish-brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="thorn-tipped" value_original="thorn-tipped" />
        <character is_modifier="false" name="shape" src="d0_s2" value="round" value_original="round" />
        <character constraint="in cross-section" constraintid="o7535" is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s2" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7535" name="cross-section" name_original="cross-section" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–2 mm;</text>
      <biological_entity id="o7536" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7537" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade flat to cupped, linear, oblong, or oblong-obovate, 8–20 × 2–10 mm, base cuneate to obtuse, margins entire or obscurely glandular-denticulate, weakly revolute, glands 23–31, apex truncate or retuse, abaxial surface green, densely tomentulose, adaxial surface dark green, glandular-papillate and sometimes villosulous;</text>
      <biological_entity id="o7538" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7539" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-obovate" value_original="oblong-obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7540" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o7541" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s4" value="glandular-denticulate" value_original="glandular-denticulate" />
        <character is_modifier="false" modifier="weakly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o7542" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="23" name="quantity" src="d0_s4" to="31" />
      </biological_entity>
      <biological_entity id="o7543" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7544" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7545" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="relief" src="d0_s4" value="glandular-papillate" value_original="glandular-papillate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pinnately veined, veins ± furrowed.</text>
      <biological_entity id="o7546" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o7547" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, umbellike or racemelike, 1–5 cm.</text>
      <biological_entity id="o7548" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="umbel-like" value_original="umbellike" />
        <character is_modifier="false" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals, petals, and nectary deep blue.</text>
      <biological_entity id="o7549" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7550" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o7551" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o7552" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 4–5 mm wide, not lobed to weakly lobed;</text>
      <biological_entity id="o7553" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="not lobed" name="shape" src="d0_s8" to="weakly lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves smooth, not crested.</text>
      <biological_entity id="o7554" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus hearstiorum occurs in a small area of coastal bluffs in northern San Luis Obispo County, growing in close proximity to another local endemic, C. maritimus (subg. Cerastes).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Consolidated alluvial or serpentine soils, maritime chaparral, coastal prairies.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="consolidated alluvial" />
        <character name="habitat" value="serpentine soils" />
        <character name="habitat" value="maritime chaparral" />
        <character name="habitat" value="coastal prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>