<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Garden in C. Linnaeus" date="1767" rank="genus">STILLINGIA</taxon_name>
    <taxon_name authority="Torrey in W. H. Emory" date="1848" rank="species">spinulosa</taxon_name>
    <place_of_publication>
      <publication_title>Not. Milit. Reconn.,</publication_title>
      <place_in_publication>151. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus stillingia;species spinulosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101958</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Annual toothleaf</other_name>
  <other_name type="common_name">broad-leaved stillingia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, with taproot.</text>
      <biological_entity id="o24655" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24656" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o24655" id="r2006" name="with" negation="false" src="d0_s0" to="o24656" />
    </statement>
    <statement id="d0_s1">
      <text>Stems fascicled, decumbent to erect, branching distally, 0.5–3 dm.</text>
      <biological_entity id="o24657" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite or subopposite;</text>
      <biological_entity id="o24658" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o24659" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o24660" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic to elliptic-spatulate, 1.5–4 (–4.5) × 0.5–1.4 (–1.8) cm, base narrowly cuneate, margins prominently spinulose-dentate, teeth without prominent blackened tips, not incurved, apex acuminate;</text>
      <biological_entity id="o24661" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="elliptic-spatulate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24662" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24663" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s5" value="spinulose-dentate" value_original="spinulose-dentate" />
      </biological_entity>
      <biological_entity id="o24664" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" notes="" src="d0_s5" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity id="o24665" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="blackened" value_original="blackened" />
      </biological_entity>
      <biological_entity id="o24666" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o24664" id="r2007" name="without" negation="false" src="d0_s5" to="o24665" />
    </statement>
    <statement id="d0_s6">
      <text>midvein and secondary-veins prominent.</text>
      <biological_entity id="o24667" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o24668" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences sessile, 1–1.2 (–2) cm;</text>
      <biological_entity id="o24669" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminate flowers crowded, 1 per node;</text>
      <biological_entity id="o24670" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
        <character constraint="per node" constraintid="o24671" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24671" name="node" name_original="node" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pistillate flowers 1–3, crowded;</text>
      <biological_entity id="o24672" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts linear-lanceolate, to 2 mm, apex acute, glands patelliform, long-stalked, 1.5–-2 mm diam.</text>
      <biological_entity id="o24673" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24674" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o24675" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="patelliform" value_original="patelliform" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="long-stalked" value_original="long-stalked" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: calyx to 1 mm.</text>
      <biological_entity id="o24676" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24677" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: sepals 0;</text>
      <biological_entity id="o24678" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24679" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles connate only at base, to 3 mm.</text>
      <biological_entity id="o24680" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24681" name="style" name_original="styles" src="d0_s13" type="structure">
        <character constraint="at base" constraintid="o24682" is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24682" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid, 3.5 × 4–4.5 mm, deeply 3-lobed;</text>
      <biological_entity id="o24683" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character name="length" src="d0_s14" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s14" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lobes of gynobase 2 mm;</text>
      <biological_entity id="o24684" name="lobe" name_original="lobes" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>columella sometimes persistent.</text>
      <biological_entity id="o24685" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds mottled light gray, cylindric-ovoid, 3.5 × 1.8 mm, smooth;</text>
      <biological_entity id="o24686" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="mottled light" value_original="mottled light" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindric-ovoid" value_original="cylindric-ovoid" />
        <character name="length" src="d0_s17" unit="mm" value="3.5" value_original="3.5" />
        <character name="width" src="d0_s17" unit="mm" value="1.8" value_original="1.8" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o24687" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Dec–Mar(–Apr); fruiting Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Dec" />
        <character name="flowering time" char_type="atypical_range" to="Apr" from="Dec" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy desert soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy desert soils" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>