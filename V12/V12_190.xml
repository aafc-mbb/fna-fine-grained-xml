<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">TRACHYPHYTUM</taxon_name>
    <taxon_name authority="(Douglas) Douglas ex Torrey &amp; A. Gray" date="1840" rank="species">albicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 534. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section trachyphytum;species albicaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250101871</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bartonia</taxon_name>
    <taxon_name authority="Douglas in W. J. Hooker" date="1832" rank="species">albicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 222. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bartonia;species albicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mentzelia</taxon_name>
    <taxon_name authority="H. J. Thompson &amp; J. E. Roberts" date="unknown" rank="species">mojavensis</taxon_name>
    <taxon_hierarchy>genus mentzelia;species mojavensis</taxon_hierarchy>
  </taxon_identification>
  <number>66.</number>
  <other_name type="common_name">Whitestem blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants wandlike or candelabra-form, (2–) 10–40 (–50) cm.</text>
      <biological_entity id="o22486" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="wand-like" value_original="wandlike" />
        <character name="shape" src="d0_s0" value="candelabra-form" value_original="candelabra-form" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves persisting;</text>
      <biological_entity constraint="basal" id="o22487" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present or absent;</text>
      <biological_entity id="o22488" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear-lanceolate to linear, margins deeply to shallowly lobed.</text>
      <biological_entity id="o22489" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o22490" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply to shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: petiole absent;</text>
      <biological_entity constraint="cauline" id="o22491" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22492" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovatelanceolate to linear, to 15 cm, margins deeply to shallowly lobed or entire.</text>
      <biological_entity constraint="cauline" id="o22493" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o22494" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22495" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Bracts green, ovate to linear, 3.7–8.6 × 0.8–3.9 mm, width 1/6–2/3 length, not concealing capsule, margins 3-lobed or entire, lateral lobes never prominent.</text>
      <biological_entity id="o22496" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s6" to="8.6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="3.9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1/(1/6-2/3)" value_original="1/(1/6-2/3)" />
      </biological_entity>
      <biological_entity id="o22497" name="capsule" name_original="capsule" src="d0_s6" type="structure" />
      <biological_entity id="o22498" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o22499" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="never" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o22496" id="r1846" name="concealing" negation="true" src="d0_s6" to="o22497" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–5 mm;</text>
      <biological_entity id="o22500" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22501" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals orange proximally, yellow distally, 3–7 (–8) mm, apex usually acute, rarely retuse;</text>
      <biological_entity id="o22502" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22503" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22504" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20+, 3–5 mm, filaments monomorphic, filiform, unlobed;</text>
      <biological_entity id="o22505" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22506" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22507" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 2–5 mm.</text>
      <biological_entity id="o22508" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22509" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules clavate, 8–28 (–35) × 1.5–3.5 mm, longest capsules usually 15+ mm, axillary curved to 180° at maturity, usually inconspicuously longitudinally ribbed.</text>
      <biological_entity id="o22510" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="28" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longest" id="o22511" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" upper_restricted="false" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character constraint="at maturity" is_modifier="false" modifier="0-180°" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually inconspicuously longitudinally" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 10–30, in 2+ rows distal to mid fruit, dark-brown or tan, moderately to densely dark-mottled, usually irregularly polygonal, occasionally triangular prisms proximal to mid fruit, surface tuberculate under 10× magnification;</text>
      <biological_entity id="o22512" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="30" />
        <character char_type="range_value" from="tan" name="coloration" notes="" src="d0_s12" to="moderately densely dark-mottled" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="moderately densely dark-mottled" />
        <character is_modifier="false" modifier="usually irregularly" name="shape" src="d0_s12" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o22513" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22514" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o22515" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o22516" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character constraint="under 10×magnification" is_modifier="false" name="relief" src="d0_s12" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <relation from="o22512" id="r1847" name="in" negation="false" src="d0_s12" to="o22513" />
    </statement>
    <statement id="d0_s13">
      <text>recurved flap over hilum absent;</text>
      <biological_entity id="o22517" name="flap" name_original="flap" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o22518" name="hilum" name_original="hilum" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o22517" id="r1848" name="over" negation="false" src="d0_s13" to="o22518" />
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cell outer periclinal wall domed, domes on seed edges more than 1/2 as tall as wide at maturity.</text>
      <biological_entity constraint="seed-coat" id="o22519" name="cell" name_original="cell" src="d0_s14" type="structure" />
      <biological_entity constraint="outer" id="o22520" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="periclinal" value_original="periclinal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="domed" value_original="domed" />
      </biological_entity>
      <biological_entity constraint="seed" id="o22522" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character name="quantity" src="d0_s14" value="/2" value_original="/2" />
      </biological_entity>
      <relation from="o22521" id="r1849" name="on" negation="false" src="d0_s14" to="o22522" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 54, 72.</text>
      <biological_entity id="o22521" name="dome" name_original="domes" src="d0_s14" type="structure">
        <character constraint="at maturity" is_modifier="false" name="width" src="d0_s14" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22523" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="54" value_original="54" />
        <character name="quantity" src="d0_s15" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia albicaulis is the most widespread species in sect. Trachyphytum and exhibits extensive morphological variation. Most populations of M. albicaulis are octoploid; however, hexaploids from southern California that have been called M. mojavensis and occasionally M. californica are also treated here as M. albicaulis. Two tetraploids in sect. Trachyphytum, M. montana and M. obscura, also have been treated previously as M. albicaulis (N. H. Holmgren et al. 2005). Both exhibit morphological forms and distributions overlapping with M. albicaulis. However, in most cases these species can be distinguished without chromosome counts, and their distinctiveness has been supported by phylogenetic analyses (J. M. Brokaw and L. Hufford 2010b).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes, gravel fans, washes, desert scrub, sagebrush or antelope bitterbrush scrub, open ponderosa pine woodlands, pinyon/juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="gravel fans" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="antelope" />
        <character name="habitat" value="scrub" modifier="bitterbrush" />
        <character name="habitat" value="open ponderosa pine woodlands" />
        <character name="habitat" value="pinyon\/juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Sask.; Ariz., Calif., Colo., Idaho, Mont., Nebr., Nev., N.Mex., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico (Baja California, Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>