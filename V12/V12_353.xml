<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Engelmann &amp; A. Gray" date="1845" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>5: 233. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species bicolor;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101532</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Snow-on-the-prairie</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o13733" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o13734" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o13733" id="r1139" name="with" negation="false" src="d0_s0" to="o13734" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, unbranched or branched, 40–100 cm, pilose.</text>
      <biological_entity id="o13735" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o13736" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules 0.3–0.4 mm;</text>
      <biological_entity id="o13737" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–1 mm, pilose;</text>
      <biological_entity id="o13738" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly elliptic to lanceolate, 37–54 × 7–17 mm, base cuneate to slightly rounded, margins entire, apex aristate or acute, surfaces pilose;</text>
      <biological_entity id="o13739" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="37" from_unit="mm" name="length" src="d0_s5" to="54" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13740" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="slightly rounded" />
      </biological_entity>
      <biological_entity id="o13741" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13742" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation obscure, only midvein conspicuous.</text>
      <biological_entity id="o13743" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o13744" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in terminal pleiochasia dichasial and pleiochasial bracts linear to narrowly oblanceolate, with conspicuous white margins;</text>
      <biological_entity id="o13745" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o13746" name="pleiochasium" name_original="pleiochasia" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
      </biological_entity>
      <biological_entity id="o13747" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="narrowly oblanceolate" />
      </biological_entity>
      <biological_entity id="o13748" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <relation from="o13745" id="r1140" name="in" negation="false" src="d0_s7" to="o13746" />
      <relation from="o13745" id="r1141" name="in" negation="false" src="d0_s7" to="o13747" />
      <relation from="o13745" id="r1142" name="with" negation="false" src="d0_s7" to="o13748" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 1.2–3 mm, densely pilose.</text>
      <biological_entity id="o13749" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 2.7–3.5 × 2.2–3 mm, densely pilose;</text>
      <biological_entity id="o13750" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s9" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4–5, green to pale greenish yellow, reniform, 0.6–0.7 × 1.4–1.6 mm;</text>
      <biological_entity id="o13751" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="pale greenish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s10" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s10" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white, obdeltate to orbiculate, 1.4–2.5 × 1.7–3 mm, dentate to erose.</text>
      <biological_entity id="o13752" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="obdeltate" name="shape" src="d0_s11" to="orbiculate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="dentate" name="architecture" src="d0_s11" to="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 30–70.</text>
      <biological_entity id="o13753" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary pilose;</text>
      <biological_entity id="o13754" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13755" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.7–1.2 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o13756" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13757" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules depressed-ovoid, 3.5–7.5 × 6–8.7 mm, densely pilose;</text>
      <biological_entity id="o13758" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-ovoid" value_original="depressed-ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s15" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s15" to="8.7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 4.5–5.5 mm.</text>
      <biological_entity id="o13759" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds tan to brown, ovoid, 4.3–4.5 × 3.7–3.9 mm, alveolate;</text>
      <biological_entity id="o13760" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4.3" from_unit="mm" name="length" src="d0_s17" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="width" src="d0_s17" to="3.9" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s17" value="alveolate" value_original="alveolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o13761" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia bicolor is similar in appearance to E. marginata but can be distinguished by its linear to narrowly oblanceolate bracts and the presence of hairs on all parts of the plant.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, blackland (calcareous) prairies, pastures and clearings in former blackland prairie areas, roadside clearings.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="pastures" constraint="in former blackland prairie areas" />
        <character name="habitat" value="clearings" constraint="in former blackland prairie areas" />
        <character name="habitat" value="former blackland prairie areas" />
        <character name="habitat" value="clearings" modifier="roadside" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., La., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>