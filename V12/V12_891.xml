<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Cavanilles" date="1791" rank="species">dioicus</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>1: 4, plate 6. 1791</place_in_publication>
      <other_info_on_pub>(as dioicum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species dioicus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101921</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Croton</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="species">elaeagnifolius</taxon_name>
    <taxon_hierarchy>genus croton;species elaeagnifolius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>genus c.;species gracilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Müller Arg." date="unknown" rank="species">neomexicanus</taxon_name>
    <taxon_hierarchy>genus c.;species neomexicanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Sessé &amp; Mociño" date="unknown" rank="species">vulpinus</taxon_name>
    <taxon_hierarchy>genus c.;species vulpinus</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Grassland croton</other_name>
  <other_name type="common_name">hierba del gato</other_name>
  <other_name type="common_name">rosval</other_name>
  <other_name type="common_name">rubaldo</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 2–5 (–9) dm, dioecious.</text>
      <biological_entity id="o26083" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems well-branched from base, stellate-lepidote.</text>
      <biological_entity id="o26084" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from base" constraintid="o26085" is_modifier="false" name="architecture" src="d0_s1" value="well-branched" value_original="well-branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="stellate-lepidote" value_original="stellate-lepidote" />
      </biological_entity>
      <biological_entity id="o26085" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves not clustered;</text>
      <biological_entity id="o26086" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules rudimentary or absent;</text>
      <biological_entity id="o26087" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.2–0.8 (–2) cm, usually less than 1/2 blade length, glands absent at apex;</text>
      <biological_entity id="o26088" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s4" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0" modifier="usually" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o26089" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o26090" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o26091" is_modifier="false" name="length" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26091" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly elliptic-ovate to lanceolate, 1–6.5 × 0.6–2.2 cm, usually less than 1/2 as wide as long, base rounded, margins entire, apex acute to rounded, abaxial surface pale green, densely silvery lepidote or stellate-lepidote, adaxial surface darker green, less densely lepidote.</text>
      <biological_entity id="o26092" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic-ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s5" to="2.2" to_unit="cm" />
        <character char_type="range_value" constraint="as-wide-as long" from="0" modifier="usually" name="quantity" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o26093" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o26094" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26095" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26096" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s5" value="silvery" value_original="silvery" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lepidote" value_original="lepidote" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="stellate-lepidote" value_original="stellate-lepidote" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26097" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker green" value_original="darker green" />
        <character is_modifier="false" modifier="less densely" name="pubescence" src="d0_s5" value="lepidote" value_original="lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences unisexual, racemes;</text>
      <biological_entity id="o26098" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminate 2–8 cm, flowers 4–16;</text>
      <biological_entity id="o26099" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26100" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate 0.5–1 cm, flowers 2–5.</text>
      <biological_entity id="o26101" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="16" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26102" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26103" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels: staminate 1–4 mm, pistillate 2–5 mm.</text>
      <biological_entity id="o26104" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: sepals 5, 1 mm, abaxial surface lepidote;</text>
      <biological_entity id="o26105" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26106" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26107" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="lepidote" value_original="lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0;</text>
      <biological_entity id="o26108" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26109" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10–12.</text>
      <biological_entity id="o26110" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26111" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: sepals 5, equal, 1.5–2 mm, margins entire, apex straight to slightly incurved, abaxial surface stellate-lepidote;</text>
      <biological_entity id="o26112" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26113" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26114" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26115" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s13" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26116" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stellate-lepidote" value_original="stellate-lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 0;</text>
      <biological_entity id="o26117" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26118" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 3-locular;</text>
      <biological_entity id="o26119" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26120" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 3, 0.5–1.5 mm, 2–3 times 2-fid, terminal segments 12–24.</text>
      <biological_entity id="o26121" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26122" name="style" name_original="styles" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character constraint="segment" constraintid="o26123" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2-3 times 2-fid terminal segments" />
        <character constraint="segment" constraintid="o26124" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2-3 times 2-fid terminal segments" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26123" name="segment" name_original="segments" src="d0_s16" type="structure" />
      <biological_entity constraint="terminal" id="o26124" name="segment" name_original="segments" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules 5–6 mm diam., smooth;</text>
      <biological_entity id="o26125" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s17" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella 3-winged.</text>
      <biological_entity id="o26126" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="3-winged" value_original="3-winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 3.5–5 × 3–4 mm, shiny or dull.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 28, 56.</text>
      <biological_entity id="o26127" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s19" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s19" to="4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26128" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="28" value_original="28" />
        <character name="quantity" src="d0_s20" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone and igneous mountains, canyons, mesas, flats, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone" />
        <character name="habitat" value="igneous mountains" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>