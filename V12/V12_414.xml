<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jinshuang Ma, Geoffrey A. Levin</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CELASTRUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 196. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 91. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus celastrus</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kelastros, ancient name for holly, Ilex aquifolium</other_info_on_name>
    <other_info_on_name type="fna_id">105982</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Bittersweet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, twining, polygamodioecious.</text>
      <biological_entity id="o25980" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="twining" value_original="twining" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets terete.</text>
      <biological_entity id="o25981" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous [persistent], alternate;</text>
      <biological_entity id="o25982" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present;</text>
      <biological_entity id="o25983" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o25984" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins denticulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o25985" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal or axillary, panicles or cymes [racemes].</text>
      <biological_entity id="o25986" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o25987" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o25988" name="cyme" name_original="cymes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual and unisexual, radially symmetric;</text>
      <biological_entity id="o25989" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o25990" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o25991" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium absent;</text>
      <biological_entity id="o25992" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, distinct;</text>
      <biological_entity id="o25993" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, white or greenish white;</text>
      <biological_entity id="o25994" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white" value_original="greenish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary intrastaminal, fleshy.</text>
      <biological_entity id="o25995" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="intrastaminal" value_original="intrastaminal" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Bisexual flowers: stamens 5, free from and inserted under nectary;</text>
      <biological_entity id="o25996" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o25997" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character constraint="under nectary" constraintid="o25998" is_modifier="false" name="position" src="d0_s14" value="free-inserted" value_original="free-inserted" />
      </biological_entity>
      <biological_entity id="o25998" name="nectary" name_original="nectary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes 0;</text>
      <biological_entity id="o25999" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26000" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o26001" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26002" name="pistil" name_original="pistil" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary superior, 3-locular;</text>
      <biological_entity id="o26003" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26004" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s17" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>placentation axile;</text>
      <biological_entity id="o26005" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s18" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="placentation" src="d0_s18" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>style 1;</text>
      <biological_entity id="o26006" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s19" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26007" name="style" name_original="style" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas 3;</text>
      <biological_entity id="o26008" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s20" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26009" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 2 per locule.</text>
      <biological_entity id="o26010" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s21" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o26011" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character constraint="per locule" constraintid="o26012" name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26012" name="locule" name_original="locule" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Staminate flowers: stamens 5;</text>
      <biological_entity id="o26013" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26014" name="stamen" name_original="stamens" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>free from and inserted under nectary;</text>
      <biological_entity id="o26015" name="flower" name_original="flowers" src="d0_s23" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s23" value="staminate" value_original="staminate" />
        <character constraint="under nectary" constraintid="o26016" is_modifier="false" name="position" src="d0_s23" value="free-inserted" value_original="free-inserted" />
      </biological_entity>
      <biological_entity id="o26016" name="nectary" name_original="nectary" src="d0_s23" type="structure" />
    </statement>
    <statement id="d0_s24">
      <text>staminodes 0;</text>
      <biological_entity id="o26017" name="flower" name_original="flowers" src="d0_s24" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s24" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26018" name="staminode" name_original="staminodes" src="d0_s24" type="structure">
        <character name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>pistillode present.</text>
      <biological_entity id="o26019" name="flower" name_original="flowers" src="d0_s25" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s25" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26020" name="pistillode" name_original="pistillode" src="d0_s25" type="structure">
        <character is_modifier="false" name="presence" src="d0_s25" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>Pistillate flowers: staminodes 5, alternate with petals, undivided, not gland-tipped, minute;</text>
      <biological_entity id="o26021" name="flower" name_original="flowers" src="d0_s26" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s26" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26022" name="staminode" name_original="staminodes" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="5" value_original="5" />
        <character constraint="with petals" constraintid="o26023" is_modifier="false" name="arrangement" src="d0_s26" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s26" value="undivided" value_original="undivided" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s26" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="size" src="d0_s26" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o26023" name="petal" name_original="petals" src="d0_s26" type="structure" />
    </statement>
    <statement id="d0_s27">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o26024" name="flower" name_original="flowers" src="d0_s27" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s27" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26025" name="pistil" name_original="pistil" src="d0_s27" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s27" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>ovary superior, 3-locular, placentation axile;</text>
      <biological_entity id="o26026" name="flower" name_original="flowers" src="d0_s28" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s28" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26027" name="ovary" name_original="ovary" src="d0_s28" type="structure">
        <character is_modifier="false" name="position" src="d0_s28" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s28" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="placentation" src="d0_s28" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>style 1;</text>
      <biological_entity id="o26028" name="flower" name_original="flowers" src="d0_s29" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s29" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26029" name="style" name_original="style" src="d0_s29" type="structure">
        <character name="quantity" src="d0_s29" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>stigmas 3;</text>
      <biological_entity id="o26030" name="flower" name_original="flowers" src="d0_s30" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s30" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26031" name="stigma" name_original="stigmas" src="d0_s30" type="structure">
        <character name="quantity" src="d0_s30" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s31">
      <text>ovules 2 per locule.</text>
      <biological_entity id="o26032" name="flower" name_original="flowers" src="d0_s31" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s31" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26033" name="ovule" name_original="ovules" src="d0_s31" type="structure">
        <character constraint="per locule" constraintid="o26034" name="quantity" src="d0_s31" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26034" name="locule" name_original="locule" src="d0_s31" type="structure" />
    </statement>
    <statement id="d0_s32">
      <text>Fruits capsules, 3-locular, globose or subglobose, 3-lobed distally, apex not beaked.</text>
      <biological_entity constraint="fruits" id="o26035" name="capsule" name_original="capsules" src="d0_s32" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s32" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s32" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s32" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s32" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o26036" name="apex" name_original="apex" src="d0_s32" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s32" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s33">
      <text>Seeds [1–] 2 per locule, ellipsoid, not winged;</text>
      <biological_entity id="o26037" name="seed" name_original="seeds" src="d0_s33" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s33" to="2" to_inclusive="false" />
        <character constraint="per locule" constraintid="o26038" name="quantity" src="d0_s33" value="2" value_original="2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s33" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s33" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o26038" name="locule" name_original="locule" src="d0_s33" type="structure" />
    </statement>
    <statement id="d0_s34">
      <text>aril red, completely surrounding seed.</text>
      <biological_entity id="o26039" name="aril" name_original="aril" src="d0_s34" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s34" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s35">
      <text>x = 23.</text>
      <biological_entity id="o26040" name="seed" name_original="seed" src="d0_s34" type="structure">
        <character is_modifier="true" modifier="completely" name="position_relational" src="d0_s34" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity constraint="x" id="o26041" name="chromosome" name_original="" src="d0_s35" type="structure">
        <character name="quantity" src="d0_s35" value="23" value_original="23" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 30 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, e Asia; primarily tropics and subtropics, some temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="primarily tropics and subtropics" establishment_means="native" />
        <character name="distribution" value="some temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Hou, D. 1955. A revision of the genus Celastrus. Ann. Missouri Bot. Gard. 42: 215–302.</reference>
    <reference>Leicht-Young, S. A. et al. 2007. Distinguishing native (Celastrus scandens L.) and invasive (C. orbiculatus Thunb.) bittersweet species using morphological characteristics. J. Torrey Bot. Soc. 134: 441–450.</reference>
    <reference>Pooler, M. R., R. L. Dix, and J. Feely. 2002. Interspecific hybridizations between the native bittersweet, Celastrus scandens, and the introduced invasive species, C. orbiculatus. SouthE. Naturalist 1: 69–76.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences terminal, panicles; capsules orange; pollen yellow; leaf blades usually oblong, aestivation involute.</description>
      <determination>1. Celastrus scandens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences axillary, cymes; capsules yellow; pollen white; leaf blades suborbiculate to broadly oblong-obovate or ovate-orbiculate, aestivation conduplicate.</description>
      <determination>2. Celastrus orbiculatus</determination>
    </key_statement>
  </key>
</bio:treatment>