<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="genus">JAMESIA</taxon_name>
    <taxon_name authority="N. H. Holmgren &amp; P. K. Holmgren" date="1989" rank="species">tetrapetala</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>41: 348, fig. 3J–K. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus jamesia;species tetrapetala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101794</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Four-petal cliffbush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–20 (–40) dm.</text>
      <biological_entity id="o24125" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark exfoliating in gray, orangish gray, or brownish strips or strings.</text>
      <biological_entity id="o24126" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character constraint="in strings" constraintid="o24128" is_modifier="false" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o24127" name="strip" name_original="strips" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="orangish gray" value_original="orangish gray" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o24128" name="string" name_original="strings" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Branches spreading or descending, often stunted and straggly;</text>
      <biological_entity id="o24129" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="descending" value_original="descending" />
        <character is_modifier="false" modifier="often" name="development" src="d0_s2" value="stunted" value_original="stunted" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="straggly" value_original="straggly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>twigs densely spreading or retrorse-pilose.</text>
      <biological_entity id="o24130" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="retrorse-pilose" value_original="retrorse-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 1–4 (–7) mm, ascending-strigose to canescent or sericeous;</text>
      <biological_entity id="o24131" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o24132" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="ascending-strigose" name="pubescence" src="d0_s4" to="canescent or sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate or obovate, (0.5–) 1–2 (–2.7) × (0.2–) 0.8–1.2 (–1.6) cm, base obtuse to usually rounded, sometimes cuneate, usually asymmetric, margins usually dentate, rarely entire, teeth 3–13 (–16), apex obtuse to rounded, abaxial surface moderately to densely canescent or sericeous, adaxial sparsely strigose to glabrescent.</text>
      <biological_entity id="o24133" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o24134" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s5" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24135" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="usually rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o24136" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24137" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="16" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o24138" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24139" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24140" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s5" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1 (–3) –flowered;</text>
      <biological_entity id="o24141" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1(-3)-flowered" value_original="1(-3)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 2–10 mm, canescent or subsericeous.</text>
      <biological_entity id="o24142" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="subsericeous" value_original="subsericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–5 mm, canescent or subsericeous.</text>
      <biological_entity id="o24143" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="subsericeous" value_original="subsericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium 2.5–3.5 × 1–1.5 mm, canescent or sericeous;</text>
      <biological_entity id="o24144" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24145" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4, lanceolate to deltate-ovate, (3–) 5–9 × (1.5–) 2–2.8 mm, margins usually entire, rarely 2-lobed apically, apex acute to obtuse, abaxial surface canescent or sericeous;</text>
      <biological_entity id="o24146" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24147" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="deltate-ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24148" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely; apically" name="shape" src="d0_s10" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o24149" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24150" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4, white with pinkish margin or entirely pink, 5–13 (–15) × 3–4.9 mm, sparsely to densely strigose or canescent, especially distally;</text>
      <biological_entity id="o24151" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24152" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character constraint="with margin" constraintid="o24153" is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="entirely" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4.9" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o24153" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 8;</text>
      <biological_entity id="o24154" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o24155" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments (4–) 5–8 × 0.8–1.4 mm;</text>
      <biological_entity id="o24156" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24157" name="all" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s13" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 0.8–1.1 mm;</text>
      <biological_entity id="o24158" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o24159" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3–5, 4.8–7.7 mm.</text>
      <biological_entity id="o24160" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o24161" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="5" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="some_measurement" src="d0_s15" to="7.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules 3–5.5 (–7) × 2.5–3.5 mm.</text>
      <biological_entity id="o24162" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 0.6–0.8 mm.</text>
      <biological_entity id="o24163" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Jamesia tetrapetala is known from the Grant, Highland, and Snake ranges in eastern Nevada and the House Range in western Utah.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs, crevices, talus.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>