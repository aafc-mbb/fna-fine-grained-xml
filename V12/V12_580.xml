<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">prostratus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">prostratus</taxon_name>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species prostratus;variety prostratus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101452</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">prostratus</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">laxus</taxon_name>
    <taxon_hierarchy>genus ceanothus;species prostratus;variety laxus</taxon_hierarchy>
  </taxon_identification>
  <number>43a.</number>
  <other_name type="common_name">Mahala mat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.1–0.2 m, matlike.</text>
      <biological_entity id="o32627" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.2" to_unit="m" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, with short erect shoots.</text>
      <biological_entity id="o32628" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <biological_entity id="o32629" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o32628" id="r2674" name="with" negation="false" src="d0_s1" to="o32629" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades flat, margins not wavy, teeth usually 3–5 (–7).</text>
      <biological_entity id="o32630" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o32631" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o32632" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsule horns erect, rugose.</text>
      <biological_entity constraint="capsule" id="o32633" name="horn" name_original="horns" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="relief" src="d0_s3" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety prostratus often forms mats one to two and half meters wide.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, open flats, gentle slopes, conifer forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="open flats" />
        <character name="habitat" value="gentle slopes" />
        <character name="habitat" value="forests" modifier="conifer" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>