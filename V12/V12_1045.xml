<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Hamid Razifard, Gordon C. Tucker, Donald H. Les</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">349</other_info_on_meta>
    <other_info_on_meta type="mention_page">348</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">ELATINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ELATINE</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 367. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 172. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family elatinaceae;genus elatine</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek name for a plant with firlike leaves</other_info_on_name>
    <other_info_on_name type="fna_id">111398</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Crypta</taxon_name>
    <taxon_hierarchy>genus crypta</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Waterwort</other_name>
  <other_name type="common_name">élatine</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, submersed or emergent aquatic, glabrous.</text>
      <biological_entity id="o174" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="location" src="d0_s0" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending, decumbent, or prostrate, with longitudinal air spaces, rooting at nodes.</text>
      <biological_entity id="o175" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="at nodes" constraintid="o178" is_modifier="false" name="architecture" notes="" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o176" name="air" name_original="air" src="d0_s1" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s1" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o177" name="space" name_original="spaces" src="d0_s1" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s1" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o178" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o175" id="r12" name="with" negation="false" src="d0_s1" to="o176" />
      <relation from="o175" id="r13" name="with" negation="false" src="d0_s1" to="o177" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules membranous;</text>
      <biological_entity id="o179" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o180" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present or absent;</text>
      <biological_entity id="o181" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o182" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire, with hydathodes.</text>
      <biological_entity id="o183" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="blade" id="o184" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o185" name="hydathode" name_original="hydathodes" src="d0_s4" type="structure" />
      <relation from="o184" id="r14" name="with" negation="false" src="d0_s4" to="o185" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: flowers usually solitary, sometimes 2 [–3] per node.</text>
      <biological_entity id="o186" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o187" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="2" from_inclusive="false" modifier="sometimes" name="atypical_quantity" src="d0_s5" to="3" />
        <character constraint="per node" constraintid="o188" modifier="sometimes" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o188" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels present or absent.</text>
      <biological_entity id="o189" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 2–4, connate basally, equal or 1 smaller, not carinate, apex obtuse;</text>
      <biological_entity id="o190" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o191" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity id="o192" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (0 or) 2–4, apex obtuse;</text>
      <biological_entity id="o193" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o194" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o195" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens [0–] 1–8;</text>
      <biological_entity id="o196" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o197" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s9" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil 2–4-carpellate, ovary 2–4-locular, apex truncate;</text>
      <biological_entity id="o198" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o199" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-4-carpellate" value_original="2-4-carpellate" />
      </biological_entity>
      <biological_entity id="o200" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="2-4-locular" value_original="2-4-locular" />
      </biological_entity>
      <biological_entity id="o201" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 2–4;</text>
      <biological_entity id="o202" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o203" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 2–4.</text>
      <biological_entity id="o204" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o205" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules membranous.</text>
      <biological_entity id="o206" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–33 [–44] per locule, brown to yellowish-brown, straight or curved (nearly circular in E. californica), surface with hexagonal, rectangular, elliptic, or ± round pits (pits oriented with longer dimension at right angles to length of seed).</text>
      <biological_entity id="o207" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="33" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="44" />
        <character char_type="range_value" constraint="per locule" constraintid="o208" from="2" name="quantity" src="d0_s14" to="33" />
        <character char_type="range_value" from="brown" name="coloration" notes="" src="d0_s14" to="yellowish-brown" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o208" name="locule" name_original="locule" src="d0_s14" type="structure" />
      <biological_entity id="o210" name="pit" name_original="pits" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="true" name="shape" src="d0_s14" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" name="shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s14" value="round" value_original="round" />
      </biological_entity>
      <relation from="o209" id="r15" name="with" negation="false" src="d0_s14" to="o210" />
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o209" name="surface" name_original="surface" src="d0_s14" type="structure" />
      <biological_entity constraint="x" id="o211" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 25 (10 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America, Europe, Asia, Africa, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Duncan, W. H. 1964. New Elatine populations in the southeastern United States. Rhodora 66: 47–53.</reference>
    <reference>Fassett, N. C. 1939. Notes from the herbarium of the University of Wisconsin. No. 17. Elatine and other aquatics. Rhodora 41: 367–377.</reference>
    <reference>Fernald, M. L. 1917. The genus Elatine in eastern North America. Rhodora 19: 10–15.</reference>
    <reference>Fernald, M. L. 1941b. Elatine americana and E. triandra. Rhodora 43: 208–211.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 2; capsules 2-locular.</description>
      <determination>10. Elatine minima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually 3–4, sometimes 0; capsules 3–4-locular.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals 4; petals 4; capsules 4-locular.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Seeds curved 90–180°; pedicels 1.5–2.5(–3.5) mm, recurved in fruit; w United States.</description>
      <determination>1. Elatine californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Seeds straight or curved to 15º; pedicels 0.1–23 mm, erect; Quebec.</description>
      <determination>2. Elatine ojibwayensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals 2–3 (rarely 4 in E. triandra); petals 3 (sometimes 0 in E. ambigua); capsules 3-locular.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pedicels recurved in fruit, 0.5–2.5 mm.</description>
      <determination>6. Elatine ambigua</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pedicels erect, 0–0.5 mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stamens 1–6, number variable within plant.</description>
      <determination>9. Elatine heterandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stamens 3.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seed pits ± round, (9–)14–17 per row.</description>
      <determination>8. Elatine brachysperma</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seed pits angular-hexagonal, (13–)16–35 per row.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Seeds ellipsoid, pit length 3–5 times width; stipule margins entire.</description>
      <determination>7. Elatine chilensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Seeds oblong or slenderly cylindric, pit length 1–3 times width; stipule margins dentate.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves reddish green; seed pit length 1–2 times width.</description>
      <determination>5. Elatine rubella</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves light green to green; seed pit length 2–3 times width.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades linear, lanceolate or narrowly oblong, apices acute or obtuse.</description>
      <determination>3. Elatine triandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades obovate or broadly spatulate, apices rounded to shallowly emarginate.</description>
      <determination>4. Elatine americana</determination>
    </key_statement>
  </key>
</bio:treatment>