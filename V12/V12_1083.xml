<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">474</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHILADELPHUS</taxon_name>
    <taxon_name authority="S. Y. Hu" date="1956" rank="species">texensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">texensis</taxon_name>
    <taxon_hierarchy>family hydrangeaceae;genus philadelphus;species texensis;variety texensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250102017</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Philadelphus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">texensis</taxon_name>
    <taxon_name authority="S. Y. Hu" date="unknown" rank="variety">coryanus</taxon_name>
    <taxon_hierarchy>genus philadelphus;species texensis;variety coryanus</taxon_hierarchy>
  </taxon_identification>
  <number>2a.</number>
  <other_name type="common_name">Texas mock orange</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades (1–) 1.6–3.3 (–4.7) × (0.5–) 0.6–1 (–2.3) cm, abaxial surface loosely strigose, hairs 0.4–0.8 mm, with understory of white, more slender, tightly coiled-crisped hairs.</text>
      <biological_entity id="o10666" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s0" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s0" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s0" to="0.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s0" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10667" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o10668" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s0" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10669" name="understory" name_original="understory" src="d0_s0" type="structure" />
      <biological_entity id="o10670" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="true" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="true" modifier="tightly" name="shape" src="d0_s0" value="coiled-crisped" value_original="coiled-crisped" />
      </biological_entity>
      <relation from="o10668" id="r879" name="with" negation="false" src="d0_s0" to="o10669" />
      <relation from="o10669" id="r880" name="part_of" negation="false" src="d0_s0" to="o10670" />
    </statement>
  </description>
  <discussion>Variety texensis has a more westward range than var. ernestii; it is known from Bandera, Bexar, Edwards, Kendall, Medina, Real, and Uvalde counties in central Texas and is rare in the mountains of central Coahuila, Mexico. The two varieties are identical in all important characteristics, differing primarily in the presence or absence of the highly coiled-crisped of understory hairs on the abaxial leaf surfaces. There are several collections of var. texansis in which most leaves lack understory vestiture, as in var. ernestii, but the understory vestiture occurs on some leaves or branches indicating the expression or non-expression of a simple genetic trait. In both varieties, sepals can be glabrous or sparsely sericeous, even within a population.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone slopes and ravines, slopes in oak-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone slopes" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="oak-juniper woodlands" modifier="slopes in" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>