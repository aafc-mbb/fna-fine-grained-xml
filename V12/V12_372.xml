<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="Greene" date="1893" rank="species">pumilus</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>1: 149. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species pumilus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101454</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">prostratus</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">profugus</taxon_name>
    <taxon_hierarchy>genus ceanothus;species prostratus;variety profugus</taxon_hierarchy>
  </taxon_identification>
  <number>44.</number>
  <other_name type="common_name">Siskiyou mat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.1–0.4 m, matlike to moundlike.</text>
      <biological_entity id="o24100" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.4" to_unit="m" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to spreading, sometimes rooting at proximal nodes;</text>
      <biological_entity id="o24101" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="spreading" />
        <character constraint="at proximal nodes" constraintid="o24102" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24102" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets reddish-brown, flexible to ± rigid, tomentulose.</text>
      <biological_entity id="o24103" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled;</text>
      <biological_entity id="o24104" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–2 mm;</text>
      <biological_entity id="o24105" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat to ± cupped, slightly folded lengthwise adaxially, narrowly oblanceolate to narrowly oblong-oblanceolate, 5–15 × 3–6 mm, base cuneate, margins thick to ± revolute, usually denticulate near apex, sometimes entire, teeth (0 or) 2–3, apex usually truncate, sometimes obtuse, abaxial surface pale green, sparsely strigillose to glabrous, adaxial surface green to grayish green, dull, glabrous, sometimes glaucous.</text>
      <biological_entity id="o24106" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less; slightly; lengthwise adaxially; adaxially" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s5" to="narrowly oblong-oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24107" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24108" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character constraint="near apex" constraintid="o24109" is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24109" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o24110" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o24111" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24112" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s5" to="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24113" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="grayish green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, 1–1.7 cm.</text>
      <biological_entity id="o24114" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals, petals, and nectary pale blue to lavender.</text>
      <biological_entity id="o24115" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24116" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o24117" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o24118" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s7" to="lavender" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 4–6 mm wide, lobed;</text>
      <biological_entity id="o24119" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves smooth, horns subapical, minute or weakly developed bulges, intermediate ridges absent.</text>
      <biological_entity id="o24120" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o24121" name="horn" name_original="horns" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o24122" name="bulge" name_original="bulges" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="weakly" name="development" src="d0_s9" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o24123" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="intermediate" value_original="intermediate" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24124" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus pumilus is endemic to the Klamath Mountains, where it occurs strictly on serpentine soils; it sometimes has been confused with C. arcuatus and C. prostratus, from which it differs principally by its oblanceolate to oblong-lanceolate leaf blades with a truncate, 3-toothed apex.</discussion>
  <discussion>Hybrids between Ceanothus pumilus and C. cuneatus have been called C. ×humboldtensis Roof.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils derived from serpentine, open flats and slopes, chaparral, conifer forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="serpentine" />
        <character name="habitat" value="open flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="forests" modifier="conifer" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>