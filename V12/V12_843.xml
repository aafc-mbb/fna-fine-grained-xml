<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MALPIGHIACEAE</taxon_name>
    <taxon_name authority="Richard" date="1815" rank="genus">ASPICARPA</taxon_name>
    <taxon_name authority="A. Gray" date="1850" rank="species">hyssopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 167. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malpighiaceae;genus aspicarpa;species hyssopifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101688</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect or spreading-ascending, 8–30 cm.</text>
      <biological_entity id="o15447" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 0.5 mm;</text>
      <biological_entity id="o15448" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15449" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade narrowly lanceolate, larger blades 10–25 × 2–8 mm, length (2.2–) 2.7–7 (–8.3) times width, base rounded, apex acute or slightly obtuse, surfaces thinly sericeous to glabrescent, hairs persisting longest on margins and abaxial midrib.</text>
      <biological_entity id="o15450" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15451" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="larger" id="o15452" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="(2.2-)2.7-7(-8.3)" value_original="(2.2-)2.7-7(-8.3)" />
      </biological_entity>
      <biological_entity id="o15453" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15454" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o15455" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="thinly sericeous" name="pubescence" src="d0_s2" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o15456" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character constraint="on abaxial midrib" constraintid="o15458" is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity id="o15457" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o15458" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Chasmogamous flowers borne singly in axils of full-sized leaves;</text>
      <biological_entity id="o15459" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o15460" name="axil" name_original="axils" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o15461" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="full-sized" value_original="full-sized" />
      </biological_entity>
      <relation from="o15459" id="r1303" name="borne" negation="false" src="d0_s3" to="o15460" />
      <relation from="o15459" id="r1304" name="part_of" negation="false" src="d0_s3" to="o15461" />
    </statement>
    <statement id="d0_s4">
      <text>petals lemon yellow, outermost sometimes with red blotch;</text>
      <biological_entity id="o15462" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="lemon yellow" value_original="lemon yellow" />
        <character constraint="with blotch" constraintid="o15463" is_modifier="false" name="position" src="d0_s4" value="outermost" value_original="outermost" />
      </biological_entity>
      <biological_entity id="o15463" name="blotch" name_original="blotch" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminodes shorter than fertile stamens, ± hidden by sepals.</text>
      <biological_entity id="o15464" name="staminode" name_original="staminodes" src="d0_s5" type="structure">
        <character constraint="than fertile stamens" constraintid="o15465" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character constraint="by sepals" constraintid="o15466" is_modifier="false" modifier="more or less" name="prominence" src="d0_s5" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o15465" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o15466" name="sepal" name_original="sepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Cleistogamous flowers sessile or subsessile in axils of full-sized leaves, sometimes absent.</text>
      <biological_entity id="o15467" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character constraint="in axils" constraintid="o15468" is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15468" name="axil" name_original="axils" src="d0_s6" type="structure" />
      <biological_entity id="o15469" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="full-sized" value_original="full-sized" />
      </biological_entity>
      <relation from="o15468" id="r1305" name="part_of" negation="false" src="d0_s6" to="o15469" />
    </statement>
    <statement id="d0_s7">
      <text>Nutlet 2.5–3 mm diam., rugose or bearing toothlike protuberances, dorsal crest 0.5–1 mm, coarsely toothed, extended forward 1.5–2 mm. 2n = 80.</text>
      <biological_entity id="o15470" name="nutlet" name_original="nutlet" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s7" value="rugose" value_original="rugose" />
        <character name="relief" src="d0_s7" value="bearing toothlike protuberances" value_original="bearing toothlike protuberances" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o15471" name="crest" name_original="crest" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="forward" name="size" src="d0_s7" value="extended" value_original="extended" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15472" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Aspicarpa hyssopifolia is found only in southern and western Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brushy, dry, calcareous slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brushy" />
        <character name="habitat" value="slopes" modifier="dry calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>