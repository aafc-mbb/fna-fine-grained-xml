<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="illustration_page">182</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <taxon_name authority="Cavanilles" date="1800" rank="species">nepetifolia</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>6: 37, plate 557, fig. 1. 1800</place_in_publication>
      <other_info_on_pub>(as nepetaefolia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species nepetifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101939</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Catnip noseburn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 1.5–5 dm.</text>
      <biological_entity id="o24757" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to trailing, green to reddish green, apex never flexuous.</text>
      <biological_entity id="o24758" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="trailing" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="reddish green" />
      </biological_entity>
      <biological_entity id="o24759" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 3–25 (–41) mm;</text>
      <biological_entity id="o24760" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24761" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="41" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade triangular to ovate [linear], proximal broadly ovate to sometimes suborbiculate, 1.8–5 × 0.9–3.6 cm, often red-green, base truncate to cordate, margins coarsely dentate to coarsely serrate, apex acute.</text>
      <biological_entity id="o24762" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24763" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s3" to="ovate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24764" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate to sometimes" value_original="ovate to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s3" to="3.6" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="red-green" value_original="red-green" />
      </biological_entity>
      <biological_entity id="o24765" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
      <biological_entity id="o24766" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="dentate to coarsely" value_original="dentate to coarsely" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="dentate to coarsely" value_original="dentate to coarsely" />
      </biological_entity>
      <biological_entity id="o24767" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal (often appearing leaf-opposed), glands sessile or absent, staminate flowers 8–40 per raceme, distally clustered [evenly distributed];</text>
      <biological_entity id="o24768" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o24769" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24770" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per raceme" constraintid="o24771" from="8" name="quantity" src="d0_s4" to="40" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_growth_form" notes="" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o24771" name="raceme" name_original="raceme" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>staminate bracts 1.3–1.6 mm.</text>
      <biological_entity id="o24772" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate 1.4–1.7 mm, persistent base 0.5–0.7 mm;</text>
      <biological_entity id="o24773" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24774" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 2.9–3.3 mm in fruit.</text>
      <biological_entity id="o24775" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o24776" from="2.9" from_unit="mm" name="some_measurement" src="d0_s7" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24776" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3–4, reddish green, 1–2 mm;</text>
      <biological_entity id="o24777" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24778" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish green" value_original="reddish green" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3–4, filaments 0.3–0.6 mm.</text>
      <biological_entity id="o24779" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24780" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o24781" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals lanceolate [ovate], 1.4–2.3 mm;</text>
      <biological_entity id="o24782" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24783" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles connate 1/4–1/3 length;</text>
      <biological_entity id="o24784" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24785" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s11" to="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas papillate.</text>
      <biological_entity id="o24786" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24787" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 6–8 mm wide.</text>
      <biological_entity id="o24788" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds brownish black, 3–4 mm.</text>
      <biological_entity id="o24789" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brownish black" value_original="brownish black" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tragia nepetifolia is typically found at high elevations in Mexico and the southwestern United States. Since it was described more than 200 years ago, many collections of Tragia in Mexico and the United States have been identified mistakenly as this species.</discussion>
  <discussion>Tragia nepetifolia includes four varieties in Mexico, but none match plants occurring in the United States. These most closely resemble var. dissecta Müller Arg. of western Mexico, sharing inflorescences with distally clustered staminate flowers and a tendency toward reddish coloration, but differing in that their leaf blades are not as deeply toothed.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring; fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine-oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine-oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex.; Mexico, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>