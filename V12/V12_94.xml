<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">VITACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VITIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Vitis</taxon_name>
    <taxon_name authority="Buckley" date="1862" rank="species">monticola</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>13: 450. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family vitaceae;genus vitis;subgenus vitis;species monticola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101316</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vitis</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">aestivalis</taxon_name>
    <taxon_name authority="(Buckley) Engelmann" date="unknown" rank="variety">monticola</taxon_name>
    <taxon_hierarchy>genus vitis;species aestivalis;variety monticola</taxon_hierarchy>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Sweet mountain grape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate to high climbing, sparsely branched.</text>
      <biological_entity id="o16281" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="moderate" value_original="moderate" />
        <character is_modifier="false" name="height" src="d0_s0" value="high" value_original="high" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branches: bark exfoliating in shreds;</text>
      <biological_entity id="o16282" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o16283" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character constraint="in shreds" constraintid="o16284" is_modifier="false" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o16284" name="shred" name_original="shreds" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodal diaphragms 1–2.5 mm thick;</text>
      <biological_entity id="o16285" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity constraint="nodal" id="o16286" name="diaphragm" name_original="diaphragms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets gray to green or brown, if purplish only on one side, terete, sparsely arachnoid or glabrous, growing tips not enveloped by unfolding leaves, sparsely to densely hairy;</text>
      <biological_entity id="o16287" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o16288" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s3" to="green or brown" />
        <character constraint="on side" constraintid="o16289" is_modifier="false" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="by leaves" constraintid="o16291" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="enveloped" value_original="enveloped" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" notes="" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16289" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o16290" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <biological_entity id="o16291" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <relation from="o16288" id="r1358" name="growing" negation="false" src="d0_s3" to="o16290" />
    </statement>
    <statement id="d0_s4">
      <text>tendrils along length of branchlets, persistent, branched, tendrils (or inflorescences) at only 2 consecutive nodes;</text>
      <biological_entity id="o16292" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <biological_entity id="o16293" name="tendril" name_original="tendrils" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o16294" name="branchlet" name_original="branchlets" src="d0_s4" type="structure" />
      <biological_entity id="o16295" name="tendril" name_original="tendrils" src="d0_s4" type="structure" />
      <biological_entity id="o16296" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="consecutive" value_original="consecutive" />
      </biological_entity>
      <relation from="o16293" id="r1359" name="along" negation="false" src="d0_s4" to="o16294" />
      <relation from="o16295" id="r1360" name="at" negation="false" src="d0_s4" to="o16296" />
    </statement>
    <statement id="d0_s5">
      <text>nodes not red-banded.</text>
      <biological_entity id="o16297" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o16298" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s5" value="red-banded" value_original="red-banded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves: stipules 1.5–3 mm;</text>
      <biological_entity id="o16299" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o16300" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole 1/2 blade;</text>
      <biological_entity id="o16301" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o16302" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o16303" name="blade" name_original="blade" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>blade cordate, 5–8 (–10) cm, unlobed or shallowly 3-lobed, apex acute to short acuminate, abaxial surface not glaucous, glabrous or sparsely hirtellous, visible through hairs, adaxial surface usually glabrous.</text>
      <biological_entity id="o16304" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o16305" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o16306" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="short acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16307" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
        <character constraint="through hairs" constraintid="o16308" is_modifier="false" name="prominence" src="d0_s8" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o16308" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o16309" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 3–7 cm.</text>
      <biological_entity id="o16310" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers functionally unisexual.</text>
      <biological_entity id="o16311" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries black, usually not, sometimes very slightly, glaucous, globose, 8–10 mm diam., skin separating from pulp;</text>
      <biological_entity id="o16312" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" modifier="usually not; not; sometimes very; very slightly; slightly" name="pubescence" src="d0_s11" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16313" name="skin" name_original="skin" src="d0_s11" type="structure">
        <character constraint="from pulp" constraintid="o16314" is_modifier="false" name="arrangement" src="d0_s11" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o16314" name="pulp" name_original="pulp" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lenticels usually present.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 38.</text>
      <biological_entity id="o16315" name="lenticel" name_original="lenticels" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16316" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vitis monticola is endemic to dry areas on the Edwards Plateau.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone hills and ridges.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hills" modifier="limestone" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>