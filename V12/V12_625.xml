<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">506</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="(Osterhout) H. J. Thompson &amp; Prigge" date="1986" rank="species">marginata</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist,</publication_title>
      <place_in_publication>46: 549. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species marginata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101809</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuttallia</taxon_name>
    <taxon_name authority="Osterhout" date="1922" rank="species">marginata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>49: 183. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus nuttallia;species marginata</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Colorado blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual or biennial, candelabra-form.</text>
      <biological_entity id="o7140" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, erect, straight;</text>
      <biological_entity id="o7141" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches mostly distal, proximal longer than distal, all usually extending to near the distal end of plant, antrorse, straight;</text>
      <biological_entity id="o7142" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o7143" name="end" name_original="end" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7145" name="end" name_original="end" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o7146" name="end" name_original="end" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o7147" name="plant" name_original="plant" src="d0_s2" type="structure" />
      <relation from="o7144" id="r624" modifier="usually" name="extending to" negation="false" src="d0_s2" to="o7146" />
      <relation from="o7144" id="r625" modifier="usually" name="extending to" negation="false" src="d0_s2" to="o7147" />
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="proximal" id="o7144" name="end" name_original="end" src="d0_s2" type="structure">
        <character constraint="than distal end" constraintid="o7145" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 22–115 × 2.8–10.9 (–17.2) mm, widest intersinus distance 2.2–7 (–9) mm;</text>
      <biological_entity id="o7148" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7149" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" name="length" src="d0_s4" to="115" to_unit="mm" />
        <character char_type="range_value" from="10.9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="17.2" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s4" to="10.9" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate, elliptic, or lanceolate, margins dentate, teeth 10–24 (–38), perpendicular to leaf axis, 0.5–4 mm;</text>
      <biological_entity id="o7150" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o7151" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o7152" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o7153" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="24" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="38" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="24" />
        <character constraint="to leaf axis" constraintid="o7154" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o7154" name="axis" name_original="axis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal elliptic to lanceolate, base not clasping, margins dentate, teeth 8–20, perpendicular to leaf axis, 0.3–5.6 mm;</text>
      <biological_entity id="o7155" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o7156" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o7157" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o7158" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o7159" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="20" />
        <character constraint="to leaf axis" constraintid="o7160" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="5.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o7160" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with simple grappling-hook, complex grappling-hook, and usually with needlelike trichomes, adaxial surface with simple grappling-hook and needlelike trichomes.</text>
      <biological_entity id="o7161" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o7162" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o7163" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o7164" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o7165" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7166" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o7167" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o7168" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o7162" id="r626" name="with" negation="false" src="d0_s7" to="o7163" />
      <relation from="o7164" id="r627" modifier="usually" name="with" negation="false" src="d0_s7" to="o7165" />
      <relation from="o7166" id="r628" name="with" negation="false" src="d0_s7" to="o7167" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o7169" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o7170" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 8–14.4 × 2.1–3.9 mm, apex acute, hairy abaxially;</text>
      <biological_entity id="o7171" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7172" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="14.4" to_unit="mm" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="width" src="d0_s9" to="3.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7173" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 5.5–11.1 × 1.2–2.9 mm, with anthers, second whorl with anthers;</text>
      <biological_entity id="o7174" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7175" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o7176" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o7177" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s10" to="11.1" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="2.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7178" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o7179" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o7180" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o7177" id="r629" name="with" negation="false" src="d0_s10" to="o7178" />
      <relation from="o7179" id="r630" name="with" negation="false" src="d0_s10" to="o7180" />
    </statement>
    <statement id="d0_s11">
      <text>anthers straight after dehiscence, epidermis papillate;</text>
      <biological_entity id="o7181" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7182" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o7183" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.3–10 mm.</text>
      <biological_entity id="o7184" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7185" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.3" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cylindric, 7–14.6 × 3.5–6.7 mm, base tapering, not longitudinally ridged.</text>
      <biological_entity id="o7186" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="14.6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s13" to="6.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7187" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls straight to slightly wavy, papillae 4–10 per cell.</text>
      <biological_entity id="o7188" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o7189" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o7190" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o7192" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 20.</text>
      <biological_entity id="o7191" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o7192" from="4" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7193" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia marginata occurs on the western edge of Colorado in Delta, Garfield, Mesa, and Montrose counties. Reports of this species from Utah are based on specimens treated here as M. cronquistii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep roadside banks, steep cliffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep roadside banks" />
        <character name="habitat" value="steep cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>