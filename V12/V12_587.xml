<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">397</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="(A. Gray) Small in N. L. Britton et al." date="1907" rank="genus">HESPEROLINON</taxon_name>
    <taxon_name authority="(A. Gray) Small in N. L. Britton et al." date="1907" rank="species">congestum</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>25: 86. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus hesperolinon;species congestum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101745</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linum</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">congestum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 521. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus linum;species congestum</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Marin dwarf flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, (5–) 15–30 (–45) cm, glabrous or glabrate;</text>
      <biological_entity id="o21957" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches from distal nodes, alternate, strongly virgate (proximal unbranched main axis usually long in comparison to branched distal portion).</text>
      <biological_entity id="o21958" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="virgate" value_original="virgate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21959" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o21958" id="r1806" name="from" negation="false" src="d0_s1" to="o21959" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o21960" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands present;</text>
      <biological_entity constraint="stipular" id="o21961" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, (5–) 15–25 × 1–1.5 (–2) mm, base flat, not clasping, margins eglandular or glands minute.</text>
      <biological_entity id="o21962" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21963" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o21964" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o21965" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: cymes monochasial, dense, sparingly branched, internodes sometimes all condensed or long proximally and condensed distally, flowers condensed at apices;</text>
      <biological_entity id="o21966" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o21967" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="monochasial" value_original="monochasial" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s5" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21968" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="condensed" value_original="condensed" />
        <character is_modifier="false" modifier="proximally" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s5" value="condensed" value_original="condensed" />
      </biological_entity>
      <biological_entity id="o21969" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="at apices" constraintid="o21970" is_modifier="false" name="architecture" src="d0_s5" value="condensed" value_original="condensed" />
      </biological_entity>
      <biological_entity id="o21970" name="apex" name_original="apices" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bract margins eglandular.</text>
      <biological_entity id="o21971" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="bract" id="o21972" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–2 (–5) mm, scarcely longer in fruit, ascending, not bent at apex.</text>
      <biological_entity id="o21973" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character constraint="in fruit" constraintid="o21974" is_modifier="false" modifier="scarcely" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="ascending" value_original="ascending" />
        <character constraint="at apex" constraintid="o21975" is_modifier="false" modifier="not" name="shape" src="d0_s7" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o21974" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o21975" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals erect, ± reflexed at tip, lanceolate, 3–4 mm, equal, margins minutely glandular, surfaces hairy, hairs intertwined or matted;</text>
      <biological_entity id="o21976" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o21977" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="at tip" constraintid="o21978" is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o21978" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o21979" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o21980" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21981" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="intertwined" value_original="intertwined" />
        <character is_modifier="false" name="growth_form" src="d0_s8" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals ± reflexed, pink to rose, oblanceolate, (3–) 6–7 (–8) mm, apex obtuse, sometimes slightly notched;</text>
      <biological_entity id="o21982" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21983" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="rose" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21984" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s9" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cup white, rim with petal attachments set in deep notches between filaments;</text>
      <biological_entity id="o21985" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21986" name="cup" name_original="cup" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21987" name="rim" name_original="rim" src="d0_s10" type="structure" />
      <biological_entity constraint="attachments" id="o21988" name="set" name_original="set" src="d0_s10" type="structure" constraint_original="petal attachments" />
      <biological_entity constraint="between filaments" constraintid="o21990" id="o21989" name="notch" name_original="notches" src="d0_s10" type="structure" constraint_original="between  filaments, ">
        <character is_modifier="true" name="depth" src="d0_s10" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o21990" name="all" name_original="filaments" src="d0_s10" type="structure" />
      <relation from="o21987" id="r1807" name="with" negation="false" src="d0_s10" to="o21988" />
      <relation from="o21988" id="r1808" name="in" negation="false" src="d0_s10" to="o21989" />
    </statement>
    <statement id="d0_s11">
      <text>stamens exserted;</text>
      <biological_entity id="o21991" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21992" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 4–5 mm;</text>
      <biological_entity id="o21993" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21994" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers rose to purple, dehisced anthers 1.5–2 mm;</text>
      <biological_entity id="o21995" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21996" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s13" to="purple" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21997" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o21996" id="r1809" name="dehisced" negation="false" src="d0_s13" to="o21997" />
    </statement>
    <statement id="d0_s14">
      <text>ovary chambers 6;</text>
      <biological_entity id="o21998" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="ovary" id="o21999" name="chamber" name_original="chambers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, white, (3–) 4–4.5 (–5) mm, exserted.</text>
      <biological_entity id="o22000" name="flower" name_original="flowers" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 36.</text>
      <biological_entity id="o22001" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22002" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hesperolinon congestum is known from a narrow band within the Outer Coast Ranges from Marin County south to San Mateo County. It can be distinguished from all other species in the genus by its hairy sepals.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands on serpentine soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soils" modifier="grasslands on" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>