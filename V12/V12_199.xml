<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">389</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="(Reichenbach) Engelmann" date="1852" rank="section">Linopsis</taxon_name>
    <taxon_name authority="Sivinski &amp; M. O. Howard" date="2011" rank="species">allredii</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2011-33: 1, figs. 1, 3. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linopsis;species allredii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101717</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Allred's flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, to 25 cm, puberulent or glabrescent in proximal 1/3, otherwise glabrous;</text>
      <biological_entity id="o9240" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character constraint="in proximal 1/3" constraintid="o9241" is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9241" name="1/3" name_original="1/3" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>roots relatively thick, lateral.</text>
      <biological_entity id="o9242" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="position" src="d0_s1" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems stiffly ascending, suffrutescent from woody branching base.</text>
      <biological_entity id="o9243" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="from base" constraintid="o9244" is_modifier="false" name="growth_form" src="d0_s2" value="suffrutescent" value_original="suffrutescent" />
      </biological_entity>
      <biological_entity id="o9244" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, tightly appressed or ascending to spreading;</text>
      <biological_entity id="o9245" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipular glands present throughout, dark;</text>
      <biological_entity constraint="stipular" id="o9246" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to linear-lanceolate, proximal and midstem leaves 3–10 (–12) mm, distal leaves 3–7 × 0.6–1 mm, margins of proximal and midstem leaves entire, distal serrulate, teeth usually gland-tipped, not ciliate, apex of proximal and midstem leaves mucronate, distal acuminate-aristate.</text>
      <biological_entity id="o9247" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o9248" name="leaf" name_original="leaves" src="d0_s5" type="structure" constraint_original="proximal and midstem">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9249" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9250" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o9251" name="proximal" name_original="proximal" src="d0_s5" type="structure" />
      <biological_entity constraint="midstem" id="o9252" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o9253" name="leaf" name_original="leaves" src="d0_s5" type="structure" constraint_original="distal and midstem">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o9254" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9255" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o9256" name="leaf" name_original="leaves" src="d0_s5" type="structure" constraint_original="proximal and midstem" />
      <biological_entity constraint="midstem" id="o9257" name="leaf" name_original="leaves" src="d0_s5" type="structure" constraint_original="distal and midstem">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate-aristate" value_original="acuminate-aristate" />
      </biological_entity>
      <relation from="o9250" id="r763" name="part_of" negation="false" src="d0_s5" to="o9251" />
      <relation from="o9255" id="r764" name="part_of" negation="false" src="d0_s5" to="o9256" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences few-flowered panicles;</text>
      <biological_entity id="o9258" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o9259" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts with irregular scarious margins.</text>
      <biological_entity id="o9260" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o9261" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s7" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o9260" id="r765" name="with" negation="false" src="d0_s7" to="o9261" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–3 mm, conspicuously articulated.</text>
      <biological_entity id="o9262" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s8" value="articulated" value_original="articulated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals deciduous, lanceolate, 4.5–7 mm, margins not scarious, glandular-toothed, apex acute-aristate, glabrous;</text>
      <biological_entity id="o9263" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9264" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9265" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s9" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o9266" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute-aristate" value_original="acute-aristate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>prominently 1-nerved;</text>
      <biological_entity id="o9267" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pumpkin yellow with a wide, pale, red band distal to a deeply wine red band at base, broadly obovate, 10–13 mm;</text>
      <biological_entity id="o9268" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9269" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character constraint="with band" constraintid="o9270" is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s11" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9270" name="band" name_original="band" src="d0_s11" type="structure">
        <character is_modifier="true" name="width" src="d0_s11" value="wide" value_original="wide" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character constraint="to band" constraintid="o9271" is_modifier="false" name="position_or_shape" src="d0_s11" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o9271" name="band" name_original="band" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="deeply" name="coloration" src="d0_s11" value="wine red" value_original="wine red" />
      </biological_entity>
      <biological_entity id="o9272" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o9271" id="r766" name="at" negation="false" src="d0_s11" to="o9272" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 5.5–7 mm;</text>
      <biological_entity id="o9273" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9274" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1.2–1.6 mm;</text>
      <biological_entity id="o9275" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9276" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>staminodia absent;</text>
      <biological_entity id="o9277" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o9278" name="staminodium" name_original="staminodia" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles connate nearly to apex, 7–9 mm;</text>
      <biological_entity id="o9279" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o9280" name="style" name_original="styles" src="d0_s15" type="structure">
        <character constraint="to apex" constraintid="o9281" is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9281" name="apex" name_original="apex" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stigmas dark, capitate.</text>
      <biological_entity id="o9282" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o9283" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark" value_original="dark" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules ovoid, 3.7–4 × 3 mm, apex obtuse, dehiscing into 5, 2-seeded segments, segments persistent on plant, false septa complete, translucent, proximal part membranaceous, not terminating in loose fringe, distal part cartilaginous, margins ciliate.</text>
      <biological_entity id="o9284" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s17" to="4" to_unit="mm" />
        <character name="width" src="d0_s17" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o9285" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="obtuse" value_original="obtuse" />
        <character constraint="into segments" constraintid="o9286" is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o9286" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="2-seeded" value_original="2-seeded" />
      </biological_entity>
      <biological_entity id="o9287" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character constraint="on plant" constraintid="o9288" is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o9288" name="plant" name_original="plant" src="d0_s17" type="structure" />
      <biological_entity constraint="false" id="o9289" name="septum" name_original="septa" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="complete" value_original="complete" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s17" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9290" name="part" name_original="part" src="d0_s17" type="structure">
        <character is_modifier="false" name="texture" src="d0_s17" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity id="o9291" name="fringe" name_original="fringe" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s17" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9292" name="part" name_original="part" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence_or_texture" src="d0_s17" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity id="o9293" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s17" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o9290" id="r767" name="terminating in" negation="false" src="d0_s17" to="o9291" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds 2.4–2.7 × 0.9–1.1 mm.</text>
      <biological_entity id="o9294" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s18" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s18" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Linum allredii is a rare endemic known from seven to 12 occurrences in the Yeso Hills border region of New Mexico and Texas, apparently restricted to gypsum soils. When Sivinski and Howard described this species, they noted that it occurs only on pale, sandy, biologically crusted gypsum distinct from adjacent, darker gypsum. The corollas are deeply bowl-shaped. The filaments and styles are the same pumpkin color as the petals, the stigmas are dark maroon, and the pollen is bright yellow. Linum allredii and L. kingii are the only species in the flora area growing from a woody base.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gypsum soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" modifier="gypsum" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>