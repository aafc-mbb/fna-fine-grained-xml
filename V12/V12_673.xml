<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Lynn J. Gillespie</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MERCURIALIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1035. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 457. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus mercurialis</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin Mercurius, Roman mythological deity, and -alis, belonging to, alluding to belief that it was discovered by him</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">120266</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Mercury</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual [perennial], dioecious or monoecious;</text>
      <biological_entity id="o26203" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs unbranched;</text>
      <biological_entity id="o26204" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex absent.</text>
      <biological_entity id="o26205" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, simple;</text>
      <biological_entity id="o26206" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, persistent;</text>
      <biological_entity id="o26207" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present [absent], glands present at apex;</text>
      <biological_entity id="o26208" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26209" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character constraint="at apex" constraintid="o26210" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26210" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed, margins serrate or crenate, laminar glands absent;</text>
      <biological_entity id="o26211" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o26212" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation pinnate.</text>
      <biological_entity constraint="laminar" id="o26213" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually unisexual, rarely bisexual, axillary, staminate elongate spikelike thyrses, pistillate and bisexual fascicles or cymules [short spikelike thyrses];</text>
      <biological_entity id="o26214" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o26215" name="thyrse" name_original="thyrses" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o26216" name="cymule" name_original="cymules" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>glands subtending each bract 0.</text>
      <biological_entity id="o26217" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o26218" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels: staminate rudimentary or absent, pistillate present.</text>
      <biological_entity id="o26219" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals 3, valvate, distinct;</text>
      <biological_entity id="o26220" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26221" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o26222" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26223" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary absent;</text>
      <biological_entity id="o26224" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26225" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 8–12 (–20), distinct;</text>
      <biological_entity id="o26226" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26227" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="20" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="12" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistillode absent.</text>
      <biological_entity id="o26228" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26229" name="pistillode" name_original="pistillode" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: sepals 3, distinct;</text>
      <biological_entity id="o26230" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26231" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 0;</text>
      <biological_entity id="o26232" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26233" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>nectary 2 glands;</text>
      <biological_entity id="o26234" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26235" name="nectary" name_original="nectary" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26236" name="gland" name_original="glands" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>pistil 2-carpellate;</text>
      <biological_entity id="o26237" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26238" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 2, distinct or connate basally, unbranched.</text>
      <biological_entity id="o26239" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26240" name="style" name_original="styles" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, hispid [glabrous].</text>
      <biological_entity constraint="fruits" id="o26241" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s21" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ovoid;</text>
      <biological_entity id="o26242" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>caruncle present.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 8.</text>
      <biological_entity id="o26243" name="caruncle" name_original="caruncle" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o26244" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 10 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, Calif.; Eurasia, n Africa, Atlantic Islands (Macaronesia); introduced also in s South America, s Africa, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="native" />
        <character name="distribution" value="also in s South America" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>