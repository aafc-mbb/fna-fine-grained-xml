<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Engelmann ex Chapman" date="1860" rank="species">curtisii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>401. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species curtisii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101534</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">eriogonoides</taxon_name>
    <taxon_hierarchy>genus euphorbia;species eriogonoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tithymalopsis</taxon_name>
    <taxon_name authority="(Engelmann ex Chapman) Small" date="unknown" rank="species">curtisii</taxon_name>
    <taxon_hierarchy>genus tithymalopsis;species curtisii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">T.</taxon_name>
    <taxon_name authority="(Small) Small" date="unknown" rank="species">eriogonoides</taxon_name>
    <taxon_hierarchy>genus t.;species eriogonoides</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Curtis’s or sandhills spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with spreading rootstock.</text>
      <biological_entity id="o8533" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o8534" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o8533" id="r702" name="with" negation="false" src="d0_s0" to="o8534" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, branched, solitary or few, previous years dead stems not persistent, 20–40 cm, usually glabrous, rarely strigose to sericeous at nodes.</text>
      <biological_entity id="o8535" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="growth_order" src="d0_s1" value="previous" value_original="previous" />
      </biological_entity>
      <biological_entity id="o8536" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="dead" value_original="dead" />
        <character is_modifier="false" modifier="not" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" constraint="at nodes" constraintid="o8537" from="rarely strigose" name="pubescence" src="d0_s1" to="sericeous" />
      </biological_entity>
      <biological_entity id="o8537" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o8538" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules to 0.1 mm;</text>
      <biological_entity id="o8539" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole to (0–) 1–2 mm, glabrous or strigose to sericeous;</text>
      <biological_entity id="o8540" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="strigose" modifier="to (0-)1-2 mm" name="pubescence" src="d0_s4" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually linear, occasionally elliptic, rarely ovate, proximal often greatly reduced and often scalelike, 10–30 × 1.5–6 mm, base cuneate, margins entire, occasionally sparsely ciliate, apex rounded or broadly acute, abaxial surface glabrous or sparsely strigose to sericeous, adaxial surface glabrous;</text>
      <biological_entity id="o8541" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="occasionally" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="often greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8542" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o8543" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="occasionally sparsely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8544" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8545" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s5" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation obscure, only midvein conspicuous.</text>
      <biological_entity constraint="adaxial" id="o8546" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o8547" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in terminal pleiochasia (fertile axillary branches occasionally present);</text>
      <biological_entity id="o8548" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o8549" name="pleiochasium" name_original="pleiochasia" src="d0_s7" type="structure" />
      <relation from="o8548" id="r703" name="in" negation="false" src="d0_s7" to="o8549" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 6.5–17 mm, filiform, glabrous.</text>
      <biological_entity id="o8550" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1–1.2 × 1.3–1.5 (–1.7) mm, glabrous or strigose to sericeous on distal 1/2;</text>
      <biological_entity id="o8551" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character char_type="range_value" constraint="on distal 1/2" constraintid="o8552" from="strigose" name="pubescence" src="d0_s9" to="sericeous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8552" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>glands 5, green, reniform, 0.3 × 0.6 mm;</text>
      <biological_entity id="o8553" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="reniform" value_original="reniform" />
        <character name="length" src="d0_s10" unit="mm" value="0.3" value_original="0.3" />
        <character name="width" src="d0_s10" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white, semicircular, 0.3–0.4 × 0.6–0.8 mm, entire.</text>
      <biological_entity id="o8554" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="semicircular" value_original="semicircular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s11" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 20–25.</text>
      <biological_entity id="o8555" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous or sparsely strigose to sericeous;</text>
      <biological_entity id="o8556" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8557" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s13" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.6–1.1 mm, 2-fid at apex to 1/2 length.</text>
      <biological_entity id="o8558" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8559" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
        <character constraint="at apex" constraintid="o8560" is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o8560" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" name="length" src="d0_s14" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules globose, 2.5–3.2 × 4.3–5.1 mm, glabrous or sparsely strigose to sericeous;</text>
      <biological_entity id="o8561" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="4.3" from_unit="mm" name="width" src="d0_s15" to="5.1" to_unit="mm" />
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s15" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 2.4–3.1 mm.</text>
      <biological_entity id="o8562" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s16" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds usually gray to black, occasionally brown, ovoid-globose, 2.2 × 1.8 mm, smooth;</text>
      <biological_entity id="o8563" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="usually gray" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid-globose" value_original="ovoid-globose" />
        <character name="length" src="d0_s17" unit="mm" value="2.2" value_original="2.2" />
        <character name="width" src="d0_s17" unit="mm" value="1.8" value_original="1.8" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o8564" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia curtisii is found in the Gulf and Atlantic coastal plains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting early spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="early spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Xeric to dry oak or oak-pine scrub of sand hills, pine-oak woodlands, pine-oak savannas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry oak" />
        <character name="habitat" value="oak-pine scrub" constraint="of sand" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="xeric to dry oak" />
        <character name="habitat" value="oak-pine scrub of sand hills" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="pine-oak savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>