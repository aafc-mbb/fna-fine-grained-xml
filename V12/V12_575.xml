<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="L. C. Wheeler" date="1939" rank="species">innocua</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>127: 62, plate 3, fig. D. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species innocua;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101538</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Velvet spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with moderately to strongly thickened rootstock.</text>
      <biological_entity id="o15238" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o15239" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="moderately; strongly" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o15238" id="r1282" name="with" negation="false" src="d0_s0" to="o15239" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to decumbent or ascending, branched (often near base), 7–45 cm, densely pilose.</text>
      <biological_entity id="o15240" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o15241" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules to 0.1 mm;</text>
      <biological_entity id="o15242" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0.7–) 1.1–3.5 mm, pilose;</text>
      <biological_entity id="o15243" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to orbiculate, 4.6–17 (–25) × 4.5–15 (–19) mm, base cordate, margins entire, apex rounded to obtuse, surfaces densely pilose;</text>
      <biological_entity id="o15244" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="length" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="19" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15245" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o15246" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15247" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation obscure, usually only midvein conspicuous.</text>
      <biological_entity id="o15248" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o15249" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in terminal dichasia (often weakly defined);</text>
      <biological_entity id="o15250" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o15251" name="dichasium" name_original="dichasia" src="d0_s7" type="structure" />
      <relation from="o15250" id="r1283" name="in" negation="false" src="d0_s7" to="o15251" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 1–2.7 mm, densely pilose.</text>
      <biological_entity id="o15252" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2.7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1–1.3 × 1.2–1.4 mm, pilose;</text>
      <biological_entity id="o15253" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellow to green, elliptic, 0.2–0.3 × 0.5–0.6 mm;</text>
      <biological_entity id="o15254" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="green" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s10" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages green, elliptic, 0.4–0.5 × 0.5–0.9 mm, entire or crenulate, ciliate.</text>
      <biological_entity id="o15255" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s11" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 5–10.</text>
      <biological_entity id="o15256" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary pilose;</text>
      <biological_entity id="o15257" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15258" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.4–0.7 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o15259" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15260" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules depressed-ovoid, 2–2.5 × 2.7–3.3 mm, pilose;</text>
      <biological_entity id="o15261" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-ovoid" value_original="depressed-ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s15" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.6–2.1 mm.</text>
      <biological_entity id="o15262" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds gray to brown, ovoid, 1.5–1.7 × 1.2–1.3 mm, rugose with whitish ridges;</text>
      <biological_entity id="o15263" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s17" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s17" to="1.3" to_unit="mm" />
        <character constraint="with ridges" constraintid="o15264" is_modifier="false" name="relief" src="d0_s17" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o15264" name="ridge" name_original="ridges" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o15265" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia innocua is restricted to south coastal Texas in Aransas, Calhoun, Kenedy, Kleberg, Nueces, Refugio, San Patricio, and Willacy counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting early winter–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="early winter" />
        <character name="fruiting time" char_type="range_value" to="late spring" from="early winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils or dunes, grasslands, pastures.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>