<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">purpurea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 433. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species purpurea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101503</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxalis</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">variabilis</taxon_name>
    <taxon_hierarchy>genus oxalis;species variabilis</taxon_hierarchy>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Purple wood-sorrel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, acaulous, rhizomes present, slender, sparsely scaly, stolons absent, bulb solitary, 1–2.5 cm, or with clustered bulblets;</text>
      <biological_entity id="o30238" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulous" value_original="acaulous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o30239" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o30240" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o30241" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30242" name="bulblet" name_original="bulblets" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
      </biological_entity>
      <relation from="o30241" id="r2474" name="with" negation="false" src="d0_s0" to="o30242" />
    </statement>
    <statement id="d0_s1">
      <text>bulb scales black, thickened, not prominently nerved.</text>
      <biological_entity constraint="bulb" id="o30243" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="not prominently" name="architecture" src="d0_s1" value="nerved" value_original="nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, rarely absent at flowering;</text>
      <biological_entity id="o30244" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character constraint="at flowering" is_modifier="false" modifier="rarely" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole (1.5–) 3–5 cm;</text>
      <biological_entity id="o30245" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3, green to deep purple abaxially, green adaxially, broadly obovate to obtriangular or broadly rounded-rhombic, 10–20 mm, not lobed, apex truncate to rounded or obtuse, rarely slightly emarginate, margins and abaxial surface hairy, adaxial surface glabrous, oxalate deposits absent.</text>
      <biological_entity id="o30246" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="deep purple abaxially" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s4" to="obtriangular or broadly rounded-rhombic" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o30247" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="rounded or obtuse" />
        <character is_modifier="false" modifier="rarely slightly" name="architecture_or_shape" src="d0_s4" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o30248" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30249" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30250" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o30251" name="deposit" name_original="deposits" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o30252" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scapes 1.5–6 (–8) cm, sparsely to moderately villous, hairs eglandular.</text>
      <biological_entity id="o30253" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o30254" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers tristylous;</text>
      <biological_entity id="o30255" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>sepal apices without tubercles;</text>
      <biological_entity constraint="sepal" id="o30256" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o30257" name="tubercle" name_original="tubercles" src="d0_s8" type="structure" />
      <relation from="o30256" id="r2475" name="without" negation="false" src="d0_s8" to="o30257" />
    </statement>
    <statement id="d0_s9">
      <text>petals yellow basally, usually purple to red, pink, salmon, or white, rarely yellow, distally, 25–35 mm.</text>
      <biological_entity id="o30258" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="usually purple" name="coloration" src="d0_s9" to="red pink salmon or white" />
        <character char_type="range_value" from="usually purple" name="coloration" src="d0_s9" to="red pink salmon or white" />
        <character char_type="range_value" from="usually purple" name="coloration" src="d0_s9" to="red pink salmon or white" />
        <character char_type="range_value" from="usually purple" name="coloration" src="d0_s9" to="red pink salmon or white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="25" from_unit="mm" modifier="distally" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules not seen.</text>
      <biological_entity id="o30259" name="capsule" name_original="capsules" src="d0_s10" type="structure" />
    </statement>
  </description>
  <discussion>Oxalis purpurea is widely cultivated as an ornamental because of its large, solitary flowers in many color forms, borne on short scapes barely higher than the level of the leaves. Plants of O. purpurea apparently do not produce fertile fruit in California, where it is naturalized in scattered central and southern coastal counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, especially near gardens.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste" />
        <character name="habitat" value="gardens" modifier="places especially near" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; s Africa; introduced also in Europe, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>