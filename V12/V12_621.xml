<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">30</other_info_on_meta>
    <other_info_on_meta type="illustration_page">31</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">ZYGOPHYLLACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">FAGONIA</taxon_name>
    <taxon_name authority="Standley" date="1911" rank="species">laevis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>24: 249. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zygophyllaceae;genus fagonia;species laevis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101324</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fagonia</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Standley) Wiggins" date="unknown" rank="subspecies">laevis</taxon_name>
    <taxon_hierarchy>genus fagonia;species californica;subspecies laevis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">F.</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">chilensis</taxon_name>
    <taxon_name authority="(Standley) I. M. Johnston" date="unknown" rank="variety">laevis</taxon_name>
    <taxon_hierarchy>genus f.;species chilensis;variety laevis</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Smooth-stemmed fagonia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, less than 1 m, to 1 m diam.</text>
      <biological_entity id="o6316" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, intricately branched, dark green, not noticeably slender, mostly glabrous, but ultimate branches rarely sparsely stipitate-glandular, glands clear or yellow, to 0.1 mm diam.;</text>
      <biological_entity id="o6318" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="not noticeably" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6319" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6320" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="clear" value_original="clear" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s1" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>older branches and sometimes younger parts scabrous, older branches becoming stolonlike, bearing many erect smaller branches.</text>
      <biological_entity id="o6321" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6322" name="part" name_original="parts" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="true" modifier="sometimes" name="life_cycle" src="d0_s2" value="younger" value_original="younger" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6323" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" modifier="becoming" name="architecture_or_shape" src="d0_s2" value="stolonlike" value_original="stolonlike" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o6324" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="many" value_original="many" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o6323" id="r561" name="bearing" negation="false" src="d0_s2" to="o6324" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves (1–) 3-foliolate;</text>
      <biological_entity id="o6325" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="(1-)3-foliolate" value_original="(1-)3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules curved, reflexed to spreading, subulate, 1–6 mm, shorter than petioles, glabrous or sometimes sparsely stipitate-glandular;</text>
      <biological_entity id="o6326" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character char_type="range_value" from="reflexed" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character constraint="than petioles" constraintid="o6327" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6327" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 2–15 mm, glabrous or sometimes sparsely stipitate-glandular;</text>
      <biological_entity id="o6328" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets linear-elliptic, glabrous, generally longer than petiole, apex spinose, terminal 3–18 × 1–5 mm, laterals to 15 × 3 mm, shorter and narrower than terminal, one or both commonly caducous.</text>
      <biological_entity id="o6329" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="than petiole" constraintid="o6330" is_modifier="false" name="length_or_size" src="d0_s6" value="generally longer" value_original="generally longer" />
      </biological_entity>
      <biological_entity id="o6330" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o6331" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="18" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6332" name="lateral" name_original="laterals" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character constraint="than terminal" constraintid="o6333" is_modifier="false" name="width" src="d0_s6" value="shorter and narrower" value_original="shorter and narrower" />
        <character is_modifier="false" modifier="commonly" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity id="o6333" name="terminal" name_original="terminal" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1.5–7 mm, glabrous or often sparsely stipitate-glandular.</text>
      <biological_entity id="o6334" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 1 cm diam.;</text>
      <biological_entity id="o6335" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character name="diameter" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals green to purple, elliptic to lanceolate, 2–3 × 1 mm, glabrous or often sparsely stipitate-glandular;</text>
      <biological_entity id="o6336" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s9" to="purple" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often sparsely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pink to dark red-purple, 4–7 × 1.5–3 mm;</text>
      <biological_entity id="o6337" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="dark red-purple" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamen filaments 3–4.5 mm;</text>
      <biological_entity constraint="stamen" id="o6338" name="all" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 2–3 mm, glabrous or puberulent, not glandular;</text>
      <biological_entity id="o6339" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1–2 mm.</text>
      <biological_entity id="o6340" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 4–5 × 3–6 mm, usually minutely strigose or puberulent, rarely glabrous, not glandular;</text>
      <biological_entity id="o6341" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s14" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="usually minutely" name="pubescence" src="d0_s14" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1–2 mm, wider at base.</text>
      <biological_entity id="o6342" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character constraint="at base" constraintid="o6343" is_modifier="false" name="width" src="d0_s15" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o6343" name="base" name_original="base" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Fagonia laevis is restricted to the Mojave and Sonoran deserts, where it appears to have a more southern distribution than F. longipes. There has been some controversy about whether F. laevis and F. longipes are separate species. They are superficially similar to one another; however, F. longipes can be distinguished by its slender branches and longer pedicels and its minute stipitate-glandular hairs on stems, stipules, petioles, leaflets, pedicels, sepals, ovaries, and capsules (D. M. Porter 1963). Although F. laevis is usually glabrous, often pedicels and sepals, sometimes stipules and petioles, and rarely ultimate branches have a few small stipitate glands, but these never occur on ovaries or capsules. In addition, although the more southerly F. laevis and more northerly F. longipes overlap in their distributions in Arizona and California, and both species are found in dry, rocky or sandy, usually hillside habitats, they apparently do not grow together (R. S. Felger, pers. comm.). Also according to Felger, the herbage of the two differs, with F. laevis being dark green and F. longipes light to grayish green.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Nov–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky desert hillsides to sandy washes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky desert" />
        <character name="habitat" value="hillsides to sandy washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Baja California Sur, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>