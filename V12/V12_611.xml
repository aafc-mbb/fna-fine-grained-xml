<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="illustration_page">212</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Engelmann ex Torrey in W. H. Emory" date="1859" rank="species">fruticulosus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 194. 1859</place_in_publication>
      <other_info_on_pub>(as fruticulosum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species fruticulosus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101912</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Bush croton</other_name>
  <other_name type="common_name">encinilla</other_name>
  <other_name type="common_name">hierba loca</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2–10 dm, monoecious.</text>
      <biological_entity id="o602" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems much branched distally, stellate-hairy.</text>
      <biological_entity id="o603" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="much; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not clustered;</text>
      <biological_entity id="o604" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules rudimentary or absent;</text>
      <biological_entity id="o605" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.4–1 (–1.5) cm, 1/8–1/5 blade length, glands absent at apex;</text>
      <biological_entity id="o606" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
        <character char_type="range_value" from="1/8" name="quantity" src="d0_s4" to="1/5" />
      </biological_entity>
      <biological_entity id="o607" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o608" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o609" is_modifier="false" name="length" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o609" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to ovatelanceolate, 2–8 × 2–4 cm, base truncate to cordate, margins serrulate, often slightly undulate, apex attenuate, acute, abaxial surface pale green, stellate-hairy, adaxial surface darker green, puberulent.</text>
      <biological_entity id="o610" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovatelanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o611" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="cordate" />
      </biological_entity>
      <biological_entity id="o612" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="often slightly" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o613" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o614" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o615" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker green" value_original="darker green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences bisexual, racemes, 3–12 cm, staminate flowers 10–20, pistillate flowers 2–5.</text>
      <biological_entity id="o616" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o617" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o618" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o619" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: staminate 2.5–4 mm, pistillate 0–0.5 mm.</text>
      <biological_entity id="o620" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 5, 0.8–1.2 mm, abaxial surface stellate-tomentose;</text>
      <biological_entity id="o621" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o622" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o623" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-tomentose" value_original="stellate-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, oblanceolate to spatulate, 2 mm, abaxial surface glabrous except margins densely fimbrillate-villous;</text>
      <biological_entity id="o624" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o625" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="spatulate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o626" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character constraint="except margins" constraintid="o627" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o627" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="fimbrillate-villous" value_original="fimbrillate-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 9–16.</text>
      <biological_entity id="o628" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o629" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s10" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 5, equal, 2.2 mm, margins entire, apex incurved, abaxial surface stellate-hairy;</text>
      <biological_entity id="o630" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o631" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2.2" value_original="2.2" />
      </biological_entity>
      <biological_entity id="o632" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o633" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o634" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o635" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o636" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular;</text>
      <biological_entity id="o637" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o638" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, 3–4.5 mm, 2-fid to base, terminal segments 6.</text>
      <biological_entity id="o639" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o640" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character constraint="to base" constraintid="o641" is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o641" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="terminal" id="o642" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 5–6 mm diam., smooth;</text>
      <biological_entity id="o643" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella apex with 3 rounded, inflated lobes.</text>
      <biological_entity constraint="columella" id="o644" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o645" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s16" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o644" id="r64" name="with" negation="false" src="d0_s16" to="o645" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 4–5 × 3.2–3.8 mm, shiny.</text>
      <biological_entity id="o646" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s17" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Croton fruticulosus is known in the flora area from southeastern Arizona through southern New Mexico and trans-Pecos Texas to the Edwards Plateau.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone or basalt hills.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hills" modifier="limestone or basalt" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Nuevo León, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>