<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">hypericifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 454. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species hypericifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242321438</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="Millspaugh" date="unknown" rank="species">glomerifera</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species glomerifera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="(Linnaeus) Millspaugh" date="unknown" rank="species">hypericifolia</taxon_name>
    <taxon_hierarchy>genus c.;species hypericifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="(Millspaugh) L. C. Wheeler" date="unknown" rank="species">glomerifera</taxon_name>
    <taxon_hierarchy>genus euphorbia;species glomerifera</taxon_hierarchy>
  </taxon_identification>
  <number>48.</number>
  <other_name type="common_name">Graceful sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o27211" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o27212" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o27211" id="r2229" name="with" negation="false" src="d0_s0" to="o27212" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, 15–50 cm, glabrous.</text>
      <biological_entity id="o27213" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o27214" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules connate, deltate, usually entire, sometimes laciniate-fringed at tip, 1.5–2.2 mm, glabrous;</text>
      <biological_entity id="o27215" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="at tip" constraintid="o27216" is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="laciniate-fringed" value_original="laciniate-fringed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27216" name="tip" name_original="tip" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–3 mm, glabrous;</text>
      <biological_entity id="o27217" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obliquely oblong-oblanceolate, 10–35 × 7–15 mm, base asymmetric, oblique, margins serrate or serrulate, especially toward apex, apex broadly acute, surfaces glabrous;</text>
      <biological_entity id="o27218" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s5" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27219" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o27220" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o27221" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o27222" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o27220" id="r2230" modifier="especially" name="toward" negation="false" src="d0_s5" to="o27221" />
    </statement>
    <statement id="d0_s6">
      <text>palmately veined at base, pinnate distally.</text>
      <biological_entity id="o27223" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o27224" is_modifier="false" modifier="palmately" name="architecture" src="d0_s6" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o27224" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in dense, axillary and terminal, capitate glomerules with reduced, bractlike leaves subtending cyathia;</text>
      <biological_entity id="o27225" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity id="o27226" name="glomerule" name_original="glomerules" src="d0_s7" type="structure" constraint="terminal">
        <character is_modifier="true" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="true" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o27227" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="shape" src="d0_s7" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity id="o27228" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <relation from="o27225" id="r2231" name="in" negation="false" src="d0_s7" to="o27226" />
      <relation from="o27226" id="r2232" name="with" negation="false" src="d0_s7" to="o27227" />
      <relation from="o27227" id="r2233" name="subtending" negation="false" src="d0_s7" to="o27228" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.5–1.8 mm.</text>
      <biological_entity id="o27229" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic, 0.9–1.1 × 0.4–0.9 mm, glabrous;</text>
      <biological_entity id="o27230" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s9" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellow-green to brown, stipitate, subcircular, 0.2 × 0.2 mm, occasionally nearly rudimentary;</text>
      <biological_entity id="o27231" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s10" to="brown" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subcircular" value_original="subcircular" />
        <character name="length" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
        <character name="width" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" modifier="occasionally nearly" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages absent on smaller glands or white to pink, shape highly variable, usually round to ± elliptic, 0.3–0.4 × 0.5–0.7 mm, distal margin entire.</text>
      <biological_entity id="o27232" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character constraint="on " constraintid="o27234" is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o27233" name="gland" name_original="glands" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o27234" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="character" src="d0_s11" value="shape" value_original="shape" />
        <character is_modifier="true" modifier="highly" name="variability" src="d0_s11" value="variable" value_original="variable" />
        <character char_type="range_value" constraint="on " constraintid="o27236" from="white" name="coloration" src="d0_s11" to="pink" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o27235" name="gland" name_original="glands" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o27236" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="character" src="d0_s11" value="shape" value_original="shape" />
        <character is_modifier="true" modifier="highly" name="variability" src="d0_s11" value="variable" value_original="variable" />
        <character char_type="range_value" constraint="on " constraintid="o27238" from="usually round" name="shape" src="d0_s11" to="more or less elliptic" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o27237" name="gland" name_original="glands" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o27238" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="character" src="d0_s11" value="shape" value_original="shape" />
        <character is_modifier="true" modifier="highly" name="variability" src="d0_s11" value="variable" value_original="variable" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s11" to="0.4" to_unit="mm" />
        <character char_type="range_value" constraint="on " constraintid="o27240" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o27239" name="gland" name_original="glands" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o27240" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="character" src="d0_s11" value="shape" value_original="shape" />
        <character is_modifier="true" modifier="highly" name="variability" src="d0_s11" value="variable" value_original="variable" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers (0–) 2–20.</text>
      <biological_entity id="o27241" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s12" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o27242" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27243" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.4 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o27244" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27245" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.4" value_original="0.4" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules depressed-globoid, 1.3–1.4 × 1.1–1.5 mm, glabrous;</text>
      <biological_entity id="o27246" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s15" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1–1.1 mm.</text>
      <biological_entity id="o27247" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds with very thin whitish mucilaginous coat over light-brown testa below, ovoid-triangular, bluntly 4-angled in cross-section, 0.9–1.1 × 0.5 mm, with shallow irregular depressions alternating with low, smooth ridges.</text>
      <biological_entity id="o27248" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="ovoid-triangular" value_original="ovoid-triangular" />
        <character constraint="in cross-section" constraintid="o27251" is_modifier="false" modifier="bluntly" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" notes="" src="d0_s17" to="1.1" to_unit="mm" />
        <character name="width" notes="" src="d0_s17" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o27249" name="coat" name_original="coat" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="very" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="coating" src="d0_s17" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o27250" name="testa" name_original="testa" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity id="o27251" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
      <biological_entity id="o27252" name="depression" name_original="depressions" src="d0_s17" type="structure">
        <character is_modifier="true" name="depth" src="d0_s17" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
        <character constraint="with low" constraintid="o27253" is_modifier="false" name="arrangement" src="d0_s17" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o27253" name="low" name_original="low" src="d0_s17" type="structure" />
      <biological_entity id="o27254" name="ridge" name_original="ridges" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o27248" id="r2234" name="with" negation="false" src="d0_s17" to="o27249" />
      <relation from="o27249" id="r2235" name="over" negation="false" src="d0_s17" to="o27250" />
      <relation from="o27248" id="r2236" name="with" negation="false" src="d0_s17" to="o27252" />
    </statement>
  </description>
  <discussion>Euphorbia hypericifolia is native to the New World tropics, and it is most likely adventive in the flora area (where it is most widely distributed in Florida and Texas). Reports from Arizona, California, and Maryland likely represent waifs or misidentifications.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting early spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="early spring" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, disturbed areas, nurseries.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="nurseries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Okla., S.C., Tex.; West Indies; Central America; South America; introduced in Asia, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Asia" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>