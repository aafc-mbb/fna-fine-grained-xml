<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Hooker" date="1838" rank="species">setigerus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 141. 1838</place_in_publication>
      <other_info_on_pub>(as setigerum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species setigerus</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250101947</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eremocarpus</taxon_name>
    <taxon_name authority="(Hooker) Bentham" date="unknown" rank="species">setigerus</taxon_name>
    <taxon_hierarchy>genus eremocarpus;species setigerus</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Dove weed</other_name>
  <other_name type="common_name">turkey mullein</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 0.5–5 dm, monoecious.</text>
      <biological_entity id="o2516" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems densely and dichotomously shortly branched, forming loose, prostrate circular mats 5–80 cm across, proximally bristly stellate-hairy, central radii spreading, 2–3 mm.</text>
      <biological_entity id="o2517" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="dichotomously shortly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="position" src="d0_s1" value="central" value_original="central" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2518" name="mat" name_original="mats" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s1" value="loose" value_original="loose" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="circular" value_original="circular" />
      </biological_entity>
      <relation from="o2517" id="r230" name="forming" negation="false" src="d0_s1" to="o2518" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly clustered near inflorescences;</text>
      <biological_entity id="o2519" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near inflorescences" constraintid="o2520" is_modifier="false" modifier="mostly" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o2520" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipules rudimentary;</text>
      <biological_entity id="o2521" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–5 cm, glands absent at apex;</text>
      <biological_entity id="o2522" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2523" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o2524" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2524" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to rhombic, 0.8–6.5 × 0.8–4 cm, base cuneate, margins entire, apex obtuse, abaxial surface pale grayish green, adaxial surface grayish green, both densely stellate-hairy.</text>
      <biological_entity id="o2525" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="rhombic" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s5" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2526" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o2527" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2528" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2529" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale grayish green" value_original="pale grayish green" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2530" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences unisexual;</text>
    </statement>
    <statement id="d0_s7">
      <text>staminate dense capitate clusters, 1–2 cm, flowers 2–10;</text>
      <biological_entity id="o2531" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2532" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate clusters, 1–2 cm, flowers 1–3.</text>
      <biological_entity id="o2533" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2534" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2535" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels: staminate 0–1 mm, pistillate absent.</text>
      <biological_entity id="o2536" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: sepals 5–6, 2–2.5 mm, abaxial surface densely stellate-hairy;</text>
      <biological_entity id="o2537" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2538" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="6" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2539" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0;</text>
      <biological_entity id="o2540" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2541" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 5–9.</text>
      <biological_entity id="o2542" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2543" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: sepals 0;</text>
      <biological_entity id="o2544" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2545" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 0;</text>
      <biological_entity id="o2546" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2547" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1-locular;</text>
      <biological_entity id="o2548" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2549" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style 1, 2–3 mm, unbranched.</text>
      <biological_entity id="o2550" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2551" name="style" name_original="style" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules follicular (1-seeded), 3–6 × 2–3 mm, smooth;</text>
      <biological_entity id="o2552" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="follicular" value_original="follicular" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella absent.</text>
      <biological_entity id="o2553" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 3–5 × 2–3 mm, shiny.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 20.</text>
      <biological_entity id="o2554" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s19" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s19" to="3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2555" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Croton setigerus is nearly unique in the genus with its one-locular fruit, single unbranched style, and pistillate flowers devoid of any perianth. The foliage is toxic to animals, and the crushed plants were used by Native Americans to stupefy fish. The seeds are palatable to birds, giving rise to the common names cited above. Individual plants produce either mottled, striped, or solid gray or black seeds. Gray seeds are produced by desiccating plants and appear to be much less palatable to doves than the other color morphs (A. D. Cook et al. 1971).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal sage scrub, foothill woodlands, valley grasslands, oak woodlands, edges of fields, dry stream beds, disturbed areas, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="foothill woodlands" />
        <character name="habitat" value="valley grasslands" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="edges" constraint="of fields , dry stream beds" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="dry stream beds" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Idaho, Nev., Oreg., Utah, Wash.; Mexico (Baja California); introduced s South America (Chile), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="s South America (Chile)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>