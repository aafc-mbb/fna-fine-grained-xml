<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">VITACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1830" rank="genus">CAUSONIS</taxon_name>
    <place_of_publication>
      <publication_title>Med. Fl.</publication_title>
      <place_in_publication>2: 122. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family vitaceae;genus causonis</taxon_hierarchy>
    <other_info_on_name type="etymology">Derivation unknown; perhaps Latin causa, reason, and onus, necessity, alluding to segregation from Cissus</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">105947</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines [lianas], sprawling to moderately high climbing, synoecious or polygamomonoecious.</text>
      <biological_entity id="o10184" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="sprawling" name="growth_form" src="d0_s0" to="moderately high climbing" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="synoecious" value_original="synoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamomonoecious" value_original="polygamomonoecious" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branches: bark adherent;</text>
      <biological_entity id="o10185" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o10186" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="adherent" value_original="adherent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pith white, continuous through nodes;</text>
      <biological_entity id="o10187" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o10188" name="pith" name_original="pith" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character constraint="through nodes" constraintid="o10189" is_modifier="false" name="architecture" src="d0_s2" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o10189" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>tendrils usually 2–3 [–5] -branched, without [with] adhesive discs.</text>
      <biological_entity id="o10190" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o10191" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="2-3[-5]-branched" value_original="2-3[-5]-branched" />
      </biological_entity>
      <biological_entity constraint="adhesive" id="o10192" name="disc" name_original="discs" src="d0_s3" type="structure" />
      <relation from="o10191" id="r849" name="without" negation="false" src="d0_s3" to="o10192" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves palmately (pedately) compound.</text>
      <biological_entity id="o10193" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences usually bisexual, axillary, corymblike cymes, compound.</text>
      <biological_entity id="o10194" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o10195" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="corymblike" value_original="corymblike" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual or unisexual;</text>
      <biological_entity id="o10196" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx cupshaped, indistinctly 4-lobed;</text>
      <biological_entity id="o10197" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s7" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 4, distinct;</text>
      <biological_entity id="o10198" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary adnate to base of ovary, cupshaped, 4-lobed;</text>
      <biological_entity id="o10199" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character constraint="to base" constraintid="o10200" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" name="shape" src="d0_s9" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
      <biological_entity id="o10200" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o10201" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <relation from="o10200" id="r850" name="part_of" negation="false" src="d0_s9" to="o10201" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 4;</text>
      <biological_entity id="o10202" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style conic, short.</text>
      <biological_entity id="o10203" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Berries dark blue to black.</text>
      <biological_entity id="o10204" name="berry" name_original="berries" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s12" to="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–4 per fruit.</text>
      <biological_entity id="o10206" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>x = 10.</text>
      <biological_entity id="o10205" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per fruit" constraintid="o10206" from="2" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
      <biological_entity constraint="x" id="o10207" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 25 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; e, se Asia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e" establishment_means="introduced" />
        <character name="distribution" value="se Asia" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Causonis is separated from Cayratia (Wen J. et al. 2013) based on phylogenetic evidence (Wen et al. 2007; Lu L. M. et al. 2013). It corresponds to the Asian and Australian Cayratia sect. Discypharia Suessenguth (K. Suessenguth 1953; A. Latiff 1981). Causonis trifolia (Linnaeus) Rafinesque, native to southeast Asia and Australia, is an aggressive weed at Fairchild Tropical Garden in Miami, Florida. No other locations are known for this species in the flora area, but it may become naturalized in southern Florida. It can be distinguished from C. japonica by having 3-foliolate leaves and tendrils that are 3–5-branched and usually have adhesive discs at their tips.</discussion>
  
</bio:treatment>