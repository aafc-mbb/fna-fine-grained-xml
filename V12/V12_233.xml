<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">544</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="A. Gray" date="1854" rank="genus">PETALONYX</taxon_name>
    <taxon_name authority="A. Gray" date="1874" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 72. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus petalonyx;species parryi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101893</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Parry’s sandpaper plant</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, bushy to moundlike, to 15 dm;</text>
      <biological_entity id="o25259" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushy" value_original="bushy" />
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches of current season to 13 cm.</text>
      <biological_entity id="o25260" name="branch" name_original="branches" src="d0_s1" type="structure" constraint="season; season">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="current" id="o25261" name="season" name_original="season" src="d0_s1" type="structure" />
      <relation from="o25260" id="r2057" name="part_of" negation="false" src="d0_s1" to="o25261" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0.5–3.5 mm;</text>
      <biological_entity id="o25262" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25263" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to elliptic, without marked size dimorphism, to 40 × 30 mm, base acute to rounded, margins usually crenate to serrate, sometimes small leaves entire, apex acute.</text>
      <biological_entity id="o25264" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25265" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25266" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" modifier="without marked size dimorphism" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o25267" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually crenate" name="shape" src="d0_s3" to="serrate" />
      </biological_entity>
      <biological_entity id="o25268" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sometimes" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25269" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 35–65-flowered.</text>
      <biological_entity id="o25270" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="35-65-flowered" value_original="35-65-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers strongly bilaterally symmetric;</text>
      <biological_entity id="o25271" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly bilaterally" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals spatulate, 10–15 mm, claws postgenitally distally coherent, forming slitted corolla-tube;</text>
      <biological_entity id="o25272" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25273" name="claw" name_original="claws" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="postgenitally distally" name="fusion" src="d0_s6" value="coherent" value_original="coherent" />
      </biological_entity>
      <biological_entity id="o25274" name="corolla-tube" name_original="corolla-tube" src="d0_s6" type="structure" />
      <relation from="o25273" id="r2058" name="forming slitted" negation="false" src="d0_s6" to="o25274" />
    </statement>
    <statement id="d0_s7">
      <text>stamens exserted laterally through slits between petal claws.</text>
      <biological_entity id="o25275" name="stamen" name_original="stamens" src="d0_s7" type="structure" />
      <biological_entity constraint="petal" id="o25277" name="claw" name_original="claws" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 46.</text>
      <biological_entity constraint="between claws" constraintid="o25277" id="o25276" name="slit" name_original="slits" src="d0_s7" type="structure" constraint_original="between  claws, ">
        <character is_modifier="true" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25278" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wash bottoms, desert plains, usually white to gray, clayey soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottoms" modifier="wash" />
        <character name="habitat" value="desert plains" />
        <character name="habitat" value="usually white to gray" />
        <character name="habitat" value="usually white to clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>