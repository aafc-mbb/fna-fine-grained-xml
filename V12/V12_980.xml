<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
    <other_info_on_meta type="illustration_page">416</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Nickrent &amp; Der" date="unknown" rank="family">CERVANTESIACEAE</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">PYRULARIA</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">pubera</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 233. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cervantesiaceae;genus pyrularia;species pubera</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220011316</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, rhizomatous, much branched, to 4 m;</text>
      <biological_entity id="o63" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>young growth minutely pilosulous.</text>
      <biological_entity id="o64" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s1" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petioles (5–) 10 (–19) mm.</text>
      <biological_entity id="o65" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="19" to_unit="mm" />
        <character name="some_measurement" src="d0_s2" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades ovate-oblong, obovate, or elliptic, (4.2–) 10 (–21) × (2–) 4 (–8) cm, base acute to rounded, apex acute to acuminate, surfaces puberulent when young.</text>
      <biological_entity id="o66" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4.2" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="21" to_unit="cm" />
        <character name="length" src="d0_s3" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_width" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="8" to_unit="cm" />
        <character name="width" src="d0_s3" unit="cm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o67" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o68" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o69" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: staminate terminal on axillary branches, erect, 3–8 cm, 15+-flowered;</text>
      <biological_entity id="o70" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character constraint="on axillary branches" constraintid="o71" is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="15+-flowered" value_original="15+-flowered" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o71" name="branch" name_original="branches" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pistillate terminal or axillary, to 9-flowered;</text>
      <biological_entity id="o72" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="9-flowered" value_original="9-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts caducous, pilose.</text>
      <biological_entity id="o73" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o74" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers green, turbinate, 4 mm diam.;</text>
      <biological_entity id="o75" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character name="diameter" src="d0_s7" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillode stigma above anthers.</text>
      <biological_entity constraint="pistillode" id="o76" name="stigma" name_original="stigma" src="d0_s8" type="structure" />
      <biological_entity id="o77" name="anther" name_original="anthers" src="d0_s8" type="structure" />
      <relation from="o76" id="r6" name="above" negation="false" src="d0_s8" to="o77" />
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers green, turbinate, 5–6 mm diam.;</text>
      <biological_entity id="o78" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma at same height as staminode anthers.</text>
      <biological_entity id="o79" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity constraint="staminode" id="o80" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o79" id="r7" name="as" negation="false" src="d0_s10" to="o80" />
    </statement>
    <statement id="d0_s11">
      <text>Pseudodrupes yellowish, pyriform or subglobose, 2–3 × 1–2 cm;</text>
      <biological_entity id="o81" name="pseudodrupe" name_original="pseudodrupes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s11" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exocarp splitting irregularly when mature, releasing mesocarp/seed.</text>
      <biological_entity id="o83" name="seed" name_original="mesocarp/seed" src="d0_s12" type="structure" />
      <relation from="o82" id="r8" name="releasing" negation="false" src="d0_s12" to="o83" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 38.</text>
      <biological_entity id="o82" name="exocarp" name_original="exocarp" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="when mature" name="architecture_or_dehiscence" src="d0_s12" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity constraint="2n" id="o84" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pyrularia pubera can be locally abundant in the Blue Ridge Mountains and Appalachian Plateau, often forming dense stands in second-growth forests. The species is apparently a host generalist (D. J. Leopold and R. N. Muller 1983) and has been reported to parasitize planted fir trees (Abies fraseri) in Virginia (L. J. Musselman and S. C. Haynes 1996). The seeds are very high in oil. Cytotoxic and antimicrobial peptides called thionins are present in P. pubera (L. P. Vernon et al. 1985).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., N.Y., N.C., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>