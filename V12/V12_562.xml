<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHILADELPHUS</taxon_name>
    <taxon_name authority="S. Y. Hu" date="1956" rank="species">texensis</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>37: 54. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus philadelphus;species texensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101962</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 6–15 dm.</text>
      <biological_entity id="o11074" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, green to yellowish, weathering grayish and striate, divaricately branched, moderately to sparsely strigose-sericeous;</text>
      <biological_entity id="o11075" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="yellowish" />
        <character is_modifier="false" name="condition" src="d0_s1" value="weathering" value_original="weathering" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish and striate" value_original="grayish and striate" />
        <character is_modifier="false" modifier="divaricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="moderately to sparsely" name="pubescence" src="d0_s1" value="strigose-sericeous" value_original="strigose-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 1.5–5.5 cm (long-shoots), 0.2–2.2 cm (lateral branches);</text>
      <biological_entity id="o11076" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s2" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>axillary buds exposed.</text>
      <biological_entity constraint="axillary" id="o11077" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 2–4.5 (–8) mm;</text>
      <biological_entity id="o11078" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11079" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade grayish or whitish abaxially, green adaxially, usually narrowly lanceolate to lanceovate, sometimes ovate, (1–) 1.6–3.3 (–4.7) × (0.4–) 0.5–1.1 (–2.3) cm, ± herbaceous, margins usually entire, rarely sparsely serrulate, plane, sometimes drying revolute, abaxial surface densely and loosely strigose, hairs arched-twisted, 0.3–1.2 mm, with or without understory of coiled-crisped, slender hairs between raised veins, adaxial surface sparsely to moderately strigose, hairs antrorsely appressed, 0.4–0.9 mm, 1–3 hairs per mm of width.</text>
      <biological_entity id="o11080" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o11081" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character char_type="range_value" from="usually narrowly lanceolate" name="shape" src="d0_s5" to="lanceovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s5" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s5" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_width" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.1" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o11082" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes" name="condition" src="d0_s5" value="drying" value_original="drying" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11083" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="between vein" constraintid="o11086" id="o11084" name="hair" name_original="hairs" src="d0_s5" type="structure" constraint_original="between  vein, ">
        <character is_modifier="false" name="architecture" src="d0_s5" value="arched-twisted" value_original="arched-twisted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o11085" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="coiled-crisped" value_original="coiled-crisped" />
      </biological_entity>
      <biological_entity id="o11086" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11087" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o11088" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="antrorsely" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o11089" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o11090" name="mm" name_original="mm" src="d0_s5" type="structure" />
      <relation from="o11084" id="r919" modifier="with or without understory" name="part_of" negation="false" src="d0_s5" to="o11085" />
      <relation from="o11089" id="r920" name="per" negation="false" src="d0_s5" to="o11090" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: flowers solitary.</text>
      <biological_entity id="o11091" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o11092" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–2 (–4) mm, loosely strigose or glabrous.</text>
      <biological_entity id="o11093" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: hypanthium loosely strigose or glabrous;</text>
      <biological_entity id="o11094" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11095" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to ovatelanceolate, 2.5–5.5 × 1–2.5 mm, apex obtuse to long acuminate-caudate, abaxial surface loosely strigose or glabrous, adaxial surface glabrous except ciliate and villous along distal margins;</text>
      <biological_entity id="o11096" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11097" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11098" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="long acuminate-caudate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11099" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11100" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character constraint="except ciliate" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="along distal margins" constraintid="o11101" is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11101" name="margin" name_original="margins" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals white, oblong-ovate to ovate, 8–10 (–14) × 3.5–5 (–10.5) mm, margins entire to undulate, apex obtuse to notched;</text>
      <biological_entity id="o11102" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11103" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11104" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s10" to="undulate" />
      </biological_entity>
      <biological_entity id="o11105" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens (11–) 14–16 (–24);</text>
      <biological_entity id="o11106" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11107" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s11" to="14" to_inclusive="false" />
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="24" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s11" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct, 1–4.7 mm;</text>
      <biological_entity id="o11108" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11109" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1, clavate, 1.9–3 (–3.5) mm, slender base 0.5–2 mm;</text>
      <biological_entity id="o11110" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o11111" name="style" name_original="style" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o11112" name="base" name_original="base" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmatic portion obovoid, 1.2–1.7 × 1.1 mm.</text>
      <biological_entity id="o11113" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="stigmatic" id="o11114" name="portion" name_original="portion" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s14" to="1.7" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="1.1" value_original="1.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules turbinate-spheroid, 3.5–5.5 × 3.5–5 mm, sepals at distal 1/3, tardily falling, capsule distal surface smooth or impressed in 4 vertical lines.</text>
      <biological_entity id="o11115" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="turbinate-spheroid" value_original="turbinate-spheroid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s15" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11116" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="tardily" name="life_cycle" notes="" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11117" name="1/3" name_original="1/3" src="d0_s15" type="structure" />
      <biological_entity id="o11118" name="capsule" name_original="capsule" src="d0_s15" type="structure" />
      <biological_entity constraint="distal" id="o11119" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character constraint="in lines" constraintid="o11120" is_modifier="false" name="prominence" src="d0_s15" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o11120" name="line" name_original="lines" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="true" name="orientation" src="d0_s15" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o11116" id="r921" name="at" negation="false" src="d0_s15" to="o11117" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds short-caudate distally, 1–1.2 mm.</text>
      <biological_entity id="o11121" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s16" value="short-caudate" value_original="short-caudate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>A. E. Weakley (2002) found that the ITS sequences of Philadelphus texensis and P. mearnsii do not differ, suggesting that they are very closely related.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces strigose and with understory of slender, white, tightly coiled-crisped hairs.</description>
      <determination>2a. Philadelphus texensis var. texensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces strigose, without understory of white, tightly coiled-crisped hairs.</description>
      <determination>2b. Philadelphus texensis var. ernestii</determination>
    </key_statement>
  </key>
</bio:treatment>