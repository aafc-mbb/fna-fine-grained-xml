<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Parry" date="1889" rank="species">foliosus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">foliosus</taxon_name>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species foliosus;variety foliosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101421</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">dentatus</taxon_name>
    <taxon_name authority="Fosberg" date="unknown" rank="variety">dickeyi</taxon_name>
    <taxon_hierarchy>genus ceanothus;species dentatus;variety dickeyi</taxon_hierarchy>
  </taxon_identification>
  <number>26a.</number>
  <other_name type="common_name">Wavyleaf ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1.5–3.5 m.</text>
      <biological_entity id="o26266" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="3.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect.</text>
      <biological_entity id="o26267" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ± folded lengthwise, margins glandular-denticulate, wavy, abaxial surface pale green to grayish green, glabrous, sometimes sparsely puberulent on veins.</text>
      <biological_entity id="o26268" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less; lengthwise" name="architecture_or_shape" src="d0_s2" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o26269" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="glandular-denticulate" value_original="glandular-denticulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o26271" name="vein" name_original="veins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 24.</text>
      <biological_entity constraint="abaxial" id="o26270" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s2" to="grayish green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o26271" is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26272" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety foliosus occurs in the outer North Coast Ranges and Santa Cruz Mountains, from Humboldt to Santa Clara counties. H. McMinn (1944) reported putative hybrids with Ceanothus parryi and C. thyrsiflorus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils, chaparral, mixed evergreen and conifer forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="mixed evergreen" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>