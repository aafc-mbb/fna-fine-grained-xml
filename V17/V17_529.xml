<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="A. Nelson" date="1898" rank="species">rydbergii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 281. 1898</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species rydbergii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>185.</number>
  <other_name type="common_name">Rydberg’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o31514" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, ascending, or erect, 20–70 (–120) cm, glabrous or retrorsely hairy, sometimes only in lines, not glaucous.</text>
      <biological_entity id="o31515" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o31516" name="line" name_original="lines" src="d0_s1" type="structure" />
      <relation from="o31515" id="r2387" modifier="sometimes only; only" name="in" negation="false" src="d0_s1" to="o31516" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline (25–) 35–70 (–150) × (5–) 10–15 (–22) mm, blade oblanceolate to elliptic, base tapered, margins entire, apex obtuse to acute, rarely mucronate;</text>
      <biological_entity id="o31517" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o31518" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o31519" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic" />
      </biological_entity>
      <biological_entity id="o31520" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o31521" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31522" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–6 pairs, sessile or proximals short-petiolate, 25–70 (–110) × 3–24 mm, blade lanceolate to elliptic or oblong, base tapered to clasping, margins entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o31523" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o31524" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="110" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31525" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="elliptic or oblong" />
      </biological_entity>
      <biological_entity id="o31526" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o31527" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31528" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, cylindric, 3–10 (–18) cm, axis retrorsely hairy or puberulent, sometimes glabrous, verticillasters (1 or) 2–7, cymes (3–) 5–11-flowered, 2 per node;</text>
      <biological_entity id="o31529" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31530" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31531" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o31532" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(3-)5-11-flowered" value_original="(3-)5-11-flowered" />
        <character constraint="per node" constraintid="o31533" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o31533" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, (4–) 11–52 (–70) × (1–) 2–18 mm, margins entire or erose to lacerate;</text>
      <biological_entity constraint="proximal" id="o31534" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s6" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="52" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="70" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s6" to="52" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s6" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31535" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels erect, glabrous, retrorsely hairy, or puberulent.</text>
      <biological_entity id="o31536" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o31537" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes oblong to ovate or obovate, 3–8 × 1.2–3.5 mm, apex acute to acuminate or short to long-caudate, glabrous or puberulent;</text>
      <biological_entity id="o31538" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o31539" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="ovate or obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31540" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s8" value="long-caudate" value_original="long-caudate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla violet to blue or purple, without nectar guides, funnelform, 10–20 mm, glabrous externally, sparsely white or yellowish villous internally abaxially, tube 3–4 mm, throat gradually inflated, 3–4.5 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o31541" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o31542" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s9" to="blue or purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o31543" name="guide" name_original="guides" src="d0_s9" type="structure" />
      <biological_entity id="o31544" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31545" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o31542" id="r2388" name="without" negation="false" src="d0_s9" to="o31543" />
    </statement>
    <statement id="d0_s10">
      <text>stamens: longer pair reaching orifice or exserted, pollen-sacs opposite, navicular, 0.5–1.1 mm, dehiscing completely, connective splitting, sides glabrous, sutures smooth or ± papillate;</text>
      <biological_entity id="o31546" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o31547" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o31548" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s10" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o31549" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o31550" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31551" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o31546" id="r2389" name="reaching" negation="false" src="d0_s10" to="o31547" />
    </statement>
    <statement id="d0_s11">
      <text>staminode 6–7 mm, reaching orifice, 0.2–0.4 mm diam., tip straight, distal 1.5–2 mm moderately to densely pilose, hairs golden yellow, to 1 mm;</text>
      <biological_entity id="o31552" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o31553" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31554" name="orifice" name_original="orifice" src="d0_s11" type="structure" />
      <biological_entity id="o31555" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="position_or_shape" src="d0_s11" value="distal" value_original="distal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o31556" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o31553" id="r2390" name="reaching" negation="false" src="d0_s11" to="o31554" />
    </statement>
    <statement id="d0_s12">
      <text>style 8–10 mm.</text>
      <biological_entity id="o31557" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o31558" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–5 × 3–4 mm, glabrous.</text>
      <biological_entity id="o31559" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As treated here, Penstemon rydbergii includes three partially sympatric varieties. D. D. Keck (1945) treated the same taxa as four species and two subspecies (P. hesperius, P. oreocharis, P. rydbergii subsp. aggregatus, P. rydbergii subsp. rydbergii, and P. vaseyanus).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx lobe margins lacerate, rarely erose, broadly scarious, apices acuminate or short- to long-caudate; proximal bract margins erose to lacerate proximally, sometimes entire; thyrse axes retrorsely hairy or puberulent.</description>
      <determination>185a. Penstemon rydbergii var. rydbergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx lobe margins erose, sometimes lacerate, narrowly to broadly scarious, apices acute, acuminate, or short-caudate; proximal bract margins entire; thyrse axes glabrous or retrorsely hairy.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems ± retrorsely hairy; thyrse axes glabrous or retrorsely hairy; corollas (10–)13–20 mm; pollen sacs (0.6–)0.8–1.1 mm.</description>
      <determination>185b. Penstemon rydbergii var. aggregatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrous, rarely ± retrorsely hairy; thyrse axes glabrous, rarely ± retrorsely hairy; corollas 10–14 mm; pollen sacs 0.5–0.7(–0.8) mm.</description>
      <determination>185c. Penstemon rydbergii var. oreocharis</determination>
    </key_statement>
  </key>
</bio:treatment>