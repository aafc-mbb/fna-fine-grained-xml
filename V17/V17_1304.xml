<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Douglas ex Bentham) G. L. Nesom &amp; N. S. Fraga" date="2012" rank="species">alsinoides</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 37. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species alsinoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1835" rank="species">alsinoides</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>29. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species alsinoides</taxon_hierarchy>
  </taxon_identification>
  <number>32.</number>
  <other_name type="common_name">Wing-stem or chickweed monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, fibrous-rooted.</text>
      <biological_entity id="o1467" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually simple, (0.5–) 2–6 (–15) cm, glandular-puberulent, hairs 0.1–0.2 mm, gland-tipped, nodes 2 (or 3), usually red-tinged.</text>
      <biological_entity id="o1468" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o1469" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s1" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o1470" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="red-tinged" value_original="red-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o1471" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–20 (–30) mm, distinctly 3-veined (winged);</text>
      <biological_entity id="o1472" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s3" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 3-veined, lanceolate-ovate to ovate, elliptic, or suborbicular, 3–18 (–32) × 3–12 (–25) mm, base cuneate to truncate, margins dentate to denticulate or subentire, apex acute to obtuse or rounded, surfaces glandular-puberulent, hairs 0.1–0.2 mm, gland-tipped.</text>
      <biological_entity id="o1473" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="lanceolate-ovate" name="shape" src="d0_s4" to="ovate elliptic or suborbicular" />
        <character char_type="range_value" from="lanceolate-ovate" name="shape" src="d0_s4" to="ovate elliptic or suborbicular" />
        <character char_type="range_value" from="lanceolate-ovate" name="shape" src="d0_s4" to="ovate elliptic or suborbicular" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="32" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1474" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o1475" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s4" to="denticulate or subentire" />
      </biological_entity>
      <biological_entity id="o1476" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse or rounded" />
      </biological_entity>
      <biological_entity id="o1477" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o1478" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–4 (–8), from distal or medial to distal nodes.</text>
      <biological_entity id="o1480" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o1479" id="r123" name="from" negation="false" src="d0_s5" to="o1480" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 15–32 mm, glandular-puberulent, hairs 0.1–0.2 mm, gland-tipped.</text>
      <biological_entity id="o1479" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o1481" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o1482" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="32" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <relation from="o1479" id="r124" name="fruiting" negation="false" src="d0_s6" to="o1481" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces purplish, slightly ridge-angled, campanulate-cylindric, weakly or not inflated, 5–8 mm, margins subtruncate, sparsely minutely stipitate-glandular, lobes 4, (0–) 0.5–1 mm, sometimes barely evident, 1 lobe usually slightly longer, margins appearing subtruncate, shallowly convex to rounded-mucronulate.</text>
      <biological_entity id="o1483" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o1484" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o1485" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="ridge-angled" value_original="ridge-angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate-cylindric" value_original="campanulate-cylindric" />
        <character is_modifier="false" modifier="not; not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1486" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="subtruncate" value_original="subtruncate" />
        <character is_modifier="false" modifier="sparsely minutely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1487" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes barely" name="prominence" src="d0_s7" value="evident" value_original="evident" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1488" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually slightly" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1489" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subtruncate" value_original="subtruncate" />
        <character char_type="range_value" from="shallowly convex" name="shape" src="d0_s7" to="rounded-mucronulate" />
      </biological_entity>
      <relation from="o1483" id="r125" name="fruiting" negation="false" src="d0_s7" to="o1484" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, abaxial limb with a large maroon splotch, also red-spotted, bilaterally symmetric, ± bilabiate;</text>
      <biological_entity id="o1490" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1491" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="with a large maroon splotch" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat funnelform-cylindric, 6–9 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o1492" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="funnelform-cylindric" value_original="funnelform-cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o1493" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o1492" id="r126" name="exserted beyond" negation="false" src="d0_s9" to="o1493" />
    </statement>
    <statement id="d0_s10">
      <text>throat open, palate villous, abaxial ridges low.</text>
      <biological_entity id="o1494" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o1495" name="palate" name_original="palate" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1496" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="low" value_original="low" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o1497" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o1498" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, (3–) 5–7 mm.</text>
      <biological_entity id="o1499" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe alsinoides is distinct in its short, erect stems with few nodes, small, mostly ovate to elliptic-ovate, petiolate leaves, minutely stipitate-glandular vestiture, small corollas with a prominent maroon splotch on the abaxial limb, small, non-inflated mature calyces and, most especially, by its nearly truncate calyx margin. Erythranthe pulsiferae is superficially similar to E. alsinoides but has larger calyces borne on divergent-arcuate pedicels, smaller leaf blades with attenuate to cuneate bases, and the corolla limbs are smaller and lack a prominent maroon splotch.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky slopes, cliff faces, bluffs, mossy rock crevices, ledges, moist rocks, roadsides, along wet paths and trails.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliff faces" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="mossy rock crevices" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="moist rocks" />
        <character name="habitat" value="roadsides" constraint="along wet paths and trails" />
        <character name="habitat" value="wet paths" />
        <character name="habitat" value="trails" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>