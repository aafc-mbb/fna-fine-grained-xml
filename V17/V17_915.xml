<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">524</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">518</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Wirsing" date="1778" rank="species">labradorica</taxon_name>
    <place_of_publication>
      <publication_title>Eclog. Bot.</publication_title>
      <place_in_publication>[2], plate 10. 1778</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species labradorica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pedicularis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">labradorica</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">sulphurea</taxon_name>
    <taxon_hierarchy>genus pedicularis;species labradorica;variety sulphurea</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Labrador lousewort</other_name>
  <other_name type="common_name">pédiculaire du Labrador</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–25 cm.</text>
      <biological_entity id="o26668" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 2 or 3, blade lanceolate, 10–20 x 2–3 mm, 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping, serrate, surfaces glabrous;</text>
      <biological_entity id="o26669" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o26670" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s1" unit="or" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o26671" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o26672" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="nonoverlapping" value_original="nonoverlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o26673" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o26674" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o26672" id="r2046" name="part_of" negation="false" src="d0_s1" to="o26673" />
    </statement>
    <statement id="d0_s2">
      <text>cauline 1–4, blade linear to lanceolate, 10–50 x 2–10 mm, undivided or 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping, serrate to 2-serrate, surfaces glabrous or sparsely downy to hispid.</text>
      <biological_entity id="o26675" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o26676" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o26677" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o26678" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="nonoverlapping" value_original="nonoverlapping" />
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s2" to="2-serrate" />
      </biological_entity>
      <biological_entity id="o26679" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o26680" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparsely downy" name="pubescence" src="d0_s2" to="hispid" />
      </biological_entity>
      <relation from="o26678" id="r2047" name="part_of" negation="false" src="d0_s2" to="o26679" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes paniculate or buds present in cauline leaf-axils, 1–8, exceeding basal leaves, each 5–20-flowered;</text>
      <biological_entity id="o26681" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o26682" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character constraint="in cauline leaf-axils" constraintid="o26683" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="5-20-flowered" value_original="5-20-flowered" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o26683" name="leaf-axil" name_original="leaf-axils" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity constraint="basal" id="o26684" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o26682" id="r2048" name="exceeding" negation="false" src="d0_s3" to="o26684" />
    </statement>
    <statement id="d0_s4">
      <text>bracts linear to narrowly lanceolate, 7–15 x 1–2 mm, undivided or 1-pinnatifid, proximal margins entire, distal 1-serrate or 2-serrate, sometimes crenulate, surfaces glabrous or hispid.</text>
      <biological_entity id="o26685" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s4" value="1-pinnatifid" value_original="1-pinnatifid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o26686" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26687" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-serrate" value_original="2-serrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity id="o26688" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0.5–2 mm.</text>
      <biological_entity id="o26689" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 5–8 mm, glabrous, lobes 2, triangular, 0.5–1.5 mm, apex entire, sometimes slightly bifurcate, glabrous;</text>
      <biological_entity id="o26690" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26691" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26692" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26693" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s6" value="bifurcate" value_original="bifurcate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 12–18 mm, tube deep yellow, 7–10 mm;</text>
      <biological_entity id="o26694" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26695" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26696" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>galea dark yellow or yellow tinged with purple or spotted, 5–9 mm, beakless, margins entire medially, 1-toothed distally, apex arching over abaxial lip;</text>
      <biological_entity id="o26697" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26698" name="galea" name_original="galea" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark yellow" value_original="dark yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow tinged with purple or yellow tinged with spotted" />
        <character name="coloration" src="d0_s8" value="," value_original="," />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity id="o26699" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="1-toothed" value_original="1-toothed" />
      </biological_entity>
      <biological_entity id="o26700" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="over abaxial lip" constraintid="o26701" is_modifier="false" name="orientation" src="d0_s8" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26701" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip dark yellow, 5–7 mm. 2n = 16.</text>
      <biological_entity id="o26702" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o26703" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark yellow" value_original="dark yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26704" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The flowers of Pedicularis labradorica are usually yellow or dark yellow, and the galea is tinged distally with red or purple; sometimes, the yellow color of the tube abruptly transitions into red or purple. Hultén based var. sulphurea on the solid yellow color variant from the Yukon.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open forests, tundras, heathlands, rocky slopes, muskegs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open forests" />
        <character name="habitat" value="tundras" />
        <character name="habitat" value="heathlands" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="muskegs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska; Asia (China, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>