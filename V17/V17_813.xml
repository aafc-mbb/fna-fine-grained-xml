<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">478</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="1828" rank="species">californica</taxon_name>
    <taxon_name authority="(Beck) Heckard" date="1973" rank="subspecies">grayana</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>22: 54. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species californica;subspecies grayana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orobanche</taxon_name>
    <taxon_name authority="Beck" date="1890" rank="species">grayana</taxon_name>
    <place_of_publication>
      <publication_title>Biblioth. Bot.</publication_title>
      <place_in_publication>19: 79. 1890</place_in_publication>
      <other_info_on_pub>based on O. comosa Hooker, Fl. Bor.-Amer. 2: 92. 1837, not Wallroth 1822</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus orobanche;species grayana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphyllon</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) A. Gray" date="unknown" rank="species">californicum</taxon_name>
    <taxon_name authority="(Beck) A. C. Schneider" date="unknown" rank="subspecies">grayanum</taxon_name>
    <taxon_hierarchy>genus aphyllon;species californicum;subspecies grayanum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myzorrhiza</taxon_name>
    <taxon_name authority="(Beck) Rydberg" date="unknown" rank="species">grayana</taxon_name>
    <taxon_hierarchy>genus myzorrhiza;species grayana</taxon_hierarchy>
  </taxon_identification>
  <number>9b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually branched proximally, 4–10 cm, portion proximal to inflorescence 1–3 (–4) cm, slender.</text>
      <biological_entity id="o39435" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o39436" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character constraint="to inflorescence" constraintid="o39437" is_modifier="false" name="position" src="d0_s0" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="size" notes="" src="d0_s0" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o39437" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences corymbs, sometimes subcorymbose racemes, 3–6 cm;</text>
      <biological_entity constraint="inflorescences" id="o39438" name="corymb" name_original="corymbs" src="d0_s1" type="structure" />
      <biological_entity id="o39439" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="sometimes" name="arrangement" src="d0_s1" value="subcorymbose" value_original="subcorymbose" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracts pallid to pinkish tinged, drying brown.</text>
      <biological_entity id="o39440" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="pallid" name="coloration" src="d0_s2" to="pinkish tinged" />
        <character is_modifier="false" name="condition" src="d0_s2" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 5–20 mm.</text>
      <biological_entity id="o39441" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyces 11–15 mm, lobes pallid, sometimes purplish, linear-subulate, (7–) 9–13 (–16) mm.</text>
      <biological_entity id="o39442" name="calyx" name_original="calyces" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39443" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pallid" value_original="pallid" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="16" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas pallid to pinkish or pale lavender, often with lavender veins, (25–) 28–33 mm;</text>
      <biological_entity id="o39444" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="pallid" name="coloration" src="d0_s5" to="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale lavender" value_original="pale lavender" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s5" to="28" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="33" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39445" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration_or_odor" src="d0_s5" value="lavender" value_original="lavender" />
      </biological_entity>
      <relation from="o39444" id="r3046" modifier="often" name="with" negation="false" src="d0_s5" to="o39445" />
    </statement>
    <statement id="d0_s6">
      <text>tube slender, abruptly widening toward throat;</text>
      <biological_entity id="o39446" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character constraint="toward throat" constraintid="o39447" is_modifier="false" modifier="abruptly" name="width" src="d0_s6" value="widening" value_original="widening" />
      </biological_entity>
      <biological_entity id="o39447" name="throat" name_original="throat" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>throat 8–10 mm wide at base of lobes;</text>
      <biological_entity id="o39448" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o39449" from="8" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39449" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o39450" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <relation from="o39449" id="r3047" name="part_of" negation="false" src="d0_s7" to="o39450" />
    </statement>
    <statement id="d0_s8">
      <text>lips 10–12 mm, abaxial lobes lanceolate to lanceolate-subulate, 2–3 mm wide, apex acute, adaxial lobes oblong-ovate, apex narrowly rounded, shallowly retuse, or erosulate.</text>
      <biological_entity id="o39451" name="lip" name_original="lips" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39452" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="lanceolate-subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39453" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39454" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 48.</text>
      <biological_entity id="o39455" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="erosulate" value_original="erosulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39456" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies grayana is distributed in the Cascade-Sierra Nevada ranges, Coast Ranges (central California and southern Oregon), and mountains of the Columbia Plateau in Oregon, from Klickitat County, Washington, south to Tuolumne County, California. It is rare throughout the range, possibly locally extirpated in portions of the range in Oregon.</discussion>
  <discussion>The hosts are primarily species of Aster and Erigeron and occasionally Grindelia. Other reported non-Asteraceae hosts are unlikely.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows and stream margins.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="stream margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(50–)300–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>