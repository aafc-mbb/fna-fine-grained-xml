<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="species">sparsiflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">sparsiflora</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species sparsiflora;variety sparsiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Collinsia</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">bruceae</taxon_name>
    <taxon_hierarchy>genus collinsia;species bruceae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sparsiflora</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">arvensis</taxon_name>
    <taxon_hierarchy>genus c.;species sparsiflora;variety arvensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sparsiflora</taxon_name>
    <taxon_name authority="(M. E. Jones) Newsom" date="unknown" rank="variety">bruceae</taxon_name>
    <taxon_hierarchy>genus c.;species sparsiflora;variety bruceae</taxon_hierarchy>
  </taxon_identification>
  <number>12a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: corolla 9–20 mm, angle between corolla tube-throat and caly× 45–70°.</text>
      <biological_entity id="o40863" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o40864" name="corolla" name_original="corolla" src="d0_s0" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s0" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="between corolla" constraintid="o40866" id="o40865" name="angle" name_original="angle" src="d0_s0" type="structure" constraint_original="between  corolla, ">
        <character name="degree" src="d0_s0" value="caly×45-70°" value_original="caly×45-70°" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o40866" name="tube-throat" name_original="tube-throat" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Seeds 2.5–3 mm. 2n = 14.</text>
      <biological_entity id="o40867" name="seed" name_original="seeds" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o40868" name="chromosome" name_original="" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the southern portion of the interior North Coast Ranges of California, plants of var. sparsiflora with corollas 14–20 mm and strongly declined occur on mafic substrates of volcanic origin. V. M. Newsom (1929) treated these plants as var. arvensis.</discussion>
  <discussion>V. M. Newsom (1929) treated plants of var. sparsiflora with corollas 9–13 mm in the Cascade Ranges of southern Oregon and northern California as var. bruceae. The shape of the corolla of var. bruceae is intermediate to that of the smaller-flowered var. collina with which it is sympatric. These plants are not as tall as those of the Coastal Ranges of California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy, sometimes disturbed, places, drying meadows, chaparral, oak woodlands, dry mixed woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" modifier="grassy sometimes disturbed places drying" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="dry mixed woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>