<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">630</other_info_on_meta>
    <other_info_on_meta type="mention_page">569</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">631</other_info_on_meta>
    <other_info_on_meta type="mention_page">636</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in W. H. Brewer et al." date="1876" rank="species">minor</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>1: 573. 1876</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species minor</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">affinis</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="variety">minor</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 119. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus castilleja;species affinis;variety minor</taxon_hierarchy>
  </taxon_identification>
  <number>68.</number>
  <other_name type="common_name">Seep or thread-torch paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 2–10 (–15) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with a short taproot or small, fibrous-root system.</text>
      <biological_entity id="o6318" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o6319" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="fibrous-root" id="o6320" name="system" name_original="system" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o6318" id="r541" name="with" negation="false" src="d0_s1" to="o6319" />
      <relation from="o6318" id="r542" name="with" negation="false" src="d0_s1" to="o6320" />
    </statement>
    <statement id="d0_s2">
      <text>Stems solitary or few, erect, unbranched, rarely branched distally, hairs sparse to dense, spreading, sometimes shaggy (var. minor), short to long, soft to stiff, eglandular and/or sparsely to densely stipitate-glandular.</text>
      <biological_entity id="o6321" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o6322" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s2" to="dense" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="shaggy" value_original="shaggy" />
        <character is_modifier="false" name="size_or_length" src="d0_s2" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green or purple to ± gray, linear to lanceolate, 2–10 cm, not fleshy, margins plane, sometimes wavy, ± involute, 0-lobed, apex acuminate to acute, sometimes obtuse.</text>
      <biological_entity id="o6323" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s3" to="more or less gray" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o6324" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="wavy" value_original="wavy" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="0-lobed" value_original="0-lobed" />
      </biological_entity>
      <biological_entity id="o6325" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 5–40 × 1–4 cm;</text>
      <biological_entity id="o6326" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts proximally greenish, distally red, red-orange, or pale orange, rarely yellow, on apices, narrowly lanceolate, sometimes narrowly oblong to spatulate distally, 0-lobed, plane-margined, apex acuminate (oblong to narrowly spatulate in var. spiralis).</text>
    </statement>
    <statement id="d0_s6">
      <text>Calyces green or yellowish green, 13–27 (–28) mm;</text>
      <biological_entity id="o6329" name="calyx" name_original="calyces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="28" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial and adaxial clefts 6–15 mm, 33–75% of calyx length, deeper than laterals, lateral 0.5–4 mm, 5–20% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o6330" name="cleft" name_original="clefts" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o6331" is_modifier="false" name="coloration_or_size" src="d0_s7" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="33-75%" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6331" name="lateral" name_original="laterals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lobes linear to narrowly triangular or narrowly lanceolate, apex acute or acuminate.</text>
      <biological_entity id="o6332" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly triangular or narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o6333" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas straight, 13–39 mm;</text>
      <biological_entity id="o6334" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="39" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tube 11–16 (–20) mm;</text>
      <biological_entity id="o6335" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak partially to completely exserted, sometimes included, adaxially yellow, pale orange to red-orange, reddish-brown, green, or white, 5–15 (–20) mm;</text>
      <biological_entity id="o6336" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="partially to completely" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s11" value="included" value_original="included" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="pale orange" name="coloration" src="d0_s11" to="red-orange reddish-brown green or white" />
        <character char_type="range_value" from="pale orange" name="coloration" src="d0_s11" to="red-orange reddish-brown green or white" />
        <character char_type="range_value" from="pale orange" name="coloration" src="d0_s11" to="red-orange reddish-brown green or white" />
        <character char_type="range_value" from="pale orange" name="coloration" src="d0_s11" to="red-orange reddish-brown green or white" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>abaxial lip yellow, white, red, deep red, red-violet, or green, colored as or strongly contrasting with rest of corolla, small but jutting out at 90° from axis of corolla, often readily visible through abaxial cleft, 1–3 mm, 5–25% as long as beak;</text>
      <biological_entity constraint="corolla" id="o6337" name="lip" name_original="lip" src="d0_s12" type="structure" constraint_original="corolla abaxial; corolla">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red-violet" value_original="red-violet" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red-violet" value_original="red-violet" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s12" value="colored" value_original="colored" />
        <character constraint="from axis" constraintid="o6340" is_modifier="false" modifier="90°" name="size" src="d0_s12" value="small" value_original="small" />
        <character constraint="through abaxial lip" constraintid="o6342" is_modifier="false" modifier="often readily" name="prominence" notes="" src="d0_s12" value="visible" value_original="visible" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6338" name="rest" name_original="rest" src="d0_s12" type="structure" />
      <biological_entity id="o6339" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o6340" name="axis" name_original="axis" src="d0_s12" type="structure" />
      <biological_entity id="o6341" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o6342" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="through" name="position" src="d0_s12" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o6343" name="beak" name_original="beak" src="d0_s12" type="structure" />
      <relation from="o6337" id="r544" name="contrasting with" negation="false" src="d0_s12" to="o6338" />
      <relation from="o6337" id="r545" name="part_of" negation="false" src="d0_s12" to="o6339" />
      <relation from="o6340" id="r546" name="part_of" negation="false" src="d0_s12" to="o6341" />
    </statement>
    <statement id="d0_s13">
      <text>teeth spreading to strongly incurved, green, white, yellow, red, or red-purple, 0.2–1 mm. 2n = 24.</text>
      <biological_entity id="o6344" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="strongly incurved" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red-purple" value_original="red-purple" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6345" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja minor is a widespread specialist of seeps, saline shores, and wet ground at moderate elevations. California has many populations on serpentine substrates. Most of the varieties have distinct ranges with little overlap, with the exception of the edaphic obligate var. spiralis, which is restricted to serpentine substrates.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla beaks 5–8(–10) mm, usually 1/3 or less of length of corollas, included to more often partially exserted from calyces.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Abaxial lips of corollas red to reddish purple; leaves linear or linear-lanceolate, soft; mountains of c, e Arizona and adjacent New Mexico southward.</description>
      <determination>68a. Castilleja minor var. minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Abaxial lips of corollas whitish, pale green, or pale yellowish; leaves linear-lanceolate to lanceolate, coarse; widespread in Great Basin region.</description>
      <determination>68b. Castilleja minor var. exilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla beaks 8–15(–20) mm, usually 1/3+ of length of corollas, exserted from calyces.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Proximal bracts narrowly lanceolate or oblanceolate, distals oblong to narrowly spatulate, apices rounded or obtuse, rarely acute; abaxial lips of corollas red.</description>
      <determination>68c. Castilleja minor var. spiralis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Bracts linear to narrowly lanceolate, apices acuminate; abaxial lips of corollas whitish, rarely greenish or pale yellow (sometimes red in c Arizona).</description>
      <determination>68d. Castilleja minor var. stenantha</determination>
    </key_statement>
  </key>
</bio:treatment>