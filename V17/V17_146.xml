<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="D. A. Sutton" date="1988" rank="genus">SAIROCARPUS</taxon_name>
    <taxon_name authority="(Kellogg) D. A. Sutton" date="1988" rank="species">vexillocalyculatus</taxon_name>
    <taxon_name authority="(A. Gray) Barringer" date="2013" rank="subspecies">breweri</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2013-34: 2. 2013</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus sairocarpus;species vexillocalyculatus;subspecies breweri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antirrhinum</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">breweri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 374. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum;species breweri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">vagans</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="variety">breweri</taxon_name>
    <taxon_hierarchy>genus a.;species vagans;variety breweri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">vexillocalyculatum</taxon_name>
    <taxon_name authority="(A. Gray) D. M. Thompson" date="unknown" rank="subspecies">breweri</taxon_name>
    <taxon_hierarchy>genus a.;species vexillocalyculatum;subspecies breweri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sairocarpus</taxon_name>
    <taxon_name authority="(A. Gray) D. A. Sutton" date="unknown" rank="species">breweri</taxon_name>
    <taxon_hierarchy>genus sairocarpus;species breweri</taxon_hierarchy>
  </taxon_identification>
  <number>7b.</number>
  <other_name type="common_name">Brewer's snapdragon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–70 (–100) cm, proximally glandular-hairy and, sometimes, eglandular-hairy.</text>
      <biological_entity id="o8436" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: branches with 1 leaf at each proximal node.</text>
      <biological_entity id="o8437" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o8438" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o8439" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8440" name="node" name_original="node" src="d0_s1" type="structure" />
      <relation from="o8438" id="r693" name="with" negation="false" src="d0_s1" to="o8439" />
      <relation from="o8439" id="r694" name="at" negation="false" src="d0_s1" to="o8440" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers: calyx adaxial lobe 3.5–5.5 mm;</text>
      <biological_entity id="o8441" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o8442" name="calyx" name_original="calyx" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o8443" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla white to light purple, often with darker veins, 8–15 mm, adaxial lip 2–5.5 mm.</text>
      <biological_entity id="o8444" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o8445" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s3" to="light purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8446" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8447" name="lip" name_original="lip" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5.5" to_unit="mm" />
      </biological_entity>
      <relation from="o8445" id="r695" modifier="often" name="with" negation="false" src="d0_s3" to="o8446" />
    </statement>
  </description>
  <discussion>Subspecies breweri and Sairocarpus cornutus both have white to purple corollas and grow in the same general area; S. cornutus has a deeper spur at the base of the corolla and is self-supporting, without twining branches.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly lower slopes, disturbed areas, often on serpentine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lower slopes" modifier="gravelly" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>