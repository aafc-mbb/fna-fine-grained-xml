<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">644</other_info_on_meta>
    <other_info_on_meta type="mention_page">576</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="mention_page">643</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Eastwood" date="1935" rank="species">peirsonii</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>1: 175. 1935</place_in_publication>
      <other_info_on_pub>(as peirsoni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species peirsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Beane" date="unknown" rank="species">carterae</taxon_name>
    <taxon_hierarchy>genus castilleja;species carterae</taxon_hierarchy>
  </taxon_identification>
  <number>86.</number>
  <other_name type="common_name">Peirson’s paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.6–3 (–4) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o21878" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o21879" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o21878" id="r1663" name="with" negation="false" src="d0_s2" to="o21879" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few to several, erect or ascending, unbranched, sometimes branched, hairs dense distally, less so proximally, spreading, long, soft, eglandular, also mixed with short-stipitate-glandular ones distally.</text>
      <biological_entity id="o21880" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21881" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="less; proximally" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with ones" constraintid="o21882" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o21882" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green to purple, narrowly to broadly lanceolate to oblong, (0.7–) 1.5–4.2 (–5) cm, not fleshy, margins plane, flat or involute, (0–) 3 (–5) -lobed, apex acuminate;</text>
      <biological_entity id="o21883" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple" />
        <character char_type="range_value" from="lanceolate" modifier="narrowly to broadly; broadly" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4.2" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21884" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3(-5)-lobed" value_original="(0-)3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o21885" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending-spreading, narrowly lanceolate to linear, apex acute.</text>
      <biological_entity id="o21886" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o21887" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2.5–7.5 (–15) × 1.5–3 cm;</text>
      <biological_entity id="o21888" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish to dull reddish, distally bright red, orange, or pale orange, sometimes yellowish or dull red, broadly lanceolate to oblong, 3–5-lobed;</text>
      <biological_entity id="o21889" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="dull reddish distally bright red orange or pale orange" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="dull reddish distally bright red orange or pale orange" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="dull reddish distally bright red orange or pale orange" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="dull reddish distally bright red orange or pale orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, linear to lanceolate, ± long, arising above mid length, sometimes below mid length on proximal bracts, central lobe apex acute, rarely narrowly obtuse, lateral ones acute.</text>
      <biological_entity id="o21890" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o21891" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="sometimes below" name="position" src="d0_s8" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21892" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity constraint="lobe" id="o21893" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character is_modifier="false" name="length" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely narrowly" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o21890" id="r1664" name="sometimes below mid" negation="false" src="d0_s8" to="o21891" />
      <relation from="o21891" id="r1665" name="on" negation="false" src="d0_s8" to="o21892" />
    </statement>
    <statement id="d0_s9">
      <text>Calyces proximally light green or yellow, distally yellow or colored as bracts, 12–20 mm;</text>
      <biological_entity id="o21894" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character constraint="as bracts" constraintid="o21895" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21895" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 5–9 mm, 40–50% of calyx length, deeper than laterals, lateral (1.5–) 3–5.8 mm, 10–30% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o21896" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character constraint="than laterals" constraintid="o21897" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="40-50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21897" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes oblong, apex acute to obtuse.</text>
      <biological_entity id="o21898" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o21899" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas slightly curved, 15–28 mm;</text>
      <biological_entity id="o21900" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 13–15 mm;</text>
      <biological_entity id="o21901" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, adaxially yellow or yellow-green, 7–8 mm;</text>
      <biological_entity id="o21902" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip yellow or deep green, reduced, slightly inflated, pouched, protruding out abaxial cleft, 1–2.5 mm, 13–33% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o21903" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="pouched" value_original="pouched" />
        <character constraint="out abaxial lip" constraintid="o21904" is_modifier="false" name="prominence" src="d0_s15" value="protruding" value_original="protruding" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21904" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o21905" name="beak" name_original="beak" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect or curved, yellow or green, 0.7–1 mm. 2n = 24.</text>
      <biological_entity id="o21906" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s16" value="curved" value_original="curved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21907" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja peirsonii is endemic to the higher elevations in the Sierra Nevada of California and in Tahoe Meadows in adjacent Washoe County, Nevada. Plants with yellow bracts in the southern portion of the range were named C. carterae. Castilleja peirsonii has shorter, wider corolla beaks than the related C. parviflora. In addition to the corolla shape differences, C. peirsonii also has red to yellow bracts and fairly bright yellow corollas, especially on the beaks, while C. parviflora has purple, pink, or white bracts and greenish corolla tubes and dorsal beak surfaces, with the beak margins pink, purple, or white. Reports from outside the Sierra Nevada in California and immediately adjacent Nevada are misidentifications. Castilleja peirsonii sometimes hybridizes with C. lemmonii in meadows where both species often occur in large numbers.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet meadows, stream banks, lakeshores, montane to alpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="moist to wet meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–3400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>