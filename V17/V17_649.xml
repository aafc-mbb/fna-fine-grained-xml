<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">268</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GRATIOLA</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">ramosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>61. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus gratiola;species ramosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Branched hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o36974" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, simple or few-branched, 10–35 cm, glabrous or glabrate proximally, glandular-puberulent distally.</text>
      <biological_entity id="o36975" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear-lanceolate to lanceolate-ovate, 7–20 × 1–3 mm, margins entire or with 1 or 2 (or 3) pairs of blunt teeth distally, apex acute, surfaces glabrate or sparsely glandular-puberulent.</text>
      <biological_entity id="o36976" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o36977" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="lanceolate-ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36978" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="of teeth" constraintid="o36979" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with 1 or 2pairs" value_original="with 1 or 2pairs" />
      </biological_entity>
      <biological_entity id="o36979" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o36980" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o36981" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels slender to stout, 6–17 mm, length 0.8–2 times bract, sparsely to densely glandular-puberulent;</text>
      <biological_entity id="o36982" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s3" to="stout" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="17" to_unit="mm" />
        <character constraint="bract" constraintid="o36983" is_modifier="false" name="length" src="d0_s3" value="0.8-2 times bract" value_original="0.8-2 times bract" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o36983" name="bract" name_original="bract" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 0 or 1, 0.8–1 mm.</text>
      <biological_entity id="o36984" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s4" unit="or" value="1" value_original="1" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals distinct, linear to narrowly lanceolate or oblong, 3–6 mm;</text>
      <biological_entity id="o36985" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o36986" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate or oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla 10–14 mm, tube greenish yellow, veins yellow, brownish yellow, or purple, limb white;</text>
      <biological_entity id="o36987" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o36988" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36989" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o36990" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o36991" name="limb" name_original="limb" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 2–4 mm.</text>
      <biological_entity id="o36992" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o36993" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules subglobular to ovoid, 1.2–2 × 1.2–2 mm.</text>
      <biological_entity id="o36994" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="subglobular" name="shape" src="d0_s8" to="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 0.2–0.3 mm. 2n = 14.</text>
      <biological_entity id="o36995" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36996" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet pine savannas, Carolina bays, shorelines of ponds, marshes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet pine savannas" />
        <character name="habitat" value="carolina bays" />
        <character name="habitat" value="shorelines" constraint="of ponds , marshes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Md., Miss., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>