<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="species">clevelandii</taxon_name>
    <taxon_name authority="Munz &amp; I. M. Johnston" date="1923" rank="variety">connatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>49: 357. 1923</place_in_publication>
      <other_info_on_pub>(as clevelandi)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species clevelandii;variety connatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">clevelandii</taxon_name>
    <taxon_name authority="(Munz &amp; I. M. Johnston) D. D. Keck" date="unknown" rank="subspecies">connatus</taxon_name>
    <taxon_hierarchy>genus penstemon;species clevelandii;subspecies connatus</taxon_hierarchy>
  </taxon_identification>
  <number>229b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal cauline leaves sessile, base connate-perfoliate.</text>
      <biological_entity constraint="distal cauline" id="o34854" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o34855" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="connate-perfoliate" value_original="connate-perfoliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Thyrses: peduncles and pedicels glabrous.</text>
      <biological_entity id="o34856" name="thyrse" name_original="thyrses" src="d0_s1" type="structure" />
      <biological_entity id="o34857" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34858" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: calyx lobes glabrous;</text>
      <biological_entity id="o34859" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="calyx" id="o34860" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla sparsely white or yellowish pilose internally abaxially;</text>
      <biological_entity id="o34861" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o34862" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pollen-sacs navicular, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o34863" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o34864" name="pollen-sac" name_original="pollen-sacs" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="navicular" value_original="navicular" />
      </biological_entity>
      <biological_entity id="o34865" name="suture" name_original="sutures" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o34866" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminode 7–11 mm.</text>
      <biological_entity id="o34867" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o34868" name="staminode" name_original="staminode" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety connatus is known only from southern California, especially in the Cuyamaca-Lacuna, San Jacinto, and Santa Rosa mountains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky to sandy slopes, pinyon-juniper woodlands, scrub, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky to sandy slopes" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>