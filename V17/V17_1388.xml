<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">442</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">432</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; N. S. Fraga" date="2012" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 27. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species parryi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 97. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="A. L. Grant" date="unknown" rank="species">spissus</taxon_name>
    <taxon_name authority="Edwin" date="unknown" rank="variety">lincolnensis</taxon_name>
    <taxon_hierarchy>genus m.;species spissus;variety lincolnensis</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Parry’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o28337" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 10–120 (–170) mm, finely and minutely glandular-puberulent.</text>
      <biological_entity id="o28338" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="170" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="120" to_unit="mm" />
        <character is_modifier="false" modifier="finely; minutely" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually cauline, relatively even-sized;</text>
      <biological_entity id="o28339" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o28340" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent;</text>
      <biological_entity id="o28341" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic to sublinear or oblanceolate, sometimes obovate, (5–) 8–25 (–31) × (1–) 2–9 (–12) mm, margins entire, plane, not ciliate, apex: proximals usually rounded, distals usually acute, surfaces glandular-puberulent.</text>
      <biological_entity id="o28342" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="sublinear or oblanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="31" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28343" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o28344" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o28345" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28346" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28347" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels (1.5–) 2–4 (–9) mm in fruit.</text>
      <biological_entity id="o28348" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o28349" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28349" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o28350" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o28351" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28351" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28352" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o28353" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28352" id="r2167" name="on" negation="false" src="d0_s6" to="o28353" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, not inflated in fruit, (5–) 7–12 (–13) mm, glandular-puberulent, lobes unequal, adaxial longer, apex broadly rounded to acute, often apiculate, ribs often dark purple, intercostal areas purplish or white.</text>
      <biological_entity id="o28354" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o28355" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o28356" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o28355" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o28356" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o28357" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28358" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o28359" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s7" to="acute" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s7" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o28360" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o28361" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas of 2 color forms: (a) magenta, ± deepening at mouth, usually with 6–8 darker spots in arc on abaxial lip around mouth, throat floor yellow to whitish with reddish speckling and (b) yellow with 6–8 narrow reddish spots or lines in arc on abaxial lip around mouth and reddish speckling on throat floor, palate ridges yellow extending onto lip, tube-throat (10–) 12–18 (–20) mm, limb 11–17.5 (–20) mm diam., not bilabiate.</text>
      <biological_entity id="o28362" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="of 2 color forms" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character constraint="at mouth" constraintid="o28363" is_modifier="false" modifier="more or less" name="coloration" src="d0_s8" value="deepening" value_original="deepening" />
        <character constraint="in arc" constraintid="o28364" is_modifier="false" name="coloration" src="d0_s8" value="darker spots" value_original="darker spots" />
      </biological_entity>
      <biological_entity id="o28363" name="mouth" name_original="mouth" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" modifier="with" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o28364" name="arc" name_original="arc" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o28365" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o28366" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
      <biological_entity id="o28367" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with speckling" constraintid="o28368" from="yellow" name="coloration" src="d0_s8" to="whitish" />
        <character constraint="with lines" constraintid="o28369" is_modifier="false" name="coloration" notes="" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o28368" name="speckling" name_original="speckling" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o28369" name="line" name_original="lines" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish spots" value_original="reddish spots" />
      </biological_entity>
      <biological_entity id="o28370" name="arc" name_original="arc" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o28371" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o28372" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
      <biological_entity id="o28373" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o28374" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o28375" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o28376" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_distance" src="d0_s8" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="distance" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28377" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="17.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="diameter" src="d0_s8" to="17.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o28364" id="r2168" name="on" negation="false" src="d0_s8" to="o28365" />
      <relation from="o28365" id="r2169" name="around" negation="false" src="d0_s8" to="o28366" />
      <relation from="o28369" id="r2170" name="in" negation="false" src="d0_s8" to="o28370" />
      <relation from="o28370" id="r2171" name="on" negation="false" src="d0_s8" to="o28371" />
      <relation from="o28371" id="r2172" name="around" negation="false" src="d0_s8" to="o28372" />
      <relation from="o28367" id="r2173" name="speckling on" negation="false" src="d0_s8" to="o28373" />
      <relation from="o28374" id="r2174" name="extending onto" negation="false" src="d0_s8" to="o28375" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o28378" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent.</text>
      <biological_entity id="o28379" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas included, lobes equal.</text>
      <biological_entity id="o28380" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o28381" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules (5.5–) 6.5–10.5 mm. 2n = 16.</text>
      <biological_entity id="o28382" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28383" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus parryi has a limited range, primarily in the Mohave Desert in four states: Arizona (Mohave County), California (Inyo County, where apparently disjunct, in pinyon-juniper woodlands, and at higher than typical elevation), Nevada (Clark and Lincoln counties), and Utah (Washington County). The glandular-puberulent vestiture and unequal calyx lobes are diagnostic.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Banks, gravel bars, washes, sandy ravines, rocky hillsides, ledges and bases of limestone ledges and boulders, clay loam-basalt, bare areas, often with Coleogyne and Larrea, sagebrush and pinyon-juniper.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="banks" />
        <character name="habitat" value="gravel bars" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="sandy ravines" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="ledges" constraint="of limestone ledges and boulders" />
        <character name="habitat" value="bases" constraint="of limestone ledges and boulders" />
        <character name="habitat" value="limestone ledges" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="clay loam-basalt" />
        <character name="habitat" value="bare areas" />
        <character name="habitat" value="coleogyne" modifier="often with" />
        <character name="habitat" value="larrea" />
        <character name="habitat" value="sagebrush and pinyon-juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(600–)800–1700(–2200) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>