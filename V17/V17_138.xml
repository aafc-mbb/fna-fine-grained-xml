<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="D. A. Sutton" date="1988" rank="genus">SAIROCARPUS</taxon_name>
    <taxon_name authority="(Bentham) D. A. Sutton" date="1988" rank="species">cornutus</taxon_name>
    <taxon_name authority="(A. Gray) Barringer" date="2013" rank="subspecies">leptaleus</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2013-34: 1. 2013</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus sairocarpus;species cornutus;subspecies leptaleus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antirrhinum</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">leptaleum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 373. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum;species leptaleum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">cornutum</taxon_name>
    <taxon_name authority="(A. Gray) Munz" date="unknown" rank="variety">leptaleum</taxon_name>
    <taxon_hierarchy>genus a.;species cornutum;variety leptaleum</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blade oblanceolate.</text>
      <biological_entity id="o25843" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o25844" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pedicels 1–2 mm.</text>
      <biological_entity id="o25845" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: calyx lobes subequal, 3–7 mm;</text>
      <biological_entity id="o25846" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="calyx" id="o25847" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla white to pale lavender, veins not contrasting, 7–14 mm, palate angular, glabrous or hairy, often with 2 yellow patches.</text>
      <biological_entity id="o25848" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o25849" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s3" to="pale lavender" />
      </biological_entity>
      <biological_entity id="o25850" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25851" name="palate" name_original="palate" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="angular" value_original="angular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25852" name="patch" name_original="patches" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o25851" id="r1962" modifier="often" name="with" negation="false" src="d0_s3" to="o25852" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds: ridges reticulate.</text>
      <biological_entity id="o25853" name="seed" name_original="seeds" src="d0_s4" type="structure" />
      <biological_entity id="o25854" name="ridge" name_original="ridges" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s4" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies leptaleus is known from the Sierra Nevada, especially in the foothill region.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky washes, disturbed areas, foothills.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky washes" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="foothills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>