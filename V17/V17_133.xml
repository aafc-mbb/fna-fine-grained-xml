<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kerry A. Barringer, Neil A. Harriman†</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="mention_page">37</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Rothmaler" date="1943" rank="genus">PSEUDORONTIUM</taxon_name>
    <place_of_publication>
      <publication_title>Feddes Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>52: 33. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus pseudorontium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudo, false, and genus Orontium, alluding to resemblance of seeds</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Antirrhinum</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="section">Pseudorontium</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 81. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum;section pseudorontium</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o5818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branching, filiform, twining branches absent, glandular-hairy, viscid.</text>
      <biological_entity id="o5819" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o5820" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s1" value="twining" value_original="twining" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite proximally, alternate distally;</text>
      <biological_entity id="o5821" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o5822" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade not fleshy, not leathery, margins entire.</text>
      <biological_entity id="o5823" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o5824" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary;</text>
      <biological_entity id="o5825" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o5826" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts present, alternate.</text>
      <biological_entity id="o5827" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present, not twining, recurved in fruit;</text>
      <biological_entity id="o5828" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s7" value="twining" value_original="twining" />
        <character constraint="in fruit" constraintid="o5829" is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o5829" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracteoles absent.</text>
      <biological_entity id="o5830" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o5831" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, basally connate, calyx slightly bilaterally symmetric, cupulate, lobes linear to narrowly oblanceolate;</text>
      <biological_entity id="o5832" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5833" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity id="o5834" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="narrowly oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla purple with darker veins, bilaterally symmetric, bilabiate and personate, tubular, tube base gibbous abaxially, not spurred, lobes 5, abaxial 2, adaxial 3;</text>
      <biological_entity id="o5835" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character constraint="with veins" constraintid="o5836" is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="personate" value_original="personate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o5836" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="tube" id="o5837" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o5838" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5839" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5840" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, basally adnate to corolla, didynamous, filaments glabrous or sparsely hairy, pollen-sacs 1 per filament;</text>
      <biological_entity id="o5841" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o5842" is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o5842" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o5843" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5844" name="pollen-sac" name_original="pollen-sacs" src="d0_s12" type="structure">
        <character constraint="per filament" constraintid="o5845" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5845" name="filament" name_original="filament" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>staminode 0 or 1, capitate;</text>
      <biological_entity id="o5846" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s13" unit="or" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o5847" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma subcapitate.</text>
      <biological_entity id="o5848" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, locules equal, dehiscence poricidal.</text>
      <biological_entity constraint="fruits" id="o5849" name="capsule" name_original="capsules" src="d0_s16" type="structure" />
      <biological_entity id="o5850" name="locule" name_original="locules" src="d0_s16" type="structure">
        <character is_modifier="false" name="variability" src="d0_s16" value="equal" value_original="equal" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="poricidal" value_original="poricidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 30–70, black or dark-brown, ellipsoid, wings present, cupulate.</text>
      <biological_entity id="o5851" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s17" to="70" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 13.</text>
      <biological_entity id="o5852" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o5853" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pseudorontium is similar to Mohavea in having winged seeds, but R. K. Oyama and D. A. Baum (2004) and P. Vargas et al. (2004) found Pseudorontium positioned well outside the taxa usually included in Antirrhinum in both the narrow (D. A. Sutton 1988) and broad (D. M. Thompson 1988) senses. ITS results in M. Fernández-Mazuecos et al. (2013) placed Pseudorontium and Mohavea in different clades; Pseudorontium was sister to Galvezia. The pendent, globular capsules, eccentrically winged seeds, and lack of twining branches distinguish P. cyathiferum from species of Sairocarpus.</discussion>
  
</bio:treatment>