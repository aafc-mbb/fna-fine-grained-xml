<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">triphyllos</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 14. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species triphyllos</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Fingered speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, sometimes biennials.</text>
      <biological_entity id="o4002" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, often blue tinged, (3–) 5–15 (–20) cm, densely hairy.</text>
      <biological_entity id="o4004" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="blue tinged" value_original="blue tinged" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade: proximal ovate to triangular, 5–7 × 3–6 mm, distal 8–18 × 8–18 mm, base rounded, margins (proximal) coarsely crenate or (distal) 3–5 (–7) -palmatifid, apex obtuse, surfaces sparsely hairy, often glandular.</text>
      <biological_entity id="o4005" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4006" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o4007" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="triangular" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4008" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="18" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4009" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4010" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-5(-7)-palmatifid" value_original="3-5(-7)-palmatifid" />
      </biological_entity>
      <biological_entity id="o4011" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4012" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1, terminal, sometimes also 1 or 2 axillary, 30–120 (–150) mm, 6–15 (–25) -flowered, axis densely eglandular and glandular-hairy;</text>
      <biological_entity id="o4013" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character modifier="sometimes" name="quantity" src="d0_s3" unit="or axillary" value="1" value_original="1" />
        <character modifier="sometimes" name="quantity" src="d0_s3" unit="or axillary" value="2" value_original="2" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="150" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s3" to="120" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="6-15(-25)-flowered" value_original="6-15(-25)-flowered" />
      </biological_entity>
      <biological_entity id="o4014" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts 5-palmatifid, distal 3-fid, 4–18 mm.</text>
      <biological_entity constraint="proximal" id="o4015" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="5-palmatifid" value_original="5-palmatifid" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4016" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="3-fid" value_original="3-fid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels patent or arcuate-ascending, 4–11 (–20) mm, ± equal to subtending bract, length 1–2 times calyx, densely hairy.</text>
      <biological_entity id="o4017" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="arcuate-ascending" value_original="arcuate-ascending" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="calyx" constraintid="o4019" is_modifier="false" name="length" src="d0_s5" value="1-2 times calyx" value_original="1-2 times calyx" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4018" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o4019" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
      <relation from="o4017" id="r370" name="subtending" negation="false" src="d0_s5" to="o4018" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes 4–6 (–11) mm, apex obtuse, glandular-hairy;</text>
      <biological_entity id="o4020" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o4021" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4022" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla deep blue, 5–10 mm diam.;</text>
      <biological_entity id="o4023" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4024" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 0.9–1.7 mm;</text>
      <biological_entity id="o4025" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4026" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style (0.5–) 0.7–1.5 (–2) mm.</text>
      <biological_entity id="o4027" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4028" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ± inflated basally, compressed distally in cross-section, obcordiform, 4–5 (–10) × 4.5–6.5 (–8) mm, apex emarginate, glandular-hairy.</text>
      <biological_entity id="o4029" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less; basally" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character constraint="in cross-section" constraintid="o4030" is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="obcordiform" value_original="obcordiform" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4030" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o4031" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 14–22 (–35), dark-brown or blackish, subglobular, cymbiform, 1–2.2 × 0.7–2 mm, 0.4–0.9 mm thick, ± rugulose to cristate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 14 (Eurasia).</text>
      <biological_entity id="o4032" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="22" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="35" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s11" to="22" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobular" value_original="subglobular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cymbiform" value_original="cymbiform" />
        <character name="thickness" src="d0_s11" unit="mm" value="1-2.2×0.7-2" value_original="1-2.2×0.7-2" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s11" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cristate" value_original="cristate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4033" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–May(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine forests, stony pastures, rocky banks, sandy fields, gardens, roadsides, rarely calcareous soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="stony pastures" />
        <character name="habitat" value="rocky banks" />
        <character name="habitat" value="sandy fields" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="soils" modifier="rarely calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Idaho, Kans., Mo., Okla., Oreg., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>