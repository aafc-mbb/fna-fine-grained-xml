<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">449</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">450</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="Eastwood" date="1906" rank="species">calycinus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>41: 287. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species calycinus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diplacus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">longiflorus</taxon_name>
    <taxon_name authority="(Eastwood) Jepson" date="unknown" rank="variety">calycinus</taxon_name>
    <taxon_hierarchy>genus diplacus;species longiflorus;variety calycinus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="(Nuttall) A. L. Grant" date="unknown" rank="species">longiflorus</taxon_name>
    <taxon_name authority="(Eastwood) Munz" date="unknown" rank="subspecies">calycinus</taxon_name>
    <taxon_hierarchy>genus mimulus;species longiflorus;subspecies calycinus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longiflorus</taxon_name>
    <taxon_name authority="(Eastwood) A. L. Grant" date="unknown" rank="variety">calycinus</taxon_name>
    <taxon_hierarchy>genus m.;species longiflorus;variety calycinus</taxon_hierarchy>
  </taxon_identification>
  <number>43.</number>
  <other_name type="common_name">Rock bush monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs.</text>
      <biological_entity id="o13226" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 150–1500 mm, glandular-pubescent to viscid-villous.</text>
      <biological_entity id="o13227" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="150" from_unit="mm" name="some_measurement" src="d0_s1" to="1500" to_unit="mm" />
        <character char_type="range_value" from="glandular-pubescent" name="pubescence" src="d0_s1" to="viscid-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, relatively even-sized;</text>
      <biological_entity id="o13228" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent;</text>
      <biological_entity id="o13229" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic-lanceolate or lanceolate to narrowly lanceolate or linear-lanceolate, sometimes narrowly oblong, 20–75 (–100) × 4–20 (–28) mm, margins entire or shallowly crenate, plane or revolute, apex acute to obtuse, abaxial surfaces moderately villous, hairs unbranched, vitreous, adaxial glabrous.</text>
      <biological_entity id="o13230" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly lanceolate or linear-lanceolate" />
        <character is_modifier="false" modifier="sometimes narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="75" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="28" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13231" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o13232" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13233" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o13234" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_coloration" src="d0_s4" value="vitreous" value_original="vitreous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13235" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 3–5 mm in fruit.</text>
      <biological_entity id="o13236" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o13237" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13237" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, chasmogamous.</text>
      <biological_entity id="o13238" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o13239" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o13239" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces not inflated in fruit, 28–40 mm, densely glandular-pubescent to short glandular-villous, tube slightly dilated distally, lobes unequal, apex acute, ribs green, intercostal areas light green.</text>
      <biological_entity id="o13240" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o13241" is_modifier="false" modifier="not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="densely glandular-pubescent" name="pubescence" src="d0_s7" to="short glandular-villous" />
      </biological_entity>
      <biological_entity id="o13241" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o13242" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s7" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o13243" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o13244" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13245" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o13246" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light green" value_original="light green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas usually pale-yellow or cream to yellow, not spotted or striped, palate ridges yellow to golden yellow, tube-throat 35–42 mm, limb 20–30 mm diam., bilabiate, lobes oblong, apex of adaxial 2 each shallowly, asymmetrically incised.</text>
      <biological_entity id="o13247" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s8" to="yellow not spotted or striped" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s8" to="yellow not spotted or striped" />
      </biological_entity>
      <biological_entity constraint="palate" id="o13248" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="golden yellow" />
      </biological_entity>
      <biological_entity id="o13249" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="distance" src="d0_s8" to="42" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13250" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s8" to="30" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity id="o13251" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o13252" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shallowly; asymmetrically" name="shape" notes="" src="d0_s8" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13253" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o13252" id="r1034" name="consist_of" negation="false" src="d0_s8" to="o13253" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers exserted, glabrous.</text>
      <biological_entity id="o13254" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles minutely glandular.</text>
      <biological_entity id="o13255" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes equal.</text>
      <biological_entity id="o13256" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o13257" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 25–35 mm. 2n = 20.</text>
      <biological_entity id="o13258" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s12" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13259" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Although first described as a separate species, Diplacus calycinus has more recently been treated at subspecific or varietal rank (A. L. Grant 1924; F. W. Pennell 1951; P. A. Munz and D. D. Keck 1973). D. M. Thompson (2005) included both D. calycinus and D. longiflorus within his concept of Mimulus aurantiacus var. pubescens (Torrey) D. M. Thompson. He did not reference the study of sect. Diplacus by M. C. Tulig (2000), but results from the Tulig morphometric analyses indicated that D. calycinus is distinct from D. longiflorus, especially in corolla length, corolla tube length, and style length.</discussion>
  <discussion>The type of Diplacus calycinus is from Tulare County, and the concept of the species is perhaps best restricted to the Sierran population system in Fresno, Kern, and Tulare counties, disjunct from D. longiflorus, which occurs primarily in coastal counties. The Sierran system is characterized by distinct abaxial leaf vestiture; the hairs are unbranched, broad, and vitreous, compared to the branched, thinner, and dull hairs of D. longiflorus. Plants of D. calycinus parapatric with D. longiflorus also show a tendency toward the characteristic vestiture and also have lighter-colored (but more variable in color) corollas with narrower but slightly shorter tubes. Intergradation between D. calycinus and D. longiflorus occurs in the region connecting the San Bernardino and San Gabriel mountains in San Bernardino County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granite outcrops, boulders, rocky gullies.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granite outcrops" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="rocky gullies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(300–)700–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="700" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>