<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">483</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="(Jepson) Heckard" date="1973" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>22: 66. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species parishii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orobanche</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="Jepson" date="1925" rank="variety">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>952. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orobanche;species californica;variety parishii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphyllon</taxon_name>
    <taxon_name authority="(Jepson) A. C. Schneider" date="unknown" rank="species">parishii</taxon_name>
    <taxon_hierarchy>genus aphyllon;species parishii</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Parish’s broomrape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple, rarely branched, 5–22 (–35) cm, stout, base not enlarged.</text>
      <biological_entity id="o19375" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19376" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems thickened, fleshy.</text>
      <biological_entity id="o19377" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Roots inconspicuous (often very short and knobby), slender, usually unbranched.</text>
      <biological_entity id="o19378" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves numerous, appressed;</text>
      <biological_entity id="o19379" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate, 7–12 mm, margins entire, apex obtuse, surfaces glandular-pubescent.</text>
      <biological_entity id="o19380" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19381" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19382" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o19383" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences spikelike racemes, pallid creamy or yellow, purplish tinged, simple, sometimes branched, densely glandular-pubescent;</text>
      <biological_entity id="o19384" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pallid creamy" value_original="pallid creamy" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purplish tinged" value_original="purplish tinged" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o19385" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers numerous;</text>
      <biological_entity id="o19386" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts erect to spreading, narrowly lanceolate-ovate, 10–12 (–19) mm, apex acute, glandular-pubescent.</text>
      <biological_entity id="o19387" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="19" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19388" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0–10 (–12) mm, much shorter than plant axis;</text>
      <biological_entity id="o19389" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character constraint="than plant axis" constraintid="o19390" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="plant" id="o19390" name="axis" name_original="axis" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 2.</text>
      <biological_entity id="o19391" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx white or pinkish, or purple-tinged, often pallid, radially or weakly bilaterally symmetric, 7–18 mm, deeply divided into 5 lobes, lobes subulate to attenuate, densely glandular-pubescent;</text>
      <biological_entity id="o19392" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19393" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="pallid" value_original="pallid" />
        <character is_modifier="false" modifier="radially; weakly bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
        <character constraint="into lobes" constraintid="o19394" is_modifier="false" modifier="deeply" name="shape" src="d0_s10" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o19394" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o19395" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s10" to="attenuate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla 15–25 mm, tube white to pallid, yellow, or buff, slightly constricted above ovary, straight to curved forward, glandular-pubescent;</text>
      <biological_entity id="o19396" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19397" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19398" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pallid yellow or buff" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pallid yellow or buff" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pallid yellow or buff" />
        <character constraint="above ovary" constraintid="o19399" is_modifier="false" modifier="slightly" name="size" src="d0_s11" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="straight" modifier="forward" name="course" notes="" src="d0_s11" to="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o19399" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>palatal folds prominent, yellow, glabrous;</text>
      <biological_entity id="o19400" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19401" name="fold" name_original="folds" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lips externally white to pallid, yellow, or buff, sometimes slightly reddish tinged distally, internally maroon or reddish purple, sometimes with maroon or reddish purple stripes, veins, or blotches, abaxial lip erect to spreading or recurved, 4–8 mm, lobes narrowly ovate to oblong, apex rounded, blunt, retuse, or erosulate, adaxial lip erect to ± spreading, 4–8 mm, lobes oblong or oblong-ovate, apex rounded, truncate, retuse, or erosulate;</text>
      <biological_entity id="o19402" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19403" name="lip" name_original="lips" src="d0_s13" type="structure">
        <character char_type="range_value" from="externally white" name="coloration" src="d0_s13" to="pallid yellow or buff" />
        <character char_type="range_value" from="externally white" name="coloration" src="d0_s13" to="pallid yellow or buff" />
        <character char_type="range_value" from="externally white" name="coloration" src="d0_s13" to="pallid yellow or buff" />
        <character is_modifier="false" modifier="sometimes slightly; slightly; distally" name="coloration" src="d0_s13" value="reddish tinged" value_original="reddish tinged" />
        <character is_modifier="false" modifier="internally" name="coloration" src="d0_s13" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o19404" name="strip" name_original="stripes" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="maroon" value_original="maroon" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o19405" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="blotches" value_original="blotches" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19406" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="spreading or recurved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19407" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s13" to="oblong" />
      </biological_entity>
      <biological_entity id="o19408" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="erosulate" value_original="erosulate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19409" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="more or less spreading" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19410" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
      <biological_entity id="o19411" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="erosulate" value_original="erosulate" />
      </biological_entity>
      <relation from="o19403" id="r1498" modifier="sometimes" name="with" negation="false" src="d0_s13" to="o19404" />
    </statement>
    <statement id="d0_s14">
      <text>filaments sparsely pilose at base, sometimes glandular-hairs present near connective, anthers included, moderately woolly or glabrous.</text>
      <biological_entity id="o19412" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19413" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="at base" constraintid="o19414" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o19414" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o19415" name="glandular-hair" name_original="glandular-hairs" src="d0_s14" type="structure">
        <character constraint="near connective" constraintid="o19416" is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19416" name="connective" name_original="connective" src="d0_s14" type="structure" />
      <biological_entity id="o19417" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong-ovoid, 7–12 mm.</text>
      <biological_entity id="o19418" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 0.3–0.5 mm.</text>
      <biological_entity id="o19419" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orobanche parishii is distributed mainly in the southern High Sierra Nevada, Tehachapi Mountains, Central Coast, southwestern California, Desert, Inyo, and White mountains, and Channel Islands.</discussion>
  <discussion>The hosts of Orobanche parishii include several herbs and shrubs of Asteraceae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla lobes 18–25 mm, lips 6–8 mm; calyces (7–)10–15(–18) mm, lobes subulate-attenuate; anthers moderately woolly on sutures; stigma lobes spreading, not recurved, thin; mainland range of species.</description>
      <determination>12a. Orobanche parishii subsp. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla lobes 15–20(–24) mm, lips 4–6(–7) mm; calyces 9–11 mm, lobes subulate; anthers glabrous, rarely sparsely woolly; stigma lobes strongly recurved, thick; seaside plants of Channel Islands and mainland San Diego County, California.</description>
      <determination>12b. Orobanche parishii subsp. brachyloba</determination>
    </key_statement>
  </key>
</bio:treatment>