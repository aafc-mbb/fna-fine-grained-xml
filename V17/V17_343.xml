<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">eriantherus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">eriantherus</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species eriantherus;variety eriantherus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>61a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems retrorsely hairy and, usually, sparsely villous or glandular-villous distally.</text>
      <biological_entity id="o35643" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually; sparsely" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves retrorsely hairy and, sometimes, sparsely villous abaxially, retrorsely hairy and villous or glandular-villous adaxially.</text>
      <biological_entity id="o35644" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes; sparsely; abaxially" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: corolla lavender to purple or pink, ventricose-ampliate, (20–) 22–35 (–42) mm, not constricted at orifice;</text>
      <biological_entity id="o35645" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o35646" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s2" to="purple or pink" />
        <character is_modifier="false" name="size" src="d0_s2" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="22" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="42" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s2" to="35" to_unit="mm" />
        <character constraint="at orifice" constraintid="o35647" is_modifier="false" modifier="not" name="size" src="d0_s2" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o35647" name="orifice" name_original="orifice" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>pollen-sacs explanate, 1–1.6 (–1.8) mm;</text>
      <biological_entity id="o35648" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o35649" name="pollen-sac" name_original="pollen-sacs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s3" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s3" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>staminode: distal 4–5 mm densely lanate, hairs yellowish, to 4 mm;</text>
      <biological_entity id="o35650" name="staminode" name_original="staminode" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o35651" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>style 15–20 mm. 2n = 16.</text>
      <biological_entity id="o35652" name="staminode" name_original="staminode" src="d0_s5" type="structure" />
      <biological_entity id="o35653" name="style" name_original="style" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35654" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly, shaley, and clayey soils, shortgrass prairies, sagebrush shrublands, stream terraces.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaley" modifier="sandy gravelly" />
        <character name="habitat" value="clayey soils" />
        <character name="habitat" value="shortgrass prairies" />
        <character name="habitat" value="shrublands" modifier="sagebrush" />
        <character name="habitat" value="stream terraces" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Colo., Idaho, Mont., Nebr., N.Dak., S.Dak., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>