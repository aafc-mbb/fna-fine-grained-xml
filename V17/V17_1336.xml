<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">412</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="2012" rank="species">grandis</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 43. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species grandis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">guttatus</taxon_name>
    <taxon_name authority="Greene" date="1894" rank="variety">grandis</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. San Francisco,</publication_title>
      <place_in_publication>277. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species guttatus;variety grandis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="(Greene) A. Heller" date="unknown" rank="species">grandis</taxon_name>
    <taxon_hierarchy>genus m.;species grandis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">guttatus</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="subspecies">litoralis</taxon_name>
    <taxon_hierarchy>genus m.;species guttatus;subspecies litoralis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Donn ex Greene" date="unknown" rank="species">langsdorffii</taxon_name>
    <taxon_name authority="(Greene) Greene" date="unknown" rank="variety">grandis</taxon_name>
    <taxon_hierarchy>genus m.;species langsdorffii;variety grandis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">procerus</taxon_name>
    <taxon_hierarchy>genus m.;species procerus</taxon_hierarchy>
  </taxon_identification>
  <number>64.</number>
  <other_name type="common_name">Magnificent monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, rhizomatous, sometimes rooting at proximal nodes.</text>
      <biological_entity id="o1302" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character constraint="at proximal nodes" constraintid="o1303" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1303" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sometimes decumbent basally, branched, often fistulose, (25–) 50–120 (–160) cm, densely hirsutulous to softly hirtellous-puberulent to pilose-hirsutulous, hairs usually crinkly, and eglandular or with a mixture of hirtellous-puberulent and stipitate-glandular hairs, sometimes ± stipitate-glandular or glandular-villous without hirtellous-puberulent hairs.</text>
      <biological_entity id="o1304" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes; basally" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="25" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="160" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="densely hirsutulous" name="pubescence" src="d0_s1" to="softly hirtellous-puberulent" />
      </biological_entity>
      <biological_entity id="o1305" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_shape" src="d0_s1" value="crinkly" value_original="crinkly" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="with a mixture" value_original="with a mixture" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="without hairs" constraintid="o1308" is_modifier="false" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o1306" name="mixture" name_original="mixture" src="d0_s1" type="structure" />
      <biological_entity id="o1307" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1308" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
      </biological_entity>
      <relation from="o1305" id="r102" name="with" negation="false" src="d0_s1" to="o1306" />
      <relation from="o1306" id="r103" name="part_of" negation="false" src="d0_s1" to="o1307" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal usually not persistent, bracteate in inflorescence;</text>
      <biological_entity id="o1309" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1310" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character constraint="in inflorescence" constraintid="o1311" is_modifier="false" name="architecture" src="d0_s2" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o1311" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–80 mm, gradually reduced distally;</text>
      <biological_entity id="o1312" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="80" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subpinnately, sometimes palmately, 5–7-veined, ovate to broadly elliptic, 25–60 × 20–40 (–60) mm, usually 1–2 times longer than wide, base truncate or truncate-cuneate to subcordate, margins crenulate to dentate, proximally sometimes sublyrate, apex rounded to obtuse, surfaces of distals densely hirsutulous to softly hirtellous-puberulent to pilose-hirsutulous, hairs usually crinkly, and eglandular or with a mixture of hirtellous-puberulent and stipitate-glandular hairs, sometimes ± stipitate-glandular or glandular-villous without hirtellous-puberulent hairs.</text>
      <biological_entity id="o1313" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes palmately; palmately" name="architecture" src="d0_s4" value="5-7-veined" value_original="5-7-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly elliptic" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="1-2" value_original="1-2" />
      </biological_entity>
      <biological_entity id="o1314" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate-cuneate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o1315" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenulate" name="shape" src="d0_s4" to="dentate" />
        <character is_modifier="false" modifier="proximally sometimes" name="shape" src="d0_s4" value="sublyrate" value_original="sublyrate" />
      </biological_entity>
      <biological_entity id="o1316" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o1317" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="densely hirsutulous" name="pubescence" src="d0_s4" to="softly hirtellous-puberulent" />
      </biological_entity>
      <biological_entity id="o1318" name="distal" name_original="distals" src="d0_s4" type="structure" />
      <biological_entity id="o1319" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_shape" src="d0_s4" value="crinkly" value_original="crinkly" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="with a mixture" value_original="with a mixture" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="without hairs" constraintid="o1322" is_modifier="false" name="pubescence" src="d0_s4" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o1320" name="mixture" name_original="mixture" src="d0_s4" type="structure" />
      <biological_entity id="o1321" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1322" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
      </biological_entity>
      <relation from="o1317" id="r104" name="part_of" negation="false" src="d0_s4" to="o1318" />
      <relation from="o1319" id="r105" name="with" negation="false" src="d0_s4" to="o1320" />
      <relation from="o1320" id="r106" name="part_of" negation="false" src="d0_s4" to="o1321" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 8–26, mostly from distal nodes, usually in bracteate racemes.</text>
      <biological_entity constraint="distal" id="o1324" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o1325" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <relation from="o1323" id="r107" modifier="mostly" name="from" negation="false" src="d0_s5" to="o1324" />
      <relation from="o1323" id="r108" modifier="usually" name="in" negation="false" src="d0_s5" to="o1325" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 10–35 mm, densely hirsutulous to softly hirtellous-puberulent to pilose-hirsutulous, hairs usually crinkly, and eglandular or with a mixture of hirtellous-puberulent and stipitate-glandular hairs, sometimes ± stipitate-glandular or glandular-villous without hirtellous-puberulent hairs.</text>
      <biological_entity id="o1323" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="26" />
      </biological_entity>
      <biological_entity id="o1326" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o1327" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="densely hirsutulous" name="pubescence" src="d0_s6" to="softly hirtellous-puberulent" />
      </biological_entity>
      <biological_entity id="o1329" name="mixture" name_original="mixture" src="d0_s6" type="structure" />
      <biological_entity id="o1330" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1331" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
      </biological_entity>
      <relation from="o1323" id="r109" name="fruiting" negation="false" src="d0_s6" to="o1326" />
      <relation from="o1328" id="r110" name="with" negation="false" src="d0_s6" to="o1329" />
      <relation from="o1329" id="r111" name="part_of" negation="false" src="d0_s6" to="o1330" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces straight-erect or nodding 45–100º, ovate-campanulate, inflated, sagittally compressed, 15–22 (–25) mm, densely hirsutulous to softly hirtellous-puberulent to pilose-hirsutulous, hairs usually crinkly, and eglandular or with a mixture of hirtellous-puberulent and stipitate-glandular hairs, sometimes ± stipitate-glandular or glandular-villous without hirtellous-puberulent hairs, throat closing.</text>
      <biological_entity id="o1328" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_shape" src="d0_s6" value="crinkly" value_original="crinkly" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="with a mixture" value_original="with a mixture" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="without hairs" constraintid="o1331" is_modifier="false" name="pubescence" src="d0_s6" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o1332" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o1333" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="straight-erect" value_original="straight-erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-campanulate" value_original="ovate-campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sagittally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="22" to_unit="mm" />
        <character char_type="range_value" from="densely hirsutulous" name="pubescence" src="d0_s7" to="softly hirtellous-puberulent" />
      </biological_entity>
      <biological_entity id="o1334" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_shape" src="d0_s7" value="crinkly" value_original="crinkly" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="with a mixture" value_original="with a mixture" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="without hairs" constraintid="o1337" is_modifier="false" name="pubescence" src="d0_s7" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o1335" name="mixture" name_original="mixture" src="d0_s7" type="structure" />
      <biological_entity id="o1336" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
        <character is_modifier="true" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1337" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
      </biological_entity>
      <biological_entity id="o1338" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
      <relation from="o1328" id="r112" name="fruiting" negation="false" src="d0_s7" to="o1332" />
      <relation from="o1334" id="r113" name="with" negation="false" src="d0_s7" to="o1335" />
      <relation from="o1335" id="r114" name="part_of" negation="false" src="d0_s7" to="o1336" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, red-dotted within, bilaterally symmetric, bilabiate;</text>
      <biological_entity id="o1339" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-dotted" value_original="red-dotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat broadly funnelform, (14–) 16–24 mm, exserted (8–) 10–15 mm beyond calyx margin;</text>
      <biological_entity id="o1340" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_distance" src="d0_s9" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="distance" src="d0_s9" to="24" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_distance" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" constraint="beyond calyx margin" constraintid="o1341" from="10" from_unit="mm" name="distance" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o1341" name="margin" name_original="margin" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>limb broadly expanded.</text>
      <biological_entity id="o1342" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles hirtellous.</text>
      <biological_entity id="o1343" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o1344" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 8–12 mm. 2n = 28.</text>
      <biological_entity id="o1345" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1346" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The densely, evenly puberulent vestiture of pedicels, calyces, and distal stems usually is diagnostic, especially in combination with the large flowers (corollas and mature calyces) and tall stature. Plants from scattered collections are much shorter than normal but have large corollas and characteristic vestiture.</discussion>
  <discussion>Erythranthe grandis characteristically occurs in coastal localities from southern California to northern Oregon but also is found in inland localities and habitats near the coast but well away from salt spray.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Beaches, dunes, coastal bluffs, wet cliff faces, mud flats and seeps, marshes, drainage ditches, creeks, rarely in coastal sage scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beaches" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="wet cliff" />
        <character name="habitat" value="mud flats" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="drainage ditches" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="coastal sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200(–800) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>