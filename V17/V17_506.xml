<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">210</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">laevigatus</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 361. 1789</place_in_publication>
      <other_info_on_pub>(as Pentstemon laevigata)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species laevigatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>170.</number>
  <other_name type="common_name">Eastern beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o8483" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 40–115 cm, glabrous or retrorsely hairy, not glaucous.</text>
      <biological_entity id="o8484" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="115" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal sometimes withering by anthesis, not leathery, glabrous or puberulent;</text>
      <biological_entity id="o8485" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 40–150 × 7–48 mm, blade oblanceolate to lanceolate, base tapered, margins entire or ± serrate, apex obtuse to acute;</text>
      <biological_entity constraint="basal" id="o8486" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="sometimes" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o8487" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o8488" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o8489" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8490" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8491" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–7 pairs, sessile or proximals sometimes petiolate, 18–128 × 8–28 mm, blade lanceolate, proximals sometimes oblanceolate, base clasping, sometimes tapered, margins entire or ± serrate, apex acute.</text>
      <biological_entity constraint="cauline" id="o8492" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o8493" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s4" to="128" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8494" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o8495" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o8496" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8497" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8498" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, narrowly conic, (3–) 8–20 cm, axis glabrous or retrorsely hairy and sparsely glandular-pubescent, verticillasters 2–6, cymes (2–) 4–11-flowered, 2 per node;</text>
      <biological_entity id="o8499" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8500" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o8501" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o8502" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(2-)4-11-flowered" value_original="(2-)4-11-flowered" />
        <character constraint="per node" constraintid="o8503" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8503" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate to linear, 8–38 × 1–3 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o8504" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="38" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8505" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels spreading or ascending, glabrous or retrorsely hairy and sparsely glandular-pubescent.</text>
      <biological_entity id="o8506" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o8507" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate, 3–6 × 1.5–2.1 mm, glabrous or glandular-pubescent;</text>
      <biological_entity id="o8508" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o8509" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pale lavender to violet, with reddish purple nectar guides, ventricose, 15–22 mm, glandular-pubescent externally, sparsely to moderately white-lanate internally abaxially, tube 5–6 mm, throat abruptly inflated, 5–8 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o8510" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8511" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s9" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sparsely to moderately; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o8512" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o8513" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8514" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o8511" id="r699" name="with" negation="false" src="d0_s9" to="o8512" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included or longer pair reaching orifice, pollen-sacs opposite, navicular, 1–1.2 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o8515" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8516" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8517" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o8518" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o8519" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o8520" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8521" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o8516" id="r700" name="reaching" negation="false" src="d0_s10" to="o8517" />
    </statement>
    <statement id="d0_s11">
      <text>staminode 10–12 mm, exserted, 0.4–0.5 mm diam., tip straight to slightly recurved, distal 5–7 mm moderately to densely pubescent, hairs yellow, to 2.3 mm;</text>
      <biological_entity id="o8522" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8523" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8524" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8525" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8526" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 12–15 mm.</text>
      <biological_entity id="o8527" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8528" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–8 × 3.5–5 mm, glabrous.</text>
      <biological_entity id="o8529" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon laevigatus primarily is a species of the Piedmont and eastern Appalachians; scattered populations occur outside those regions. L. La Cour (1931) reported a chromosome count of n = 48 for P. laevigatus. Without a voucher, it is impossible to know if this count was for the species as treated here, or P. calycosus or P. digitalis, which have been treated as infraspecific taxa of P. laevigatus.</discussion>
  <discussion>An infusion made from Penstemon laevigatus is used by the Cherokee tribe of the southeastern United States as a gastrointestinal aid (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, floodplain forests, fields, rock outcrops, calcareous bluffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="floodplain forests" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="bluffs" modifier="calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Del., D.C., Fla., Ga., Ky., La., Md., Mass., Miss., N.J., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>