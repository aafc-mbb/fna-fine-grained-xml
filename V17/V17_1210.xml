<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">674</other_info_on_meta>
    <other_info_on_meta type="mention_page">670</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">CORDYLANTHUS</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">pilosus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 382. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus cordylanthus;species pilosus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Hairy bird’s-beak</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending, 10–90 (–120) cm, densely puberulent and glandular-puberulent, and pilose.</text>
      <biological_entity id="o16147" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent and pilose;</text>
      <biological_entity id="o16148" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal 15–40 mm, margins 3-lobed, lobes 0.1–0.5 mm wide;</text>
      <biological_entity constraint="proximal" id="o16149" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16150" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o16151" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal 10–25 × 0.1–0.3 mm, margins entire.</text>
      <biological_entity constraint="distal" id="o16152" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s3" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16153" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences spikes, 2-flowered or 3-flowered, or flowers solitary;</text>
      <biological_entity constraint="inflorescences" id="o16154" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-flowered" value_original="2-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-flowered" value_original="3-flowered" />
      </biological_entity>
      <biological_entity id="o16155" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 1–4, 10–20 mm, margins entire or 3-lobed, lobes green or purple distally, linear-lanceolate.</text>
      <biological_entity id="o16156" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16157" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o16158" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: bracteoles 15–20 mm, margins entire or toothed.</text>
      <biological_entity id="o16159" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o16160" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16161" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 15–20 mm, tube 0–1 mm, apex 2-fid, cleft 0.5–1 mm;</text>
      <biological_entity id="o16162" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16163" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16164" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16165" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla pale-yellow to yellow-green, streaked and spotted with maroon, 15–20 mm, tube 5–10 mm, throat 5–8 mm diam., abaxial lip 5–10 mm, ca. equal to and appressed to adaxial;</text>
      <biological_entity id="o16166" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16167" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="streaked and spotted with maroon" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16168" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16169" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16170" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16171" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4, filaments hairy, fertile pollen-sacs 2 per filament, equal.</text>
      <biological_entity id="o16172" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16173" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o16174" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16175" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character constraint="per filament" constraintid="o16176" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="variability" notes="" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o16176" name="filament" name_original="filament" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules oblong-ovoid, 6–10 mm.</text>
      <biological_entity id="o16177" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 10–18, dark-brown, ovoid, 1.5–2.5 mm, striate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28.</text>
      <biological_entity id="o16178" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="18" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s11" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16179" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts entire.</description>
      <determination>8a. Cordylanthus pilosus subsp. pilosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts 3-lobed.</description>
      <determination>8b. Cordylanthus pilosus subsp. trifidus</determination>
    </key_statement>
  </key>
</bio:treatment>