<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
    <other_info_on_meta type="mention_page">511</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">centranthera</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 120. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species centranthera</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Dwarf lousewort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–12 cm.</text>
      <biological_entity id="o33610" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 6–8, blade elliptic or spatulate, 35–120 x 10–30 mm, undivided or 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping or extensively overlapping distally, entire or 1-serrate or 2-serrate, surfaces glabrous or scattered abaxial glands;</text>
      <biological_entity id="o33611" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o33612" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s1" to="8" />
      </biological_entity>
      <biological_entity id="o33613" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s1" to="120" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="30" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o33614" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="nonoverlapping" value_original="nonoverlapping" />
        <character is_modifier="false" modifier="extensively; distally" name="arrangement" src="d0_s1" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-serrate" value_original="2-serrate" />
      </biological_entity>
      <biological_entity id="o33615" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o33616" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33617" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o33614" id="r2557" name="part_of" negation="false" src="d0_s1" to="o33615" />
    </statement>
    <statement id="d0_s2">
      <text>cauline 0–4, blade elliptic, sometimes lanceolate, 20–110 x 5–30 mm, 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes overlapping distally, 1-serrate or 2-serrate, surfaces glabrous.</text>
      <biological_entity id="o33618" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o33619" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o33620" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="110" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o33621" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="2-serrate" value_original="2-serrate" />
      </biological_entity>
      <biological_entity id="o33622" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o33623" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o33621" id="r2558" name="part_of" negation="false" src="d0_s2" to="o33622" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes simple, 1–4, not exceeding basal leaves, each 8–14-flowered;</text>
      <biological_entity id="o33624" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="8-14-flowered" value_original="8-14-flowered" />
      </biological_entity>
      <biological_entity constraint="basal" id="o33625" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o33624" id="r2559" name="exceeding" negation="true" src="d0_s3" to="o33625" />
    </statement>
    <statement id="d0_s4">
      <text>bracts spatulate, 40–60 x 3–6 mm, undivided proximally, undivided or 1-pinnatifid distally, proximal margins entire, distal 1-serrate or 2-serrate, surfaces glabrous, sometimes arachnoid along main veins.</text>
      <biological_entity id="o33626" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="1-pinnatifid" value_original="1-pinnatifid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o33627" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o33628" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-serrate" value_original="2-serrate" />
      </biological_entity>
      <biological_entity id="o33629" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="along main veins" constraintid="o33630" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity constraint="main" id="o33630" name="vein" name_original="veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–5 mm.</text>
      <biological_entity id="o33631" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 17–22 mm, glabrous, lobes 5, narrowly triangular, 4–7 mm, apex entire or serrate, glabrous or ciliate;</text>
      <biological_entity id="o33632" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o33633" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s6" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33634" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33635" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 28–40 mm, tube white or pale-purple, 15–30 mm;</text>
      <biological_entity id="o33636" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o33637" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33638" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-purple" value_original="pale-purple" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>galea white or pale-purple, apically sometimes dark violet to purple, 13–15 mm, beakless, margins entire medially and distally, apex arching over abaxial lip;</text>
      <biological_entity id="o33639" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33640" name="galea" name_original="galea" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-purple" value_original="pale-purple" />
        <character char_type="range_value" from="dark violet" modifier="apically sometimes; sometimes" name="coloration" src="d0_s8" to="purple" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity id="o33641" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o33642" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="over abaxial lip" constraintid="o33643" is_modifier="false" modifier="distally" name="orientation" src="d0_s8" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33643" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip purple, 1–4 mm.</text>
      <biological_entity id="o33644" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o33645" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., N.Mex., Nev., Oreg., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The leaves of Pedicularis centranthera exceed the inflorescence, giving the impression that the cauline leaves are basal. Proximalmost basal leaves are distinct: brown, membranous, and spatulate with undivided and entire margins. Pedicularis semibarbata has similar basal leaves.</discussion>
  <discussion>Pedicularis centranthera occurs in evergreen forests, often under pinyon pine, juniper, ponderosa pine, and yellow pine.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts arachnoid and/or margins ciliate.</description>
      <determination>7a. Pedicularis centranthera var. centranthera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts glabrous.</description>
      <determination>7b. Pedicularis centranthera var. exulans</determination>
    </key_statement>
  </key>
</bio:treatment>