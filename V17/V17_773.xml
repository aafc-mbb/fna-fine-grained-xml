<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">agrestis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 13. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species agrestis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Field speedwell</other_name>
  <other_name type="common_name">véronique agreste</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o40032" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping to decumbent, 5–25 cm, hairy.</text>
      <biological_entity id="o40033" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="creeping" name="growth_form_or_orientation" src="d0_s1" to="decumbent" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade elliptic-ovate to oblong, (6–) 8–16 (–19) × (3–) 4–10 (–13) mm, base truncate, margins serrate, teeth 4–6 (–8) per side, apex obtuse to acute, abaxial surface ± more densely hairy than adaxial.</text>
      <biological_entity id="o40034" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o40035" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s2" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="19" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="13" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40036" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o40037" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o40038" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="8" />
        <character char_type="range_value" constraint="per side" constraintid="o40039" from="4" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o40039" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o40040" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o40041" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character constraint="than adaxial surface" constraintid="o40042" is_modifier="false" name="pubescence" src="d0_s2" value="more or less more densely hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o40042" name="surface" name_original="surface" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1–5, terminal, 50–250 mm, 5–15-flowered, axis eglandular-hairy, sometimes glandular-hairy;</text>
      <biological_entity id="o40043" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s3" to="250" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="5-15-flowered" value_original="5-15-flowered" />
      </biological_entity>
      <biological_entity id="o40044" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts elliptic-ovate to oblong, (6–) 9–16 (–19) mm.</text>
      <biological_entity id="o40045" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels deflexed, (5–) 6–10 (–15) mm, ± shorter than subtending bracts, densely eglandular and, sometimes, glandular-hairy distally.</text>
      <biological_entity id="o40046" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="deflexed" value_original="deflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character constraint="than subtending bracts" constraintid="o40047" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="more or less shorter" value_original="more or less shorter" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o40047" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes linear-lanceolate, (5–) 6–7 mm, 2.2–2.6 mm wide, apex rounded, sparsely eglandular or glandular-hairy;</text>
      <biological_entity id="o40048" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o40049" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s6" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40050" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla white or pale pinkish or pale blue, 4–5 (–6) mm diam.;</text>
      <biological_entity id="o40051" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o40052" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale pinkish" value_original="pale pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale blue" value_original="pale blue" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 0.5–1 mm;</text>
      <biological_entity id="o40053" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o40054" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style (0.6–) 0.9–1.1 (–1.2) mm.</text>
      <biological_entity id="o40055" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o40056" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ± compressed in cross-section, ± subglobular, 3.5–4.5 (–4.7) × 4.5–6 (–6.3) mm, apex emarginate, sparsely glandular-hairy.</text>
      <biological_entity id="o40057" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o40058" is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s10" value="subglobular" value_original="subglobular" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="6.3" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40058" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o40059" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 6–14, yellow to ochre, globular, cymbiform, 1.3–2.1 × 1–1.6 mm, 0.6–1.1 mm thick, strongly rugose (transversely ribbed).</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28 (Europe).</text>
      <biological_entity id="o40060" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="14" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="ochre" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globular" value_original="globular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cymbiform" value_original="cymbiform" />
        <character name="thickness" src="d0_s11" unit="mm" value="1.3-2.1×1-1.6" value_original="1.3-2.1×1-1.6" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="thickness" src="d0_s11" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o40061" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Veronica agrestis is similar to the more frequent V. persica and probably frequently overlooked and to be expected elsewhere. However, it should be noted that it is less common than V. persica or V. polita and always in more humid habitats.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Light, noncalcareous, moist soils, gravelly soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="noncalcareous" modifier="light" />
        <character name="habitat" value="moist soils" />
        <character name="habitat" value="gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(0–)300–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Nfld. and Labr. (Nfld.), N.S., Que.; D.C., Fla., Ill., La., Mass., N.Y., Pa., Tex., Vt.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>