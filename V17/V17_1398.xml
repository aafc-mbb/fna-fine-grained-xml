<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="2012" rank="species">angustatus</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 30. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species angustatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eunanus</taxon_name>
    <taxon_name authority="Harvey &amp; A. Gray ex Bentham" date="unknown" rank="species">coulteri</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="variety">angustatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 381. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eunanus;species coulteri;variety angustatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">angustatus</taxon_name>
    <taxon_hierarchy>genus e.;species angustatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">angustatus</taxon_name>
    <taxon_hierarchy>genus mimulus;species angustatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Hartweg ex Lindley" date="unknown" rank="species">tricolor</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">angustatus</taxon_name>
    <taxon_hierarchy>genus m.;species tricolor;variety angustatus</taxon_hierarchy>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Narrow-leaved or purple-lip pansy monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, acaulescent or caulescent.</text>
      <biological_entity id="o17807" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 0 or 20–70 mm, eglandular-puberulent.</text>
      <biological_entity id="o17808" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s1" value="20-70 mm" value_original="20-70 mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-puberulent" value_original="eglandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal densely clustered;</text>
      <biological_entity id="o17809" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o17810" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent;</text>
      <biological_entity id="o17811" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17812" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly lanceolate or elliptic to linear, (3.5–) 5–36 × 0.5–3.8 (–5) mm, margins entire, plane, proximal 1/2+ ciliate, apex obtuse, surfaces glabrous.</text>
      <biological_entity id="o17813" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17814" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic to linear" value_original="elliptic to linear" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="36" to_unit="mm" />
        <character char_type="range_value" from="3.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17815" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o17816" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17817" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0–1 mm in fruit.</text>
      <biological_entity id="o17818" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o17819" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17819" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1 per node, chasmogamous.</text>
      <biological_entity id="o17820" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o17821" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o17821" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces slightly asymmetrically attached to pedicel, inflated in fruit, (4–) 7–14 (–17) mm, pilose, lobes subequal, apex obtuse, ribs becoming reddish, intercostal areas pale green to whitish.</text>
      <biological_entity id="o17822" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicel" constraintid="o17823" is_modifier="false" modifier="slightly asymmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o17824" is_modifier="false" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o17823" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o17824" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o17825" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o17826" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17827" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o17828" name="area" name_original="areas" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s7" to="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas: throat and limb magenta to purple with a dark maroon-purple spot or elongate blotch at base of each abaxial lobe, palate ridges and base of abaxial lip yellow, tube-throat 20–60 mm, limb 10–20 mm diam., bilabiate, lobes equal.</text>
      <biological_entity id="o17829" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
      <biological_entity id="o17830" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with blotch" constraintid="o17832" from="magenta" name="coloration" src="d0_s8" to="purple" />
      </biological_entity>
      <biological_entity id="o17831" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with blotch" constraintid="o17832" from="magenta" name="coloration" src="d0_s8" to="purple" />
      </biological_entity>
      <biological_entity id="o17832" name="blotch" name_original="blotch" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark maroon-purple spot" value_original="dark maroon-purple spot" />
        <character is_modifier="true" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o17833" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17834" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o17835" name="ridge" name_original="ridges" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o17836" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity id="o17837" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o17838" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o17839" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s8" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17840" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity id="o17841" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <relation from="o17832" id="r1377" name="at" negation="false" src="d0_s8" to="o17833" />
      <relation from="o17833" id="r1378" name="part_of" negation="false" src="d0_s8" to="o17834" />
      <relation from="o17833" id="r1379" name="part_of" negation="false" src="d0_s8" to="o17835" />
      <relation from="o17833" id="r1380" name="part_of" negation="false" src="d0_s8" to="o17837" />
      <relation from="o17833" id="r1381" name="part_of" negation="false" src="d0_s8" to="o17838" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers (distal pair) sometimes nearly exserted, hirsute with 2 apical tufts.</text>
      <biological_entity id="o17842" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes nearly" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character constraint="with apical tufts" constraintid="o17843" is_modifier="false" name="pubescence" src="d0_s9" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="apical" id="o17843" name="tuft" name_original="tufts" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent.</text>
      <biological_entity id="o17844" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas usually exserted, lobes subequal.</text>
      <biological_entity id="o17845" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o17846" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 2–4.5 mm, indehiscent.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o17847" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17848" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus angustatus occurs in northern California in the Sierra Nevada and across the Sacramento Valley in the North Coast Range (Lake, Mendocino, and Napa counties); it also is known from one area in northwestern Shasta County and has a disjunct series of populations in Fresno and Tulare counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernally flooded depressions or swales, summits of mesas or foothills.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="depressions" modifier="vernally flooded" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="summits" constraint="of mesas or foothills" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="foothills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>