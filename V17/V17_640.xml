<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GRATIOLA</taxon_name>
    <taxon_name authority="D. Estes &amp; R. L. Small" date="2008" rank="species">amphiantha</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>33: 181. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus gratiola;species amphiantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amphianthus</taxon_name>
    <taxon_name authority="Torrey" date="1837" rank="species">pusillus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>4: 82. 1837</place_in_publication>
      <other_info_on_pub>not Gratiola pusilla Willdenow 1797 [= Lindernia pusilla]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus amphianthus;species pusillus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Pool sprite</other_name>
  <other_name type="common_name">snorkelwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o24485" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or few-branched, (7–) 9–21 (–29) cm, glabrous or glabrate proximally, glabrate or glandular-puberulent distally.</text>
      <biological_entity id="o24486" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="21" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="29" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s1" to="21" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: dimorphic, submersed basal and clustered, floating paired at ends of branches to 70 mm, blade of submersed lanceolate to oblanceolate or oblong, 1–7 × 0.5–3 mm, margins entire, apex obtuse to acute, surfaces glabrous, blade of floating elliptic to ovate or nearly round, 3–10 × 2–9 mm, margins entire, apex obtuse to rounded or retuse, surfaces glabrous.</text>
      <biological_entity id="o24487" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24488" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
        <character constraint="at ends" constraintid="o24489" is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
      </biological_entity>
      <biological_entity id="o24489" name="end" name_original="ends" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24490" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o24491" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o24492" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="submersed" value_original="submersed" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s2" to="oblanceolate or oblong" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24493" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="submersed" value_original="submersed" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s2" to="oblanceolate or oblong" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24494" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24495" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="rounded or retuse" />
      </biological_entity>
      <biological_entity id="o24496" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="floating" value_original="floating" />
        <character char_type="range_value" from="elliptic" is_modifier="true" name="shape" src="d0_s2" to="ovate or nearly round" />
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="width" src="d0_s2" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24497" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="floating" value_original="floating" />
        <character char_type="range_value" from="elliptic" is_modifier="true" name="shape" src="d0_s2" to="ovate or nearly round" />
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="width" src="d0_s2" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24498" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o24489" id="r1868" name="part_of" negation="false" src="d0_s2" to="o24490" />
      <relation from="o24491" id="r1869" name="part_of" negation="false" src="d0_s2" to="o24492" />
      <relation from="o24491" id="r1870" name="part_of" negation="false" src="d0_s2" to="o24493" />
      <relation from="o24495" id="r1871" name="part_of" negation="false" src="d0_s2" to="o24496" />
      <relation from="o24495" id="r1872" name="part_of" negation="false" src="d0_s2" to="o24497" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels stout, 0.1–3 mm, length less than 0.1 times bract, glabrous;</text>
      <biological_entity id="o24499" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character constraint="bract" constraintid="o24500" is_modifier="false" name="length" src="d0_s3" value="0-0.1 times bract" value_original="0-0.1 times bract" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24500" name="bract" name_original="bract" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 0.</text>
      <biological_entity id="o24501" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals connate proximally, calyx lobes obovate to oblong, 0.7–1 mm;</text>
      <biological_entity id="o24502" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o24503" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o24504" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla 3–4 mm, tube and limb white tinged pink or purple, veins lavender or purple;</text>
      <biological_entity id="o24505" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o24506" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24507" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white tinged" value_original="white tinged" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o24508" name="limb" name_original="limb" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white tinged" value_original="white tinged" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o24509" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 0.5–1 mm.</text>
      <biological_entity id="o24510" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24511" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules bilaterally symmetric, obcordiform, 2–3 × 3–4 mm.</text>
      <biological_entity id="o24512" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obcordiform" value_original="obcordiform" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds (0.3–) 0.4–0.5 mm. 2n = 18.</text>
      <biological_entity id="o24513" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24514" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phylogenetic studies confirmed that Gratiola amphiantha, long treated in monospecific Amphianthus, is embedded in Gratiola (D. Estes and R. L. Small 2008). Gratiola amphiantha is listed as a federally-threatened species by the United States Fish and Wildlife Service.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow, ephemeral pools on exposed granite outcrops.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow" constraint="on exposed granite" />
        <character name="habitat" value="ephemeral pools" constraint="on exposed granite" />
        <character name="habitat" value="exposed granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>