<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="Brandegee" date="1899" rank="species">incertus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 454. 1899</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species incertus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>234.</number>
  <other_name type="common_name">Mojave beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs.</text>
      <biological_entity id="o32952" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 22–100 cm, glaucous.</text>
      <biological_entity id="o32954" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="22" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: cauline 6–18 pairs, sessile, (8–) 25–70 × (1–) 2.5–4 (–6) mm, blade lanceolate to linear, base tapered, margins entire, apex acute to acuminate.</text>
      <biological_entity id="o32955" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o32956" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="18" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s2" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s2" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32957" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
      <biological_entity id="o32958" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o32959" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o32960" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses interrupted, cylindric, 3–27 cm, axis sparsely glandular-pubescent, rarely glabrous, verticillasters (2–) 4–9, cymes 1–3 (–5) -flowered;</text>
      <biological_entity id="o32961" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="27" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32962" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32963" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity id="o32964" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-3(-5)-flowered" value_original="1-3(-5)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts lanceolate to linear, 3–35 × 1–5 mm;</text>
      <biological_entity constraint="proximal" id="o32965" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels spreading or ascending, glandular-pubescent.</text>
      <biological_entity id="o32966" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o32967" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes ovate, 3.8–6 × 2.6–4 mm, glabrous or sparsely glandular-pubescent;</text>
      <biological_entity id="o32968" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o32969" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla blue to violet or purple, without nectar guides, strongly bilabiate, ventricose-ampliate, 23–32 mm, glabrous or glandular-pubescent externally, sparsely white-lanate internally abaxially, glandular-pubescent adaxially, tube 7–11 mm, length 1.8–2 times calyx lobes, throat gradually inflated, not constricted at orifice, 8–13 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o32970" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o32971" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s7" to="violet or purple" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s7" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="size" src="d0_s7" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s7" to="32" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sparsely; internally abaxially; abaxially" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o32972" name="guide" name_original="guides" src="d0_s7" type="structure" />
      <biological_entity id="o32973" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character constraint="lobe" constraintid="o32974" is_modifier="false" name="length" src="d0_s7" value="1.8-2 times calyx lobes" value_original="1.8-2 times calyx lobes" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o32974" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o32975" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o32976" is_modifier="false" modifier="not" name="size" src="d0_s7" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" notes="" src="d0_s7" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s7" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o32976" name="orifice" name_original="orifice" src="d0_s7" type="structure" />
      <relation from="o32971" id="r2498" name="without" negation="false" src="d0_s7" to="o32972" />
    </statement>
    <statement id="d0_s8">
      <text>stamens included, filaments of shorter pair glandular-puberulent proximally, pollen-sacs navicular, 1.8–2.4 mm, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o32977" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o32978" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o32979" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o32980" name="pair" name_original="pair" src="d0_s8" type="structure" />
      <biological_entity id="o32981" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="distance" src="d0_s8" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32982" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o32983" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o32979" id="r2499" name="part_of" negation="false" src="d0_s8" to="o32980" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 10–13 mm, included, 0.6–0.7 mm diam., tip straight, distal 3–7 mm sparsely to densely pilose, hairs yellow, to 1 mm;</text>
      <biological_entity id="o32984" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32985" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32986" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32987" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o32988" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal 1–3 mm glandular-pubescent;</text>
      <biological_entity id="o32989" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o32990" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 19–23 mm, glabrous.</text>
      <biological_entity id="o32991" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32992" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s11" to="23" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 10–17 × 5–7 mm, glabrous.</text>
      <biological_entity id="o32993" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s12" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Populations of Penstemon incertus are concentrated in the southern Sierra Nevada and adjacent Tehachapi Mountains, with other populations scattered in mountain ranges in and near the Mojave Desert. The species is documented in Inyo, Kern, Los Angeles, Riverside, San Bernardino, San Diego, and Tulare counties. Occasional specimens from the Kingston Range and the eastern flank of the southern Sierra Nevada combine features of P. incertus and P. fruticiformis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, sagebrush shrublands, Joshua tree and pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="joshua tree" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>