<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Gentianoides</taxon_name>
    <taxon_name authority="(Bentham) Bentham" date="1835" rank="species">centranthifolius</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>7. 1835</place_in_publication>
      <other_info_on_pub>(as Pentstemon centranthifolium)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section gentianoides;species centranthifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chelone</taxon_name>
    <taxon_name authority="Bentham" date="1835" rank="species">centranthifolia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Hort. Soc. London, ser.</publication_title>
      <place_in_publication>2, 1: 481. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus chelone;species centranthifolia</taxon_hierarchy>
  </taxon_identification>
  <number>86.</number>
  <other_name type="common_name">Scarlet bugler</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 30–120 cm, glaucous.</text>
      <biological_entity id="o31011" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves glabrous, glaucous;</text>
      <biological_entity id="o31012" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 5–11 pairs, short-petiolate or sessile, 40–100 × 10–40 mm, blade ovate to lanceolate, base tapered to auriculate-clasping, margins entire, apex rounded to acute.</text>
      <biological_entity constraint="cauline" id="o31013" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="11" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31014" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o31015" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o31016" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31017" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses interrupted, secund to ± cylindric, 15–60 (–100) cm, axis glabrous or obscurely scabrous, verticillasters 8–18 (–22), cymes 1–5 (–11) -flowered;</text>
      <biological_entity id="o31018" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="100" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31019" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31020" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="22" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="18" />
      </biological_entity>
      <biological_entity id="o31021" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-5(-11)-flowered" value_original="1-5(-11)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts ovate to lanceolate, (3–) 7–25 (–87) × (1–) 3–14 (–30) mm;</text>
      <biological_entity constraint="proximal" id="o31022" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s4" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="87" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels erect, glabrous or obscurely scabrous.</text>
      <biological_entity id="o31023" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31024" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes ovate to lanceolate, 3–6 × 2–3 mm, margins erose, glabrous;</text>
      <biological_entity id="o31025" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o31026" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31027" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla scarlet, without nectar guides, nearly radially symmetric, weakly bilabiate, salverform, 25–33 mm, glabrous externally, glabrous internally, tube 8–10 mm, throat slightly inflated, 4.5–6 mm diam., rounded abaxially;</text>
      <biological_entity id="o31028" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o31029" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" modifier="nearly radially" name="architecture_or_shape" notes="" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="salverform" value_original="salverform" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="33" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o31030" name="guide" name_original="guides" src="d0_s7" type="structure" />
      <biological_entity id="o31031" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31032" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o31029" id="r2355" name="without" negation="false" src="d0_s7" to="o31030" />
    </statement>
    <statement id="d0_s8">
      <text>stamens: longer pair reaching orifice, pollen-sacs explanate, 0.9–1.2 mm, sutures smooth;</text>
      <biological_entity id="o31033" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o31034" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <biological_entity id="o31035" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31036" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o31033" id="r2356" name="reaching" negation="false" src="d0_s8" to="o31034" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 13–14 mm, terete to slightly flattened distally, 0.1–0.3 mm diam., tip straight, glabrous;</text>
      <biological_entity id="o31037" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o31038" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="terete" modifier="distally" name="shape" src="d0_s9" to="slightly flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31039" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 18–27 mm, exserted.</text>
      <biological_entity id="o31040" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o31041" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s10" to="27" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 8–11 × 6–7 mm. 2n = 16.</text>
      <biological_entity id="o31042" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31043" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon ×parishii A. Gray, a naturally occurring hybrid between P. centranthifolius and P. spectabilis (A. D. Wolfe  and W. J. Elisens 1993), has been reported from Los Angeles, Riverside, San Bernardino, and San Diego counties, California (D. D. Keck 1937; Paul Wilson and M. Valenzuela 2002). A wild hybrid between P. centranthifolius and P. eatonii also was reported from San Bernardino County, California (Wilson and Valenzuela). Penstemon ×dubius Davidson was described from Mount Lowe in the San Gabriel Mountains, Los Angeles County, California, growing with P. centranthifolius and P. grinnellii. Hybridization between P. centranthifolius and P. grinnellii has been documented using allozyme and DNA data (Wolfe and Elisens 1993, 1994).</discussion>
  <discussion>Three southern and central Californian tribes of Native Americans used Penstemon centranthifolius for drugs, food, and decorations (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hillsides, chaparral, oak, pinyon-juniper, and Joshua tree woodlands, coastal sage scrub, pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="joshua tree woodlands" />
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="forests" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>