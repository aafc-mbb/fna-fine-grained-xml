<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MIMULUS</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">alatus</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 361. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus mimulus;species alatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Sharp-wing monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, narrowly winged on angles, 30–70 cm.</text>
      <biological_entity id="o7384" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character constraint="on angles" constraintid="o7385" is_modifier="false" modifier="narrowly" name="architecture" src="d0_s0" value="winged" value_original="winged" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7385" name="angle" name_original="angles" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves petiolate;</text>
      <biological_entity id="o7386" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblong-lanceolate to broadly lanceolate or ovate, 50–80 (–150) × 25–40 mm, base rounded to cuneate, margins coarsely serrate, apex acute-acuminate.</text>
      <biological_entity id="o7387" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="broadly lanceolate or ovate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="150" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7388" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o7389" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting pedicels 5–14 (–30) mm, shorter than calyces.</text>
      <biological_entity id="o7390" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute-acuminate" value_original="acute-acuminate" />
        <character constraint="than calyces" constraintid="o7392" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7391" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <biological_entity id="o7392" name="calyx" name_original="calyces" src="d0_s3" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
      </biological_entity>
      <relation from="o7390" id="r618" name="fruiting" negation="false" src="d0_s3" to="o7391" />
    </statement>
    <statement id="d0_s4">
      <text>Calyces broadly cylindric, 12–18 mm, lobes deltate, 0.8–2.5 mm, apex obtuse-aristate, apiculate, or subulate, ciliate or glabrous.</text>
      <biological_entity id="o7393" name="calyx" name_original="calyces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7394" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7395" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse-aristate" value_original="obtuse-aristate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas blue to light violet or pinkish, rarely white, tube 20–28 mm, throats closed, palate villous.</text>
      <biological_entity id="o7396" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s5" to="light violet or pinkish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7397" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7398" name="throat" name_original="throats" src="d0_s5" type="structure">
        <character is_modifier="false" name="condition" src="d0_s5" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity id="o7399" name="palate" name_original="palate" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules ovate-oblong to ovoid, 8–11 mm. 2n = 22.</text>
      <biological_entity id="o7400" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s6" to="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7401" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Floodplain forests, swamps, stream banks, marshy shores, ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="floodplain forests" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="marshy shores" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>