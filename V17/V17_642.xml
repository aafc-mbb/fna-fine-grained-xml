<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GRATIOLA</taxon_name>
    <taxon_name authority="Bentham ex A. de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">ebracteata</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 595. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus gratiola;species ebracteata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Bractless hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o26363" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending or erect, simple or few-branched, (5–) 15–22 cm, glabrous or glandular-puberulent distally.</text>
      <biological_entity id="o26364" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="22" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear-lanceolate to lanceolate-ovate, 7–20 (–32) × 1.5–5 (–7) mm, margins entire, rarely with 1 or 2 pairs of teeth distally, apex acuminate to attenuate, surfaces glabrate or glandular-pubescent.</text>
      <biological_entity id="o26365" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26366" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="lanceolate-ovate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="32" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26367" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26368" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <biological_entity id="o26369" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o26370" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <relation from="o26367" id="r2015" modifier="with 1 or 2pairs" name="part_of" negation="false" src="d0_s2" to="o26368" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels stout, (3–) 7–25 mm, length 0.5–2.5 times bract, glabrous or obscurely glandular-pubescent distally;</text>
      <biological_entity id="o26371" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="25" to_unit="mm" />
        <character constraint="bract" constraintid="o26372" is_modifier="false" name="length" src="d0_s3" value="0.5-2.5 times bract" value_original="0.5-2.5 times bract" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely; distally" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o26372" name="bract" name_original="bract" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 0.</text>
      <biological_entity id="o26373" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals distinct, lanceolate, (4–) 7–11 mm;</text>
      <biological_entity id="o26374" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26375" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx slightly bilaterally symmetric;</text>
      <biological_entity id="o26376" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26377" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 5–8 (–10) mm, tube yellowish green or yellow, veins purple, limb white to pinkish white;</text>
      <biological_entity id="o26378" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26379" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26380" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o26381" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o26382" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pinkish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 2–3 mm.</text>
      <biological_entity id="o26383" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26384" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules subglobular, 3–6 × 3.5–5 mm.</text>
      <biological_entity id="o26385" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobular" value_original="subglobular" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.4–0.9 mm.</text>
      <biological_entity id="o26386" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Muddy to sandy stream banks, pond and lake shorelines, shallow water, wet meadows, vernal pools.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy stream" />
        <character name="habitat" value="muddy to sandy stream banks" />
        <character name="habitat" value="lake shorelines" modifier="pond and" />
        <character name="habitat" value="shallow water" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="vernal pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>