<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="A. Heller" date="1894" rank="species">smallii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>21: 25. 1894</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species smallii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>187.</number>
  <other_name type="common_name">Blue Ridge beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o36738" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 35–80 cm, puberulent or retrorsely hairy proximally, retrorsely hairy and sparsely glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o36739" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal sometimes withering by anthesis, not leathery, glabrous or puberulent along midveins and, sometimes, on proximal parts of blade;</text>
      <biological_entity id="o36740" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o36742" name="midvein" name_original="midveins" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o36743" name="part" name_original="parts" src="d0_s2" type="structure" />
      <biological_entity id="o36744" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o36741" id="r2820" modifier="sometimes" name="on" negation="false" src="d0_s2" to="o36743" />
      <relation from="o36743" id="r2821" name="part_of" negation="false" src="d0_s2" to="o36744" />
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 55–170 × 15–60 mm, blade triangular-ovate to cordate or lanceolate, base tapered, margins crenate to sharply serrate, apex obtuse to acute;</text>
      <biological_entity constraint="basal" id="o36741" name="midvein" name_original="midveins" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="sometimes" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="along midveins" constraintid="o36742" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o36745" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o36746" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="triangular-ovate" name="shape" src="d0_s3" to="cordate or lanceolate" />
      </biological_entity>
      <biological_entity id="o36747" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o36748" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="sharply serrate" />
      </biological_entity>
      <biological_entity id="o36749" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 4–6 pairs, sessile, 66–105 × 12–48 mm, blade ovate to lanceolate, proximals sometimes lyrate, base truncate to broadly clasping, margins crenate to sharply serrate, apex acute.</text>
      <biological_entity constraint="cauline" id="o36750" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="66" from_unit="mm" name="length" src="d0_s4" to="105" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="48" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36751" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o36752" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
      </biological_entity>
      <biological_entity id="o36753" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="architecture" src="d0_s4" to="broadly clasping" />
      </biological_entity>
      <biological_entity id="o36754" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="sharply serrate" />
      </biological_entity>
      <biological_entity id="o36755" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, narrowly conic, (6–) 10–28 cm, axis puberulent and glandular-pubescent, verticillasters 4–7, cymes (3–) 5–12-flowered, 2 per node;</text>
      <biological_entity id="o36756" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="28" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o36757" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o36758" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o36759" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(3-)5-12-flowered" value_original="(3-)5-12-flowered" />
        <character constraint="per node" constraintid="o36760" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o36760" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 56–130 × 23–55 mm, margins sharply serrate;</text>
      <biological_entity constraint="proximal" id="o36761" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="56" from_unit="mm" name="length" src="d0_s6" to="130" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="width" src="d0_s6" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36762" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels ascending to erect, puberulent and glandular-pubescent.</text>
      <biological_entity id="o36763" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o36764" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate to lanceolate, 3.5–5 × 1.2–2.1 mm, glandular-pubescent;</text>
      <biological_entity id="o36765" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o36766" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla lavender to violet or purple, with violet nectar guides, funnelform to ventricose-ampliate, 28–35 mm, glandular-pubescent externally, moderately white-lanate internally abaxially, tube 7–8 mm, throat abruptly inflated, 9–16 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o36767" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o36768" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s9" to="violet or purple" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="moderately; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o36769" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o36770" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36771" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s9" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o36768" id="r2822" name="with" negation="false" src="d0_s9" to="o36769" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, 1.3–1.5 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o36772" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o36773" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o36774" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="distance" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o36775" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o36776" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o36777" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 15–18 mm, included, 0.4–0.6 mm diam., tip straight, distal 13–15 mm ± pilose, hairs yellow, to 2 mm;</text>
      <biological_entity id="o36778" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o36779" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36780" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o36781" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o36782" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 17–20 mm.</text>
      <biological_entity id="o36783" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o36784" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 7–10 × 4–5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o36785" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36786" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon smallii is known from the southern Appalachians. Foliose inflorescence bracts, truncate or cordate cauline leaf bases, and lavender to purple corollas distinguish it from other eastern penstemons.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, bluffs, cliffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>