<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="mention_page">154</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Gentianoides</taxon_name>
    <taxon_name authority="Eastwood" date="1893" rank="species">utahensis</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>4: 124. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section gentianoides;species utahensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>93.</number>
  <other_name type="common_name">Utah beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 15–60 cm, glaucous.</text>
      <biological_entity id="o34300" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves glabrous or obscurely scabrous, especially along margins, glaucous;</text>
      <biological_entity id="o34302" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <relation from="o34301" id="r2618" modifier="especially" name="along" negation="false" src="d0_s1" to="o34302" />
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 35–80 (–100) × 5–20 mm, blade oblanceolate, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o34301" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o34303" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o34304" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o34305" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o34306" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o34307" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–4 pairs, sessile, 15–75 × 4–26 mm, blade elliptic to lanceolate, rarely ovate, base tapered, margins entire, apex acute.</text>
      <biological_entity constraint="cauline" id="o34308" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="75" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34309" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o34310" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o34311" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o34312" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses ± interrupted, ± secund, 11–25 cm, axis glabrous, verticillasters 5–15, cymes 1–4-flowered;</text>
      <biological_entity id="o34313" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34314" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34315" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o34316" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, 6–23 × 1–4 mm;</text>
      <biological_entity constraint="proximal" id="o34317" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="23" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels erect, glabrous.</text>
      <biological_entity id="o34318" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34319" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 2.5–4 (–5) × 1.5–4 (–4.6) mm, margins erose, glabrous;</text>
      <biological_entity id="o34320" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o34321" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34322" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla red to crimson, with red nectar guides, bilaterally symmetric, bilabiate, tubular-salverform, 17–22 (–25) mm, glandular-pubescent externally, especially distally, glandular-pubescent internally, tube 6–9 mm, throat slightly inflated, 6–7 mm diam., rounded abaxially;</text>
      <biological_entity id="o34323" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34324" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s8" to="crimson" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular-salverform" value_original="tubular-salverform" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="especially distally; distally; internally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o34325" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o34326" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34327" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o34324" id="r2619" name="with" negation="false" src="d0_s8" to="o34325" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs explanate, 0.6–1.1 mm, sutures smooth;</text>
      <biological_entity id="o34328" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34329" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o34330" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s9" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34331" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 7–10 mm, flattened distally, 0.4–0.5 mm diam., tip recurved, glabrous, rarely distal 1 mm papillate, papillae golden yellow, to 0.1 mm;</text>
      <biological_entity id="o34332" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34333" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34334" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34335" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="rarely" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o34336" name="papilla" name_original="papillae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 10–14 mm.</text>
      <biological_entity id="o34337" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34338" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–10 × 5–7 mm.</text>
      <biological_entity id="o34339" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon utahensis is widespread in the Colorado River Basin from western Colorado through northwestern Arizona, southeastern California, southern Nevada, and southern Utah.</discussion>
  <discussion>The Hopi tribe of northeastern Arizona used Penstemon utahensis as a ceremonial and decorative plant (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>