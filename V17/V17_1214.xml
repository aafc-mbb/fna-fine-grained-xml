<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">675</other_info_on_meta>
    <other_info_on_meta type="mention_page">670</other_info_on_meta>
    <other_info_on_meta type="mention_page">671</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">CORDYLANTHUS</taxon_name>
    <taxon_name authority="Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">ramosus</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 597. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus cordylanthus;species ramosus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Adenostegia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">ciliosa</taxon_name>
    <taxon_hierarchy>genus adenostegia;species ciliosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="(Nuttall ex Bentham) Greene" date="unknown" rank="species">ramosa</taxon_name>
    <taxon_hierarchy>genus a.;species ramosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cordylanthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosus</taxon_name>
    <taxon_name authority="J. F. Macbride" date="unknown" rank="variety">puberulus</taxon_name>
    <taxon_hierarchy>genus cordylanthus;species ramosus;variety puberulus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosus</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="subspecies">setosus</taxon_name>
    <taxon_hierarchy>genus c.;species ramosus;subspecies setosus</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Bushy bird’s-beak</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 10–30 (–90) cm, puberulent, sometimes pilose.</text>
      <biological_entity id="o9215" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent;</text>
      <biological_entity id="o9216" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal 10–40 mm, margins 3–5-lobed, lobes 1–2 mm wide;</text>
      <biological_entity constraint="proximal" id="o9217" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9218" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o9219" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal 10–15 × 0.5–1 mm, margins entire.</text>
      <biological_entity constraint="distal" id="o9220" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9221" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences capitate spikes, 3–7-flowered;</text>
      <biological_entity id="o9222" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="3-7-flowered" value_original="3-7-flowered" />
      </biological_entity>
      <biological_entity id="o9223" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 1–7, 10–20 mm, margins 5–7-lobed, lobes green, sometimes purple distally, filiform.</text>
      <biological_entity id="o9224" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9225" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="5-7-lobed" value_original="5-7-lobed" />
      </biological_entity>
      <biological_entity id="o9226" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes; distally" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: bracteoles 10–20 mm, margins entire.</text>
      <biological_entity id="o9227" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o9228" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9229" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 15–20 mm, tube 0 mm, apex entire or 2-fid, cleft 0–1 mm;</text>
      <biological_entity id="o9230" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9231" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9232" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0" value_original="0" />
      </biological_entity>
      <biological_entity id="o9233" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla pale-yellow, spotted and streaked with purple, 10–20 mm, throat 4–6 mm diam., abaxial lip 3–5 mm, ca. equal to and appressed to adaxial;</text>
      <biological_entity id="o9234" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9235" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="spotted and streaked with purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9236" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9237" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9238" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4, filaments hairy distally, fertile pollen-sacs 2 per filament, unequal.</text>
      <biological_entity id="o9239" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9240" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o9241" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9242" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character constraint="per filament" constraintid="o9243" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o9243" name="filament" name_original="filament" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules oblong-lanceoloid, 8–10 mm.</text>
      <biological_entity id="o9244" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-lanceoloid" value_original="oblong-lanceoloid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 10–20, light-brown, narrowly ovate, 1.5–2 mm, reticulate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o9245" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s11" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9246" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, alkaline soils, sagebrush scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="alkaline soils" />
        <character name="habitat" value="sagebrush scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>