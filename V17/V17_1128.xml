<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">637</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">635</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(A. Gray) Greenman" date="1898" rank="species">pallescens</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>25: 266. 1898</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species pallescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="A. Gray" date="1862" rank="species">pallescens</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 34: 339. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species pallescens</taxon_hierarchy>
  </taxon_identification>
  <number>80.</number>
  <other_name type="common_name">Pale paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (0.4–) 1–3 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o35611" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o35612" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o35611" id="r2727" name="with" negation="false" src="d0_s2" to="o35612" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, erect to ascending, decumbent at base, unbranched, sometimes branched, hairs moderately to very dense, retrorsely curved to appressed, short, ± stiff, eglandular.</text>
      <biological_entity id="o35613" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character constraint="at base" constraintid="o35614" is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o35614" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o35615" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately to very" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="retrorsely" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves purple-tinged or deep purple, sometimes green, linear to narrowly lanceolate, 1–4 (–5) cm, not fleshy, margins plane, sometimes ± wavy, ± involute, (0–) 3–5 (–7) -lobed, apex acute;</text>
      <biological_entity id="o35616" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o35617" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3-5(-7)-lobed" value_original="(0-)3-5(-7)-lobed" />
      </biological_entity>
      <biological_entity id="o35618" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading or ascending-spreading, linear, apex acute.</text>
      <biological_entity id="o35619" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o35620" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (1.5–) 4–8 (–12) × 1.5–5.5 cm;</text>
      <biological_entity id="o35621" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts pale green to yellow-green or reddish purple throughout, or proximally pale green to yellow-green, distally white to cream or pale yellowish, sometimes pink to reddish purple, lanceolate to linear-lanceolate, elliptic, or ovate, 3–5 (–9) -lobed;</text>
      <biological_entity id="o35622" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale green" modifier="throughout" name="coloration" src="d0_s7" to="yellow-green or reddish purple" />
        <character char_type="range_value" from="proximally pale green" name="coloration" src="d0_s7" to="yellow-green distally white" />
        <character char_type="range_value" from="proximally pale green" name="coloration" src="d0_s7" to="yellow-green distally white" />
        <character char_type="range_value" from="pink" modifier="sometimes" name="coloration" src="d0_s7" to="reddish purple" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="linear-lanceolate elliptic or ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="linear-lanceolate elliptic or ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="linear-lanceolate elliptic or ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5(-9)-lobed" value_original="3-5(-9)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading to ascending, linear, long, arising along distal 2/3, apex acute to obtuse.</text>
      <biological_entity id="o35623" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character constraint="along distal 2/3" constraintid="o35624" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="distal" id="o35624" name="2/3" name_original="2/3" src="d0_s8" type="structure" />
      <biological_entity id="o35625" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, sometimes distally purple with age, 11–25 (–27) mm;</text>
      <biological_entity id="o35626" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o35627" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character constraint="with age" constraintid="o35628" is_modifier="false" modifier="sometimes distally" name="coloration_or_density" notes="" src="d0_s9" value="purple" value_original="purple" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="27" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35627" name="bract" name_original="bracts" src="d0_s9" type="structure" />
      <biological_entity id="o35628" name="age" name_original="age" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 7–13.6 mm, 40–50% of calyx length, deeper than laterals, lateral 0.5–4.3 (–6) mm, (0–) 5–25% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o35629" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="13.6" to_unit="mm" />
        <character constraint="than laterals" constraintid="o35630" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="40-50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="4.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35630" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes lanceolate to triangular, apex usually triangular or acute, rarely ± obtuse.</text>
      <biological_entity id="o35631" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="triangular" />
      </biological_entity>
      <biological_entity id="o35632" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely more or less" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, 13–23 (–27) mm;</text>
      <biological_entity id="o35633" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="27" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s12" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 10–20 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>subequal to calyx or beak slightly exserted;</text>
      <biological_entity id="o35634" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
        <character constraint="to beak" constraintid="o35636" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o35635" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <biological_entity id="o35636" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially whitish or buff, rarely pink to pink-purple, 3.5–8 mm;</text>
      <biological_entity id="o35637" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character char_type="range_value" from="buff rarely pink" name="coloration" src="d0_s15" to="pink-purple" />
        <character char_type="range_value" from="buff rarely pink" name="coloration" src="d0_s15" to="pink-purple" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip proximally green, white, purple, or purplish brown, distally white, yellow, green, pink, or reddish, prominent, pouched, pouches pleated, longer than deep, gradually expanded, 2.5–8 mm, 70–100% as long as beak, puberulent;</text>
      <biological_entity constraint="abaxial" id="o35638" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="pouched" value_original="pouched" />
      </biological_entity>
      <biological_entity id="o35639" name="pouch" name_original="pouches" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s16" value="pleated" value_original="pleated" />
        <character is_modifier="false" name="length_or_size" src="d0_s16" value="longer than deep" value_original="longer than deep" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s16" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o35640" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o35639" id="r2728" modifier="70-100%" name="as long as" negation="false" src="d0_s16" to="o35640" />
    </statement>
    <statement id="d0_s17">
      <text>teeth erect to spreading, pink, cream, or white, sometimes with a yellow spot proximally, 1.5–3.5 mm. 2n = 24, 48.</text>
      <biological_entity id="o35641" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s17" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" modifier="with; proximally" name="coloration" src="d0_s17" value="yellow spot" value_original="yellow spot" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35642" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Nev., Oreg., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja pallescens occurs from valleys to alpine ridges and summits throughout its range, usually in sagebrush communities, but at higher elevations it is also found on dry sites associated with other plant species. The alpine plants are greatly reduced in stature.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts not rigid, veins inconspicuous, usually same color as surfaces; herbs (0.5–)1–3 dm; ne Idaho, sw Montana, nw Wyoming.</description>
      <determination>80a. Castilleja pallescens var. pallescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts rigid, veins prominent, pale and contrasting with color of surfaces; herbs 0.4–1.2(–1.7) dm; s, se Idaho, ne Nevada, Oregon.</description>
      <determination>80b. Castilleja pallescens var. inverta</determination>
    </key_statement>
  </key>
</bio:treatment>