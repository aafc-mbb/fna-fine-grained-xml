<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CALLITRICHE</taxon_name>
    <taxon_name authority="Scopoli" date="1772" rank="species">stagnalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carniol. ed.</publication_title>
      <place_in_publication>2, 2: 251. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus callitriche;species stagnalis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Pond water-starwort</other_name>
  <other_name type="common_name">callitriche des eau× stagnantes</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves connate at base, obovate-spatulate to ± round, 4–9 × 1–4 mm, 1+-veined.</text>
      <biological_entity id="o25608" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at base" constraintid="o25609" is_modifier="false" name="fusion" src="d0_s0" value="connate" value_original="connate" />
        <character char_type="range_value" from="obovate-spatulate" name="shape" notes="" src="d0_s0" to="more or less round" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s0" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="1+-veined" value_original="1+-veined" />
      </biological_entity>
      <biological_entity id="o25609" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stem and leaf scales present.</text>
      <biological_entity id="o25610" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o25611" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: bracts persistent.</text>
      <biological_entity id="o25612" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o25613" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 0–0.2 mm in fruit.</text>
      <biological_entity id="o25614" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o25615" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25615" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers solitary;</text>
      <biological_entity id="o25616" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>styles erect;</text>
      <biological_entity id="o25617" name="style" name_original="styles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen yellow.</text>
      <biological_entity id="o25618" name="pollen" name_original="pollen" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Schizocarps 1.2–1.8 × 1–1.7 mm, ± as long as wide;</text>
      <biological_entity id="o25619" name="schizocarp" name_original="schizocarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" modifier="more or less" name="length" src="d0_s7" to="1.8" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as wide" from="1" from_unit="mm" modifier="more or less" name="width" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mericarps pale grayish brown, not swollen, winged throughout, wings straight, 0.1–0.5 mm wide.</text>
      <biological_entity id="o25620" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale grayish" value_original="pale grayish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 10.</text>
      <biological_entity id="o25621" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25622" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Philbrick, C. T., R. A. Aakjar Jr., and R. L. Stuckey. 1998. Invasion and spread of Callitriche stagnalis (Callitrichaceae) in North America. Rhodora 100: 25–38.</reference>
  </references>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Standing water, stream margins, backwaters, ponds, pools, ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="water" modifier="standing" />
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="backwaters" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; B.C., Nfld. and Labr. (Nfld.), Que.; Ala., Calif., Conn., Maine, Md., Mass., Mo., Mont., N.J., N.Y., Oreg., Pa., Va., Wash., Wis.; Europe; introduced also in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>