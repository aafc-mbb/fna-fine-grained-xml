<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="Penland" date="1958" rank="species">yampaënsis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>14: 156, fig. 2. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species yampaënsis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="L. O. Williams" date="unknown" rank="species">acaulis</taxon_name>
    <taxon_name authority="(Penland) Neese" date="unknown" rank="variety">yampaënsis</taxon_name>
    <taxon_hierarchy>genus penstemon;species acaulis;variety yampaënsis</taxon_hierarchy>
  </taxon_identification>
  <number>79.</number>
  <other_name type="common_name">Yampa beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate to ascending, to 1 cm, scabrous or puberulent.</text>
      <biological_entity id="o39731" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="ascending" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves essentially basal, not leathery, scabrous;</text>
      <biological_entity id="o39733" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="essentially" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline sessile, 15–25 (–35) × 1.5–2.6 (–4.5) mm, blade oblanceolate to linear, base tapered, margins entire or obscurely dentate distally, apex obtuse to acute.</text>
      <biological_entity id="o39732" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="texture" notes="" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o39734" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o39735" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
      <biological_entity id="o39736" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o39737" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely; distally" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o39738" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses essentially absent, 0.1–0.6 cm, axis puberulent, verticillasters 1 or 2, cymes 1 (–3) -flowered, 1 (or 2) per node;</text>
      <biological_entity id="o39739" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s3" to="0.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39740" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o39741" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" unit="or" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o39742" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1(-3)-flowered" value_original="1(-3)-flowered" />
        <character constraint="per node" constraintid="o39743" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o39743" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts oblanceolate to linear, 8–20 × 1–3 mm;</text>
      <biological_entity constraint="proximal" id="o39744" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels glandular-pubescent and scabrous.</text>
      <biological_entity id="o39745" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o39746" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate, 5–6.5 × 1–1.8 mm, glandular-pubescent and scabrous;</text>
      <biological_entity id="o39747" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o39748" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lavender to bluish lavender, without nectar guides, funnelform, 14–18 mm, yellowish or whitish villous internally abaxially, tube 5–6 mm, throat gradually inflated, not constricted at orifice, 4.5–6 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o39749" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o39750" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender to bluish lavender" value_original="lavender to bluish lavender" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="18" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o39751" name="guide" name_original="guides" src="d0_s7" type="structure" />
      <biological_entity id="o39752" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39753" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o39754" is_modifier="false" modifier="not" name="size" src="d0_s7" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="diameter" notes="" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s7" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o39754" name="orifice" name_original="orifice" src="d0_s7" type="structure" />
      <relation from="o39750" id="r3061" name="without" negation="false" src="d0_s7" to="o39751" />
    </statement>
    <statement id="d0_s8">
      <text>stamens: longer pair reaching orifice, pollen-sacs widely divergent to opposite, navicular, 0.8–1.3 mm, dehiscing completely, sutures papillate;</text>
      <biological_entity id="o39755" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o39756" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <biological_entity id="o39757" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character char_type="range_value" from="widely divergent" name="arrangement" src="d0_s8" to="opposite" />
        <character is_modifier="false" name="shape" src="d0_s8" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s8" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s8" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o39758" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o39755" id="r3062" name="reaching" negation="false" src="d0_s8" to="o39756" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 8–11 mm, prominently exserted, 0.5–0.8 mm diam., tip straight to recurved, distal 5–7 mm densely pubescent, hairs orange, to 1 mm;</text>
      <biological_entity id="o39759" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o39760" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39761" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39762" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 9–12 mm.</text>
      <biological_entity id="o39763" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o39764" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 3.5–5 × 3–4 mm.</text>
      <biological_entity id="o39765" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon yampaënsis is known from Moffat County, Colorado, Daggett and Uintah counties, Utah, and Sweetwater County, Wyoming. The species appears to be closely related to P. acaulis. Seed release appears to occur as the walls of the capsules deteriorate, rather than through an apical opening as in other Penstemon species. This type of dehiscence also occurs in P. acaulis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Semi-barren ledges and ridges, hilltops, pinyon-juniper woodlands, sagebrush shrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="semi-barren ledges" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="hilltops" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="shrublands" modifier="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>