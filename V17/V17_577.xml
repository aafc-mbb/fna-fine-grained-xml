<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="(A. Gray) Krautter" date="1908" rank="species">parvulus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Bot. Lab. Morris Arbor. Univ. Pennsylvania</publication_title>
      <place_in_publication>3: 193. 1908</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species parvulus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">azureus</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="variety">parvulus</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 272. 1878</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species azureus;variety parvulus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">azureus</taxon_name>
    <taxon_name authority="(A. Gray) D. D. Keck" date="unknown" rank="subspecies">parvulus</taxon_name>
    <taxon_hierarchy>genus p.;species azureus;subspecies parvulus</taxon_hierarchy>
  </taxon_identification>
  <number>215.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs.</text>
      <biological_entity id="o25488" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (10–) 15–40 cm, glabrous, glaucous.</text>
      <biological_entity id="o25490" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite, glabrous, sometimes proximals retrorsely hairy, hairs pointed, usually glaucous;</text>
      <biological_entity id="o25491" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25492" name="proximal" name_original="proximals" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25493" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–8 pairs, short-petiolate or sessile, (6–) 10–50 × (2–) 4–9 mm, blade oblanceolate to oblong or lanceolate, base tapered to truncate or cordate, margins entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o25494" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s3" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25495" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblong or lanceolate" />
      </biological_entity>
      <biological_entity id="o25496" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s3" to="truncate or cordate" />
      </biological_entity>
      <biological_entity id="o25497" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25498" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous, cylindric, 4–11 cm, axis glabrous, verticillasters 4 or 5, cymes 2–4-flowered, 2 per node;</text>
      <biological_entity id="o25499" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25500" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25501" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s4" unit="or" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o25502" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
        <character constraint="per node" constraintid="o25503" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25503" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to oblong or lanceolate, 4–34 × 2–8 mm;</text>
      <biological_entity constraint="proximal" id="o25504" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="oblong or lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="34" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glabrous.</text>
      <biological_entity id="o25505" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25506" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes obovate to ovate or lanceolate, 3–5 × 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o25507" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o25508" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="ovate or lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue to violet or purple, without nectar guides, ventricose, 14–20 mm, glabrous externally, glabrous internally, tube 4–5 mm, throat gradually inflated, 5–7 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o25509" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25510" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="violet or purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o25511" name="guide" name_original="guides" src="d0_s8" type="structure" />
      <biological_entity id="o25512" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25513" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o25510" id="r1933" name="without" negation="false" src="d0_s8" to="o25511" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, filaments glabrous, pollen-sacs parallel, 1.4–1.8 mm, distal 1/3–1/2 indehiscent, sides hispidulous, hairs white, to 0.3 mm near filament attachment, sometimes sparsely so, sutures denticulate, teeth to 0.2 mm;</text>
      <biological_entity id="o25514" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25515" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o25516" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25517" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="distance" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s9" to="1/2" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o25518" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o25519" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" constraint="near filament attachment" constraintid="o25520" from="0" from_unit="mm" name="location" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="filament" id="o25520" name="attachment" name_original="attachment" src="d0_s9" type="structure" />
      <biological_entity id="o25521" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely; sparsely" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o25522" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 10–12 mm, included, 0.4–0.6 mm diam., glabrous or distal 0.5 mm sparsely pubescent, hairs whitish, to 0.2 mm;</text>
      <biological_entity id="o25523" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25524" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s10" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25525" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25526" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 11–14 mm.</text>
      <biological_entity id="o25527" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25528" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 5–8 × 3.5–5 mm. 2n = 32.</text>
      <biological_entity id="o25529" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25530" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon parvulus occurs in the Klamath Ranges of northern California and southwestern Oregon and in the southern High Sierra Nevada of south-central California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, open foothills and montane forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="open foothills" />
        <character name="habitat" value="montane forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2500(–3000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>