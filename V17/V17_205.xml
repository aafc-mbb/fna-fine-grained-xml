<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">bartsiifolia</taxon_name>
    <taxon_name authority="(Kellogg) Pennell in L. Abrams and R. S. Ferris" date="1951" rank="variety">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Ill. Fl. Pacific States</publication_title>
      <place_in_publication>3: 776. 1951</place_in_publication>
      <other_info_on_pub>(as bartsiaefolia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species bartsiifolia;variety hirsuta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Collinsia</taxon_name>
    <taxon_name authority="Kellogg" date="1863" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 110, fig. 34. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus collinsia;species hirsuta</taxon_hierarchy>
  </taxon_identification>
  <number>18d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–20 cm.</text>
      <biological_entity id="o20616" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences eglandular.</text>
      <biological_entity id="o20617" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: corolla usually white to pale lavender, 13–18 mm, eglandular;</text>
      <biological_entity id="o20618" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o20619" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually white" name="coloration" src="d0_s2" to="pale lavender" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="18" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>banner length 0.5–0.6 (–0.7) times wings.</text>
      <biological_entity id="o20620" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o20622" name="wing" name_original="wings" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 14.</text>
      <biological_entity id="o20621" name="banner" name_original="banner" src="d0_s3" type="structure">
        <character constraint="wing" constraintid="o20622" is_modifier="false" name="length" src="d0_s3" value="0.5-0.6(-0.7) times wings" value_original="0.5-0.6(-0.7) times wings" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20623" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety hirsuta is presumed extinct on the San Francisco Peninsula. It is known from a single collection at Point Reyes (Marin County) in 1906.</discussion>
  <discussion>The type collection of Collinsia hirsuta is unknown and presumed to have been lost in 1906. The depiction of corollas is inconsistent in the illustration accompanying the original description. In the detail of a single corolla, slight reduction of the banner is depicted, but in the habit, there is near equivalence in length between the banner and wings. The original publication states that the description was based on plants collected within the vicinity of the meeting (at the California Academy of Sciences in San Francisco). Most known collections are from western San Francisco; these conform to the variety as described here.</discussion>
  <discussion>Some treatments have classified plants of var. hirsuta (as treated here) as Collinsia corymbosa (V. M. Newsom 1929, cited as transitional to C. bartsiifolia var. bartsiifolia by E. C. Neese 1993b). The reduction of the corolla banner in var. hirsuta is not extreme like that of C. corymbosa. Plants of the former have vegetative characteristics and the inflorescence structure of C. bartsiifolia var. bartsiifolia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes, sandy sites, rarely rocky slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="sandy sites" />
        <character name="habitat" value="rocky slopes" modifier="rarely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>