<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <taxon_name authority="(Bentham e× A. de Candolle) Straw" date="1967" rank="species">antirrhinoides</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 203. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella;species antirrhinoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Bentham e× A. de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">antirrhinoides</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 594. 1846</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species antirrhinoides</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Snapdragon penstemon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems spreading to erect, 6–25 dm, canescent, rarely glabrous when young.</text>
      <biological_entity id="o36530" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite, on short-shoots as axillary clusters on older stems;</text>
      <biological_entity id="o36531" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o36532" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="axillary" id="o36533" name="cluster" name_original="clusters" src="d0_s1" type="structure" />
      <biological_entity id="o36534" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="older" value_original="older" />
      </biological_entity>
      <relation from="o36531" id="r2795" name="on" negation="false" src="d0_s1" to="o36532" />
      <relation from="o36532" id="r2796" name="as" negation="false" src="d0_s1" to="o36533" />
      <relation from="o36533" id="r2797" name="on" negation="false" src="d0_s1" to="o36534" />
    </statement>
    <statement id="d0_s2">
      <text>blade oblanceolate or lanceolate to narrowly obovate or ovate, 5–20 mm, margins entire or 2-toothed, sometimes relatively small.</text>
      <biological_entity id="o36535" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="narrowly obovate or ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36536" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-toothed" value_original="2-toothed" />
        <character is_modifier="false" modifier="sometimes relatively" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences panicles, finely short-hairy and sparsely glandular.</text>
      <biological_entity constraint="inflorescences" id="o36537" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s3" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: caly× 3–9 mm, lobes lanceolate to ovate;</text>
      <biological_entity id="o36538" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="area" src="d0_s4" unit="mm" value="×3-9" value_original="×3-9" />
      </biological_entity>
      <biological_entity id="o36539" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla yellow drying blackish, 15–23 mm, tube plus distinct throat 6–10 mm, shorter than adaxial lip, adaxial lip 8–15 mm;</text>
      <biological_entity id="o36540" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o36541" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="condition" src="d0_s5" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36542" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o36543" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character constraint="than adaxial lip" constraintid="o36544" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o36544" name="lip" name_original="lip" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o36545" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs 1.1–1.8 mm;</text>
      <biological_entity id="o36546" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o36547" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s6" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode densely yellow-hairy, exserted.</text>
      <biological_entity id="o36548" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o36549" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="yellow-hairy" value_original="yellow-hairy" />
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces 3–6 mm, lobes widely ovate, apices obtuse to acute; plants finely hairy; pollen sacs 1.4–1.8 mm.</description>
      <determination>2a. Keckiella antirrhinoides var. antirrhinoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces 5.5–9 mm, lobes lanceolate, apices acute to acuminate; plants canescent; pollen sacs 1.1–1.5 mm.</description>
      <determination>2b. Keckiella antirrhinoides var. microphylla</determination>
    </key_statement>
  </key>
</bio:treatment>