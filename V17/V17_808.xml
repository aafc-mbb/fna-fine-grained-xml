<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="(Rydberg) Ferris" date="1958" rank="species">corymbosa</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>5: 99. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species corymbosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myzorrhiza</taxon_name>
    <taxon_name authority="Rydberg" date="1909" rank="species">corymbosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>36: 696. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus myzorrhiza;species corymbosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphyllon</taxon_name>
    <taxon_name authority="(Rydberg) A. C. Schneider" date="unknown" rank="species">corymbosum</taxon_name>
    <taxon_hierarchy>genus aphyllon;species corymbosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orobanche</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Rydberg) Munz" date="unknown" rank="variety">corymbosa</taxon_name>
    <taxon_hierarchy>genus orobanche;species californica;variety corymbosa</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Flat-top broomrape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple, rarely branched, 5–12 (–18) cm, usually slender, base sometimes enlarged.</text>
      <biological_entity id="o551" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o552" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots usually inconspicuous (sometimes forming an irregular mass), slender, unbranched or with short bifurcations.</text>
      <biological_entity id="o553" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="with short bifurcations" value_original="with short bifurcations" />
      </biological_entity>
      <biological_entity id="o554" name="bifurcation" name_original="bifurcations" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o553" id="r40" name="with" negation="false" src="d0_s1" to="o554" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves few to several, appressed;</text>
      <biological_entity id="o555" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly lanceolate or ovate, 5–10 mm, margins entire or slightly erose, apex acute or obtuse, surfaces often glandular-pubescent.</text>
      <biological_entity id="o556" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o557" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s3" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o558" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o559" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemelike to elongate corymbs or short racemes (sometimes corymbose), rose, pink, purple, or white, simple, densely glandular-pubescent;</text>
      <biological_entity id="o560" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="racemelike" name="shape" src="d0_s4" to="elongate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o561" name="corymb" name_original="corymbs" src="d0_s4" type="structure" />
      <biological_entity id="o562" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers numerous (rarely 10 or fewer in depauperate plants);</text>
      <biological_entity id="o563" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts slightly reflexed, narrowly lanceolate or almost linear, 7–11 mm, apex acute, glandular-pubescent.</text>
      <biological_entity id="o564" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="almost; almost" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o565" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 3–20 mm proximally, 0 mm distally, shorter than plant axis;</text>
      <biological_entity id="o566" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" modifier="proximally" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
        <character modifier="distally" name="some_measurement" src="d0_s7" unit="mm" value="0" value_original="0" />
        <character constraint="than plant axis" constraintid="o567" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="plant" id="o567" name="axis" name_original="axis" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 2.</text>
      <biological_entity id="o568" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx lavender, pink, white, or yellow, sometimes burgundy, ± radially symmetric, 12–24 mm, deeply divided into 5 subequal (reflexed or contorted) lobes, lobes linear-subulate, densely glandular-pubescent;</text>
      <biological_entity id="o569" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o570" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="burgundy" value_original="burgundy" />
        <character is_modifier="false" modifier="more or less radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="24" to_unit="mm" />
        <character constraint="into lobes" constraintid="o571" is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o571" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o572" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla (15–) 18–34 mm, tube white to grayish white, pale-pink to pink, or pale-purple to purple, rarely brick-red, sometimes with darker-pink to purple veins, slightly constricted above ovary, bent forward, glandular-pubescent or glabrate;</text>
      <biological_entity id="o573" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o574" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s10" to="34" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o575" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="grayish white pale-pink" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="grayish white pale-pink" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="grayish white pale-pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="brick-red" value_original="brick-red" />
        <character constraint="above ovary" constraintid="o577" is_modifier="false" modifier="slightly" name="size" notes="" src="d0_s10" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="forward" name="shape" notes="" src="d0_s10" value="bent" value_original="bent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o576" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="darker-pink" is_modifier="true" name="coloration" src="d0_s10" to="purple" />
      </biological_entity>
      <biological_entity id="o577" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <relation from="o575" id="r41" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o576" />
    </statement>
    <statement id="d0_s11">
      <text>palatal folds prominent, yellow, glabrous (with blisterlike swellings);</text>
      <biological_entity id="o578" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o579" name="fold" name_original="folds" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lips white to ± pink, pale-purple to purple, brick-red, or pinkish red, sometimes with darker-pink to purple veins or internally darker, abaxial lip spreading, 5–9 mm, lobes oblong to oblong-lanceolate, apex acute or rounded, adaxial lip erect or reflexed, 5–9 mm, lobes oblong, apex rounded, truncate, or emarginate, rarely acute;</text>
      <biological_entity id="o580" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o581" name="lip" name_original="lips" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="more or less pink pale-purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="more or less pink pale-purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="more or less pink pale-purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="more or less pink pale-purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o582" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character char_type="range_value" from="darker-pink" is_modifier="true" name="coloration" src="d0_s12" to="purple" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o583" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="internally" name="coloration" src="d0_s12" value="darker" value_original="darker" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o584" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o585" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o586" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o587" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o588" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o581" id="r42" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o582" />
      <relation from="o581" id="r43" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o583" />
    </statement>
    <statement id="d0_s13">
      <text>filaments glabrous, anthers included, tomentose, sometimes glabrous (subsp. mutabilis).</text>
      <biological_entity id="o589" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o590" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o591" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid to oblong-ovoid, 5–13 mm.</text>
      <biological_entity id="o592" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="oblong-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.3–0.5 mm.</text>
      <biological_entity id="o593" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man.; Ariz., Calif., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The range of Orobanche corymbosa includes the Great Basin Desert, Intermountain Region, Columbia Plateau, and contiguous areas. The two subspecies are sympatric through most of the geographic range of the species. Areas of sympatry exist with several other species of Orobanche: in the west with O. californica; in the north and east with O. ludoviciana; and in the south with O. arizonica and O. multiflora. Subspecies mutabilis tends to produce more racemose inflorescences, while subsp. corymbosa typically produces more compact corymbose inflorescences. This suggests a genetic affinity between O. corymbosa and members of the O. californica and O. ludoviciana groups. The southern members of subsp. corymbosa in California and Nevada appear to intergrade with O. californica across the Sierra Nevada. However, there are exceptional individuals in every population, so seasonal weather may cause variations in plant morphology.</discussion>
  <discussion>Inflorescence differences could also be associated with the ploidy differences reported in Orobanche corymbosa (L. R. Heckard and T. I. Chuang 1975). Ploidy and morphological instability may indicate significant introgression or a hybrid origin of O. corymbosa.</discussion>
  <discussion>Both subspecies share a somewhat ampliate corolla tube, which is a good field character for this species when compared with species with which it has an overlapping range. Orobanche corymbosa is parasitic on species of Artemisia (Asteraceae), principally A. tridentata, but has occasionally been reported on Iva (Asteraceae) and Atriplex and Sarcobatus (Chenopodiaceae).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences simple corymbs or short corymbose racemes; calyces (13–)15–24 mm, equal to or shorter than corollas; corollas glandular-pubescent; anthers tomentose.</description>
      <determination>8a. Orobanche corymbosa subsp. corymbosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences short racemes or racemelike to elongate corymbs; calyces 12–18 mm, sometimes equal to or longer than corollas; corollas glabrate or slightly glandular-pubescent; anthers tomentose or glabrous.</description>
      <determination>8b. Orobanche corymbosa subsp. mutabilis</determination>
    </key_statement>
  </key>
</bio:treatment>