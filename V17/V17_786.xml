<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Bruce A. Sorrie</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">SCHWALBEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 606. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 265. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus schwalbea</taxon_hierarchy>
    <other_info_on_name type="etymology">For Christian Georg Schwalbe, eighteenth-century medical botanist</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Chaffseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>hemiparasitic, caudex knotty, semiwoody.</text>
      <biological_entity id="o21673" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o21674" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" name="texture" src="d0_s1" value="semiwoody" value_original="semiwoody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, not fleshy, densely pubescent.</text>
      <biological_entity id="o21675" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate;</text>
      <biological_entity id="o21676" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o21677" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire.</text>
      <biological_entity id="o21678" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o21679" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes;</text>
      <biological_entity id="o21680" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21681" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o21682" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present;</text>
      <biological_entity id="o21683" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles present.</text>
      <biological_entity id="o21684" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5, calyx bilaterally symmetric, tubular, abaxial lobes long-triangular, larger and with 2 lateral lobes and larger central lobe 3-notched at summit, adaxial short-triangular;</text>
      <biological_entity id="o21685" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21686" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21687" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21688" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="long-triangular" value_original="long-triangular" />
        <character is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21689" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="larger central" id="o21690" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character constraint="at summit" constraintid="o21691" is_modifier="false" name="shape" src="d0_s10" value="3-notched" value_original="3-notched" />
      </biological_entity>
      <biological_entity id="o21691" name="summit" name_original="summit" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o21692" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
      <relation from="o21688" id="r1654" name="with" negation="false" src="d0_s10" to="o21689" />
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla pale-yellow, strongly suffused with purple distally, strongly bilabiate, tubular, abaxial lobes 3, adaxial 2, adaxial lip ± galeate;</text>
      <biological_entity id="o21693" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21694" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21695" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="strongly; distally" name="coloration" src="d0_s11" value="suffused with purple" value_original="suffused with purple" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21696" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21697" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21698" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="galeate" value_original="galeate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, equal, filaments pilose;</text>
      <biological_entity id="o21699" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21700" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o21701" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o21702" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21703" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o21704" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o21705" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate.</text>
      <biological_entity id="o21706" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o21707" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence septicidal and loculicidal.</text>
      <biological_entity id="o21708" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ca. 300, brown or amber, linear, flattened, ± twisted, wings present.</text>
      <biological_entity id="o21709" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="300" value_original="300" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o21710" name="amber" name_original="amber" src="d0_s17" type="substance" />
    </statement>
    <statement id="d0_s18">
      <text>x = 18.</text>
      <biological_entity id="o21711" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o21712" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Kirkman, L. K., M. B. Drew, and D. Edwards. 1998. Effects of experimental fire regimes on the population dynamics of Schwalbea americana L. Pl. Ecol. 137: 115–137.</reference>
  </references>
  
</bio:treatment>