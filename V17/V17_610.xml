<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">palmeri</taxon_name>
    <taxon_name authority="(D. D. Keck) N. H. Holmgren" date="1979" rank="variety">eglandulosus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>31: 105. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species palmeri;variety eglandulosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palmeri</taxon_name>
    <taxon_name authority="D. D. Keck" date="1937" rank="subspecies">eglandulosus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>18: 797. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species palmeri;subspecies eglandulosus</taxon_hierarchy>
  </taxon_identification>
  <number>235b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal cauline leaves connate-perfoliate.</text>
      <biological_entity constraint="distal cauline" id="o4648" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="connate-perfoliate" value_original="connate-perfoliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Thyrses: peduncles and pedicels glabrous.</text>
      <biological_entity id="o4649" name="thyrse" name_original="thyrses" src="d0_s1" type="structure" />
      <biological_entity id="o4650" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4651" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: calyx lobes glabrous;</text>
      <biological_entity id="o4652" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="calyx" id="o4653" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla-tube 4–6 mm;</text>
      <biological_entity id="o4654" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o4655" name="corolla-tube" name_original="corolla-tube" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pollen-sacs 1.6–2 (–2.2) mm.</text>
      <biological_entity id="o4656" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4657" name="pollen-sac" name_original="pollen-sacs" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s4" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="distance" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety eglandulosus occurs from the southern end of the Utah Plateau (Emery, Garfield, Grand, Iron, Kane, San Juan, Washington, and Wayne counties, Utah) to the Kaibab Plateau in northern Coconino and Mohave counties, Arizona. A population in Socorro County, New Mexico, appears to have been seeded along a roadside.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Washes, roadsides, desert shrublands, pinyon-juniper woodlands, pine woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="washes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="desert shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="woodlands" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>