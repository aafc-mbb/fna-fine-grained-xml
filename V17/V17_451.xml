<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Glabri</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="species">tidestromii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>20: 379. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section glabri;species tidestromii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="species">leptanthus</taxon_name>
    <taxon_hierarchy>genus penstemon;species leptanthus</taxon_hierarchy>
  </taxon_identification>
  <number>134.</number>
  <other_name type="common_name">Tidestrom’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 25–50 cm, retrorsely hairy, not glaucous.</text>
      <biological_entity id="o6617" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, ± leathery or not, retrorsely hairy, not glaucous;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 40–120 × 4–33 mm, blade obovate to oblanceolate, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o6618" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character name="texture" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o6619" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o6620" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o6621" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o6622" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6623" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–5 pairs, short-petiolate or sessile, 35–70 (–90) × 8–22 mm, blade lanceolate to oblanceolate, base tapered to truncate or clasping, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o6624" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6625" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o6626" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o6627" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, secund, 9–22 cm, axis glabrous or sparsely retrorsely hairy, verticillasters 7–15, cymes 5-flowered or 6-flowered, 2 per node;</text>
      <biological_entity id="o6628" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s4" to="22" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6629" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6630" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o6631" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-flowered" value_original="5-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="6-flowered" value_original="6-flowered" />
        <character constraint="per node" constraintid="o6632" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6632" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, 12–50 × 3–16 mm;</text>
      <biological_entity constraint="proximal" id="o6633" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glabrous or sparsely retrorsely hairy.</text>
      <biological_entity id="o6634" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6635" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 2.5–4 × 1.5–1.7 (–2) mm, margins narrowly scarious, apex obtuse, sometimes acute to cuspidate, glabrous;</text>
      <biological_entity id="o6636" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o6637" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6638" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o6639" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="acute" modifier="sometimes" name="shape" src="d0_s7" to="cuspidate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue to violet, without nectar guides, ventricose, 13–21 mm, glabrous externally, glabrous internally, tube 5–6 mm, throat gradually inflated, not constricted at orifice, 5–7 mm diam., rounded abaxially;</text>
      <biological_entity id="o6640" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6641" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o6642" name="guide" name_original="guides" src="d0_s8" type="structure" />
      <biological_entity id="o6643" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6644" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o6645" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o6645" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o6641" id="r567" name="without" negation="false" src="d0_s8" to="o6642" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included or longer pair reaching orifice, pollen-sacs divergent, navicular, 1.3–1.7 mm, dehiscing incompletely, proximal 1/3–1/2 sometimes indehiscent, connective not splitting, sides glabrous, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o6646" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6647" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6648" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o6649" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="distance" src="d0_s9" to="1.7" to_unit="mm" />
        <character is_modifier="false" modifier="incompletely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s9" to="1/2" />
        <character is_modifier="false" modifier="sometimes" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o6650" name="connective" name_original="connective" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s9" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o6651" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6652" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o6653" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o6647" id="r568" name="reaching" negation="false" src="d0_s9" to="o6648" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 8–11 mm, included, 0.6–0.8 mm diam., tip straight, distal 5–8 mm sparsely to moderately pilose, hairs yellow, to 1 mm;</text>
      <biological_entity id="o6654" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6655" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6656" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6657" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o6658" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 11–13 mm.</text>
      <biological_entity id="o6659" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6660" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–9 × 4.5–6.5 mm.</text>
      <biological_entity id="o6661" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon tidestromii is known from Juab and Sanpete counties. Specimens from the San Pitch Mountains with retrorsely hairy stems, corollas obscurely glandular, and pollen sacs 1.5–1.7 mm and sparsely hispid might be hybrids or introgressants between P. subglaber and P. tidestromii. Penstemon leptanthus, known only from the type collection (Ward 280, US), seems to be morphologically confluent with P. tidestromii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>