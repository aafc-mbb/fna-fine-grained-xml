<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">278</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in J. Lindley" date="1836" rank="genus">SOPHRONANTHE</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Syst. Bot.,</publication_title>
      <place_in_publication>445. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus sophronanthe</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek sphoron, modest, and anthos, flower, alluding to small flowers</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small &amp; Pennell" date="unknown" rank="genus">Tragiola</taxon_name>
    <taxon_hierarchy>genus tragiola</taxon_hierarchy>
  </taxon_identification>
  <number>37.</number>
  <other_name type="common_name">Hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
      <biological_entity id="o20149" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex thick, hard.</text>
      <biological_entity id="o20150" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, villous (hairs jointed), rarely glabrous.</text>
      <biological_entity id="o20151" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, sometimes also basal, opposite;</text>
      <biological_entity id="o20152" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" notes="" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20153" name="stem" name_original="stems" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole absent, sometimes present on basal leaves;</text>
      <biological_entity id="o20154" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="on basal leaves" constraintid="o20155" is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20155" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, leathery, margins entire or serrate.</text>
      <biological_entity id="o20156" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o20157" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, flowers 1 or 2 per node;</text>
      <biological_entity id="o20158" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o20159" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20160" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o20161" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present or absent;</text>
      <biological_entity id="o20162" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles present, 2.</text>
      <biological_entity id="o20163" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual;</text>
      <biological_entity id="o20164" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, distinct, linear to linear-lanceolate, calyx ± bilaterally symmetric, campanulate;</text>
      <biological_entity id="o20165" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o20166" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less bilaterally" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla white, sometimes tinged purple or pink, bilaterally symmetric, bilabiate to bilabiate and personate, tubular to salverform, tube base not spurred or gibbous, lobes 5, abaxial 3, adaxial 2, throat hairy;</text>
      <biological_entity id="o20167" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="tinged purple" value_original="tinged purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="bilaterally" name="architecture" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="bilabiate" name="architecture" src="d0_s12" to="bilabiate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="personate" value_original="personate" />
        <character char_type="range_value" from="tubular" name="shape" src="d0_s12" to="salverform" />
      </biological_entity>
      <biological_entity constraint="tube" id="o20168" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="shape" src="d0_s12" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o20169" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20170" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20171" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20172" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2, medially adnate to corolla, filaments glabrous, pollen-sacs parallel to filaments, connective not dilated;</text>
      <biological_entity id="o20173" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character constraint="to corolla" constraintid="o20174" is_modifier="false" modifier="medially" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o20174" name="corolla" name_original="corolla" src="d0_s13" type="structure" />
      <biological_entity id="o20175" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20176" name="pollen-sac" name_original="pollen-sacs" src="d0_s13" type="structure">
        <character constraint="to filaments" constraintid="o20177" is_modifier="false" name="arrangement" src="d0_s13" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o20177" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o20178" name="connective" name_original="connective" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>staminodes 0 or 2, minute;</text>
      <biological_entity id="o20179" name="staminode" name_original="staminodes" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s14" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s14" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o20180" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma crateriform.</text>
      <biological_entity id="o20181" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="crateriform" value_original="crateriform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, dehiscence septicidal, sometimes also loculicidal.</text>
      <biological_entity constraint="fruits" id="o20182" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" modifier="sometimes" name="dehiscence" src="d0_s17" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ca. 200, yellow to brown or black, ovoid to short-cylindric, wings absent.</text>
      <biological_entity id="o20183" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="200" value_original="200" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s18" to="brown or black" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="short-cylindric" />
      </biological_entity>
      <biological_entity id="o20184" name="wing" name_original="wings" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The two species of Sophronanthe often have been included in Gratiola or in the monospecific genera Sophronanthe (S. hispida) and Tragiola (S. pilosa). Molecular and morphological data support the monophyly of Sophronanthe (D. Estes 2008), which is sister to Gratiola in the narrow sense.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 0.7–3 mm wide, linear to lanceolate-ovate, margins prominently revolute; corollas salverform, 7–16 mm.</description>
      <determination>1. Sophronanthe hispida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 4–11 mm wide, lanceolate to ovate, margins not or slightly revolute; corollas tubular, 5–9 mm.</description>
      <determination>2. Sophronanthe pilosa</determination>
    </key_statement>
  </key>
</bio:treatment>