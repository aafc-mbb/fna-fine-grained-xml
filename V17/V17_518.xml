<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Douglas ex Graham" date="1829" rank="species">procerus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">procerus</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species procerus;variety procerus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>181a.</number>
  <other_name type="common_name">Pincushion beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (6–) 15–55 (–70) cm.</text>
      <biological_entity id="o30236" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal and proximal cauline 23–90 (–115) × 4–19 mm;</text>
      <biological_entity id="o30237" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="115" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s1" to="90" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 13–85 × 4–17 mm, blade lanceolate to oblanceolate, sometimes elliptic or oblong.</text>
      <biological_entity id="o30238" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o30239" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s2" to="85" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30240" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="oblanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses (1–) 5–23 cm, verticillasters (1–) 3–8 (–11).</text>
      <biological_entity id="o30241" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="23" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30242" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="11" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx lobes (2.5–) 3–5 × 0.6–2 mm, apex acuminate or short to long-caudate;</text>
      <biological_entity id="o30243" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="calyx" id="o30244" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30245" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-caudate" value_original="long-caudate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla (7–) 8–11 mm;</text>
      <biological_entity id="o30246" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o30247" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs (0.3–) 0.4–0.6 mm;</text>
      <biological_entity id="o30248" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o30249" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_distance" src="d0_s6" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="distance" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode: distal 0.5–1 mm densely pilose, hairs yellow or golden yellow, to 0.5 mm. 2n = 16, 32.</text>
      <biological_entity id="o30250" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o30251" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30252" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
        <character name="quantity" src="d0_s7" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety procerus is widespread in the Rocky Mountains from southern Colorado to the Yukon. The caudate apices of the calyx lobes sometimes are as long as the bodies of the lobes, especially in more northern populations.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, grassy slopes, aspen woodlands, open forest slopes, subalpine and alpine meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="grassy slopes" />
        <character name="habitat" value="aspen woodlands" />
        <character name="habitat" value="open forest slopes" />
        <character name="habitat" value="alpine meadows" modifier="subalpine and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(300–)600–3700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask., Yukon; Alaska, Colo., Idaho, Mont., N.Dak., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>