<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Gentianoides</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in A. Gray et al." date="1878" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 264. 1878</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section gentianoides;species parryi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">puniceus</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="variety">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 113. 1859</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species puniceus;variety parryi</taxon_hierarchy>
  </taxon_identification>
  <number>89.</number>
  <other_name type="common_name">Parry’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 30–120 (–160) cm, glaucous.</text>
      <biological_entity id="o19846" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves glabrous, glaucous;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 36–125 × 8–25 mm, blade spatulate to oblanceolate or elliptic, base tapered, margins entire, rarely remotely and obscurely dentate, apex rounded to obtuse or acute;</text>
      <biological_entity id="o19847" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o19848" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o19849" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="oblanceolate or elliptic" />
      </biological_entity>
      <biological_entity id="o19850" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o19851" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely; remotely; obscurely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19852" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–5 pairs, sessile, 23–130 × 5–25 mm, blade lanceolate to oblong, base clasping to auriculate-clasping, margins entire, rarely remotely and obscurely dentate, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o19853" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s3" to="130" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19854" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblong" />
      </biological_entity>
      <biological_entity id="o19855" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="fixation_or_architecture" src="d0_s3" value="clasping to auriculate-clasping" value_original="clasping to auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o19856" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely; remotely; obscurely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19857" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, cylindric, (4–) 10–60 cm, axis glabrous, verticillasters (2–) 5–16, cymes 2–7-flowered;</text>
      <biological_entity id="o19858" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19859" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19860" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="16" />
      </biological_entity>
      <biological_entity id="o19861" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-7-flowered" value_original="2-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, (8–) 13–55 × 2–8 mm;</text>
      <biological_entity constraint="proximal" id="o19862" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s5" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glabrous or pedicels slightly glandular-pubescent.</text>
      <biological_entity id="o19863" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19864" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19865" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 3–4.6 × 1.2–2 mm, apex acute to short-acuminate, glabrous or sparsely glandular-pubescent;</text>
      <biological_entity id="o19866" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o19867" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19868" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="short-acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla rose-pink to rose magenta, with reddish purple nectar guides, bilaterally symmetric, bilabiate, ventricose, 13–22 mm, glandular-pubescent externally, white-pilose and glandular-pubescent internally abaxially, tube 3–7 mm, throat gradually inflated, 5–7 mm diam., rounded abaxially;</text>
      <biological_entity id="o19869" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19870" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="rose-pink" name="coloration" src="d0_s8" to="rose magenta" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="white-pilose" value_original="white-pilose" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o19871" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o19872" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19873" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o19870" id="r1528" name="with" negation="false" src="d0_s8" to="o19871" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs explanate, 1.1–1.4 mm, sutures smooth;</text>
      <biological_entity id="o19874" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19875" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o19876" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19877" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 10–12 mm, flattened distally, 0.7–1 (–2.5) mm diam., tip straight, distal 4–5 mm retrorsely hairy, hairs yellow or whitish, to 1 mm;</text>
      <biological_entity id="o19878" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19879" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="diameter" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19880" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19881" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19882" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 13–15 mm.</text>
      <biological_entity id="o19883" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19884" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–9 × 4–5 mm. 2n = 16.</text>
      <biological_entity id="o19885" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19886" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon parryi is known from the scattered desert mountain ranges of southern Arizona in Cochise, Pima, Pinal, Santa Cruz, and Yavapai counties. The species resembles P. superbus; it is distinguished by its narrow leaves and corollas that are rose pink or rose magenta, more bilaterally symmetric, and white-pilose abaxially in the throats.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides, washes, canyons, oak scrub and deserts.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="oak scrub" />
        <character name="habitat" value="deserts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>