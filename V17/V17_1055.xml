<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">602</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Bentham) T. I. Chuang &amp; Heckard" date="1991" rank="species">densiflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">densiflora</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species densiflora;variety densiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>26a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs 0.9–4.7 dm.</text>
      <biological_entity id="o6827" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.9" from_unit="dm" name="some_measurement" src="d0_s0" to="4.7" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: hairs spreading, long, soft or proximal ones stiff.</text>
      <biological_entity id="o6828" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o6829" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6830" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear to broadly lanceolate or ovate, 1.5–8 cm, 0–5-lobed.</text>
      <biological_entity id="o6831" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="broadly lanceolate or ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="0-5-lobed" value_original="0-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2.5–16 (–20) cm;</text>
      <biological_entity id="o6832" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts distally pink to pink-purple or reddish purple on apices, sometimes white distally, if white sometimes aging pink.</text>
      <biological_entity id="o6833" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink to pink-purple" value_original="pink to pink-purple" />
        <character constraint="on apices" constraintid="o6834" is_modifier="false" name="coloration" src="d0_s4" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" modifier="sometimes; distally" name="coloration" notes="" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o6834" name="apex" name_original="apices" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Calyces 8–20 mm;</text>
      <biological_entity id="o6835" name="calyx" name_original="calyces" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial and adaxial clefts 6.5–15 mm, 75% of calyx length, often deeper than laterals, lateral 6.5–8 mm, 40–60% of calyx length.</text>
      <biological_entity constraint="abaxial and adaxial" id="o6836" name="cleft" name_original="clefts" src="d0_s6" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o6837" is_modifier="false" name="coloration_or_size" src="d0_s6" value="often deeper" value_original="often deeper" />
        <character is_modifier="false" modifier="75%" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6837" name="lateral" name_original="laterals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Corollas 18–29 mm;</text>
      <biological_entity id="o6838" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s7" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak adaxially pink or purple;</text>
      <biological_entity id="o6839" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip proximally white, yellow, or purple, often with purple spot in each crease, pouches widened gradually, appearing slightly inflated, longer than deep, 3–7 mm.</text>
      <biological_entity constraint="abaxial" id="o6840" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character constraint="in crease" constraintid="o6841" is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="purple spot" value_original="purple spot" />
      </biological_entity>
      <biological_entity id="o6841" name="crease" name_original="crease" src="d0_s9" type="structure" />
      <biological_entity id="o6842" name="pouch" name_original="pouches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="width" src="d0_s9" value="widened" value_original="widened" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer than deep" value_original="longer than deep" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety densiflora usually has a pink to purple inflorescence, although rare individuals in a population may be white; in the San Francisco Bay region, purely white-bracted populations are interspersed among typical populations with pink-purple bracts. This variety differs from the other two varieties in the structure of the abaxial lip of the corolla. In addition, it is distinguished from var. gracilis by a less exserted corolla within a slightly longer calyx and subtending bracts. Within the range of the species, var. densiflora extends a little farther north, and var. gracilis is more common to the south. Confusing intermediates are occasionally found, suggesting possible hybridization with either Castilleja ambigua var. ambigua or C. attenuata. A peculiar form from Inverness, Marin County, was named Orthocarpus noctuinus Eastwood and treated later by some as a hybrid with C. rubicundula var. lithospermoides.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, forest openings, meadows, roadsides, sometimes on serpentine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>