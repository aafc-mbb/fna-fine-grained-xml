<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">436</other_info_on_meta>
    <other_info_on_meta type="mention_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(A. L. Grant) G. L. Nesom" date="2012" rank="species">constrictus</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 28. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species constrictus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">subsecundus</taxon_name>
    <taxon_name authority="A. L. Grant" date="1925" rank="subspecies">constrictus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>11: 287. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species subsecundus;subspecies constrictus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="(A. L. Grant) Pennell" date="unknown" rank="species">constrictus</taxon_name>
    <taxon_hierarchy>genus m.;species constrictus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Congdon" date="unknown" rank="species">viscidus</taxon_name>
    <taxon_name authority="(A. L. Grant) Munz" date="unknown" rank="subspecies">constrictus</taxon_name>
    <taxon_hierarchy>genus m.;species viscidus;subspecies constrictus</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Dense-fruited monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, herbage usually drying dark.</text>
      <biological_entity id="o4381" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4382" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="condition" src="d0_s0" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (10–) 20–240 (–350) mm, nodes 3–6, internodes 1–6 mm, glandular-villous.</text>
      <biological_entity id="o4383" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="240" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="350" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="240" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4384" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="6" />
      </biological_entity>
      <biological_entity id="o4385" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, relatively even-sized;</text>
      <biological_entity id="o4386" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole indistinct;</text>
      <biological_entity id="o4387" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate, oblanceolate, or narrowly elliptic, (3.5–) 5–32 (–47) × 3–15 (–18) mm, margins entire or toothed, plane, apex acute or rounded, surfaces: proximals glabrous, distals glandular-pubescent.</text>
      <biological_entity id="o4388" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="32" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="47" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="32" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4389" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o4390" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4391" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o4392" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4393" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0.5–3 (–4) mm in fruit.</text>
      <biological_entity id="o4394" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o4395" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4395" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o4396" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o4397" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4397" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4398" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o4399" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4398" id="r400" name="on" negation="false" src="d0_s6" to="o4399" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, inflated in fruit, (5–) 7–12 (–15) mm, glandular-pubescent to glandular-villous, tube strongly plicate, lobes triangular, subequal, apex acute, ribs broad, darkened, blackish, thickened, strongly raised, intercostal areas whitish, membranous.</text>
      <biological_entity id="o4400" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o4401" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o4402" is_modifier="false" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="glandular-pubescent" name="pubescence" src="d0_s7" to="glandular-villous" />
      </biological_entity>
      <biological_entity id="o4401" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o4402" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o4403" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s7" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o4404" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o4405" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4406" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="darkened" value_original="darkened" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="strongly" name="prominence" src="d0_s7" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o4407" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas magenta or pinkish to red-purple, throat floor whitish with dark lines or streaks, often yellowish deep inside throat, never at mouth, palate ridges white, tube-throat (10–) 13–22 (–25) mm, limb 14–23 mm diam., not bilabiate.</text>
      <biological_entity id="o4408" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s8" to="red-purple" />
      </biological_entity>
      <biological_entity id="o4409" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character constraint="with streaks" constraintid="o4411" is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="often" name="coloration" notes="" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o4410" name="line" name_original="lines" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o4411" name="streak" name_original="streaks" src="d0_s8" type="structure" />
      <biological_entity id="o4412" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <biological_entity id="o4413" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o4414" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o4415" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_distance" src="d0_s8" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="distance" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4416" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="diameter" src="d0_s8" to="23" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o4409" id="r401" modifier="never" name="at" negation="false" src="d0_s8" to="o4413" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, ciliate.</text>
      <biological_entity id="o4417" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent.</text>
      <biological_entity id="o4418" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas included, lobes equal.</text>
      <biological_entity id="o4419" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o4420" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules (7–) 8–12 (–13.5) mm.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o4421" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="13.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4422" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus constrictus is endemic to Kern, Los Angeles, Tulare, and Ventura counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas with concentrated runoff from rains on, or just above, verges of roadside banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" constraint="with concentrated runoff" />
        <character name="habitat" value="concentrated runoff" modifier="with" />
        <character name="habitat" value="rains" modifier="from" constraint="on" />
        <character name="habitat" value="verges" modifier="on" constraint="of roadside banks" />
        <character name="habitat" value="roadside banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2100(–2400) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>