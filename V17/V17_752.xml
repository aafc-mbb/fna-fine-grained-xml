<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">scutellata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 12. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species scutellata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">connata</taxon_name>
    <taxon_hierarchy>genus veronica;species connata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scutellata</taxon_name>
    <taxon_name authority="Schumacher" date="unknown" rank="variety">villosa</taxon_name>
    <taxon_hierarchy>genus v.;species scutellata;variety villosa</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Marsh speedwell</other_name>
  <other_name type="common_name">véronique en écusson</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o4549" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, (5–) 15–45 (–80) cm, glabrous, sometimes sparsely eglandular-hairy distally.</text>
      <biological_entity id="o4550" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0 mm;</text>
      <biological_entity id="o4551" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4552" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly lanceolate or linear, rarely elliptic, (10–) 30–50 (–80) × (2–) 3–7 (–14) mm, 6–10 times as long as wide, base cuneate, margins remote and fine-dentate, apex acute, abaxial surface glabrous, adaxial glabrous or sparsely eglandular-hairy.</text>
      <biological_entity id="o4553" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4554" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s3" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="6-10" value_original="6-10" />
      </biological_entity>
      <biological_entity id="o4555" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4556" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s3" value="remote" value_original="remote" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="fine-dentate" value_original="fine-dentate" />
      </biological_entity>
      <biological_entity id="o4557" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4558" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4559" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Racemes 1–5 (–10), axillary, 40–90 (–150) mm, (2–) 5–20-flowered, axis glabrous, sometimes sparsely eglandular-hairy;</text>
      <biological_entity id="o4560" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s4" to="90" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="(2-)5-20-flowered" value_original="(2-)5-20-flowered" />
      </biological_entity>
      <biological_entity id="o4561" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s4" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate, 1.5–3 mm.</text>
      <biological_entity id="o4562" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels patent or ± curled, (6–) 10–15 (–17) mm, longer than subtending bracts, glabrous.</text>
      <biological_entity id="o4563" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="curled" value_original="curled" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character constraint="than subtending bracts" constraintid="o4564" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o4564" name="bract" name_original="bracts" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes (1–) 2–3 mm, apex obtuse to acute, glabrate;</text>
      <biological_entity id="o4565" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o4566" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4567" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla pale lilac, pale sky blue, or whitish, with pink or dark blue narrow stripes, 2.5–7 mm diam.;</text>
      <biological_entity id="o4568" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4569" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale lilac" value_original="pale lilac" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="punct" value_original="punct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale sky blue" value_original="pale sky blue" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="punct" value_original="punct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4570" name="strip" name_original="stripes" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark blue" value_original="dark blue" />
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o4569" id="r407" name="with" negation="false" src="d0_s8" to="o4570" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4–5 mm;</text>
      <biological_entity id="o4571" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4572" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style (1–) 2–4 mm.</text>
      <biological_entity id="o4573" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4574" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules compressed in cross-section, ovoid or reniform, 2.5–5 × (3–) 4–6 mm, apex emarginate by 1/3 length, glabrous.</text>
      <biological_entity id="o4575" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character constraint="in cross-section" constraintid="o4576" is_modifier="false" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4576" name="cross-section" name_original="cross-section" src="d0_s11" type="structure" />
      <biological_entity id="o4577" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 5–20, pale brownish, globular or ovoid, flat, 1.2–1.9 × 1–1.5 mm, 0.2 mm thick, smooth.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o4578" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale brownish" value_original="pale brownish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globular" value_original="globular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character name="thickness" src="d0_s12" unit="mm" value="1.2-1.9×1-1.5" value_original="1.2-1.9×1-1.5" />
        <character name="thickness" src="d0_s12" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4579" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Alpine dwarf forms of Veronica scutellata tend to have relatively broad leaves and may therefore be easily confused with other species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, marshes, shallows, forests, steppes, fens, stream banks, lakeshores.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="shallows" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="steppes" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800(–2400) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., D.C., Idaho, Ill., Ind., Iowa, La., Maine, Md., Mass., Mich., Minn., Mont., Nev., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis., Wyo.; Eurasia; Africa (Algeria); introduced in s South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa (Algeria)" establishment_means="native" />
        <character name="distribution" value="in s South America (Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>