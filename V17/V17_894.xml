<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">517</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Bentham in W. J. Hooker" date="1838" rank="species">bracteosa</taxon_name>
    <taxon_name authority="(Pennell) Cronquist in C. L. Hitchcock et al." date="1959" rank="variety">paysoniana</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>4: 358. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species bracteosa;variety paysoniana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pedicularis</taxon_name>
    <taxon_name authority="Pennell" date="1934" rank="species">paysoniana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>61: 446. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus pedicularis;species paysoniana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bracteosa</taxon_name>
    <taxon_name authority="(Pennell) W. A. Weber" date="unknown" rank="subspecies">paysoniana</taxon_name>
    <taxon_hierarchy>genus p.;species bracteosa;subspecies paysoniana</taxon_hierarchy>
  </taxon_identification>
  <number>4g.</number>
  <other_name type="common_name">Payson's lousewort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: calyx tomentose, lobes triangular;</text>
      <biological_entity id="o1292" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o1293" name="calyx" name_original="calyx" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o1294" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>corolla: tube yellow;</text>
      <biological_entity id="o1295" name="corolla" name_original="corolla" src="d0_s1" type="structure" />
      <biological_entity id="o1296" name="tube" name_original="tube" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>galea yellow, 10–15 mm, beakless, apex obtuse, sometimes acute;</text>
      <biological_entity id="o1297" name="corolla" name_original="corolla" src="d0_s2" type="structure" />
      <biological_entity id="o1298" name="galea" name_original="galea" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity id="o1299" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>abaxial lip yellow.</text>
      <biological_entity id="o1300" name="corolla" name_original="corolla" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o1301" name="lip" name_original="lip" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The long, open throat of the galea of var. paysoniana raises the summit of the galea well above the abaxial lip as compared to the other varieties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist alpine slopes, grassy meadows, coniferous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist alpine slopes" />
        <character name="habitat" value="grassy meadows" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–3600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>