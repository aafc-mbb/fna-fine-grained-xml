<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">446</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">445</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(Pennell) G. L. Nesom" date="2012" rank="species">brandegeei</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-54: 1. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species brandegeei</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Pennell" date="1947" rank="species">brandegeei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>99: 170. 1947</place_in_publication>
      <other_info_on_pub>(as brandegei)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species brandegeei</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="A. Gray 1876" date="unknown" rank="species">latifolius</taxon_name>
    <other_info_on_name>not Diplacus latifolius Nuttall 1838</other_info_on_name>
    <taxon_hierarchy>genus m.;species latifolius</taxon_hierarchy>
  </taxon_identification>
  <number>32.</number>
  <other_name type="common_name">Santa Cruz Island or broadleaf monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o4213" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 10–100 mm, glandular-puberulent to glandular-pubescent.</text>
      <biological_entity id="o4214" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="100" to_unit="mm" />
        <character char_type="range_value" from="glandular-puberulent" name="pubescence" src="d0_s1" to="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline, relatively even-sized;</text>
      <biological_entity id="o4215" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o4216" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent, larger with petiolelike extension;</text>
      <biological_entity id="o4217" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character constraint="with extension" constraintid="o4218" is_modifier="false" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o4218" name="extension" name_original="extension" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="petiole-like" value_original="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly to broadly ovate to broadly elliptic, (4–) 8–39 × (2–) 4–23 mm, margins entire or crenate, plane, proximal 1/2 usually ciliate, apex obtuse, surfaces glandular-pubescent.</text>
      <biological_entity id="o4219" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" modifier="broadly" name="shape" src="d0_s4" to="broadly elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="39" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4220" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o4221" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4222" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1.5–4 mm in fruit, not twisting to invert calyx.</text>
      <biological_entity id="o4223" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o4224" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="twisting" value_original="twisting" />
      </biological_entity>
      <biological_entity id="o4224" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity id="o4225" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
      <relation from="o4223" id="r389" name="invert" negation="false" src="d0_s5" to="o4225" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o4226" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o4227" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4227" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4228" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o4229" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4228" id="r390" name="on" negation="false" src="d0_s6" to="o4229" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces distinctly asymmetrically attached to pedicel, inflated in fruit, 8–13 mm, glandular-pubescent, lobes subequal, apex obtuse, ribs green, intercostal areas pale green to whitish.</text>
      <biological_entity id="o4230" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicel" constraintid="o4231" is_modifier="false" modifier="distinctly asymmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o4232" is_modifier="false" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o4231" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o4232" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o4233" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o4234" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4235" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o4236" name="area" name_original="areas" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s7" to="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas: throat magenta, golden yellow at base, limb magenta, palate ridges golden yellow, purple-speckled, tube-throat 12–19 mm, limb 5–11 mm diam., bilabiate, abaxial lip smaller than adaxial.</text>
      <biological_entity id="o4237" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
      <biological_entity id="o4238" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character constraint="at base" constraintid="o4239" is_modifier="false" name="coloration" src="d0_s8" value="golden yellow" value_original="golden yellow" />
      </biological_entity>
      <biological_entity id="o4239" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o4240" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
      </biological_entity>
      <biological_entity constraint="palate" id="o4241" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple-speckled" value_original="purple-speckled" />
      </biological_entity>
      <biological_entity id="o4242" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="distance" src="d0_s8" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4243" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4244" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character constraint="than adaxial lip" constraintid="o4245" is_modifier="false" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4245" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers (distal pair) nearly exserted, glabrous.</text>
      <biological_entity id="o4246" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles pubescent distally.</text>
      <biological_entity id="o4247" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes unequal, abaxial 3–4 times adaxial.</text>
      <biological_entity id="o4248" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o4249" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4250" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character constraint="lobe" constraintid="o4251" is_modifier="false" name="size_or_quantity" src="d0_s11" value="3-4 times adaxial lobes" value_original="3-4 times adaxial lobes" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4251" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules (5–) 6–11 mm, indehiscent.</text>
      <biological_entity id="o4252" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus brandegeei is known only from Santa Catalina (Los Angeles County), Santa Cruz (Santa Barbara County), and Guadalupe (Baja California) islands. The earliest name for the species, Mimulus latifolius A. Gray, was first transferred to Diplacus as D. latifolius (A. Gray) G. L. Nesom, but that name is a later homonym of D. latifolius Nuttall.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr(–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, brushy slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brushy" modifier="rocky" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>