<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">595</other_info_on_meta>
    <other_info_on_meta type="mention_page">657</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Nuttall) G. Don" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_name authority="A. Nelson" date="1902" rank="variety">dubia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 404. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species angustifolia;variety dubia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="(A. Nelson) A. Nelson" date="unknown" rank="species">dubia</taxon_name>
    <taxon_hierarchy>genus castilleja;species dubia</taxon_hierarchy>
  </taxon_identification>
  <number>3b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: hairs fairly dense, spreading, short and long, soft to stiff, eglandular.</text>
      <biological_entity id="o27737" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o27738" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="fairly" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bracts distally yellow to pale orange or white, sometimes red, 3–9-lobed, sometimes with secondary lobes.</text>
      <biological_entity id="o27739" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="distally yellow" name="coloration" src="d0_s1" to="pale orange or white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s1" value="3-9-lobed" value_original="3-9-lobed" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o27740" name="lobe" name_original="lobes" src="d0_s1" type="structure" />
      <relation from="o27739" id="r2131" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o27740" />
    </statement>
    <statement id="d0_s2">
      <text>Calyces 18–22 mm;</text>
      <biological_entity id="o27741" name="calyx" name_original="calyces" src="d0_s2" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s2" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>abaxial clefts 5–7 mm, adaxial 7–8 mm.</text>
      <biological_entity constraint="abaxial" id="o27742" name="cleft" name_original="clefts" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27743" name="cleft" name_original="clefts" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Corollas 18–27 mm.</text>
      <biological_entity id="o27744" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s4" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety dubia is often confused with Castilleja chromosa in herbaria, floras, and databases, but they are not synonymous. Where C. angustifolia and C. chromosa are sympatric in Wyoming, the specimens show little evidence of hybridization. Variety dubia, found primarily in southwestern South Dakota and east-central Wyoming, can be recognized by its usually narrower, yellow to pale orange inflorescences, as well as by its shorter corollas, while C. chromosa has wider, red inflorescences and longer corollas. Occasional hybrids between var. dubia and C. sessiliflora are known from northeastern Wyoming.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sagebrush slopes and flats in mountains, high plains.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sagebrush slopes" constraint="in mountains , high plains" />
        <character name="habitat" value="flats" constraint="in mountains , high plains" />
        <character name="habitat" value="mountains" />
        <character name="habitat" value="high plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>