<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">115</other_info_on_meta>
    <other_info_on_meta type="mention_page">114</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Coerulei</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">angustifolius</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">angustifolius</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species angustifolius;variety angustifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>30a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (6–) 10–30 cm, glabrous or scabrous.</text>
      <biological_entity id="o24271" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves 2–5 (or 6) pairs, blade lanceolate to linear.</text>
      <biological_entity constraint="cauline" id="o24272" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
      <biological_entity id="o24273" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Thyrses: proximal bracts lanceolate to linear.</text>
      <biological_entity id="o24274" name="thyrse" name_original="thyrses" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o24275" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: corolla blue, rarely lavender or pink, 14–18 mm. 2n = 16.</text>
      <biological_entity id="o24276" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o24277" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s3" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24278" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety angustifolius is known from the northern Great Plains, and it passes into var. caudatus in northeastern Colorado and western Nebraska.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, shortgrass prairies, sagebrush shrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="shortgrass prairies" />
        <character name="habitat" value="shrublands" modifier="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>