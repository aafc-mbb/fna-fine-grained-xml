<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">149</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Reichenbach ex Spach) Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Elmigera</taxon_name>
    <taxon_name authority="A. Gray" date="1872" rank="species">eatonii</taxon_name>
    <taxon_name authority="(A. Nelson) C. C. Freeman" date="2017" rank="variety">exsertus</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>80: 35. 2017</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section elmigera;species eatonii;variety exsertus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Nelson" date="1931" rank="species">exsertus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>18: 438. 1931</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species exsertus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">eatonii</taxon_name>
    <taxon_name authority="(A. Nelson) D. D. Keck" date="unknown" rank="subspecies">exsertus</taxon_name>
    <taxon_hierarchy>genus p.;species eatonii;subspecies exsertus</taxon_hierarchy>
  </taxon_identification>
  <number>83b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems retrorsely hairy.</text>
      <biological_entity id="o3587" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves retrorsely hairy.</text>
      <biological_entity id="o3588" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: stamens: both pairs exserted.</text>
      <biological_entity id="o3589" name="flower" name_original="flowers" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 16.</text>
      <biological_entity id="o3590" name="stamen" name_original="stamens" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3591" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety exsertus is concentrated in central Arizona, especially from Flagstaff to Tucson. Records are known from Coconino, Gila, Greenlee, Maricopa, Mohave, Pinal, and Yavapai counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, pinyon-juniper woodlands, pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="forests" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>