<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PLANTAGO</taxon_name>
    <taxon_name authority="Linnaeus" date="1762" rank="species">afra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 1: 168. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus plantago;species afra</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Glandular plantain</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
      <biological_entity id="o11741" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots taproots, slender.</text>
      <biological_entity constraint="roots" id="o11742" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 100–350 mm, freely branched.</text>
      <biological_entity id="o11743" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="100" from_unit="mm" name="some_measurement" src="d0_s2" to="350" to_unit="mm" />
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, opposite, 30–60 × 1–4 mm;</text>
      <biological_entity id="o11744" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to linear-lanceolate, margins entire or slightly toothed, veins conspicuous or not, surfaces hairy.</text>
      <biological_entity id="o11745" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o11746" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o11747" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character name="prominence" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o11748" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scapes 30–50 mm, hairy.</text>
      <biological_entity id="o11749" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes greenish or brownish, 40–65 mm, densely flowered, glandular-hairy;</text>
      <biological_entity id="o11750" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="65" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts all similar, ovate, 3–5 mm, lengths 1–1.5 times sepals.</text>
      <biological_entity id="o11751" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character constraint="sepal" constraintid="o11752" is_modifier="false" name="length" src="d0_s7" value="1-1.5 times sepals" value_original="1-1.5 times sepals" />
      </biological_entity>
      <biological_entity id="o11752" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 3–3.5 mm;</text>
      <biological_entity id="o11753" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11754" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla radially symmetric, lobes reflexed, 2–3 mm, base obtuse;</text>
      <biological_entity id="o11755" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11756" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o11757" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11758" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4.</text>
      <biological_entity id="o11759" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11760" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules lanceoloid.</text>
      <biological_entity id="o11761" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceoloid" value_original="lanceoloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2, 2–3 mm. 2n = 12.</text>
      <biological_entity id="o11762" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" unit=",-3 mm" />
        <character name="some_measurement" src="d0_s12" unit=",-3 mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11763" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plantago afra is known in Massachusetts from a single collection made in 1927 in Worcester County.</discussion>
  <discussion>Plantago psyllium Linnaeus (1762, not 1753), a rejected name, and P. indica Linnaeus are misapplied names that pertain here. Plantago squalida Salisbury is an illegitimate name that pertains here.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed habitats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="habitats" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mass.; s Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>