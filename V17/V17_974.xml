<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="Dubrule &amp; Canne-Hilliker" date="1993" rank="species">navasotensis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>15: 426, figs. 1–7. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species navasotensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Navasota false foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems branched, 25–80 cm;</text>
      <biological_entity id="o37911" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches spreading-ascending, nearly terete proximally, obtusely quadrangular-ridged distally, glabrous or scabridulous distally.</text>
      <biological_entity id="o37912" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" modifier="nearly; proximally" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="obtusely; distally" name="shape" src="d0_s1" value="quadrangular-ridged" value_original="quadrangular-ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves proximal to mid reflexed or recurved, distal spreading;</text>
      <biological_entity id="o37913" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal to mid" value_original="proximal to mid" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade filiform, (11–) 17–30 (–40) x 0.5–1.2 mm, not fleshy, margins entire, siliceous, abaxial midvein scabridulous, adaxial surface scabridulous;</text>
      <biological_entity id="o37914" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_length" src="d0_s3" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o37915" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="siliceous" value_original="siliceous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o37916" name="midvein" name_original="midvein" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity constraint="adaxial" id="o37917" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemiform-paniculate, flowers 1 or 2 per node;</text>
      <biological_entity id="o37918" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemiform-paniculate" value_original="racemiform-paniculate" />
      </biological_entity>
      <biological_entity id="o37919" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o37920" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts both longer and shorter than, or shorter than, pedicels.</text>
      <biological_entity id="o37921" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o37922" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending-spreading, (2–) 6–25 mm, scabridulous proximally or glabrous.</text>
      <biological_entity id="o37923" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx funnelform-obconic, tube 2.2–4.6 mm, glabrous, lobes triangular-subulate to subulate, 0.5–1.5 mm;</text>
      <biological_entity id="o37924" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o37925" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="funnelform-obconic" value_original="funnelform-obconic" />
      </biological_entity>
      <biological_entity id="o37926" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s8" to="4.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o37927" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="triangular-subulate" name="shape" src="d0_s8" to="subulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pink to rose, with 2 yellow lines and red spots in abaxial throat, 15–24 mm, throat pilose externally and glabrous within across bases of adaxial lobes, sparsely villous at sinus, lobes spreading, 5–7 mm, equal, glabrous externally;</text>
      <biological_entity id="o37928" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37929" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="rose" />
        <character constraint="in abaxial throat" constraintid="o37931" is_modifier="false" name="coloration" notes="" src="d0_s9" value="red spots" value_original="red spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37930" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o37931" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity id="o37932" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character constraint="within bases" constraintid="o37933" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="at sinus" constraintid="o37934" is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o37933" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o37934" name="sinus" name_original="sinus" src="d0_s9" type="structure" />
      <biological_entity id="o37935" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o37929" id="r2916" name="with" negation="false" src="d0_s9" to="o37930" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal perpendicular to filaments, pollen-sacs 2–3.2 mm;</text>
      <biological_entity id="o37936" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o37937" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o37938" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character constraint="to filaments" constraintid="o37939" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o37938" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o37939" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o37940" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s10" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style exserted, 11–15 mm.</text>
      <biological_entity id="o37941" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o37942" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid to obovoid-oblong, (4–) 6–7 mm.</text>
      <biological_entity id="o37943" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="obovoid-oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds dark-brown, 0.8–2.3 mm. 2n = 26.</text>
      <biological_entity id="o37944" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37945" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agalinis navasotensis is known from Grimes and Tyler counties in eastern Texas; it should be looked for elsewhere in eastern Texas and northwestern Louisiana. Agalinis navasotensis differs from A. caddoensis by subtleties of its calyx shape, leaf length, corolla length, inflorescence form, and offset pollen sacs. Additional collections may show that A. navasotensis and the morphologically similar, but poorly known, A. caddoensis of western Louisiana are conspecific.</discussion>
  <discussion>Agalinis navasotensis is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky prairie remnants on sandstone outcrops, sandy clay soils of longleaf pine savannas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky prairie" />
        <character name="habitat" value="sandstone outcrops" modifier="remnants on" />
        <character name="habitat" value="sandy clay soils" constraint="of longleaf pine savannas" />
        <character name="habitat" value="longleaf pine savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>