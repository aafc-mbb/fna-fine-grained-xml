<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">477</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="(Rydberg) Ferris" date="1958" rank="species">corymbosa</taxon_name>
    <taxon_name authority="Heckard" date="1978" rank="subspecies">mutabilis</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>56: 187. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species corymbosa;subspecies mutabilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphyllon</taxon_name>
    <taxon_name authority="(Rydberg) A. C. Schneider" date="unknown" rank="species">corymbosum</taxon_name>
    <taxon_name authority="(Heckard) A. C. Schneider" date="unknown" rank="subspecies">mutabile</taxon_name>
    <taxon_hierarchy>genus aphyllon;species corymbosum;subspecies mutabile</taxon_hierarchy>
  </taxon_identification>
  <number>8b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple, rarely branched.</text>
      <biological_entity id="o7782" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences short racemes or racemelike to elongate corymbs.</text>
      <biological_entity id="o7783" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o7784" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character char_type="range_value" from="racemelike" name="shape" src="d0_s1" to="elongate" />
      </biological_entity>
      <biological_entity id="o7785" name="corymb" name_original="corymbs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Calyces 12–18 mm, sometimes equal to or longer than corolla.</text>
      <biological_entity id="o7786" name="calyx" name_original="calyces" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s2" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character constraint="than corolla" constraintid="o7787" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7787" name="corolla" name_original="corolla" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Corollas rose, pink, pale-purple, or white with darker veins, glabrate or slightly glandular-pubescent;</text>
      <biological_entity id="o7788" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale-purple" value_original="pale-purple" />
        <character constraint="with veins" constraintid="o7789" is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o7789" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="darker" value_original="darker" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>anthers tomentose or glabrous.</text>
      <biological_entity id="o7790" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Populations of subsp. mutabilis are more abundant in the northern portion of the range; populations elsewhere appear to be somewhat isolated. The more elongated inflorescence seems to be the distinguishing character, although this is often difficult to determine. The corollas often display less pigmentation. Anthers vary from quite woolly in some populations to completely glabrous in other populations. This variation could be the result of geographic isolation or hybridization.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils and rocky slopes in open sagebrush communities.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="open sagebrush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–3200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man.; Calif., Nev., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>