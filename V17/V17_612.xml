<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="M. E. Jones" date="1908" rank="species">pseudospectabilis</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>12: 66. 1908</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species pseudospectabilis;</taxon_hierarchy>
  </taxon_identification>
  <number>236.</number>
  <other_name type="common_name">Mojave beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o12105" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (30–) 60–100 cm, slightly glaucous or not.</text>
      <biological_entity id="o12106" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s1" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline 20–165 × 12–50 mm, blade ovate to lanceolate, base tapered, margins finely to coarsely dentate, apex obtuse to acute;</text>
      <biological_entity id="o12107" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="165" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s2" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12108" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o12109" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o12110" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="finely to coarsely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o12111" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 4–7 pairs, sessile or proximals short-petiolate, 45–90 × 22–76 mm, blade ovate to triangular, base tapered or sessile on proximals, auriculate-clasping to connate-perfoliate on distal ones, margins finely to coarsely dentate, apex acute.</text>
      <biological_entity id="o12112" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o12113" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o12114" name="proximal" name_original="proximals" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="width" src="d0_s3" to="76" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12115" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="triangular" />
      </biological_entity>
      <biological_entity id="o12116" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character constraint="on proximals" constraintid="o12117" is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" constraint="on distal leaves" constraintid="o12118" from="auriculate-clasping" name="architecture" notes="" src="d0_s3" to="connate-perfoliate" />
      </biological_entity>
      <biological_entity id="o12117" name="proximal" name_original="proximals" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o12118" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12119" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely to coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o12120" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted or continuous, secund, (6–) 12–48 cm, axis glabrous, verticillasters 4–13, cymes 1–3 (–6) -flowered;</text>
      <biological_entity id="o12121" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s4" to="48" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12122" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12123" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="13" />
      </biological_entity>
      <biological_entity id="o12124" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3(-6)-flowered" value_original="1-3(-6)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate (connate-perfoliate) or lanceolate (auriculate-clasping), (9–) 16–55 × 10–60 mm;</text>
      <biological_entity constraint="proximal" id="o12125" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s5" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glabrous or glandular-pubescent.</text>
      <biological_entity id="o12126" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o12127" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 3–5.5 × 2–3.2 mm, glabrous or glandular-pubescent;</text>
      <biological_entity id="o12128" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o12129" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla rose-pink, with reddish purple nectar guides, strongly bilabiate, ventricose, (18–) 25–34 mm, glandular-pubescent externally, glandular-pubescent internally, tube 6–9 mm, length 1.6–2 times calyx lobes, throat gradually inflated, ± constricted at orifice, 7–10 mm diam., rounded abaxially;</text>
      <biological_entity id="o12130" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12131" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose-pink" value_original="rose-pink" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="18" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="34" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o12132" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o12133" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character constraint="lobe" constraintid="o12134" is_modifier="false" name="length" src="d0_s8" value="1.6-2 times calyx lobes" value_original="1.6-2 times calyx lobes" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o12134" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o12135" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o12136" is_modifier="false" modifier="more or less" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" notes="" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o12136" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o12131" id="r964" name="with" negation="false" src="d0_s8" to="o12132" />
    </statement>
    <statement id="d0_s9">
      <text>stamens: longer pair reaching orifice, filaments of shorter pair glandular-puberulent proximally, pollen-sacs explanate, 1.5–2 mm, sutures smooth;</text>
      <biological_entity id="o12137" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12138" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o12139" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o12140" name="pair" name_original="pair" src="d0_s9" type="structure" />
      <biological_entity id="o12141" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12142" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o12137" id="r965" name="reaching" negation="false" src="d0_s9" to="o12138" />
      <relation from="o12139" id="r966" name="part_of" negation="false" src="d0_s9" to="o12140" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 11–17 mm, included, 0.2–0.4 mm diam., tip straight to recurved, glabrous or distal 1–2 mm sparsely to moderately lanate, hairs yellow, to 1 mm;</text>
      <biological_entity id="o12143" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o12144" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12145" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s10" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o12146" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 20–25 mm, glabrous.</text>
      <biological_entity id="o12147" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o12148" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–12 × 4–6 mm, glabrous.</text>
      <biological_entity id="o12149" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Utah; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Two varieties, distinguished on the basis of inflorescence indument, are largely allopatric. Penstemon ×crideri A. Nelson, a putative hybrid between P. eatonii and P. pseudospectabilis, has been reported from Arizona (A. Nelson 1936).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Peduncles, pedicels, and calyx lobes glandular-pubescent.</description>
      <determination>236a. Penstemon pseudospectabilis var. pseudospectabilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Peduncles, pedicels, and calyx lobes glabrous.</description>
      <determination>236b. Penstemon pseudospectabilis var. connatifolius</determination>
    </key_statement>
  </key>
</bio:treatment>