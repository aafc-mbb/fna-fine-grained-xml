<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Willdenow" date="1800" rank="species">sudetica</taxon_name>
    <taxon_name authority="(A. Gray) Hultén" date="1964" rank="subspecies">scopulorum</taxon_name>
    <place_of_publication>
      <publication_title>Svensk Bot. Tidskr.</publication_title>
      <place_in_publication>58: 437. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species sudetica;subspecies scopulorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pedicularis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="species">scopulorum</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 308. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus pedicularis;species scopulorum</taxon_hierarchy>
  </taxon_identification>
  <number>35e.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Racemes: bracts white-lanate.</text>
      <biological_entity id="o15940" name="raceme" name_original="racemes" src="d0_s0" type="structure" />
      <biological_entity id="o15941" name="bract" name_original="bracts" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx white-lanate, lobes triangular, margins entire distally;</text>
      <biological_entity id="o15942" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o15943" name="calyx" name_original="calyx" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity id="o15944" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o15945" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>corolla: galea purple to magenta, margins entire medially, 1-toothed or entire distally;</text>
      <biological_entity id="o15946" name="corolla" name_original="corolla" src="d0_s2" type="structure" />
      <biological_entity id="o15947" name="galea" name_original="galea" src="d0_s2" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s2" to="magenta" />
      </biological_entity>
      <biological_entity id="o15948" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-toothed" value_original="1-toothed" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>abaxial lip pale-purple to magenta, not purple-spotted.</text>
      <biological_entity id="o15949" name="corolla" name_original="corolla" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o15950" name="lip" name_original="lip" src="d0_s3" type="structure">
        <character char_type="range_value" from="pale-purple" name="coloration" src="d0_s3" to="magenta" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="purple-spotted" value_original="purple-spotted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies scopulorum can be mistaken for Pedicularis cystopteridifolia, which has similar flowers and growth form. The leaves of subsp. scopulorum, however, are not finely dissected like those of P. cystopteridifolia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra meadows" modifier="alpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3100–3700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="3100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>