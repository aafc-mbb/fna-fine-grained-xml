<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHRASIA</taxon_name>
    <taxon_name authority="(B. L. Robinson) Gussarova" date="2017" rank="species">farlowii</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>11: 290. 2017</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus euphrasia;species farlowii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphrasia</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">randii</taxon_name>
    <taxon_name authority="B. L. Robinson" date="1901" rank="variety">farlowii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>3: 274. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus euphrasia;species randii;variety farlowii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="Reeks ex Fernald &amp; Wiegand [not Desfontaines]" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(B. L. Robinson) Fernald &amp; Wiegand" date="unknown" rank="variety">farlowii</taxon_name>
    <taxon_hierarchy>genus e.;species purpurea;variety farlowii</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Farlow’s eyebright</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, to 9 (–12) cm;</text>
      <biological_entity id="o33493" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1–3 pairs, ascending, from basal cauline nodes;</text>
      <biological_entity id="o33494" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="basal cauline" id="o33495" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o33494" id="r2549" name="from" negation="false" src="d0_s1" to="o33495" />
    </statement>
    <statement id="d0_s2">
      <text>cauline internode lengths 1–2 times subtending leaves.</text>
      <biological_entity constraint="cauline" id="o33496" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o33497" is_modifier="false" name="length" src="d0_s2" value="1-2 times subtending leaves" value_original="1-2 times subtending leaves" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o33497" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade orbiculate to triangular-ovate or oval, 2–4 (–6) mm, margins crenate to incised-crenate, teeth 1–4 pairs, apices obtuse to subacute.</text>
      <biological_entity id="o33498" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o33499" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s3" to="triangular-ovate or oval" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33500" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="incised-crenate" />
      </biological_entity>
      <biological_entity id="o33501" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o33502" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences sparsely spicate, not 4-angled, beginning at node 4–6, proximal internode lengths 1–1.5 times bracts;</text>
      <biological_entity id="o33503" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o33504" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o33505" name="internode" name_original="internode" src="d0_s4" type="structure">
        <character constraint="bract" constraintid="o33506" is_modifier="false" name="length" src="d0_s4" value="1-1.5 times bracts" value_original="1-1.5 times bracts" />
      </biological_entity>
      <biological_entity id="o33506" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <relation from="o33503" id="r2550" name="beginning at" negation="false" src="d0_s4" to="o33504" />
    </statement>
    <statement id="d0_s5">
      <text>bracts green or suffused with purple, as broad as leaves, ovate or oval, length not more than 2 times width, 2–4 mm, base round, surfaces coarsely, densely hirsute with short-eglandular hairs, teeth 3–5 pairs, as long as wide, apices obtuse to acute, sinuses between teeth acute.</text>
      <biological_entity id="o33507" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="suffused with purple" value_original="suffused with purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oval" value_original="oval" />
        <character is_modifier="false" modifier="not" name="l_w_ratio" src="d0_s5" value="2+" value_original="2+" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33508" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o33509" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o33510" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character constraint="with hairs" constraintid="o33511" is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o33511" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="short-eglandular" value_original="short-eglandular" />
      </biological_entity>
      <biological_entity id="o33512" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="width" src="d0_s5" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o33513" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity constraint="between teeth" constraintid="o33515" id="o33514" name="sinuse" name_original="sinuses" src="d0_s5" type="structure" constraint_original="between  teeth, ">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o33515" name="tooth" name_original="teeth" src="d0_s5" type="structure" />
      <relation from="o33507" id="r2551" name="as broad as" negation="false" src="d0_s5" to="o33508" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes straight, apex acute;</text>
      <biological_entity id="o33516" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o33517" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o33518" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla white or cream, rarely purple, adaxial lip lilac or purple, 2.5–4.5 mm, lips moreorless equal.</text>
      <biological_entity id="o33519" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o33520" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33521" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lilac" value_original="lilac" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33522" name="lip" name_original="lips" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules oval to oblong or obovate, 2.5–4 mm, apex retuse to emarginate.</text>
      <biological_entity id="o33523" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s8" to="oblong or obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33524" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="retuse" name="shape" src="d0_s8" to="emarginate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphrasia farlowii differs from E. randii by its compact growth with condensed (versus elongated) cauline internodes and growth on calcareous soils versus non-calcareous soils (G. Gussarova 2017).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, grassy habitats on sandstone or limestone barrens, rocks, ledges, sandy beaches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" constraint="on sandstone or limestone barrens , rocks , ledges , sandy beaches" />
        <character name="habitat" value="grassy habitats" constraint="on sandstone or limestone barrens , rocks , ledges , sandy beaches" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="limestone barrens" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="sandy beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I.; Maine.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>