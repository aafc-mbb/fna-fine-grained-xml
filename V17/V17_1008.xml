<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">564</other_info_on_meta>
    <other_info_on_meta type="mention_page">561</other_info_on_meta>
    <other_info_on_meta type="mention_page">563</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="genus">SEYMERIA</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">pectinata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 737. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus seymeria;species pectinata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Afzelia</taxon_name>
    <taxon_name authority="(Pursh) Kuntze" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>genus afzelia;species pectinata</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Comb seymeria</other_name>
  <other_name type="common_name">combleaf senna</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems pubescent to villous, eglandular.</text>
      <biological_entity id="o26089" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s0" to="villous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade margins pinnatifid to 2-pinnatifid, pinnules lanceolate, surfaces not scabrid.</text>
      <biological_entity id="o26090" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="blade" id="o26091" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s1" to="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o26092" name="pinnule" name_original="pinnules" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o26093" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence_or_relief" src="d0_s1" value="scabrid" value_original="scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels 3–5 mm.</text>
      <biological_entity id="o26094" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: calyx lobes lanceolate;</text>
      <biological_entity id="o26095" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="calyx" id="o26096" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla yellow, sometimes with maroon spots on adaxial lobes, externally pubescent, internally pubescent between lobes and in a ring at stamen insertion;</text>
      <biological_entity id="o26097" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o26098" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="externally" name="pubescence" notes="" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character constraint="between lobes and in ring" constraintid="o26100" is_modifier="false" modifier="internally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26099" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="maroon spots" value_original="maroon spots" />
      </biological_entity>
      <biological_entity id="o26100" name="ring" name_original="ring" src="d0_s4" type="structure" />
      <relation from="o26098" id="r1986" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o26099" />
    </statement>
    <statement id="d0_s5">
      <text>filaments tomentose to lanate distally, anthers dehiscing to 1/4 length.</text>
      <biological_entity id="o26101" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26102" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" modifier="distally" name="pubescence" src="d0_s5" to="lanate" />
      </biological_entity>
      <biological_entity id="o26103" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules symmetric, pyriform, glabrescent to densely tomentose.</text>
      <biological_entity id="o26104" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s6" to="densely tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds irregularly shaped, wings present.</text>
      <biological_entity id="o26105" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="irregularly--shaped" value_original="irregularly--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 26.</text>
      <biological_entity id="o26106" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26107" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems pubescent to villous; capsules densely tomentose.</description>
      <determination>4a. Seymeria pectinata subsp. pectinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems pubescent to puberulous; capsules glabrescent to pubescent.</description>
      <determination>4b. Seymeria pectinata subsp. peninsularis</determination>
    </key_statement>
  </key>
</bio:treatment>