<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">131</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="(D. D. Keck) G. T. Nisbet &amp; R. C. Jackson" date="1960" rank="species">breviculus</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Kansas Sci. Bull.</publication_title>
      <place_in_publication>41: 734. 1960</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species breviculus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">jamesii</taxon_name>
    <taxon_name authority="D. D. Keck" date="1938" rank="subspecies">breviculus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>65: 241. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species jamesii;subspecies breviculus</taxon_hierarchy>
  </taxon_identification>
  <number>54.</number>
  <other_name type="common_name">Short-stem beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 8–20 (–35) cm, retrorsely hairy.</text>
      <biological_entity id="o28715" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, retrorsely hairy, sometimes glabrate;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, 35–95 × 3–18 mm, blade oblanceolate to lanceolate or linear, base tapered, margins entire or obscurely dentate, rarely prominently dentate distally, apex rounded to obtuse or acute;</text>
      <biological_entity id="o28716" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o28717" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o28718" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o28719" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o28720" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely prominently; distally" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o28721" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline (1 or) 2–5 pairs, short-petiolate or sessile, 33–78 × 4–10 mm, blade elliptic to oblanceolate, lanceolate or linear, base tapered to clasping, margins entire or dentate, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o28722" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="33" from_unit="mm" name="length" src="d0_s3" to="78" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28723" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblanceolate lanceolate or linear" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblanceolate lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o28724" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o28725" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o28726" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous or ± interrupted, cylindric, 4–11 (–18) cm, axis glandular-pubescent, verticillasters 3–5, cymes (1 or) 2–6-flowered, 2 per node;</text>
      <biological_entity id="o28727" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="18" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28728" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o28729" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o28730" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-6-flowered" value_original="2-6-flowered" />
        <character constraint="per node" constraintid="o28731" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28731" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 16–38 × 2–8 mm;</text>
      <biological_entity constraint="proximal" id="o28732" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s5" to="38" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o28733" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o28734" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate, 4.5–7.5 × 1.2–2 mm, glandular-pubescent;</text>
      <biological_entity id="o28735" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o28736" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla dark blue to violet or purple, with reddish violet nectar guides, ampliate, 10–15 (–18) mm, yellowish or whitish villous internally abaxially, tube 5–6 mm, throat abruptly inflated, ± constricted at orifice, 3.5–6 mm diam., rounded abaxially;</text>
      <biological_entity id="o28737" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28738" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s8" to="violet or purple" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="18" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o28739" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish violet" value_original="reddish violet" />
      </biological_entity>
      <biological_entity id="o28740" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28741" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o28742" is_modifier="false" modifier="more or less" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28742" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o28738" id="r2198" name="with" negation="false" src="d0_s8" to="o28739" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs opposite, explanate, 0.6–1 mm, dehiscing completely, sutures smooth;</text>
      <biological_entity id="o28743" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28744" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o28745" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o28746" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 7–8 mm, reaching orifice or exserted, 0.3–0.4 mm diam., tip straight to recurved, distal 5–6 mm densely pubescent, hairs orangish yellow, to 1.5 mm, and medial hairs shorter, stiffer, and retrorse;</text>
      <biological_entity id="o28747" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28748" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28749" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o28750" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28751" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28752" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish yellow" value_original="orangish yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="medial" id="o28753" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiffer" value_original="stiffer" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <relation from="o28748" id="r2199" name="reaching" negation="false" src="d0_s10" to="o28749" />
    </statement>
    <statement id="d0_s11">
      <text>style 9–11 mm.</text>
      <biological_entity id="o28754" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28755" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–9 × 4.5–6 mm. 2n = 16.</text>
      <biological_entity id="o28756" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28757" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon breviculus is known from the Four Corners region. Populations have been documented in Arizona (Apache County), Colorado (Montezuma, Montrose, and San Miguel counties), New Mexico (McKinley and San Juan counties), and Utah (Grand, San Juan, and Wayne counties).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or clayey soils, sagebrush shrublands, pinyon-juniper woodlands, desert grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clayey soils" />
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="desert grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>