<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">502</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MELAMPYRUM</taxon_name>
    <taxon_name authority="Desrousseaux in J. Lamarck et al." date="1797" rank="species">lineare</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>4: 22. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus melampyrum;species lineare</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melampyrum</taxon_name>
    <taxon_name authority="Muhlenberg ex Britton" date="unknown" rank="species">latifolium</taxon_name>
    <taxon_hierarchy>genus melampyrum;species latifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lineare</taxon_name>
    <taxon_name authority="(Michaux) Beauverd" date="unknown" rank="variety">americanum</taxon_name>
    <taxon_hierarchy>genus m.;species lineare;variety americanum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lineare</taxon_name>
    <taxon_name authority="(Muhlenberg ex Britton) Beauverd" date="unknown" rank="variety">latifolium</taxon_name>
    <taxon_hierarchy>genus m.;species lineare;variety latifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lineare</taxon_name>
    <taxon_name authority="(Pennell) Fernald" date="unknown" rank="variety">pectinatum</taxon_name>
    <taxon_hierarchy>genus m.;species lineare;variety pectinatum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Narrowleaf cow-wheat</other_name>
  <other_name type="common_name">mélampyre linéaire</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 5–40 cm;</text>
      <biological_entity id="o29797" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1–3 (–5) pairs, opposite, rarely with secondary branches.</text>
      <biological_entity id="o29798" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o29799" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <relation from="o29798" id="r2263" modifier="rarely" name="with" negation="false" src="d0_s1" to="o29799" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear to ovate, 13–55 x 2–22 mm, puberulent.</text>
      <biological_entity id="o29800" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o29801" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: flower pairs 4–10;</text>
      <biological_entity id="o29802" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o29803" name="flower" name_original="flower" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts resembling foliage leaves or with 1 or 2 pairs of proximal teeth.</text>
      <biological_entity id="o29804" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o29805" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity constraint="foliage" id="o29806" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o29807" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <relation from="o29805" id="r2264" name="resembling" negation="false" src="d0_s4" to="o29806" />
      <relation from="o29805" id="r2265" name="part_of" negation="false" src="d0_s4" to="o29807" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0–3 (–5) mm.</text>
      <biological_entity id="o29808" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 3–5 x 2–3 mm, lobes equal to tube, often reflexed, adaxial slightly longer than abaxial;</text>
      <biological_entity id="o29809" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o29810" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29811" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character constraint="to tube" constraintid="o29812" is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="often" name="orientation" notes="" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o29812" name="tube" name_original="tube" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o29813" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character constraint="than abaxial lobes" constraintid="o29814" is_modifier="false" name="length_or_size" src="d0_s6" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29814" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla 10–14 x 3–4 mm, abaxial lip with palate yellow and divided by a longitudinal groove, sinuses 1–2 mm, lanose within;</text>
      <biological_entity id="o29815" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o29816" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29817" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character constraint="by groove" constraintid="o29819" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o29818" name="palate" name_original="palate" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o29819" name="groove" name_original="groove" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o29820" name="sinuse" name_original="sinuses" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="lanose" value_original="lanose" />
      </biological_entity>
      <relation from="o29817" id="r2266" name="with" negation="false" src="d0_s7" to="o29818" />
    </statement>
    <statement id="d0_s8">
      <text>anthers included in adaxial lip, hairy.</text>
      <biological_entity id="o29821" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o29822" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29823" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <relation from="o29822" id="r2267" name="included in" negation="false" src="d0_s8" to="o29823" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules 6–9 x 3–5 mm, moreorless falcate, compressed.</text>
      <biological_entity id="o29824" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 2–4 x 0.5–1 mm, testa smooth.</text>
      <biological_entity id="o29825" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o29826" name="testa" name_original="testa" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29827" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melampyrum lineare has been divided into four varieties on the basis of vegetative traits: leaf shape and margin, internode length, and degree of branching (or bushiness). Without exception, circumscriptions are overlapping for these traits, none of which is strictly diagnostic. Authors are also not in agreement about which varieties to recognize. Geographic ranges of the varieties are largely confluent; much of the morphological diversity of M. lineare is present throughout the range.</discussion>
  <discussion>Melampyrum lineare is an obligate parasite insofar as it does not flower or fruit without a host (J. E. Cantlon et al. 1963). It parasitizes primarily woody plants, including Acer saccharum, Pinus banksiana, P. resinosa, P. strobus, P. sylvestris, Populus grandidentata, P. tremuloides, Quercus rubra, and Vaccinium angustifolium.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep; fruiting Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous and deciduous forests, sandy glades, gravelly terraces, heaths, rocky barrens, coastal headlands, dry meadows, peatlands, fens, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="deciduous forests" />
        <character name="habitat" value="sandy glades" />
        <character name="habitat" value="gravelly terraces" />
        <character name="habitat" value="heaths" />
        <character name="habitat" value="rocky barrens" />
        <character name="habitat" value="coastal headlands" />
        <character name="habitat" value="dry meadows" />
        <character name="habitat" value="peatlands" />
        <character name="habitat" value="fens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000(–3000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Conn., Del., Ga., Idaho, Ill., Ind., Ky., Maine, Md., Mass., Mich., Minn., Mont., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>