<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">501</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHRASIA</taxon_name>
    <taxon_name authority="Funck ex Hoppe" date="1794" rank="species">salisburgensis</taxon_name>
    <taxon_name authority="Pugsley" date="1930" rank="variety">hibernica</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>48: 533, plate 37, figs. f, g. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus euphrasia;species salisburgensis;variety hibernica</taxon_hierarchy>
  </taxon_identification>
  <number>18a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, to 10 cm;</text>
      <biological_entity id="o6056" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1–6 pairs, from middle cauline nodes;</text>
      <biological_entity id="o6057" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="6" />
      </biological_entity>
      <biological_entity constraint="middle cauline" id="o6058" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o6057" id="r510" name="from" negation="false" src="d0_s1" to="o6058" />
    </statement>
    <statement id="d0_s2">
      <text>cauline internode lengths 1 times subtending leaves.</text>
      <biological_entity constraint="cauline" id="o6059" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o6060" is_modifier="false" name="length" src="d0_s2" value="1 times subtending leaves" value_original="1 times subtending leaves" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o6060" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade narrowly oblong or narrowly oblanceolate, 2–8 (–10) mm, margins sparsely dentate, teeth 1 or 2 (or 3) pairs, apices acute.</text>
      <biological_entity id="o6061" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6062" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6063" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o6064" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" unit="or pairs" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" unit="or pairs" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6065" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences beginning at node 5–10;</text>
      <biological_entity id="o6066" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
      <biological_entity id="o6067" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o6066" id="r511" name="beginning at" negation="false" src="d0_s4" to="o6067" />
    </statement>
    <statement id="d0_s5">
      <text>proximal internode lengths 0.8–1.5 times subtending bracts;</text>
      <biological_entity constraint="proximal" id="o6068" name="internode" name_original="internode" src="d0_s5" type="structure">
        <character constraint="bract" constraintid="o6069" is_modifier="false" name="length" src="d0_s5" value="0.8-1.5 times subtending bracts" value_original="0.8-1.5 times subtending bracts" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o6069" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts green, sometimes purplish, slightly broader than leaves, oblong to lanceolate, length 2.5+ times width, 4–8 mm, surfaces glabrous, teeth 1–3 pairs, longer or much longer than wide, apices narrowly acute, sometimes aristate.</text>
      <biological_entity id="o6070" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character constraint="than leaves" constraintid="o6071" is_modifier="false" name="width" src="d0_s6" value="slightly broader" value_original="slightly broader" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="2.5+" value_original="2.5+" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6071" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o6072" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6073" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="much longer than wide" value_original="much longer than wide" />
      </biological_entity>
      <biological_entity id="o6074" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: corolla white, adaxial lip lilac, 5–7 mm, abaxial lip slightly exceeding or equal to adaxial.</text>
      <biological_entity id="o6075" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6076" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6077" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lilac" value_original="lilac" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6078" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="position_relational" src="d0_s7" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6079" name="lip" name_original="lip" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules oblong to elliptic-oblong, 4–5 mm, margins eciliate or short-ciliate, apex truncate, retuse, or emarginate.</text>
      <biological_entity id="o6080" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="elliptic-oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6081" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 44 (Ireland).</text>
      <biological_entity id="o6082" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6083" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Short turf on shallow soils over limestone.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="short turf" constraint="on shallow soils" />
        <character name="habitat" value="shallow soils" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr. (Nfld.); w Europe (Ireland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" value="w Europe (Ireland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>