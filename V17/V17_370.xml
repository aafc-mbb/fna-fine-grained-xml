<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">92</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Bentham) Pennell" date="1935" rank="section">Dissecti</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 270. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section dissecti;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="subsect." date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="subgenus">Bentham</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 322. 1846</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;subgenus bentham</taxon_hierarchy>
  </taxon_identification>
  <number>26b8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o14309" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems retrorsely hairy, not glaucous.</text>
      <biological_entity id="o14310" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal usually withering, opposite, not leathery, glabrous or puberulent abaxially along midrib, not glaucous;</text>
      <biological_entity id="o14311" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14312" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="along midrib" constraintid="o14313" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o14313" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline short-petiolate or sessile, blade ovate, margins deeply pinnatifid to nearly pinnatisect.</text>
      <biological_entity constraint="cauline" id="o14314" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o14315" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o14316" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="deeply pinnatifid" name="shape" src="d0_s3" to="nearly pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, cylindric, axis puberulent, cymes 2 per node;</text>
      <biological_entity id="o14317" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o14318" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o14319" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o14320" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14320" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels ascending.</text>
      <biological_entity id="o14321" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o14322" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes: margins entire or erose, narrowly scarious, glandular-pubescent;</text>
      <biological_entity id="o14323" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o14324" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <biological_entity id="o14325" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lavender to violet, bilaterally symmetric, bilabiate, not personate, ventricose, glandular-pubescent externally, hairy internally abaxially, throat abruptly inflated, not constricted at orifice, slightly 2-ridged abaxially;</text>
      <biological_entity id="o14326" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14327" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s7" to="violet" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="personate" value_original="personate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14328" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o14329" is_modifier="false" modifier="not" name="size" src="d0_s7" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" notes="" src="d0_s7" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o14329" name="orifice" name_original="orifice" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>stamens included or longer pair reaching orifice, filaments glabrous, pollen-sacs divergent, saccate, dehiscing incompletely, distal 1/5–1/4 indehiscent, connective splitting, sides glabrous, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o14330" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o14331" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14332" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <biological_entity id="o14333" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14334" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="saccate" value_original="saccate" />
        <character is_modifier="false" modifier="incompletely" name="dehiscence" src="d0_s8" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s8" to="1/4" />
        <character is_modifier="false" name="dehiscence" src="d0_s8" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o14335" name="connective" name_original="connective" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s8" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o14336" name="side" name_original="sides" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14337" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o14338" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o14331" id="r1121" name="reaching" negation="false" src="d0_s8" to="o14332" />
    </statement>
    <statement id="d0_s9">
      <text>staminode exserted, flattened distally, 0.3–0.4 mm diam., tip straight, distal 10–20% hairy, hairs to 2 mm;</text>
      <biological_entity id="o14339" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14340" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14341" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14342" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="10-20%" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14343" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style sparsely glandular-pubescent proximally.</text>
      <biological_entity id="o14344" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14345" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules sparsely glandular-pubescent distally.</text>
      <biological_entity id="o14346" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s11" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark-brown to black, angled, 1.5–2.4 mm.</text>
      <biological_entity id="o14347" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s12" to="black" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Georgia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Georgia." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Placement of Penstemon dissectus, the lone member of sect. Dissecti, remains uncertain. It is one of two eastern North American species with saccate pollen sacs (the other being P. multiflorus) not allied with sect. Saccanthera; molecular data provide support for including it in subg. Penstemon (A. D. Wolfe et al. 2006).</discussion>
  
</bio:treatment>