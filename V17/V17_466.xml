<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="species">attenuatus</taxon_name>
    <taxon_name authority="(Greene) Cronquist in C. L. Hitchcock et al." date="1959" rank="variety">militaris</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>4: 373. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species attenuatus;variety militaris;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Greene" date="1906" rank="species">militaris</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 166. 1906</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species militaris</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">attenuatus</taxon_name>
    <taxon_name authority="(Greene) D. D. Keck" date="unknown" rank="subspecies">militaris</taxon_name>
    <taxon_hierarchy>genus p.;species attenuatus;subspecies militaris</taxon_hierarchy>
  </taxon_identification>
  <number>144b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (17–) 30–50 (–70) cm.</text>
      <biological_entity id="o10083" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="17" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx lobes lanceolate, rarely ovate, margins erose;</text>
      <biological_entity id="o10084" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity constraint="calyx" id="o10085" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o10086" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s1" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>corolla bluish purple, 10–17 mm;</text>
      <biological_entity id="o10087" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o10088" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish purple" value_original="bluish purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pollen-sacs 0.6–0.8 mm, dehiscing incompletely, distal 1/5 or less usually indehiscent, connective not splitting, sutures papillate.</text>
      <biological_entity id="o10089" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o10090" name="pollen-sac" name_original="pollen-sacs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s3" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="incompletely" name="dehiscence" src="d0_s3" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s3" value="1/5" value_original="1/5" />
        <character is_modifier="false" modifier="less usually" name="dehiscence" src="d0_s3" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o10091" name="connective" name_original="connective" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o10092" name="suture" name_original="sutures" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety militaris bears incompletely dehiscent pollen sacs, which also occur in Penstemon globosus. On specimens where the extent of dehiscence of the sacs is difficult to determine, the presence of prominently papillate-toothed sutures is a fairly reliable character distinguishing var. militaris from other varieties of P. attenuatus, though the teeth can be absent. On some specimens of var. militaris, the teeth reach nearly 0.1 mm.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky Douglas fir and lodgepole pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lodgepole pine forests" modifier="fir and" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>