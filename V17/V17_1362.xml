<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(Bentham) G. L. Nesom" date="2012" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 28. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eunanus</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 374. 1846</place_in_publication>
      <other_info_on_pub>(as fremonti)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus eunanus;species fremontii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="(Bentham) A. Gray" date="unknown" rank="species">fremontii</taxon_name>
    <taxon_hierarchy>genus mimulus;species fremontii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">subsecundus</taxon_name>
    <taxon_hierarchy>genus m.;species subsecundus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Fremont’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o9637" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 10–200 (–240) mm, glandular-puberulent or glandular-pubescent.</text>
      <biological_entity id="o9638" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="240" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="200" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal in rosette, cauline reduced distally;</text>
      <biological_entity id="o9639" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9640" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o9641" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o9642" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o9640" id="r765" name="in" negation="false" src="d0_s2" to="o9641" />
    </statement>
    <statement id="d0_s3">
      <text>petiole absent;</text>
      <biological_entity id="o9643" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic, sometimes obovate to oblanceolate, 2–30 (–55) × 1–10 (–16) mm, margins entire, sometimes crenate to serrate, plane, apex rounded to acute, surfaces: proximals glabrous, distals glandular-puberulent or glandular-pubescent.</text>
      <biological_entity id="o9644" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="obovate" modifier="sometimes" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9645" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character char_type="range_value" from="crenate" modifier="sometimes" name="shape" src="d0_s4" to="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o9646" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o9647" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o9648" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9649" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–4 (–7 on proximal) mm in fruit.</text>
      <biological_entity id="o9650" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o9651" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9651" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1 per node, chasmogamous.</text>
      <biological_entity id="o9652" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o9653" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o9653" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, inflated in fruit, 5–14 mm, glandular-puberulent to glandular-pubescent or ribs almost tomentose and viscid, lobes subequal, apex rounded and apiculate or acute, intercostal areas white.</text>
      <biological_entity id="o9654" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o9655" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o9656" is_modifier="false" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="glandular-puberulent" name="pubescence" src="d0_s7" to="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o9655" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o9656" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o9657" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="almost" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coating" src="d0_s7" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o9658" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o9659" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o9660" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas magenta to dark reddish purple, throat often darker near mouth, palate ridges yellow at mouth, throat floor glabrous or minutely puberulent, tube-throat 9–23 mm, limb 8–26 mm diam., not bilabiate.</text>
      <biological_entity id="o9661" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s8" to="dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o9662" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character constraint="near mouth" constraintid="o9663" is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o9663" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o9664" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character constraint="at mouth" constraintid="o9665" is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o9665" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
      <biological_entity id="o9666" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o9667" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="distance" src="d0_s8" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9668" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s8" to="26" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous, rarely minutely puberulent.</text>
      <biological_entity id="o9669" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely minutely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles minutely puberulent.</text>
      <biological_entity id="o9670" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas included, lobes subequal.</text>
      <biological_entity id="o9671" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o9672" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6.5–13 (–14) mm.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o9673" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9674" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus fremontii occurs from Monterey and San Benito counties south to San Diego County, east to Kern County and adjacent Inyo County, and in Baja California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soft, sandy soils along washes, flood plains, areas of water runoff, sandy hilltops and flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soft" constraint="along washes" />
        <character name="habitat" value="sandy soils" constraint="along washes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="areas" constraint="of water runoff" />
        <character name="habitat" value="water runoff" />
        <character name="habitat" value="sandy hilltops" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>