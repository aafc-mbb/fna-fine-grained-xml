<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Coerulei</taxon_name>
    <taxon_name authority="N. H. Holmgren" date="1978" rank="species">immanifestus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 334, fig. 1. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species immanifestus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>41.</number>
  <other_name type="common_name">Steptoe Valley beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 9–30 (–35) cm, glabrous.</text>
      <biological_entity id="o868" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, basal not reduced, glabrous;</text>
      <biological_entity id="o869" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 15–60 (–90) × 8–25 (–39) mm, blade oblanceolate to obovate, base tapered, apex rounded to obtuse, sometimes mucronate;</text>
      <biological_entity constraint="basal" id="o870" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s1" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o871" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o872" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="obovate" />
      </biological_entity>
      <biological_entity id="o873" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o874" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–7 pairs, short-petiolate or sessile, (25–) 35–80 × 10–20 (–28) mm, blade lanceolate to ovate, base tapered to clasping, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o875" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_length" src="d0_s3" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="28" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o876" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate" />
      </biological_entity>
      <biological_entity id="o877" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o878" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous or interrupted, cylindric, (4–) 6–20 cm, axis glabrous, verticillasters (3–) 5–10 (–14), cymes 2–5-flowered;</text>
      <biological_entity id="o879" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o880" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o881" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
      <biological_entity id="o882" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to lanceolate, (5–) 13–56 × (2–) 4–24 mm;</text>
      <biological_entity constraint="proximal" id="o883" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s5" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="56" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s5" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glabrous, peduncles to 12 mm.</text>
      <biological_entity id="o884" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o885" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o886" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 5–8 (–12) × 2–3.5 mm, margins entire or erose, broadly scarious, apex acuminate, glabrous;</text>
      <biological_entity id="o887" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o888" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o889" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="broadly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o890" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla bluish lavender to lavender or pink, with dark reddish violet or reddish nectar guides, funnelform to ventricose, 15–22 mm, glabrous externally, sparsely white-villous internally abaxially or glabrous, tube 5–7 mm, throat gradually inflated, 5–8 mm diam., rounded abaxially;</text>
      <biological_entity id="o891" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o892" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bluish lavender" value_original="bluish lavender" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; internally abaxially; abaxially" name="pubescence" src="d0_s8" value="white-villous" value_original="white-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o893" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="violet" value_original="violet" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o894" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o895" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o892" id="r72" name="with" negation="false" src="d0_s8" to="o893" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs opposite, (1–) 1.2–1.6 mm, sutures papillate;</text>
      <biological_entity id="o896" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o897" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o898" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_distance" src="d0_s9" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o899" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 8–10 mm, included or reaching orifice, 0.7–1 mm diam., tip strongly recurved, distal 2–5 mm densely villous, hairs yellowish, brownish, or orangish, to 1 mm;</text>
      <biological_entity id="o900" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o901" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character name="position" src="d0_s10" value="reaching orifice" value_original="reaching orifice" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="diameter" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o902" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o903" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o904" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish" value_original="orangish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish" value_original="orangish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 10–12 mm.</text>
      <biological_entity id="o905" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o906" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 8–12 × 4–6 mm.</text>
      <biological_entity id="o907" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–4 mm.</text>
      <biological_entity id="o908" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon immanifestus occurs along the western edge of the Bonneville Basin in eastern Nevada and western Utah, and in the Calcareous Mountains, central Great Basin, and Tonopah regions of eastern and central Nevada. Populations have been documented in Elko, Eureka, Lander, Nye, and White Pine counties, Nevada, and in Juab, Millard, and Tooele counties, Utah.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or sandy-loam soils, sagebrush grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="sandy-loam soils" />
        <character name="habitat" value="grasslands" modifier="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>