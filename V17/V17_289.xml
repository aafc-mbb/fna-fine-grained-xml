<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Crosswhite" date="1966" rank="section">Chamaeleon</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">lanceolatus</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>22. 1839</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section chamaeleon;species lanceolatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Crosswhite" date="unknown" rank="species">ramosus</taxon_name>
    <taxon_hierarchy>genus penstemon;species ramosus</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Lance-leaf beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 24–75 (–98) cm, sparsely to densely retrorsely hairy or puberulent, usually also glandular-pubescent distally.</text>
      <biological_entity id="o7568" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="98" to_unit="cm" />
        <character char_type="range_value" from="24" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely; densely retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="usually; distally" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves retrorsely hairy;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 8–80 × 3–12 mm, blade lanceolate or spatulate, base tapered, margins entire, apex rounded to obtuse or acute;</text>
      <biological_entity id="o7569" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o7570" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o7571" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o7572" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o7573" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7574" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 7–15 pairs, 9–85 (–110) × 1–11 (–22) mm, blade lanceolate to linear, base tapered to truncate, apex acute to acuminate, rarely obtuse.</text>
      <biological_entity constraint="cauline" id="o7575" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s3" to="15" />
        <character char_type="range_value" from="85" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="110" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s3" to="85" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7576" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o7577" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s3" to="truncate" />
      </biological_entity>
      <biological_entity id="o7578" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses ± secund, 5–36 cm, axis glandular-pubescent, verticillasters 3–6, cymes 1 or 2 (or 3) -flowered;</text>
      <biological_entity id="o7579" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="36" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7580" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o7581" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o7582" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 5–16 × 1–3 mm;</text>
      <biological_entity constraint="proximal" id="o7583" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent, sometimes also retrorsely hairy.</text>
      <biological_entity id="o7584" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7585" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 2.9–7 × 1.5–2.5 (–3.6) mm, glandular-pubescent;</text>
      <biological_entity id="o7586" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o7587" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla scarlet to red, unlined internally or lined on abaxial surface with faint reddish purple nectar guides, tubular-funnelform, 22–35 mm, glandular-pubescent internally abaxially, tube 5–6 mm, throat gradually inflated, 6–8 mm diam., rounded abaxially;</text>
      <biological_entity id="o7588" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7589" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="scarlet" name="coloration" src="d0_s8" to="red" />
        <character constraint="on abaxial surface" constraintid="o7590" is_modifier="false" modifier="internally" name="architecture" src="d0_s8" value="lined" value_original="lined" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7590" name="surface" name_original="surface" src="d0_s8" type="structure" />
      <biological_entity constraint="nectar" id="o7591" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o7592" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7593" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o7590" id="r628" name="with" negation="false" src="d0_s8" to="o7591" />
    </statement>
    <statement id="d0_s9">
      <text>stamens exserted (hidden by galeate adaxial lobes), pollen-sacs parallel to divergent, 1.5–2 mm, sutures denticulate, teeth to 0.2 mm;</text>
      <biological_entity id="o7594" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7595" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o7596" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character char_type="range_value" from="parallel" name="arrangement" src="d0_s9" to="divergent" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7597" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o7598" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 15–17 mm, included, 0.3–0.4 mm diam.;</text>
      <biological_entity id="o7599" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7600" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 25–30 mm, usually barely exserted from galea.</text>
      <biological_entity id="o7601" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7602" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7603" name="galea" name_original="galea" src="d0_s11" type="structure" />
      <relation from="o7602" id="r629" modifier="usually barely" name="exserted from" negation="false" src="d0_s11" to="o7603" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 9–14 × 6–7 mm. 2n = 16.</text>
      <biological_entity id="o7604" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7605" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon lanceolatus is known from southern Arizona (Cochise, Graham, and Greenlee counties), southern New Mexico (Doña Ana, Grant, Hidalgo, Luna, and Sierra counties), and western Texas (Brewster County).</discussion>
  <discussion>Crosswhite published Penstemon ramosus as an avowed substitute for the homonym P. pauciflorus Greene, distinguishing P. ramosus from P. lanceolatus by the former’s branched stems and relatively narrower leaves (1 mm wide versus 4–8 mm wide) with revolute margins. He mapped P. ramosus in southeastern Arizona and southwestern New Mexico and P. lanceolatus in western Texas and northern Mexico. Morphologic differences between these taxa are not consistent enough to warrant recognition of P. ramosus (J. L. Anderson et al. 2007).</discussion>
  <references>
    <reference>Anderson, J. L., S. Richmond-Williams, and O. Williams. 2007. Penstemon lanceolatus Benth. or P. ramosus Crosswhite in Arizona and New Mexico, a peripheral or endemic species?  In: P. L. Barlow-Irick et al., eds. 2007. Southwestern Rare and Endangered Plants: Proceedings of the Fourth Conference. Fort Collins. Pp. 8–15.</reference>
  </references>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly pinyon-juniper woodlands, pine woodlands, thorn scrub, desert grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly pinyon-juniper woodlands" />
        <character name="habitat" value="pine woodlands" />
        <character name="habitat" value="thorn scrub" />
        <character name="habitat" value="desert grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Aguascalientes, Chihuahua, Coahuila, Durango, Nayarit, Nuevo León, San Luis Potosí, Tamaulipas, Veracruz, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Aguascalientes)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>